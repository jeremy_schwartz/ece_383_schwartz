# Lab 5 - Rokenbok FPGA Interface

## Author: C2C Jeremy Schwartz

### Last Modified: 24 April 2018

**Documentation:** None

#### Milestone 1 (Completed 18 April 2018)

**Requirements Per Plan:**

- Create the physical interface between the Nexys Video and the Rokenbok Command Deck using a breadboard.  Use the oscilloscope or digital multimeter to verify that the logic level shifts are working correctly.
- Instantiate a Microblaze processor and verify “Hello World” functionality.
- Program the Control Unit and Datapath in VHDL.  After this point, only minor changes to the VHDL code in the AXI component will be required.
- Display received packets in the UART console to verify that the SPI interface can properly receive packets.

**Accomplishments So Far:**

- I have constructed an interface that connects to the Rokenbok command deck using a PS/2 cable and connects to the Nexys Video through the JA ports.  The interface steps up master out signals to 3.3V and steps up slave out signals to 5V.
- I have instantiated a Microblaze and have verified basic UART functionality.
- I have programmed the control unit and datapath and verified their functionality by using the logic analyzer.
- I have used a print command to print out received packets and verified that they are valid packets.

#### Milestone 2 (Completed 19 April 2018)

**Requirements Per Plan:**

- Send packets to the Rokenbok Command Deck and edit the attribute byte during the sync phase.  Verify that TPADS/SELECT editing was enabled in the attribute byte by noticing that EDIT_TPADS and EDIT_SELECT packets are now being received.
- Control a single virtual controller through the UART command line by manipulating the Command Deck’s TPADS and SELECT bytes.  Verify functionality through physical vehicle movement.


**Accomplishments So Far:**

- I have successfully edited the attribute byte during the sync phase and verified this by confirming that EDIT_TPADS and EDIT_SELECT packets are now being received.
- I can now control a single virtual controller through the UART command line by manipulating TPADS and SELECT.  I verified this by visually confirming vehicle selection and movement.

#### Required Functionality (Completed 19 April 2018)

**Requirements Per Plan:**

Program an AXI peripheral for the Microblaze that serves as an SPI interface between the microcontroller and the Rokenbok command deck.  Construct a physical link between there peripherals using a breadboard.  At this point the Microblaze can control the command deck using a preprogrammed sequence.

**Accomplishments So Far:**

I constructed an interface to connect to the Rokenbok command deck and used my Microblaze to drive a vehicle in a single direction. 

#### B Level Functionality (Completed 19 April 2018)

**Requirements Per Plan:**

Control a single virtual controller at a time using the UART interface through the command line.  The interface should allow the user to select a vehicle (1-8) and a virtual controller (5-8).

**Accomplishments So Far:**

I coded an interface in the UART console that allows me to control a single virtual controller to control a single vehicle.

#### A Level Functionality (Completed 24 April 2018)

**Requirements Per Plan:**

Use an external controller (NES, SNES, X-Box, etc.) to control a single virtual controller in addition to the virtual controller powered by the UART interface.  The system must be able to control several virtual controllers at the same time.

**Accomplishments So Far:**

I coded an interface for the SNES remote so that I can now control two virtual controllers at the same time.