/*--------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	17 Apr 2018
-- File:	main.c
-- Event:	Lab5
-- Crs:		ECE 383
--
-- Purp:	Implement the Lab5 Microblaze functionality.
--
-- Documentation:	None
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"

#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out16 and its variations

/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for our project.
 */
#define base_reg				0x44a00000
#define send_data_reg			base_reg			// slv_reg0
#define	rec_data_reg			base_reg+4			// slv_reg1
#define	new_byte_reg			base_reg+8			// slv_reg2
#define	snes_reg				base_reg+12			// slv_reg3

#define printf xil_printf			/* A smaller footprint printf */

#define	uartRegAddr				0x40600000		// read <= RX, write => TX

#define enable_attrib_byte		0x0D
#define disable_attrib_byte		0x00

/************************** Function Prototypes ****************************/
void myISR(void);

/************************** Variable Definitions **************************/
/*
 * The following are declared globally so they are zeroed and so they are
 * easily accessible from a debugger
 */

u8 v1_forward = 0;
u8 v1_back = 0;
u8 v1_left = 0;
u8 v1_right = 0;
u8 v1_a = 0;
u8 v1_b = 0;
u8 v1_x = 0;
u8 v1_y = 0;
u8 v1_slow = 0;

u8 send_no_sel_to = 0xFF;

u8 rec_attrib_byte;
u8 rec_priority_byte;
u8 rec_tpads[17];
u8 rec_select[8];

// 0 = Not In Series
// 1 = Sync
// 2 = Edit T-Pads
// 3 = Edit Select
u8 current_series = 0;

// Current transmission in series
u8 series_count = 0;

u8 rec_data = 0x00;
u8 send_data = 0x00;

u8 virt1_select = 0;
u8 virt2_select = 0;

u16 snes_buttons = 0xFFFF;

int main(void) {

	unsigned char c;

	init_platform();

	print("Welcome to Lab5\n\r");

    microblaze_register_handler((XInterruptHandler) myISR, (void *) 0);
    microblaze_enable_interrupts();

    while(1) {

    	c = XUartLite_RecvByte(uartRegAddr);

		switch(c) {

    		/*-------------------------------------------------
    		 * Reply with the help menu
    		 *-------------------------------------------------
			 */
    		case '?':
    			printf("--------------------------\r\n");
    			printf("Last Byte = 0x%02x\r\n", rec_data);
    			printf("--------------------------\r\n");
    			printf("?: help menu\r\n");
    			printf("f: clear console\r\n");
    			break;

			/*-------------------------------------------------
			 * Clear the terminal window
			 *-------------------------------------------------
			 */
            case 'f':
            	for (c=0; c<40; c++) printf("\r\n");
               	break;

			/*-------------------------------------------------
			 * Increment Select For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case ']':
               	if (virt1_select == 7) {
               		virt1_select = 0;
               	} else {
                   	virt1_select++;
               	}
    			printf("V1_Select: %i\r\n", virt1_select + 1);
				break;

			/*-------------------------------------------------
			 * Decrement Select For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case '[':
               	if (virt1_select == 0) {
               		virt1_select = 7;
               	} else {
                   	virt1_select--;
               	}
				printf("V1_Select: %i\r\n", virt1_select + 1);
				break;
				
			/*-------------------------------------------------
			 * Increment Select For Virtual Controller 2
			 *-------------------------------------------------
			 */
			case '=':
               	if (virt2_select == 7) {
               		virt2_select = 0;
               	} else {
                   	virt2_select++;
               	}
    			printf("V2_Select: %i\r\n", virt2_select + 1);
				break;

			/*-------------------------------------------------
			 * Decrement Select For Virtual Controller 2
			 *-------------------------------------------------
			 */
			case '-':
               	if (virt2_select == 0) {
               		virt2_select = 7;
               	} else {
                   	virt2_select--;
               	}
				printf("V2_Select: %i\r\n", virt2_select + 1);
				break;

			/*-------------------------------------------------
			 * Move Forward For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case 'w':
				if (v1_forward == 0) {
					v1_forward = 1;
				} else {
					v1_forward = 0;
				}
				break;

			/*-------------------------------------------------
			 * Move Back For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case 's':
				if (v1_back == 0) {
					v1_back = 1;
				} else {
					v1_back = 0;
				}
				break;

			/*-------------------------------------------------
			 * Move Left For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case 'a':
				if (v1_left == 0) {
					v1_left = 1;
				} else {
					v1_left = 0;
				}
				break;
				break;

			/*-------------------------------------------------
			 * Move Right For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case 'd':
				if (v1_right == 0) {
					v1_right = 1;
				} else {
					v1_right = 0;
				}
				break;

			/*-------------------------------------------------
			 * A For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case 'e':
				if (v1_b == 0) {
					v1_b = 1;
				} else {
					v1_b = 0;
				}
				break;

			/*-------------------------------------------------
			 * B For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case 'q':
				if (v1_a == 0) {
					v1_a = 1;
				} else {
					v1_a = 0;
				}
				break;

			/*-------------------------------------------------
			 * X For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case 'z':
				if (v1_x == 0) {
					v1_x = 1;
				} else {
					v1_x = 0;
				}
				break;

			/*-------------------------------------------------
			 * Y For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case 'x':
				if (v1_y == 0) {
					v1_y = 1;
				} else {
					v1_y = 0;
				}
				break;

			/*-------------------------------------------------
			 * Slow For Virtual Controller 1
			 *-------------------------------------------------
			 */
			case 'c':
				if (v1_slow == 0) {
					v1_slow = 1;
				} else {
					v1_slow = 0;
				}
				break;

			/*-------------------------------------------------
			 * Stop
			 *-------------------------------------------------
			 */
			case ' ':
				v1_forward = 0;
				v1_back = 0;
				v1_left = 0;
				v1_right = 0;
				v1_a = 0;
				v1_b = 0;
				v1_x = 0;
				v1_y = 0;
				break;

			/*-------------------------------------------------
			 * Unknown character was
			 *-------------------------------------------------
			 */
    		default:
    			printf("unrecognized character: %c\r\n",c);
    			break;
    	} // end case

    } // end while 1

    cleanup_platform();

    return 0;
} // end main


void myISR(void) {
	// Receive Data
	rec_data = Xil_In8(rec_data_reg);

	// Do Stuff To Determine Next Packet
	if (current_series == 0) {
		if (rec_data == 0xc6) {
			current_series = 1;
			series_count = 1;
			send_data = 0x81;
			//printf("YEAAAAAAAH BOI SYNC\r\n");
		} else if (rec_data == 0xc3) {
			snes_buttons = Xil_In16(snes_reg);
			current_series = 2;
			series_count = 1;
			send_data = 0x80;
			//printf("YEAAAAAAAH BOI TPADS\r\n");
		} else if (rec_data == 0xc4) {
			current_series = 3;
			series_count = 1;
			send_data = 0x80;
			//printf("YEAAAAAAAH BOI SELECT\r\n");
		} else {
			current_series = 0;
			series_count = 0;
			send_data = 0x00;
		}

	} else if (current_series == 1) { // Sync
		if (series_count == 1) {
			series_count = 2;
			send_data = enable_attrib_byte;
		} else if (series_count == 2) {
			series_count = 3;
			send_data = send_no_sel_to;
		} else if (series_count == 3) {
			current_series = 0;
			series_count = 0;
			send_data = 0x00;
		}

	} else if (current_series == 2) { // Edit T-Pads
		if (series_count == 1) {
			series_count = 2;
			rec_tpads[0] = rec_data; // Select
			send_data = rec_tpads[0] & 0b11001111;
		} else if (series_count == 2) {
			series_count = 3;
			rec_tpads[1] = rec_data; // Left Trigger (Last Select)
			send_data = rec_tpads[1] & 0b11001111;
		} else if (series_count == 3) {
			series_count = 4;
			rec_tpads[2] = rec_data; // Sharing Mode (1 = allow sharing);
			send_data = rec_tpads[2] & 0b11001111;
		} else if (series_count == 4) {
			series_count = 5;
			rec_tpads[3] = rec_data; // RESERVED
			send_data = rec_tpads[3];
		} else if (series_count == 5) {
			series_count = 6;
			rec_tpads[4] = rec_data; // IS16SEL?
			send_data = rec_tpads[4] & 0b11001111;
		} else if (series_count == 6) {
			series_count = 7;
			rec_tpads[5] = rec_data; // D Pad Up
			if (v1_forward == 0) {
				send_data = rec_tpads[5] & 0b11101111;
			} else {
				send_data = rec_tpads[5] | 0b00010000;
			}
			
			if (snes_buttons & (1 << 11)) {
				send_data = send_data & 0b11011111;
			} else {
				send_data = send_data | 0b00100000;
			}
		} else if (series_count == 7) {
			series_count = 8;
			rec_tpads[6] = rec_data; // D Pad Down
			if (v1_back == 0) {
				send_data = rec_tpads[6] & 0b11101111;
			} else {
				send_data = rec_tpads[6] | 0b00010000;
			}
			
			if (snes_buttons & (1 << 10)) {
				send_data = send_data & 0b11011111;
			} else {
				send_data = send_data | 0b00100000;
			}
		} else if (series_count == 8) {
			series_count = 9;
			rec_tpads[7] = rec_data; // D Pad Right
			if (v1_right == 0) {
				send_data = rec_tpads[7] & 0b11101111;
			} else {
				send_data = rec_tpads[7] | 0b00010000;
			}
			
			if (snes_buttons & (1 << 8)) {
				send_data = send_data & 0b11011111;
			} else {
				send_data = send_data | 0b00100000;
			}
		} else if (series_count == 9) {
			series_count = 10;
			rec_tpads[8] = rec_data; // D Pad Left
			if (v1_left == 0) {
				send_data = rec_tpads[8] & 0b11101111;
			} else {
				send_data = rec_tpads[8] | 0b00010000;
			}
			
			if (snes_buttons & (1 << 9)) {
				send_data = send_data & 0b11011111;
			} else {
				send_data = send_data | 0b00100000;
			}
		} else if (series_count == 10) {
			series_count = 11;
			rec_tpads[9] = rec_data; // A
			if (v1_a == 0) {
				send_data = rec_tpads[9] & 0b11101111;
			} else {
				send_data = rec_tpads[9] | 0b00010000;
			}
			
			if (snes_buttons & (1 << 7)) {
				send_data = send_data & 0b11011111;
			} else {
				send_data = send_data | 0b00100000;
			}
		} else if (series_count == 11) {
			series_count = 12;
			rec_tpads[10] = rec_data; // B
			if (v1_b == 0) {
				send_data = rec_tpads[10] & 0b11101111;
			} else {
				send_data = rec_tpads[10] | 0b00010000;
			}
			
			if (snes_buttons & (1 << 15)) {
				send_data = send_data & 0b11011111;
			} else {
				send_data = send_data | 0b00100000;
			}
		} else if (series_count == 12) {
			series_count = 13;
			rec_tpads[11] = rec_data; // X
			if (v1_x == 0) {
				send_data = rec_tpads[11] & 0b11101111;
			} else {
				send_data = rec_tpads[11] | 0b00010000;
			}
			
			if (snes_buttons & (1 << 6)) {
				send_data = send_data & 0b11011111;
			} else {
				send_data = send_data | 0b00100000;
			}
		} else if (series_count == 13) {
			series_count = 14;
			rec_tpads[12] = rec_data; // Y
			if (v1_y == 0) {
				send_data = rec_tpads[12] & 0b11101111;
			} else {
				send_data = rec_tpads[12] | 0b00010000;
			}
			
			if (snes_buttons & (1 << 14)) {
				send_data = send_data & 0b11011111;
			} else {
				send_data = send_data | 0b00100000;
			}
		} else if (series_count == 14) {
			series_count = 15;
			rec_tpads[13] = rec_data; // RESERVED
			send_data = rec_tpads[13];
		} else if (series_count == 15) {
			series_count = 16;
			rec_tpads[14] = rec_data; // RESERVED
			send_data = rec_tpads[14];
		} else if (series_count == 16) {
			series_count = 17;
			rec_tpads[15] = rec_data; // Right Trigger (Slow)
			if (v1_slow == 0) {
				send_data = rec_tpads[15] & 0b11101111;
			} else {
				send_data = rec_tpads[15] | 0b00010000;
			}
			
			if (snes_buttons & (1 << 4)) {
				send_data = send_data & 0b11011111;
			} else {
				send_data = send_data | 0b00100000;
			}
		} else if (series_count == 17) {
			series_count = 18;
			rec_tpads[16] = rec_data; // Spare
			send_data = rec_tpads[16];
		} else if (series_count == 18) {
			series_count = 19;
			rec_priority_byte = rec_data; // Priority Byte
			send_data = rec_priority_byte | 0b00110000; //rec_priority_byte;
		} else if (series_count == 19) {
			current_series = 0;
			series_count = 0;
			send_data = 0x00;
		}

	} else if (current_series == 3) { // Edit Select
		if (series_count == 1) {
			series_count = 2;
			rec_select[0] = rec_data;
			send_data = rec_select[0];
		} else if (series_count == 2) {
			series_count = 3;
			rec_select[1] = rec_data;
			send_data = rec_select[1];
		} else if (series_count == 3) {
			series_count = 4;
			rec_select[2] = rec_data;
			send_data = rec_select[2];
		} else if (series_count == 4) {
			series_count = 5;
			rec_select[3] = rec_data;
			send_data = rec_select[3];
		} else if (series_count == 5) {
			series_count = 6;
			rec_select[4] = rec_data;
			send_data = virt1_select;
		} else if (series_count == 6) {
			series_count = 7;
			rec_select[5] = rec_data;
			send_data = virt2_select;
		} else if (series_count == 7) {
			series_count = 8;
			rec_select[6] = rec_data;
			send_data = 0x0F;
		} else if (series_count == 8) {
			series_count = 9;
			rec_select[7] = rec_data;
			send_data = 0x0F;
		} else if (series_count == 9) {
			series_count = 10;
			send_data = 0x00;
			// Timer Value Received, Send Null
		} else if (series_count == 10) {
			current_series = 0;
			series_count = 0;
			send_data = 0x00;
		}

	} else {
		current_series = 0;
		series_count = 0;
		send_data = 0x00;
	}

	// Send Data
	Xil_Out8(send_data_reg, send_data);

	Xil_Out8(new_byte_reg, 0x01);
	Xil_Out8(new_byte_reg, 0x00);
}
