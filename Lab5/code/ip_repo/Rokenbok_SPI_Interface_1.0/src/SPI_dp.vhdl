--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 22 April 2018
-- Course: ECE 383
-- File: SPI_dp.vhdl
-- Project: Lab5
--
-- Purp: This file implements the datapath for lab 5.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SPI_dp is
    Port(
	clk : in  STD_LOGIC;
	fast_clk : in STD_LOGIC;
	reset_n : in  STD_LOGIC;
	
	sclk : in  STD_LOGIC;
	miso : out  STD_LOGIC;
	mosi : in  STD_LOGIC;
	slave_ready : out  STD_LOGIC;
	frame_end : in  STD_LOGIC;
	
	receive_byte : out  STD_LOGIC_VECTOR(7 downto 0);
	send_byte : in  STD_LOGIC_VECTOR(7 downto 0);
	ready : out  STD_LOGIC;
	new_byte : in  STD_LOGIC;
	
	waiting_byte : out STD_LOGIC;
	
	cw: in STD_LOGIC_VECTOR(6 downto 0);
	sw: out STD_LOGIC_VECTOR(3 downto 0));
end SPI_dp;

architecture structure of SPI_dp is

	signal bit_count : UNSIGNED(3 downto 0);
	signal packet_in_process : STD_LOGIC_VECTOR(7 downto 0);
    signal packet_out_process : STD_LOGIC_VECTOR(7 downto 0);
    signal new_packet_s, ready_s : STD_LOGIC;

begin

    -- Counter Process
    process(clk)
        begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                bit_count <= "0000";
            elsif (cw(1) = '1') then
                bit_count <= "0000";
            elsif (cw(0) = '1') then
                bit_count <= bit_count + 1;
            end if;
        end if;
    end process;
    
    -- Input SR Process
    process(clk)
        begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                packet_in_process <= x"00";
            elsif (cw(2) = '1') then
                packet_in_process <= packet_in_process(6 downto 0) & mosi;
            elsif (cw(5) = '1') then
                receive_byte <= packet_in_process;
            end if;
        end if;
    end process;
    
    -- Output SR Process
    process(clk)
        begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                packet_out_process <= x"00";
            elsif (cw(3) = '1') then
                packet_out_process <= packet_out_process(6 downto 0) & "0";
                --miso <= packet_out_process(7);
            elsif (cw(6) = '1') then
                packet_out_process <= send_byte;
                --miso <= packet_out_process(7);
            end if;
        end if;
    end process;
    
    -- New Byte Process
    process(fast_clk)
        begin
        if (rising_edge(fast_clk)) then
            if (reset_n = '0') then
                new_packet_s <= '0';
            elsif (cw(6) = '1') then
                new_packet_s <= '0';
            elsif (new_byte = '1') then
                new_packet_s <= '1';
            end if;
        end if;
    end process;
    
    -- Rising Edge Ready Process
    process(clk)
        begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                ready_s <= '0';
            else
                ready_s <= cw(5);
            end if;
        end if;
    end process;
    
    sw(0) <= sclk;
    sw(1) <= new_packet_s;
    sw(2) <= '1' when bit_count > "0111" else '0';
    sw(3) <= frame_end;
    
    miso <= packet_out_process(7);
    waiting_byte <= new_packet_s;
    
    slave_ready <= cw(4);
    ready <= '1' when (((cw(5) xor ready_s) = '1') and (cw(5) = '1')) else '0';
	
end structure;
