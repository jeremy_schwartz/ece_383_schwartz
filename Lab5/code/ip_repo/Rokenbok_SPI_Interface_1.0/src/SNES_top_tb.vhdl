--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 22 April 2018
-- Course: ECE 383
-- File: SNES_top_tb.vhdl
-- Project: Lab5
--
-- Purp: This file contains the tesbench for the SNES unit.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY SNES_top_tb IS
END SNES_top_tb;
 
ARCHITECTURE behavior OF SNES_top_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
	component SNES_top is
		Port(
		-- Basic Signals
		clk : in  STD_LOGIC;
		reset_n : in  STD_LOGIC;
		
		-- Serial Interface
		snes_data : in STD_LOGIC;
		snes_sclk : out STD_LOGIC;
		snes_latch : out STD_LOGIC;
		
		-- Parallel Interface
		button_double : out STD_LOGIC_VECTOR(15 downto 0));
	end component;

	signal clk : STD_LOGIC;
	signal reset_n : STD_LOGIC;
	signal snes_data : STD_LOGIC;
	signal snes_sclk : STD_LOGIC;
	signal snes_latch : STD_LOGIC;
	signal button_double : STD_LOGIC_VECTOR(15 downto 0);
	
   -- Clock period definitions
   constant clk_period : time := 10 ns;  -- Sets clock to ~ 100MHz

BEGIN
	-- Instantiate the Unit Under Test (UUT)
   uut: SNES_top PORT MAP (
          clk => clk,
          reset_n => reset_n,
  		  snes_data => snes_data,
          snes_sclk => snes_sclk,
          snes_latch => snes_latch,
          button_double => button_double
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
   
   snes_data <= '1';
   reset_n <= '1';

   -- -- Stimulus process
   -- stim_proc: process 
   -- begin		
      -- -- hold reset state for 100 ns.
		-- reset_n <= '0', '1' after 10 ns;
		-- cw <= "011", "011" after 30 ns;
			-- -- insert stimulus here 
		-- wait;
   -- end process;

END;
