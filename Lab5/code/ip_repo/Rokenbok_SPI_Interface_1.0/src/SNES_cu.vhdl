--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 22 April 2018
-- Course: ECE 383
-- File: SNES_cu.vhdl
-- Project: Lab5
--
-- Purp: This file implements the SNES capture control unit for Lab5.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

-- sw(0) = frame_end
-- sw(1) = sclk

-- cw(0) = byte_received
-- cw(1) = slave_ready

library IEEE;		
use IEEE.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

entity SNES_cu is
	Port(	clk: in  STD_LOGIC;
	        snes_clk: in  STD_LOGIC;
	        old_snes_clk: in  STD_LOGIC;
            reset_n : in  STD_LOGIC;
            cw: out STD_LOGIC_VECTOR(2 downto 0);
            sw: in STD_LOGIC_VECTOR(1 downto 0));
end SNES_cu;

architecture behavior of SNES_cu is

	type state_type is (reset, wait_pulse_high, wait_pulse_low, wait_one, sclk_low, sclk_high, done);
	signal state: state_type;
			
begin
	
    state_process: process(clk)
    begin
        if rising_edge(clk) then
            if (old_snes_clk = '0' and snes_clk = '1') then
                if (reset_n = '0') then 
                    state <= reset;
                else
                    case state is
                        when reset =>
                            state <= wait_pulse_high;
                            
                        when wait_pulse_high =>
                            if (sw(0) = '1') then
                                state <= wait_pulse_low;
                            end if;
                            
                        when wait_pulse_low =>
                            if (sw(0) = '0') then
                                state <= wait_one;
                            end if;
                            
                        when wait_one =>
                            state <= sclk_low;
                                                  
                        when sclk_low =>
                            state <= sclk_high;
                            
                        when sclk_high =>
                            if (sw(1) = '1') then
                                state <= done;
                            else
                                state <= sclk_low;
                            end if;
                                                
                        when done =>
                            state <= wait_pulse_high;
                            
                    end case;
                end if;
            end if;
        end if;
    end process;
    
cw <=   "100" when state = reset else
        "100" when state = wait_pulse_high else
        "100" when state = wait_pulse_low else 
        "100" when state = wait_one else
        "001" when state = sclk_low else
        "100" when state = sclk_high else
        "110" when state = done else
        "100"; -- Catch All

end behavior;