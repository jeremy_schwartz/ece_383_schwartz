--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 22 April 2018
-- Course: ECE 383
-- File: SPI_cu.vhdl
-- Project: Lab5
--
-- Purp: This file implements the SPI capture control unit for Lab5.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

-- CW(0): Increment Counter
-- CW(1): Reset Counter
-- CW(2): Perform Shift Operation
-- CW(3): Slave Ready
-- CW(4): Byte Complete

-- SW(0): Slave Clock
-- SW(1): New Byte Received
-- SW(2): Byte Complete
-- SW(3): Frame End

library IEEE;		
use IEEE.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

entity SPI_cu is
	Port(	clk: in  STD_LOGIC;
            reset_n : in  STD_LOGIC;
            cw: out STD_LOGIC_VECTOR(6 downto 0);
            sw: in STD_LOGIC_VECTOR(3 downto 0));
end SPI_cu;

architecture behavior of SPI_cu is

	type state_type is (reset, wait_byte, load_byte, wait_high, capture, wait_low, change, end_frame);
	signal state: state_type;
			
begin
	
    state_process: process(clk)
    begin
        if rising_edge(clk) then
            if (reset_n = '0') then 
                state <= reset;
            else
                case state is
                    when reset =>
                        state <= wait_byte;
                        
                    when wait_byte =>
                        if (sw(1) = '1' and sw(3) = '1') then
                            state <= load_byte;
                        elsif (sw(0) = '1' and sw(3) = '1') then
                            state <= capture;
                        end if;
                        
                    when load_byte =>
                        state <= wait_high;
                                              
                    when wait_high =>
                        if sw(0) = '1' then
                            state <= capture;
                        elsif sw(3) = '0' then
                            state <= end_frame;
                        end if;
                        
                    when capture =>
                        state <= wait_low;
                        
                    when wait_low =>
                        if sw(0) = '0' then
                            state <= change;
                        end if;
                        
                    when change =>
                        if sw(2) = '1' then
                            state <= end_frame;
                        else
                            state <= wait_high;
                        end if;
                        
                    when end_frame =>
                        if sw(3) = '0' then
                            state <= wait_byte;
                        end if;
                        
                end case;
            end if;
        end if;
    end process;

cw <=   "0010010" when state = reset else
        "0010010" when state = wait_byte else
        "1010000" when state = load_byte else 
        "0000000" when state = wait_high else
        "0000101" when state = capture else
        "0000000" when state = wait_low else
        "0001000" when state = change else
        "0110000" when state = end_frame else 
        "0000000"; -- Catch All

end behavior;