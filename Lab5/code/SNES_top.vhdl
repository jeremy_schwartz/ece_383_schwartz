--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 22 April 2018
-- Course: ECE 383
-- File: SNES_top.vhdl
-- Project: Lab5
--
-- Purp: This file implements the top level for the Lab5 SNES interface.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity SNES_top is
    Port(
	-- Basic Signals
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	
	-- Serial Interface
	snes_data : in STD_LOGIC;
	snes_sclk : out STD_LOGIC;
	snes_latch : out STD_LOGIC;
	
	-- Parallel Interface
	button_double : out STD_LOGIC_VECTOR(15 downto 0));
end SNES_top;

architecture structure of SNES_top is

signal cw_s : STD_LOGIC_VECTOR(2 downto 0);
signal sw_s : STD_LOGIC_VECTOR(1 downto 0);
signal clock_divider : UNSIGNED(9 downto 0) := "0000000000";
signal clock_divider2 : UNSIGNED(11 downto 0) := "000000000000";
signal pulse_60hz : STD_LOGIC;
signal snes_clk : STD_LOGIC;
signal old_snes_clk : STD_LOGIC;
signal sys_clk_r : STD_LOGIC;

component SNES_cu is
	Port(	clk: in  STD_LOGIC;
	        snes_clk: in  STD_LOGIC;
	        old_snes_clk: in  STD_LOGIC;
            reset_n : in  STD_LOGIC;
            cw: out STD_LOGIC_VECTOR(2 downto 0);
            sw: in STD_LOGIC_VECTOR(1 downto 0));
end component;

component SNES_dp is
    Port(
	clk : in  STD_LOGIC;
    snes_clk: in  STD_LOGIC;
    old_snes_clk: in  STD_LOGIC;
	pulse : in STD_LOGIC;
	reset_n : in  STD_LOGIC;
	
	-- Serial Interface
    snes_data : in STD_LOGIC;
    snes_sclk : out STD_LOGIC;
        
    -- Parallel Interface
    button_double : out STD_LOGIC_VECTOR(15 downto 0);
	
	cw: in STD_LOGIC_VECTOR(2 downto 0);
	sw: out STD_LOGIC_VECTOR(1 downto 0));
end component;

begin
    -- Clock Divider Process 1
    process(clk)
        begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                clock_divider <= to_unsigned(0, 10);
            elsif (clock_divider >= to_unsigned(599, 10)) then
                clock_divider <= to_unsigned(0, 10);
            else
                clock_divider <= clock_divider + 1;
            end if;
        end if;
    end process;
    
     -- Clock Divider Process 2
    process(snes_clk)
        begin
        if (rising_edge(snes_clk)) then
            if (old_snes_clk = '0' and snes_clk = '1') then
                if (reset_n = '0') then
                    clock_divider2 <= to_unsigned(0, 12);
                elsif (clock_divider2 >= to_unsigned(2777, 12)) then
                    clock_divider2 <= to_unsigned(0, 12);
                else
                    clock_divider2 <= clock_divider2 + 1;
                end if;
            end if;
        end if;
    end process;
    
   process(clk)
       begin
       if (rising_edge(clk)) then
           old_snes_clk <= snes_clk;
       end if;
   end process;

	control_unit: SNES_cu port map(
        clk => clk,
        snes_clk => snes_clk,
        old_snes_clk => old_snes_clk,
        reset_n => reset_n,
        
        cw => cw_s,
        sw => sw_s);
        
    datapath: SNES_dp port map(
        clk => clk,
        snes_clk => snes_clk,
        old_snes_clk => old_snes_clk,
        pulse => pulse_60hz,
        reset_n => reset_n,
        
        snes_data => snes_data,
        snes_sclk => snes_sclk,

        button_double => button_double,
    
        cw => cw_s,
        sw => sw_s);

snes_clk <= '1' when (clock_divider > to_unsigned(299, 10)) else '0';
pulse_60hz <= '1' when (clock_divider2 >= to_unsigned(2776, 12)) else '0';

snes_latch <= pulse_60hz;

end structure;