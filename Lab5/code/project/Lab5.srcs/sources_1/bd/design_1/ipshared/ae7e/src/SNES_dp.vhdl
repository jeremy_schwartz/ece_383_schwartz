--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 22 April 2018
-- Course: ECE 383
-- File: SPI_dp.vhdl
-- Project: Lab5
--
-- Purp: This file implements the datapath for lab 5 SNES.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SNES_dp is
    Port(
	clk : in  STD_LOGIC;
    snes_clk: in  STD_LOGIC;
    old_snes_clk: in  STD_LOGIC;
	pulse : in STD_LOGIC;
	reset_n : in  STD_LOGIC;
	
	-- Serial Interface
    snes_data : in STD_LOGIC;
    snes_sclk : out STD_LOGIC;
        
    -- Parallel Interface
    button_double : out STD_LOGIC_VECTOR(15 downto 0);
	
	cw: in STD_LOGIC_VECTOR(2 downto 0);
	sw: out STD_LOGIC_VECTOR(1 downto 0));
end SNES_dp;

architecture structure of SNES_dp is

signal bit_count : UNSIGNED(4 downto 0) := "00000";
signal packet_in_process : STD_LOGIC_VECTOR(15 downto 0) := x"0000";

begin

    -- Counter Process
    process(clk)
        begin
        if (rising_edge(clk)) then
            if (old_snes_clk = '0' and snes_clk = '1') then  
                if (reset_n = '0') then
                    bit_count <= "00000";
                elsif (cw(1) = '1') then
                    bit_count <= "00000";
                elsif (cw(0) = '1') then
                    bit_count <= bit_count + 1;
                end if;
            end if;
        end if;
    end process;
    
    -- Input SR Process
    process(clk)
        begin
        if (rising_edge(clk)) then
            if (old_snes_clk = '0' and snes_clk = '1') then
                if (reset_n = '0') then
                    packet_in_process <= x"0000";
                elsif (cw(0) = '1') then
                    packet_in_process <= packet_in_process(14 downto 0) & snes_data;
                elsif (cw(1) = '1') then
                    button_double <= packet_in_process;
                end if;
            end if;
        end if;
    end process;
    
    sw(0) <= pulse;
    sw(1) <= '1' when bit_count > "01111" else '0';

    snes_sclk <= cw(2);
	
end structure;
