--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 22 Apr 2018
-- Course: ECE 383
-- File: SPI_top.vhdl
-- Project: Lab5
--
-- Purp: This file implements the top level for the Lab5 SPI interface.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity SPI_top is
    Port(
	-- Basic Signals
	clk : in  STD_LOGIC;
	slow_clk : in STD_LOGIC;
	reset_n : in  STD_LOGIC;
	
	-- Serial Interface
	miso : out STD_LOGIC;
	mosi : in STD_LOGIC;
	sclk : in STD_LOGIC;
	frame_end : in STD_LOGIC;
	slave_ready : out STD_LOGIC;

	waiting_byte : out STD_LOGIC;
		
	-- Parallel Interface
    new_byte : in STD_LOGIC;
    ready : out STD_LOGIC;
	send_byte : in STD_LOGIC_VECTOR(7 downto 0);
	receive_byte : out STD_LOGIC_VECTOR(7 downto 0));
end SPI_top;

architecture structure of SPI_top is

    signal cw_s : STD_LOGIC_VECTOR(6 downto 0);
    signal sw_s : STD_LOGIC_VECTOR(3 downto 0);

    component SPI_cu is
        Port(
        clk: in  STD_LOGIC;
        reset_n : in  STD_LOGIC;
        cw: out STD_LOGIC_VECTOR(6 downto 0);
        sw: in STD_LOGIC_VECTOR(3 downto 0));
    end component;
    
    component SPI_dp is
        Port(
        clk : in  STD_LOGIC;
        fast_clk : in STD_LOGIC;
        reset_n : in  STD_LOGIC;
        
        sclk : in  STD_LOGIC;
        miso : out  STD_LOGIC;
        mosi : in  STD_LOGIC;
        slave_ready : out  STD_LOGIC;
        frame_end : in  STD_LOGIC;
        
        receive_byte : out  STD_LOGIC_VECTOR(7 downto 0);
        send_byte : in  STD_LOGIC_VECTOR(7 downto 0);
        
        ready : out  STD_LOGIC;
        new_byte : in  STD_LOGIC;
        
        waiting_byte : out  STD_LOGIC;
        
        cw: in STD_LOGIC_VECTOR(6 downto 0);
        sw: out STD_LOGIC_VECTOR(3 downto 0));
    end component;

begin

	control_unit: SPI_cu port map(
        clk => slow_clk,
        reset_n => reset_n,
        
        cw => cw_s,
        sw => sw_s);
        
    datapath: SPI_dp port map(
        clk => slow_clk,
        fast_clk => clk,
        reset_n => reset_n,
        
        sclk => sclk,
        miso => miso,
        mosi => mosi,
        slave_ready => slave_ready,
        frame_end => frame_end,

        waiting_byte => waiting_byte,

        ready => ready,
        new_byte => new_byte,
        send_byte => send_byte,
        receive_byte => receive_byte,
    
        cw => cw_s,
        sw => sw_s);

end structure;
