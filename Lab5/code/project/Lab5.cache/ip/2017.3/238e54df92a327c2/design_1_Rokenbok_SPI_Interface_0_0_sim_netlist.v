// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.3 (win64) Build 2018833 Wed Oct  4 19:58:22 MDT 2017
// Date        : Wed Apr 25 09:05:28 2018
// Host        : C19JMSCHWARTZ running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_Rokenbok_SPI_Interface_0_0_sim_netlist.v
// Design      : design_1_Rokenbok_SPI_Interface_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tsbg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Rokenbok_SPI_Interface_v1_0
   (ready,
    snes_sclk,
    S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rvalid,
    waiting_byte,
    slave_ready,
    miso,
    waiting_byte_cpu,
    s00_axi_rdata,
    snes_latch,
    s00_axi_bvalid,
    reset_n,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_arvalid,
    sclk,
    frame_end,
    clk,
    s00_axi_awaddr,
    s00_axi_aclk,
    s00_axi_wdata,
    mosi,
    snes_data,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output ready;
  output snes_sclk;
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output s00_axi_rvalid;
  output waiting_byte;
  output slave_ready;
  output miso;
  output waiting_byte_cpu;
  output [31:0]s00_axi_rdata;
  output snes_latch;
  output s00_axi_bvalid;
  input reset_n;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_arvalid;
  input sclk;
  input frame_end;
  input clk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_aclk;
  input [31:0]s00_axi_wdata;
  input mosi;
  input snes_data;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire clk;
  wire frame_end;
  wire miso;
  wire mosi;
  wire ready;
  wire reset_n;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire sclk;
  wire slave_ready;
  wire snes_data;
  wire snes_latch;
  wire snes_sclk;
  wire waiting_byte;
  wire waiting_byte_cpu;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Rokenbok_SPI_Interface_v1_0_S00_AXI Rokenbok_SPI_Interface_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .clk(clk),
        .frame_end(frame_end),
        .miso(miso),
        .mosi(mosi),
        .ready(ready),
        .reset_n(reset_n),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .sclk(sclk),
        .slave_ready(slave_ready),
        .snes_data(snes_data),
        .snes_latch(snes_latch),
        .snes_sclk(snes_sclk),
        .waiting_byte(waiting_byte),
        .waiting_byte_cpu(waiting_byte_cpu));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Rokenbok_SPI_Interface_v1_0_S00_AXI
   (ready,
    snes_sclk,
    S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rvalid,
    waiting_byte,
    slave_ready,
    miso,
    waiting_byte_cpu,
    s00_axi_rdata,
    snes_latch,
    s00_axi_bvalid,
    reset_n,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_arvalid,
    sclk,
    frame_end,
    clk,
    s00_axi_awaddr,
    s00_axi_aclk,
    s00_axi_wdata,
    mosi,
    snes_data,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output ready;
  output snes_sclk;
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output s00_axi_rvalid;
  output waiting_byte;
  output slave_ready;
  output miso;
  output waiting_byte_cpu;
  output [31:0]s00_axi_rdata;
  output snes_latch;
  output s00_axi_bvalid;
  input reset_n;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_arvalid;
  input sclk;
  input frame_end;
  input clk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_aclk;
  input [31:0]s00_axi_wdata;
  input mosi;
  input snes_data;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire \axi_araddr[4]_i_1_n_0 ;
  wire axi_arready_i_1_n_0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire \axi_awaddr[4]_i_1_n_0 ;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_1_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready_i_1_n_0;
  wire clk;
  wire \clock_div_reg_n_0_[0] ;
  wire \clock_div_reg_n_0_[1] ;
  wire \clock_div_reg_n_0_[2] ;
  wire \clock_div_reg_n_0_[3] ;
  wire frame_end;
  wire miso;
  wire mosi;
  wire [2:0]p_0_in;
  wire [4:0]plusOp;
  wire ready;
  wire [7:0]receive_byte;
  wire [31:0]reg_data_out;
  wire reset_n;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire sclk;
  wire [2:0]sel0;
  wire slave_ready;
  wire slow_clk;
  wire [31:0]slv_reg0;
  wire \slv_reg0[15]_i_1_n_0 ;
  wire \slv_reg0[23]_i_1_n_0 ;
  wire \slv_reg0[31]_i_1_n_0 ;
  wire \slv_reg0[7]_i_1_n_0 ;
  wire [31:1]slv_reg2;
  wire \slv_reg2[0]_i_1_n_0 ;
  wire \slv_reg2[0]_i_2_n_0 ;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire [31:0]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [31:0]slv_reg5;
  wire \slv_reg5[15]_i_1_n_0 ;
  wire \slv_reg5[23]_i_1_n_0 ;
  wire \slv_reg5[31]_i_1_n_0 ;
  wire \slv_reg5[7]_i_1_n_0 ;
  wire [31:0]slv_reg6;
  wire \slv_reg6[15]_i_1_n_0 ;
  wire \slv_reg6[23]_i_1_n_0 ;
  wire \slv_reg6[31]_i_1_n_0 ;
  wire \slv_reg6[7]_i_1_n_0 ;
  wire [31:0]slv_reg7;
  wire \slv_reg7[15]_i_1_n_0 ;
  wire \slv_reg7[23]_i_1_n_0 ;
  wire \slv_reg7[31]_i_1_n_0 ;
  wire \slv_reg7[7]_i_1_n_0 ;
  wire slv_reg_wren__2;
  wire snes_data;
  wire snes_latch;
  wire snes_sclk;
  wire waiting_byte;
  wire waiting_byte_cpu;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SNES_top SNES_interface
       (.D(reg_data_out[15:0]),
        .Q({slv_reg2[15:1],waiting_byte_cpu}),
        .clk(clk),
        .\receive_byte_reg[7] (receive_byte),
        .reset_n(reset_n),
        .sel0(sel0),
        .\slv_reg0_reg[15] (slv_reg0[15:0]),
        .\slv_reg7_reg[0] (\axi_rdata[0]_i_3_n_0 ),
        .\slv_reg7_reg[10] (\axi_rdata[10]_i_3_n_0 ),
        .\slv_reg7_reg[11] (\axi_rdata[11]_i_3_n_0 ),
        .\slv_reg7_reg[12] (\axi_rdata[12]_i_3_n_0 ),
        .\slv_reg7_reg[13] (\axi_rdata[13]_i_3_n_0 ),
        .\slv_reg7_reg[14] (\axi_rdata[14]_i_3_n_0 ),
        .\slv_reg7_reg[15] (\axi_rdata[15]_i_3_n_0 ),
        .\slv_reg7_reg[1] (\axi_rdata[1]_i_3_n_0 ),
        .\slv_reg7_reg[2] (\axi_rdata[2]_i_3_n_0 ),
        .\slv_reg7_reg[3] (\axi_rdata[3]_i_3_n_0 ),
        .\slv_reg7_reg[4] (\axi_rdata[4]_i_3_n_0 ),
        .\slv_reg7_reg[5] (\axi_rdata[5]_i_3_n_0 ),
        .\slv_reg7_reg[6] (\axi_rdata[6]_i_3_n_0 ),
        .\slv_reg7_reg[7] (\axi_rdata[7]_i_3_n_0 ),
        .\slv_reg7_reg[8] (\axi_rdata[8]_i_3_n_0 ),
        .\slv_reg7_reg[9] (\axi_rdata[9]_i_3_n_0 ),
        .snes_data(snes_data),
        .snes_latch(snes_latch),
        .snes_sclk(snes_sclk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SPI_top SPI_interface
       (.Q(slow_clk),
        .\axi_rdata_reg[7] (receive_byte),
        .clk(clk),
        .frame_end(frame_end),
        .miso(miso),
        .mosi(mosi),
        .ready(ready),
        .reset_n(reset_n),
        .sclk(sclk),
        .slave_ready(slave_ready),
        .\slv_reg0_reg[7] (slv_reg0[7:0]),
        .waiting_byte(waiting_byte),
        .waiting_byte_cpu(waiting_byte_cpu));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[0]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[1]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[4]_i_1 
       (.I0(s00_axi_araddr[2]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[2]),
        .O(\axi_araddr[4]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(sel0[0]),
        .S(\slv_reg2[0]_i_1_n_0 ));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(sel0[1]),
        .S(\slv_reg2[0]_i_1_n_0 ));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[4]_i_1_n_0 ),
        .Q(sel0[2]),
        .S(\slv_reg2[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready_i_1_n_0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready_i_1_n_0),
        .Q(S_AXI_ARREADY),
        .R(\slv_reg2[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[4]_i_1 
       (.I0(s00_axi_awaddr[2]),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_0_in[2]),
        .O(\axi_awaddr[4]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[4]_i_1_n_0 ),
        .Q(p_0_in[2]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_AWREADY),
        .O(axi_awready_i_1_n_0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready_i_1_n_0),
        .Q(S_AXI_AWREADY),
        .R(\slv_reg2[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(S_AXI_WREADY),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_AWREADY),
        .I3(s00_axi_awvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(\slv_reg2[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_3 
       (.I0(slv_reg7[0]),
        .I1(slv_reg6[0]),
        .I2(sel0[1]),
        .I3(slv_reg5[0]),
        .I4(sel0[0]),
        .I5(slv_reg4[0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_3 
       (.I0(slv_reg7[10]),
        .I1(slv_reg6[10]),
        .I2(sel0[1]),
        .I3(slv_reg5[10]),
        .I4(sel0[0]),
        .I5(slv_reg4[10]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_3 
       (.I0(slv_reg7[11]),
        .I1(slv_reg6[11]),
        .I2(sel0[1]),
        .I3(slv_reg5[11]),
        .I4(sel0[0]),
        .I5(slv_reg4[11]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_3 
       (.I0(slv_reg7[12]),
        .I1(slv_reg6[12]),
        .I2(sel0[1]),
        .I3(slv_reg5[12]),
        .I4(sel0[0]),
        .I5(slv_reg4[12]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_3 
       (.I0(slv_reg7[13]),
        .I1(slv_reg6[13]),
        .I2(sel0[1]),
        .I3(slv_reg5[13]),
        .I4(sel0[0]),
        .I5(slv_reg4[13]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_3 
       (.I0(slv_reg7[14]),
        .I1(slv_reg6[14]),
        .I2(sel0[1]),
        .I3(slv_reg5[14]),
        .I4(sel0[0]),
        .I5(slv_reg4[14]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_3 
       (.I0(slv_reg7[15]),
        .I1(slv_reg6[15]),
        .I2(sel0[1]),
        .I3(slv_reg5[15]),
        .I4(sel0[0]),
        .I5(slv_reg4[15]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[16]),
        .I3(sel0[1]),
        .I4(slv_reg0[16]),
        .I5(sel0[0]),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(slv_reg7[16]),
        .I1(slv_reg6[16]),
        .I2(sel0[1]),
        .I3(slv_reg5[16]),
        .I4(sel0[0]),
        .I5(slv_reg4[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[17]),
        .I3(sel0[1]),
        .I4(slv_reg0[17]),
        .I5(sel0[0]),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(slv_reg7[17]),
        .I1(slv_reg6[17]),
        .I2(sel0[1]),
        .I3(slv_reg5[17]),
        .I4(sel0[0]),
        .I5(slv_reg4[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[18]),
        .I3(sel0[1]),
        .I4(slv_reg0[18]),
        .I5(sel0[0]),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_2 
       (.I0(slv_reg7[18]),
        .I1(slv_reg6[18]),
        .I2(sel0[1]),
        .I3(slv_reg5[18]),
        .I4(sel0[0]),
        .I5(slv_reg4[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[19]),
        .I3(sel0[1]),
        .I4(slv_reg0[19]),
        .I5(sel0[0]),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_2 
       (.I0(slv_reg7[19]),
        .I1(slv_reg6[19]),
        .I2(sel0[1]),
        .I3(slv_reg5[19]),
        .I4(sel0[0]),
        .I5(slv_reg4[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_3 
       (.I0(slv_reg7[1]),
        .I1(slv_reg6[1]),
        .I2(sel0[1]),
        .I3(slv_reg5[1]),
        .I4(sel0[0]),
        .I5(slv_reg4[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[20]),
        .I3(sel0[1]),
        .I4(slv_reg0[20]),
        .I5(sel0[0]),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_2 
       (.I0(slv_reg7[20]),
        .I1(slv_reg6[20]),
        .I2(sel0[1]),
        .I3(slv_reg5[20]),
        .I4(sel0[0]),
        .I5(slv_reg4[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[21]),
        .I3(sel0[1]),
        .I4(slv_reg0[21]),
        .I5(sel0[0]),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(slv_reg7[21]),
        .I1(slv_reg6[21]),
        .I2(sel0[1]),
        .I3(slv_reg5[21]),
        .I4(sel0[0]),
        .I5(slv_reg4[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[22]),
        .I3(sel0[1]),
        .I4(slv_reg0[22]),
        .I5(sel0[0]),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_2 
       (.I0(slv_reg7[22]),
        .I1(slv_reg6[22]),
        .I2(sel0[1]),
        .I3(slv_reg5[22]),
        .I4(sel0[0]),
        .I5(slv_reg4[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[23]),
        .I3(sel0[1]),
        .I4(slv_reg0[23]),
        .I5(sel0[0]),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_2 
       (.I0(slv_reg7[23]),
        .I1(slv_reg6[23]),
        .I2(sel0[1]),
        .I3(slv_reg5[23]),
        .I4(sel0[0]),
        .I5(slv_reg4[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[24]),
        .I3(sel0[1]),
        .I4(slv_reg0[24]),
        .I5(sel0[0]),
        .O(reg_data_out[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(slv_reg7[24]),
        .I1(slv_reg6[24]),
        .I2(sel0[1]),
        .I3(slv_reg5[24]),
        .I4(sel0[0]),
        .I5(slv_reg4[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[25]),
        .I3(sel0[1]),
        .I4(slv_reg0[25]),
        .I5(sel0[0]),
        .O(reg_data_out[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(slv_reg7[25]),
        .I1(slv_reg6[25]),
        .I2(sel0[1]),
        .I3(slv_reg5[25]),
        .I4(sel0[0]),
        .I5(slv_reg4[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[26]),
        .I3(sel0[1]),
        .I4(slv_reg0[26]),
        .I5(sel0[0]),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(slv_reg7[26]),
        .I1(slv_reg6[26]),
        .I2(sel0[1]),
        .I3(slv_reg5[26]),
        .I4(sel0[0]),
        .I5(slv_reg4[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[27]),
        .I3(sel0[1]),
        .I4(slv_reg0[27]),
        .I5(sel0[0]),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(slv_reg7[27]),
        .I1(slv_reg6[27]),
        .I2(sel0[1]),
        .I3(slv_reg5[27]),
        .I4(sel0[0]),
        .I5(slv_reg4[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[28]),
        .I3(sel0[1]),
        .I4(slv_reg0[28]),
        .I5(sel0[0]),
        .O(reg_data_out[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(slv_reg7[28]),
        .I1(slv_reg6[28]),
        .I2(sel0[1]),
        .I3(slv_reg5[28]),
        .I4(sel0[0]),
        .I5(slv_reg4[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[29]),
        .I3(sel0[1]),
        .I4(slv_reg0[29]),
        .I5(sel0[0]),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(slv_reg7[29]),
        .I1(slv_reg6[29]),
        .I2(sel0[1]),
        .I3(slv_reg5[29]),
        .I4(sel0[0]),
        .I5(slv_reg4[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_3 
       (.I0(slv_reg7[2]),
        .I1(slv_reg6[2]),
        .I2(sel0[1]),
        .I3(slv_reg5[2]),
        .I4(sel0[0]),
        .I5(slv_reg4[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[30]),
        .I3(sel0[1]),
        .I4(slv_reg0[30]),
        .I5(sel0[0]),
        .O(reg_data_out[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(slv_reg7[30]),
        .I1(slv_reg6[30]),
        .I2(sel0[1]),
        .I3(slv_reg5[30]),
        .I4(sel0[0]),
        .I5(slv_reg4[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(\axi_rdata[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8BBB888)) 
    \axi_rdata[31]_i_2 
       (.I0(\axi_rdata[31]_i_3_n_0 ),
        .I1(sel0[2]),
        .I2(slv_reg2[31]),
        .I3(sel0[1]),
        .I4(slv_reg0[31]),
        .I5(sel0[0]),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_3 
       (.I0(slv_reg7[31]),
        .I1(slv_reg6[31]),
        .I2(sel0[1]),
        .I3(slv_reg5[31]),
        .I4(sel0[0]),
        .I5(slv_reg4[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_3 
       (.I0(slv_reg7[3]),
        .I1(slv_reg6[3]),
        .I2(sel0[1]),
        .I3(slv_reg5[3]),
        .I4(sel0[0]),
        .I5(slv_reg4[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_3 
       (.I0(slv_reg7[4]),
        .I1(slv_reg6[4]),
        .I2(sel0[1]),
        .I3(slv_reg5[4]),
        .I4(sel0[0]),
        .I5(slv_reg4[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_3 
       (.I0(slv_reg7[5]),
        .I1(slv_reg6[5]),
        .I2(sel0[1]),
        .I3(slv_reg5[5]),
        .I4(sel0[0]),
        .I5(slv_reg4[5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_3 
       (.I0(slv_reg7[6]),
        .I1(slv_reg6[6]),
        .I2(sel0[1]),
        .I3(slv_reg5[6]),
        .I4(sel0[0]),
        .I5(slv_reg4[6]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_3 
       (.I0(slv_reg7[7]),
        .I1(slv_reg6[7]),
        .I2(sel0[1]),
        .I3(slv_reg5[7]),
        .I4(sel0[0]),
        .I5(slv_reg4[7]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_3 
       (.I0(slv_reg7[8]),
        .I1(slv_reg6[8]),
        .I2(sel0[1]),
        .I3(slv_reg5[8]),
        .I4(sel0[0]),
        .I5(slv_reg4[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_3 
       (.I0(slv_reg7[9]),
        .I1(slv_reg6[9]),
        .I2(sel0[1]),
        .I3(slv_reg5[9]),
        .I4(sel0[0]),
        .I5(slv_reg4[9]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(\slv_reg2[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_WREADY),
        .O(axi_wready_i_1_n_0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready_i_1_n_0),
        .Q(S_AXI_WREADY),
        .R(\slv_reg2[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \clock_div[0]_i_1 
       (.I0(\clock_div_reg_n_0_[0] ),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \clock_div[1]_i_1 
       (.I0(\clock_div_reg_n_0_[0] ),
        .I1(\clock_div_reg_n_0_[1] ),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \clock_div[2]_i_1 
       (.I0(\clock_div_reg_n_0_[0] ),
        .I1(\clock_div_reg_n_0_[1] ),
        .I2(\clock_div_reg_n_0_[2] ),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \clock_div[3]_i_1 
       (.I0(\clock_div_reg_n_0_[1] ),
        .I1(\clock_div_reg_n_0_[0] ),
        .I2(\clock_div_reg_n_0_[2] ),
        .I3(\clock_div_reg_n_0_[3] ),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \clock_div[4]_i_1 
       (.I0(\clock_div_reg_n_0_[2] ),
        .I1(\clock_div_reg_n_0_[0] ),
        .I2(\clock_div_reg_n_0_[1] ),
        .I3(\clock_div_reg_n_0_[3] ),
        .I4(slow_clk),
        .O(plusOp[4]));
  FDRE \clock_div_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(\clock_div_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \clock_div_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(\clock_div_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \clock_div_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(\clock_div_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \clock_div_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(\clock_div_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \clock_div_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(slow_clk),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg0[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg0[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg0[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg0[7]_i_1_n_0 ));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg0[0]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg0[10]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg0[11]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg0[12]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg0[13]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg0[14]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg0[15]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg0[16]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg0[17]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg0[18]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg0[19]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg0[1]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg0[20]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg0[21]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg0[22]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg0[23]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg0[24]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg0[25]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg0[26]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg0[27]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg0[28]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg0[29]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg0[2]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg0[30]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg0[31]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg0[3]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg0[4]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg0[5]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg0[6]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg0[7]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg0[8]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg0[9]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \slv_reg2[0]_i_1 
       (.I0(s00_axi_aresetn),
        .O(\slv_reg2[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[0]_i_2 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg2[0]_i_3 
       (.I0(S_AXI_WREADY),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_AWREADY),
        .I3(s00_axi_awvalid),
        .O(slv_reg_wren__2));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[0]_i_2_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(waiting_byte_cpu),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[0]_i_2_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[0]_i_2_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[0]_i_2_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[0]_i_2_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[0]_i_2_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[0]_i_2_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[0]_i_2_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg4[0]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4[10]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4[11]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4[12]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4[13]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4[14]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4[15]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4[16]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4[17]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4[18]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4[19]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg4[1]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4[20]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4[21]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4[22]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4[23]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg4[2]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg4[3]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg4[4]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg4[5]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4[8]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4[9]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .O(\slv_reg5[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .O(\slv_reg5[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .O(\slv_reg5[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .O(\slv_reg5[7]_i_1_n_0 ));
  FDRE \slv_reg5_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg5[0]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg5[10]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg5[11]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg5[12]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg5[13]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg5[14]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg5[15]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg5[16]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg5[17]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg5[18]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg5[19]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg5[1]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg5[20]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg5[21]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg5[22]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg5[23]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg5[24]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg5[25]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg5[26]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg5[27]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg5[28]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg5[29]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg5[2]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg5[30]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg5[31]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg5[3]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg5[4]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg5[5]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg5[6]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg5[7]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg5[8]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg5_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg5[9]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg6[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[2]),
        .O(\slv_reg6[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg6[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[2]),
        .O(\slv_reg6[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg6[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[2]),
        .O(\slv_reg6[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg6[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[2]),
        .O(\slv_reg6[7]_i_1_n_0 ));
  FDRE \slv_reg6_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg6[0]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg6[10]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg6[11]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg6[12]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg6[13]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg6[14]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg6[15]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg6[16]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg6[17]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg6[18]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg6[19]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg6[1]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg6[20]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg6[21]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg6[22]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg6[23]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg6[24]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg6[25]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg6[26]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg6[27]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg6[28]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg6[29]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg6[2]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg6[30]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg6[31]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg6[3]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg6[4]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg6[5]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg6[6]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg6[7]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg6[8]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg6_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg6[9]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg7[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg7[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg7[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg7[7]_i_1_n_0 ));
  FDRE \slv_reg7_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg7[0]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg7[10]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg7[11]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg7[12]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg7[13]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg7[14]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg7[15]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg7[16]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg7[17]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg7[18]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg7[19]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg7[1]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg7[20]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg7[21]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg7[22]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg7[23]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg7[24]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg7[25]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg7[26]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg7[27]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg7[28]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg7[29]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg7[2]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg7[30]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg7[31]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg7[3]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg7[4]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg7[5]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg7[6]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg7[7]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg7[8]),
        .R(\slv_reg2[0]_i_1_n_0 ));
  FDRE \slv_reg7_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg7[9]),
        .R(\slv_reg2[0]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SNES_cu
   (snes_sclk,
    E,
    \packet_in_process_reg[15] ,
    SR,
    \FSM_sequential_state_reg[0]_0 ,
    \clock_divider_reg[7] ,
    reset_n,
    clock_divider2_reg,
    clk,
    Q);
  output snes_sclk;
  output [0:0]E;
  output [0:0]\packet_in_process_reg[15] ;
  output [0:0]SR;
  output \FSM_sequential_state_reg[0]_0 ;
  input \clock_divider_reg[7] ;
  input reset_n;
  input [8:0]clock_divider2_reg;
  input clk;
  input [0:0]Q;

  wire \/FSM_sequential_state[1]_i_2_n_0 ;
  wire \/FSM_sequential_state[2]_i_4_n_0 ;
  wire [0:0]E;
  wire \FSM_sequential_state[0]_i_1__0_n_0 ;
  wire \FSM_sequential_state[0]_i_2__0_n_0 ;
  wire \FSM_sequential_state[1]_i_1__0_n_0 ;
  wire \FSM_sequential_state[2]_i_1__0_n_0 ;
  wire \FSM_sequential_state[2]_i_2_n_0 ;
  wire \FSM_sequential_state[2]_i_3_n_0 ;
  wire \FSM_sequential_state[2]_i_5_n_0 ;
  wire \FSM_sequential_state_reg[0]_0 ;
  wire [0:0]Q;
  wire [0:0]SR;
  wire clk;
  wire [8:0]clock_divider2_reg;
  wire \clock_divider_reg[7] ;
  wire [0:0]\packet_in_process_reg[15] ;
  wire reset_n;
  wire snes_sclk;
  (* RTL_KEEP = "yes" *) wire [2:0]state;

  LUT4 #(
    .INIT(16'h083C)) 
    \/FSM_sequential_state[1]_i_2 
       (.I0(Q),
        .I1(state[0]),
        .I2(state[1]),
        .I3(state[2]),
        .O(\/FSM_sequential_state[1]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h38)) 
    \/FSM_sequential_state[2]_i_4 
       (.I0(state[0]),
        .I1(state[1]),
        .I2(state[2]),
        .O(\/FSM_sequential_state[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0F8E0000AAAAAAAA)) 
    \FSM_sequential_state[0]_i_1__0 
       (.I0(state[0]),
        .I1(\FSM_sequential_state[0]_i_2__0_n_0 ),
        .I2(state[0]),
        .I3(\FSM_sequential_state[2]_i_3_n_0 ),
        .I4(reset_n),
        .I5(\clock_divider_reg[7] ),
        .O(\FSM_sequential_state[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_state[0]_i_2__0 
       (.I0(state[2]),
        .I1(state[1]),
        .O(\FSM_sequential_state[0]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'hFE020000AAAAAAAA)) 
    \FSM_sequential_state[1]_i_1__0 
       (.I0(state[1]),
        .I1(\FSM_sequential_state[2]_i_2_n_0 ),
        .I2(\FSM_sequential_state[2]_i_3_n_0 ),
        .I3(\/FSM_sequential_state[1]_i_2_n_0 ),
        .I4(reset_n),
        .I5(\clock_divider_reg[7] ),
        .O(\FSM_sequential_state[1]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFE020000AAAAAAAA)) 
    \FSM_sequential_state[2]_i_1__0 
       (.I0(state[2]),
        .I1(\FSM_sequential_state[2]_i_2_n_0 ),
        .I2(\FSM_sequential_state[2]_i_3_n_0 ),
        .I3(\/FSM_sequential_state[2]_i_4_n_0 ),
        .I4(reset_n),
        .I5(\clock_divider_reg[7] ),
        .O(\FSM_sequential_state[2]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h59)) 
    \FSM_sequential_state[2]_i_2 
       (.I0(state[0]),
        .I1(state[1]),
        .I2(state[2]),
        .O(\FSM_sequential_state[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h55555555666A6A6A)) 
    \FSM_sequential_state[2]_i_3 
       (.I0(state[1]),
        .I1(clock_divider2_reg[8]),
        .I2(clock_divider2_reg[7]),
        .I3(\FSM_sequential_state[2]_i_5_n_0 ),
        .I4(\FSM_sequential_state_reg[0]_0 ),
        .I5(state[2]),
        .O(\FSM_sequential_state[2]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hA8)) 
    \FSM_sequential_state[2]_i_5 
       (.I0(clock_divider2_reg[6]),
        .I1(clock_divider2_reg[5]),
        .I2(clock_divider2_reg[3]),
        .O(\FSM_sequential_state[2]_i_5_n_0 ));
  (* FSM_ENCODED_STATES = "reset:000,wait_pulse_high:001,wait_pulse_low:010,wait_one:011,sclk_low:100,sclk_high:101,done:110" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_sequential_state[0]_i_1__0_n_0 ),
        .Q(state[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "reset:000,wait_pulse_high:001,wait_pulse_low:010,wait_one:011,sclk_low:100,sclk_high:101,done:110" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1]_i_1__0_n_0 ),
        .Q(state[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "reset:000,wait_pulse_high:001,wait_pulse_low:010,wait_one:011,sclk_low:100,sclk_high:101,done:110" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_sequential_state[2]_i_1__0_n_0 ),
        .Q(state[2]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h2A222222)) 
    \bit_count[4]_i_1 
       (.I0(\clock_divider_reg[7] ),
        .I1(reset_n),
        .I2(state[0]),
        .I3(state[2]),
        .I4(state[1]),
        .O(SR));
  LUT5 #(
    .INIT(32'h40000000)) 
    \button_double[15]_i_1 
       (.I0(state[0]),
        .I1(state[2]),
        .I2(state[1]),
        .I3(\clock_divider_reg[7] ),
        .I4(reset_n),
        .O(E));
  LUT4 #(
    .INIT(16'h0200)) 
    \packet_in_process[15]_i_2 
       (.I0(\clock_divider_reg[7] ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .O(\packet_in_process_reg[15] ));
  LUT5 #(
    .INIT(32'hFFF8FF00)) 
    snes_latch_INST_0_i_1
       (.I0(clock_divider2_reg[1]),
        .I1(clock_divider2_reg[0]),
        .I2(clock_divider2_reg[2]),
        .I3(clock_divider2_reg[5]),
        .I4(clock_divider2_reg[4]),
        .O(\FSM_sequential_state_reg[0]_0 ));
  LUT3 #(
    .INIT(8'h6F)) 
    snes_sclk_INST_0
       (.I0(state[1]),
        .I1(state[0]),
        .I2(state[2]),
        .O(snes_sclk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SNES_dp
   (\clock_divider2_reg[11] ,
    \clock_divider2_reg[11]_0 ,
    \bit_count_reg[4]_0 ,
    D,
    reset_n,
    Q,
    old_snes_clk,
    sel0,
    \slv_reg7_reg[0] ,
    \slv_reg2_reg[15] ,
    \receive_byte_reg[7] ,
    \slv_reg0_reg[15] ,
    \slv_reg7_reg[1] ,
    \slv_reg7_reg[2] ,
    \slv_reg7_reg[3] ,
    \slv_reg7_reg[4] ,
    \slv_reg7_reg[5] ,
    \slv_reg7_reg[6] ,
    \slv_reg7_reg[7] ,
    \slv_reg7_reg[8] ,
    \slv_reg7_reg[9] ,
    \slv_reg7_reg[10] ,
    \slv_reg7_reg[11] ,
    \slv_reg7_reg[12] ,
    \slv_reg7_reg[13] ,
    \slv_reg7_reg[14] ,
    \slv_reg7_reg[15] ,
    E,
    clk,
    snes_data,
    \FSM_sequential_state_reg[0] ,
    SR);
  output \clock_divider2_reg[11] ;
  output \clock_divider2_reg[11]_0 ;
  output [0:0]\bit_count_reg[4]_0 ;
  output [15:0]D;
  input reset_n;
  input [7:0]Q;
  input old_snes_clk;
  input [2:0]sel0;
  input \slv_reg7_reg[0] ;
  input [15:0]\slv_reg2_reg[15] ;
  input [7:0]\receive_byte_reg[7] ;
  input [15:0]\slv_reg0_reg[15] ;
  input \slv_reg7_reg[1] ;
  input \slv_reg7_reg[2] ;
  input \slv_reg7_reg[3] ;
  input \slv_reg7_reg[4] ;
  input \slv_reg7_reg[5] ;
  input \slv_reg7_reg[6] ;
  input \slv_reg7_reg[7] ;
  input \slv_reg7_reg[8] ;
  input \slv_reg7_reg[9] ;
  input \slv_reg7_reg[10] ;
  input \slv_reg7_reg[11] ;
  input \slv_reg7_reg[12] ;
  input \slv_reg7_reg[13] ;
  input \slv_reg7_reg[14] ;
  input \slv_reg7_reg[15] ;
  input [0:0]E;
  input clk;
  input snes_data;
  input [0:0]\FSM_sequential_state_reg[0] ;
  input [0:0]SR;

  wire [15:0]D;
  wire [0:0]E;
  wire [0:0]\FSM_sequential_state_reg[0] ;
  wire [7:0]Q;
  wire [0:0]SR;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire [0:0]\bit_count_reg[4]_0 ;
  wire \bit_count_reg_n_0_[0] ;
  wire \bit_count_reg_n_0_[1] ;
  wire \bit_count_reg_n_0_[2] ;
  wire \bit_count_reg_n_0_[3] ;
  wire [15:0]button_double;
  wire clk;
  wire \clock_divider2_reg[11] ;
  wire \clock_divider2_reg[11]_0 ;
  wire old_snes_clk;
  wire \packet_in_process[15]_i_1_n_0 ;
  wire \packet_in_process_reg_n_0_[0] ;
  wire \packet_in_process_reg_n_0_[10] ;
  wire \packet_in_process_reg_n_0_[11] ;
  wire \packet_in_process_reg_n_0_[12] ;
  wire \packet_in_process_reg_n_0_[13] ;
  wire \packet_in_process_reg_n_0_[14] ;
  wire \packet_in_process_reg_n_0_[15] ;
  wire \packet_in_process_reg_n_0_[1] ;
  wire \packet_in_process_reg_n_0_[2] ;
  wire \packet_in_process_reg_n_0_[3] ;
  wire \packet_in_process_reg_n_0_[4] ;
  wire \packet_in_process_reg_n_0_[5] ;
  wire \packet_in_process_reg_n_0_[6] ;
  wire \packet_in_process_reg_n_0_[7] ;
  wire \packet_in_process_reg_n_0_[8] ;
  wire \packet_in_process_reg_n_0_[9] ;
  wire [4:0]plusOp__2;
  wire [7:0]\receive_byte_reg[7] ;
  wire reset_n;
  wire [2:0]sel0;
  wire [15:0]\slv_reg0_reg[15] ;
  wire [15:0]\slv_reg2_reg[15] ;
  wire \slv_reg7_reg[0] ;
  wire \slv_reg7_reg[10] ;
  wire \slv_reg7_reg[11] ;
  wire \slv_reg7_reg[12] ;
  wire \slv_reg7_reg[13] ;
  wire \slv_reg7_reg[14] ;
  wire \slv_reg7_reg[15] ;
  wire \slv_reg7_reg[1] ;
  wire \slv_reg7_reg[2] ;
  wire \slv_reg7_reg[3] ;
  wire \slv_reg7_reg[4] ;
  wire \slv_reg7_reg[5] ;
  wire \slv_reg7_reg[6] ;
  wire \slv_reg7_reg[7] ;
  wire \slv_reg7_reg[8] ;
  wire \slv_reg7_reg[9] ;
  wire snes_data;

  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(button_double[0]),
        .I1(\slv_reg2_reg[15] [0]),
        .I2(sel0[1]),
        .I3(\receive_byte_reg[7] [0]),
        .I4(sel0[0]),
        .I5(\slv_reg0_reg[15] [0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[10]_i_2 
       (.I0(button_double[10]),
        .I1(\slv_reg2_reg[15] [10]),
        .I2(sel0[1]),
        .I3(\slv_reg0_reg[15] [10]),
        .I4(sel0[0]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[11]_i_2 
       (.I0(button_double[11]),
        .I1(\slv_reg2_reg[15] [11]),
        .I2(sel0[1]),
        .I3(\slv_reg0_reg[15] [11]),
        .I4(sel0[0]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[12]_i_2 
       (.I0(button_double[12]),
        .I1(\slv_reg2_reg[15] [12]),
        .I2(sel0[1]),
        .I3(\slv_reg0_reg[15] [12]),
        .I4(sel0[0]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[13]_i_2 
       (.I0(button_double[13]),
        .I1(\slv_reg2_reg[15] [13]),
        .I2(sel0[1]),
        .I3(\slv_reg0_reg[15] [13]),
        .I4(sel0[0]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[14]_i_2 
       (.I0(button_double[14]),
        .I1(\slv_reg2_reg[15] [14]),
        .I2(sel0[1]),
        .I3(\slv_reg0_reg[15] [14]),
        .I4(sel0[0]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[15]_i_2 
       (.I0(button_double[15]),
        .I1(\slv_reg2_reg[15] [15]),
        .I2(sel0[1]),
        .I3(\slv_reg0_reg[15] [15]),
        .I4(sel0[0]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_2 
       (.I0(button_double[1]),
        .I1(\slv_reg2_reg[15] [1]),
        .I2(sel0[1]),
        .I3(\receive_byte_reg[7] [1]),
        .I4(sel0[0]),
        .I5(\slv_reg0_reg[15] [1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_2 
       (.I0(button_double[2]),
        .I1(\slv_reg2_reg[15] [2]),
        .I2(sel0[1]),
        .I3(\receive_byte_reg[7] [2]),
        .I4(sel0[0]),
        .I5(\slv_reg0_reg[15] [2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_2 
       (.I0(button_double[3]),
        .I1(\slv_reg2_reg[15] [3]),
        .I2(sel0[1]),
        .I3(\receive_byte_reg[7] [3]),
        .I4(sel0[0]),
        .I5(\slv_reg0_reg[15] [3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(button_double[4]),
        .I1(\slv_reg2_reg[15] [4]),
        .I2(sel0[1]),
        .I3(\receive_byte_reg[7] [4]),
        .I4(sel0[0]),
        .I5(\slv_reg0_reg[15] [4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(button_double[5]),
        .I1(\slv_reg2_reg[15] [5]),
        .I2(sel0[1]),
        .I3(\receive_byte_reg[7] [5]),
        .I4(sel0[0]),
        .I5(\slv_reg0_reg[15] [5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(button_double[6]),
        .I1(\slv_reg2_reg[15] [6]),
        .I2(sel0[1]),
        .I3(\receive_byte_reg[7] [6]),
        .I4(sel0[0]),
        .I5(\slv_reg0_reg[15] [6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_2 
       (.I0(button_double[7]),
        .I1(\slv_reg2_reg[15] [7]),
        .I2(sel0[1]),
        .I3(\receive_byte_reg[7] [7]),
        .I4(sel0[0]),
        .I5(\slv_reg0_reg[15] [7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[8]_i_2 
       (.I0(button_double[8]),
        .I1(\slv_reg2_reg[15] [8]),
        .I2(sel0[1]),
        .I3(\slv_reg0_reg[15] [8]),
        .I4(sel0[0]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[9]_i_2 
       (.I0(button_double[9]),
        .I1(\slv_reg2_reg[15] [9]),
        .I2(sel0[1]),
        .I3(\slv_reg0_reg[15] [9]),
        .I4(sel0[0]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  MUXF7 \axi_rdata_reg[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(\slv_reg7_reg[0] ),
        .O(D[0]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(\slv_reg7_reg[10] ),
        .O(D[10]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(\slv_reg7_reg[11] ),
        .O(D[11]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(\slv_reg7_reg[12] ),
        .O(D[12]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(\slv_reg7_reg[13] ),
        .O(D[13]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(\slv_reg7_reg[14] ),
        .O(D[14]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\slv_reg7_reg[15] ),
        .O(D[15]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(\slv_reg7_reg[1] ),
        .O(D[1]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(\slv_reg7_reg[2] ),
        .O(D[2]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(\slv_reg7_reg[3] ),
        .O(D[3]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(\slv_reg7_reg[4] ),
        .O(D[4]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(\slv_reg7_reg[5] ),
        .O(D[5]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(\slv_reg7_reg[6] ),
        .O(D[6]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(\slv_reg7_reg[7] ),
        .O(D[7]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(\slv_reg7_reg[8] ),
        .O(D[8]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(\slv_reg7_reg[9] ),
        .O(D[9]),
        .S(sel0[2]));
  LUT1 #(
    .INIT(2'h1)) 
    \bit_count[0]_i_1__0 
       (.I0(\bit_count_reg_n_0_[0] ),
        .O(plusOp__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \bit_count[1]_i_1__0 
       (.I0(\bit_count_reg_n_0_[0] ),
        .I1(\bit_count_reg_n_0_[1] ),
        .O(plusOp__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \bit_count[2]_i_1__0 
       (.I0(\bit_count_reg_n_0_[0] ),
        .I1(\bit_count_reg_n_0_[1] ),
        .I2(\bit_count_reg_n_0_[2] ),
        .O(plusOp__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \bit_count[3]_i_1 
       (.I0(\bit_count_reg_n_0_[1] ),
        .I1(\bit_count_reg_n_0_[0] ),
        .I2(\bit_count_reg_n_0_[2] ),
        .I3(\bit_count_reg_n_0_[3] ),
        .O(plusOp__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \bit_count[4]_i_2 
       (.I0(\bit_count_reg_n_0_[2] ),
        .I1(\bit_count_reg_n_0_[0] ),
        .I2(\bit_count_reg_n_0_[1] ),
        .I3(\bit_count_reg_n_0_[3] ),
        .I4(\bit_count_reg[4]_0 ),
        .O(plusOp__2[4]));
  FDRE #(
    .INIT(1'b0)) 
    \bit_count_reg[0] 
       (.C(clk),
        .CE(E),
        .D(plusOp__2[0]),
        .Q(\bit_count_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \bit_count_reg[1] 
       (.C(clk),
        .CE(E),
        .D(plusOp__2[1]),
        .Q(\bit_count_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \bit_count_reg[2] 
       (.C(clk),
        .CE(E),
        .D(plusOp__2[2]),
        .Q(\bit_count_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \bit_count_reg[3] 
       (.C(clk),
        .CE(E),
        .D(plusOp__2[3]),
        .Q(\bit_count_reg_n_0_[3] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \bit_count_reg[4] 
       (.C(clk),
        .CE(E),
        .D(plusOp__2[4]),
        .Q(\bit_count_reg[4]_0 ),
        .R(SR));
  FDRE \button_double_reg[0] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[0] ),
        .Q(button_double[0]),
        .R(1'b0));
  FDRE \button_double_reg[10] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[10] ),
        .Q(button_double[10]),
        .R(1'b0));
  FDRE \button_double_reg[11] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[11] ),
        .Q(button_double[11]),
        .R(1'b0));
  FDRE \button_double_reg[12] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[12] ),
        .Q(button_double[12]),
        .R(1'b0));
  FDRE \button_double_reg[13] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[13] ),
        .Q(button_double[13]),
        .R(1'b0));
  FDRE \button_double_reg[14] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[14] ),
        .Q(button_double[14]),
        .R(1'b0));
  FDRE \button_double_reg[15] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[15] ),
        .Q(button_double[15]),
        .R(1'b0));
  FDRE \button_double_reg[1] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[1] ),
        .Q(button_double[1]),
        .R(1'b0));
  FDRE \button_double_reg[2] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[2] ),
        .Q(button_double[2]),
        .R(1'b0));
  FDRE \button_double_reg[3] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[3] ),
        .Q(button_double[3]),
        .R(1'b0));
  FDRE \button_double_reg[4] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[4] ),
        .Q(button_double[4]),
        .R(1'b0));
  FDRE \button_double_reg[5] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[5] ),
        .Q(button_double[5]),
        .R(1'b0));
  FDRE \button_double_reg[6] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[6] ),
        .Q(button_double[6]),
        .R(1'b0));
  FDRE \button_double_reg[7] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[7] ),
        .Q(button_double[7]),
        .R(1'b0));
  FDRE \button_double_reg[8] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[8] ),
        .Q(button_double[8]),
        .R(1'b0));
  FDRE \button_double_reg[9] 
       (.C(clk),
        .CE(\FSM_sequential_state_reg[0] ),
        .D(\packet_in_process_reg_n_0_[9] ),
        .Q(button_double[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000FFFFFD55)) 
    \clock_divider2[0]_i_2 
       (.I0(\clock_divider2_reg[11]_0 ),
        .I1(Q[5]),
        .I2(Q[4]),
        .I3(Q[6]),
        .I4(Q[7]),
        .I5(old_snes_clk),
        .O(\clock_divider2_reg[11] ));
  LUT5 #(
    .INIT(32'h77777FFF)) 
    old_snes_clk_i_2
       (.I0(Q[3]),
        .I1(Q[6]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(\clock_divider2_reg[11]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \packet_in_process[15]_i_1 
       (.I0(\clock_divider2_reg[11] ),
        .I1(reset_n),
        .O(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[0] 
       (.C(clk),
        .CE(E),
        .D(snes_data),
        .Q(\packet_in_process_reg_n_0_[0] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[10] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[9] ),
        .Q(\packet_in_process_reg_n_0_[10] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[11] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[10] ),
        .Q(\packet_in_process_reg_n_0_[11] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[12] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[11] ),
        .Q(\packet_in_process_reg_n_0_[12] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[13] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[12] ),
        .Q(\packet_in_process_reg_n_0_[13] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[14] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[13] ),
        .Q(\packet_in_process_reg_n_0_[14] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[15] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[14] ),
        .Q(\packet_in_process_reg_n_0_[15] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[1] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[0] ),
        .Q(\packet_in_process_reg_n_0_[1] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[2] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[1] ),
        .Q(\packet_in_process_reg_n_0_[2] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[3] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[2] ),
        .Q(\packet_in_process_reg_n_0_[3] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[4] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[3] ),
        .Q(\packet_in_process_reg_n_0_[4] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[5] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[4] ),
        .Q(\packet_in_process_reg_n_0_[5] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[6] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[5] ),
        .Q(\packet_in_process_reg_n_0_[6] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[7] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[6] ),
        .Q(\packet_in_process_reg_n_0_[7] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[8] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[7] ),
        .Q(\packet_in_process_reg_n_0_[8] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_in_process_reg[9] 
       (.C(clk),
        .CE(E),
        .D(\packet_in_process_reg_n_0_[8] ),
        .Q(\packet_in_process_reg_n_0_[9] ),
        .R(\packet_in_process[15]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SNES_top
   (snes_sclk,
    snes_latch,
    D,
    clk,
    reset_n,
    sel0,
    \slv_reg7_reg[0] ,
    Q,
    \receive_byte_reg[7] ,
    \slv_reg0_reg[15] ,
    \slv_reg7_reg[1] ,
    \slv_reg7_reg[2] ,
    \slv_reg7_reg[3] ,
    \slv_reg7_reg[4] ,
    \slv_reg7_reg[5] ,
    \slv_reg7_reg[6] ,
    \slv_reg7_reg[7] ,
    \slv_reg7_reg[8] ,
    \slv_reg7_reg[9] ,
    \slv_reg7_reg[10] ,
    \slv_reg7_reg[11] ,
    \slv_reg7_reg[12] ,
    \slv_reg7_reg[13] ,
    \slv_reg7_reg[14] ,
    \slv_reg7_reg[15] ,
    snes_data);
  output snes_sclk;
  output snes_latch;
  output [15:0]D;
  input clk;
  input reset_n;
  input [2:0]sel0;
  input \slv_reg7_reg[0] ;
  input [15:0]Q;
  input [7:0]\receive_byte_reg[7] ;
  input [15:0]\slv_reg0_reg[15] ;
  input \slv_reg7_reg[1] ;
  input \slv_reg7_reg[2] ;
  input \slv_reg7_reg[3] ;
  input \slv_reg7_reg[4] ;
  input \slv_reg7_reg[5] ;
  input \slv_reg7_reg[6] ;
  input \slv_reg7_reg[7] ;
  input \slv_reg7_reg[8] ;
  input \slv_reg7_reg[9] ;
  input \slv_reg7_reg[10] ;
  input \slv_reg7_reg[11] ;
  input \slv_reg7_reg[12] ;
  input \slv_reg7_reg[13] ;
  input \slv_reg7_reg[14] ;
  input \slv_reg7_reg[15] ;
  input snes_data;

  wire [15:0]D;
  wire [15:0]Q;
  wire bit_count;
  wire clk;
  wire clock_divider2;
  wire \clock_divider2[0]_i_4_n_0 ;
  wire \clock_divider2[0]_i_5_n_0 ;
  wire \clock_divider2[0]_i_6_n_0 ;
  wire [11:0]clock_divider2_reg;
  wire \clock_divider2_reg[0]_i_3_n_0 ;
  wire \clock_divider2_reg[0]_i_3_n_1 ;
  wire \clock_divider2_reg[0]_i_3_n_2 ;
  wire \clock_divider2_reg[0]_i_3_n_3 ;
  wire \clock_divider2_reg[0]_i_3_n_4 ;
  wire \clock_divider2_reg[0]_i_3_n_5 ;
  wire \clock_divider2_reg[0]_i_3_n_6 ;
  wire \clock_divider2_reg[0]_i_3_n_7 ;
  wire \clock_divider2_reg[4]_i_1_n_0 ;
  wire \clock_divider2_reg[4]_i_1_n_1 ;
  wire \clock_divider2_reg[4]_i_1_n_2 ;
  wire \clock_divider2_reg[4]_i_1_n_3 ;
  wire \clock_divider2_reg[4]_i_1_n_4 ;
  wire \clock_divider2_reg[4]_i_1_n_5 ;
  wire \clock_divider2_reg[4]_i_1_n_6 ;
  wire \clock_divider2_reg[4]_i_1_n_7 ;
  wire \clock_divider2_reg[8]_i_1_n_1 ;
  wire \clock_divider2_reg[8]_i_1_n_2 ;
  wire \clock_divider2_reg[8]_i_1_n_3 ;
  wire \clock_divider2_reg[8]_i_1_n_4 ;
  wire \clock_divider2_reg[8]_i_1_n_5 ;
  wire \clock_divider2_reg[8]_i_1_n_6 ;
  wire \clock_divider2_reg[8]_i_1_n_7 ;
  wire \clock_divider[9]_i_1_n_0 ;
  wire \clock_divider[9]_i_3_n_0 ;
  wire \clock_divider[9]_i_4_n_0 ;
  wire \clock_divider[9]_i_5_n_0 ;
  wire [9:0]clock_divider_reg__0;
  wire control_unit_n_1;
  wire control_unit_n_3;
  wire control_unit_n_4;
  wire datapath_n_0;
  wire datapath_n_1;
  wire old_snes_clk;
  wire [9:0]plusOp__1;
  wire [7:0]\receive_byte_reg[7] ;
  wire reset_n;
  wire [2:0]sel0;
  wire [15:0]\slv_reg0_reg[15] ;
  wire \slv_reg7_reg[0] ;
  wire \slv_reg7_reg[10] ;
  wire \slv_reg7_reg[11] ;
  wire \slv_reg7_reg[12] ;
  wire \slv_reg7_reg[13] ;
  wire \slv_reg7_reg[14] ;
  wire \slv_reg7_reg[15] ;
  wire \slv_reg7_reg[1] ;
  wire \slv_reg7_reg[2] ;
  wire \slv_reg7_reg[3] ;
  wire \slv_reg7_reg[4] ;
  wire \slv_reg7_reg[5] ;
  wire \slv_reg7_reg[6] ;
  wire \slv_reg7_reg[7] ;
  wire \slv_reg7_reg[8] ;
  wire \slv_reg7_reg[9] ;
  wire snes_clk;
  wire snes_data;
  wire snes_latch;
  wire snes_sclk;
  wire [1:1]sw_s;
  wire [3:3]\NLW_clock_divider2_reg[8]_i_1_CO_UNCONNECTED ;

  LUT5 #(
    .INIT(32'h2A222222)) 
    \clock_divider2[0]_i_1 
       (.I0(datapath_n_0),
        .I1(reset_n),
        .I2(\clock_divider2[0]_i_4_n_0 ),
        .I3(\clock_divider2[0]_i_5_n_0 ),
        .I4(clock_divider2_reg[11]),
        .O(clock_divider2));
  LUT5 #(
    .INIT(32'h0000777F)) 
    \clock_divider2[0]_i_4 
       (.I0(control_unit_n_4),
        .I1(clock_divider2_reg[9]),
        .I2(clock_divider2_reg[8]),
        .I3(clock_divider2_reg[6]),
        .I4(clock_divider2_reg[10]),
        .O(\clock_divider2[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \clock_divider2[0]_i_5 
       (.I0(clock_divider2_reg[2]),
        .I1(clock_divider2_reg[0]),
        .I2(clock_divider2_reg[1]),
        .I3(clock_divider2_reg[8]),
        .I4(clock_divider2_reg[5]),
        .I5(clock_divider2_reg[10]),
        .O(\clock_divider2[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \clock_divider2[0]_i_6 
       (.I0(clock_divider2_reg[0]),
        .O(\clock_divider2[0]_i_6_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[0] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[0]_i_3_n_7 ),
        .Q(clock_divider2_reg[0]),
        .R(clock_divider2));
  CARRY4 \clock_divider2_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\clock_divider2_reg[0]_i_3_n_0 ,\clock_divider2_reg[0]_i_3_n_1 ,\clock_divider2_reg[0]_i_3_n_2 ,\clock_divider2_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\clock_divider2_reg[0]_i_3_n_4 ,\clock_divider2_reg[0]_i_3_n_5 ,\clock_divider2_reg[0]_i_3_n_6 ,\clock_divider2_reg[0]_i_3_n_7 }),
        .S({clock_divider2_reg[3:1],\clock_divider2[0]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[10] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[8]_i_1_n_5 ),
        .Q(clock_divider2_reg[10]),
        .R(clock_divider2));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[11] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[8]_i_1_n_4 ),
        .Q(clock_divider2_reg[11]),
        .R(clock_divider2));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[1] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[0]_i_3_n_6 ),
        .Q(clock_divider2_reg[1]),
        .R(clock_divider2));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[2] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[0]_i_3_n_5 ),
        .Q(clock_divider2_reg[2]),
        .R(clock_divider2));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[3] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[0]_i_3_n_4 ),
        .Q(clock_divider2_reg[3]),
        .R(clock_divider2));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[4] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[4]_i_1_n_7 ),
        .Q(clock_divider2_reg[4]),
        .R(clock_divider2));
  CARRY4 \clock_divider2_reg[4]_i_1 
       (.CI(\clock_divider2_reg[0]_i_3_n_0 ),
        .CO({\clock_divider2_reg[4]_i_1_n_0 ,\clock_divider2_reg[4]_i_1_n_1 ,\clock_divider2_reg[4]_i_1_n_2 ,\clock_divider2_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clock_divider2_reg[4]_i_1_n_4 ,\clock_divider2_reg[4]_i_1_n_5 ,\clock_divider2_reg[4]_i_1_n_6 ,\clock_divider2_reg[4]_i_1_n_7 }),
        .S(clock_divider2_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[5] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[4]_i_1_n_6 ),
        .Q(clock_divider2_reg[5]),
        .R(clock_divider2));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[6] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[4]_i_1_n_5 ),
        .Q(clock_divider2_reg[6]),
        .R(clock_divider2));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[7] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[4]_i_1_n_4 ),
        .Q(clock_divider2_reg[7]),
        .R(clock_divider2));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[8] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[8]_i_1_n_7 ),
        .Q(clock_divider2_reg[8]),
        .R(clock_divider2));
  CARRY4 \clock_divider2_reg[8]_i_1 
       (.CI(\clock_divider2_reg[4]_i_1_n_0 ),
        .CO({\NLW_clock_divider2_reg[8]_i_1_CO_UNCONNECTED [3],\clock_divider2_reg[8]_i_1_n_1 ,\clock_divider2_reg[8]_i_1_n_2 ,\clock_divider2_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clock_divider2_reg[8]_i_1_n_4 ,\clock_divider2_reg[8]_i_1_n_5 ,\clock_divider2_reg[8]_i_1_n_6 ,\clock_divider2_reg[8]_i_1_n_7 }),
        .S(clock_divider2_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider2_reg[9] 
       (.C(snes_clk),
        .CE(datapath_n_0),
        .D(\clock_divider2_reg[8]_i_1_n_6 ),
        .Q(clock_divider2_reg[9]),
        .R(clock_divider2));
  LUT1 #(
    .INIT(2'h1)) 
    \clock_divider[0]_i_1 
       (.I0(clock_divider_reg__0[0]),
        .O(plusOp__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \clock_divider[1]_i_1 
       (.I0(clock_divider_reg__0[0]),
        .I1(clock_divider_reg__0[1]),
        .O(plusOp__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \clock_divider[2]_i_1 
       (.I0(clock_divider_reg__0[0]),
        .I1(clock_divider_reg__0[1]),
        .I2(clock_divider_reg__0[2]),
        .O(plusOp__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \clock_divider[3]_i_1 
       (.I0(clock_divider_reg__0[1]),
        .I1(clock_divider_reg__0[0]),
        .I2(clock_divider_reg__0[2]),
        .I3(clock_divider_reg__0[3]),
        .O(plusOp__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \clock_divider[4]_i_1 
       (.I0(clock_divider_reg__0[2]),
        .I1(clock_divider_reg__0[0]),
        .I2(clock_divider_reg__0[1]),
        .I3(clock_divider_reg__0[3]),
        .I4(clock_divider_reg__0[4]),
        .O(plusOp__1[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \clock_divider[5]_i_1 
       (.I0(clock_divider_reg__0[3]),
        .I1(clock_divider_reg__0[1]),
        .I2(clock_divider_reg__0[0]),
        .I3(clock_divider_reg__0[2]),
        .I4(clock_divider_reg__0[4]),
        .I5(clock_divider_reg__0[5]),
        .O(plusOp__1[5]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \clock_divider[6]_i_1 
       (.I0(\clock_divider[9]_i_5_n_0 ),
        .I1(clock_divider_reg__0[6]),
        .O(plusOp__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \clock_divider[7]_i_1 
       (.I0(\clock_divider[9]_i_5_n_0 ),
        .I1(clock_divider_reg__0[6]),
        .I2(clock_divider_reg__0[7]),
        .O(plusOp__1[7]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \clock_divider[8]_i_1 
       (.I0(clock_divider_reg__0[6]),
        .I1(\clock_divider[9]_i_5_n_0 ),
        .I2(clock_divider_reg__0[7]),
        .I3(clock_divider_reg__0[8]),
        .O(plusOp__1[8]));
  LUT4 #(
    .INIT(16'hD0FF)) 
    \clock_divider[9]_i_1 
       (.I0(\clock_divider[9]_i_3_n_0 ),
        .I1(\clock_divider[9]_i_4_n_0 ),
        .I2(clock_divider_reg__0[9]),
        .I3(reset_n),
        .O(\clock_divider[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \clock_divider[9]_i_2 
       (.I0(clock_divider_reg__0[7]),
        .I1(\clock_divider[9]_i_5_n_0 ),
        .I2(clock_divider_reg__0[6]),
        .I3(clock_divider_reg__0[8]),
        .I4(clock_divider_reg__0[9]),
        .O(plusOp__1[9]));
  LUT6 #(
    .INIT(64'h007FFFFF00FFFFFF)) 
    \clock_divider[9]_i_3 
       (.I0(clock_divider_reg__0[1]),
        .I1(clock_divider_reg__0[2]),
        .I2(clock_divider_reg__0[0]),
        .I3(clock_divider_reg__0[5]),
        .I4(clock_divider_reg__0[6]),
        .I5(clock_divider_reg__0[4]),
        .O(\clock_divider[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEEEEEEEEEEEEE)) 
    \clock_divider[9]_i_4 
       (.I0(clock_divider_reg__0[7]),
        .I1(clock_divider_reg__0[8]),
        .I2(clock_divider_reg__0[4]),
        .I3(clock_divider_reg__0[5]),
        .I4(clock_divider_reg__0[3]),
        .I5(clock_divider_reg__0[6]),
        .O(\clock_divider[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \clock_divider[9]_i_5 
       (.I0(clock_divider_reg__0[5]),
        .I1(clock_divider_reg__0[3]),
        .I2(clock_divider_reg__0[1]),
        .I3(clock_divider_reg__0[0]),
        .I4(clock_divider_reg__0[2]),
        .I5(clock_divider_reg__0[4]),
        .O(\clock_divider[9]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp__1[0]),
        .Q(clock_divider_reg__0[0]),
        .R(\clock_divider[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp__1[1]),
        .Q(clock_divider_reg__0[1]),
        .R(\clock_divider[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp__1[2]),
        .Q(clock_divider_reg__0[2]),
        .R(\clock_divider[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp__1[3]),
        .Q(clock_divider_reg__0[3]),
        .R(\clock_divider[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp__1[4]),
        .Q(clock_divider_reg__0[4]),
        .R(\clock_divider[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp__1[5]),
        .Q(clock_divider_reg__0[5]),
        .R(\clock_divider[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp__1[6]),
        .Q(clock_divider_reg__0[6]),
        .R(\clock_divider[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp__1[7]),
        .Q(clock_divider_reg__0[7]),
        .R(\clock_divider[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp__1[8]),
        .Q(clock_divider_reg__0[8]),
        .R(\clock_divider[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clock_divider_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(plusOp__1[9]),
        .Q(clock_divider_reg__0[9]),
        .R(\clock_divider[9]_i_1_n_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SNES_cu control_unit
       (.E(control_unit_n_1),
        .\FSM_sequential_state_reg[0]_0 (control_unit_n_4),
        .Q(sw_s),
        .SR(control_unit_n_3),
        .clk(clk),
        .clock_divider2_reg(clock_divider2_reg[11:3]),
        .\clock_divider_reg[7] (datapath_n_0),
        .\packet_in_process_reg[15] (bit_count),
        .reset_n(reset_n),
        .snes_sclk(snes_sclk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SNES_dp datapath
       (.D(D),
        .E(bit_count),
        .\FSM_sequential_state_reg[0] (control_unit_n_1),
        .Q(clock_divider_reg__0[9:2]),
        .SR(control_unit_n_3),
        .\bit_count_reg[4]_0 (sw_s),
        .clk(clk),
        .\clock_divider2_reg[11] (datapath_n_0),
        .\clock_divider2_reg[11]_0 (datapath_n_1),
        .old_snes_clk(old_snes_clk),
        .\receive_byte_reg[7] (\receive_byte_reg[7] ),
        .reset_n(reset_n),
        .sel0(sel0),
        .\slv_reg0_reg[15] (\slv_reg0_reg[15] ),
        .\slv_reg2_reg[15] (Q),
        .\slv_reg7_reg[0] (\slv_reg7_reg[0] ),
        .\slv_reg7_reg[10] (\slv_reg7_reg[10] ),
        .\slv_reg7_reg[11] (\slv_reg7_reg[11] ),
        .\slv_reg7_reg[12] (\slv_reg7_reg[12] ),
        .\slv_reg7_reg[13] (\slv_reg7_reg[13] ),
        .\slv_reg7_reg[14] (\slv_reg7_reg[14] ),
        .\slv_reg7_reg[15] (\slv_reg7_reg[15] ),
        .\slv_reg7_reg[1] (\slv_reg7_reg[1] ),
        .\slv_reg7_reg[2] (\slv_reg7_reg[2] ),
        .\slv_reg7_reg[3] (\slv_reg7_reg[3] ),
        .\slv_reg7_reg[4] (\slv_reg7_reg[4] ),
        .\slv_reg7_reg[5] (\slv_reg7_reg[5] ),
        .\slv_reg7_reg[6] (\slv_reg7_reg[6] ),
        .\slv_reg7_reg[7] (\slv_reg7_reg[7] ),
        .\slv_reg7_reg[8] (\slv_reg7_reg[8] ),
        .\slv_reg7_reg[9] (\slv_reg7_reg[9] ),
        .snes_data(snes_data));
  LUT5 #(
    .INIT(32'hEEEAFFFF)) 
    old_snes_clk_i_1
       (.I0(clock_divider_reg__0[9]),
        .I1(clock_divider_reg__0[8]),
        .I2(clock_divider_reg__0[6]),
        .I3(clock_divider_reg__0[7]),
        .I4(datapath_n_1),
        .O(snes_clk));
  FDRE old_snes_clk_reg
       (.C(clk),
        .CE(1'b1),
        .D(snes_clk),
        .Q(old_snes_clk),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAA8888888888888)) 
    snes_latch_INST_0
       (.I0(clock_divider2_reg[11]),
        .I1(clock_divider2_reg[10]),
        .I2(clock_divider2_reg[6]),
        .I3(clock_divider2_reg[8]),
        .I4(clock_divider2_reg[9]),
        .I5(control_unit_n_4),
        .O(snes_latch));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SPI_cu
   (SR,
    ready,
    E,
    \packet_out_process_reg[0] ,
    p_2_in,
    p_1_in,
    new_packet_s_reg,
    ready_s_reg,
    \packet_in_process_reg[7] ,
    slave_ready,
    reset_n,
    ready_s,
    \packet_out_process_reg[0]_0 ,
    \slv_reg0_reg[7] ,
    \packet_out_process_reg[1] ,
    \packet_out_process_reg[2] ,
    \packet_out_process_reg[3] ,
    \packet_out_process_reg[4] ,
    \packet_out_process_reg[5] ,
    \packet_out_process_reg[6] ,
    new_packet_s_reg_0,
    waiting_byte_cpu,
    Q,
    sclk,
    frame_end,
    \bit_count_reg[3] );
  output [0:0]SR;
  output ready;
  output [0:0]E;
  output \packet_out_process_reg[0] ;
  output [6:0]p_2_in;
  output [0:0]p_1_in;
  output new_packet_s_reg;
  output ready_s_reg;
  output [0:0]\packet_in_process_reg[7] ;
  output slave_ready;
  input reset_n;
  input ready_s;
  input \packet_out_process_reg[0]_0 ;
  input [7:0]\slv_reg0_reg[7] ;
  input \packet_out_process_reg[1] ;
  input \packet_out_process_reg[2] ;
  input \packet_out_process_reg[3] ;
  input \packet_out_process_reg[4] ;
  input \packet_out_process_reg[5] ;
  input \packet_out_process_reg[6] ;
  input new_packet_s_reg_0;
  input waiting_byte_cpu;
  input [0:0]Q;
  input sclk;
  input frame_end;
  input [0:0]\bit_count_reg[3] ;

  wire [0:0]E;
  wire \FSM_sequential_state[0]_i_1_n_0 ;
  wire \FSM_sequential_state[0]_i_2_n_0 ;
  wire \FSM_sequential_state[1]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_2_n_0 ;
  wire \FSM_sequential_state[2]_i_3_n_0 ;
  wire [0:0]Q;
  wire [0:0]SR;
  wire [0:0]\bit_count_reg[3] ;
  wire frame_end;
  wire new_packet_s_reg;
  wire new_packet_s_reg_0;
  wire [0:0]p_1_in;
  wire [6:0]p_2_in;
  wire [0:0]\packet_in_process_reg[7] ;
  wire \packet_out_process_reg[0] ;
  wire \packet_out_process_reg[0]_0 ;
  wire \packet_out_process_reg[1] ;
  wire \packet_out_process_reg[2] ;
  wire \packet_out_process_reg[3] ;
  wire \packet_out_process_reg[4] ;
  wire \packet_out_process_reg[5] ;
  wire \packet_out_process_reg[6] ;
  wire ready;
  wire ready_s;
  wire ready_s_reg;
  wire reset_n;
  wire sclk;
  wire slave_ready;
  wire [7:0]\slv_reg0_reg[7] ;
  (* RTL_KEEP = "yes" *) wire [2:0]state;
  wire waiting_byte_cpu;

  LUT4 #(
    .INIT(16'hE200)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(state[0]),
        .I1(\FSM_sequential_state[2]_i_2_n_0 ),
        .I2(\FSM_sequential_state[0]_i_2_n_0 ),
        .I3(reset_n),
        .O(\FSM_sequential_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAB3AAF7AAF7AAF7)) 
    \FSM_sequential_state[0]_i_2 
       (.I0(state[2]),
        .I1(state[0]),
        .I2(\bit_count_reg[3] ),
        .I3(state[1]),
        .I4(new_packet_s_reg_0),
        .I5(frame_end),
        .O(\FSM_sequential_state[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h2EE20000)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(state[1]),
        .I1(\FSM_sequential_state[2]_i_2_n_0 ),
        .I2(state[1]),
        .I3(state[0]),
        .I4(reset_n),
        .O(\FSM_sequential_state[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hE200)) 
    \FSM_sequential_state[2]_i_1 
       (.I0(state[2]),
        .I1(\FSM_sequential_state[2]_i_2_n_0 ),
        .I2(\FSM_sequential_state[2]_i_3_n_0 ),
        .I3(reset_n),
        .O(\FSM_sequential_state[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h77EE76EEB7BFB7BF)) 
    \FSM_sequential_state[2]_i_2 
       (.I0(state[1]),
        .I1(state[2]),
        .I2(sclk),
        .I3(frame_end),
        .I4(new_packet_s_reg_0),
        .I5(state[0]),
        .O(\FSM_sequential_state[2]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h1FF0)) 
    \FSM_sequential_state[2]_i_3 
       (.I0(state[0]),
        .I1(sclk),
        .I2(state[1]),
        .I3(state[2]),
        .O(\FSM_sequential_state[2]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "reset:000,wait_byte:001,load_byte:010,wait_high:110,capture:011,wait_low:100,change:101,end_frame:111" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[0] 
       (.C(Q),
        .CE(1'b1),
        .D(\FSM_sequential_state[0]_i_1_n_0 ),
        .Q(state[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "reset:000,wait_byte:001,load_byte:010,wait_high:110,capture:011,wait_low:100,change:101,end_frame:111" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[1] 
       (.C(Q),
        .CE(1'b1),
        .D(\FSM_sequential_state[1]_i_1_n_0 ),
        .Q(state[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "reset:000,wait_byte:001,load_byte:010,wait_high:110,capture:011,wait_low:100,change:101,end_frame:111" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_state_reg[2] 
       (.C(Q),
        .CE(1'b1),
        .D(\FSM_sequential_state[2]_i_1_n_0 ),
        .Q(state[2]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h57)) 
    \bit_count[3]_i_1__0 
       (.I0(reset_n),
        .I1(state[2]),
        .I2(state[1]),
        .O(SR));
  LUT6 #(
    .INIT(64'hE0E0E0E0E0E000E0)) 
    new_packet_s_i_1
       (.I0(new_packet_s_reg_0),
        .I1(waiting_byte_cpu),
        .I2(reset_n),
        .I3(state[1]),
        .I4(state[2]),
        .I5(state[0]),
        .O(new_packet_s_reg));
  LUT3 #(
    .INIT(8'h08)) 
    \packet_in_process[7]_i_1 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(state[2]),
        .O(\packet_in_process_reg[7] ));
  LUT6 #(
    .INIT(64'hA000A0A0A0A0C0A0)) 
    \packet_out_process[0]_i_1 
       (.I0(\packet_out_process_reg[0]_0 ),
        .I1(\slv_reg0_reg[7] [0]),
        .I2(reset_n),
        .I3(state[1]),
        .I4(state[0]),
        .I5(state[2]),
        .O(\packet_out_process_reg[0] ));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \packet_out_process[1]_i_1 
       (.I0(\packet_out_process_reg[0]_0 ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(\slv_reg0_reg[7] [1]),
        .O(p_2_in[0]));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \packet_out_process[2]_i_1 
       (.I0(\packet_out_process_reg[1] ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(\slv_reg0_reg[7] [2]),
        .O(p_2_in[1]));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \packet_out_process[3]_i_1 
       (.I0(\packet_out_process_reg[2] ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(\slv_reg0_reg[7] [3]),
        .O(p_2_in[2]));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \packet_out_process[4]_i_1 
       (.I0(\packet_out_process_reg[3] ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(\slv_reg0_reg[7] [4]),
        .O(p_2_in[3]));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \packet_out_process[5]_i_1 
       (.I0(\packet_out_process_reg[4] ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(\slv_reg0_reg[7] [5]),
        .O(p_2_in[4]));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \packet_out_process[6]_i_1 
       (.I0(\packet_out_process_reg[5] ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(\slv_reg0_reg[7] [6]),
        .O(p_2_in[5]));
  LUT3 #(
    .INIT(8'h18)) 
    \packet_out_process[7]_i_2 
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .O(p_1_in));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \packet_out_process[7]_i_3 
       (.I0(\packet_out_process_reg[6] ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(\slv_reg0_reg[7] [7]),
        .O(p_2_in[6]));
  LUT4 #(
    .INIT(16'h0080)) 
    ready_INST_0
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .I3(ready_s),
        .O(ready));
  LUT4 #(
    .INIT(16'h8000)) 
    ready_s_i_1
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .I3(reset_n),
        .O(ready_s_reg));
  LUT4 #(
    .INIT(16'h8000)) 
    \receive_byte[7]_i_1 
       (.I0(reset_n),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .O(E));
  LUT3 #(
    .INIT(8'h87)) 
    slave_ready_INST_0
       (.I0(state[1]),
        .I1(state[0]),
        .I2(state[2]),
        .O(slave_ready));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SPI_dp
   (miso,
    \packet_out_process_reg[7]_0 ,
    \packet_out_process_reg[6]_0 ,
    \packet_out_process_reg[5]_0 ,
    \packet_out_process_reg[4]_0 ,
    \packet_out_process_reg[3]_0 ,
    \packet_out_process_reg[2]_0 ,
    \packet_out_process_reg[0]_0 ,
    ready_s,
    waiting_byte,
    \bit_count_reg[3]_0 ,
    \axi_rdata_reg[7] ,
    p_1_in,
    p_2_in,
    Q,
    \packet_out_process_reg[0]_1 ,
    \FSM_sequential_state_reg[2] ,
    new_packet_s_reg_0,
    clk,
    reset_n,
    E,
    mosi,
    \FSM_sequential_state_reg[1] ,
    SR);
  output miso;
  output \packet_out_process_reg[7]_0 ;
  output \packet_out_process_reg[6]_0 ;
  output \packet_out_process_reg[5]_0 ;
  output \packet_out_process_reg[4]_0 ;
  output \packet_out_process_reg[3]_0 ;
  output \packet_out_process_reg[2]_0 ;
  output \packet_out_process_reg[0]_0 ;
  output ready_s;
  output waiting_byte;
  output [0:0]\bit_count_reg[3]_0 ;
  output [7:0]\axi_rdata_reg[7] ;
  input [0:0]p_1_in;
  input [6:0]p_2_in;
  input [0:0]Q;
  input \packet_out_process_reg[0]_1 ;
  input \FSM_sequential_state_reg[2] ;
  input new_packet_s_reg_0;
  input clk;
  input reset_n;
  input [0:0]E;
  input mosi;
  input [0:0]\FSM_sequential_state_reg[1] ;
  input [0:0]SR;

  wire [0:0]E;
  wire [0:0]\FSM_sequential_state_reg[1] ;
  wire \FSM_sequential_state_reg[2] ;
  wire [0:0]Q;
  wire [0:0]SR;
  wire [7:0]\axi_rdata_reg[7] ;
  wire [0:0]\bit_count_reg[3]_0 ;
  wire \bit_count_reg_n_0_[0] ;
  wire \bit_count_reg_n_0_[1] ;
  wire \bit_count_reg_n_0_[2] ;
  wire clk;
  wire miso;
  wire mosi;
  wire new_packet_s_reg_0;
  wire p_0_in0;
  wire [0:0]p_1_in;
  wire [6:0]p_2_in;
  wire [7:0]packet_in_process;
  wire \packet_out_process_reg[0]_0 ;
  wire \packet_out_process_reg[0]_1 ;
  wire \packet_out_process_reg[2]_0 ;
  wire \packet_out_process_reg[3]_0 ;
  wire \packet_out_process_reg[4]_0 ;
  wire \packet_out_process_reg[5]_0 ;
  wire \packet_out_process_reg[6]_0 ;
  wire \packet_out_process_reg[7]_0 ;
  wire [3:0]plusOp__0;
  wire ready_s;
  wire reset_n;
  wire waiting_byte;

  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \bit_count[0]_i_1 
       (.I0(\bit_count_reg_n_0_[0] ),
        .O(plusOp__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \bit_count[1]_i_1 
       (.I0(\bit_count_reg_n_0_[0] ),
        .I1(\bit_count_reg_n_0_[1] ),
        .O(plusOp__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \bit_count[2]_i_1 
       (.I0(\bit_count_reg_n_0_[0] ),
        .I1(\bit_count_reg_n_0_[1] ),
        .I2(\bit_count_reg_n_0_[2] ),
        .O(plusOp__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \bit_count[3]_i_2 
       (.I0(\bit_count_reg_n_0_[1] ),
        .I1(\bit_count_reg_n_0_[0] ),
        .I2(\bit_count_reg_n_0_[2] ),
        .I3(\bit_count_reg[3]_0 ),
        .O(plusOp__0[3]));
  FDRE \bit_count_reg[0] 
       (.C(Q),
        .CE(E),
        .D(plusOp__0[0]),
        .Q(\bit_count_reg_n_0_[0] ),
        .R(SR));
  FDRE \bit_count_reg[1] 
       (.C(Q),
        .CE(E),
        .D(plusOp__0[1]),
        .Q(\bit_count_reg_n_0_[1] ),
        .R(SR));
  FDRE \bit_count_reg[2] 
       (.C(Q),
        .CE(E),
        .D(plusOp__0[2]),
        .Q(\bit_count_reg_n_0_[2] ),
        .R(SR));
  FDRE \bit_count_reg[3] 
       (.C(Q),
        .CE(E),
        .D(plusOp__0[3]),
        .Q(\bit_count_reg[3]_0 ),
        .R(SR));
  FDRE new_packet_s_reg
       (.C(clk),
        .CE(1'b1),
        .D(new_packet_s_reg_0),
        .Q(waiting_byte),
        .R(1'b0));
  FDRE \packet_in_process_reg[0] 
       (.C(Q),
        .CE(E),
        .D(mosi),
        .Q(packet_in_process[0]),
        .R(p_0_in0));
  FDRE \packet_in_process_reg[1] 
       (.C(Q),
        .CE(E),
        .D(packet_in_process[0]),
        .Q(packet_in_process[1]),
        .R(p_0_in0));
  FDRE \packet_in_process_reg[2] 
       (.C(Q),
        .CE(E),
        .D(packet_in_process[1]),
        .Q(packet_in_process[2]),
        .R(p_0_in0));
  FDRE \packet_in_process_reg[3] 
       (.C(Q),
        .CE(E),
        .D(packet_in_process[2]),
        .Q(packet_in_process[3]),
        .R(p_0_in0));
  FDRE \packet_in_process_reg[4] 
       (.C(Q),
        .CE(E),
        .D(packet_in_process[3]),
        .Q(packet_in_process[4]),
        .R(p_0_in0));
  FDRE \packet_in_process_reg[5] 
       (.C(Q),
        .CE(E),
        .D(packet_in_process[4]),
        .Q(packet_in_process[5]),
        .R(p_0_in0));
  FDRE \packet_in_process_reg[6] 
       (.C(Q),
        .CE(E),
        .D(packet_in_process[5]),
        .Q(packet_in_process[6]),
        .R(p_0_in0));
  FDRE \packet_in_process_reg[7] 
       (.C(Q),
        .CE(E),
        .D(packet_in_process[6]),
        .Q(packet_in_process[7]),
        .R(p_0_in0));
  LUT1 #(
    .INIT(2'h1)) 
    \packet_out_process[7]_i_1 
       (.I0(reset_n),
        .O(p_0_in0));
  FDRE \packet_out_process_reg[0] 
       (.C(Q),
        .CE(1'b1),
        .D(\packet_out_process_reg[0]_1 ),
        .Q(\packet_out_process_reg[0]_0 ),
        .R(1'b0));
  FDRE \packet_out_process_reg[1] 
       (.C(Q),
        .CE(p_1_in),
        .D(p_2_in[0]),
        .Q(\packet_out_process_reg[2]_0 ),
        .R(p_0_in0));
  FDRE \packet_out_process_reg[2] 
       (.C(Q),
        .CE(p_1_in),
        .D(p_2_in[1]),
        .Q(\packet_out_process_reg[3]_0 ),
        .R(p_0_in0));
  FDRE \packet_out_process_reg[3] 
       (.C(Q),
        .CE(p_1_in),
        .D(p_2_in[2]),
        .Q(\packet_out_process_reg[4]_0 ),
        .R(p_0_in0));
  FDRE \packet_out_process_reg[4] 
       (.C(Q),
        .CE(p_1_in),
        .D(p_2_in[3]),
        .Q(\packet_out_process_reg[5]_0 ),
        .R(p_0_in0));
  FDRE \packet_out_process_reg[5] 
       (.C(Q),
        .CE(p_1_in),
        .D(p_2_in[4]),
        .Q(\packet_out_process_reg[6]_0 ),
        .R(p_0_in0));
  FDRE \packet_out_process_reg[6] 
       (.C(Q),
        .CE(p_1_in),
        .D(p_2_in[5]),
        .Q(\packet_out_process_reg[7]_0 ),
        .R(p_0_in0));
  FDRE \packet_out_process_reg[7] 
       (.C(Q),
        .CE(p_1_in),
        .D(p_2_in[6]),
        .Q(miso),
        .R(p_0_in0));
  FDRE ready_s_reg
       (.C(Q),
        .CE(1'b1),
        .D(\FSM_sequential_state_reg[2] ),
        .Q(ready_s),
        .R(1'b0));
  FDRE \receive_byte_reg[0] 
       (.C(Q),
        .CE(\FSM_sequential_state_reg[1] ),
        .D(packet_in_process[0]),
        .Q(\axi_rdata_reg[7] [0]),
        .R(1'b0));
  FDRE \receive_byte_reg[1] 
       (.C(Q),
        .CE(\FSM_sequential_state_reg[1] ),
        .D(packet_in_process[1]),
        .Q(\axi_rdata_reg[7] [1]),
        .R(1'b0));
  FDRE \receive_byte_reg[2] 
       (.C(Q),
        .CE(\FSM_sequential_state_reg[1] ),
        .D(packet_in_process[2]),
        .Q(\axi_rdata_reg[7] [2]),
        .R(1'b0));
  FDRE \receive_byte_reg[3] 
       (.C(Q),
        .CE(\FSM_sequential_state_reg[1] ),
        .D(packet_in_process[3]),
        .Q(\axi_rdata_reg[7] [3]),
        .R(1'b0));
  FDRE \receive_byte_reg[4] 
       (.C(Q),
        .CE(\FSM_sequential_state_reg[1] ),
        .D(packet_in_process[4]),
        .Q(\axi_rdata_reg[7] [4]),
        .R(1'b0));
  FDRE \receive_byte_reg[5] 
       (.C(Q),
        .CE(\FSM_sequential_state_reg[1] ),
        .D(packet_in_process[5]),
        .Q(\axi_rdata_reg[7] [5]),
        .R(1'b0));
  FDRE \receive_byte_reg[6] 
       (.C(Q),
        .CE(\FSM_sequential_state_reg[1] ),
        .D(packet_in_process[6]),
        .Q(\axi_rdata_reg[7] [6]),
        .R(1'b0));
  FDRE \receive_byte_reg[7] 
       (.C(Q),
        .CE(\FSM_sequential_state_reg[1] ),
        .D(packet_in_process[7]),
        .Q(\axi_rdata_reg[7] [7]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SPI_top
   (miso,
    waiting_byte,
    ready,
    \axi_rdata_reg[7] ,
    slave_ready,
    Q,
    clk,
    reset_n,
    \slv_reg0_reg[7] ,
    waiting_byte_cpu,
    mosi,
    sclk,
    frame_end);
  output miso;
  output waiting_byte;
  output ready;
  output [7:0]\axi_rdata_reg[7] ;
  output slave_ready;
  input [0:0]Q;
  input clk;
  input reset_n;
  input [7:0]\slv_reg0_reg[7] ;
  input waiting_byte_cpu;
  input mosi;
  input sclk;
  input frame_end;

  wire [0:0]Q;
  wire [7:0]\axi_rdata_reg[7] ;
  wire clk;
  wire control_unit_n_0;
  wire control_unit_n_12;
  wire control_unit_n_13;
  wire control_unit_n_2;
  wire control_unit_n_3;
  wire [2:2]cw_s;
  wire datapath_n_1;
  wire datapath_n_2;
  wire datapath_n_3;
  wire datapath_n_4;
  wire datapath_n_5;
  wire datapath_n_6;
  wire datapath_n_7;
  wire frame_end;
  wire miso;
  wire mosi;
  wire [7:7]p_1_in;
  wire [7:1]p_2_in;
  wire ready;
  wire ready_s;
  wire reset_n;
  wire sclk;
  wire slave_ready;
  wire [7:0]\slv_reg0_reg[7] ;
  wire [2:2]sw_s;
  wire waiting_byte;
  wire waiting_byte_cpu;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SPI_cu control_unit
       (.E(control_unit_n_2),
        .Q(Q),
        .SR(control_unit_n_0),
        .\bit_count_reg[3] (sw_s),
        .frame_end(frame_end),
        .new_packet_s_reg(control_unit_n_12),
        .new_packet_s_reg_0(waiting_byte),
        .p_1_in(p_1_in),
        .p_2_in(p_2_in),
        .\packet_in_process_reg[7] (cw_s),
        .\packet_out_process_reg[0] (control_unit_n_3),
        .\packet_out_process_reg[0]_0 (datapath_n_7),
        .\packet_out_process_reg[1] (datapath_n_6),
        .\packet_out_process_reg[2] (datapath_n_5),
        .\packet_out_process_reg[3] (datapath_n_4),
        .\packet_out_process_reg[4] (datapath_n_3),
        .\packet_out_process_reg[5] (datapath_n_2),
        .\packet_out_process_reg[6] (datapath_n_1),
        .ready(ready),
        .ready_s(ready_s),
        .ready_s_reg(control_unit_n_13),
        .reset_n(reset_n),
        .sclk(sclk),
        .slave_ready(slave_ready),
        .\slv_reg0_reg[7] (\slv_reg0_reg[7] ),
        .waiting_byte_cpu(waiting_byte_cpu));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_SPI_dp datapath
       (.E(cw_s),
        .\FSM_sequential_state_reg[1] (control_unit_n_2),
        .\FSM_sequential_state_reg[2] (control_unit_n_13),
        .Q(Q),
        .SR(control_unit_n_0),
        .\axi_rdata_reg[7] (\axi_rdata_reg[7] ),
        .\bit_count_reg[3]_0 (sw_s),
        .clk(clk),
        .miso(miso),
        .mosi(mosi),
        .new_packet_s_reg_0(control_unit_n_12),
        .p_1_in(p_1_in),
        .p_2_in(p_2_in),
        .\packet_out_process_reg[0]_0 (datapath_n_7),
        .\packet_out_process_reg[0]_1 (control_unit_n_3),
        .\packet_out_process_reg[2]_0 (datapath_n_6),
        .\packet_out_process_reg[3]_0 (datapath_n_5),
        .\packet_out_process_reg[4]_0 (datapath_n_4),
        .\packet_out_process_reg[5]_0 (datapath_n_3),
        .\packet_out_process_reg[6]_0 (datapath_n_2),
        .\packet_out_process_reg[7]_0 (datapath_n_1),
        .ready_s(ready_s),
        .reset_n(reset_n),
        .waiting_byte(waiting_byte));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_Rokenbok_SPI_Interface_0_0,Rokenbok_SPI_Interface_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "Rokenbok_SPI_Interface_v1_0,Vivado 2017.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    reset_n,
    miso,
    mosi,
    sclk,
    frame_end,
    slave_ready,
    ready,
    waiting_byte,
    waiting_byte_cpu,
    snes_sclk,
    snes_latch,
    snes_data,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_RESET reset_n, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1" *) input clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 reset_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME reset_n, POLARITY ACTIVE_LOW" *) input reset_n;
  output miso;
  input mosi;
  input sclk;
  input frame_end;
  output slave_ready;
  output ready;
  output waiting_byte;
  output waiting_byte_cpu;
  output snes_sclk;
  output snes_latch;
  input snes_data;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 8, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [4:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [4:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire clk;
  wire frame_end;
  wire miso;
  wire mosi;
  wire ready;
  wire reset_n;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire sclk;
  wire slave_ready;
  wire snes_data;
  wire snes_latch;
  wire snes_sclk;
  wire waiting_byte;
  wire waiting_byte_cpu;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Rokenbok_SPI_Interface_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .clk(clk),
        .frame_end(frame_end),
        .miso(miso),
        .mosi(mosi),
        .ready(ready),
        .reset_n(reset_n),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .sclk(sclk),
        .slave_ready(slave_ready),
        .snes_data(snes_data),
        .snes_latch(snes_latch),
        .snes_sclk(snes_sclk),
        .waiting_byte(waiting_byte),
        .waiting_byte_cpu(waiting_byte_cpu));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
