----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw1_tb.vhd
-- HW:		HW1
-- Purp:	Test a digital system with four bits of inputs and two bits of outputs.
--          At least one of the inputs is always equal to 1.
--          The output encodes the index of the most significant 1 in the input.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity hw1_tb is 
end entity hw1_tb;

architecture behavior of hw1_tb is

	----------------------------------------------------------------------
	-- These signal names must match the names of the I/O markers
	----------------------------------------------------------------------
	component hw1 is
        port(i3, i2, i1, i0:	in std_logic; 
                o1, o0:           out std_logic);
	end component;
	
	signal i3_s, i2_s, i1_s, i0_s, o1_s, o0_s : std_logic;
	  
	CONSTANT TEST_ELEMENTS:integer:=16;
	SUBTYPE INPUT is std_logic_vector(3 downto 0);
	TYPE TEST_INPUT_VECTOR is array (1 to TEST_ELEMENTS) of INPUT;
	SIGNAL TEST_IN: TEST_INPUT_VECTOR := (	"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111");

	TYPE TEST_OUTPUT_VECTOR is array (1 to TEST_ELEMENTS) of std_logic;
	SIGNAL TEST_OUT_1: TEST_OUTPUT_VECTOR := ('0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');
	SIGNAL TEST_OUT_0: TEST_OUTPUT_VECTOR := ('0', '0', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1');

	SIGNAL i : integer;		

begin

	----------------------------------------------------------------------
	-- Create an instance of hw1
	----------------------------------------------------------------------
	UUT:	hw1 port map (i3_s, i2_s, i1_s, i0_s, o1_s, o0_s);

	tb : PROCESS
	BEGIN
	for i in 1 to TEST_ELEMENTS loop
		-----------------------------------------
		-- Parse out the bits of the test_vector
		-----------------------------------------
		i3_s <= test_in(i)(3);
		i2_s <= test_in(i)(2);
		i1_s <= test_in(i)(1);
		i0_s <= test_in(i)(0);
		
		wait for 10 ns; 
		assert o1_s = test_out_1(i)
 				report "Error with input " & integer'image(i) & " in O1"
				severity warning;
				
        assert o0_s = test_out_0(i)
                report "Error with input " & integer'image(i) & " in O0"
                severity warning;
				
	end loop;
	
	---------------------------
	-- Just halt the simulator
	---------------------------
	assert TRUE = FALSE 
		report "---------------Self-checking testbench completed.  Nominal circuit behavior---------------"
		severity failure;
			
	END PROCESS tb;

end architecture behavior;
