----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw1.vhd
-- HW:		HW1
-- Purp:	Implements a digital system with four bits of inputs and two bits of outputs.
--          At least one of the inputs is always equal to 1.
--          The output encodes the index of the most significant 1 in the input.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;						-- These lines are similar to a #include in C
use IEEE.std_logic_1164.all;

entity hw1 is
        port(i3, i2, i1, i0:	in std_logic; 
					o1, o0:   		out std_logic);
end hw1;

architecture structure of hw1 is
-- signal	s1, s2, s3: std_logic;	-- wires which begin and end in the component

begin 
	o1 <= i2 or i3;
	o0 <= i3 or (not i2 and i1);
end structure;
