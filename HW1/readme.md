# Homework #1

## Author: C2C Jeremy Schwartz
### Last Modified: 8 Jan 2018

#### Problem 1

1a. Cost Per Unit Equations

 - FPGA = 15*N
 - Gate Array = 3*N + 20,0000
 - Standard Cell = 1*N + 100,000

1b. Plot of Equations

![CPU Plot](images/graph.png)
##### Figure 1: Cost Per Unit Plots

1c. FPGA technology has the minimal per-unit cost from 1 to 1,666 units.  I solved for 1,666 by combining the FPGA and Gate Array equations and solving for N.

1d. Gate Array technology has the minimal per-unit cost from 1,667 to 40,000 units.  I solved for 1,667 by combining the FPGA and Gate Array equations and solving for N.  I solved for 40,000 by combining the Standard Cell and Gate Array equations and solving for N.

1e. Standard Cell technology has the minimal per-unit cost from 40,000 units upwards.  I solved for 40,000 by combining the Standard Cell and Gate Array equations and solving for N.

#### Problem 2

The illustration shown is a structural view.

#### Problem 3

Abstraction is the process of concealing the complexity of a system by simplifying it into a "black box" with inputs and outputs.  For example, the transistor layout of an AND gate is abstracted away when it is represented with a simple symbol with inputs and outputs.  Abstraction is important for digital system design because some systems are simply to complicated to reasonably display in a less abstracted form or because their inner construction is not as important and their actually functionality.

#### Problem 4

Verification is checking to see if a design meets its required specifications. Testing is checking to see if a physical product still performs properly (i.e. looking for defects during manufacturing).

#### Problem 5

Vivado has been installed!

#### Problem 6

6a. Truth Table

| I3 | I2 | I1 | I0 |   | O1 | O0 |
|:--:|:--:|:--:|:--:|---|:--:|:--:|
|  0 |  0 |  0 |  0 |   |  X |  X |
|  0 |  0 |  0 |  1 |   |  0 |  0 |
|  0 |  0 |  1 |  0 |   |  0 |  1 |
|  0 |  0 |  1 |  1 |   |  0 |  1 |
|  0 |  1 |  0 |  0 |   |  1 |  0 |
|  0 |  1 |  0 |  1 |   |  1 |  0 |
|  0 |  1 |  1 |  0 |   |  1 |  0 |
|  0 |  1 |  1 |  1 |   |  1 |  0 |
|  1 |  0 |  0 |  0 |   |  1 |  1 |
|  1 |  0 |  0 |  1 |   |  1 |  1 |
|  1 |  0 |  1 |  0 |   |  1 |  1 |
|  1 |  0 |  1 |  1 |   |  1 |  1 |
|  1 |  1 |  0 |  0 |   |  1 |  1 |
|  1 |  1 |  0 |  1 |   |  1 |  1 |
|  1 |  1 |  1 |  0 |   |  1 |  1 |
|  1 |  1 |  1 |  1 |   |  1 |  1 |

Above: Full Truth Table

| I3 | I2 | I1 | I0 |   | O1 | O0 |
|:--:|:--:|:--:|:--:|---|:--:|:--:|
|  0 |  0 |  0 |  0 |   |  X |  X |
|  0 |  0 |  0 |  1 |   |  0 |  0 |
|  0 |  0 |  1 |  X |   |  0 |  1 |
|  0 |  1 |  X |  X |   |  1 |  0 |
|  1 |  X |  X |  X |   |  1 |  1 |

Above: Simplified Truth Table

6b. Two K-Maps

![K Maps](images/kmaps.png)
##### Figure 2: O1 and O0 K-Maps

6c. Minimal SOP Expressions for O1 and O0

O1 = I2 + I3

O0 = I3 + I2'I1

6d. VHDL Code in "Code" Folder