set_property SRC_FILE_INFO {cfile:C:/ece_383_schwartz/Lab4/code/Lab4.xdc rfile:../../../../../Lab4.xdc id:1} [current_design]
set_property src_info {type:XDC file:1 line:101 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN T4    IOSTANDARD LVCMOS33 } [get_ports { ac_adc_sdata_0 }]; #IO_L13N_T2_MRCC_34 Sch=ac_adc_sdata #SDATA_IN
set_property src_info {type:XDC file:1 line:102 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN T5    IOSTANDARD LVCMOS33 } [get_ports { ac_bclk_0 }]; #IO_L14P_T2_SRCC_34 Sch=ac_bclk #BIT_CLK
set_property src_info {type:XDC file:1 line:103 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN W6    IOSTANDARD LVCMOS33 } [get_ports { ac_dac_sdata_0 }]; #IO_L15P_T2_DQS_34 Sch=ac_dac_sdata #SDATA_OUT
set_property src_info {type:XDC file:1 line:104 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN U5    IOSTANDARD LVCMOS33 } [get_ports { ac_lrclk_0 }]; #IO_L14N_T2_SRCC_34 Sch=ac_lrclk #LRCLK
set_property src_info {type:XDC file:1 line:105 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN U6    IOSTANDARD LVCMOS33 } [get_ports { ac_mclk_0 }]; #IO_L16P_T2_34 Sch=ac_mclk #mclk
set_property src_info {type:XDC file:1 line:109 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN AB22  IOSTANDARD LVCMOS33 } [get_ports { ready_0 }]; #IO_L10N_T1_D15_14 Sch=ja[1]
set_property src_info {type:XDC file:1 line:212 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN W5 IOSTANDARD LVCMOS33} [get_ports scl_0]
set_property src_info {type:XDC file:1 line:213 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN V5 IOSTANDARD LVCMOS33} [get_ports sda_0]
