# Lab 4 - Function Generator

## Author: C2C Jeremy Schwartz

### Last Modified: 3 April 2018

**Documentation:** None

#### Gate Check 1

I am using the Q7.9 format.  This gives me 128 values in my LUT and a precision of 0.001953125 in my phase increment.

Minimum Frequency: (48Kready/sec) * (2^-9) * (1/128) = 0.732421875Hz

Phase Increment = 1 Frequency: (48Kready/sec) * (1/128 ready/cycle) = 375Hz

Maximum Frequency: 48KHz/4 = 12KHz (due to Nyquist effects)

![Schematic](images/schematic.PNG)
##### Figure 1: Schematic

**DISCLAIMER:** I am violating a principle of design by using different signals as clocks.  I am doing this to avoid coding an FSM and acknowledge the risks.

#### Gate Check 2

![GC2 Proof 1](images/GC2_1.PNG)
##### Figure 2: Proof of Phase Increment Change Functionality

![GC2 Proof 2](images/GC2_2.PNG)
##### Figure 3: Proof of Amplitude Adjustment Functionality

**NOTE:** If you look closely at the interpolated output and final output signals, you will notice instability immediately after ready goes high.  This is because it takes an additional clock cycle to update the base value and next value signals after ready goes high.  This instability is not an issue, because the audio codec will sample final output immediately before the instability occurs.

**NOTE:** I am using a multiplier of the form Q0.16.  The number 0x0.FFFF is the highest possible value of 0.9999847412109375, which I decided was close enough to 1.0 for this project.

#### Required Functionality

Required Functionality was demoed to Dr. York at 1050 on 3 April 2018.

#### B Functionality

B Functionality was demoed to Dr. York at 1050 on 3 April 2018.

#### A Functionality

A Functionality was demoed to Dr. York at 1050 on 3 April 2018.

#### Bonus Functionality

Bonus Functionality was demoed to Dr. York at 1030 on 21 March 2018 (Microblaze Lab 4).