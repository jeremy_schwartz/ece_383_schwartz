--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 27 Mar 2018
-- Course: ECE 383
-- File: lab4_dp_tb.vhdl
-- Project: Lab4
--
-- Purp: This file contains the testbench for the Lab 4 datapath.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------ 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.vcomponents.all;
 
ENTITY Lab4_dp_tb IS
END Lab4_dp_tb;
 
ARCHITECTURE behavior OF Lab4_dp_tb IS 

	-- Component Declaration for the Unit Under Test (UUT)

	component lab4_datapath is
	Port(
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	buttons : in STD_LOGIC_VECTOR(4 downto 0);
	switches : in STD_LOGIC_VECTOR(7 downto 0);

	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC);
	end component;

	--Inputs
	signal clk : std_logic := '0';
	signal reset_n : std_logic := '0';
	signal ac_adc_sdata : std_logic := '0';
	signal sda : std_logic := '0';
	signal switches : std_logic_vector(7 downto 0) := "11111111";
	signal buttons : std_logic_vector(4 downto 0) := "00000";

	--Outputs
	signal ac_mclk : std_logic;
	signal ac_dac_sdata : std_logic;
	signal ac_bclk : std_logic;
	signal ac_lrclk : std_logic;
	signal scl : std_logic;

	-- Clock period definitions
	constant clk_period : time := 10 ns;  -- Sets clock to ~ 100MHz

	constant ready_period : time := 20.8333 us;  -- Sets ready to ~ 48KHz

BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: lab4_datapath PORT MAP (
          clk => clk,
          reset_n => reset_n,
  		  ac_mclk => ac_mclk,
          ac_adc_sdata => ac_adc_sdata,
          ac_dac_sdata => ac_dac_sdata,
          ac_bclk => ac_bclk,
          ac_lrclk => ac_lrclk,
          scl => scl,
          sda => sda,
		  buttons => buttons,
          switches => switches);

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
--   ready_process :process
--   begin
--		ready <= '0';
--		wait for ready_period/2;
--		ready <= '1';
--		wait for ready_period/2;
--   end process;
    
    -- Stimulus process
   stim_proc: process 
   begin		
      -- hold reset state for 10 ns.
		reset_n <= '0', '1' after 500 ns;
		buttons <= "00000", "00100" after 10 us, "00000" after 20 us, "00010" after 40 us, "00000" after 50 us, "01000" after 200 us, "00000" after 210 us, "00001" after 230 us, "00000" after 235 us;
        -- buttons <= "00000", "00100" after 10 us, "00000" after 15 us;
		wait;
   end process;

END;
