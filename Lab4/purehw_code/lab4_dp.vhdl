--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 21 Mar 2018
-- Course: ECE 383
-- File: lab4_dp.vhdl
-- Project: Lab4
--
-- Purp: This file implements the datapath for lab 4.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity lab4_datapath is
    Port(
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	buttons : in STD_LOGIC_VECTOR(4 downto 0);
    switches : in STD_LOGIC_VECTOR(7 downto 0);
	
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC);
end lab4_datapath;

architecture structure of lab4_datapath is

component Audio_Codec_Wrapper is
    Port ( clk : in STD_LOGIC;
        reset_n : in STD_LOGIC;
        ac_mclk : out STD_LOGIC;
        ac_adc_sdata : in STD_LOGIC;
        ac_dac_sdata : out STD_LOGIC;
        ac_bclk : out STD_LOGIC;
        ac_lrclk : out STD_LOGIC;
        ready : out STD_LOGIC;
        L_bus_in : in std_logic_vector(17 downto 0); -- left channel input to DAC
        R_bus_in : in std_logic_vector(17 downto 0); -- right channel input to DAC
        L_bus_out : out  std_logic_vector(17 downto 0); -- left channel output from ADC
        R_bus_out : out  std_logic_vector(17 downto 0); -- right channel output from ADC
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC);
end component;

-- Random Signals
signal ready_s, btn_clock, reset_bram : STD_LOGIC;
signal clock_divider: unsigned(22 downto 0);

-- Left Channel
signal leftBaseValue, leftNextValue, leftNextMinusBase, leftBeforeMultiplier, leftFinalOutput : STD_LOGIC_VECTOR(15 downto 0);
signal leftDeltaTimesOffset : STD_LOGIC_VECTOR(25 downto 0);
signal leftMultipliedValue : std_logic_vector(32 downto 0);
signal leftBram1Read, leftBram2Read : STD_LOGIC_VECTOR(9 downto 0);

-- Right Channel
signal rightBaseValue, rightNextValue, rightNextMinusBase, rightBeforeMultiplier, rightFinalOutput : STD_LOGIC_VECTOR(15 downto 0);
signal rightDeltaTimesOffset : STD_LOGIC_VECTOR(25 downto 0);
signal rightMultipliedValue : std_logic_vector(32 downto 0);
signal rightBram1Read, rightBram2Read : STD_LOGIC_VECTOR(9 downto 0);

-- Shared Signals
signal phaseInc, indexDotOffset, sub, add : STD_LOGIC_VECTOR(15 downto 0) := "0000001000000000";
signal memAddr, nextMemAddr : STD_LOGIC_VECTOR(6 downto 0) := "0000000";
signal multiplier : STD_LOGIC_VECTOR(15 downto 0) := x"FFFF";
signal offset : STD_LOGIC_VECTOR(8 downto 0);

-- Final Values
signal leftOut16, rightOut16 : STD_LOGIC_VECTOR(15 downto 0);
signal leftOut18, rightOut18 : STD_LOGIC_VECTOR(17 downto 0);

begin

    leftBram1: BRAM_SDP_MACRO 
        generic map (
        BRAM_SIZE => "18Kb",			-- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",			-- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6", "7SERIES"
                DO_REG => 0, 				-- Optional output register disabled
                INIT => X"000000000000000000",	-- Initial values on output port
                INIT_FILE => "NONE",			-- 
                WRITE_WIDTH => 16,			-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                READ_WIDTH => 16,			-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                SIM_COLLISION_CHECK => "NONE", 	-- Simulation collision check
                SRVAL => X"000000000000000000",	-- Set/Reset value for port output
                INIT_00 => X"55F551334C3F471C41CE3C5636BA30FB2B1F25281F1A18F912C80C8C06480000",
                INIT_01 => X"7FD87F617E9C7D897C297A7C7884764173B570E26DC96A6D66CF62F15ED75A82",
                INIT_02 => X"5ED762F166CF6A6D6DC970E273B5764178847A7C7C297D897E9C7F617FD87FFF",
                INIT_03 => X"06480C8C12C818F91F1A25282B1F30FB36BA3C5641CE471C4C3F513355F55A82",
                INIT_04 => X"AA0BAECDB3C1B8E4BE32C3AAC946CF05D4E1DAD8E0E6E707ED38F374F9B80000",
                INIT_05 => X"8028809F8164827783D78584877C89BF8C4B8F1E9237959399319D0FA129A57E",
                INIT_06 => X"A1299D0F9931959392378F1E8C4B89BF877C858483D782778164809F80288001",
                INIT_07 => X"F9B8F374ED38E707E0E6DAD8D4E1CF05C946C3AABE32B8E4B3C1AECDAA0BA57E")
            port map (
                DO => leftBaseValue,		    -- Output read data port, width defined by READ_WIDTH parameter
                RDADDR => leftBram1Read,	    -- Input address, width defined by port depth
                RDCLK => clk,	 			-- 1-bit input clock
                RST => reset_bram,		    -- active high reset
                RDEN => '1',				-- read enable 
                REGCE => '1',				-- 1-bit input read output register enable - ignored
                DI => x"0000",				-- Dummy write data - never used in this application
                WE => "00",				    -- write to neither byte
                WRADDR => "0000000000",     -- Dummy place holder address
                WRCLK => clk,				-- 1-bit input write clock
                WREN => '0');				-- we are not writing to this RAM
             
    leftBram2: BRAM_SDP_MACRO 
        generic map (
        BRAM_SIZE => "18Kb",            -- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6", "7SERIES"
                DO_REG => 0,                 -- Optional output register disabled
                INIT => X"000000000000000000",    -- Initial values on output port
                INIT_FILE => "NONE",            -- 
        WRITE_WIDTH => 16,            -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        READ_WIDTH => 16,            -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                SIM_COLLISION_CHECK => "NONE",     -- Simulation collision check
                SRVAL => X"000000000000000000",    -- Set/Reset value for port output
                INIT_00 => X"55F551334C3F471C41CE3C5636BA30FB2B1F25281F1A18F912C80C8C06480000",
                INIT_01 => X"7FD87F617E9C7D897C297A7C7884764173B570E26DC96A6D66CF62F15ED75A82",
                INIT_02 => X"5ED762F166CF6A6D6DC970E273B5764178847A7C7C297D897E9C7F617FD87FFF",
                INIT_03 => X"06480C8C12C818F91F1A25282B1F30FB36BA3C5641CE471C4C3F513355F55A82",
                INIT_04 => X"AA0BAECDB3C1B8E4BE32C3AAC946CF05D4E1DAD8E0E6E707ED38F374F9B80000",
                INIT_05 => X"8028809F8164827783D78584877C89BF8C4B8F1E9237959399319D0FA129A57E",
                INIT_06 => X"A1299D0F9931959392378F1E8C4B89BF877C858483D782778164809F80288001",
                INIT_07 => X"F9B8F374ED38E707E0E6DAD8D4E1CF05C946C3AABE32B8E4B3C1AECDAA0BA57E")
            port map (
                DO => leftNextValue,            -- Output read data port, width defined by READ_WIDTH parameter
                RDADDR => leftBram2Read,            -- Input address, width defined by port depth
                RDCLK => clk,                 -- 1-bit input clock
                RST => reset_bram,                -- active high reset
                RDEN => '1',                -- read enable 
                REGCE => '1',                -- 1-bit input read output register enable - ignored
                DI => x"0000",                -- Dummy write data - never used in this application
                WE => "00",                    -- write to neither byte
                WRADDR => "0000000000",        -- Dummy place holder address
                WRCLK => clk,                -- 1-bit input write clock
                WREN => '0');                -- we are not writing to this RAM
				
	rightBram1: BRAM_SDP_MACRO 
        generic map (
        BRAM_SIZE => "18Kb",			-- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",			-- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6", "7SERIES"
                DO_REG => 0, 				-- Optional output register disabled
                INIT => X"000000000000000000",	-- Initial values on output port
                INIT_FILE => "NONE",			-- 
                WRITE_WIDTH => 16,			-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                READ_WIDTH => 16,			-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                SIM_COLLISION_CHECK => "NONE", 	-- Simulation collision check
                SRVAL => X"000000000000000000",	-- Set/Reset value for port output
                INIT_00 => X"8001800180018001800180018001800180018001800180018001800180018001",
                INIT_01 => X"8001800180018001800180018001800180018001800180018001800180018001",
                INIT_02 => X"8001800180018001800180018001800180018001800180018001800180018001",
                INIT_03 => X"8001800180018001800180018001800180018001800180018001800180018001",
                INIT_04 => X"7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF",
                INIT_05 => X"7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF",
                INIT_06 => X"7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF",
                INIT_07 => X"7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF")
            port map (
                DO => rightBaseValue,		    -- Output read data port, width defined by READ_WIDTH parameter
                RDADDR => rightBram1Read,	    -- Input address, width defined by port depth
                RDCLK => clk,	 			-- 1-bit input clock
                RST => reset_bram,		    -- active high reset
                RDEN => '1',				-- read enable 
                REGCE => '1',				-- 1-bit input read output register enable - ignored
                DI => x"0000",				-- Dummy write data - never used in this application
                WE => "00",				    -- write to neither byte
                WRADDR => "0000000000",     -- Dummy place holder address
                WRCLK => clk,				-- 1-bit input write clock
                WREN => '0');				-- we are not writing to this RAM
             
    rightBram2: BRAM_SDP_MACRO 
        generic map (
        BRAM_SIZE => "18Kb",            -- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6", "7SERIES"
                DO_REG => 0,                 -- Optional output register disabled
                INIT => X"000000000000000000",    -- Initial values on output port
                INIT_FILE => "NONE",            -- 
        WRITE_WIDTH => 16,            -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        READ_WIDTH => 16,            -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                SIM_COLLISION_CHECK => "NONE",     -- Simulation collision check
                SRVAL => X"000000000000000000",    -- Set/Reset value for port output
                INIT_00 => X"8001800180018001800180018001800180018001800180018001800180018001",
                INIT_01 => X"8001800180018001800180018001800180018001800180018001800180018001",
                INIT_02 => X"8001800180018001800180018001800180018001800180018001800180018001",
                INIT_03 => X"8001800180018001800180018001800180018001800180018001800180018001",
                INIT_04 => X"7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF",
                INIT_05 => X"7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF",
                INIT_06 => X"7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF",
                INIT_07 => X"7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF")
            port map (
                DO => rightNextValue,            -- Output read data port, width defined by READ_WIDTH parameter
                RDADDR => rightBram2Read,            -- Input address, width defined by port depth
                RDCLK => clk,                 -- 1-bit input clock
                RST => reset_bram,                -- active high reset
                RDEN => '1',                -- read enable 
                REGCE => '1',                -- 1-bit input read output register enable - ignored
                DI => x"0000",                -- Dummy write data - never used in this application
                WE => "00",                    -- write to neither byte
                WRADDR => "0000000000",        -- Dummy place holder address
                WRCLK => clk,                -- 1-bit input write clock
                WREN => '0');                -- we are not writing to this RAM

    audio_codec: Audio_Codec_Wrapper port map( 
        clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk, 
        ready => ready_s,
        L_bus_in => leftOut18,
        R_bus_in => rightOut18,
        L_bus_out => OPEN,
        R_bus_out => OPEN,
        scl => scl,
        sda => sda);
        
    clock_divider_process: process(clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                clock_divider <= to_unsigned(0, 23);
            else
                clock_divider <= clock_divider + to_unsigned(1, 23);
            end if;
        end if;
    end process;

    button_addition_process: process(btn_clock)
    begin
        if rising_edge(btn_clock) then
            if reset_n = '0' then
                phaseInc <= "0000001000000000"; -- 0000001.000000000 Increment
                multiplier <= x"FFFF";
            elsif buttons(4) = '1'  then -- Center
                phaseInc <= "0000001000000000"; -- 0000001.000000000 Increment
                multiplier <= x"FFFF";
            elsif (buttons(1) = '1') and (sub(15) = '0') then -- Right
                phaseInc <= sub;
            elsif (buttons(2) = '1') then -- Left
                if phaseInc(15) = '1' then
                    if phaseInc(15) = add(15) then
                        phaseInc <= add;
                    end if;
                else
                    phaseInc <= add;
                end if;
            elsif (buttons(0) = '1') then -- Up
                multiplier <= std_logic_vector(unsigned(multiplier) + unsigned("00" & switches));
            elsif (buttons(3) = '1') then -- Down
                multiplier <= std_logic_vector(unsigned(multiplier) - unsigned("00" & switches));
            end if;
        end if;
    end process;
    
   address_process: process(ready_s)
    begin
        if rising_edge(ready_s) then
            indexDotOffset <= std_logic_vector(unsigned(indexDotOffset) + unsigned(phaseInc));
        end if;
    end process;
		
-- Random Logic
btn_clock <= clock_divider(22); -- clock_divider(8);
reset_bram <= not(reset_n);

-- Final Logic
leftOut18 <= leftOut16 & "00";
rightOut18 <= rightOut16 & "00";

-- Shared Logic
offset <= indexDotOffset(8 downto 0);
memAddr <= indexDotOffset(15 downto 9);
nextMemAddr <= std_logic_vector(unsigned(memAddr) + to_unsigned(1, 7));
sub <= std_logic_vector(unsigned(phaseInc) - unsigned(switches));
add <= std_logic_vector(unsigned(phaseInc) + unsigned(switches));

-- Left Logic
leftBram1Read <= "000" & memAddr;
leftBram2Read <= "000" & nextMemAddr;

leftNextMinusBase <= std_logic_vector(signed(leftNextValue) - signed(leftBaseValue));
leftDeltaTimesOffset <= std_logic_vector(signed(leftNextMinusBase) * signed("0" & offset));
leftBeforeMultiplier <=  std_logic_vector(signed(leftDeltaTimesOffset(24 downto 9)) + signed(leftBaseValue));
leftMultipliedValue <= std_logic_vector(signed(leftBeforeMultiplier) * signed("0" & multiplier));
leftFinalOutput <= leftMultipliedValue(31 downto 16);
leftOut16 <= leftFinalOutput;

-- Right Logic
rightBram1Read <= "000" & memAddr;
rightBram2Read <= "000" & nextMemAddr;

rightNextMinusBase <= std_logic_vector(signed(rightNextValue) - signed(rightBaseValue));
rightDeltaTimesOffset <= std_logic_vector(signed(rightNextMinusBase) * signed("0" & offset));
rightBeforeMultiplier <=  std_logic_vector(signed(rightDeltaTimesOffset(24 downto 9)) + signed(rightBaseValue));
rightMultipliedValue <= std_logic_vector(signed(rightBeforeMultiplier) * signed("0" & multiplier));
rightFinalOutput <= rightMultipliedValue(31 downto 16);
rightOut16 <= rightFinalOutput;

end structure;