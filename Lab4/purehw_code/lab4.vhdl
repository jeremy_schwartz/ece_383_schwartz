--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 21 Mar 2018
-- Course: ECE 383
-- File: lab4.vhdl
-- Project: Lab4
--
-- Purp: This file contains the top level for Lab 4.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;	

entity lab4 is
    Port (
        clk : in  STD_LOGIC;
        reset_n : in  STD_LOGIC;
        buttons : in STD_LOGIC_VECTOR(4 downto 0);
        switches : in STD_LOGIC_VECTOR(7 downto 0);
        
        ac_mclk : out STD_LOGIC;
        ac_adc_sdata : in STD_LOGIC;
        ac_dac_sdata : out STD_LOGIC;
        ac_bclk : out STD_LOGIC;
        ac_lrclk : out STD_LOGIC;
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC);
end lab4;

architecture behavior of lab4 is

	component lab4_datapath is
        Port(
        clk : in  STD_LOGIC;
        reset_n : in  STD_LOGIC;
        buttons : in STD_LOGIC_VECTOR(4 downto 0);
        switches : in STD_LOGIC_VECTOR(7 downto 0);
        
        ac_mclk : out STD_LOGIC;
        ac_adc_sdata : in STD_LOGIC;
        ac_dac_sdata : out STD_LOGIC;
        ac_bclk : out STD_LOGIC;
        ac_lrclk : out STD_LOGIC;
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC);
    end component;

begin

	datapath: lab4_datapath port map(
        clk => clk,
        reset_n => reset_n,
        buttons => buttons,
        switches => switches,
    
        ac_mclk => ac_mclk,
        ac_adc_sdata=> ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        scl => scl,
        sda => sda);
        
end behavior;
