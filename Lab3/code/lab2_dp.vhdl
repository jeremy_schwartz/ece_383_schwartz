--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 12 Feb 2018
-- Course: ECE 383
-- File: lab2_dp.vhdl
-- Project: Lab2
--
-- Purp: This file implements the datapath for lab 2.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use work.lab2Parts.all;

entity lab2_datapath is
    Port(
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC;	
	tmds : out  STD_LOGIC_VECTOR (3 downto 0);
	tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
	sw: out std_logic_vector(2 downto 0);
	switch: in std_logic_vector(3 downto 0);
	cw: in std_logic_vector (2 downto 0);
	btn: in	STD_LOGIC_VECTOR(4 downto 0);
	exWrAddr: in std_logic_vector(9 downto 0);
	exWen, exSel: in std_logic;
	Lbus_out, Rbus_out: out std_logic_vector(15 downto 0);
	exLbus, exRbus: in std_logic_vector(15 downto 0);
	flagQ: out std_logic_vector(7 downto 0);
	flagClear: in std_logic_vector(7 downto 0);
	ready: out std_logic;
	trigger_time, trigger_volt: in std_logic_vector(9 downto 0));
end lab2_datapath;

architecture structure of lab2_datapath is

	signal trigger_time_s, trigger_volt_s, int_trigger_time_s, int_trigger_volt_s, row, column, write_cntr: unsigned(9 downto 0);
	signal ch1_wave, ch2_wave, trigger_clock, ready_s, LR_write_enable, L_bus_g, L_bus_l, R_bus_g, R_bus_l, not_reset, count_cmp: std_logic;
	signal clock_divider: unsigned(22 downto 0);
	signal L_bus_in_s, L_bus_out_s, R_bus_in_s, R_bus_out_s: std_logic_vector(17 downto 0);

    signal L_unsigned_data_18: std_logic_vector(17 downto 0);
	signal L_into_bram, L_out_bram, LR_write_address, LR_read_address, L_unsigned_data, L_unsigned_data_prev: std_logic_vector(9 downto 0);
	
	signal R_unsigned_data_18: std_logic_vector(17 downto 0);
    signal R_into_bram, R_out_bram, R_unsigned_data, R_unsigned_data_prev: std_logic_vector(9 downto 0);

begin

    clock_divider_process: process(clk)
    begin
        if rising_edge(clk) then
            clock_divider <= clock_divider + to_unsigned(1, 23);
        end if;
    end process;

    voltage_trigger_process: process(trigger_clock)
    begin
        if rising_edge(trigger_clock) then
            if reset_n = '0' then
                int_trigger_volt_s <= to_unsigned(200, 10);
             elsif btn(0) = '1' and int_trigger_volt_s > 0 then
                int_trigger_volt_s <= int_trigger_volt_s - to_unsigned(1, 10);
              elsif btn(2) = '1' and int_trigger_volt_s < 400 then
                int_trigger_volt_s <= int_trigger_volt_s + to_unsigned(1, 10);
              elsif btn(4) = '1' then
                int_trigger_volt_s <= to_unsigned(200, 10);
             end if;
        end if;
    end process;
    
    time_trigger_process: process(trigger_clock)
    begin
        if rising_edge(trigger_clock) then
            if reset_n = '0' then
                int_trigger_time_s <= to_unsigned(300, 10);
             elsif btn(1) = '1' and trigger_time_s > 0 then
                int_trigger_time_s <= int_trigger_time_s - to_unsigned(1, 10);
              elsif btn(3) = '1'  and int_trigger_time_s < 600 then
                int_trigger_time_s <= int_trigger_time_s + to_unsigned(1, 10);
              elsif btn(4) = '1' then
                int_trigger_time_s <= to_unsigned(300, 10);
             end if;
        end if;
    end process;

	video_inst: video port map( 
		clk => clk,
		reset_n => reset_n,
		tmds => tmds,
		tmdsb => tmdsb,
		trigger_time => trigger_time_s,
		trigger_volt => trigger_volt_s,
		row => row, 
		column => column,
		ch1 => ch1_wave,
		ch1_enb => switch(0),
		ch2 => ch2_wave,
		ch2_enb => switch(1)); 
		
    audio_codec: Audio_Codec_Wrapper port map( 
        clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk, 
        ready => ready_s,
        L_bus_in => L_bus_in_s,
        R_bus_in => R_bus_in_s,
        L_bus_out => L_bus_out_s,
        R_bus_out => R_bus_out_s,
        scl => scl,
        sda => sda);
        
    sampCounter: sample_counter port map (        
            cntr_clk => clk,
            reset => reset_n,
            ctrl => cw(2 downto 1),
            sample_count => write_cntr);
            
    flagReg: flagRegister port map (        
                    clk => clk,
                    reset_n => reset_n,
                    set(7 downto 3) => "00000",
                    set(2) => count_cmp,
                    set(1) => '0', -- SHOULD BE VSYNC
                    set(0) => ready_s,
                    clear => flagClear,
                    Q => flagQ);
        
    lBRAM: BRAM_SDP_MACRO
        generic map (
            BRAM_SIZE => "18Kb",                     -- Target BRAM, "18Kb" or "36Kb"
            DEVICE => "7SERIES",                     -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
            DO_REG => 0,                             -- Optional output register disabled
            INIT => X"0000000000",                  -- Initial values on output port
            INIT_FILE => "NONE",                    -- Not sure how to initialize the RAM from a file
            WRITE_WIDTH => 10,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
            READ_WIDTH => 10,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
            SIM_COLLISION_CHECK => "NONE",             -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
            SRVAL => X"0000000000")                  -- Set/Reset value for port output
        port map (
            DO => L_out_bram,                        -- Output read data port, width defined by READ_WIDTH parameter
            RDADDR => LR_read_address,      -- Input address, width defined by port depth
            RDCLK => clk,                             -- 1-bit input clock
            RST => not_reset,                            -- active high reset
            RDEN => '1',                            -- read enable 
            REGCE => '1',                            -- 1-bit input read output register enable - ignored
            DI => L_into_bram,                        -- Input data port, width defined by WRITE_WIDTH parameter
            WE => "11",                               -- since RAM is byte read, this determines high or low byte
            WRADDR => LR_write_address,                    -- Input write address, width defined by write port depth
            WRCLK => clk,                            -- 1-bit input write clock
            WREN => LR_write_enable);                             -- 1-bit input write port enable
            
    rBRAM: BRAM_SDP_MACRO
        generic map (
            BRAM_SIZE => "18Kb",                     -- Target BRAM, "18Kb" or "36Kb"
            DEVICE => "7SERIES",                     -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
            DO_REG => 0,                             -- Optional output register disabled
            INIT => X"0000000000",                  -- Initial values on output port
            INIT_FILE => "NONE",                    -- Not sure how to initialize the RAM from a file
            WRITE_WIDTH => 10,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
            READ_WIDTH => 10,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
            SIM_COLLISION_CHECK => "NONE",             -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
            SRVAL => X"0000000000")                  -- Set/Reset value for port output
        port map (
            DO => R_out_bram,                        -- Output read data port, width defined by READ_WIDTH parameter
            RDADDR => LR_read_address,      -- Input address, width defined by port depth
            RDCLK => clk,                             -- 1-bit input clock
            RST => not_reset,                            -- active high reset
            RDEN => '1',                            -- read enable 
            REGCE => '1',                            -- 1-bit input read output register enable - ignored
            DI => R_into_bram,                        -- Input data port, width defined by WRITE_WIDTH parameter
            WE => "11",                               -- since RAM is byte read, this determines high or low byte
            WRADDR => LR_write_address,                    -- Input write address, width defined by write port depth
            WRCLK => clk,                            -- 1-bit input write clock
            WREN => LR_write_enable);                             -- 1-bit input write port enable
               
    process (clk)
        begin
        if (rising_edge(clk)) then
            if reset_n = '0' then
                L_bus_in_s <= (others => '0');
                R_bus_in_s <= (others => '0');                
            elsif(ready_s = '1') then
                L_bus_in_s <= L_bus_out_s;
                R_bus_in_s <= R_bus_out_s;
            end if;
        end if;
    end process;
    
    process (ready_s)
        begin
        if (rising_edge(ready_s)) then
            L_unsigned_data_prev <= L_unsigned_data;
            R_unsigned_data_prev <= R_unsigned_data;
        end if;
    end process;
		
	-- Trigger Clock Signal
    trigger_clock <= clock_divider(21) when switch(2) = '1' else clock_divider(22);
    
    -- Write Enable For Both BRAMs
    LR_write_enable <= cw(0) when exSel = '0' else exWen;
    
    -- Addresses for both BRAMs
    LR_read_address <= std_logic_vector(column - 20);
    LR_write_address <= STD_LOGIC_VECTOR(write_cntr) when exSel = '0' else exWrAddr;

    -- Left Signals
	L_unsigned_data <= std_logic_vector(unsigned("0" & L_unsigned_data_18(17 downto 9)) - 56);
	L_into_bram <= L_unsigned_data when exSel = '0' else exLBus(9 downto 0);
	ch1_wave <= '1' when (std_logic_vector(unsigned(L_out_bram) + 20) = std_logic_vector(row)) else '0';
    L_unsigned_data_18 <= std_logic_vector(unsigned(L_bus_out_s) + to_unsigned(131072 , 18));

	-- Right Signals
    R_unsigned_data <= std_logic_vector(unsigned("0" & R_unsigned_data_18(17 downto 9)) - 56);
    R_into_bram <= R_unsigned_data when exSel = '0' else exRBus(9 downto 0);
    ch2_wave <= '1' when (std_logic_vector(unsigned(R_out_bram) + 20) = std_logic_vector(row)) else '0';
    R_unsigned_data_18 <= std_logic_vector(unsigned(R_bus_out_s) + to_unsigned(131072 , 18));

    -- Triggering Signal
	L_bus_g <= '1' when (unsigned(L_unsigned_data) >= trigger_volt_s) else '0';
	L_bus_l <= '1' when (unsigned(L_unsigned_data_prev) <= trigger_volt_s) else '0';
    R_bus_g <= '1' when (unsigned(R_unsigned_data) >= trigger_volt_s) else '0';
    R_bus_l <= '1' when (unsigned(R_unsigned_data_prev) <= trigger_volt_s) else '0';
    sw(2) <= (L_bus_g and L_bus_l) when switch(3) = '0' else (R_bus_g and R_bus_l);
    
    trigger_volt_s <= int_trigger_volt_s when exSel = '0' else unsigned(trigger_volt);
    trigger_time_s <= int_trigger_time_s when exSel = '0' else unsigned(trigger_time);
	
	-- External Connections
	Lbus_out <= L_bus_out_s(17 downto 2);
	Rbus_out <= R_bus_out_s(17 downto 2);
	ready <= ready_s;
	
	-- Misc
	not_reset <= not reset_n;
	count_cmp <= '1' when write_cntr = x"3FF" else '0';
	sw(0) <= ready_s;
	sw(1) <= count_cmp;
	
end structure;
