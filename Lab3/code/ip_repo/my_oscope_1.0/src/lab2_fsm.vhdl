--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 12 Feb 2018
-- Course: ECE 383
-- File: lab2_fsm.vhdl
-- Project: Lab2
--
-- Purp: This file implements the sample capture control state machine for lab 2.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

-- sw(2) = Triggered
-- sw(1) = Done
-- sw(0) = Ready

library IEEE;		
use IEEE.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

entity lab2_fsm is
	Port(	clk: in  STD_LOGIC;
            reset_n : in  STD_LOGIC;
            cw: out STD_LOGIC_VECTOR(2 downto 0);
            sw: in STD_LOGIC_VECTOR(2 downto 0));

end lab2_fsm;

architecture behavior of lab2_fsm is

	type state_type is (waitTrigger, storeSample, waitSample);
	signal state: state_type;
	
begin
	
    state_process: process(clk)
    begin
        if rising_edge(clk) then
            if (reset_n = '0') then 
                state <= waitTrigger;
            else
                case state is
                when waitTrigger =>
                    if sw(2) = '1' then
                        state <= storeSample;
                    end if;
                when storeSample =>
                    if sw(0) = '0' and sw(1) = '0' then
                        state <= waitSample;
                    end if;   
                when waitSample =>
                    if sw(0) = '1' then
                        state <= storeSample;
                    elsif sw(0) = '0' and sw(1) = '1' then
                        state <= waitTrigger;
                    end if;          
                end case;
            end if;
        end if;
    end process;

-- cw(2 downto 1) = Counter Control
-- cw(0) = Write Enable

cw <=   "110" when state = waitTrigger else
        "011" when state = storeSample else
        "000" when state = waitSample else 
        "000"; -- Catch All

end behavior;