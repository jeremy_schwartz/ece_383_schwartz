/*--------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	6 Mar 2018
-- File:	main.c
-- Event:	Lab3
-- Crs:		ECE 383
--
-- Purp:	Implement Lab3 Microblaze O-Scope functionality.
--
-- Documentation:	None
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"

#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out16 and its variations
#include <xil_exception.h>

/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for our project.
 */
#define base_reg				0x44a00000
#define exWrAddr_reg			base_reg			// slv_reg0
#define	exLbus_reg				base_reg+4			// slv_reg1
#define	exRbus_reg				base_reg+8			// slv_reg2
#define	flagClear_reg			base_reg+12		// slv_reg3
#define	exWen_reg				base_reg+16		// slv_reg4
#define	exSel_reg				base_reg+20		// slv_reg5
#define	Lbus_out_reg			base_reg+24		// slv_reg6
#define	Rbus_out_reg			base_reg+28		// slv_reg7
#define	flagQ_reg				base_reg+32		// slv_reg8
#define	ready_reg				base_reg+36		// slv_reg9
#define	trigger_time_reg		base_reg+40		// slv_reg10
#define	trigger_volt_reg		base_reg+44		// slv_reg11

#define printf xil_printf			/* A smaller footprint printf */

#define	uartRegAddr			0x40600000		// read <= RX, write => TX

/************************** Function Prototypes ****************************/
void myISR(void);

/************************** Variable Definitions **************************/
/*
 * The following are declared globally so they are zeroed and so they are
 * easily accessible from a debugger
 */

u16 exWrAddr = 0; //r0
u16 exLbus = 0; //r1
u16 exRbus = 0; //r2
u16 flagClear = 0; //r3
u16 exWen = 0; //r4
u16 exSel = 0; //r5
u16 trigger_time = 300; //r10
u16 trigger_volt = 200; //r11

u16 Lbus_out = 0; //r6
u16 Rbus_out = 0; //r7
u16 flagQ = 0; //r8
u16 ready = 0; //r9

u16 right_sig = 0;
u16 right_sig_prev = 0;
u16 left_sig = 0;
u16 left_sig_prev = 0;

u16 left_array[2048];
u16 right_array[2048];
u16 array_index = 0;
u16 trigger_found = 0; // 0 = not found, 1 = found
u16 trigger_location = 0;
u16 trig_chan = 0; // 0 = left, 1 = right

int main(void) {

	unsigned char c;

	init_platform();

	print("Welcome to Lab3\n\r");

    microblaze_register_handler((XInterruptHandler) myISR, (void *) 0);
    microblaze_enable_interrupts();

	Xil_Out16(exSel_reg, 0x00);
	Xil_Out16(trigger_volt_reg, trigger_volt);
	Xil_Out16(trigger_time_reg, trigger_time);


    while(1) {

    	c = XUartLite_RecvByte(uartRegAddr);

		switch(c) {

    		/*-------------------------------------------------
    		 * Reply with the help menu
    		 *-------------------------------------------------
			 */
    		case '?':
    			printf("--------------------------\r\n");
    			printf("Lbus_out = %x\r\n",Lbus_out);
    			printf("Rbus_out = %x\r\n",Rbus_out);
    			printf("flagQ = %x\r\n",Xil_In16(flagQ_reg));
    			printf("ready = %x\r\n",Xil_In16(ready_reg));
    			printf("--------------------------\r\n");
    			printf("?: help menu\r\n");
    			printf("wasd: trigger control\r\n");
    			printf("r: reset triggers\r\n");
    			printf("e: go to external control\r\n");
    			printf("i: got to internal control\r\n");
    			printf("q: change trigger channel\r\n");
    			break;


			/*-------------------------------------------------
			 * Volt up
			 *-------------------------------------------------
			 */
			case 'w':
				if (trigger_volt > 0) {
					trigger_volt -= 10;
					printf("%d\r\n",trigger_volt);
				}
				Xil_Out16(trigger_volt_reg, trigger_volt);
				break;

			/*-------------------------------------------------
			 * Time left
			 *-------------------------------------------------
			 */
			case 'a':
				if (trigger_time > 0) {
					trigger_time -= 10;
					printf("%d\r\n",trigger_time);
				}
				Xil_Out16(trigger_time_reg, trigger_time);
				break;

			/*-------------------------------------------------
			 * Volt down
			 *-------------------------------------------------
			 */
			case 's':
				if (trigger_volt < 400) {
					trigger_volt += 10;
					printf("%d\r\n",trigger_volt);
				}
				Xil_Out16(trigger_volt_reg, trigger_volt);
				break;

			/*-------------------------------------------------
			 * Time Right
			 *-------------------------------------------------
			 */
			case 'd':
				if (trigger_time < 600) {
					trigger_time += 10;
					printf("%d\r\n",trigger_time);
				}
				Xil_Out16(trigger_time_reg, trigger_time);
				break;

			/*-------------------------------------------------
			 * Reset triggers
			 *-------------------------------------------------
			 */
			case 'r':
				trigger_time = 300;
				trigger_volt = 200;
				Xil_Out16(trigger_time_reg, trigger_time);
				Xil_Out16(trigger_volt_reg, trigger_volt);
				break;

			/*-------------------------------------------------
			 * Go to external control
			 *-------------------------------------------------
			 */
			case 'e':
				Xil_Out16(exSel_reg, 0x01);
				break;

			/*-------------------------------------------------
			 * Go to internal control
			 *-------------------------------------------------
			 */
			case 'i':
				Xil_Out16(exSel_reg, 0x00);
				break;


			/*-------------------------------------------------
			 * Clear the terminal window
			 *-------------------------------------------------
			 */
            case 'f':
            	for (c=0; c<40; c++) printf("\r\n");
               	break;

			/*-------------------------------------------------
			 * Switch Trigger Channel
			 *-------------------------------------------------
			 */
			case 'q':
				if (trig_chan == 0) {
					trig_chan = 1;
				} else {
					trig_chan = 0;
				}
				break;

			/*-------------------------------------------------
			 * Unknown character was
			 *-------------------------------------------------
			 */
    		default:
    			printf("unrecognized character: %c\r\n",c);
    			break;
    	} // end case

    } // end while 1

    cleanup_platform();

    return 0;
} // end main


void myISR(void) {
	left_sig_prev = left_sig;
	right_sig_prev = right_sig;
	Lbus_out = Xil_In16(Lbus_out_reg);
	Rbus_out = Xil_In16(Rbus_out_reg);
	left_sig = Lbus_out;
	right_sig = Rbus_out;
	left_sig ^= 1UL << 15;
	right_sig ^= 1UL << 15;
	left_sig = (left_sig >> 7);
	right_sig = (right_sig >> 7);
	left_sig -= 56;
	right_sig -= 56;

	left_array[array_index] = left_sig;
	right_array[array_index] = right_sig;

	if (((!trigger_found) && (array_index > trigger_time)) && (((trig_chan == 0) && (left_sig <= trigger_volt) && (left_sig_prev >= trigger_volt)) || ((trig_chan == 1) && (right_sig <= trigger_volt) && (right_sig_prev >= trigger_volt)))) {
		trigger_location = array_index;
		trigger_found = 1;
	}

	if ((array_index >= trigger_location + 600) && (trigger_found)) {
		for (int j = 0; j < 600; j++) {
			Xil_Out16(exWrAddr_reg, j);
			Xil_Out16(exWen_reg, 0x01);
			Xil_Out16(exLbus_reg, left_array[trigger_location + j - trigger_time]);
			Xil_Out16(exRbus_reg, right_array[trigger_location + j - trigger_time]);
			Xil_Out16(exWen_reg, 0x00);
		}
		trigger_found = 0;
		array_index = 0;
	}

	if (array_index < 2047) {
		array_index++;
	} else {
		array_index = 0;
		trigger_found = 0;
	}
}
