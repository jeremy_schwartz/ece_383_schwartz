--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 12 Feb 2018
-- Course: ECE 383
-- File: flagRegister.vhdl
-- Project: Lab1
--
-- Purp: This file implements the flag register for Lab 2.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity flagRegister is
	Generic (N: integer := 8);
	Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			set, clear: in std_logic_vector(N-1 downto 0);
			Q: out std_logic_vector(N-1 downto 0));
end flagRegister;

architecture behavior of flagRegister is

    begin
        process(clk)
        begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                Q <= (others => '0');
            else
                for i in 0 to N-1 loop
                    if (set(i) = '1' and clear(i) = '0') then
                        Q(i) <= '1';
                    elsif (set(i) = '0' and clear(i) = '1') then
                        Q(i) <= '0';
                    end if;
                end loop;
            end if;
        end if;
    end process;
    
end behavior;