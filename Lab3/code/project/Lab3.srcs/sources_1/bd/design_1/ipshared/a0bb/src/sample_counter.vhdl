--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 12 Feb 2018
-- Course: ECE 383
-- File: sample_counter.vhdl
-- Project: Lab2
--
-- Purp: This file implements a 10 bit counter.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity sample_counter is
        port(	cntr_clk, reset: in std_logic; 
                ctrl: in std_logic_vector(1 downto 0);
                sample_count: out unsigned(9 downto 0));
end sample_counter;

architecture behavior of sample_counter is
    signal processQ: unsigned (9 downto 0);

    begin
        process(cntr_clk)
        begin
        if (rising_edge(cntr_clk)) then
            if (reset = '0') then
                processQ <= (others => '0');
            elsif ((processQ < 1023) and (ctrl = "01")) then
                processQ <= processQ + 1;
            elsif ((processQ = 1023) and (ctrl = "01")) then
                processQ <= (others => '0');
			elsif (ctrl = "11") then
				processQ <= (others => '0');
            end if;
        end if;
    end process;

    sample_count <= processQ;

end behavior;