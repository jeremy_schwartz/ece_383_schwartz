// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.3 (win64) Build 2018833 Wed Oct  4 19:58:22 MDT 2017
// Date        : Sun Mar  4 16:12:23 2018
// Host        : C19JMSCHWARTZ running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_my_oscope_0_0_sim_netlist.v
// Design      : design_1_my_oscope_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tsbg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper
   (ac_mclk,
    ac_bclk,
    BCLK_int_reg,
    ac_lrclk,
    CLK,
    sw,
    \R_bus_in_s_reg[17] ,
    D,
    \L_bus_in_s_reg[17] ,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    ac_dac_sdata,
    scl,
    sda,
    clk,
    ac_adc_sdata,
    reset_n,
    Q,
    \L_bus_in_s_reg[17]_0 ,
    CO,
    switch,
    \trigger_volt_s_reg[9] ,
    \trigger_volt_s_reg[9]_0 ,
    lopt);
  output ac_mclk;
  output ac_bclk;
  output BCLK_int_reg;
  output ac_lrclk;
  output CLK;
  output [0:0]sw;
  output [17:0]\R_bus_in_s_reg[17] ;
  output [6:0]D;
  output [17:0]\L_bus_in_s_reg[17] ;
  output [6:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  output ac_dac_sdata;
  inout scl;
  inout sda;
  input clk;
  input ac_adc_sdata;
  input reset_n;
  input [17:0]Q;
  input [17:0]\L_bus_in_s_reg[17]_0 ;
  input [0:0]CO;
  input [0:0]switch;
  input [0:0]\trigger_volt_s_reg[9] ;
  input [9:0]\trigger_volt_s_reg[9]_0 ;
  output lopt;

  wire BCLK_int_reg;
  wire CLK;
  wire [0:0]CO;
  wire [6:0]D;
  wire [17:0]\L_bus_in_s_reg[17] ;
  wire [17:0]\L_bus_in_s_reg[17]_0 ;
  wire [17:0]Q;
  wire [17:0]\R_bus_in_s_reg[17] ;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_lrclk_count0;
  wire [3:0]ac_lrclk_count_reg__0;
  wire ac_lrclk_sig_prev_reg_n_0;
  wire ac_mclk;
  wire audio_inout_n_3;
  wire audio_inout_n_5;
  wire audio_inout_n_6;
  wire clk;
  wire clk_50;
  wire lopt;
  wire [3:0]plusOp;
  wire reset_n;
  wire scl;
  wire sda;
  wire [6:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire [0:0]sw;
  wire [0:0]switch;
  wire [0:0]\trigger_volt_s_reg[9] ;
  wire [9:0]\trigger_volt_s_reg[9]_0 ;

  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \ac_lrclk_count[0]_i_1 
       (.I0(ac_lrclk_count_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \ac_lrclk_count[1]_i_1 
       (.I0(ac_lrclk_count_reg__0[0]),
        .I1(ac_lrclk_count_reg__0[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \ac_lrclk_count[2]_i_1 
       (.I0(ac_lrclk_count_reg__0[2]),
        .I1(ac_lrclk_count_reg__0[1]),
        .I2(ac_lrclk_count_reg__0[0]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \ac_lrclk_count[3]_i_3 
       (.I0(ac_lrclk_count_reg__0[3]),
        .I1(ac_lrclk_count_reg__0[0]),
        .I2(ac_lrclk_count_reg__0[1]),
        .I3(ac_lrclk_count_reg__0[2]),
        .O(plusOp[3]));
  FDRE \ac_lrclk_count_reg[0] 
       (.C(clk),
        .CE(ac_lrclk_count0),
        .D(plusOp[0]),
        .Q(ac_lrclk_count_reg__0[0]),
        .R(audio_inout_n_3));
  FDRE \ac_lrclk_count_reg[1] 
       (.C(clk),
        .CE(ac_lrclk_count0),
        .D(plusOp[1]),
        .Q(ac_lrclk_count_reg__0[1]),
        .R(audio_inout_n_3));
  FDRE \ac_lrclk_count_reg[2] 
       (.C(clk),
        .CE(ac_lrclk_count0),
        .D(plusOp[2]),
        .Q(ac_lrclk_count_reg__0[2]),
        .R(audio_inout_n_3));
  FDRE \ac_lrclk_count_reg[3] 
       (.C(clk),
        .CE(ac_lrclk_count0),
        .D(plusOp[3]),
        .Q(ac_lrclk_count_reg__0[3]),
        .R(audio_inout_n_3));
  FDRE ac_lrclk_sig_prev_reg
       (.C(clk),
        .CE(1'b1),
        .D(audio_inout_n_5),
        .Q(ac_lrclk_sig_prev_reg_n_0),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl audio_inout
       (.CLK(CLK),
        .CO(CO),
        .D(D),
        .E(ac_lrclk_count0),
        .\L_bus_in_s_reg[17] (\L_bus_in_s_reg[17] ),
        .\L_bus_in_s_reg[17]_0 (\L_bus_in_s_reg[17]_0 ),
        .Q(ac_lrclk_count_reg__0),
        .\R_bus_in_s_reg[17] (\R_bus_in_s_reg[17] ),
        .\R_bus_in_s_reg[17]_0 (Q),
        .SR(BCLK_int_reg),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .\ac_lrclk_count_reg[0] (audio_inout_n_3),
        .ac_lrclk_sig_prev_reg(audio_inout_n_5),
        .ac_lrclk_sig_prev_reg_0(ac_lrclk_sig_prev_reg_n_0),
        .clk(clk),
        .ready_sig_reg(audio_inout_n_6),
        .reset_n(reset_n),
        .\sdp_bl.ramb18_dp_bl.ram18_bl (\sdp_bl.ramb18_dp_bl.ram18_bl ),
        .sw(sw),
        .switch(switch),
        .\trigger_volt_s_reg[9] (\trigger_volt_s_reg[9] ),
        .\trigger_volt_s_reg[9]_0 (\trigger_volt_s_reg[9]_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1 audiocodec_master_clock
       (.clk_in1(clk),
        .clk_out1(ac_mclk),
        .clk_out2(clk_50),
        .lopt(lopt),
        .resetn(reset_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init initialize_audio
       (.SR(BCLK_int_reg),
        .clk_out2(clk_50),
        .reset_n(reset_n),
        .scl(scl),
        .sda(sda));
  FDRE ready_sig_reg
       (.C(clk),
        .CE(1'b1),
        .D(audio_inout_n_6),
        .Q(CLK),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder
   (D,
    \encoded_reg[8]_0 ,
    Q,
    \dc_bias_reg[2]_0 ,
    \dc_bias_reg[1]_0 ,
    \dc_bias_reg[1]_1 ,
    \dc_bias_reg[1]_2 ,
    \dc_bias_reg[1]_3 ,
    \dc_bias_reg[1]_4 ,
    \dc_bias_reg[1]_5 ,
    \dc_bias_reg[2]_1 ,
    \dc_bias_reg[1]_6 ,
    \dc_bias_reg[1]_7 ,
    \dc_bias_reg[1]_8 ,
    \dc_bias_reg[1]_9 ,
    \encoded_reg[8]_1 ,
    \processQ_reg[6] ,
    CLK,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[3]_2 ,
    \dc_bias_reg[3]_3 ,
    \processQ_reg[5] ,
    \processQ_reg[1] ,
    \processQ_reg[1]_0 ,
    \processQ_reg[5]_0 ,
    \processQ_reg[9] ,
    \trigger_volt_s_reg[6] ,
    \trigger_time_s_reg[7] ,
    \dc_bias_reg[0]_0 ,
    \processQ_reg[5]_1 ,
    SR,
    \dc_bias_reg[1]_10 );
  output [5:0]D;
  output \encoded_reg[8]_0 ;
  output [2:0]Q;
  output \dc_bias_reg[2]_0 ;
  output \dc_bias_reg[1]_0 ;
  output \dc_bias_reg[1]_1 ;
  output \dc_bias_reg[1]_2 ;
  output \dc_bias_reg[1]_3 ;
  output \dc_bias_reg[1]_4 ;
  output \dc_bias_reg[1]_5 ;
  output \dc_bias_reg[2]_1 ;
  output \dc_bias_reg[1]_6 ;
  output \dc_bias_reg[1]_7 ;
  output \dc_bias_reg[1]_8 ;
  output \dc_bias_reg[1]_9 ;
  output \encoded_reg[8]_1 ;
  input \processQ_reg[6] ;
  input CLK;
  input \dc_bias_reg[3]_0 ;
  input \dc_bias_reg[3]_1 ;
  input \dc_bias_reg[3]_2 ;
  input \dc_bias_reg[3]_3 ;
  input \processQ_reg[5] ;
  input \processQ_reg[1] ;
  input \processQ_reg[1]_0 ;
  input \processQ_reg[5]_0 ;
  input \processQ_reg[9] ;
  input [4:0]\trigger_volt_s_reg[6] ;
  input [7:0]\trigger_time_s_reg[7] ;
  input \dc_bias_reg[0]_0 ;
  input \processQ_reg[5]_1 ;
  input [0:0]SR;
  input [0:0]\dc_bias_reg[1]_10 ;

  wire CLK;
  wire [5:0]D;
  wire [2:0]Q;
  wire [0:0]SR;
  wire \dc_bias[0]_i_1__0_n_0 ;
  wire \dc_bias[2]_i_1_n_0 ;
  wire \dc_bias[2]_i_2_n_0 ;
  wire \dc_bias[2]_i_5_n_0 ;
  wire \dc_bias[2]_i_6_n_0 ;
  wire \dc_bias[3]_i_1_n_0 ;
  wire \dc_bias[3]_i_3__0_n_0 ;
  wire \dc_bias[3]_i_5_n_0 ;
  wire \dc_bias_reg[0]_0 ;
  wire \dc_bias_reg[1]_0 ;
  wire \dc_bias_reg[1]_1 ;
  wire [0:0]\dc_bias_reg[1]_10 ;
  wire \dc_bias_reg[1]_2 ;
  wire \dc_bias_reg[1]_3 ;
  wire \dc_bias_reg[1]_4 ;
  wire \dc_bias_reg[1]_5 ;
  wire \dc_bias_reg[1]_6 ;
  wire \dc_bias_reg[1]_7 ;
  wire \dc_bias_reg[1]_8 ;
  wire \dc_bias_reg[1]_9 ;
  wire \dc_bias_reg[2]_0 ;
  wire \dc_bias_reg[2]_1 ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire \dc_bias_reg[3]_3 ;
  wire \dc_bias_reg_n_0_[2] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \processQ_reg[1] ;
  wire \processQ_reg[1]_0 ;
  wire \processQ_reg[5] ;
  wire \processQ_reg[5]_0 ;
  wire \processQ_reg[5]_1 ;
  wire \processQ_reg[6] ;
  wire \processQ_reg[9] ;
  wire [7:0]\trigger_time_s_reg[7] ;
  wire [4:0]\trigger_volt_s_reg[6] ;

  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[0]_i_1__0 
       (.I0(Q[2]),
        .I1(Q[0]),
        .O(\dc_bias[0]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h6F6F6F606060606F)) 
    \dc_bias[2]_i_1 
       (.I0(\dc_bias[2]_i_2_n_0 ),
        .I1(\dc_bias_reg[0]_0 ),
        .I2(\dc_bias_reg[2]_0 ),
        .I3(\dc_bias_reg[2]_1 ),
        .I4(\dc_bias[2]_i_5_n_0 ),
        .I5(\dc_bias[2]_i_6_n_0 ),
        .O(\dc_bias[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h66656666999A9999)) 
    \dc_bias[2]_i_2 
       (.I0(\dc_bias_reg_n_0_[2] ),
        .I1(Q[1]),
        .I2(\processQ_reg[1] ),
        .I3(\processQ_reg[1]_0 ),
        .I4(Q[0]),
        .I5(\processQ_reg[5]_0 ),
        .O(\dc_bias[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \dc_bias[2]_i_3 
       (.I0(Q[2]),
        .I1(\processQ_reg[5]_0 ),
        .O(\dc_bias_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \dc_bias[2]_i_4 
       (.I0(Q[0]),
        .I1(\processQ_reg[5]_0 ),
        .O(\dc_bias_reg[2]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \dc_bias[2]_i_5 
       (.I0(Q[1]),
        .I1(\processQ_reg[9] ),
        .O(\dc_bias[2]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'h65)) 
    \dc_bias[2]_i_6 
       (.I0(\dc_bias_reg_n_0_[2] ),
        .I1(\processQ_reg[9] ),
        .I2(Q[1]),
        .O(\dc_bias[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hE7FFFF8EE700008E)) 
    \dc_bias[3]_i_1 
       (.I0(\dc_bias_reg[0]_0 ),
        .I1(\dc_bias[3]_i_3__0_n_0 ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(Q[2]),
        .I4(\processQ_reg[5]_0 ),
        .I5(\dc_bias[3]_i_5_n_0 ),
        .O(\dc_bias[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT4 #(
    .INIT(16'h5455)) 
    \dc_bias[3]_i_3__0 
       (.I0(Q[1]),
        .I1(\processQ_reg[1] ),
        .I2(\processQ_reg[1]_0 ),
        .I3(Q[0]),
        .O(\dc_bias[3]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT5 #(
    .INIT(32'h0030FBFF)) 
    \dc_bias[3]_i_5 
       (.I0(\processQ_reg[5]_0 ),
        .I1(Q[0]),
        .I2(\processQ_reg[9] ),
        .I3(Q[1]),
        .I4(\dc_bias_reg_n_0_[2] ),
        .O(\dc_bias[3]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1__0_n_0 ),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[1]_10 ),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[2]_i_1_n_0 ),
        .Q(\dc_bias_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[3]_i_1_n_0 ),
        .Q(Q[2]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \encoded[8]_i_1__0 
       (.I0(\processQ_reg[5]_0 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\dc_bias_reg_n_0_[2] ),
        .I4(Q[2]),
        .I5(\processQ_reg[5]_1 ),
        .O(\encoded_reg[8]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \encoded[8]_i_4 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(Q[2]),
        .O(\encoded_reg[8]_1 ));
  FDRE \encoded_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_3 ),
        .Q(D[0]),
        .R(1'b0));
  FDRE \encoded_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_2 ),
        .Q(D[1]),
        .R(1'b0));
  FDRE \encoded_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_1 ),
        .Q(D[2]),
        .R(1'b0));
  FDRE \encoded_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_0 ),
        .Q(D[3]),
        .R(1'b0));
  FDSE \encoded_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\processQ_reg[6] ),
        .Q(D[4]),
        .S(\encoded_reg[8]_0 ));
  FDRE \encoded_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\processQ_reg[5] ),
        .Q(D[5]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hEAAA000000000000)) 
    i__carry__0_i_3__6
       (.I0(\trigger_time_s_reg[7] [4]),
        .I1(\trigger_time_s_reg[7] [0]),
        .I2(\trigger_time_s_reg[7] [1]),
        .I3(\dc_bias_reg[1]_4 ),
        .I4(\trigger_time_s_reg[7] [5]),
        .I5(\trigger_time_s_reg[7] [6]),
        .O(\dc_bias_reg[1]_3 ));
  LUT6 #(
    .INIT(64'hFEAA000000000000)) 
    i__carry__0_i_3__7
       (.I0(\trigger_time_s_reg[7] [4]),
        .I1(\trigger_time_s_reg[7] [0]),
        .I2(\trigger_time_s_reg[7] [1]),
        .I3(\dc_bias_reg[1]_4 ),
        .I4(\trigger_time_s_reg[7] [5]),
        .I5(\trigger_time_s_reg[7] [6]),
        .O(\dc_bias_reg[1]_5 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    i__carry_i_10__5
       (.I0(\trigger_time_s_reg[7] [2]),
        .I1(\trigger_time_s_reg[7] [3]),
        .I2(\trigger_time_s_reg[7] [1]),
        .I3(\trigger_time_s_reg[7] [0]),
        .O(\dc_bias_reg[1]_7 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    i__carry_i_10__6
       (.I0(\trigger_time_s_reg[7] [2]),
        .I1(\trigger_time_s_reg[7] [3]),
        .I2(\trigger_time_s_reg[7] [1]),
        .I3(\trigger_time_s_reg[7] [0]),
        .O(\dc_bias_reg[1]_9 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'h8)) 
    i__carry_i_10__7
       (.I0(\trigger_time_s_reg[7] [3]),
        .I1(\trigger_time_s_reg[7] [2]),
        .O(\dc_bias_reg[1]_4 ));
  LUT6 #(
    .INIT(64'h8888800000000000)) 
    i__carry_i_5__10
       (.I0(\trigger_time_s_reg[7] [6]),
        .I1(\trigger_time_s_reg[7] [5]),
        .I2(\trigger_time_s_reg[7] [3]),
        .I3(\trigger_time_s_reg[7] [2]),
        .I4(\trigger_time_s_reg[7] [4]),
        .I5(\trigger_time_s_reg[7] [7]),
        .O(\dc_bias_reg[1]_1 ));
  LUT5 #(
    .INIT(32'hEA000000)) 
    i__carry_i_5__9
       (.I0(\trigger_volt_s_reg[6] [2]),
        .I1(\trigger_volt_s_reg[6] [1]),
        .I2(\trigger_volt_s_reg[6] [0]),
        .I3(\trigger_volt_s_reg[6] [3]),
        .I4(\trigger_volt_s_reg[6] [4]),
        .O(\dc_bias_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT5 #(
    .INIT(32'hEA000000)) 
    i__carry_i_6__10
       (.I0(\trigger_time_s_reg[7] [4]),
        .I1(\trigger_time_s_reg[7] [2]),
        .I2(\trigger_time_s_reg[7] [3]),
        .I3(\trigger_time_s_reg[7] [5]),
        .I4(\trigger_time_s_reg[7] [6]),
        .O(\dc_bias_reg[1]_2 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA80000000)) 
    i__carry_i_9__6
       (.I0(\trigger_time_s_reg[7] [5]),
        .I1(\trigger_time_s_reg[7] [2]),
        .I2(\trigger_time_s_reg[7] [3]),
        .I3(\trigger_time_s_reg[7] [1]),
        .I4(\trigger_time_s_reg[7] [0]),
        .I5(\trigger_time_s_reg[7] [4]),
        .O(\dc_bias_reg[1]_6 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA80808000)) 
    i__carry_i_9__7
       (.I0(\trigger_time_s_reg[7] [5]),
        .I1(\trigger_time_s_reg[7] [2]),
        .I2(\trigger_time_s_reg[7] [3]),
        .I3(\trigger_time_s_reg[7] [1]),
        .I4(\trigger_time_s_reg[7] [0]),
        .I5(\trigger_time_s_reg[7] [4]),
        .O(\dc_bias_reg[1]_8 ));
endmodule

(* ORIG_REF_NAME = "TDMS_encoder" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1
   (D,
    Q,
    CLK,
    \dc_bias_reg[3]_0 ,
    \processQ_reg[5] ,
    \processQ_reg[1] ,
    SR);
  output [3:0]D;
  output [0:0]Q;
  input CLK;
  input \dc_bias_reg[3]_0 ;
  input \processQ_reg[5] ;
  input \processQ_reg[1] ;
  input [0:0]SR;

  wire CLK;
  wire [3:0]D;
  wire [0:0]Q;
  wire [0:0]SR;
  wire \dc_bias[0]_i_1_n_0 ;
  wire \dc_bias[1]_i_1__0_n_0 ;
  wire \dc_bias[2]_i_1__1_n_0 ;
  wire \dc_bias[3]_i_1__0_n_0 ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg_n_0_[0] ;
  wire \dc_bias_reg_n_0_[1] ;
  wire \dc_bias_reg_n_0_[2] ;
  wire \encoded[0]_i_1__1_n_0 ;
  wire \encoded[2]_i_1__1_n_0 ;
  wire \encoded[8]_i_1_n_0 ;
  wire \encoded[8]_i_2_n_0 ;
  wire \processQ_reg[1] ;
  wire \processQ_reg[5] ;

  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[0]_i_1 
       (.I0(Q),
        .I1(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT4 #(
    .INIT(16'h69AA)) 
    \dc_bias[1]_i_1__0 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\processQ_reg[1] ),
        .I3(Q),
        .O(\dc_bias[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT5 #(
    .INIT(32'h95565555)) 
    \dc_bias[2]_i_1__1 
       (.I0(\dc_bias_reg_n_0_[2] ),
        .I1(\dc_bias_reg_n_0_[1] ),
        .I2(\processQ_reg[1] ),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(Q),
        .O(\dc_bias[2]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT5 #(
    .INIT(32'h070F0F2F)) 
    \dc_bias[3]_i_1__0 
       (.I0(Q),
        .I1(\processQ_reg[1] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\dc_bias_reg_n_0_[1] ),
        .I4(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[3]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1_n_0 ),
        .Q(\dc_bias_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[1]_i_1__0_n_0 ),
        .Q(\dc_bias_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[2]_i_1__1_n_0 ),
        .Q(\dc_bias_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[3]_i_1__0_n_0 ),
        .Q(Q),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \encoded[0]_i_1__1 
       (.I0(Q),
        .I1(\processQ_reg[5] ),
        .O(\encoded[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \encoded[2]_i_1__1 
       (.I0(Q),
        .I1(\processQ_reg[5] ),
        .O(\encoded[2]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \encoded[8]_i_1 
       (.I0(\processQ_reg[1] ),
        .I1(Q),
        .I2(\processQ_reg[5] ),
        .I3(\dc_bias_reg_n_0_[2] ),
        .I4(\dc_bias_reg_n_0_[1] ),
        .I5(\dc_bias_reg_n_0_[0] ),
        .O(\encoded[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00FF00FF)) 
    \encoded[8]_i_2 
       (.I0(\dc_bias_reg_n_0_[0] ),
        .I1(\dc_bias_reg_n_0_[1] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\processQ_reg[5] ),
        .I4(Q),
        .I5(\processQ_reg[1] ),
        .O(\encoded[8]_i_2_n_0 ));
  FDRE \encoded_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\encoded[0]_i_1__1_n_0 ),
        .Q(D[0]),
        .R(1'b0));
  FDRE \encoded_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\encoded[2]_i_1__1_n_0 ),
        .Q(D[1]),
        .R(1'b0));
  FDSE \encoded_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\encoded[8]_i_2_n_0 ),
        .Q(D[2]),
        .S(\encoded[8]_i_1_n_0 ));
  FDRE \encoded_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_0 ),
        .Q(D[3]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "TDMS_encoder" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2
   (D,
    Q,
    \encoded_reg[3]_0 ,
    CLK,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \processQ_reg[5] ,
    \processQ_reg[1] ,
    \processQ_reg[1]_0 ,
    \processQ_reg[9] ,
    SR);
  output [4:0]D;
  output [0:0]Q;
  output \encoded_reg[3]_0 ;
  input CLK;
  input [0:0]\dc_bias_reg[3]_0 ;
  input \dc_bias_reg[3]_1 ;
  input \processQ_reg[5] ;
  input \processQ_reg[1] ;
  input \processQ_reg[1]_0 ;
  input \processQ_reg[9] ;
  input [0:0]SR;

  wire CLK;
  wire [4:0]D;
  wire [0:0]Q;
  wire [0:0]SR;
  wire \dc_bias[0]_i_1__1_n_0 ;
  wire \dc_bias[1]_i_1__1_n_0 ;
  wire \dc_bias[2]_i_1__0_n_0 ;
  wire \dc_bias[3]_i_2__0_n_0 ;
  wire [0:0]\dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg_n_0_[0] ;
  wire \dc_bias_reg_n_0_[1] ;
  wire \dc_bias_reg_n_0_[2] ;
  wire [8:8]encoded0_in;
  wire [2:0]encoded1_in;
  wire \encoded[8]_i_2__0_n_0 ;
  wire \encoded_reg[3]_0 ;
  wire \processQ_reg[1] ;
  wire \processQ_reg[1]_0 ;
  wire \processQ_reg[5] ;
  wire \processQ_reg[9] ;

  LUT6 #(
    .INIT(64'h9999666266669998)) 
    \dc_bias[0]_i_1__1 
       (.I0(\dc_bias_reg_n_0_[0] ),
        .I1(\processQ_reg[1] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\dc_bias_reg_n_0_[1] ),
        .I4(Q),
        .I5(\processQ_reg[1]_0 ),
        .O(\dc_bias[0]_i_1__1_n_0 ));
  LUT5 #(
    .INIT(32'h1DE28778)) 
    \dc_bias[1]_i_1__1 
       (.I0(Q),
        .I1(\processQ_reg[1] ),
        .I2(\processQ_reg[9] ),
        .I3(\dc_bias_reg_n_0_[1] ),
        .I4(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[1]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h55A659A5A555A585)) 
    \dc_bias[2]_i_1__0 
       (.I0(\dc_bias_reg_n_0_[2] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\processQ_reg[9] ),
        .I3(\dc_bias_reg_n_0_[1] ),
        .I4(\processQ_reg[1] ),
        .I5(Q),
        .O(\dc_bias[2]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h0E0FAFEF05050505)) 
    \dc_bias[3]_i_2__0 
       (.I0(\processQ_reg[9] ),
        .I1(\processQ_reg[1] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(\dc_bias_reg_n_0_[1] ),
        .I5(Q),
        .O(\dc_bias[3]_i_2__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1__1_n_0 ),
        .Q(\dc_bias_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[1]_i_1__1_n_0 ),
        .Q(\dc_bias_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[2]_i_1__0_n_0 ),
        .Q(\dc_bias_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[3]_i_2__0_n_0 ),
        .Q(Q),
        .R(SR));
  LUT6 #(
    .INIT(64'hAAAAAAAA00000008)) 
    \encoded[0]_i_1__0 
       (.I0(\processQ_reg[5] ),
        .I1(\processQ_reg[9] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(\dc_bias_reg_n_0_[1] ),
        .I5(Q),
        .O(encoded1_in[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFF5555555D)) 
    \encoded[2]_i_1__0 
       (.I0(\processQ_reg[5] ),
        .I1(\processQ_reg[9] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(\dc_bias_reg_n_0_[1] ),
        .I5(Q),
        .O(encoded1_in[2]));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \encoded[3]_i_2 
       (.I0(Q),
        .I1(\dc_bias_reg_n_0_[1] ),
        .I2(\dc_bias_reg_n_0_[0] ),
        .I3(\dc_bias_reg_n_0_[2] ),
        .O(\encoded_reg[3]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \encoded[8]_i_1__1 
       (.I0(\processQ_reg[1] ),
        .I1(\processQ_reg[5] ),
        .I2(Q),
        .I3(\dc_bias_reg_n_0_[1] ),
        .I4(\dc_bias_reg_n_0_[0] ),
        .I5(\dc_bias_reg_n_0_[2] ),
        .O(encoded0_in));
  LUT6 #(
    .INIT(64'h0000FFFFFFFEFFFF)) 
    \encoded[8]_i_2__0 
       (.I0(\dc_bias_reg_n_0_[2] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\dc_bias_reg_n_0_[1] ),
        .I3(Q),
        .I4(\processQ_reg[5] ),
        .I5(\processQ_reg[1] ),
        .O(\encoded[8]_i_2__0_n_0 ));
  FDRE \encoded_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(encoded1_in[0]),
        .Q(D[0]),
        .R(1'b0));
  FDRE \encoded_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(encoded1_in[2]),
        .Q(D[1]),
        .R(1'b0));
  FDRE \encoded_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_0 ),
        .Q(D[2]),
        .R(1'b0));
  FDSE \encoded_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\encoded[8]_i_2__0_n_0 ),
        .Q(D[3]),
        .S(encoded0_in));
  FDRE \encoded_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_1 ),
        .Q(D[4]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl
   (D,
    E,
    \state_reg[0] ,
    initEn_reg,
    scl,
    sda,
    clk_out2,
    Q,
    \initWord_reg[20] ,
    initEn_reg_0,
    data_i,
    reset_n,
    stb,
    \initWord_reg[0] ,
    \delaycnt_reg[11] ,
    \initA_reg[0] ,
    \state_reg[2] ,
    initEn,
    msg);
  output [3:0]D;
  output [0:0]E;
  output [0:0]\state_reg[0] ;
  output initEn_reg;
  inout scl;
  inout sda;
  input clk_out2;
  input [3:0]Q;
  input \initWord_reg[20] ;
  input initEn_reg_0;
  input [7:0]data_i;
  input reset_n;
  input stb;
  input \initWord_reg[0] ;
  input \delaycnt_reg[11] ;
  input \initA_reg[0] ;
  input \state_reg[2] ;
  input initEn;
  input msg;

  wire [3:0]D;
  wire DONE_O_i_1_n_0;
  wire DONE_O_i_3_n_0;
  wire DONE_O_i_4_n_0;
  wire DONE_O_i_5_n_0;
  wire [0:0]E;
  wire ERR_O_i_1_n_0;
  wire ERR_O_i_2_n_0;
  wire \FSM_gray_state[0]_i_1_n_0 ;
  wire \FSM_gray_state[0]_i_2_n_0 ;
  wire \FSM_gray_state[1]_i_1_n_0 ;
  wire \FSM_gray_state[1]_i_2_n_0 ;
  wire \FSM_gray_state[1]_i_3_n_0 ;
  wire \FSM_gray_state[2]_i_1_n_0 ;
  wire \FSM_gray_state[2]_i_2_n_0 ;
  wire \FSM_gray_state[3]_i_10_n_0 ;
  wire \FSM_gray_state[3]_i_1_n_0 ;
  wire \FSM_gray_state[3]_i_2_n_0 ;
  wire \FSM_gray_state[3]_i_3_n_0 ;
  wire \FSM_gray_state[3]_i_4_n_0 ;
  wire \FSM_gray_state[3]_i_6_n_0 ;
  wire \FSM_gray_state[3]_i_7_n_0 ;
  wire \FSM_gray_state[3]_i_8_n_0 ;
  wire \FSM_gray_state[3]_i_9_n_0 ;
  wire \FSM_gray_state_reg[3]_i_5_n_0 ;
  wire [3:0]Q;
  wire addrNData_i_1_n_0;
  wire addrNData_reg_n_0;
  wire arbLost;
  wire [2:0]bitCount;
  wire \bitCount[0]_i_1_n_0 ;
  wire \bitCount[1]_i_1_n_0 ;
  wire \bitCount[2]_i_1_n_0 ;
  wire [6:0]busFreeCnt0;
  wire busFreeCnt0_1;
  wire \busFreeCnt[1]_i_1_n_0 ;
  wire \busFreeCnt[6]_i_3_n_0 ;
  wire [6:0]busFreeCnt_reg__0;
  wire busState0;
  wire \busState[0]_i_1_n_0 ;
  wire \busState[1]_i_1_n_0 ;
  wire \busState_reg_n_0_[0] ;
  wire \busState_reg_n_0_[1] ;
  wire clk_out2;
  wire dScl;
  wire dataByte0;
  wire dataByte1;
  wire \dataByte[7]_i_1_n_0 ;
  wire \dataByte[7]_i_4_n_0 ;
  wire \dataByte[7]_i_5_n_0 ;
  wire \dataByte[7]_i_6_n_0 ;
  wire \dataByte_reg_n_0_[0] ;
  wire \dataByte_reg_n_0_[1] ;
  wire \dataByte_reg_n_0_[2] ;
  wire \dataByte_reg_n_0_[3] ;
  wire \dataByte_reg_n_0_[4] ;
  wire \dataByte_reg_n_0_[5] ;
  wire \dataByte_reg_n_0_[6] ;
  wire \dataByte_reg_n_0_[7] ;
  wire [7:0]data_i;
  wire ddSda;
  wire \delaycnt_reg[11] ;
  wire done;
  wire error;
  wire \initA[6]_i_4_n_0 ;
  wire \initA_reg[0] ;
  wire initEn;
  wire initEn_i_2_n_0;
  wire initEn_reg;
  wire initEn_reg_0;
  wire \initWord_reg[0] ;
  wire \initWord_reg[20] ;
  wire int_Rst;
  wire int_Rst_i_1_n_0;
  wire msg;
  wire nstate122_out;
  wire [0:0]p_0_in;
  wire p_0_in7_in;
  wire [7:0]p_1_in;
  wire p_1_in4_in;
  wire rScl;
  wire rScl_i_1_n_0;
  wire rScl_i_2_n_0;
  wire rSda;
  wire rSda_i_1_n_0;
  wire rSda_i_2_n_0;
  wire rSda_i_3_n_0;
  wire rSda_i_4_n_0;
  wire rSda_i_5_n_0;
  wire rSda_i_6_n_0;
  wire rSda_i_7_n_0;
  wire reset_n;
  wire scl;
  wire [6:0]sclCnt0;
  wire sclCnt0_0;
  wire \sclCnt[1]_i_1_n_0 ;
  wire \sclCnt[4]_i_1_n_0 ;
  wire \sclCnt[6]_i_2_n_0 ;
  wire \sclCnt[6]_i_4_n_0 ;
  wire [6:0]sclCnt_reg__0;
  wire scl_INST_0_i_1_n_0;
  wire sda;
  wire sda_INST_0_i_1_n_0;
  (* RTL_KEEP = "yes" *) wire [3:0]state;
  wire [0:0]\state_reg[0] ;
  wire \state_reg[2] ;
  wire stb;
  wire \subState[0]_i_1_n_0 ;
  wire \subState[1]_i_1_n_0 ;
  wire \subState[1]_i_2_n_0 ;
  wire \subState[1]_i_3_n_0 ;
  wire \subState_reg_n_0_[0] ;
  wire \subState_reg_n_0_[1] ;

  LUT6 #(
    .INIT(64'h888888888F888888)) 
    DONE_O_i_1
       (.I0(arbLost),
        .I1(p_0_in7_in),
        .I2(DONE_O_i_3_n_0),
        .I3(\subState[1]_i_2_n_0 ),
        .I4(\subState_reg_n_0_[0] ),
        .I5(\subState_reg_n_0_[1] ),
        .O(DONE_O_i_1_n_0));
  LUT4 #(
    .INIT(16'h0008)) 
    DONE_O_i_2
       (.I0(state[1]),
        .I1(state[0]),
        .I2(state[2]),
        .I3(state[3]),
        .O(p_0_in7_in));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAFF0CFF)) 
    DONE_O_i_3
       (.I0(DONE_O_i_4_n_0),
        .I1(addrNData_reg_n_0),
        .I2(p_0_in),
        .I3(DONE_O_i_5_n_0),
        .I4(state[2]),
        .I5(state[3]),
        .O(DONE_O_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    DONE_O_i_4
       (.I0(bitCount[2]),
        .I1(bitCount[1]),
        .I2(bitCount[0]),
        .O(DONE_O_i_4_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    DONE_O_i_5
       (.I0(state[1]),
        .I1(state[0]),
        .O(DONE_O_i_5_n_0));
  FDRE DONE_O_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(DONE_O_i_1_n_0),
        .Q(done),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFF20000000)) 
    ERR_O_i_1
       (.I0(p_1_in4_in),
        .I1(\subState_reg_n_0_[1] ),
        .I2(\subState_reg_n_0_[0] ),
        .I3(\subState[1]_i_2_n_0 ),
        .I4(p_0_in),
        .I5(ERR_O_i_2_n_0),
        .O(ERR_O_i_1_n_0));
  LUT5 #(
    .INIT(32'h10000000)) 
    ERR_O_i_2
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(arbLost),
        .O(ERR_O_i_2_n_0));
  FDRE ERR_O_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(ERR_O_i_1_n_0),
        .Q(error),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h4445444544555555)) 
    \FSM_gray_state[0]_i_1 
       (.I0(state[3]),
        .I1(\FSM_gray_state[0]_i_2_n_0 ),
        .I2(state[0]),
        .I3(state[2]),
        .I4(arbLost),
        .I5(state[1]),
        .O(\FSM_gray_state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0400)) 
    \FSM_gray_state[0]_i_2 
       (.I0(int_Rst),
        .I1(stb),
        .I2(state[0]),
        .I3(state[1]),
        .O(\FSM_gray_state[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA2AAAAAA)) 
    \FSM_gray_state[1]_i_1 
       (.I0(\FSM_gray_state[1]_i_2_n_0 ),
        .I1(msg),
        .I2(state[0]),
        .I3(state[1]),
        .I4(\FSM_gray_state[1]_i_3_n_0 ),
        .O(\FSM_gray_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000A000517A017A)) 
    \FSM_gray_state[1]_i_2 
       (.I0(state[0]),
        .I1(arbLost),
        .I2(state[1]),
        .I3(state[2]),
        .I4(nstate122_out),
        .I5(state[3]),
        .O(\FSM_gray_state[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAAAAABA)) 
    \FSM_gray_state[1]_i_3 
       (.I0(state[2]),
        .I1(addrNData_reg_n_0),
        .I2(stb),
        .I3(int_Rst),
        .I4(\dataByte_reg_n_0_[0] ),
        .O(\FSM_gray_state[1]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_gray_state[1]_i_4 
       (.I0(stb),
        .I1(int_Rst),
        .O(nstate122_out));
  LUT6 #(
    .INIT(64'hEE0000000000EE0E)) 
    \FSM_gray_state[2]_i_1 
       (.I0(\FSM_gray_state[2]_i_2_n_0 ),
        .I1(state[2]),
        .I2(arbLost),
        .I3(state[1]),
        .I4(state[0]),
        .I5(state[3]),
        .O(\FSM_gray_state[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4440444044404444)) 
    \FSM_gray_state[2]_i_2 
       (.I0(state[0]),
        .I1(state[1]),
        .I2(\dataByte_reg_n_0_[0] ),
        .I3(int_Rst),
        .I4(addrNData_reg_n_0),
        .I5(stb),
        .O(\FSM_gray_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_gray_state[2]_i_3 
       (.I0(rSda),
        .I1(dScl),
        .I2(p_0_in),
        .O(arbLost));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \FSM_gray_state[3]_i_1 
       (.I0(\FSM_gray_state[3]_i_3_n_0 ),
        .I1(state[3]),
        .I2(\FSM_gray_state[3]_i_4_n_0 ),
        .I3(state[2]),
        .I4(\FSM_gray_state_reg[3]_i_5_n_0 ),
        .O(\FSM_gray_state[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \FSM_gray_state[3]_i_10 
       (.I0(bitCount[1]),
        .I1(bitCount[0]),
        .I2(bitCount[2]),
        .O(\FSM_gray_state[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \FSM_gray_state[3]_i_2 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(stb),
        .I3(int_Rst),
        .I4(\FSM_gray_state[3]_i_6_n_0 ),
        .I5(msg),
        .O(\FSM_gray_state[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \FSM_gray_state[3]_i_3 
       (.I0(\subState[1]_i_2_n_0 ),
        .I1(\subState_reg_n_0_[0] ),
        .I2(\subState_reg_n_0_[1] ),
        .O(\FSM_gray_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8B8B8B8B8B8)) 
    \FSM_gray_state[3]_i_4 
       (.I0(\FSM_gray_state[3]_i_7_n_0 ),
        .I1(state[1]),
        .I2(\FSM_gray_state[3]_i_3_n_0 ),
        .I3(p_0_in),
        .I4(dScl),
        .I5(rSda),
        .O(\FSM_gray_state[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_gray_state[3]_i_6 
       (.I0(state[3]),
        .I1(state[2]),
        .O(\FSM_gray_state[3]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h08004800)) 
    \FSM_gray_state[3]_i_7 
       (.I0(state[0]),
        .I1(\subState_reg_n_0_[1] ),
        .I2(\subState_reg_n_0_[0] ),
        .I3(\subState[1]_i_2_n_0 ),
        .I4(\FSM_gray_state[3]_i_10_n_0 ),
        .O(\FSM_gray_state[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h8B88888888888888)) 
    \FSM_gray_state[3]_i_8 
       (.I0(\FSM_gray_state[3]_i_3_n_0 ),
        .I1(state[0]),
        .I2(\busState_reg_n_0_[1] ),
        .I3(reset_n),
        .I4(\busState_reg_n_0_[0] ),
        .I5(stb),
        .O(\FSM_gray_state[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAAABFFFFAAAA0000)) 
    \FSM_gray_state[3]_i_9 
       (.I0(arbLost),
        .I1(bitCount[2]),
        .I2(bitCount[1]),
        .I3(bitCount[0]),
        .I4(state[0]),
        .I5(\FSM_gray_state[3]_i_3_n_0 ),
        .O(\FSM_gray_state[3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[0] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[0]_i_1_n_0 ),
        .Q(state[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[1] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[1]_i_1_n_0 ),
        .Q(state[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[2] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[2]_i_1_n_0 ),
        .Q(state[2]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[3] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[3]_i_2_n_0 ),
        .Q(state[3]),
        .R(1'b0));
  MUXF7 \FSM_gray_state_reg[3]_i_5 
       (.I0(\FSM_gray_state[3]_i_8_n_0 ),
        .I1(\FSM_gray_state[3]_i_9_n_0 ),
        .O(\FSM_gray_state_reg[3]_i_5_n_0 ),
        .S(state[1]));
  LUT6 #(
    .INIT(64'h0EAAEEAAEEAAEEAA)) 
    addrNData_i_1
       (.I0(addrNData_reg_n_0),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(p_1_in4_in),
        .I3(\subState[1]_i_2_n_0 ),
        .I4(\subState_reg_n_0_[0] ),
        .I5(\subState_reg_n_0_[1] ),
        .O(addrNData_i_1_n_0));
  LUT4 #(
    .INIT(16'h0004)) 
    addrNData_i_2
       (.I0(state[0]),
        .I1(state[1]),
        .I2(state[2]),
        .I3(state[3]),
        .O(p_1_in4_in));
  FDRE addrNData_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(addrNData_i_1_n_0),
        .Q(addrNData_reg_n_0),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hF6)) 
    \bitCount[0]_i_1 
       (.I0(bitCount[0]),
        .I1(dataByte0),
        .I2(dataByte1),
        .O(\bitCount[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hFFA6)) 
    \bitCount[1]_i_1 
       (.I0(bitCount[1]),
        .I1(dataByte0),
        .I2(bitCount[0]),
        .I3(dataByte1),
        .O(\bitCount[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hFFFFAAA6)) 
    \bitCount[2]_i_1 
       (.I0(bitCount[2]),
        .I1(dataByte0),
        .I2(bitCount[0]),
        .I3(bitCount[1]),
        .I4(dataByte1),
        .O(\bitCount[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0002030000020000)) 
    \bitCount[2]_i_2 
       (.I0(\FSM_gray_state[3]_i_3_n_0 ),
        .I1(state[2]),
        .I2(state[3]),
        .I3(state[0]),
        .I4(state[1]),
        .I5(\subState[1]_i_2_n_0 ),
        .O(dataByte1));
  FDRE #(
    .INIT(1'b1)) 
    \bitCount_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\bitCount[0]_i_1_n_0 ),
        .Q(bitCount[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \bitCount_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\bitCount[1]_i_1_n_0 ),
        .Q(bitCount[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \bitCount_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\bitCount[2]_i_1_n_0 ),
        .Q(bitCount[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \busFreeCnt[0]_i_1 
       (.I0(busFreeCnt_reg__0[0]),
        .O(busFreeCnt0[0]));
  LUT2 #(
    .INIT(4'h9)) 
    \busFreeCnt[1]_i_1 
       (.I0(busFreeCnt_reg__0[0]),
        .I1(busFreeCnt_reg__0[1]),
        .O(\busFreeCnt[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hA9)) 
    \busFreeCnt[2]_i_1 
       (.I0(busFreeCnt_reg__0[2]),
        .I1(busFreeCnt_reg__0[1]),
        .I2(busFreeCnt_reg__0[0]),
        .O(busFreeCnt0[2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \busFreeCnt[3]_i_1 
       (.I0(busFreeCnt_reg__0[3]),
        .I1(busFreeCnt_reg__0[2]),
        .I2(busFreeCnt_reg__0[0]),
        .I3(busFreeCnt_reg__0[1]),
        .O(busFreeCnt0[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \busFreeCnt[4]_i_1 
       (.I0(busFreeCnt_reg__0[4]),
        .I1(busFreeCnt_reg__0[3]),
        .I2(busFreeCnt_reg__0[1]),
        .I3(busFreeCnt_reg__0[0]),
        .I4(busFreeCnt_reg__0[2]),
        .O(busFreeCnt0[4]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \busFreeCnt[5]_i_1 
       (.I0(busFreeCnt_reg__0[5]),
        .I1(busFreeCnt_reg__0[4]),
        .I2(busFreeCnt_reg__0[2]),
        .I3(busFreeCnt_reg__0[0]),
        .I4(busFreeCnt_reg__0[1]),
        .I5(busFreeCnt_reg__0[3]),
        .O(busFreeCnt0[5]));
  LUT3 #(
    .INIT(8'hBF)) 
    \busFreeCnt[6]_i_1 
       (.I0(int_Rst),
        .I1(dScl),
        .I2(p_0_in),
        .O(busFreeCnt0_1));
  LUT2 #(
    .INIT(4'h6)) 
    \busFreeCnt[6]_i_2 
       (.I0(busFreeCnt_reg__0[6]),
        .I1(\busFreeCnt[6]_i_3_n_0 ),
        .O(busFreeCnt0[6]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \busFreeCnt[6]_i_3 
       (.I0(busFreeCnt_reg__0[4]),
        .I1(busFreeCnt_reg__0[2]),
        .I2(busFreeCnt_reg__0[0]),
        .I3(busFreeCnt_reg__0[1]),
        .I4(busFreeCnt_reg__0[3]),
        .I5(busFreeCnt_reg__0[5]),
        .O(\busFreeCnt[6]_i_3_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[0]),
        .Q(busFreeCnt_reg__0[0]),
        .S(busFreeCnt0_1));
  FDRE #(
    .INIT(1'b0)) 
    \busFreeCnt_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\busFreeCnt[1]_i_1_n_0 ),
        .Q(busFreeCnt_reg__0[1]),
        .R(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[2]),
        .Q(busFreeCnt_reg__0[2]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[3]),
        .Q(busFreeCnt_reg__0[3]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[4]),
        .Q(busFreeCnt_reg__0[4]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[5]),
        .Q(busFreeCnt_reg__0[5]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[6]),
        .Q(busFreeCnt_reg__0[6]),
        .S(busFreeCnt0_1));
  LUT6 #(
    .INIT(64'h4555FFFF45550000)) 
    \busState[0]_i_1 
       (.I0(int_Rst),
        .I1(p_0_in),
        .I2(dScl),
        .I3(ddSda),
        .I4(busState0),
        .I5(\busState_reg_n_0_[0] ),
        .O(\busState[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \busState[1]_i_1 
       (.I0(p_0_in),
        .I1(dScl),
        .I2(ddSda),
        .I3(int_Rst),
        .I4(busState0),
        .I5(\busState_reg_n_0_[1] ),
        .O(\busState[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF4444F444)) 
    \busState[1]_i_2 
       (.I0(busFreeCnt_reg__0[6]),
        .I1(\busFreeCnt[6]_i_3_n_0 ),
        .I2(ddSda),
        .I3(dScl),
        .I4(p_0_in),
        .I5(int_Rst),
        .O(busState0));
  FDRE #(
    .INIT(1'b0)) 
    \busState_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\busState[0]_i_1_n_0 ),
        .Q(\busState_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \busState_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\busState[1]_i_1_n_0 ),
        .Q(\busState_reg_n_0_[1] ),
        .R(1'b0));
  FDRE dScl_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(scl),
        .Q(dScl),
        .R(1'b0));
  FDRE dSda_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(sda),
        .Q(p_0_in),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h88B8)) 
    \dataByte[0]_i_1 
       (.I0(p_0_in),
        .I1(\dataByte[7]_i_4_n_0 ),
        .I2(data_i[0]),
        .I3(\dataByte[7]_i_5_n_0 ),
        .O(p_1_in[0]));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \dataByte[1]_i_1 
       (.I0(\dataByte_reg_n_0_[0] ),
        .I1(\dataByte[7]_i_4_n_0 ),
        .I2(\dataByte[7]_i_5_n_0 ),
        .I3(data_i[1]),
        .O(p_1_in[1]));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \dataByte[2]_i_1 
       (.I0(\dataByte_reg_n_0_[1] ),
        .I1(\dataByte[7]_i_4_n_0 ),
        .I2(\dataByte[7]_i_5_n_0 ),
        .I3(data_i[2]),
        .O(p_1_in[2]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \dataByte[3]_i_1 
       (.I0(\dataByte_reg_n_0_[2] ),
        .I1(\dataByte[7]_i_4_n_0 ),
        .I2(data_i[3]),
        .I3(\dataByte[7]_i_5_n_0 ),
        .O(p_1_in[3]));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \dataByte[4]_i_1 
       (.I0(\dataByte_reg_n_0_[3] ),
        .I1(\dataByte[7]_i_4_n_0 ),
        .I2(\dataByte[7]_i_5_n_0 ),
        .I3(data_i[4]),
        .O(p_1_in[4]));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \dataByte[5]_i_1 
       (.I0(\dataByte_reg_n_0_[4] ),
        .I1(\dataByte[7]_i_4_n_0 ),
        .I2(\dataByte[7]_i_5_n_0 ),
        .I3(data_i[5]),
        .O(p_1_in[5]));
  LUT4 #(
    .INIT(16'hBBB8)) 
    \dataByte[6]_i_1 
       (.I0(\dataByte_reg_n_0_[5] ),
        .I1(\dataByte[7]_i_4_n_0 ),
        .I2(\dataByte[7]_i_5_n_0 ),
        .I3(data_i[6]),
        .O(p_1_in[6]));
  LUT2 #(
    .INIT(4'hB)) 
    \dataByte[7]_i_1 
       (.I0(dataByte0),
        .I1(\dataByte[7]_i_4_n_0 ),
        .O(\dataByte[7]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \dataByte[7]_i_2 
       (.I0(\dataByte_reg_n_0_[6] ),
        .I1(\dataByte[7]_i_4_n_0 ),
        .I2(data_i[7]),
        .I3(\dataByte[7]_i_5_n_0 ),
        .O(p_1_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h800080A0)) 
    \dataByte[7]_i_3 
       (.I0(\subState[1]_i_2_n_0 ),
        .I1(p_0_in7_in),
        .I2(\subState_reg_n_0_[0] ),
        .I3(\subState_reg_n_0_[1] ),
        .I4(\dataByte[7]_i_6_n_0 ),
        .O(dataByte0));
  LUT6 #(
    .INIT(64'hFFF0F7FFFFFFFFFF)) 
    \dataByte[7]_i_4 
       (.I0(\subState_reg_n_0_[0] ),
        .I1(\subState_reg_n_0_[1] ),
        .I2(\subState[1]_i_3_n_0 ),
        .I3(state[1]),
        .I4(state[0]),
        .I5(\subState[1]_i_2_n_0 ),
        .O(\dataByte[7]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \dataByte[7]_i_5 
       (.I0(state[2]),
        .I1(state[3]),
        .I2(state[0]),
        .I3(state[1]),
        .O(\dataByte[7]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hFB)) 
    \dataByte[7]_i_6 
       (.I0(state[3]),
        .I1(state[1]),
        .I2(state[0]),
        .O(\dataByte[7]_i_6_n_0 ));
  FDRE \dataByte_reg[0] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[0]),
        .Q(\dataByte_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \dataByte_reg[1] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[1]),
        .Q(\dataByte_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \dataByte_reg[2] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[2]),
        .Q(\dataByte_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \dataByte_reg[3] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[3]),
        .Q(\dataByte_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \dataByte_reg[4] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[4]),
        .Q(\dataByte_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \dataByte_reg[5] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[5]),
        .Q(\dataByte_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \dataByte_reg[6] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[6]),
        .Q(\dataByte_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \dataByte_reg[7] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[7]),
        .Q(\dataByte_reg_n_0_[7] ),
        .R(1'b0));
  FDRE ddSda_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(p_0_in),
        .Q(ddSda),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h44544444)) 
    \initA[6]_i_1 
       (.I0(Q[3]),
        .I1(initEn_reg_0),
        .I2(done),
        .I3(Q[2]),
        .I4(\initA[6]_i_4_n_0 ),
        .O(E));
  LUT4 #(
    .INIT(16'h0040)) 
    \initA[6]_i_4 
       (.I0(\initWord_reg[20] ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(error),
        .O(\initA[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF57FF00005400)) 
    initEn_i_1
       (.I0(Q[2]),
        .I1(initEn_reg_0),
        .I2(initEn_i_2_n_0),
        .I3(reset_n),
        .I4(Q[3]),
        .I5(initEn),
        .O(initEn_reg));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    initEn_i_2
       (.I0(\initWord_reg[0] ),
        .I1(Q[2]),
        .I2(done),
        .I3(error),
        .O(initEn_i_2_n_0));
  LUT6 #(
    .INIT(64'hBBBBBBBBBBBBBBB3)) 
    int_Rst_i_1
       (.I0(int_Rst),
        .I1(reset_n),
        .I2(state[1]),
        .I3(state[0]),
        .I4(state[2]),
        .I5(state[3]),
        .O(int_Rst_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    int_Rst_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(int_Rst_i_1_n_0),
        .Q(int_Rst),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h7F70)) 
    rScl_i_1
       (.I0(\subState_reg_n_0_[1] ),
        .I1(\subState_reg_n_0_[0] ),
        .I2(rScl_i_2_n_0),
        .I3(rScl),
        .O(rScl_i_1_n_0));
  LUT6 #(
    .INIT(64'h9554D554D5540000)) 
    rScl_i_2
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(\subState_reg_n_0_[1] ),
        .I5(\subState_reg_n_0_[0] ),
        .O(rScl_i_2_n_0));
  FDRE #(
    .INIT(1'b1)) 
    rScl_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(rScl_i_1_n_0),
        .Q(rScl),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAABFFFFAAAB0000)) 
    rSda_i_1
       (.I0(rSda_i_2_n_0),
        .I1(rSda_i_3_n_0),
        .I2(\subState_reg_n_0_[1] ),
        .I3(\subState_reg_n_0_[0] ),
        .I4(rSda_i_4_n_0),
        .I5(rSda),
        .O(rSda_i_1_n_0));
  LUT6 #(
    .INIT(64'h4CCCCECC4CCCC4CC)) 
    rSda_i_2
       (.I0(rSda_i_5_n_0),
        .I1(rSda_i_6_n_0),
        .I2(state[3]),
        .I3(rSda_i_7_n_0),
        .I4(state[2]),
        .I5(\dataByte_reg_n_0_[7] ),
        .O(rSda_i_2_n_0));
  LUT3 #(
    .INIT(8'hFB)) 
    rSda_i_3
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[1]),
        .O(rSda_i_3_n_0));
  LUT6 #(
    .INIT(64'h001040104010D554)) 
    rSda_i_4
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(\subState_reg_n_0_[1] ),
        .I5(\subState_reg_n_0_[0] ),
        .O(rSda_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h1)) 
    rSda_i_5
       (.I0(\subState_reg_n_0_[1] ),
        .I1(\subState_reg_n_0_[0] ),
        .O(rSda_i_5_n_0));
  LUT6 #(
    .INIT(64'h3433333333333333)) 
    rSda_i_6
       (.I0(\subState_reg_n_0_[0] ),
        .I1(\subState_reg_n_0_[1] ),
        .I2(state[3]),
        .I3(state[2]),
        .I4(state[0]),
        .I5(state[1]),
        .O(rSda_i_6_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    rSda_i_7
       (.I0(state[0]),
        .I1(state[1]),
        .O(rSda_i_7_n_0));
  FDRE #(
    .INIT(1'b1)) 
    rSda_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(rSda_i_1_n_0),
        .Q(rSda),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \sclCnt[0]_i_1 
       (.I0(sclCnt_reg__0[0]),
        .O(sclCnt0[0]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \sclCnt[1]_i_1 
       (.I0(sclCnt_reg__0[0]),
        .I1(sclCnt_reg__0[1]),
        .O(\sclCnt[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hA9)) 
    \sclCnt[2]_i_1 
       (.I0(sclCnt_reg__0[2]),
        .I1(sclCnt_reg__0[1]),
        .I2(sclCnt_reg__0[0]),
        .O(sclCnt0[2]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \sclCnt[3]_i_1 
       (.I0(sclCnt_reg__0[3]),
        .I1(sclCnt_reg__0[2]),
        .I2(sclCnt_reg__0[0]),
        .I3(sclCnt_reg__0[1]),
        .O(sclCnt0[3]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \sclCnt[4]_i_1 
       (.I0(sclCnt_reg__0[4]),
        .I1(sclCnt_reg__0[1]),
        .I2(sclCnt_reg__0[0]),
        .I3(sclCnt_reg__0[2]),
        .I4(sclCnt_reg__0[3]),
        .O(\sclCnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \sclCnt[5]_i_1 
       (.I0(sclCnt_reg__0[5]),
        .I1(sclCnt_reg__0[3]),
        .I2(sclCnt_reg__0[2]),
        .I3(sclCnt_reg__0[0]),
        .I4(sclCnt_reg__0[1]),
        .I5(sclCnt_reg__0[4]),
        .O(sclCnt0[5]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \sclCnt[6]_i_1 
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(\subState[1]_i_2_n_0 ),
        .O(sclCnt0_0));
  LUT2 #(
    .INIT(4'hB)) 
    \sclCnt[6]_i_2 
       (.I0(dScl),
        .I1(rScl),
        .O(\sclCnt[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sclCnt[6]_i_3 
       (.I0(sclCnt_reg__0[6]),
        .I1(\sclCnt[6]_i_4_n_0 ),
        .O(sclCnt0[6]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \sclCnt[6]_i_4 
       (.I0(sclCnt_reg__0[3]),
        .I1(sclCnt_reg__0[2]),
        .I2(sclCnt_reg__0[0]),
        .I3(sclCnt_reg__0[1]),
        .I4(sclCnt_reg__0[4]),
        .I5(sclCnt_reg__0[5]),
        .O(\sclCnt[6]_i_4_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[0] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[0]),
        .Q(sclCnt_reg__0[0]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \sclCnt_reg[1] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(\sclCnt[1]_i_1_n_0 ),
        .Q(sclCnt_reg__0[1]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[2] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[2]),
        .Q(sclCnt_reg__0[2]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[3] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[3]),
        .Q(sclCnt_reg__0[3]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[4] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(\sclCnt[4]_i_1_n_0 ),
        .Q(sclCnt_reg__0[4]),
        .S(sclCnt0_0));
  FDRE #(
    .INIT(1'b1)) 
    \sclCnt_reg[5] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[5]),
        .Q(sclCnt_reg__0[5]),
        .R(sclCnt0_0));
  FDRE #(
    .INIT(1'b1)) 
    \sclCnt_reg[6] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[6]),
        .Q(sclCnt_reg__0[6]),
        .R(sclCnt0_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    scl_INST_0
       (.I0(1'b0),
        .I1(scl_INST_0_i_1_n_0),
        .I2(1'b0),
        .I3(1'b0),
        .I4(1'b0),
        .I5(1'b0),
        .O(scl));
  LUT1 #(
    .INIT(2'h1)) 
    scl_INST_0_i_1
       (.I0(rScl),
        .O(scl_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    sda_INST_0
       (.I0(1'b0),
        .I1(sda_INST_0_i_1_n_0),
        .I2(1'b0),
        .I3(1'b0),
        .I4(1'b0),
        .I5(1'b0),
        .O(sda));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT1 #(
    .INIT(2'h1)) 
    sda_INST_0_i_1
       (.I0(rSda),
        .O(sda_INST_0_i_1_n_0));
  LUT5 #(
    .INIT(32'h0000FF0D)) 
    \state[0]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(error),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h0B000A000B000000)) 
    \state[1]_i_1 
       (.I0(\initA_reg[0] ),
        .I1(\initWord_reg[20] ),
        .I2(error),
        .I3(\state_reg[2] ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hC0CD)) 
    \state[2]_i_1 
       (.I0(\initWord_reg[0] ),
        .I1(error),
        .I2(Q[3]),
        .I3(Q[2]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h80CF00AA800F00AA)) 
    \state[3]_i_1 
       (.I0(done),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(\delaycnt_reg[11] ),
        .O(\state_reg[0] ));
  LUT6 #(
    .INIT(64'h0000000000000040)) 
    \state[3]_i_2 
       (.I0(error),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\initWord_reg[20] ),
        .I4(Q[3]),
        .I5(Q[2]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h6666666666666660)) 
    \subState[0]_i_1 
       (.I0(\subState_reg_n_0_[0] ),
        .I1(\subState[1]_i_2_n_0 ),
        .I2(state[3]),
        .I3(state[2]),
        .I4(state[0]),
        .I5(state[1]),
        .O(\subState[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6A6A6A6A6A6A6A00)) 
    \subState[1]_i_1 
       (.I0(\subState_reg_n_0_[1] ),
        .I1(\subState[1]_i_2_n_0 ),
        .I2(\subState_reg_n_0_[0] ),
        .I3(\subState[1]_i_3_n_0 ),
        .I4(state[0]),
        .I5(state[1]),
        .O(\subState[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \subState[1]_i_2 
       (.I0(\sclCnt[6]_i_4_n_0 ),
        .I1(sclCnt_reg__0[6]),
        .O(\subState[1]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \subState[1]_i_3 
       (.I0(state[3]),
        .I1(state[2]),
        .O(\subState[1]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \subState_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\subState[0]_i_1_n_0 ),
        .Q(\subState_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \subState_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\subState[1]_i_1_n_0 ),
        .Q(\subState_reg_n_0_[1] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init
   (scl,
    sda,
    clk_out2,
    reset_n,
    SR);
  inout scl;
  inout sda;
  input clk_out2;
  input reset_n;
  input [0:0]SR;

  wire [0:0]SR;
  wire clk_out2;
  wire [6:6]data0;
  wire [7:0]data1;
  wire [7:0]data2;
  wire [7:0]data_i;
  wire \data_i[0]_i_1_n_0 ;
  wire \data_i[1]_i_1_n_0 ;
  wire \data_i[2]_i_1_n_0 ;
  wire \data_i[3]_i_1_n_0 ;
  wire \data_i[4]_i_1_n_0 ;
  wire \data_i[5]_i_1_n_0 ;
  wire \data_i[5]_i_2_n_0 ;
  wire \data_i[6]_i_1_n_0 ;
  wire \data_i[6]_i_2_n_0 ;
  wire \data_i[7]_i_1_n_0 ;
  wire delayEn;
  wire delayEn_i_1_n_0;
  wire delayEn_i_2_n_0;
  wire [31:0]delaycnt;
  wire delaycnt0;
  wire delaycnt0_carry__0_i_1_n_0;
  wire delaycnt0_carry__0_i_2_n_0;
  wire delaycnt0_carry__0_i_3_n_0;
  wire delaycnt0_carry__0_i_4_n_0;
  wire delaycnt0_carry__0_n_0;
  wire delaycnt0_carry__0_n_1;
  wire delaycnt0_carry__0_n_2;
  wire delaycnt0_carry__0_n_3;
  wire delaycnt0_carry__0_n_4;
  wire delaycnt0_carry__0_n_5;
  wire delaycnt0_carry__0_n_6;
  wire delaycnt0_carry__0_n_7;
  wire delaycnt0_carry__1_i_1_n_0;
  wire delaycnt0_carry__1_i_2_n_0;
  wire delaycnt0_carry__1_i_3_n_0;
  wire delaycnt0_carry__1_i_4_n_0;
  wire delaycnt0_carry__1_n_0;
  wire delaycnt0_carry__1_n_1;
  wire delaycnt0_carry__1_n_2;
  wire delaycnt0_carry__1_n_3;
  wire delaycnt0_carry__1_n_4;
  wire delaycnt0_carry__1_n_5;
  wire delaycnt0_carry__1_n_6;
  wire delaycnt0_carry__1_n_7;
  wire delaycnt0_carry__2_i_1_n_0;
  wire delaycnt0_carry__2_i_2_n_0;
  wire delaycnt0_carry__2_i_3_n_0;
  wire delaycnt0_carry__2_i_4_n_0;
  wire delaycnt0_carry__2_n_0;
  wire delaycnt0_carry__2_n_1;
  wire delaycnt0_carry__2_n_2;
  wire delaycnt0_carry__2_n_3;
  wire delaycnt0_carry__2_n_4;
  wire delaycnt0_carry__2_n_5;
  wire delaycnt0_carry__2_n_6;
  wire delaycnt0_carry__2_n_7;
  wire delaycnt0_carry__3_i_1_n_0;
  wire delaycnt0_carry__3_i_2_n_0;
  wire delaycnt0_carry__3_i_3_n_0;
  wire delaycnt0_carry__3_i_4_n_0;
  wire delaycnt0_carry__3_n_0;
  wire delaycnt0_carry__3_n_1;
  wire delaycnt0_carry__3_n_2;
  wire delaycnt0_carry__3_n_3;
  wire delaycnt0_carry__3_n_4;
  wire delaycnt0_carry__3_n_5;
  wire delaycnt0_carry__3_n_6;
  wire delaycnt0_carry__3_n_7;
  wire delaycnt0_carry__4_i_1_n_0;
  wire delaycnt0_carry__4_i_2_n_0;
  wire delaycnt0_carry__4_i_3_n_0;
  wire delaycnt0_carry__4_i_4_n_0;
  wire delaycnt0_carry__4_n_0;
  wire delaycnt0_carry__4_n_1;
  wire delaycnt0_carry__4_n_2;
  wire delaycnt0_carry__4_n_3;
  wire delaycnt0_carry__4_n_4;
  wire delaycnt0_carry__4_n_5;
  wire delaycnt0_carry__4_n_6;
  wire delaycnt0_carry__4_n_7;
  wire delaycnt0_carry__5_i_1_n_0;
  wire delaycnt0_carry__5_i_2_n_0;
  wire delaycnt0_carry__5_i_3_n_0;
  wire delaycnt0_carry__5_i_4_n_0;
  wire delaycnt0_carry__5_n_0;
  wire delaycnt0_carry__5_n_1;
  wire delaycnt0_carry__5_n_2;
  wire delaycnt0_carry__5_n_3;
  wire delaycnt0_carry__5_n_4;
  wire delaycnt0_carry__5_n_5;
  wire delaycnt0_carry__5_n_6;
  wire delaycnt0_carry__5_n_7;
  wire delaycnt0_carry__6_i_1_n_0;
  wire delaycnt0_carry__6_i_2_n_0;
  wire delaycnt0_carry__6_i_3_n_0;
  wire delaycnt0_carry__6_n_2;
  wire delaycnt0_carry__6_n_3;
  wire delaycnt0_carry__6_n_5;
  wire delaycnt0_carry__6_n_6;
  wire delaycnt0_carry__6_n_7;
  wire delaycnt0_carry_i_1_n_0;
  wire delaycnt0_carry_i_2_n_0;
  wire delaycnt0_carry_i_3_n_0;
  wire delaycnt0_carry_i_4_n_0;
  wire delaycnt0_carry_n_0;
  wire delaycnt0_carry_n_1;
  wire delaycnt0_carry_n_2;
  wire delaycnt0_carry_n_3;
  wire delaycnt0_carry_n_4;
  wire delaycnt0_carry_n_5;
  wire delaycnt0_carry_n_6;
  wire delaycnt0_carry_n_7;
  wire \delaycnt[0]_i_1_n_0 ;
  wire \initA[0]_i_1_n_0 ;
  wire \initA[6]_i_3_n_0 ;
  wire \initA_reg_n_0_[0] ;
  wire \initA_reg_n_0_[1] ;
  wire \initA_reg_n_0_[2] ;
  wire \initA_reg_n_0_[3] ;
  wire \initA_reg_n_0_[4] ;
  wire \initA_reg_n_0_[5] ;
  wire \initA_reg_n_0_[6] ;
  wire initEn;
  wire \initWord[0]_i_1_n_0 ;
  wire \initWord[10]_i_1_n_0 ;
  wire \initWord[11]_i_1_n_0 ;
  wire \initWord[12]_i_1_n_0 ;
  wire \initWord[13]_i_1_n_0 ;
  wire \initWord[14]_i_1_n_0 ;
  wire \initWord[15]_i_1_n_0 ;
  wire \initWord[16]_i_1_n_0 ;
  wire \initWord[17]_i_1_n_0 ;
  wire \initWord[18]_i_1_n_0 ;
  wire \initWord[19]_i_1_n_0 ;
  wire \initWord[20]_i_1_n_0 ;
  wire \initWord[21]_i_1_n_0 ;
  wire \initWord[23]_i_1_n_0 ;
  wire \initWord[30]_i_1_n_0 ;
  wire \initWord[30]_i_2_n_0 ;
  wire \initWord[30]_i_3_n_0 ;
  wire \initWord[6]_i_1_n_0 ;
  wire \initWord[8]_i_1_n_0 ;
  wire \initWord[9]_i_1_n_0 ;
  wire \initWord_reg_n_0_[0] ;
  wire \initWord_reg_n_0_[6] ;
  wire msg;
  wire msg0;
  wire [6:1]p_1_in__0;
  wire reset_n;
  wire scl;
  wire sda;
  wire \state[1]_i_2_n_0 ;
  wire \state[1]_i_3_n_0 ;
  wire \state[1]_i_4_n_0 ;
  wire \state[2]_i_2_n_0 ;
  wire \state[3]_i_10_n_0 ;
  wire \state[3]_i_11_n_0 ;
  wire \state[3]_i_12_n_0 ;
  wire \state[3]_i_13_n_0 ;
  wire \state[3]_i_3_n_0 ;
  wire \state[3]_i_4_n_0 ;
  wire \state[3]_i_5_n_0 ;
  wire \state[3]_i_6_n_0 ;
  wire \state[3]_i_7_n_0 ;
  wire \state[3]_i_8_n_0 ;
  wire \state[3]_i_9_n_0 ;
  wire \state_reg_n_0_[0] ;
  wire \state_reg_n_0_[1] ;
  wire \state_reg_n_0_[2] ;
  wire \state_reg_n_0_[3] ;
  wire stb;
  wire stb_i_1_n_0;
  wire twi_controller_n_0;
  wire twi_controller_n_1;
  wire twi_controller_n_2;
  wire twi_controller_n_3;
  wire twi_controller_n_4;
  wire twi_controller_n_5;
  wire twi_controller_n_6;
  wire [3:2]NLW_delaycnt0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_delaycnt0_carry__6_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hF8C83808FFFFFFFF)) 
    \data_i[0]_i_1 
       (.I0(data1[0]),
        .I1(\state_reg_n_0_[0] ),
        .I2(\state_reg_n_0_[1] ),
        .I3(data2[0]),
        .I4(\initWord_reg_n_0_[0] ),
        .I5(\state[1]_i_3_n_0 ),
        .O(\data_i[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0011100000001000)) 
    \data_i[1]_i_1 
       (.I0(\state_reg_n_0_[3] ),
        .I1(\state_reg_n_0_[2] ),
        .I2(data2[1]),
        .I3(\state_reg_n_0_[1] ),
        .I4(\state_reg_n_0_[0] ),
        .I5(data1[1]),
        .O(\data_i[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFDDF55555DDF555)) 
    \data_i[2]_i_1 
       (.I0(\state[1]_i_3_n_0 ),
        .I1(data1[2]),
        .I2(data2[2]),
        .I3(\state_reg_n_0_[1] ),
        .I4(\state_reg_n_0_[0] ),
        .I5(\initWord_reg_n_0_[6] ),
        .O(\data_i[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFDDF55555DDF555)) 
    \data_i[3]_i_1 
       (.I0(\state[1]_i_3_n_0 ),
        .I1(data1[3]),
        .I2(data2[3]),
        .I3(\state_reg_n_0_[1] ),
        .I4(\state_reg_n_0_[0] ),
        .I5(\initWord_reg_n_0_[6] ),
        .O(\data_i[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBCB08C8000000000)) 
    \data_i[4]_i_1 
       (.I0(\initWord_reg_n_0_[6] ),
        .I1(\state_reg_n_0_[0] ),
        .I2(\state_reg_n_0_[1] ),
        .I3(data1[4]),
        .I4(data2[4]),
        .I5(\state[1]_i_3_n_0 ),
        .O(\data_i[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \data_i[5]_i_1 
       (.I0(\state_reg_n_0_[2] ),
        .I1(\state_reg_n_0_[1] ),
        .I2(\state_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[3] ),
        .I4(reset_n),
        .O(\data_i[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFDDF55555DDF555)) 
    \data_i[5]_i_2 
       (.I0(\state[1]_i_3_n_0 ),
        .I1(data1[5]),
        .I2(data2[5]),
        .I3(\state_reg_n_0_[1] ),
        .I4(\state_reg_n_0_[0] ),
        .I5(\initWord_reg_n_0_[6] ),
        .O(\data_i[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hC000222200002222)) 
    \data_i[6]_i_1 
       (.I0(\data_i[6]_i_2_n_0 ),
        .I1(\state_reg_n_0_[3] ),
        .I2(\state_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[1] ),
        .I4(\state_reg_n_0_[2] ),
        .I5(data0),
        .O(\data_i[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_i[6]_i_2 
       (.I0(\initWord_reg_n_0_[6] ),
        .I1(data2[6]),
        .I2(\state_reg_n_0_[1] ),
        .I3(data1[7]),
        .I4(\state_reg_n_0_[0] ),
        .I5(data0),
        .O(\data_i[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000A000C0000)) 
    \data_i[7]_i_1 
       (.I0(data2[7]),
        .I1(data1[7]),
        .I2(\state_reg_n_0_[2] ),
        .I3(\state_reg_n_0_[3] ),
        .I4(\state_reg_n_0_[0] ),
        .I5(\state_reg_n_0_[1] ),
        .O(\data_i[7]_i_1_n_0 ));
  FDRE \data_i_reg[0] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[0]_i_1_n_0 ),
        .Q(data_i[0]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[1] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[1]_i_1_n_0 ),
        .Q(data_i[1]),
        .R(1'b0));
  FDRE \data_i_reg[2] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[2]_i_1_n_0 ),
        .Q(data_i[2]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[3] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[3]_i_1_n_0 ),
        .Q(data_i[3]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[4] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[4]_i_1_n_0 ),
        .Q(data_i[4]),
        .R(1'b0));
  FDRE \data_i_reg[5] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[5]_i_2_n_0 ),
        .Q(data_i[5]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[6] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[6]_i_1_n_0 ),
        .Q(data_i[6]),
        .R(1'b0));
  FDRE \data_i_reg[7] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[7]_i_1_n_0 ),
        .Q(data_i[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h2AAAEAAA00000000)) 
    delayEn_i_1
       (.I0(delayEn),
        .I1(delayEn_i_2_n_0),
        .I2(\state_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[1] ),
        .I4(\state[3]_i_3_n_0 ),
        .I5(reset_n),
        .O(delayEn_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h2)) 
    delayEn_i_2
       (.I0(\state_reg_n_0_[2] ),
        .I1(\state_reg_n_0_[3] ),
        .O(delayEn_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    delayEn_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(delayEn_i_1_n_0),
        .Q(delayEn),
        .R(1'b0));
  CARRY4 delaycnt0_carry
       (.CI(1'b0),
        .CO({delaycnt0_carry_n_0,delaycnt0_carry_n_1,delaycnt0_carry_n_2,delaycnt0_carry_n_3}),
        .CYINIT(delaycnt[0]),
        .DI(delaycnt[4:1]),
        .O({delaycnt0_carry_n_4,delaycnt0_carry_n_5,delaycnt0_carry_n_6,delaycnt0_carry_n_7}),
        .S({delaycnt0_carry_i_1_n_0,delaycnt0_carry_i_2_n_0,delaycnt0_carry_i_3_n_0,delaycnt0_carry_i_4_n_0}));
  CARRY4 delaycnt0_carry__0
       (.CI(delaycnt0_carry_n_0),
        .CO({delaycnt0_carry__0_n_0,delaycnt0_carry__0_n_1,delaycnt0_carry__0_n_2,delaycnt0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[8:5]),
        .O({delaycnt0_carry__0_n_4,delaycnt0_carry__0_n_5,delaycnt0_carry__0_n_6,delaycnt0_carry__0_n_7}),
        .S({delaycnt0_carry__0_i_1_n_0,delaycnt0_carry__0_i_2_n_0,delaycnt0_carry__0_i_3_n_0,delaycnt0_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_1
       (.I0(delaycnt[8]),
        .O(delaycnt0_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_2
       (.I0(delaycnt[7]),
        .O(delaycnt0_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_3
       (.I0(delaycnt[6]),
        .O(delaycnt0_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_4
       (.I0(delaycnt[5]),
        .O(delaycnt0_carry__0_i_4_n_0));
  CARRY4 delaycnt0_carry__1
       (.CI(delaycnt0_carry__0_n_0),
        .CO({delaycnt0_carry__1_n_0,delaycnt0_carry__1_n_1,delaycnt0_carry__1_n_2,delaycnt0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[12:9]),
        .O({delaycnt0_carry__1_n_4,delaycnt0_carry__1_n_5,delaycnt0_carry__1_n_6,delaycnt0_carry__1_n_7}),
        .S({delaycnt0_carry__1_i_1_n_0,delaycnt0_carry__1_i_2_n_0,delaycnt0_carry__1_i_3_n_0,delaycnt0_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_1
       (.I0(delaycnt[12]),
        .O(delaycnt0_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_2
       (.I0(delaycnt[11]),
        .O(delaycnt0_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_3
       (.I0(delaycnt[10]),
        .O(delaycnt0_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_4
       (.I0(delaycnt[9]),
        .O(delaycnt0_carry__1_i_4_n_0));
  CARRY4 delaycnt0_carry__2
       (.CI(delaycnt0_carry__1_n_0),
        .CO({delaycnt0_carry__2_n_0,delaycnt0_carry__2_n_1,delaycnt0_carry__2_n_2,delaycnt0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[16:13]),
        .O({delaycnt0_carry__2_n_4,delaycnt0_carry__2_n_5,delaycnt0_carry__2_n_6,delaycnt0_carry__2_n_7}),
        .S({delaycnt0_carry__2_i_1_n_0,delaycnt0_carry__2_i_2_n_0,delaycnt0_carry__2_i_3_n_0,delaycnt0_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_1
       (.I0(delaycnt[16]),
        .O(delaycnt0_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_2
       (.I0(delaycnt[15]),
        .O(delaycnt0_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_3
       (.I0(delaycnt[14]),
        .O(delaycnt0_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_4
       (.I0(delaycnt[13]),
        .O(delaycnt0_carry__2_i_4_n_0));
  CARRY4 delaycnt0_carry__3
       (.CI(delaycnt0_carry__2_n_0),
        .CO({delaycnt0_carry__3_n_0,delaycnt0_carry__3_n_1,delaycnt0_carry__3_n_2,delaycnt0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[20:17]),
        .O({delaycnt0_carry__3_n_4,delaycnt0_carry__3_n_5,delaycnt0_carry__3_n_6,delaycnt0_carry__3_n_7}),
        .S({delaycnt0_carry__3_i_1_n_0,delaycnt0_carry__3_i_2_n_0,delaycnt0_carry__3_i_3_n_0,delaycnt0_carry__3_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_1
       (.I0(delaycnt[20]),
        .O(delaycnt0_carry__3_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_2
       (.I0(delaycnt[19]),
        .O(delaycnt0_carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_3
       (.I0(delaycnt[18]),
        .O(delaycnt0_carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_4
       (.I0(delaycnt[17]),
        .O(delaycnt0_carry__3_i_4_n_0));
  CARRY4 delaycnt0_carry__4
       (.CI(delaycnt0_carry__3_n_0),
        .CO({delaycnt0_carry__4_n_0,delaycnt0_carry__4_n_1,delaycnt0_carry__4_n_2,delaycnt0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[24:21]),
        .O({delaycnt0_carry__4_n_4,delaycnt0_carry__4_n_5,delaycnt0_carry__4_n_6,delaycnt0_carry__4_n_7}),
        .S({delaycnt0_carry__4_i_1_n_0,delaycnt0_carry__4_i_2_n_0,delaycnt0_carry__4_i_3_n_0,delaycnt0_carry__4_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_1
       (.I0(delaycnt[24]),
        .O(delaycnt0_carry__4_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_2
       (.I0(delaycnt[23]),
        .O(delaycnt0_carry__4_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_3
       (.I0(delaycnt[22]),
        .O(delaycnt0_carry__4_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_4
       (.I0(delaycnt[21]),
        .O(delaycnt0_carry__4_i_4_n_0));
  CARRY4 delaycnt0_carry__5
       (.CI(delaycnt0_carry__4_n_0),
        .CO({delaycnt0_carry__5_n_0,delaycnt0_carry__5_n_1,delaycnt0_carry__5_n_2,delaycnt0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[28:25]),
        .O({delaycnt0_carry__5_n_4,delaycnt0_carry__5_n_5,delaycnt0_carry__5_n_6,delaycnt0_carry__5_n_7}),
        .S({delaycnt0_carry__5_i_1_n_0,delaycnt0_carry__5_i_2_n_0,delaycnt0_carry__5_i_3_n_0,delaycnt0_carry__5_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_1
       (.I0(delaycnt[28]),
        .O(delaycnt0_carry__5_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_2
       (.I0(delaycnt[27]),
        .O(delaycnt0_carry__5_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_3
       (.I0(delaycnt[26]),
        .O(delaycnt0_carry__5_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_4
       (.I0(delaycnt[25]),
        .O(delaycnt0_carry__5_i_4_n_0));
  CARRY4 delaycnt0_carry__6
       (.CI(delaycnt0_carry__5_n_0),
        .CO({NLW_delaycnt0_carry__6_CO_UNCONNECTED[3:2],delaycnt0_carry__6_n_2,delaycnt0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,delaycnt[30:29]}),
        .O({NLW_delaycnt0_carry__6_O_UNCONNECTED[3],delaycnt0_carry__6_n_5,delaycnt0_carry__6_n_6,delaycnt0_carry__6_n_7}),
        .S({1'b0,delaycnt0_carry__6_i_1_n_0,delaycnt0_carry__6_i_2_n_0,delaycnt0_carry__6_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__6_i_1
       (.I0(delaycnt[31]),
        .O(delaycnt0_carry__6_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__6_i_2
       (.I0(delaycnt[30]),
        .O(delaycnt0_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__6_i_3
       (.I0(delaycnt[29]),
        .O(delaycnt0_carry__6_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_1
       (.I0(delaycnt[4]),
        .O(delaycnt0_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_2
       (.I0(delaycnt[3]),
        .O(delaycnt0_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_3
       (.I0(delaycnt[2]),
        .O(delaycnt0_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_4
       (.I0(delaycnt[1]),
        .O(delaycnt0_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \delaycnt[0]_i_1 
       (.I0(delaycnt[0]),
        .O(\delaycnt[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \delaycnt[31]_i_1 
       (.I0(delayEn),
        .O(delaycnt0));
  FDRE \delaycnt_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\delaycnt[0]_i_1_n_0 ),
        .Q(delaycnt[0]),
        .R(delaycnt0));
  FDSE \delaycnt_reg[10] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_6),
        .Q(delaycnt[10]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[11] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_5),
        .Q(delaycnt[11]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[12] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_4),
        .Q(delaycnt[12]),
        .S(delaycnt0));
  FDRE \delaycnt_reg[13] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_7),
        .Q(delaycnt[13]),
        .R(delaycnt0));
  FDSE \delaycnt_reg[14] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_6),
        .Q(delaycnt[14]),
        .S(delaycnt0));
  FDRE \delaycnt_reg[15] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_5),
        .Q(delaycnt[15]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[16] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_4),
        .Q(delaycnt[16]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[17] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_7),
        .Q(delaycnt[17]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[18] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_6),
        .Q(delaycnt[18]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[19] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_5),
        .Q(delaycnt[19]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_7),
        .Q(delaycnt[1]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[20] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_4),
        .Q(delaycnt[20]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[21] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_7),
        .Q(delaycnt[21]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[22] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_6),
        .Q(delaycnt[22]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[23] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_5),
        .Q(delaycnt[23]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[24] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_4),
        .Q(delaycnt[24]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[25] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_7),
        .Q(delaycnt[25]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[26] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_6),
        .Q(delaycnt[26]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[27] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_5),
        .Q(delaycnt[27]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[28] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_4),
        .Q(delaycnt[28]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[29] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__6_n_7),
        .Q(delaycnt[29]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_6),
        .Q(delaycnt[2]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[30] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__6_n_6),
        .Q(delaycnt[30]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[31] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__6_n_5),
        .Q(delaycnt[31]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_5),
        .Q(delaycnt[3]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_4),
        .Q(delaycnt[4]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_7),
        .Q(delaycnt[5]),
        .R(delaycnt0));
  FDSE \delaycnt_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_6),
        .Q(delaycnt[6]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_5),
        .Q(delaycnt[7]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_4),
        .Q(delaycnt[8]),
        .S(delaycnt0));
  FDRE \delaycnt_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_7),
        .Q(delaycnt[9]),
        .R(delaycnt0));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \initA[0]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .O(\initA[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \initA[1]_i_1 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[0] ),
        .O(p_1_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \initA[2]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[1] ),
        .O(p_1_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \initA[3]_i_1 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[2] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[0] ),
        .O(p_1_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \initA[4]_i_1 
       (.I0(\initA_reg_n_0_[4] ),
        .I1(\initA_reg_n_0_[2] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[1] ),
        .I4(\initA_reg_n_0_[0] ),
        .O(p_1_in__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \initA[5]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[4] ),
        .I2(\initA_reg_n_0_[0] ),
        .I3(\initA_reg_n_0_[1] ),
        .I4(\initA_reg_n_0_[3] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(p_1_in__0[5]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \initA[6]_i_2 
       (.I0(\initA_reg_n_0_[6] ),
        .I1(\initA_reg_n_0_[5] ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[3] ),
        .I4(\initWord[30]_i_3_n_0 ),
        .I5(\initA_reg_n_0_[4] ),
        .O(p_1_in__0[6]));
  LUT5 #(
    .INIT(32'h80000000)) 
    \initA[6]_i_3 
       (.I0(\state[3]_i_3_n_0 ),
        .I1(initEn),
        .I2(\state_reg_n_0_[2] ),
        .I3(\state_reg_n_0_[1] ),
        .I4(\state_reg_n_0_[0] ),
        .O(\initA[6]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[0] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(\initA[0]_i_1_n_0 ),
        .Q(\initA_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[1] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(p_1_in__0[1]),
        .Q(\initA_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[2] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(p_1_in__0[2]),
        .Q(\initA_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[3] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(p_1_in__0[3]),
        .Q(\initA_reg_n_0_[3] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[4] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(p_1_in__0[4]),
        .Q(\initA_reg_n_0_[4] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[5] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(p_1_in__0[5]),
        .Q(\initA_reg_n_0_[5] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[6] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(p_1_in__0[6]),
        .Q(\initA_reg_n_0_[6] ),
        .R(SR));
  FDRE initEn_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(twi_controller_n_6),
        .Q(initEn),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hA8)) 
    \initWord[0]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[1] ),
        .O(\initWord[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000054042082)) 
    \initWord[10]_i_1 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[0] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[5] ),
        .O(\initWord[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h11110010)) 
    \initWord[11]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[4] ),
        .I4(\initA_reg_n_0_[5] ),
        .O(\initWord[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000006)) 
    \initWord[12]_i_1 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[2] ),
        .I2(\initA_reg_n_0_[4] ),
        .I3(\initA_reg_n_0_[5] ),
        .I4(\initA_reg_n_0_[1] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hC1C4C0C0C0C9C8C8)) 
    \initWord[13]_i_1 
       (.I0(\initA_reg_n_0_[4] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[5] ),
        .I3(\initA_reg_n_0_[0] ),
        .I4(\initA_reg_n_0_[3] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(\initWord[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000100C6100)) 
    \initWord[14]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[3] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[5] ),
        .O(\initWord[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0100000901080000)) 
    \initWord[15]_i_1 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[2] ),
        .I2(\initA_reg_n_0_[5] ),
        .I3(\initA_reg_n_0_[4] ),
        .I4(\initA_reg_n_0_[1] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h006900E00085008F)) 
    \initWord[16]_i_1 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[2] ),
        .I2(\initA_reg_n_0_[0] ),
        .I3(\initA_reg_n_0_[5] ),
        .I4(\initA_reg_n_0_[1] ),
        .I5(\initA_reg_n_0_[4] ),
        .O(\initWord[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1E1F54041F0B0450)) 
    \initWord[17]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[4] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[3] ),
        .I4(\initA_reg_n_0_[0] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(\initWord[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF00F6AAFF008991)) 
    \initWord[18]_i_1 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[2] ),
        .I2(\initA_reg_n_0_[0] ),
        .I3(\initA_reg_n_0_[1] ),
        .I4(\initA_reg_n_0_[5] ),
        .I5(\initA_reg_n_0_[4] ),
        .O(\initWord[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAA7DAA16AA7CAA1C)) 
    \initWord[19]_i_1 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[2] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[5] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FC01039F)) 
    \initWord[20]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[3] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[5] ),
        .O(\initWord[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h3113311211121010)) 
    \initWord[21]_i_1 
       (.I0(\initA_reg_n_0_[4] ),
        .I1(\initA_reg_n_0_[5] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[2] ),
        .I4(\initA_reg_n_0_[0] ),
        .I5(\initA_reg_n_0_[1] ),
        .O(\initWord[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4011010040000100)) 
    \initWord[23]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[4] ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[3] ),
        .I4(\initA_reg_n_0_[1] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000055555557)) 
    \initWord[30]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initWord[30]_i_3_n_0 ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[3] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[6] ),
        .O(\initWord[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \initWord[30]_i_2 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[5] ),
        .O(\initWord[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \initWord[30]_i_3 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[0] ),
        .O(\initWord[30]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \initWord[6]_i_1 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[5] ),
        .I2(\initA_reg_n_0_[0] ),
        .O(\initWord[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hDFFD11FFCEEE22FF)) 
    \initWord[8]_i_1 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[5] ),
        .I2(\initA_reg_n_0_[4] ),
        .I3(\initA_reg_n_0_[0] ),
        .I4(\initA_reg_n_0_[1] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(\initWord[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0003000C00D400BC)) 
    \initWord[9]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[5] ),
        .I4(\initA_reg_n_0_[1] ),
        .I5(\initA_reg_n_0_[4] ),
        .O(\initWord[9]_i_1_n_0 ));
  FDRE \initWord_reg[0] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[0]_i_1_n_0 ),
        .Q(\initWord_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \initWord_reg[10] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[10]_i_1_n_0 ),
        .Q(data2[2]),
        .R(1'b0));
  FDRE \initWord_reg[11] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[11]_i_1_n_0 ),
        .Q(data2[3]),
        .R(1'b0));
  FDRE \initWord_reg[12] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[12]_i_1_n_0 ),
        .Q(data2[4]),
        .R(1'b0));
  FDRE \initWord_reg[13] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[13]_i_1_n_0 ),
        .Q(data2[5]),
        .R(1'b0));
  FDRE \initWord_reg[14] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[14]_i_1_n_0 ),
        .Q(data2[6]),
        .R(1'b0));
  FDRE \initWord_reg[15] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[15]_i_1_n_0 ),
        .Q(data2[7]),
        .R(1'b0));
  FDRE \initWord_reg[16] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[16]_i_1_n_0 ),
        .Q(data1[0]),
        .R(1'b0));
  FDRE \initWord_reg[17] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[17]_i_1_n_0 ),
        .Q(data1[1]),
        .R(1'b0));
  FDRE \initWord_reg[18] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[18]_i_1_n_0 ),
        .Q(data1[2]),
        .R(1'b0));
  FDRE \initWord_reg[19] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[19]_i_1_n_0 ),
        .Q(data1[3]),
        .R(1'b0));
  FDRE \initWord_reg[20] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[20]_i_1_n_0 ),
        .Q(data1[4]),
        .R(1'b0));
  FDRE \initWord_reg[21] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[21]_i_1_n_0 ),
        .Q(data1[5]),
        .R(1'b0));
  FDRE \initWord_reg[23] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[23]_i_1_n_0 ),
        .Q(data1[7]),
        .R(1'b0));
  FDRE \initWord_reg[30] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[30]_i_2_n_0 ),
        .Q(data0),
        .R(1'b0));
  FDRE \initWord_reg[6] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[6]_i_1_n_0 ),
        .Q(\initWord_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \initWord_reg[8] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[8]_i_1_n_0 ),
        .Q(data2[0]),
        .R(1'b0));
  FDRE \initWord_reg[9] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[9]_i_1_n_0 ),
        .Q(data2[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    msg_i_1
       (.I0(\state_reg_n_0_[3] ),
        .I1(\state_reg_n_0_[2] ),
        .I2(\state_reg_n_0_[1] ),
        .I3(\state_reg_n_0_[0] ),
        .O(msg0));
  FDRE msg_reg
       (.C(clk_out2),
        .CE(reset_n),
        .D(msg0),
        .Q(msg),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEFFFFF)) 
    \state[1]_i_2 
       (.I0(\state[2]_i_2_n_0 ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[6] ),
        .I4(\initA_reg_n_0_[5] ),
        .I5(\state[1]_i_4_n_0 ),
        .O(\state[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \state[1]_i_3 
       (.I0(\state_reg_n_0_[2] ),
        .I1(\state_reg_n_0_[3] ),
        .O(\state[1]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \state[1]_i_4 
       (.I0(\initA_reg_n_0_[4] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[2] ),
        .O(\state[1]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0EFF)) 
    \state[2]_i_2 
       (.I0(\initWord_reg_n_0_[0] ),
        .I1(\initWord_reg_n_0_[6] ),
        .I2(\state_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[1] ),
        .O(\state[2]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_10 
       (.I0(delaycnt[13]),
        .I1(delaycnt[12]),
        .I2(delaycnt[15]),
        .I3(delaycnt[14]),
        .O(\state[3]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_11 
       (.I0(delaycnt[4]),
        .I1(delaycnt[5]),
        .I2(delaycnt[7]),
        .I3(delaycnt[6]),
        .O(\state[3]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_12 
       (.I0(delaycnt[25]),
        .I1(delaycnt[24]),
        .I2(delaycnt[27]),
        .I3(delaycnt[26]),
        .O(\state[3]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_13 
       (.I0(delaycnt[22]),
        .I1(delaycnt[23]),
        .I2(delaycnt[21]),
        .I3(delaycnt[20]),
        .O(\state[3]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \state[3]_i_3 
       (.I0(\state[3]_i_5_n_0 ),
        .I1(\state[3]_i_6_n_0 ),
        .I2(\state[3]_i_7_n_0 ),
        .I3(\state[3]_i_8_n_0 ),
        .O(\state[3]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_4 
       (.I0(data1[4]),
        .I1(data1[3]),
        .I2(data1[7]),
        .I3(\state[3]_i_9_n_0 ),
        .O(\state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \state[3]_i_5 
       (.I0(delaycnt[11]),
        .I1(delaycnt[10]),
        .I2(delaycnt[8]),
        .I3(delaycnt[9]),
        .I4(\state[3]_i_10_n_0 ),
        .O(\state[3]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \state[3]_i_6 
       (.I0(delaycnt[2]),
        .I1(delaycnt[3]),
        .I2(delaycnt[0]),
        .I3(delaycnt[1]),
        .I4(\state[3]_i_11_n_0 ),
        .O(\state[3]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \state[3]_i_7 
       (.I0(delaycnt[30]),
        .I1(delaycnt[31]),
        .I2(delaycnt[29]),
        .I3(delaycnt[28]),
        .I4(\state[3]_i_12_n_0 ),
        .O(\state[3]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \state[3]_i_8 
       (.I0(delaycnt[18]),
        .I1(delaycnt[19]),
        .I2(delaycnt[17]),
        .I3(delaycnt[16]),
        .I4(\state[3]_i_13_n_0 ),
        .O(\state[3]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \state[3]_i_9 
       (.I0(data1[0]),
        .I1(data1[5]),
        .I2(data1[1]),
        .I3(data1[2]),
        .O(\state[3]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[0] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(twi_controller_n_3),
        .Q(\state_reg_n_0_[0] ),
        .R(SR));
  FDSE #(
    .INIT(1'b1)) 
    \state_reg[1] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(twi_controller_n_2),
        .Q(\state_reg_n_0_[1] ),
        .S(SR));
  FDSE #(
    .INIT(1'b1)) 
    \state_reg[2] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(twi_controller_n_1),
        .Q(\state_reg_n_0_[2] ),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[3] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(twi_controller_n_0),
        .Q(\state_reg_n_0_[3] ),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h8055)) 
    stb_i_1
       (.I0(\state_reg_n_0_[3] ),
        .I1(\state_reg_n_0_[0] ),
        .I2(\state_reg_n_0_[1] ),
        .I3(\state_reg_n_0_[2] ),
        .O(stb_i_1_n_0));
  FDRE stb_reg
       (.C(clk_out2),
        .CE(reset_n),
        .D(stb_i_1_n_0),
        .Q(stb),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl twi_controller
       (.D({twi_controller_n_0,twi_controller_n_1,twi_controller_n_2,twi_controller_n_3}),
        .E(twi_controller_n_4),
        .Q({\state_reg_n_0_[3] ,\state_reg_n_0_[2] ,\state_reg_n_0_[1] ,\state_reg_n_0_[0] }),
        .clk_out2(clk_out2),
        .data_i(data_i),
        .\delaycnt_reg[11] (\state[3]_i_3_n_0 ),
        .\initA_reg[0] (\state[1]_i_2_n_0 ),
        .initEn(initEn),
        .initEn_reg(twi_controller_n_6),
        .initEn_reg_0(\initA[6]_i_3_n_0 ),
        .\initWord_reg[0] (\state[2]_i_2_n_0 ),
        .\initWord_reg[20] (\state[3]_i_4_n_0 ),
        .msg(msg),
        .reset_n(reset_n),
        .scl(scl),
        .sda(sda),
        .\state_reg[0] (twi_controller_n_5),
        .\state_reg[2] (\state[1]_i_3_n_0 ),
        .stb(stb));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
   (clk_out1,
    clk_out2,
    clk_out3,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  input resetn;
  input clk_in1;
  input lopt;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire clk_out3;
  wire lopt;
  wire resetn;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .clk_out3(clk_out3),
        .lopt(lopt),
        .resetn(resetn));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz
   (clk_out1,
    clk_out2,
    clk_out3,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  input resetn;
  input clk_in1;
  input lopt;

  wire clk_in1;
  wire clk_out1;
  wire clk_out1_clk_wiz_0;
  wire clk_out2;
  wire clk_out2_clk_wiz_0;
  wire clk_out3;
  wire clk_out3_clk_wiz_0;
  wire clkfbout_buf_clk_wiz_0;
  wire clkfbout_clk_wiz_0;
  wire lopt;
  wire reset_high;
  wire resetn;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_LOCKED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_0),
        .O(clkfbout_buf_clk_wiz_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_0),
        .O(clk_out1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_0),
        .O(clk_out2));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout3_buf
       (.I(clk_out3_clk_wiz_0),
        .O(clk_out3));
  (* BOX_TYPE = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(40.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(8),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_0),
        .CLKFBOUT(clkfbout_clk_wiz_0),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(lopt),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_0),
        .CLKOUT1B(clk_out3_clk_wiz_0),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(NLW_mmcm_adv_inst_LOCKED_UNCONNECTED),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(reset_high));
  LUT1 #(
    .INIT(2'h1)) 
    mmcm_adv_inst_i_1
       (.I0(resetn),
        .O(reset_high));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1
   (clk_out1,
    clk_out2,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  input resetn;
  input clk_in1;
  output lopt;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire lopt;
  wire resetn;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .lopt(lopt),
        .resetn(resetn));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz
   (clk_out1,
    clk_out2,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  input resetn;
  input clk_in1;
  output lopt;

  wire clk_in1;
  wire clk_in1_clk_wiz_1;
  wire clk_out1;
  wire clk_out1_clk_wiz_1;
  wire clk_out2;
  wire clk_out2_clk_wiz_1;
  wire clkfbout_buf_clk_wiz_1;
  wire clkfbout_clk_wiz_1;
  wire reset_high;
  wire resetn;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_LOCKED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  assign lopt = clk_in1_clk_wiz_1;
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_1),
        .O(clkfbout_buf_clk_wiz_1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibufg
       (.I(clk_in1),
        .O(clk_in1_clk_wiz_1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_1),
        .O(clk_out1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_1),
        .O(clk_out2));
  (* BOX_TYPE = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(81.375000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(20),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_1),
        .CLKFBOUT(clkfbout_clk_wiz_1),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1_clk_wiz_1),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_1),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(NLW_mmcm_adv_inst_LOCKED_UNCONNECTED),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(reset_high));
  LUT1 #(
    .INIT(2'h1)) 
    mmcm_adv_inst_i_1
       (.I0(resetn),
        .O(reset_high));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_my_oscope_0_0,my_oscope_ip_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "my_oscope_ip_v1_0,Vivado 2017.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    reset_n,
    ac_mclk,
    ac_adc_sdata,
    ac_dac_sdata,
    ac_bclk,
    ac_lrclk,
    scl,
    sda,
    tmds,
    tmdsb,
    switch,
    btn,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1" *) input clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 reset_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME reset_n, POLARITY ACTIVE_LOW" *) input reset_n;
  output ac_mclk;
  input ac_adc_sdata;
  output ac_dac_sdata;
  output ac_bclk;
  output ac_lrclk;
  inout scl;
  inout sda;
  output [3:0]tmds;
  output [3:0]tmdsb;
  input [3:0]switch;
  input [4:0]btn;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 32, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 7, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [6:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [6:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire [4:0]btn;
  (* IBUF_LOW_PWR *) wire clk;
  wire reset_n;
  wire s00_axi_aclk;
  wire [6:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [6:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire scl;
  wire sda;
  wire [3:0]switch;
  (* SLEW = "SLOW" *) wire [3:0]tmds;
  (* SLEW = "SLOW" *) wire [3:0]tmdsb;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .btn(btn),
        .clk(clk),
        .reset_n(reset_n),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[6:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[6:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .scl(scl),
        .sda(sda),
        .switch(switch),
        .tmds(tmds),
        .tmdsb(tmdsb));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid
   (red_s,
    green_s,
    blue_s,
    clock_s,
    \encoded_reg[8] ,
    Q,
    \dc_bias_reg[0] ,
    \dc_bias_reg[0]_0 ,
    \dc_bias_reg[2] ,
    \dc_bias_reg[1] ,
    \dc_bias_reg[1]_0 ,
    \dc_bias_reg[1]_1 ,
    \dc_bias_reg[1]_2 ,
    \dc_bias_reg[1]_3 ,
    \dc_bias_reg[1]_4 ,
    \dc_bias_reg[2]_0 ,
    \dc_bias_reg[1]_5 ,
    \dc_bias_reg[1]_6 ,
    \dc_bias_reg[1]_7 ,
    \dc_bias_reg[1]_8 ,
    \encoded_reg[3] ,
    \encoded_reg[8]_0 ,
    clk_out2,
    clk_out3,
    CLK,
    \dc_bias_reg[3] ,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \processQ_reg[6] ,
    \dc_bias_reg[3]_2 ,
    \dc_bias_reg[3]_3 ,
    \dc_bias_reg[3]_4 ,
    \dc_bias_reg[3]_5 ,
    \processQ_reg[5] ,
    \processQ_reg[5]_0 ,
    \processQ_reg[1] ,
    \processQ_reg[1]_0 ,
    \processQ_reg[5]_1 ,
    \processQ_reg[1]_1 ,
    \processQ_reg[9] ,
    \trigger_volt_s_reg[6] ,
    \trigger_time_s_reg[7] ,
    \processQ_reg[1]_2 ,
    \dc_bias_reg[0]_1 ,
    SR,
    D);
  output red_s;
  output green_s;
  output blue_s;
  output clock_s;
  output \encoded_reg[8] ;
  output [0:0]Q;
  output [2:0]\dc_bias_reg[0] ;
  output [0:0]\dc_bias_reg[0]_0 ;
  output \dc_bias_reg[2] ;
  output \dc_bias_reg[1] ;
  output \dc_bias_reg[1]_0 ;
  output \dc_bias_reg[1]_1 ;
  output \dc_bias_reg[1]_2 ;
  output \dc_bias_reg[1]_3 ;
  output \dc_bias_reg[1]_4 ;
  output \dc_bias_reg[2]_0 ;
  output \dc_bias_reg[1]_5 ;
  output \dc_bias_reg[1]_6 ;
  output \dc_bias_reg[1]_7 ;
  output \dc_bias_reg[1]_8 ;
  output \encoded_reg[3] ;
  output \encoded_reg[8]_0 ;
  input clk_out2;
  input clk_out3;
  input CLK;
  input [0:0]\dc_bias_reg[3] ;
  input \dc_bias_reg[3]_0 ;
  input \dc_bias_reg[3]_1 ;
  input \processQ_reg[6] ;
  input \dc_bias_reg[3]_2 ;
  input \dc_bias_reg[3]_3 ;
  input \dc_bias_reg[3]_4 ;
  input \dc_bias_reg[3]_5 ;
  input \processQ_reg[5] ;
  input \processQ_reg[5]_0 ;
  input \processQ_reg[1] ;
  input \processQ_reg[1]_0 ;
  input \processQ_reg[5]_1 ;
  input \processQ_reg[1]_1 ;
  input \processQ_reg[9] ;
  input [4:0]\trigger_volt_s_reg[6] ;
  input [7:0]\trigger_time_s_reg[7] ;
  input \processQ_reg[1]_2 ;
  input \dc_bias_reg[0]_1 ;
  input [0:0]SR;
  input [0:0]D;

  wire CLK;
  wire [0:0]D;
  wire D0;
  wire D1;
  wire [0:0]Q;
  wire [0:0]SR;
  wire TDMS_encoder_blue_n_0;
  wire TDMS_encoder_blue_n_1;
  wire TDMS_encoder_blue_n_2;
  wire TDMS_encoder_blue_n_3;
  wire TDMS_encoder_blue_n_4;
  wire TDMS_encoder_blue_n_5;
  wire TDMS_encoder_green_n_0;
  wire TDMS_encoder_green_n_1;
  wire TDMS_encoder_green_n_2;
  wire TDMS_encoder_green_n_3;
  wire TDMS_encoder_red_n_0;
  wire TDMS_encoder_red_n_1;
  wire TDMS_encoder_red_n_2;
  wire TDMS_encoder_red_n_3;
  wire TDMS_encoder_red_n_4;
  wire blue_s;
  wire clk_out2;
  wire clk_out3;
  wire clock_s;
  wire [7:0]data1;
  wire [2:0]\dc_bias_reg[0] ;
  wire [0:0]\dc_bias_reg[0]_0 ;
  wire \dc_bias_reg[0]_1 ;
  wire \dc_bias_reg[1] ;
  wire \dc_bias_reg[1]_0 ;
  wire \dc_bias_reg[1]_1 ;
  wire \dc_bias_reg[1]_2 ;
  wire \dc_bias_reg[1]_3 ;
  wire \dc_bias_reg[1]_4 ;
  wire \dc_bias_reg[1]_5 ;
  wire \dc_bias_reg[1]_6 ;
  wire \dc_bias_reg[1]_7 ;
  wire \dc_bias_reg[1]_8 ;
  wire \dc_bias_reg[2] ;
  wire \dc_bias_reg[2]_0 ;
  wire [0:0]\dc_bias_reg[3] ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire \dc_bias_reg[3]_3 ;
  wire \dc_bias_reg[3]_4 ;
  wire \dc_bias_reg[3]_5 ;
  wire \encoded_reg[3] ;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire green_s;
  wire [9:0]latched_blue;
  wire [9:0]latched_green;
  wire [9:0]latched_red;
  wire \processQ_reg[1] ;
  wire \processQ_reg[1]_0 ;
  wire \processQ_reg[1]_1 ;
  wire \processQ_reg[1]_2 ;
  wire \processQ_reg[5] ;
  wire \processQ_reg[5]_0 ;
  wire \processQ_reg[5]_1 ;
  wire \processQ_reg[6] ;
  wire \processQ_reg[9] ;
  wire red_s;
  wire [6:0]shift_blue;
  wire \shift_blue[1]_i_1_n_0 ;
  wire \shift_blue[3]_i_1_n_0 ;
  wire \shift_blue[5]_i_1_n_0 ;
  wire \shift_blue[7]_i_1_n_0 ;
  wire \shift_blue[7]_i_2_n_0 ;
  wire \shift_blue_reg_n_0_[0] ;
  wire \shift_blue_reg_n_0_[1] ;
  wire \shift_blue_reg_n_0_[2] ;
  wire \shift_blue_reg_n_0_[3] ;
  wire \shift_blue_reg_n_0_[4] ;
  wire \shift_blue_reg_n_0_[5] ;
  wire \shift_blue_reg_n_0_[6] ;
  wire \shift_blue_reg_n_0_[7] ;
  wire \shift_blue_reg_n_0_[8] ;
  wire \shift_blue_reg_n_0_[9] ;
  wire [1:0]shift_clock;
  wire [9:2]shift_clock__0;
  wire [6:2]shift_green;
  wire \shift_green[0]_i_1_n_0 ;
  wire \shift_green[1]_i_1_n_0 ;
  wire \shift_green[3]_i_1_n_0 ;
  wire \shift_green[5]_i_1_n_0 ;
  wire \shift_green[7]_i_1_n_0 ;
  wire \shift_green[7]_i_2_n_0 ;
  wire \shift_green_reg_n_0_[0] ;
  wire \shift_green_reg_n_0_[1] ;
  wire \shift_green_reg_n_0_[2] ;
  wire \shift_green_reg_n_0_[3] ;
  wire \shift_green_reg_n_0_[4] ;
  wire \shift_green_reg_n_0_[5] ;
  wire \shift_green_reg_n_0_[6] ;
  wire \shift_green_reg_n_0_[7] ;
  wire \shift_green_reg_n_0_[8] ;
  wire \shift_green_reg_n_0_[9] ;
  wire [7:0]shift_red;
  wire \shift_red[9]_i_1_n_0 ;
  wire \shift_red[9]_i_2_n_0 ;
  wire [7:0]\trigger_time_s_reg[7] ;
  wire [4:0]\trigger_volt_s_reg[6] ;
  wire NLW_ODDR2_blue_R_UNCONNECTED;
  wire NLW_ODDR2_blue_S_UNCONNECTED;
  wire NLW_ODDR2_clock_R_UNCONNECTED;
  wire NLW_ODDR2_clock_S_UNCONNECTED;
  wire NLW_ODDR2_green_R_UNCONNECTED;
  wire NLW_ODDR2_green_S_UNCONNECTED;
  wire NLW_ODDR2_red_R_UNCONNECTED;
  wire NLW_ODDR2_red_S_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_blue
       (.C(clk_out2),
        .CE(1'b1),
        .D1(\shift_blue_reg_n_0_[0] ),
        .D2(\shift_blue_reg_n_0_[1] ),
        .Q(blue_s),
        .R(NLW_ODDR2_blue_R_UNCONNECTED),
        .S(NLW_ODDR2_blue_S_UNCONNECTED));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_clock
       (.C(clk_out2),
        .CE(1'b1),
        .D1(shift_clock[0]),
        .D2(shift_clock[1]),
        .Q(clock_s),
        .R(NLW_ODDR2_clock_R_UNCONNECTED),
        .S(NLW_ODDR2_clock_S_UNCONNECTED));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_green
       (.C(clk_out2),
        .CE(1'b1),
        .D1(\shift_green_reg_n_0_[0] ),
        .D2(\shift_green_reg_n_0_[1] ),
        .Q(green_s),
        .R(NLW_ODDR2_green_R_UNCONNECTED),
        .S(NLW_ODDR2_green_S_UNCONNECTED));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_red
       (.C(clk_out2),
        .CE(1'b1),
        .D1(D0),
        .D2(D1),
        .Q(red_s),
        .R(NLW_ODDR2_red_R_UNCONNECTED),
        .S(NLW_ODDR2_red_S_UNCONNECTED));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder TDMS_encoder_blue
       (.CLK(CLK),
        .D({TDMS_encoder_blue_n_0,TDMS_encoder_blue_n_1,TDMS_encoder_blue_n_2,TDMS_encoder_blue_n_3,TDMS_encoder_blue_n_4,TDMS_encoder_blue_n_5}),
        .Q(\dc_bias_reg[0] ),
        .SR(SR),
        .\dc_bias_reg[0]_0 (\dc_bias_reg[0]_1 ),
        .\dc_bias_reg[1]_0 (\dc_bias_reg[1] ),
        .\dc_bias_reg[1]_1 (\dc_bias_reg[1]_0 ),
        .\dc_bias_reg[1]_10 (D),
        .\dc_bias_reg[1]_2 (\dc_bias_reg[1]_1 ),
        .\dc_bias_reg[1]_3 (\dc_bias_reg[1]_2 ),
        .\dc_bias_reg[1]_4 (\dc_bias_reg[1]_3 ),
        .\dc_bias_reg[1]_5 (\dc_bias_reg[1]_4 ),
        .\dc_bias_reg[1]_6 (\dc_bias_reg[1]_5 ),
        .\dc_bias_reg[1]_7 (\dc_bias_reg[1]_6 ),
        .\dc_bias_reg[1]_8 (\dc_bias_reg[1]_7 ),
        .\dc_bias_reg[1]_9 (\dc_bias_reg[1]_8 ),
        .\dc_bias_reg[2]_0 (\dc_bias_reg[2] ),
        .\dc_bias_reg[2]_1 (\dc_bias_reg[2]_0 ),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_2 ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_3 ),
        .\dc_bias_reg[3]_2 (\dc_bias_reg[3]_4 ),
        .\dc_bias_reg[3]_3 (\dc_bias_reg[3]_5 ),
        .\encoded_reg[8]_0 (\encoded_reg[8] ),
        .\encoded_reg[8]_1 (\encoded_reg[8]_0 ),
        .\processQ_reg[1] (\processQ_reg[1] ),
        .\processQ_reg[1]_0 (\processQ_reg[1]_0 ),
        .\processQ_reg[5] (\processQ_reg[5] ),
        .\processQ_reg[5]_0 (\processQ_reg[5]_1 ),
        .\processQ_reg[5]_1 (\processQ_reg[5]_0 ),
        .\processQ_reg[6] (\processQ_reg[6] ),
        .\processQ_reg[9] (\processQ_reg[9] ),
        .\trigger_time_s_reg[7] (\trigger_time_s_reg[7] ),
        .\trigger_volt_s_reg[6] (\trigger_volt_s_reg[6] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1 TDMS_encoder_green
       (.CLK(CLK),
        .D({TDMS_encoder_green_n_0,TDMS_encoder_green_n_1,TDMS_encoder_green_n_2,TDMS_encoder_green_n_3}),
        .Q(Q),
        .SR(SR),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_1 ),
        .\processQ_reg[1] (\processQ_reg[1] ),
        .\processQ_reg[5] (\processQ_reg[5]_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2 TDMS_encoder_red
       (.CLK(CLK),
        .D({TDMS_encoder_red_n_0,TDMS_encoder_red_n_1,TDMS_encoder_red_n_2,TDMS_encoder_red_n_3,TDMS_encoder_red_n_4}),
        .Q(\dc_bias_reg[0]_0 ),
        .SR(SR),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3] ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_0 ),
        .\encoded_reg[3]_0 (\encoded_reg[3] ),
        .\processQ_reg[1] (\processQ_reg[1]_1 ),
        .\processQ_reg[1]_0 (\processQ_reg[1]_2 ),
        .\processQ_reg[5] (\processQ_reg[5]_0 ),
        .\processQ_reg[9] (\processQ_reg[9] ));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_5),
        .Q(latched_blue[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_4),
        .Q(latched_blue[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_3),
        .Q(latched_blue[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_2),
        .Q(latched_blue[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_1),
        .Q(latched_blue[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_0),
        .Q(latched_blue[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_3),
        .Q(latched_green[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_2),
        .Q(latched_green[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_1),
        .Q(latched_green[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_0),
        .Q(latched_green[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_4),
        .Q(latched_red[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_3),
        .Q(latched_red[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_2),
        .Q(latched_red[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_1),
        .Q(latched_red[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_0),
        .Q(latched_red[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[0]_i_1 
       (.I0(\shift_blue_reg_n_0_[2] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[0]),
        .O(shift_blue[0]));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[1]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[3] ),
        .O(\shift_blue[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[2]_i_1 
       (.I0(\shift_blue_reg_n_0_[4] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[2]),
        .O(shift_blue[2]));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[3]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[5] ),
        .O(\shift_blue[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[4]_i_1 
       (.I0(\shift_blue_reg_n_0_[6] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[4]),
        .O(shift_blue[4]));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[5]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[7] ),
        .O(\shift_blue[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[6]_i_1 
       (.I0(\shift_blue_reg_n_0_[8] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[2]),
        .O(shift_blue[6]));
  LUT2 #(
    .INIT(4'h2)) 
    \shift_blue[7]_i_1 
       (.I0(latched_blue[1]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .O(\shift_blue[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[7]_i_2 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[9] ),
        .O(\shift_blue[7]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[0]),
        .Q(\shift_blue_reg_n_0_[0] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[1]_i_1_n_0 ),
        .Q(\shift_blue_reg_n_0_[1] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[2]),
        .Q(\shift_blue_reg_n_0_[2] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[3]_i_1_n_0 ),
        .Q(\shift_blue_reg_n_0_[3] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[4]),
        .Q(\shift_blue_reg_n_0_[4] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[5]_i_1_n_0 ),
        .Q(\shift_blue_reg_n_0_[5] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[6]),
        .Q(\shift_blue_reg_n_0_[6] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[7]_i_2_n_0 ),
        .Q(\shift_blue_reg_n_0_[7] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_blue[8]),
        .Q(\shift_blue_reg_n_0_[8] ),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_blue[9]),
        .Q(\shift_blue_reg_n_0_[9] ),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[2]),
        .Q(shift_clock[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[3]),
        .Q(shift_clock[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[4]),
        .Q(shift_clock__0[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[5]),
        .Q(shift_clock__0[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[6]),
        .Q(shift_clock__0[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[7]),
        .Q(shift_clock__0[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[8]),
        .Q(shift_clock__0[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[9]),
        .Q(shift_clock__0[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock[0]),
        .Q(shift_clock__0[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock[1]),
        .Q(shift_clock__0[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[0]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[2] ),
        .O(\shift_green[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[1]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[3] ),
        .O(\shift_green[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[2]_i_1 
       (.I0(\shift_green_reg_n_0_[4] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_green[2]),
        .O(shift_green[2]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[3]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[5] ),
        .O(\shift_green[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[4]_i_1 
       (.I0(\shift_green_reg_n_0_[6] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_green[2]),
        .O(shift_green[4]));
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[5]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[7] ),
        .O(\shift_green[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[6]_i_1 
       (.I0(\shift_green_reg_n_0_[8] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_green[2]),
        .O(shift_green[6]));
  LUT2 #(
    .INIT(4'h2)) 
    \shift_green[7]_i_1 
       (.I0(latched_green[0]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .O(\shift_green[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[7]_i_2 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[9] ),
        .O(\shift_green[7]_i_2_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[0]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[0] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[1]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[1] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_green[2]),
        .Q(\shift_green_reg_n_0_[2] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[3]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[3] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_green[4]),
        .Q(\shift_green_reg_n_0_[4] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[5]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[5] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_green[6]),
        .Q(\shift_green_reg_n_0_[6] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[7]_i_2_n_0 ),
        .Q(\shift_green_reg_n_0_[7] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_green[8]),
        .Q(\shift_green_reg_n_0_[8] ),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_green[9]),
        .Q(\shift_green_reg_n_0_[9] ),
        .R(\shift_red[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[0]_i_1 
       (.I0(data1[0]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[0]),
        .O(shift_red[0]));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[1]_i_1 
       (.I0(data1[1]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[0]),
        .O(shift_red[1]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[2]_i_1 
       (.I0(data1[2]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[2]),
        .O(shift_red[2]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[3]_i_1 
       (.I0(data1[3]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[3]),
        .O(shift_red[3]));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[4]_i_1 
       (.I0(data1[4]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[2]),
        .O(shift_red[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[5]_i_1 
       (.I0(data1[5]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[0]),
        .O(shift_red[5]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[6]_i_1 
       (.I0(data1[6]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[2]),
        .O(shift_red[6]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[7]_i_1 
       (.I0(data1[7]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[3]),
        .O(shift_red[7]));
  LUT5 #(
    .INIT(32'hEFFFFFFF)) 
    \shift_red[9]_i_1 
       (.I0(\shift_red[9]_i_2_n_0 ),
        .I1(shift_clock__0[5]),
        .I2(shift_clock__0[4]),
        .I3(shift_clock__0[2]),
        .I4(shift_clock__0[3]),
        .O(\shift_red[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFFFFFF)) 
    \shift_red[9]_i_2 
       (.I0(shift_clock__0[8]),
        .I1(shift_clock__0[9]),
        .I2(shift_clock__0[6]),
        .I3(shift_clock__0[7]),
        .I4(shift_clock[1]),
        .I5(shift_clock[0]),
        .O(\shift_red[9]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[0]),
        .Q(D0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[1]),
        .Q(D1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[2]),
        .Q(data1[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[3]),
        .Q(data1[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[4]),
        .Q(data1[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[5]),
        .Q(data1[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[6]),
        .Q(data1[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[7]),
        .Q(data1[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_red[8]),
        .Q(data1[6]),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_red[9]),
        .Q(data1[7]),
        .R(\shift_red[9]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_hcounter
   (Q,
    SR,
    ADDRARDADDR,
    S,
    DI,
    \dc_bias_reg[1] ,
    \dc_bias_reg[1]_0 ,
    \dc_bias_reg[1]_1 ,
    \dc_bias_reg[1]_2 ,
    \dc_bias_reg[1]_3 ,
    \dc_bias_reg[1]_4 ,
    \dc_bias_reg[1]_5 ,
    \dc_bias_reg[1]_6 ,
    \encoded_reg[3] ,
    \encoded_reg[8] ,
    \encoded_reg[8]_0 ,
    \encoded_reg[8]_1 ,
    \encoded_reg[2] ,
    \encoded_reg[2]_0 ,
    \encoded_reg[1] ,
    \encoded_reg[9] ,
    \dc_bias_reg[1]_7 ,
    \processQ_reg[0]_0 ,
    \dc_bias_reg[1]_8 ,
    \dc_bias_reg[1]_9 ,
    \encoded_reg[9]_0 ,
    \encoded_reg[9]_1 ,
    \dc_bias_reg[1]_10 ,
    \dc_bias_reg[1]_11 ,
    \dc_bias_reg[1]_12 ,
    \dc_bias_reg[1]_13 ,
    \dc_bias_reg[1]_14 ,
    \dc_bias_reg[1]_15 ,
    \dc_bias_reg[1]_16 ,
    \dc_bias_reg[1]_17 ,
    \dc_bias_reg[1]_18 ,
    \dc_bias_reg[1]_19 ,
    \dc_bias_reg[1]_20 ,
    \dc_bias_reg[1]_21 ,
    \dc_bias_reg[1]_22 ,
    \dc_bias_reg[1]_23 ,
    \dc_bias_reg[1]_24 ,
    \dc_bias_reg[1]_25 ,
    \encoded_reg[9]_2 ,
    \encoded_reg[8]_2 ,
    \encoded_reg[0] ,
    \encoded_reg[9]_3 ,
    reset_n,
    \trigger_time_s_reg[9] ,
    \trigger_time_s_reg[1] ,
    \trigger_time_s_reg[4] ,
    \trigger_time_s_reg[5] ,
    \trigger_time_s_reg[2] ,
    \trigger_time_s_reg[6] ,
    \trigger_time_s_reg[5]_0 ,
    \dc_bias_reg[3] ,
    \processQ_reg[1]_0 ,
    \processQ_reg[5]_0 ,
    \processQ_reg[5]_1 ,
    switch,
    CO,
    \processQ_reg[8]_0 ,
    \processQ_reg[9]_0 ,
    \processQ_reg[0]_1 ,
    \processQ_reg[4]_0 ,
    \processQ_reg[0]_2 ,
    \processQ_reg[9]_1 ,
    \processQ_reg[9]_2 ,
    \processQ_reg[9]_3 ,
    \processQ_reg[9]_4 ,
    \processQ_reg[9]_5 ,
    \trigger_time_s_reg[3] ,
    \trigger_time_s_reg[4]_0 ,
    \trigger_time_s_reg[5]_1 ,
    \trigger_time_s_reg[4]_1 ,
    \trigger_time_s_reg[2]_0 ,
    \trigger_time_s_reg[4]_2 ,
    \trigger_time_s_reg[5]_2 ,
    \trigger_time_s_reg[2]_1 ,
    \trigger_time_s_reg[4]_3 ,
    \trigger_time_s_reg[5]_3 ,
    \trigger_time_s_reg[3]_0 ,
    \trigger_time_s_reg[4]_4 ,
    \trigger_time_s_reg[5]_4 ,
    \processQ_reg[9]_6 ,
    \processQ_reg[9]_7 ,
    \trigger_time_s_reg[6]_0 ,
    CLK);
  output [1:0]Q;
  output [0:0]SR;
  output [7:0]ADDRARDADDR;
  output [3:0]S;
  output [3:0]DI;
  output [0:0]\dc_bias_reg[1] ;
  output [0:0]\dc_bias_reg[1]_0 ;
  output [3:0]\dc_bias_reg[1]_1 ;
  output [3:0]\dc_bias_reg[1]_2 ;
  output [0:0]\dc_bias_reg[1]_3 ;
  output [0:0]\dc_bias_reg[1]_4 ;
  output \dc_bias_reg[1]_5 ;
  output \dc_bias_reg[1]_6 ;
  output \encoded_reg[3] ;
  output \encoded_reg[8] ;
  output \encoded_reg[8]_0 ;
  output \encoded_reg[8]_1 ;
  output \encoded_reg[2] ;
  output \encoded_reg[2]_0 ;
  output \encoded_reg[1] ;
  output \encoded_reg[9] ;
  output [3:0]\dc_bias_reg[1]_7 ;
  output \processQ_reg[0]_0 ;
  output \dc_bias_reg[1]_8 ;
  output \dc_bias_reg[1]_9 ;
  output \encoded_reg[9]_0 ;
  output \encoded_reg[9]_1 ;
  output [3:0]\dc_bias_reg[1]_10 ;
  output [3:0]\dc_bias_reg[1]_11 ;
  output [3:0]\dc_bias_reg[1]_12 ;
  output [3:0]\dc_bias_reg[1]_13 ;
  output [0:0]\dc_bias_reg[1]_14 ;
  output [0:0]\dc_bias_reg[1]_15 ;
  output [3:0]\dc_bias_reg[1]_16 ;
  output [0:0]\dc_bias_reg[1]_17 ;
  output [0:0]\dc_bias_reg[1]_18 ;
  output [3:0]\dc_bias_reg[1]_19 ;
  output [3:0]\dc_bias_reg[1]_20 ;
  output [0:0]\dc_bias_reg[1]_21 ;
  output [0:0]\dc_bias_reg[1]_22 ;
  output [3:0]\dc_bias_reg[1]_23 ;
  output [0:0]\dc_bias_reg[1]_24 ;
  output [0:0]\dc_bias_reg[1]_25 ;
  output \encoded_reg[9]_2 ;
  output \encoded_reg[8]_2 ;
  output \encoded_reg[0] ;
  output \encoded_reg[9]_3 ;
  input reset_n;
  input [9:0]\trigger_time_s_reg[9] ;
  input \trigger_time_s_reg[1] ;
  input \trigger_time_s_reg[4] ;
  input \trigger_time_s_reg[5] ;
  input \trigger_time_s_reg[2] ;
  input \trigger_time_s_reg[6] ;
  input \trigger_time_s_reg[5]_0 ;
  input [1:0]\dc_bias_reg[3] ;
  input \processQ_reg[1]_0 ;
  input \processQ_reg[5]_0 ;
  input \processQ_reg[5]_1 ;
  input [1:0]switch;
  input [0:0]CO;
  input \processQ_reg[8]_0 ;
  input [0:0]\processQ_reg[9]_0 ;
  input \processQ_reg[0]_1 ;
  input \processQ_reg[4]_0 ;
  input \processQ_reg[0]_2 ;
  input [0:0]\processQ_reg[9]_1 ;
  input [0:0]\processQ_reg[9]_2 ;
  input [0:0]\processQ_reg[9]_3 ;
  input [0:0]\processQ_reg[9]_4 ;
  input [2:0]\processQ_reg[9]_5 ;
  input \trigger_time_s_reg[3] ;
  input \trigger_time_s_reg[4]_0 ;
  input \trigger_time_s_reg[5]_1 ;
  input \trigger_time_s_reg[4]_1 ;
  input \trigger_time_s_reg[2]_0 ;
  input \trigger_time_s_reg[4]_2 ;
  input \trigger_time_s_reg[5]_2 ;
  input \trigger_time_s_reg[2]_1 ;
  input \trigger_time_s_reg[4]_3 ;
  input \trigger_time_s_reg[5]_3 ;
  input \trigger_time_s_reg[3]_0 ;
  input \trigger_time_s_reg[4]_4 ;
  input \trigger_time_s_reg[5]_4 ;
  input [0:0]\processQ_reg[9]_6 ;
  input [0:0]\processQ_reg[9]_7 ;
  input \trigger_time_s_reg[6]_0 ;
  input CLK;

  wire [7:0]ADDRARDADDR;
  wire CLK;
  wire [0:0]CO;
  wire [3:0]DI;
  wire [1:0]Q;
  wire [3:0]S;
  wire [0:0]SR;
  wire [9:2]column;
  wire \dc_bias[3]_i_100_n_0 ;
  wire \dc_bias[3]_i_18_n_0 ;
  wire \dc_bias[3]_i_19_n_0 ;
  wire \dc_bias[3]_i_22__0_n_0 ;
  wire \dc_bias[3]_i_23_n_0 ;
  wire \dc_bias[3]_i_24__0_n_0 ;
  wire \dc_bias[3]_i_24_n_0 ;
  wire \dc_bias[3]_i_25_n_0 ;
  wire \dc_bias[3]_i_26__0_n_0 ;
  wire \dc_bias[3]_i_38_n_0 ;
  wire \dc_bias[3]_i_39_n_0 ;
  wire \dc_bias[3]_i_40_n_0 ;
  wire \dc_bias[3]_i_64_n_0 ;
  wire \dc_bias[3]_i_65_n_0 ;
  wire \dc_bias[3]_i_69_n_0 ;
  wire \dc_bias[3]_i_70_n_0 ;
  wire \dc_bias[3]_i_71_n_0 ;
  wire \dc_bias[3]_i_72_n_0 ;
  wire \dc_bias[3]_i_73_n_0 ;
  wire \dc_bias[3]_i_74_n_0 ;
  wire \dc_bias[3]_i_75_n_0 ;
  wire \dc_bias[3]_i_76_n_0 ;
  wire \dc_bias[3]_i_86_n_0 ;
  wire \dc_bias[3]_i_87_n_0 ;
  wire \dc_bias[3]_i_88_n_0 ;
  wire \dc_bias[3]_i_89_n_0 ;
  wire \dc_bias[3]_i_92_n_0 ;
  wire \dc_bias[3]_i_97_n_0 ;
  wire \dc_bias[3]_i_98_n_0 ;
  wire \dc_bias[3]_i_99_n_0 ;
  wire [0:0]\dc_bias_reg[1] ;
  wire [0:0]\dc_bias_reg[1]_0 ;
  wire [3:0]\dc_bias_reg[1]_1 ;
  wire [3:0]\dc_bias_reg[1]_10 ;
  wire [3:0]\dc_bias_reg[1]_11 ;
  wire [3:0]\dc_bias_reg[1]_12 ;
  wire [3:0]\dc_bias_reg[1]_13 ;
  wire [0:0]\dc_bias_reg[1]_14 ;
  wire [0:0]\dc_bias_reg[1]_15 ;
  wire [3:0]\dc_bias_reg[1]_16 ;
  wire [0:0]\dc_bias_reg[1]_17 ;
  wire [0:0]\dc_bias_reg[1]_18 ;
  wire [3:0]\dc_bias_reg[1]_19 ;
  wire [3:0]\dc_bias_reg[1]_2 ;
  wire [3:0]\dc_bias_reg[1]_20 ;
  wire [0:0]\dc_bias_reg[1]_21 ;
  wire [0:0]\dc_bias_reg[1]_22 ;
  wire [3:0]\dc_bias_reg[1]_23 ;
  wire [0:0]\dc_bias_reg[1]_24 ;
  wire [0:0]\dc_bias_reg[1]_25 ;
  wire [0:0]\dc_bias_reg[1]_3 ;
  wire [0:0]\dc_bias_reg[1]_4 ;
  wire \dc_bias_reg[1]_5 ;
  wire \dc_bias_reg[1]_6 ;
  wire [3:0]\dc_bias_reg[1]_7 ;
  wire \dc_bias_reg[1]_8 ;
  wire \dc_bias_reg[1]_9 ;
  wire [1:0]\dc_bias_reg[3] ;
  wire \dc_bias_reg[3]_i_56_n_0 ;
  wire \dc_bias_reg[3]_i_57_n_0 ;
  wire \encoded_reg[0] ;
  wire \encoded_reg[1] ;
  wire \encoded_reg[2] ;
  wire \encoded_reg[2]_0 ;
  wire \encoded_reg[3] ;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \encoded_reg[8]_2 ;
  wire \encoded_reg[9] ;
  wire \encoded_reg[9]_0 ;
  wire \encoded_reg[9]_1 ;
  wire \encoded_reg[9]_2 ;
  wire \encoded_reg[9]_3 ;
  wire i__carry_i_7__8_n_0;
  wire i__carry_i_8__2_n_0;
  wire [9:1]plusOp__1;
  wire \processQ[0]_i_1__1_n_0 ;
  wire \processQ[2]_i_1__0_n_0 ;
  wire \processQ[6]_i_2_n_0 ;
  wire \processQ[7]_i_1__1_n_0 ;
  wire \processQ[9]_i_2__1_n_0 ;
  wire \processQ[9]_i_4__0_n_0 ;
  wire \processQ[9]_i_5__0_n_0 ;
  wire \processQ[9]_i_6_n_0 ;
  wire \processQ_reg[0]_0 ;
  wire \processQ_reg[0]_1 ;
  wire \processQ_reg[0]_2 ;
  wire \processQ_reg[1]_0 ;
  wire \processQ_reg[4]_0 ;
  wire \processQ_reg[5]_0 ;
  wire \processQ_reg[5]_1 ;
  wire \processQ_reg[8]_0 ;
  wire [0:0]\processQ_reg[9]_0 ;
  wire [0:0]\processQ_reg[9]_1 ;
  wire [0:0]\processQ_reg[9]_2 ;
  wire [0:0]\processQ_reg[9]_3 ;
  wire [0:0]\processQ_reg[9]_4 ;
  wire [2:0]\processQ_reg[9]_5 ;
  wire [0:0]\processQ_reg[9]_6 ;
  wire [0:0]\processQ_reg[9]_7 ;
  wire reset_n;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_i_17_n_0 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_i_18_n_0 ;
  wire [1:0]switch;
  wire \trigger_time_s_reg[1] ;
  wire \trigger_time_s_reg[2] ;
  wire \trigger_time_s_reg[2]_0 ;
  wire \trigger_time_s_reg[2]_1 ;
  wire \trigger_time_s_reg[3] ;
  wire \trigger_time_s_reg[3]_0 ;
  wire \trigger_time_s_reg[4] ;
  wire \trigger_time_s_reg[4]_0 ;
  wire \trigger_time_s_reg[4]_1 ;
  wire \trigger_time_s_reg[4]_2 ;
  wire \trigger_time_s_reg[4]_3 ;
  wire \trigger_time_s_reg[4]_4 ;
  wire \trigger_time_s_reg[5] ;
  wire \trigger_time_s_reg[5]_0 ;
  wire \trigger_time_s_reg[5]_1 ;
  wire \trigger_time_s_reg[5]_2 ;
  wire \trigger_time_s_reg[5]_3 ;
  wire \trigger_time_s_reg[5]_4 ;
  wire \trigger_time_s_reg[6] ;
  wire \trigger_time_s_reg[6]_0 ;
  wire [9:0]\trigger_time_s_reg[9] ;

  LUT3 #(
    .INIT(8'h02)) 
    \dc_bias[1]_i_3 
       (.I0(\dc_bias_reg[3] [0]),
        .I1(\dc_bias_reg[1]_6 ),
        .I2(\processQ_reg[1]_0 ),
        .O(\dc_bias_reg[1]_5 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'h00000018)) 
    \dc_bias[3]_i_100 
       (.I0(column[9]),
        .I1(column[5]),
        .I2(column[6]),
        .I3(column[7]),
        .I4(column[8]),
        .O(\dc_bias[3]_i_100_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dc_bias[3]_i_101 
       (.I0(column[7]),
        .I1(column[9]),
        .O(\encoded_reg[8]_2 ));
  LUT6 #(
    .INIT(64'h4440444444404440)) 
    \dc_bias[3]_i_11 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\dc_bias[3]_i_18_n_0 ),
        .I3(\dc_bias[3]_i_74_n_0 ),
        .I4(\dc_bias[3]_i_71_n_0 ),
        .I5(column[5]),
        .O(\encoded_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hFF00F1000000F100)) 
    \dc_bias[3]_i_12 
       (.I0(\dc_bias[3]_i_69_n_0 ),
        .I1(\dc_bias[3]_i_76_n_0 ),
        .I2(\dc_bias[3]_i_70_n_0 ),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(\dc_bias[3]_i_19_n_0 ),
        .O(\encoded_reg[8] ));
  LUT6 #(
    .INIT(64'h00000000FF04FF34)) 
    \dc_bias[3]_i_13 
       (.I0(\dc_bias[3]_i_38_n_0 ),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\dc_bias[3]_i_39_n_0 ),
        .I4(\dc_bias[3]_i_40_n_0 ),
        .I5(\processQ_reg[5]_0 ),
        .O(\encoded_reg[3] ));
  LUT6 #(
    .INIT(64'h0000000010000001)) 
    \dc_bias[3]_i_15__0 
       (.I0(column[8]),
        .I1(column[7]),
        .I2(column[6]),
        .I3(column[5]),
        .I4(column[9]),
        .I5(\dc_bias[3]_i_23_n_0 ),
        .O(\dc_bias_reg[1]_9 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT5 #(
    .INIT(32'h00001420)) 
    \dc_bias[3]_i_18 
       (.I0(column[8]),
        .I1(column[9]),
        .I2(column[5]),
        .I3(column[4]),
        .I4(\dc_bias[3]_i_76_n_0 ),
        .O(\dc_bias[3]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \dc_bias[3]_i_18__0 
       (.I0(\dc_bias[3]_i_26__0_n_0 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(\encoded_reg[0] ));
  LUT6 #(
    .INIT(64'h22F222F222F2FFFF)) 
    \dc_bias[3]_i_19 
       (.I0(\dc_bias[3]_i_73_n_0 ),
        .I1(\dc_bias[3]_i_69_n_0 ),
        .I2(\dc_bias[3]_i_24__0_n_0 ),
        .I3(\dc_bias[3]_i_25_n_0 ),
        .I4(\dc_bias[3]_i_71_n_0 ),
        .I5(column[5]),
        .O(\dc_bias[3]_i_19_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT5 #(
    .INIT(32'h10000000)) 
    \dc_bias[3]_i_22__0 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\dc_bias[3]_i_26__0_n_0 ),
        .I3(\processQ_reg[9]_6 ),
        .I4(\processQ_reg[9]_7 ),
        .O(\dc_bias[3]_i_22__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'h07E0)) 
    \dc_bias[3]_i_23 
       (.I0(column[2]),
        .I1(column[3]),
        .I2(column[4]),
        .I3(column[5]),
        .O(\dc_bias[3]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'h000000008888F000)) 
    \dc_bias[3]_i_24 
       (.I0(\processQ_reg[9]_1 ),
        .I1(\processQ_reg[9]_2 ),
        .I2(\processQ_reg[9]_3 ),
        .I3(\processQ_reg[9]_4 ),
        .I4(\processQ_reg[9]_5 [1]),
        .I5(\processQ_reg[9]_5 [0]),
        .O(\dc_bias[3]_i_24_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'h0640)) 
    \dc_bias[3]_i_24__0 
       (.I0(column[8]),
        .I1(column[4]),
        .I2(column[5]),
        .I3(column[6]),
        .O(\dc_bias[3]_i_24__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'hFFFF9FFF)) 
    \dc_bias[3]_i_25 
       (.I0(column[9]),
        .I1(column[6]),
        .I2(column[2]),
        .I3(column[3]),
        .I4(column[7]),
        .O(\dc_bias[3]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \dc_bias[3]_i_26__0 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl_i_17_n_0 ),
        .I1(column[4]),
        .I2(column[9]),
        .I3(column[8]),
        .I4(column[3]),
        .I5(column[2]),
        .O(\dc_bias[3]_i_26__0_n_0 ));
  LUT6 #(
    .INIT(64'h10101010101010FF)) 
    \dc_bias[3]_i_30 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\dc_bias[3]_i_26__0_n_0 ),
        .I3(\processQ_reg[4]_0 ),
        .I4(\processQ_reg[9]_5 [1]),
        .I5(\processQ_reg[9]_5 [0]),
        .O(\encoded_reg[9]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \dc_bias[3]_i_31 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(\encoded_reg[9]_3 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFDFFB)) 
    \dc_bias[3]_i_35 
       (.I0(column[5]),
        .I1(column[6]),
        .I2(column[3]),
        .I3(column[4]),
        .I4(\dc_bias[3]_i_64_n_0 ),
        .I5(\dc_bias[3]_i_65_n_0 ),
        .O(\encoded_reg[8]_1 ));
  LUT6 #(
    .INIT(64'h00000000FFBFFFFF)) 
    \dc_bias[3]_i_38 
       (.I0(\dc_bias[3]_i_69_n_0 ),
        .I1(column[2]),
        .I2(column[3]),
        .I3(column[7]),
        .I4(column[6]),
        .I5(\dc_bias[3]_i_70_n_0 ),
        .O(\dc_bias[3]_i_38_n_0 ));
  LUT6 #(
    .INIT(64'h00000000F1FFF1F1)) 
    \dc_bias[3]_i_39 
       (.I0(column[5]),
        .I1(\dc_bias[3]_i_71_n_0 ),
        .I2(\dc_bias[3]_i_72_n_0 ),
        .I3(\dc_bias[3]_i_69_n_0 ),
        .I4(\dc_bias[3]_i_73_n_0 ),
        .I5(\processQ[6]_i_2_n_0 ),
        .O(\dc_bias[3]_i_39_n_0 ));
  LUT5 #(
    .INIT(32'h0D0D000D)) 
    \dc_bias[3]_i_40 
       (.I0(column[5]),
        .I1(\dc_bias[3]_i_71_n_0 ),
        .I2(\dc_bias[3]_i_74_n_0 ),
        .I3(\dc_bias[3]_i_75_n_0 ),
        .I4(\dc_bias[3]_i_76_n_0 ),
        .O(\dc_bias[3]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080808081)) 
    \dc_bias[3]_i_59 
       (.I0(column[5]),
        .I1(column[9]),
        .I2(column[6]),
        .I3(column[7]),
        .I4(column[8]),
        .I5(\dc_bias[3]_i_92_n_0 ),
        .O(\encoded_reg[9]_0 ));
  LUT6 #(
    .INIT(64'h1500400040004000)) 
    \dc_bias[3]_i_6 
       (.I0(\dc_bias_reg[1]_8 ),
        .I1(switch[0]),
        .I2(CO),
        .I3(\processQ_reg[8]_0 ),
        .I4(switch[1]),
        .I5(\processQ_reg[9]_0 ),
        .O(\dc_bias_reg[1]_6 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \dc_bias[3]_i_64 
       (.I0(column[8]),
        .I1(column[9]),
        .I2(column[7]),
        .I3(\processQ_reg[9]_5 [2]),
        .I4(\processQ_reg[9]_5 [0]),
        .O(\dc_bias[3]_i_64_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'h7E6E)) 
    \dc_bias[3]_i_65 
       (.I0(column[2]),
        .I1(column[3]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(\dc_bias[3]_i_65_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'hFBED)) 
    \dc_bias[3]_i_69 
       (.I0(column[5]),
        .I1(column[4]),
        .I2(column[9]),
        .I3(column[8]),
        .O(\dc_bias[3]_i_69_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT4 #(
    .INIT(16'hFFE0)) 
    \dc_bias[3]_i_6__0 
       (.I0(column[7]),
        .I1(column[8]),
        .I2(column[9]),
        .I3(\processQ_reg[9]_5 [2]),
        .O(\encoded_reg[9]_2 ));
  LUT6 #(
    .INIT(64'h00A00CA00AC0CA00)) 
    \dc_bias[3]_i_70 
       (.I0(\dc_bias[3]_i_97_n_0 ),
        .I1(\dc_bias[3]_i_73_n_0 ),
        .I2(column[4]),
        .I3(column[5]),
        .I4(column[9]),
        .I5(column[8]),
        .O(\dc_bias[3]_i_70_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF9F9FFFFFFF)) 
    \dc_bias[3]_i_71 
       (.I0(column[8]),
        .I1(column[4]),
        .I2(column[7]),
        .I3(column[6]),
        .I4(column[2]),
        .I5(column[3]),
        .O(\dc_bias[3]_i_71_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800140000)) 
    \dc_bias[3]_i_72 
       (.I0(column[5]),
        .I1(column[4]),
        .I2(column[8]),
        .I3(\dc_bias[3]_i_98_n_0 ),
        .I4(column[6]),
        .I5(column[9]),
        .O(\dc_bias[3]_i_72_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \dc_bias[3]_i_73 
       (.I0(column[3]),
        .I1(column[2]),
        .I2(column[7]),
        .I3(column[6]),
        .O(\dc_bias[3]_i_73_n_0 ));
  LUT6 #(
    .INIT(64'h20FF200020002000)) 
    \dc_bias[3]_i_74 
       (.I0(column[5]),
        .I1(\dc_bias[3]_i_99_n_0 ),
        .I2(\dc_bias[3]_i_73_n_0 ),
        .I3(column[4]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl_i_18_n_0 ),
        .I5(\dc_bias[3]_i_100_n_0 ),
        .O(\dc_bias[3]_i_74_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'h0428)) 
    \dc_bias[3]_i_75 
       (.I0(column[4]),
        .I1(column[5]),
        .I2(column[9]),
        .I3(column[8]),
        .O(\dc_bias[3]_i_75_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'hF7FF)) 
    \dc_bias[3]_i_76 
       (.I0(column[2]),
        .I1(column[3]),
        .I2(column[7]),
        .I3(column[6]),
        .O(\dc_bias[3]_i_76_n_0 ));
  LUT6 #(
    .INIT(64'hFCFFFFFCFFFFF7F7)) 
    \dc_bias[3]_i_86 
       (.I0(column[6]),
        .I1(column[8]),
        .I2(column[7]),
        .I3(column[9]),
        .I4(column[5]),
        .I5(column[4]),
        .O(\dc_bias[3]_i_86_n_0 ));
  LUT5 #(
    .INIT(32'hFF7FEFFF)) 
    \dc_bias[3]_i_87 
       (.I0(column[5]),
        .I1(column[4]),
        .I2(column[7]),
        .I3(column[6]),
        .I4(column[8]),
        .O(\dc_bias[3]_i_87_n_0 ));
  LUT5 #(
    .INIT(32'h7FFEFFFF)) 
    \dc_bias[3]_i_88 
       (.I0(column[5]),
        .I1(column[4]),
        .I2(column[6]),
        .I3(column[7]),
        .I4(column[8]),
        .O(\dc_bias[3]_i_88_n_0 ));
  LUT6 #(
    .INIT(64'hCFDFDFDFFFFFFFF3)) 
    \dc_bias[3]_i_89 
       (.I0(column[9]),
        .I1(column[7]),
        .I2(column[6]),
        .I3(column[8]),
        .I4(column[4]),
        .I5(column[5]),
        .O(\dc_bias[3]_i_89_n_0 ));
  LUT6 #(
    .INIT(64'h07E007E007E00FE0)) 
    \dc_bias[3]_i_92 
       (.I0(column[3]),
        .I1(column[2]),
        .I2(column[4]),
        .I3(column[5]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(\dc_bias[3]_i_92_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h4200)) 
    \dc_bias[3]_i_97 
       (.I0(column[3]),
        .I1(column[2]),
        .I2(column[6]),
        .I3(column[7]),
        .O(\dc_bias[3]_i_97_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \dc_bias[3]_i_98 
       (.I0(column[7]),
        .I1(column[3]),
        .I2(column[2]),
        .O(\dc_bias[3]_i_98_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \dc_bias[3]_i_99 
       (.I0(column[9]),
        .I1(column[8]),
        .O(\dc_bias[3]_i_99_n_0 ));
  LUT6 #(
    .INIT(64'hAAFBAAFBFFFFAAFB)) 
    \dc_bias[3]_i_9__0 
       (.I0(\dc_bias[3]_i_22__0_n_0 ),
        .I1(\processQ_reg[0]_1 ),
        .I2(\dc_bias[3]_i_24_n_0 ),
        .I3(\processQ_reg[4]_0 ),
        .I4(\dc_bias[3]_i_26__0_n_0 ),
        .I5(\processQ_reg[0]_2 ),
        .O(\dc_bias_reg[1]_8 ));
  MUXF8 \dc_bias_reg[3]_i_32 
       (.I0(\dc_bias_reg[3]_i_56_n_0 ),
        .I1(\dc_bias_reg[3]_i_57_n_0 ),
        .O(\encoded_reg[9] ),
        .S(column[2]));
  MUXF7 \dc_bias_reg[3]_i_56 
       (.I0(\dc_bias[3]_i_86_n_0 ),
        .I1(\dc_bias[3]_i_87_n_0 ),
        .O(\dc_bias_reg[3]_i_56_n_0 ),
        .S(column[3]));
  MUXF7 \dc_bias_reg[3]_i_57 
       (.I0(\dc_bias[3]_i_88_n_0 ),
        .I1(\dc_bias[3]_i_89_n_0 ),
        .O(\dc_bias_reg[3]_i_57_n_0 ),
        .S(column[3]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hD1)) 
    \encoded[1]_i_1 
       (.I0(\encoded_reg[2]_0 ),
        .I1(\processQ_reg[5]_1 ),
        .I2(\dc_bias_reg[3] [1]),
        .O(\encoded_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \encoded[2]_i_1 
       (.I0(\encoded_reg[2]_0 ),
        .I1(\processQ_reg[5]_1 ),
        .I2(\dc_bias_reg[3] [1]),
        .O(\encoded_reg[2] ));
  LUT6 #(
    .INIT(64'h000000007E000000)) 
    \encoded[8]_i_3 
       (.I0(column[6]),
        .I1(column[5]),
        .I2(column[4]),
        .I3(column[7]),
        .I4(column[9]),
        .I5(column[8]),
        .O(\encoded_reg[2]_0 ));
  LUT6 #(
    .INIT(64'h155540003FD55540)) 
    i__carry__0_i_1
       (.I0(column[9]),
        .I1(\trigger_time_s_reg[9] [7]),
        .I2(\trigger_time_s_reg[4] ),
        .I3(\trigger_time_s_reg[9] [8]),
        .I4(\trigger_time_s_reg[9] [9]),
        .I5(column[8]),
        .O(\dc_bias_reg[1] ));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i__carry__0_i_1__0
       (.I0(column[9]),
        .I1(\trigger_time_s_reg[6] ),
        .I2(\trigger_time_s_reg[9] [8]),
        .I3(\trigger_time_s_reg[9] [9]),
        .I4(column[8]),
        .O(\dc_bias_reg[1]_3 ));
  LUT6 #(
    .INIT(64'hC02AAABF80002AAA)) 
    i__carry__0_i_1__5
       (.I0(column[9]),
        .I1(\trigger_time_s_reg[9] [7]),
        .I2(\trigger_time_s_reg[4]_0 ),
        .I3(\trigger_time_s_reg[9] [8]),
        .I4(\trigger_time_s_reg[9] [9]),
        .I5(column[8]),
        .O(\dc_bias_reg[1]_14 ));
  LUT6 #(
    .INIT(64'hC02AAABF80002AAA)) 
    i__carry__0_i_1__6
       (.I0(column[9]),
        .I1(\trigger_time_s_reg[9] [7]),
        .I2(\trigger_time_s_reg[4]_2 ),
        .I3(\trigger_time_s_reg[9] [8]),
        .I4(\trigger_time_s_reg[9] [9]),
        .I5(column[8]),
        .O(\dc_bias_reg[1]_17 ));
  LUT6 #(
    .INIT(64'hC02AAABF80002AAA)) 
    i__carry__0_i_1__7
       (.I0(column[9]),
        .I1(\trigger_time_s_reg[9] [7]),
        .I2(\trigger_time_s_reg[4]_3 ),
        .I3(\trigger_time_s_reg[9] [8]),
        .I4(\trigger_time_s_reg[9] [9]),
        .I5(column[8]),
        .O(\dc_bias_reg[1]_21 ));
  LUT6 #(
    .INIT(64'h155540003FD55540)) 
    i__carry__0_i_1__8
       (.I0(column[9]),
        .I1(\trigger_time_s_reg[9] [7]),
        .I2(\trigger_time_s_reg[4]_4 ),
        .I3(\trigger_time_s_reg[9] [8]),
        .I4(\trigger_time_s_reg[9] [9]),
        .I5(column[8]),
        .O(\dc_bias_reg[1]_24 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2
       (.I0(\trigger_time_s_reg[9] [9]),
        .I1(column[9]),
        .I2(\trigger_time_s_reg[9] [8]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(\trigger_time_s_reg[4] ),
        .I5(column[8]),
        .O(\dc_bias_reg[1]_0 ));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry__0_i_2__0
       (.I0(\trigger_time_s_reg[9] [9]),
        .I1(column[9]),
        .I2(\trigger_time_s_reg[9] [8]),
        .I3(\trigger_time_s_reg[6] ),
        .I4(column[8]),
        .O(\dc_bias_reg[1]_4 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__5
       (.I0(\trigger_time_s_reg[9] [9]),
        .I1(column[9]),
        .I2(\trigger_time_s_reg[9] [8]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(\trigger_time_s_reg[4]_0 ),
        .I5(column[8]),
        .O(\dc_bias_reg[1]_15 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__6
       (.I0(\trigger_time_s_reg[9] [9]),
        .I1(column[9]),
        .I2(\trigger_time_s_reg[9] [8]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(\trigger_time_s_reg[4]_2 ),
        .I5(column[8]),
        .O(\dc_bias_reg[1]_18 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__7
       (.I0(\trigger_time_s_reg[9] [9]),
        .I1(column[9]),
        .I2(\trigger_time_s_reg[9] [8]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(\trigger_time_s_reg[4]_3 ),
        .I5(column[8]),
        .O(\dc_bias_reg[1]_22 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__8
       (.I0(\trigger_time_s_reg[9] [9]),
        .I1(column[9]),
        .I2(\trigger_time_s_reg[9] [8]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(\trigger_time_s_reg[4]_4 ),
        .I5(column[8]),
        .O(\dc_bias_reg[1]_25 ));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i__carry_i_1__0
       (.I0(column[7]),
        .I1(\trigger_time_s_reg[5] ),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(column[6]),
        .O(DI[3]));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i__carry_i_1__1
       (.I0(column[7]),
        .I1(\trigger_time_s_reg[5]_0 ),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(column[6]),
        .O(\dc_bias_reg[1]_2 [3]));
  LUT4 #(
    .INIT(16'h7887)) 
    i__carry_i_1__11
       (.I0(\trigger_time_s_reg[6]_0 ),
        .I1(\trigger_time_s_reg[9] [8]),
        .I2(\trigger_time_s_reg[9] [9]),
        .I3(column[9]),
        .O(\dc_bias_reg[1]_16 [3]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_1__6
       (.I0(column[7]),
        .I1(\trigger_time_s_reg[5]_1 ),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(column[6]),
        .O(\dc_bias_reg[1]_10 [3]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_1__7
       (.I0(column[7]),
        .I1(\trigger_time_s_reg[5]_2 ),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(column[6]),
        .O(\dc_bias_reg[1]_11 [3]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_1__8
       (.I0(column[7]),
        .I1(\trigger_time_s_reg[5]_3 ),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(column[6]),
        .O(\dc_bias_reg[1]_19 [3]));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i__carry_i_1__9
       (.I0(column[7]),
        .I1(\trigger_time_s_reg[5]_4 ),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[9] [7]),
        .I4(column[6]),
        .O(\dc_bias_reg[1]_7 [3]));
  LUT6 #(
    .INIT(64'h1011454434335D55)) 
    i__carry_i_2
       (.I0(column[5]),
        .I1(\trigger_time_s_reg[9] [4]),
        .I2(\trigger_time_s_reg[1] ),
        .I3(\trigger_time_s_reg[9] [3]),
        .I4(\trigger_time_s_reg[9] [5]),
        .I5(column[4]),
        .O(DI[2]));
  LUT6 #(
    .INIT(64'h1011454434335D55)) 
    i__carry_i_2__0
       (.I0(column[5]),
        .I1(\trigger_time_s_reg[9] [4]),
        .I2(\trigger_time_s_reg[2] ),
        .I3(\trigger_time_s_reg[9] [3]),
        .I4(\trigger_time_s_reg[9] [5]),
        .I5(column[4]),
        .O(\dc_bias_reg[1]_2 [2]));
  LUT5 #(
    .INIT(32'h015443D5)) 
    i__carry_i_2__10
       (.I0(column[5]),
        .I1(\trigger_time_s_reg[9] [4]),
        .I2(\trigger_time_s_reg[3]_0 ),
        .I3(\trigger_time_s_reg[9] [5]),
        .I4(column[4]),
        .O(\dc_bias_reg[1]_7 [2]));
  LUT6 #(
    .INIT(64'hBCCC2AAAA8880222)) 
    i__carry_i_2__6
       (.I0(column[5]),
        .I1(\trigger_time_s_reg[9] [4]),
        .I2(\trigger_time_s_reg[9] [1]),
        .I3(\trigger_time_s_reg[3] ),
        .I4(\trigger_time_s_reg[9] [5]),
        .I5(column[4]),
        .O(\dc_bias_reg[1]_10 [2]));
  LUT6 #(
    .INIT(64'h0000000006909009)) 
    i__carry_i_2__7
       (.I0(\trigger_time_s_reg[9] [8]),
        .I1(column[8]),
        .I2(column[7]),
        .I3(\trigger_time_s_reg[4]_1 ),
        .I4(\trigger_time_s_reg[9] [7]),
        .I5(i__carry_i_7__8_n_0),
        .O(\dc_bias_reg[1]_16 [2]));
  LUT5 #(
    .INIT(32'hBC2AA802)) 
    i__carry_i_2__8
       (.I0(column[5]),
        .I1(\trigger_time_s_reg[9] [4]),
        .I2(\trigger_time_s_reg[2]_0 ),
        .I3(\trigger_time_s_reg[9] [5]),
        .I4(column[4]),
        .O(\dc_bias_reg[1]_11 [2]));
  LUT5 #(
    .INIT(32'hBC2AA802)) 
    i__carry_i_2__9
       (.I0(column[5]),
        .I1(\trigger_time_s_reg[9] [4]),
        .I2(\trigger_time_s_reg[2]_1 ),
        .I3(\trigger_time_s_reg[9] [5]),
        .I4(column[4]),
        .O(\dc_bias_reg[1]_19 [2]));
  LUT5 #(
    .INIT(32'h015443D5)) 
    i__carry_i_3
       (.I0(column[3]),
        .I1(\trigger_time_s_reg[9] [1]),
        .I2(\trigger_time_s_reg[9] [2]),
        .I3(\trigger_time_s_reg[9] [3]),
        .I4(column[2]),
        .O(DI[1]));
  LUT6 #(
    .INIT(64'h000155544443DDD5)) 
    i__carry_i_3__0
       (.I0(column[3]),
        .I1(\trigger_time_s_reg[9] [2]),
        .I2(\trigger_time_s_reg[9] [1]),
        .I3(\trigger_time_s_reg[9] [0]),
        .I4(\trigger_time_s_reg[9] [3]),
        .I5(column[2]),
        .O(\dc_bias_reg[1]_2 [1]));
  LUT6 #(
    .INIT(64'h011154444333D555)) 
    i__carry_i_3__10
       (.I0(column[3]),
        .I1(\trigger_time_s_reg[9] [2]),
        .I2(\trigger_time_s_reg[9] [1]),
        .I3(\trigger_time_s_reg[9] [0]),
        .I4(\trigger_time_s_reg[9] [3]),
        .I5(column[2]),
        .O(\dc_bias_reg[1]_7 [1]));
  LUT6 #(
    .INIT(64'hC222ABBB80002AAA)) 
    i__carry_i_3__6
       (.I0(column[3]),
        .I1(\trigger_time_s_reg[9] [2]),
        .I2(\trigger_time_s_reg[9] [1]),
        .I3(\trigger_time_s_reg[9] [0]),
        .I4(\trigger_time_s_reg[9] [3]),
        .I5(column[2]),
        .O(\dc_bias_reg[1]_11 [1]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_3__7
       (.I0(column[3]),
        .I1(\trigger_time_s_reg[9] [1]),
        .I2(\trigger_time_s_reg[9] [2]),
        .I3(\trigger_time_s_reg[9] [3]),
        .I4(column[2]),
        .O(\dc_bias_reg[1]_10 [1]));
  LUT6 #(
    .INIT(64'h00000000556AAA95)) 
    i__carry_i_3__8
       (.I0(\trigger_time_s_reg[9] [5]),
        .I1(\trigger_time_s_reg[9] [3]),
        .I2(\trigger_time_s_reg[9] [2]),
        .I3(\trigger_time_s_reg[9] [4]),
        .I4(column[5]),
        .I5(i__carry_i_8__2_n_0),
        .O(\dc_bias_reg[1]_16 [1]));
  LUT6 #(
    .INIT(64'hE1E0E000FF1F0100)) 
    i__carry_i_3__9
       (.I0(\trigger_time_s_reg[9] [0]),
        .I1(\trigger_time_s_reg[9] [1]),
        .I2(\trigger_time_s_reg[9] [2]),
        .I3(column[2]),
        .I4(column[3]),
        .I5(\trigger_time_s_reg[9] [3]),
        .O(\dc_bias_reg[1]_19 [1]));
  LUT4 #(
    .INIT(16'h121B)) 
    i__carry_i_4
       (.I0(\trigger_time_s_reg[9] [1]),
        .I1(Q[1]),
        .I2(\trigger_time_s_reg[9] [0]),
        .I3(Q[0]),
        .O(\dc_bias_reg[1]_7 [0]));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    i__carry_i_4__10
       (.I0(\trigger_time_s_reg[9] [1]),
        .I1(Q[1]),
        .I2(column[2]),
        .I3(\trigger_time_s_reg[9] [2]),
        .I4(Q[0]),
        .I5(\trigger_time_s_reg[9] [0]),
        .O(\dc_bias_reg[1]_16 [0]));
  LUT4 #(
    .INIT(16'h2127)) 
    i__carry_i_4__5
       (.I0(\trigger_time_s_reg[9] [1]),
        .I1(Q[1]),
        .I2(\trigger_time_s_reg[9] [0]),
        .I3(Q[0]),
        .O(\dc_bias_reg[1]_2 [0]));
  LUT4 #(
    .INIT(16'hF220)) 
    i__carry_i_4__6
       (.I0(Q[0]),
        .I1(\trigger_time_s_reg[9] [0]),
        .I2(Q[1]),
        .I3(\trigger_time_s_reg[9] [1]),
        .O(\dc_bias_reg[1]_10 [0]));
  LUT4 #(
    .INIT(16'hE844)) 
    i__carry_i_4__7
       (.I0(\trigger_time_s_reg[9] [1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\trigger_time_s_reg[9] [0]),
        .O(\dc_bias_reg[1]_11 [0]));
  LUT4 #(
    .INIT(16'h022F)) 
    i__carry_i_4__8
       (.I0(\trigger_time_s_reg[9] [0]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\trigger_time_s_reg[9] [1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'hB0C8)) 
    i__carry_i_4__9
       (.I0(Q[0]),
        .I1(\trigger_time_s_reg[9] [0]),
        .I2(Q[1]),
        .I3(\trigger_time_s_reg[9] [1]),
        .O(\dc_bias_reg[1]_19 [0]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5
       (.I0(\trigger_time_s_reg[9] [7]),
        .I1(column[7]),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[5] ),
        .I4(column[6]),
        .O(S[3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__0
       (.I0(\trigger_time_s_reg[9] [7]),
        .I1(column[7]),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[5]_0 ),
        .I4(column[6]),
        .O(\dc_bias_reg[1]_1 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__5
       (.I0(\trigger_time_s_reg[9] [7]),
        .I1(column[7]),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[5]_1 ),
        .I4(column[6]),
        .O(\dc_bias_reg[1]_13 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__6
       (.I0(\trigger_time_s_reg[9] [7]),
        .I1(column[7]),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[5]_2 ),
        .I4(column[6]),
        .O(\dc_bias_reg[1]_12 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__7
       (.I0(\trigger_time_s_reg[9] [7]),
        .I1(column[7]),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[5]_3 ),
        .I4(column[6]),
        .O(\dc_bias_reg[1]_20 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__8
       (.I0(\trigger_time_s_reg[9] [7]),
        .I1(column[7]),
        .I2(\trigger_time_s_reg[9] [6]),
        .I3(\trigger_time_s_reg[5]_4 ),
        .I4(column[6]),
        .O(\dc_bias_reg[1]_23 [3]));
  LUT6 #(
    .INIT(64'h0960090960066060)) 
    i__carry_i_6
       (.I0(\trigger_time_s_reg[9] [5]),
        .I1(column[5]),
        .I2(\trigger_time_s_reg[9] [4]),
        .I3(\trigger_time_s_reg[1] ),
        .I4(\trigger_time_s_reg[9] [3]),
        .I5(column[4]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h0960090960066060)) 
    i__carry_i_6__0
       (.I0(\trigger_time_s_reg[9] [5]),
        .I1(column[5]),
        .I2(\trigger_time_s_reg[9] [4]),
        .I3(\trigger_time_s_reg[2] ),
        .I4(\trigger_time_s_reg[9] [3]),
        .I5(column[4]),
        .O(\dc_bias_reg[1]_1 [2]));
  LUT6 #(
    .INIT(64'h6009090906606060)) 
    i__carry_i_6__6
       (.I0(\trigger_time_s_reg[9] [5]),
        .I1(column[5]),
        .I2(\trigger_time_s_reg[9] [4]),
        .I3(\trigger_time_s_reg[9] [1]),
        .I4(\trigger_time_s_reg[3] ),
        .I5(column[4]),
        .O(\dc_bias_reg[1]_13 [2]));
  LUT5 #(
    .INIT(32'h60090660)) 
    i__carry_i_6__7
       (.I0(\trigger_time_s_reg[9] [5]),
        .I1(column[5]),
        .I2(\trigger_time_s_reg[9] [4]),
        .I3(\trigger_time_s_reg[2]_0 ),
        .I4(column[4]),
        .O(\dc_bias_reg[1]_12 [2]));
  LUT5 #(
    .INIT(32'h60090660)) 
    i__carry_i_6__8
       (.I0(\trigger_time_s_reg[9] [5]),
        .I1(column[5]),
        .I2(\trigger_time_s_reg[9] [4]),
        .I3(\trigger_time_s_reg[2]_1 ),
        .I4(column[4]),
        .O(\dc_bias_reg[1]_20 [2]));
  LUT5 #(
    .INIT(32'h60090660)) 
    i__carry_i_6__9
       (.I0(\trigger_time_s_reg[9] [5]),
        .I1(column[5]),
        .I2(\trigger_time_s_reg[9] [4]),
        .I3(\trigger_time_s_reg[3]_0 ),
        .I4(column[4]),
        .O(\dc_bias_reg[1]_23 [2]));
  LUT5 #(
    .INIT(32'h21188442)) 
    i__carry_i_7
       (.I0(column[2]),
        .I1(\trigger_time_s_reg[9] [3]),
        .I2(\trigger_time_s_reg[9] [2]),
        .I3(\trigger_time_s_reg[9] [1]),
        .I4(column[3]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h2221111888844442)) 
    i__carry_i_7__0
       (.I0(column[2]),
        .I1(\trigger_time_s_reg[9] [3]),
        .I2(\trigger_time_s_reg[9] [0]),
        .I3(\trigger_time_s_reg[9] [1]),
        .I4(\trigger_time_s_reg[9] [2]),
        .I5(column[3]),
        .O(\dc_bias_reg[1]_1 [1]));
  LUT6 #(
    .INIT(64'h6009090906606060)) 
    i__carry_i_7__10
       (.I0(\trigger_time_s_reg[9] [3]),
        .I1(column[3]),
        .I2(\trigger_time_s_reg[9] [2]),
        .I3(\trigger_time_s_reg[9] [1]),
        .I4(\trigger_time_s_reg[9] [0]),
        .I5(column[2]),
        .O(\dc_bias_reg[1]_23 [1]));
  LUT6 #(
    .INIT(64'h1888844442222111)) 
    i__carry_i_7__6
       (.I0(column[2]),
        .I1(\trigger_time_s_reg[9] [3]),
        .I2(\trigger_time_s_reg[9] [0]),
        .I3(\trigger_time_s_reg[9] [1]),
        .I4(\trigger_time_s_reg[9] [2]),
        .I5(column[3]),
        .O(\dc_bias_reg[1]_12 [1]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_7__7
       (.I0(\trigger_time_s_reg[9] [3]),
        .I1(column[3]),
        .I2(\trigger_time_s_reg[9] [1]),
        .I3(\trigger_time_s_reg[9] [2]),
        .I4(column[2]),
        .O(\dc_bias_reg[1]_13 [1]));
  LUT6 #(
    .INIT(64'h9999955566666AAA)) 
    i__carry_i_7__8
       (.I0(column[6]),
        .I1(\trigger_time_s_reg[9] [5]),
        .I2(\trigger_time_s_reg[9] [3]),
        .I3(\trigger_time_s_reg[9] [2]),
        .I4(\trigger_time_s_reg[9] [4]),
        .I5(\trigger_time_s_reg[9] [6]),
        .O(i__carry_i_7__8_n_0));
  LUT6 #(
    .INIT(64'h0909099060606009)) 
    i__carry_i_7__9
       (.I0(\trigger_time_s_reg[9] [3]),
        .I1(column[3]),
        .I2(\trigger_time_s_reg[9] [2]),
        .I3(\trigger_time_s_reg[9] [1]),
        .I4(\trigger_time_s_reg[9] [0]),
        .I5(column[2]),
        .O(\dc_bias_reg[1]_20 [1]));
  LUT4 #(
    .INIT(16'h0990)) 
    i__carry_i_8__1
       (.I0(\trigger_time_s_reg[9] [0]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\trigger_time_s_reg[9] [1]),
        .O(S[0]));
  LUT5 #(
    .INIT(32'hBDDEE77B)) 
    i__carry_i_8__2
       (.I0(column[3]),
        .I1(\trigger_time_s_reg[9] [4]),
        .I2(\trigger_time_s_reg[9] [2]),
        .I3(\trigger_time_s_reg[9] [3]),
        .I4(column[4]),
        .O(i__carry_i_8__2_n_0));
  LUT4 #(
    .INIT(16'h2442)) 
    i__carry_i_8__3
       (.I0(Q[0]),
        .I1(\trigger_time_s_reg[9] [0]),
        .I2(\trigger_time_s_reg[9] [1]),
        .I3(Q[1]),
        .O(\dc_bias_reg[1]_12 [0]));
  LUT4 #(
    .INIT(16'h4224)) 
    i__carry_i_8__4
       (.I0(Q[0]),
        .I1(\trigger_time_s_reg[9] [0]),
        .I2(\trigger_time_s_reg[9] [1]),
        .I3(Q[1]),
        .O(\dc_bias_reg[1]_1 [0]));
  LUT4 #(
    .INIT(16'h0990)) 
    i__carry_i_8__7
       (.I0(\trigger_time_s_reg[9] [0]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\trigger_time_s_reg[9] [1]),
        .O(\dc_bias_reg[1]_13 [0]));
  LUT4 #(
    .INIT(16'h4224)) 
    i__carry_i_8__8
       (.I0(Q[0]),
        .I1(\trigger_time_s_reg[9] [0]),
        .I2(\trigger_time_s_reg[9] [1]),
        .I3(Q[1]),
        .O(\dc_bias_reg[1]_20 [0]));
  LUT4 #(
    .INIT(16'h2442)) 
    i__carry_i_8__9
       (.I0(Q[0]),
        .I1(\trigger_time_s_reg[9] [0]),
        .I2(\trigger_time_s_reg[9] [1]),
        .I3(Q[1]),
        .O(\dc_bias_reg[1]_23 [0]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \processQ[0]_i_1__1 
       (.I0(Q[0]),
        .O(\processQ[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \processQ[1]_i_1__0 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(plusOp__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \processQ[2]_i_1__0 
       (.I0(column[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(\processQ[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processQ[3]_i_1__0 
       (.I0(column[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(column[2]),
        .O(plusOp__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[4]_i_1__0 
       (.I0(column[4]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(column[2]),
        .I4(column[3]),
        .O(plusOp__1[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processQ[5]_i_1__0 
       (.I0(column[5]),
        .I1(column[4]),
        .I2(column[3]),
        .I3(column[2]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(plusOp__1[5]));
  LUT6 #(
    .INIT(64'hAAAAAAAA6AAAAAAA)) 
    \processQ[6]_i_1__0 
       (.I0(column[6]),
        .I1(column[4]),
        .I2(column[5]),
        .I3(column[3]),
        .I4(column[2]),
        .I5(\processQ[6]_i_2_n_0 ),
        .O(plusOp__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \processQ[6]_i_2 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(\processQ[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[7]_i_1__1 
       (.I0(column[7]),
        .I1(column[6]),
        .I2(column[5]),
        .I3(column[4]),
        .I4(\processQ[9]_i_4__0_n_0 ),
        .O(\processQ[7]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processQ[8]_i_1__0 
       (.I0(column[8]),
        .I1(column[6]),
        .I2(column[7]),
        .I3(\processQ[9]_i_4__0_n_0 ),
        .I4(column[5]),
        .I5(column[4]),
        .O(plusOp__1[8]));
  LUT6 #(
    .INIT(64'h80000000FFFFFFFF)) 
    \processQ[9]_i_1__1 
       (.I0(column[9]),
        .I1(column[8]),
        .I2(column[4]),
        .I3(\processQ[9]_i_4__0_n_0 ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl_i_17_n_0 ),
        .I5(reset_n),
        .O(SR));
  LUT6 #(
    .INIT(64'h777777777777777F)) 
    \processQ[9]_i_2__1 
       (.I0(column[9]),
        .I1(column[8]),
        .I2(column[5]),
        .I3(column[7]),
        .I4(column[6]),
        .I5(\processQ[9]_i_5__0_n_0 ),
        .O(\processQ[9]_i_2__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[9]_i_3__0 
       (.I0(column[9]),
        .I1(column[8]),
        .I2(\processQ[9]_i_6_n_0 ),
        .I3(column[7]),
        .I4(column[6]),
        .O(plusOp__1[9]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \processQ[9]_i_4__0 
       (.I0(column[3]),
        .I1(column[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\processQ[9]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \processQ[9]_i_5 
       (.I0(column[6]),
        .I1(column[7]),
        .I2(column[5]),
        .I3(\processQ[9]_i_5__0_n_0 ),
        .I4(column[8]),
        .I5(column[9]),
        .O(\processQ_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \processQ[9]_i_5__0 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(column[2]),
        .I3(column[3]),
        .I4(column[4]),
        .O(\processQ[9]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \processQ[9]_i_6 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(column[2]),
        .I3(column[3]),
        .I4(column[5]),
        .I5(column[4]),
        .O(\processQ[9]_i_6_n_0 ));
  FDRE \processQ_reg[0] 
       (.C(CLK),
        .CE(\processQ[9]_i_2__1_n_0 ),
        .D(\processQ[0]_i_1__1_n_0 ),
        .Q(Q[0]),
        .R(SR));
  FDRE \processQ_reg[1] 
       (.C(CLK),
        .CE(\processQ[9]_i_2__1_n_0 ),
        .D(plusOp__1[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \processQ_reg[2] 
       (.C(CLK),
        .CE(\processQ[9]_i_2__1_n_0 ),
        .D(\processQ[2]_i_1__0_n_0 ),
        .Q(column[2]),
        .R(SR));
  FDRE \processQ_reg[3] 
       (.C(CLK),
        .CE(\processQ[9]_i_2__1_n_0 ),
        .D(plusOp__1[3]),
        .Q(column[3]),
        .R(SR));
  FDRE \processQ_reg[4] 
       (.C(CLK),
        .CE(\processQ[9]_i_2__1_n_0 ),
        .D(plusOp__1[4]),
        .Q(column[4]),
        .R(SR));
  FDRE \processQ_reg[5] 
       (.C(CLK),
        .CE(\processQ[9]_i_2__1_n_0 ),
        .D(plusOp__1[5]),
        .Q(column[5]),
        .R(SR));
  FDRE \processQ_reg[6] 
       (.C(CLK),
        .CE(\processQ[9]_i_2__1_n_0 ),
        .D(plusOp__1[6]),
        .Q(column[6]),
        .R(SR));
  FDRE \processQ_reg[7] 
       (.C(CLK),
        .CE(\processQ[9]_i_2__1_n_0 ),
        .D(\processQ[7]_i_1__1_n_0 ),
        .Q(column[7]),
        .R(SR));
  FDRE \processQ_reg[8] 
       (.C(CLK),
        .CE(\processQ[9]_i_2__1_n_0 ),
        .D(plusOp__1[8]),
        .Q(column[8]),
        .R(SR));
  FDRE \processQ_reg[9] 
       (.C(CLK),
        .CE(\processQ[9]_i_2__1_n_0 ),
        .D(plusOp__1[9]),
        .Q(column[9]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_17 
       (.I0(column[5]),
        .I1(column[7]),
        .I2(column[6]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl_i_17_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_18 
       (.I0(column[2]),
        .I1(column[3]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAA9A9A9A9A9A)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_2__0 
       (.I0(column[9]),
        .I1(column[8]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl_i_17_n_0 ),
        .I3(column[2]),
        .I4(column[3]),
        .I5(column[4]),
        .O(ADDRARDADDR[7]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAA59)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_3__0 
       (.I0(column[8]),
        .I1(column[4]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl_i_18_n_0 ),
        .I3(column[5]),
        .I4(column[7]),
        .I5(column[6]),
        .O(ADDRARDADDR[6]));
  LUT6 #(
    .INIT(64'hAAAAAAA9A9A9A9A9)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_4__0 
       (.I0(column[7]),
        .I1(column[6]),
        .I2(column[5]),
        .I3(column[2]),
        .I4(column[3]),
        .I5(column[4]),
        .O(ADDRARDADDR[5]));
  LUT5 #(
    .INIT(32'hAAAA9995)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_5__0 
       (.I0(column[6]),
        .I1(column[4]),
        .I2(column[3]),
        .I3(column[2]),
        .I4(column[5]),
        .O(ADDRARDADDR[4]));
  LUT4 #(
    .INIT(16'hA955)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_6__0 
       (.I0(column[5]),
        .I1(column[2]),
        .I2(column[3]),
        .I3(column[4]),
        .O(ADDRARDADDR[3]));
  LUT3 #(
    .INIT(8'h56)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_7__0 
       (.I0(column[4]),
        .I1(column[3]),
        .I2(column[2]),
        .O(ADDRARDADDR[2]));
  LUT2 #(
    .INIT(4'h9)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_8 
       (.I0(column[2]),
        .I1(column[3]),
        .O(ADDRARDADDR[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_9 
       (.I0(column[2]),
        .O(ADDRARDADDR[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl
   (ac_bclk,
    SR,
    ac_lrclk,
    \ac_lrclk_count_reg[0] ,
    E,
    ac_lrclk_sig_prev_reg,
    ready_sig_reg,
    sw,
    \R_bus_in_s_reg[17] ,
    D,
    \L_bus_in_s_reg[17] ,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    ac_dac_sdata,
    clk,
    ac_adc_sdata,
    Q,
    reset_n,
    ac_lrclk_sig_prev_reg_0,
    \R_bus_in_s_reg[17]_0 ,
    \L_bus_in_s_reg[17]_0 ,
    CLK,
    CO,
    switch,
    \trigger_volt_s_reg[9] ,
    \trigger_volt_s_reg[9]_0 );
  output ac_bclk;
  output [0:0]SR;
  output ac_lrclk;
  output [0:0]\ac_lrclk_count_reg[0] ;
  output [0:0]E;
  output ac_lrclk_sig_prev_reg;
  output ready_sig_reg;
  output [0:0]sw;
  output [17:0]\R_bus_in_s_reg[17] ;
  output [6:0]D;
  output [17:0]\L_bus_in_s_reg[17] ;
  output [6:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  output ac_dac_sdata;
  input clk;
  input ac_adc_sdata;
  input [3:0]Q;
  input reset_n;
  input ac_lrclk_sig_prev_reg_0;
  input [17:0]\R_bus_in_s_reg[17]_0 ;
  input [17:0]\L_bus_in_s_reg[17]_0 ;
  input CLK;
  input [0:0]CO;
  input [0:0]switch;
  input [0:0]\trigger_volt_s_reg[9] ;
  input [9:0]\trigger_volt_s_reg[9]_0 ;

  wire BCLK_Fall_int;
  wire BCLK_int_i_2_n_0;
  wire CLK;
  wire [0:0]CO;
  wire Cnt_Bclk0;
  wire \Cnt_Bclk0_inferred__0/i__carry_n_3 ;
  wire \Cnt_Bclk[4]_i_1_n_0 ;
  wire [4:0]Cnt_Bclk_reg__0;
  wire [4:0]Cnt_Lrclk;
  wire \Cnt_Lrclk[0]_i_1_n_0 ;
  wire \Cnt_Lrclk[1]_i_1_n_0 ;
  wire \Cnt_Lrclk[2]_i_1_n_0 ;
  wire \Cnt_Lrclk[3]_i_1_n_0 ;
  wire \Cnt_Lrclk[4]_i_2_n_0 ;
  wire [6:0]D;
  wire D_L_O_int;
  wire \D_R_O_int[23]_i_1_n_0 ;
  wire [31:14]Data_In_int;
  wire \Data_In_int[31]_i_1_n_0 ;
  wire \Data_In_int[31]_i_3_n_0 ;
  wire \Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11_n_0 ;
  wire \Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12_n_0 ;
  wire Data_In_int_reg_gate_n_0;
  wire Data_In_int_reg_r_0_n_0;
  wire Data_In_int_reg_r_10_n_0;
  wire Data_In_int_reg_r_11_n_0;
  wire Data_In_int_reg_r_12_n_0;
  wire Data_In_int_reg_r_1_n_0;
  wire Data_In_int_reg_r_2_n_0;
  wire Data_In_int_reg_r_3_n_0;
  wire Data_In_int_reg_r_4_n_0;
  wire Data_In_int_reg_r_5_n_0;
  wire Data_In_int_reg_r_6_n_0;
  wire Data_In_int_reg_r_7_n_0;
  wire Data_In_int_reg_r_8_n_0;
  wire Data_In_int_reg_r_9_n_0;
  wire Data_In_int_reg_r_n_0;
  wire \Data_Out_int[13]_i_1_n_0 ;
  wire \Data_Out_int[14]_i_1_n_0 ;
  wire \Data_Out_int[15]_i_1_n_0 ;
  wire \Data_Out_int[16]_i_1_n_0 ;
  wire \Data_Out_int[17]_i_1_n_0 ;
  wire \Data_Out_int[18]_i_1_n_0 ;
  wire \Data_Out_int[19]_i_1_n_0 ;
  wire \Data_Out_int[20]_i_1_n_0 ;
  wire \Data_Out_int[21]_i_1_n_0 ;
  wire \Data_Out_int[22]_i_1_n_0 ;
  wire \Data_Out_int[23]_i_1_n_0 ;
  wire \Data_Out_int[24]_i_1_n_0 ;
  wire \Data_Out_int[25]_i_1_n_0 ;
  wire \Data_Out_int[26]_i_1_n_0 ;
  wire \Data_Out_int[27]_i_1_n_0 ;
  wire \Data_Out_int[28]_i_1_n_0 ;
  wire \Data_Out_int[29]_i_1_n_0 ;
  wire \Data_Out_int[30]_i_1_n_0 ;
  wire \Data_Out_int[31]_i_1_n_0 ;
  wire \Data_Out_int[31]_i_2_n_0 ;
  wire \Data_Out_int[31]_i_3_n_0 ;
  wire \Data_Out_int_reg_n_0_[13] ;
  wire \Data_Out_int_reg_n_0_[14] ;
  wire \Data_Out_int_reg_n_0_[15] ;
  wire \Data_Out_int_reg_n_0_[16] ;
  wire \Data_Out_int_reg_n_0_[17] ;
  wire \Data_Out_int_reg_n_0_[18] ;
  wire \Data_Out_int_reg_n_0_[19] ;
  wire \Data_Out_int_reg_n_0_[20] ;
  wire \Data_Out_int_reg_n_0_[21] ;
  wire \Data_Out_int_reg_n_0_[22] ;
  wire \Data_Out_int_reg_n_0_[23] ;
  wire \Data_Out_int_reg_n_0_[24] ;
  wire \Data_Out_int_reg_n_0_[25] ;
  wire \Data_Out_int_reg_n_0_[26] ;
  wire \Data_Out_int_reg_n_0_[27] ;
  wire \Data_Out_int_reg_n_0_[28] ;
  wire \Data_Out_int_reg_n_0_[29] ;
  wire \Data_Out_int_reg_n_0_[30] ;
  wire [0:0]E;
  wire LRCLK_i_1_n_0;
  wire LRCLK_i_2_n_0;
  wire L_bus_g;
  wire [17:0]\L_bus_in_s_reg[17] ;
  wire [17:0]\L_bus_in_s_reg[17]_0 ;
  wire [3:0]Q;
  wire R_bus_g;
  wire [17:0]\R_bus_in_s_reg[17] ;
  wire [17:0]\R_bus_in_s_reg[17]_0 ;
  wire [0:0]SR;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire [0:0]\ac_lrclk_count_reg[0] ;
  wire ac_lrclk_sig_prev_reg;
  wire ac_lrclk_sig_prev_reg_0;
  wire clk;
  wire i___0_i_10_n_0;
  wire i___0_i_11_n_0;
  wire i___0_i_15_n_0;
  wire i___0_i_15_n_1;
  wire i___0_i_15_n_2;
  wire i___0_i_15_n_3;
  wire i___0_i_16_n_0;
  wire i___0_i_17_n_0;
  wire i___0_i_26_n_0;
  wire i___0_i_27_n_0;
  wire i___0_i_28_n_0;
  wire i___0_i_29_n_0;
  wire i___0_i_30_n_0;
  wire i___0_i_31_n_0;
  wire i___0_i_32_n_0;
  wire i___0_i_33_n_0;
  wire i___0_i_34_n_0;
  wire i___0_i_43_n_0;
  wire i___0_i_44_n_0;
  wire i___0_i_45_n_0;
  wire i___0_i_46_n_0;
  wire i___0_i_47_n_0;
  wire i___0_i_48_n_0;
  wire i___0_i_49_n_0;
  wire i___0_i_50_n_0;
  wire i___0_i_51_n_0;
  wire i___0_i_52_n_0;
  wire i___0_i_53_n_0;
  wire i___0_i_9_n_0;
  wire i___0_i_9_n_1;
  wire i___0_i_9_n_2;
  wire i___0_i_9_n_3;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2__11_n_0;
  wire [4:0]p_0_in;
  wire p_17_in;
  wire ready_sig_i_2_n_0;
  wire ready_sig_reg;
  wire reset_n;
  wire [6:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire [0:0]sw;
  wire [0:0]switch;
  wire [0:0]\trigger_volt_s_reg[9] ;
  wire [9:0]\trigger_volt_s_reg[9]_0 ;
  wire [3:2]\NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED ;
  wire [3:0]\NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]NLW_i___0_i_15_O_UNCONNECTED;
  wire [3:1]NLW_i___0_i_3_CO_UNCONNECTED;
  wire [3:0]NLW_i___0_i_3_O_UNCONNECTED;
  wire [3:1]NLW_i___0_i_5_CO_UNCONNECTED;
  wire [3:0]NLW_i___0_i_5_O_UNCONNECTED;
  wire [3:0]NLW_i___0_i_9_O_UNCONNECTED;

  LUT1 #(
    .INIT(2'h1)) 
    BCLK_int_i_1
       (.I0(reset_n),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    BCLK_int_i_2
       (.I0(Cnt_Bclk0),
        .I1(ac_bclk),
        .O(BCLK_int_i_2_n_0));
  FDRE BCLK_int_reg
       (.C(clk),
        .CE(1'b1),
        .D(BCLK_int_i_2_n_0),
        .Q(ac_bclk),
        .R(SR));
  CARRY4 \Cnt_Bclk0_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED [3:2],Cnt_Bclk0,\Cnt_Bclk0_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,i__carry_i_1_n_0,i__carry_i_2__11_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    \Cnt_Bclk[0]_i_1 
       (.I0(Cnt_Bclk_reg__0[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \Cnt_Bclk[1]_i_1 
       (.I0(Cnt_Bclk_reg__0[0]),
        .I1(Cnt_Bclk_reg__0[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \Cnt_Bclk[2]_i_1 
       (.I0(Cnt_Bclk_reg__0[2]),
        .I1(Cnt_Bclk_reg__0[1]),
        .I2(Cnt_Bclk_reg__0[0]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \Cnt_Bclk[3]_i_1 
       (.I0(Cnt_Bclk_reg__0[3]),
        .I1(Cnt_Bclk_reg__0[2]),
        .I2(Cnt_Bclk_reg__0[0]),
        .I3(Cnt_Bclk_reg__0[1]),
        .O(p_0_in[3]));
  LUT2 #(
    .INIT(4'hB)) 
    \Cnt_Bclk[4]_i_1 
       (.I0(Cnt_Bclk0),
        .I1(reset_n),
        .O(\Cnt_Bclk[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \Cnt_Bclk[4]_i_2 
       (.I0(Cnt_Bclk_reg__0[4]),
        .I1(Cnt_Bclk_reg__0[1]),
        .I2(Cnt_Bclk_reg__0[0]),
        .I3(Cnt_Bclk_reg__0[2]),
        .I4(Cnt_Bclk_reg__0[3]),
        .O(p_0_in[4]));
  FDRE \Cnt_Bclk_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(Cnt_Bclk_reg__0[0]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(Cnt_Bclk_reg__0[1]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(Cnt_Bclk_reg__0[2]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(Cnt_Bclk_reg__0[3]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(Cnt_Bclk_reg__0[4]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \Cnt_Lrclk[0]_i_1 
       (.I0(Cnt_Lrclk[0]),
        .O(\Cnt_Lrclk[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \Cnt_Lrclk[1]_i_1 
       (.I0(Cnt_Lrclk[0]),
        .I1(Cnt_Lrclk[1]),
        .O(\Cnt_Lrclk[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \Cnt_Lrclk[2]_i_1 
       (.I0(Cnt_Lrclk[2]),
        .I1(Cnt_Lrclk[1]),
        .I2(Cnt_Lrclk[0]),
        .O(\Cnt_Lrclk[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \Cnt_Lrclk[3]_i_1 
       (.I0(Cnt_Lrclk[3]),
        .I1(Cnt_Lrclk[0]),
        .I2(Cnt_Lrclk[1]),
        .I3(Cnt_Lrclk[2]),
        .O(\Cnt_Lrclk[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Cnt_Lrclk[4]_i_1 
       (.I0(Cnt_Bclk0),
        .I1(ac_bclk),
        .O(BCLK_Fall_int));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \Cnt_Lrclk[4]_i_2 
       (.I0(Cnt_Lrclk[4]),
        .I1(Cnt_Lrclk[2]),
        .I2(Cnt_Lrclk[1]),
        .I3(Cnt_Lrclk[0]),
        .I4(Cnt_Lrclk[3]),
        .O(\Cnt_Lrclk[4]_i_2_n_0 ));
  FDRE \Cnt_Lrclk_reg[0] 
       (.C(clk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[0]_i_1_n_0 ),
        .Q(Cnt_Lrclk[0]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[1] 
       (.C(clk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[1]_i_1_n_0 ),
        .Q(Cnt_Lrclk[1]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[2] 
       (.C(clk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[2]_i_1_n_0 ),
        .Q(Cnt_Lrclk[2]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[3] 
       (.C(clk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[3]_i_1_n_0 ),
        .Q(Cnt_Lrclk[3]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[4] 
       (.C(clk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[4]_i_2_n_0 ),
        .Q(Cnt_Lrclk[4]),
        .R(SR));
  LUT2 #(
    .INIT(4'h2)) 
    \D_L_O_int[23]_i_1 
       (.I0(ac_lrclk),
        .I1(\Data_In_int[31]_i_3_n_0 ),
        .O(D_L_O_int));
  FDRE \D_L_O_int_reg[10] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[18]),
        .Q(\L_bus_in_s_reg[17] [4]),
        .R(SR));
  FDRE \D_L_O_int_reg[11] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[19]),
        .Q(\L_bus_in_s_reg[17] [5]),
        .R(SR));
  FDRE \D_L_O_int_reg[12] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[20]),
        .Q(\L_bus_in_s_reg[17] [6]),
        .R(SR));
  FDRE \D_L_O_int_reg[13] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[21]),
        .Q(\L_bus_in_s_reg[17] [7]),
        .R(SR));
  FDRE \D_L_O_int_reg[14] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[22]),
        .Q(\L_bus_in_s_reg[17] [8]),
        .R(SR));
  FDRE \D_L_O_int_reg[15] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[23]),
        .Q(\L_bus_in_s_reg[17] [9]),
        .R(SR));
  FDRE \D_L_O_int_reg[16] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[24]),
        .Q(\L_bus_in_s_reg[17] [10]),
        .R(SR));
  FDRE \D_L_O_int_reg[17] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[25]),
        .Q(\L_bus_in_s_reg[17] [11]),
        .R(SR));
  FDRE \D_L_O_int_reg[18] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[26]),
        .Q(\L_bus_in_s_reg[17] [12]),
        .R(SR));
  FDRE \D_L_O_int_reg[19] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[27]),
        .Q(\L_bus_in_s_reg[17] [13]),
        .R(SR));
  FDRE \D_L_O_int_reg[20] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[28]),
        .Q(\L_bus_in_s_reg[17] [14]),
        .R(SR));
  FDRE \D_L_O_int_reg[21] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[29]),
        .Q(\L_bus_in_s_reg[17] [15]),
        .R(SR));
  FDRE \D_L_O_int_reg[22] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[30]),
        .Q(\L_bus_in_s_reg[17] [16]),
        .R(SR));
  FDRE \D_L_O_int_reg[23] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[31]),
        .Q(\L_bus_in_s_reg[17] [17]),
        .R(SR));
  FDRE \D_L_O_int_reg[6] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[14]),
        .Q(\L_bus_in_s_reg[17] [0]),
        .R(SR));
  FDRE \D_L_O_int_reg[7] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[15]),
        .Q(\L_bus_in_s_reg[17] [1]),
        .R(SR));
  FDRE \D_L_O_int_reg[8] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[16]),
        .Q(\L_bus_in_s_reg[17] [2]),
        .R(SR));
  FDRE \D_L_O_int_reg[9] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[17]),
        .Q(\L_bus_in_s_reg[17] [3]),
        .R(SR));
  LUT2 #(
    .INIT(4'h1)) 
    \D_R_O_int[23]_i_1 
       (.I0(ac_lrclk),
        .I1(\Data_In_int[31]_i_3_n_0 ),
        .O(\D_R_O_int[23]_i_1_n_0 ));
  FDRE \D_R_O_int_reg[10] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[18]),
        .Q(\R_bus_in_s_reg[17] [4]),
        .R(SR));
  FDRE \D_R_O_int_reg[11] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[19]),
        .Q(\R_bus_in_s_reg[17] [5]),
        .R(SR));
  FDRE \D_R_O_int_reg[12] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[20]),
        .Q(\R_bus_in_s_reg[17] [6]),
        .R(SR));
  FDRE \D_R_O_int_reg[13] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[21]),
        .Q(\R_bus_in_s_reg[17] [7]),
        .R(SR));
  FDRE \D_R_O_int_reg[14] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[22]),
        .Q(\R_bus_in_s_reg[17] [8]),
        .R(SR));
  FDRE \D_R_O_int_reg[15] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[23]),
        .Q(\R_bus_in_s_reg[17] [9]),
        .R(SR));
  FDRE \D_R_O_int_reg[16] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[24]),
        .Q(\R_bus_in_s_reg[17] [10]),
        .R(SR));
  FDRE \D_R_O_int_reg[17] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[25]),
        .Q(\R_bus_in_s_reg[17] [11]),
        .R(SR));
  FDRE \D_R_O_int_reg[18] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[26]),
        .Q(\R_bus_in_s_reg[17] [12]),
        .R(SR));
  FDRE \D_R_O_int_reg[19] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[27]),
        .Q(\R_bus_in_s_reg[17] [13]),
        .R(SR));
  FDRE \D_R_O_int_reg[20] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[28]),
        .Q(\R_bus_in_s_reg[17] [14]),
        .R(SR));
  FDRE \D_R_O_int_reg[21] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[29]),
        .Q(\R_bus_in_s_reg[17] [15]),
        .R(SR));
  FDRE \D_R_O_int_reg[22] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[30]),
        .Q(\R_bus_in_s_reg[17] [16]),
        .R(SR));
  FDRE \D_R_O_int_reg[23] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[31]),
        .Q(\R_bus_in_s_reg[17] [17]),
        .R(SR));
  FDRE \D_R_O_int_reg[6] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[14]),
        .Q(\R_bus_in_s_reg[17] [0]),
        .R(SR));
  FDRE \D_R_O_int_reg[7] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[15]),
        .Q(\R_bus_in_s_reg[17] [1]),
        .R(SR));
  FDRE \D_R_O_int_reg[8] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[16]),
        .Q(\R_bus_in_s_reg[17] [2]),
        .R(SR));
  FDRE \D_R_O_int_reg[9] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[17]),
        .Q(\R_bus_in_s_reg[17] [3]),
        .R(SR));
  LUT2 #(
    .INIT(4'h7)) 
    \Data_In_int[31]_i_1 
       (.I0(reset_n),
        .I1(\Data_In_int[31]_i_3_n_0 ),
        .O(\Data_In_int[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Data_In_int[31]_i_2 
       (.I0(Cnt_Bclk0),
        .I1(ac_bclk),
        .O(p_17_in));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \Data_In_int[31]_i_3 
       (.I0(Cnt_Lrclk[4]),
        .I1(Cnt_Lrclk[3]),
        .I2(Cnt_Lrclk[2]),
        .I3(Cnt_Lrclk[0]),
        .I4(Cnt_Lrclk[1]),
        .I5(BCLK_Fall_int),
        .O(\Data_In_int[31]_i_3_n_0 ));
  (* srl_bus_name = "\U0/my_oscope_ip_v1_0_S00_AXI_inst/datapath/audio_codec/audio_inout/Data_In_int_reg " *) 
  (* srl_name = "\U0/my_oscope_ip_v1_0_S00_AXI_inst/datapath/audio_codec/audio_inout/Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11 " *) 
  SRL16E \Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11 
       (.A0(1'b0),
        .A1(1'b0),
        .A2(1'b1),
        .A3(1'b1),
        .CE(p_17_in),
        .CLK(clk),
        .D(ac_adc_sdata),
        .Q(\Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11_n_0 ));
  FDRE \Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12 
       (.C(clk),
        .CE(p_17_in),
        .D(\Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11_n_0 ),
        .Q(\Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12_n_0 ),
        .R(1'b0));
  FDRE \Data_In_int_reg[14] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_gate_n_0),
        .Q(Data_In_int[14]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[15] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[14]),
        .Q(Data_In_int[15]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[16] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[15]),
        .Q(Data_In_int[16]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[17] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[16]),
        .Q(Data_In_int[17]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[18] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[17]),
        .Q(Data_In_int[18]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[19] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[18]),
        .Q(Data_In_int[19]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[20] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[19]),
        .Q(Data_In_int[20]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[21] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[20]),
        .Q(Data_In_int[21]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[22] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[21]),
        .Q(Data_In_int[22]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[23] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[22]),
        .Q(Data_In_int[23]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[24] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[23]),
        .Q(Data_In_int[24]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[25] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[24]),
        .Q(Data_In_int[25]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[26] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[25]),
        .Q(Data_In_int[26]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[27] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[26]),
        .Q(Data_In_int[27]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[28] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[27]),
        .Q(Data_In_int[28]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[29] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[28]),
        .Q(Data_In_int[29]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[30] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[29]),
        .Q(Data_In_int[30]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[31] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[30]),
        .Q(Data_In_int[31]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    Data_In_int_reg_gate
       (.I0(\Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12_n_0 ),
        .I1(Data_In_int_reg_r_12_n_0),
        .O(Data_In_int_reg_gate_n_0));
  FDRE Data_In_int_reg_r
       (.C(clk),
        .CE(p_17_in),
        .D(1'b1),
        .Q(Data_In_int_reg_r_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_0
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_n_0),
        .Q(Data_In_int_reg_r_0_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_1
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_0_n_0),
        .Q(Data_In_int_reg_r_1_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_10
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_9_n_0),
        .Q(Data_In_int_reg_r_10_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_11
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_10_n_0),
        .Q(Data_In_int_reg_r_11_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_12
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_11_n_0),
        .Q(Data_In_int_reg_r_12_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_2
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_1_n_0),
        .Q(Data_In_int_reg_r_2_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_3
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_2_n_0),
        .Q(Data_In_int_reg_r_3_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_4
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_3_n_0),
        .Q(Data_In_int_reg_r_4_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_5
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_4_n_0),
        .Q(Data_In_int_reg_r_5_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_6
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_5_n_0),
        .Q(Data_In_int_reg_r_6_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_7
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_6_n_0),
        .Q(Data_In_int_reg_r_7_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_8
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_7_n_0),
        .Q(Data_In_int_reg_r_8_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_9
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_8_n_0),
        .Q(Data_In_int_reg_r_9_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h8AFF8000)) 
    \Data_Out_int[13]_i_1 
       (.I0(\Data_Out_int[31]_i_3_n_0 ),
        .I1(\R_bus_in_s_reg[17]_0 [0]),
        .I2(ac_lrclk),
        .I3(reset_n),
        .I4(\L_bus_in_s_reg[17]_0 [0]),
        .O(\Data_Out_int[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[14]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [1]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[13] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [1]),
        .O(\Data_Out_int[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[15]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [2]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[14] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [2]),
        .O(\Data_Out_int[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[16]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [3]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[15] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [3]),
        .O(\Data_Out_int[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[17]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [4]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[16] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [4]),
        .O(\Data_Out_int[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[18]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [5]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[17] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [5]),
        .O(\Data_Out_int[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[19]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [6]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[18] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [6]),
        .O(\Data_Out_int[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[20]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [7]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[19] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [7]),
        .O(\Data_Out_int[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[21]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [8]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[20] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [8]),
        .O(\Data_Out_int[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[22]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [9]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[21] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [9]),
        .O(\Data_Out_int[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[23]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [10]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[22] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [10]),
        .O(\Data_Out_int[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[24]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [11]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[23] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [11]),
        .O(\Data_Out_int[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[25]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [12]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[24] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [12]),
        .O(\Data_Out_int[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[26]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [13]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[25] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [13]),
        .O(\Data_Out_int[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[27]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [14]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[26] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [14]),
        .O(\Data_Out_int[27]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[28]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [15]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[27] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [15]),
        .O(\Data_Out_int[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[29]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [16]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[28] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [16]),
        .O(\Data_Out_int[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[30]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [17]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[29] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [17]),
        .O(\Data_Out_int[30]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF8F)) 
    \Data_Out_int[31]_i_1 
       (.I0(ac_bclk),
        .I1(Cnt_Bclk0),
        .I2(reset_n),
        .I3(\Data_Out_int[31]_i_3_n_0 ),
        .O(\Data_Out_int[31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \Data_Out_int[31]_i_2 
       (.I0(\Data_Out_int_reg_n_0_[30] ),
        .I1(reset_n),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .O(\Data_Out_int[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \Data_Out_int[31]_i_3 
       (.I0(p_17_in),
        .I1(Cnt_Lrclk[4]),
        .I2(Cnt_Lrclk[3]),
        .I3(Cnt_Lrclk[2]),
        .I4(Cnt_Lrclk[0]),
        .I5(Cnt_Lrclk[1]),
        .O(\Data_Out_int[31]_i_3_n_0 ));
  FDRE \Data_Out_int_reg[13] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[13]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[14] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[14]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[15] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[15]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[16] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[16]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[17] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[17]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[18] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[18]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[19] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[19]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[20] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[20]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[20] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[21] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[21]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[22] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[22]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[23] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[23]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[23] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[24] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[24]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[24] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[25] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[25]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[25] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[26] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[26]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[26] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[27] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[27]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[27] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[28] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[28]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[28] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[29] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[29]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[29] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[30] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[30]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[30] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[31] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[31]_i_2_n_0 ),
        .Q(ac_dac_sdata),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    LRCLK_i_1
       (.I0(Cnt_Lrclk[4]),
        .I1(LRCLK_i_2_n_0),
        .I2(Cnt_Bclk0),
        .I3(ac_bclk),
        .I4(ac_lrclk),
        .O(LRCLK_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    LRCLK_i_2
       (.I0(Cnt_Lrclk[3]),
        .I1(Cnt_Lrclk[0]),
        .I2(Cnt_Lrclk[1]),
        .I3(Cnt_Lrclk[2]),
        .O(LRCLK_i_2_n_0));
  FDRE LRCLK_reg
       (.C(clk),
        .CE(1'b1),
        .D(LRCLK_i_1_n_0),
        .Q(ac_lrclk),
        .R(SR));
  LUT6 #(
    .INIT(64'hEAAA0000FFFFFFFF)) 
    \ac_lrclk_count[3]_i_1 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(E),
        .I5(reset_n),
        .O(\ac_lrclk_count_reg[0] ));
  LUT2 #(
    .INIT(4'h2)) 
    \ac_lrclk_count[3]_i_2 
       (.I0(ac_lrclk),
        .I1(ac_lrclk_sig_prev_reg_0),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hD8)) 
    ac_lrclk_sig_prev_i_1
       (.I0(reset_n),
        .I1(ac_lrclk),
        .I2(ac_lrclk_sig_prev_reg_0),
        .O(ac_lrclk_sig_prev_reg));
  LUT5 #(
    .INIT(32'h8F808080)) 
    i___0_i_1
       (.I0(CO),
        .I1(R_bus_g),
        .I2(switch),
        .I3(\trigger_volt_s_reg[9] ),
        .I4(L_bus_g),
        .O(sw));
  LUT4 #(
    .INIT(16'h40C1)) 
    i___0_i_10
       (.I0(\trigger_volt_s_reg[9]_0 [8]),
        .I1(i___0_i_34_n_0),
        .I2(\R_bus_in_s_reg[17] [17]),
        .I3(\trigger_volt_s_reg[9]_0 [9]),
        .O(i___0_i_10_n_0));
  LUT4 #(
    .INIT(16'h8114)) 
    i___0_i_11
       (.I0(\trigger_volt_s_reg[9]_0 [9]),
        .I1(\R_bus_in_s_reg[17] [17]),
        .I2(i___0_i_34_n_0),
        .I3(\trigger_volt_s_reg[9]_0 [8]),
        .O(i___0_i_11_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i___0_i_15
       (.CI(1'b0),
        .CO({i___0_i_15_n_0,i___0_i_15_n_1,i___0_i_15_n_2,i___0_i_15_n_3}),
        .CYINIT(1'b1),
        .DI({i___0_i_43_n_0,i___0_i_44_n_0,i___0_i_45_n_0,i___0_i_46_n_0}),
        .O(NLW_i___0_i_15_O_UNCONNECTED[3:0]),
        .S({i___0_i_47_n_0,i___0_i_48_n_0,i___0_i_49_n_0,i___0_i_50_n_0}));
  LUT4 #(
    .INIT(16'h40C1)) 
    i___0_i_16
       (.I0(\trigger_volt_s_reg[9]_0 [8]),
        .I1(i___0_i_51_n_0),
        .I2(\L_bus_in_s_reg[17] [17]),
        .I3(\trigger_volt_s_reg[9]_0 [9]),
        .O(i___0_i_16_n_0));
  LUT4 #(
    .INIT(16'h8114)) 
    i___0_i_17
       (.I0(\trigger_volt_s_reg[9]_0 [9]),
        .I1(\L_bus_in_s_reg[17] [17]),
        .I2(i___0_i_51_n_0),
        .I3(\trigger_volt_s_reg[9]_0 [8]),
        .O(i___0_i_17_n_0));
  LUT5 #(
    .INIT(32'h5401D543)) 
    i___0_i_26
       (.I0(\trigger_volt_s_reg[9]_0 [7]),
        .I1(\R_bus_in_s_reg[17] [15]),
        .I2(i___0_i_52_n_0),
        .I3(\R_bus_in_s_reg[17] [16]),
        .I4(\trigger_volt_s_reg[9]_0 [6]),
        .O(i___0_i_26_n_0));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i___0_i_27
       (.I0(\trigger_volt_s_reg[9]_0 [5]),
        .I1(\R_bus_in_s_reg[17] [12]),
        .I2(\R_bus_in_s_reg[17] [13]),
        .I3(\R_bus_in_s_reg[17] [14]),
        .I4(\trigger_volt_s_reg[9]_0 [4]),
        .O(i___0_i_27_n_0));
  LUT4 #(
    .INIT(16'h1171)) 
    i___0_i_28
       (.I0(\trigger_volt_s_reg[9]_0 [3]),
        .I1(\R_bus_in_s_reg[17] [12]),
        .I2(\R_bus_in_s_reg[17] [11]),
        .I3(\trigger_volt_s_reg[9]_0 [2]),
        .O(i___0_i_28_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i___0_i_29
       (.I0(\trigger_volt_s_reg[9]_0 [1]),
        .I1(\R_bus_in_s_reg[17] [10]),
        .I2(\R_bus_in_s_reg[17] [9]),
        .I3(\trigger_volt_s_reg[9]_0 [0]),
        .O(i___0_i_29_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i___0_i_3
       (.CI(i___0_i_9_n_0),
        .CO({NLW_i___0_i_3_CO_UNCONNECTED[3:1],R_bus_g}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,i___0_i_10_n_0}),
        .O(NLW_i___0_i_3_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,i___0_i_11_n_0}));
  LUT5 #(
    .INIT(32'h90060990)) 
    i___0_i_30
       (.I0(\R_bus_in_s_reg[17] [16]),
        .I1(\trigger_volt_s_reg[9]_0 [7]),
        .I2(\R_bus_in_s_reg[17] [15]),
        .I3(i___0_i_52_n_0),
        .I4(\trigger_volt_s_reg[9]_0 [6]),
        .O(i___0_i_30_n_0));
  LUT5 #(
    .INIT(32'h09906009)) 
    i___0_i_31
       (.I0(\R_bus_in_s_reg[17] [14]),
        .I1(\trigger_volt_s_reg[9]_0 [5]),
        .I2(\R_bus_in_s_reg[17] [12]),
        .I3(\R_bus_in_s_reg[17] [13]),
        .I4(\trigger_volt_s_reg[9]_0 [4]),
        .O(i___0_i_31_n_0));
  LUT4 #(
    .INIT(16'h6006)) 
    i___0_i_32
       (.I0(\R_bus_in_s_reg[17] [12]),
        .I1(\trigger_volt_s_reg[9]_0 [3]),
        .I2(\R_bus_in_s_reg[17] [11]),
        .I3(\trigger_volt_s_reg[9]_0 [2]),
        .O(i___0_i_32_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_33
       (.I0(\R_bus_in_s_reg[17] [10]),
        .I1(\trigger_volt_s_reg[9]_0 [1]),
        .I2(\R_bus_in_s_reg[17] [9]),
        .I3(\trigger_volt_s_reg[9]_0 [0]),
        .O(i___0_i_33_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h0000007F)) 
    i___0_i_34
       (.I0(\R_bus_in_s_reg[17] [14]),
        .I1(\R_bus_in_s_reg[17] [13]),
        .I2(\R_bus_in_s_reg[17] [12]),
        .I3(\R_bus_in_s_reg[17] [15]),
        .I4(\R_bus_in_s_reg[17] [16]),
        .O(i___0_i_34_n_0));
  LUT5 #(
    .INIT(32'h5401D543)) 
    i___0_i_43
       (.I0(\trigger_volt_s_reg[9]_0 [7]),
        .I1(\L_bus_in_s_reg[17] [15]),
        .I2(i___0_i_53_n_0),
        .I3(\L_bus_in_s_reg[17] [16]),
        .I4(\trigger_volt_s_reg[9]_0 [6]),
        .O(i___0_i_43_n_0));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i___0_i_44
       (.I0(\trigger_volt_s_reg[9]_0 [5]),
        .I1(\L_bus_in_s_reg[17] [12]),
        .I2(\L_bus_in_s_reg[17] [13]),
        .I3(\L_bus_in_s_reg[17] [14]),
        .I4(\trigger_volt_s_reg[9]_0 [4]),
        .O(i___0_i_44_n_0));
  LUT4 #(
    .INIT(16'h1171)) 
    i___0_i_45
       (.I0(\trigger_volt_s_reg[9]_0 [3]),
        .I1(\L_bus_in_s_reg[17] [12]),
        .I2(\L_bus_in_s_reg[17] [11]),
        .I3(\trigger_volt_s_reg[9]_0 [2]),
        .O(i___0_i_45_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i___0_i_46
       (.I0(\trigger_volt_s_reg[9]_0 [1]),
        .I1(\L_bus_in_s_reg[17] [10]),
        .I2(\L_bus_in_s_reg[17] [9]),
        .I3(\trigger_volt_s_reg[9]_0 [0]),
        .O(i___0_i_46_n_0));
  LUT5 #(
    .INIT(32'h90060990)) 
    i___0_i_47
       (.I0(\L_bus_in_s_reg[17] [16]),
        .I1(\trigger_volt_s_reg[9]_0 [7]),
        .I2(\L_bus_in_s_reg[17] [15]),
        .I3(i___0_i_53_n_0),
        .I4(\trigger_volt_s_reg[9]_0 [6]),
        .O(i___0_i_47_n_0));
  LUT5 #(
    .INIT(32'h09906009)) 
    i___0_i_48
       (.I0(\L_bus_in_s_reg[17] [14]),
        .I1(\trigger_volt_s_reg[9]_0 [5]),
        .I2(\L_bus_in_s_reg[17] [12]),
        .I3(\L_bus_in_s_reg[17] [13]),
        .I4(\trigger_volt_s_reg[9]_0 [4]),
        .O(i___0_i_48_n_0));
  LUT4 #(
    .INIT(16'h6006)) 
    i___0_i_49
       (.I0(\L_bus_in_s_reg[17] [12]),
        .I1(\trigger_volt_s_reg[9]_0 [3]),
        .I2(\L_bus_in_s_reg[17] [11]),
        .I3(\trigger_volt_s_reg[9]_0 [2]),
        .O(i___0_i_49_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i___0_i_5
       (.CI(i___0_i_15_n_0),
        .CO({NLW_i___0_i_5_CO_UNCONNECTED[3:1],L_bus_g}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,i___0_i_16_n_0}),
        .O(NLW_i___0_i_5_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,i___0_i_17_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_50
       (.I0(\L_bus_in_s_reg[17] [10]),
        .I1(\trigger_volt_s_reg[9]_0 [1]),
        .I2(\L_bus_in_s_reg[17] [9]),
        .I3(\trigger_volt_s_reg[9]_0 [0]),
        .O(i___0_i_50_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h0000007F)) 
    i___0_i_51
       (.I0(\L_bus_in_s_reg[17] [14]),
        .I1(\L_bus_in_s_reg[17] [13]),
        .I2(\L_bus_in_s_reg[17] [12]),
        .I3(\L_bus_in_s_reg[17] [15]),
        .I4(\L_bus_in_s_reg[17] [16]),
        .O(i___0_i_51_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h80)) 
    i___0_i_52
       (.I0(\R_bus_in_s_reg[17] [14]),
        .I1(\R_bus_in_s_reg[17] [13]),
        .I2(\R_bus_in_s_reg[17] [12]),
        .O(i___0_i_52_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h80)) 
    i___0_i_53
       (.I0(\L_bus_in_s_reg[17] [14]),
        .I1(\L_bus_in_s_reg[17] [13]),
        .I2(\L_bus_in_s_reg[17] [12]),
        .O(i___0_i_53_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i___0_i_9
       (.CI(1'b0),
        .CO({i___0_i_9_n_0,i___0_i_9_n_1,i___0_i_9_n_2,i___0_i_9_n_3}),
        .CYINIT(1'b1),
        .DI({i___0_i_26_n_0,i___0_i_27_n_0,i___0_i_28_n_0,i___0_i_29_n_0}),
        .O(NLW_i___0_i_9_O_UNCONNECTED[3:0]),
        .S({i___0_i_30_n_0,i___0_i_31_n_0,i___0_i_32_n_0,i___0_i_33_n_0}));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_1
       (.I0(Cnt_Bclk_reg__0[3]),
        .I1(Cnt_Bclk_reg__0[4]),
        .O(i__carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    i__carry_i_2__11
       (.I0(Cnt_Bclk_reg__0[2]),
        .I1(Cnt_Bclk_reg__0[0]),
        .I2(Cnt_Bclk_reg__0[1]),
        .O(i__carry_i_2__11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'hFFF040F0)) 
    ready_sig_i_1
       (.I0(ac_lrclk_sig_prev_reg_0),
        .I1(ac_lrclk),
        .I2(CLK),
        .I3(reset_n),
        .I4(ready_sig_i_2_n_0),
        .O(ready_sig_reg));
  LUT6 #(
    .INIT(64'h4444444440000000)) 
    ready_sig_i_2
       (.I0(ac_lrclk_sig_prev_reg_0),
        .I1(ac_lrclk),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(Q[3]),
        .O(ready_sig_i_2_n_0));
  LUT6 #(
    .INIT(64'h0002020202020202)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_1 
       (.I0(\R_bus_in_s_reg[17] [17]),
        .I1(\R_bus_in_s_reg[17] [16]),
        .I2(\R_bus_in_s_reg[17] [15]),
        .I3(\R_bus_in_s_reg[17] [12]),
        .I4(\R_bus_in_s_reg[17] [13]),
        .I5(\R_bus_in_s_reg[17] [14]),
        .O(D[6]));
  LUT6 #(
    .INIT(64'h0002020202020202)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_10 
       (.I0(\L_bus_in_s_reg[17] [17]),
        .I1(\L_bus_in_s_reg[17] [16]),
        .I2(\L_bus_in_s_reg[17] [15]),
        .I3(\L_bus_in_s_reg[17] [12]),
        .I4(\L_bus_in_s_reg[17] [13]),
        .I5(\L_bus_in_s_reg[17] [14]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [6]));
  LUT6 #(
    .INIT(64'h0000007FFFFFFF80)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_11 
       (.I0(\L_bus_in_s_reg[17] [14]),
        .I1(\L_bus_in_s_reg[17] [13]),
        .I2(\L_bus_in_s_reg[17] [12]),
        .I3(\L_bus_in_s_reg[17] [15]),
        .I4(\L_bus_in_s_reg[17] [16]),
        .I5(\L_bus_in_s_reg[17] [17]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [5]));
  LUT5 #(
    .INIT(32'hAAAA9555)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_12 
       (.I0(\L_bus_in_s_reg[17] [16]),
        .I1(\L_bus_in_s_reg[17] [14]),
        .I2(\L_bus_in_s_reg[17] [13]),
        .I3(\L_bus_in_s_reg[17] [12]),
        .I4(\L_bus_in_s_reg[17] [15]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [4]));
  LUT4 #(
    .INIT(16'h807F)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_13 
       (.I0(\L_bus_in_s_reg[17] [14]),
        .I1(\L_bus_in_s_reg[17] [13]),
        .I2(\L_bus_in_s_reg[17] [12]),
        .I3(\L_bus_in_s_reg[17] [15]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [3]));
  LUT3 #(
    .INIT(8'h6A)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_14 
       (.I0(\L_bus_in_s_reg[17] [14]),
        .I1(\L_bus_in_s_reg[17] [13]),
        .I2(\L_bus_in_s_reg[17] [12]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [2]));
  LUT2 #(
    .INIT(4'h6)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_15 
       (.I0(\L_bus_in_s_reg[17] [12]),
        .I1(\L_bus_in_s_reg[17] [13]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [1]));
  LUT1 #(
    .INIT(2'h1)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_16 
       (.I0(\L_bus_in_s_reg[17] [12]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [0]));
  LUT6 #(
    .INIT(64'h0000007FFFFFFF80)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_2 
       (.I0(\R_bus_in_s_reg[17] [14]),
        .I1(\R_bus_in_s_reg[17] [13]),
        .I2(\R_bus_in_s_reg[17] [12]),
        .I3(\R_bus_in_s_reg[17] [15]),
        .I4(\R_bus_in_s_reg[17] [16]),
        .I5(\R_bus_in_s_reg[17] [17]),
        .O(D[5]));
  LUT5 #(
    .INIT(32'hAAAA9555)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_3 
       (.I0(\R_bus_in_s_reg[17] [16]),
        .I1(\R_bus_in_s_reg[17] [14]),
        .I2(\R_bus_in_s_reg[17] [13]),
        .I3(\R_bus_in_s_reg[17] [12]),
        .I4(\R_bus_in_s_reg[17] [15]),
        .O(D[4]));
  LUT4 #(
    .INIT(16'h807F)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_4 
       (.I0(\R_bus_in_s_reg[17] [14]),
        .I1(\R_bus_in_s_reg[17] [13]),
        .I2(\R_bus_in_s_reg[17] [12]),
        .I3(\R_bus_in_s_reg[17] [15]),
        .O(D[3]));
  LUT3 #(
    .INIT(8'h6A)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_5 
       (.I0(\R_bus_in_s_reg[17] [14]),
        .I1(\R_bus_in_s_reg[17] [13]),
        .I2(\R_bus_in_s_reg[17] [12]),
        .O(D[2]));
  LUT2 #(
    .INIT(4'h6)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_6 
       (.I0(\R_bus_in_s_reg[17] [12]),
        .I1(\R_bus_in_s_reg[17] [13]),
        .O(D[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_7 
       (.I0(\R_bus_in_s_reg[17] [12]),
        .O(D[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath
   (tmds,
    tmdsb,
    ac_mclk,
    SR,
    ac_bclk,
    ac_lrclk,
    sw,
    ac_dac_sdata,
    scl,
    sda,
    clk,
    ac_adc_sdata,
    reset_n,
    cw,
    btn,
    switch,
    \state_reg[1] ,
    E);
  output [3:0]tmds;
  output [3:0]tmdsb;
  output ac_mclk;
  output [0:0]SR;
  output ac_bclk;
  output ac_lrclk;
  output [2:0]sw;
  output ac_dac_sdata;
  inout scl;
  inout sda;
  input clk;
  input ac_adc_sdata;
  input reset_n;
  input [0:0]cw;
  input [4:0]btn;
  input [3:0]switch;
  input [0:0]\state_reg[1] ;
  input [0:0]E;

  wire [0:0]E;
  wire [17:0]L_bus_in;
  wire L_bus_l;
  wire [1:0]L_bus_out_s;
  wire [9:0]L_unsigned_data_prev;
  wire [17:0]R_bus_in;
  wire R_bus_l;
  wire [1:0]R_bus_out_s;
  wire [9:0]R_unsigned_data_prev;
  wire [0:0]SR;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire audio_codec_n_10;
  wire audio_codec_n_11;
  wire audio_codec_n_12;
  wire audio_codec_n_13;
  wire audio_codec_n_14;
  wire audio_codec_n_15;
  wire audio_codec_n_16;
  wire audio_codec_n_17;
  wire audio_codec_n_18;
  wire audio_codec_n_19;
  wire audio_codec_n_20;
  wire audio_codec_n_21;
  wire audio_codec_n_24;
  wire audio_codec_n_25;
  wire audio_codec_n_26;
  wire audio_codec_n_27;
  wire audio_codec_n_28;
  wire audio_codec_n_29;
  wire audio_codec_n_30;
  wire audio_codec_n_31;
  wire audio_codec_n_32;
  wire audio_codec_n_33;
  wire audio_codec_n_34;
  wire audio_codec_n_35;
  wire audio_codec_n_36;
  wire audio_codec_n_37;
  wire audio_codec_n_38;
  wire audio_codec_n_39;
  wire audio_codec_n_40;
  wire audio_codec_n_41;
  wire audio_codec_n_42;
  wire audio_codec_n_43;
  wire audio_codec_n_44;
  wire audio_codec_n_45;
  wire audio_codec_n_46;
  wire audio_codec_n_49;
  wire audio_codec_n_50;
  wire audio_codec_n_51;
  wire audio_codec_n_52;
  wire audio_codec_n_53;
  wire audio_codec_n_54;
  wire audio_codec_n_55;
  wire audio_codec_n_6;
  wire audio_codec_n_7;
  wire audio_codec_n_8;
  wire audio_codec_n_9;
  wire [4:0]btn;
  wire ch1;
  wire ch2;
  wire clk;
  wire \clock_divider[0]_i_2_n_0 ;
  wire [22:21]clock_divider_reg;
  wire \clock_divider_reg[0]_i_1_n_0 ;
  wire \clock_divider_reg[0]_i_1_n_1 ;
  wire \clock_divider_reg[0]_i_1_n_2 ;
  wire \clock_divider_reg[0]_i_1_n_3 ;
  wire \clock_divider_reg[0]_i_1_n_4 ;
  wire \clock_divider_reg[0]_i_1_n_5 ;
  wire \clock_divider_reg[0]_i_1_n_6 ;
  wire \clock_divider_reg[0]_i_1_n_7 ;
  wire \clock_divider_reg[12]_i_1_n_0 ;
  wire \clock_divider_reg[12]_i_1_n_1 ;
  wire \clock_divider_reg[12]_i_1_n_2 ;
  wire \clock_divider_reg[12]_i_1_n_3 ;
  wire \clock_divider_reg[12]_i_1_n_4 ;
  wire \clock_divider_reg[12]_i_1_n_5 ;
  wire \clock_divider_reg[12]_i_1_n_6 ;
  wire \clock_divider_reg[12]_i_1_n_7 ;
  wire \clock_divider_reg[16]_i_1_n_0 ;
  wire \clock_divider_reg[16]_i_1_n_1 ;
  wire \clock_divider_reg[16]_i_1_n_2 ;
  wire \clock_divider_reg[16]_i_1_n_3 ;
  wire \clock_divider_reg[16]_i_1_n_4 ;
  wire \clock_divider_reg[16]_i_1_n_5 ;
  wire \clock_divider_reg[16]_i_1_n_6 ;
  wire \clock_divider_reg[16]_i_1_n_7 ;
  wire \clock_divider_reg[20]_i_1_n_2 ;
  wire \clock_divider_reg[20]_i_1_n_3 ;
  wire \clock_divider_reg[20]_i_1_n_5 ;
  wire \clock_divider_reg[20]_i_1_n_6 ;
  wire \clock_divider_reg[20]_i_1_n_7 ;
  wire \clock_divider_reg[4]_i_1_n_0 ;
  wire \clock_divider_reg[4]_i_1_n_1 ;
  wire \clock_divider_reg[4]_i_1_n_2 ;
  wire \clock_divider_reg[4]_i_1_n_3 ;
  wire \clock_divider_reg[4]_i_1_n_4 ;
  wire \clock_divider_reg[4]_i_1_n_5 ;
  wire \clock_divider_reg[4]_i_1_n_6 ;
  wire \clock_divider_reg[4]_i_1_n_7 ;
  wire \clock_divider_reg[8]_i_1_n_0 ;
  wire \clock_divider_reg[8]_i_1_n_1 ;
  wire \clock_divider_reg[8]_i_1_n_2 ;
  wire \clock_divider_reg[8]_i_1_n_3 ;
  wire \clock_divider_reg[8]_i_1_n_4 ;
  wire \clock_divider_reg[8]_i_1_n_5 ;
  wire \clock_divider_reg[8]_i_1_n_6 ;
  wire \clock_divider_reg[8]_i_1_n_7 ;
  wire \clock_divider_reg_n_0_[0] ;
  wire \clock_divider_reg_n_0_[10] ;
  wire \clock_divider_reg_n_0_[11] ;
  wire \clock_divider_reg_n_0_[12] ;
  wire \clock_divider_reg_n_0_[13] ;
  wire \clock_divider_reg_n_0_[14] ;
  wire \clock_divider_reg_n_0_[15] ;
  wire \clock_divider_reg_n_0_[16] ;
  wire \clock_divider_reg_n_0_[17] ;
  wire \clock_divider_reg_n_0_[18] ;
  wire \clock_divider_reg_n_0_[19] ;
  wire \clock_divider_reg_n_0_[1] ;
  wire \clock_divider_reg_n_0_[20] ;
  wire \clock_divider_reg_n_0_[2] ;
  wire \clock_divider_reg_n_0_[3] ;
  wire \clock_divider_reg_n_0_[4] ;
  wire \clock_divider_reg_n_0_[5] ;
  wire \clock_divider_reg_n_0_[6] ;
  wire \clock_divider_reg_n_0_[7] ;
  wire \clock_divider_reg_n_0_[8] ;
  wire \clock_divider_reg_n_0_[9] ;
  wire [1:0]column__0;
  wire [0:0]cw;
  wire i___0_i_12_n_0;
  wire i___0_i_12_n_1;
  wire i___0_i_12_n_2;
  wire i___0_i_12_n_3;
  wire i___0_i_13_n_0;
  wire i___0_i_14_n_0;
  wire i___0_i_18_n_0;
  wire i___0_i_19_n_0;
  wire i___0_i_20_n_0;
  wire i___0_i_21_n_0;
  wire i___0_i_22_n_0;
  wire i___0_i_23_n_0;
  wire i___0_i_24_n_0;
  wire i___0_i_25_n_0;
  wire i___0_i_35_n_0;
  wire i___0_i_36_n_0;
  wire i___0_i_37_n_0;
  wire i___0_i_38_n_0;
  wire i___0_i_39_n_0;
  wire i___0_i_40_n_0;
  wire i___0_i_41_n_0;
  wire i___0_i_42_n_0;
  wire i___0_i_6_n_0;
  wire i___0_i_6_n_1;
  wire i___0_i_6_n_2;
  wire i___0_i_6_n_3;
  wire i___0_i_7_n_0;
  wire i___0_i_8_n_0;
  wire lBRAM_n_0;
  wire lopt;
  wire rBRAM_n_0;
  wire reset_n;
  wire [9:0]row;
  wire scl;
  wire sda;
  wire [0:0]\state_reg[1] ;
  wire [2:0]sw;
  wire [3:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;
  wire trigger_clock;
  wire \trigger_time_s[0]_i_1_n_0 ;
  wire \trigger_time_s[4]_i_2_n_0 ;
  wire \trigger_time_s[4]_i_3_n_0 ;
  wire \trigger_time_s[4]_i_4_n_0 ;
  wire \trigger_time_s[4]_i_5_n_0 ;
  wire \trigger_time_s[4]_i_6_n_0 ;
  wire \trigger_time_s[8]_i_2_n_0 ;
  wire \trigger_time_s[8]_i_3_n_0 ;
  wire \trigger_time_s[8]_i_4_n_0 ;
  wire \trigger_time_s[8]_i_5_n_0 ;
  wire \trigger_time_s[9]_i_1_n_0 ;
  wire \trigger_time_s[9]_i_2_n_0 ;
  wire \trigger_time_s[9]_i_5_n_0 ;
  wire \trigger_time_s[9]_i_6_n_0 ;
  wire \trigger_time_s[9]_i_7_n_0 ;
  wire \trigger_time_s[9]_i_8_n_0 ;
  wire \trigger_time_s_reg[4]_i_1_n_0 ;
  wire \trigger_time_s_reg[4]_i_1_n_1 ;
  wire \trigger_time_s_reg[4]_i_1_n_2 ;
  wire \trigger_time_s_reg[4]_i_1_n_3 ;
  wire \trigger_time_s_reg[4]_i_1_n_4 ;
  wire \trigger_time_s_reg[4]_i_1_n_5 ;
  wire \trigger_time_s_reg[4]_i_1_n_6 ;
  wire \trigger_time_s_reg[4]_i_1_n_7 ;
  wire \trigger_time_s_reg[8]_i_1_n_0 ;
  wire \trigger_time_s_reg[8]_i_1_n_1 ;
  wire \trigger_time_s_reg[8]_i_1_n_2 ;
  wire \trigger_time_s_reg[8]_i_1_n_3 ;
  wire \trigger_time_s_reg[8]_i_1_n_4 ;
  wire \trigger_time_s_reg[8]_i_1_n_5 ;
  wire \trigger_time_s_reg[8]_i_1_n_6 ;
  wire \trigger_time_s_reg[8]_i_1_n_7 ;
  wire \trigger_time_s_reg[9]_i_3_n_7 ;
  wire \trigger_time_s_reg_n_0_[0] ;
  wire \trigger_time_s_reg_n_0_[1] ;
  wire \trigger_time_s_reg_n_0_[2] ;
  wire \trigger_time_s_reg_n_0_[3] ;
  wire \trigger_time_s_reg_n_0_[4] ;
  wire \trigger_time_s_reg_n_0_[5] ;
  wire \trigger_time_s_reg_n_0_[6] ;
  wire \trigger_time_s_reg_n_0_[7] ;
  wire \trigger_time_s_reg_n_0_[8] ;
  wire \trigger_time_s_reg_n_0_[9] ;
  wire \trigger_volt_s[0]_i_1_n_0 ;
  wire \trigger_volt_s[4]_i_2_n_0 ;
  wire \trigger_volt_s[4]_i_3_n_0 ;
  wire \trigger_volt_s[4]_i_4_n_0 ;
  wire \trigger_volt_s[4]_i_5_n_0 ;
  wire \trigger_volt_s[4]_i_6_n_0 ;
  wire \trigger_volt_s[8]_i_2_n_0 ;
  wire \trigger_volt_s[8]_i_3_n_0 ;
  wire \trigger_volt_s[8]_i_4_n_0 ;
  wire \trigger_volt_s[8]_i_5_n_0 ;
  wire \trigger_volt_s[9]_i_1_n_0 ;
  wire \trigger_volt_s[9]_i_2_n_0 ;
  wire \trigger_volt_s[9]_i_4_n_0 ;
  wire \trigger_volt_s[9]_i_5_n_0 ;
  wire \trigger_volt_s[9]_i_6_n_0 ;
  wire \trigger_volt_s[9]_i_7_n_0 ;
  wire \trigger_volt_s_reg[4]_i_1_n_0 ;
  wire \trigger_volt_s_reg[4]_i_1_n_1 ;
  wire \trigger_volt_s_reg[4]_i_1_n_2 ;
  wire \trigger_volt_s_reg[4]_i_1_n_3 ;
  wire \trigger_volt_s_reg[4]_i_1_n_4 ;
  wire \trigger_volt_s_reg[4]_i_1_n_5 ;
  wire \trigger_volt_s_reg[4]_i_1_n_6 ;
  wire \trigger_volt_s_reg[4]_i_1_n_7 ;
  wire \trigger_volt_s_reg[8]_i_1_n_0 ;
  wire \trigger_volt_s_reg[8]_i_1_n_1 ;
  wire \trigger_volt_s_reg[8]_i_1_n_2 ;
  wire \trigger_volt_s_reg[8]_i_1_n_3 ;
  wire \trigger_volt_s_reg[8]_i_1_n_4 ;
  wire \trigger_volt_s_reg[8]_i_1_n_5 ;
  wire \trigger_volt_s_reg[8]_i_1_n_6 ;
  wire \trigger_volt_s_reg[8]_i_1_n_7 ;
  wire \trigger_volt_s_reg[9]_i_3_n_7 ;
  wire \trigger_volt_s_reg_n_0_[0] ;
  wire \trigger_volt_s_reg_n_0_[1] ;
  wire \trigger_volt_s_reg_n_0_[2] ;
  wire \trigger_volt_s_reg_n_0_[3] ;
  wire \trigger_volt_s_reg_n_0_[4] ;
  wire \trigger_volt_s_reg_n_0_[5] ;
  wire \trigger_volt_s_reg_n_0_[6] ;
  wire \trigger_volt_s_reg_n_0_[7] ;
  wire \trigger_volt_s_reg_n_0_[8] ;
  wire \trigger_volt_s_reg_n_0_[9] ;
  wire video_inst_n_10;
  wire video_inst_n_11;
  wire video_inst_n_12;
  wire video_inst_n_13;
  wire video_inst_n_14;
  wire video_inst_n_15;
  wire video_inst_n_8;
  wire video_inst_n_9;
  wire [9:0]write_cntr;
  wire [3:2]\NLW_clock_divider_reg[20]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_clock_divider_reg[20]_i_1_O_UNCONNECTED ;
  wire [3:0]NLW_i___0_i_12_O_UNCONNECTED;
  wire [3:1]NLW_i___0_i_2_CO_UNCONNECTED;
  wire [3:0]NLW_i___0_i_2_O_UNCONNECTED;
  wire [3:1]NLW_i___0_i_4_CO_UNCONNECTED;
  wire [3:0]NLW_i___0_i_4_O_UNCONNECTED;
  wire [3:0]NLW_i___0_i_6_O_UNCONNECTED;
  wire [3:0]\NLW_trigger_time_s_reg[9]_i_3_CO_UNCONNECTED ;
  wire [3:1]\NLW_trigger_time_s_reg[9]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_trigger_volt_s_reg[9]_i_3_CO_UNCONNECTED ;
  wire [3:1]\NLW_trigger_volt_s_reg[9]_i_3_O_UNCONNECTED ;

  FDRE \L_bus_in_s_reg[0] 
       (.C(clk),
        .CE(sw[0]),
        .D(L_bus_out_s[0]),
        .Q(L_bus_in[0]),
        .R(SR));
  FDRE \L_bus_in_s_reg[10] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_38),
        .Q(L_bus_in[10]),
        .R(SR));
  FDRE \L_bus_in_s_reg[11] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_37),
        .Q(L_bus_in[11]),
        .R(SR));
  FDRE \L_bus_in_s_reg[12] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_36),
        .Q(L_bus_in[12]),
        .R(SR));
  FDRE \L_bus_in_s_reg[13] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_35),
        .Q(L_bus_in[13]),
        .R(SR));
  FDRE \L_bus_in_s_reg[14] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_34),
        .Q(L_bus_in[14]),
        .R(SR));
  FDRE \L_bus_in_s_reg[15] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_33),
        .Q(L_bus_in[15]),
        .R(SR));
  FDRE \L_bus_in_s_reg[16] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_32),
        .Q(L_bus_in[16]),
        .R(SR));
  FDRE \L_bus_in_s_reg[17] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_31),
        .Q(L_bus_in[17]),
        .R(SR));
  FDRE \L_bus_in_s_reg[1] 
       (.C(clk),
        .CE(sw[0]),
        .D(L_bus_out_s[1]),
        .Q(L_bus_in[1]),
        .R(SR));
  FDRE \L_bus_in_s_reg[2] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_46),
        .Q(L_bus_in[2]),
        .R(SR));
  FDRE \L_bus_in_s_reg[3] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_45),
        .Q(L_bus_in[3]),
        .R(SR));
  FDRE \L_bus_in_s_reg[4] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_44),
        .Q(L_bus_in[4]),
        .R(SR));
  FDRE \L_bus_in_s_reg[5] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_43),
        .Q(L_bus_in[5]),
        .R(SR));
  FDRE \L_bus_in_s_reg[6] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_42),
        .Q(L_bus_in[6]),
        .R(SR));
  FDRE \L_bus_in_s_reg[7] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_41),
        .Q(L_bus_in[7]),
        .R(SR));
  FDRE \L_bus_in_s_reg[8] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_40),
        .Q(L_bus_in[8]),
        .R(SR));
  FDRE \L_bus_in_s_reg[9] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_39),
        .Q(L_bus_in[9]),
        .R(SR));
  FDRE \L_unsigned_data_prev_reg[0] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_39),
        .Q(L_unsigned_data_prev[0]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[1] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_38),
        .Q(L_unsigned_data_prev[1]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[2] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_37),
        .Q(L_unsigned_data_prev[2]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[3] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_55),
        .Q(L_unsigned_data_prev[3]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[4] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_54),
        .Q(L_unsigned_data_prev[4]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[5] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_53),
        .Q(L_unsigned_data_prev[5]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[6] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_52),
        .Q(L_unsigned_data_prev[6]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[7] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_51),
        .Q(L_unsigned_data_prev[7]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[8] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_50),
        .Q(L_unsigned_data_prev[8]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[9] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_49),
        .Q(L_unsigned_data_prev[9]),
        .R(1'b0));
  FDRE \R_bus_in_s_reg[0] 
       (.C(clk),
        .CE(sw[0]),
        .D(R_bus_out_s[0]),
        .Q(R_bus_in[0]),
        .R(SR));
  FDRE \R_bus_in_s_reg[10] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_13),
        .Q(R_bus_in[10]),
        .R(SR));
  FDRE \R_bus_in_s_reg[11] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_12),
        .Q(R_bus_in[11]),
        .R(SR));
  FDRE \R_bus_in_s_reg[12] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_11),
        .Q(R_bus_in[12]),
        .R(SR));
  FDRE \R_bus_in_s_reg[13] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_10),
        .Q(R_bus_in[13]),
        .R(SR));
  FDRE \R_bus_in_s_reg[14] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_9),
        .Q(R_bus_in[14]),
        .R(SR));
  FDRE \R_bus_in_s_reg[15] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_8),
        .Q(R_bus_in[15]),
        .R(SR));
  FDRE \R_bus_in_s_reg[16] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_7),
        .Q(R_bus_in[16]),
        .R(SR));
  FDRE \R_bus_in_s_reg[17] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_6),
        .Q(R_bus_in[17]),
        .R(SR));
  FDRE \R_bus_in_s_reg[1] 
       (.C(clk),
        .CE(sw[0]),
        .D(R_bus_out_s[1]),
        .Q(R_bus_in[1]),
        .R(SR));
  FDRE \R_bus_in_s_reg[2] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_21),
        .Q(R_bus_in[2]),
        .R(SR));
  FDRE \R_bus_in_s_reg[3] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_20),
        .Q(R_bus_in[3]),
        .R(SR));
  FDRE \R_bus_in_s_reg[4] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_19),
        .Q(R_bus_in[4]),
        .R(SR));
  FDRE \R_bus_in_s_reg[5] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_18),
        .Q(R_bus_in[5]),
        .R(SR));
  FDRE \R_bus_in_s_reg[6] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_17),
        .Q(R_bus_in[6]),
        .R(SR));
  FDRE \R_bus_in_s_reg[7] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_16),
        .Q(R_bus_in[7]),
        .R(SR));
  FDRE \R_bus_in_s_reg[8] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_15),
        .Q(R_bus_in[8]),
        .R(SR));
  FDRE \R_bus_in_s_reg[9] 
       (.C(clk),
        .CE(sw[0]),
        .D(audio_codec_n_14),
        .Q(R_bus_in[9]),
        .R(SR));
  FDRE \R_unsigned_data_prev_reg[0] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_14),
        .Q(R_unsigned_data_prev[0]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[1] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_13),
        .Q(R_unsigned_data_prev[1]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[2] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_12),
        .Q(R_unsigned_data_prev[2]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[3] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_30),
        .Q(R_unsigned_data_prev[3]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[4] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_29),
        .Q(R_unsigned_data_prev[4]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[5] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_28),
        .Q(R_unsigned_data_prev[5]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[6] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_27),
        .Q(R_unsigned_data_prev[6]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[7] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_26),
        .Q(R_unsigned_data_prev[7]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[8] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_25),
        .Q(R_unsigned_data_prev[8]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[9] 
       (.C(sw[0]),
        .CE(1'b1),
        .D(audio_codec_n_24),
        .Q(R_unsigned_data_prev[9]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper audio_codec
       (.BCLK_int_reg(SR),
        .CLK(sw[0]),
        .CO(R_bus_l),
        .D({audio_codec_n_24,audio_codec_n_25,audio_codec_n_26,audio_codec_n_27,audio_codec_n_28,audio_codec_n_29,audio_codec_n_30}),
        .\L_bus_in_s_reg[17] ({audio_codec_n_31,audio_codec_n_32,audio_codec_n_33,audio_codec_n_34,audio_codec_n_35,audio_codec_n_36,audio_codec_n_37,audio_codec_n_38,audio_codec_n_39,audio_codec_n_40,audio_codec_n_41,audio_codec_n_42,audio_codec_n_43,audio_codec_n_44,audio_codec_n_45,audio_codec_n_46,L_bus_out_s}),
        .\L_bus_in_s_reg[17]_0 (L_bus_in),
        .Q(R_bus_in),
        .\R_bus_in_s_reg[17] ({audio_codec_n_6,audio_codec_n_7,audio_codec_n_8,audio_codec_n_9,audio_codec_n_10,audio_codec_n_11,audio_codec_n_12,audio_codec_n_13,audio_codec_n_14,audio_codec_n_15,audio_codec_n_16,audio_codec_n_17,audio_codec_n_18,audio_codec_n_19,audio_codec_n_20,audio_codec_n_21,R_bus_out_s}),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .clk(clk),
        .lopt(lopt),
        .reset_n(reset_n),
        .scl(scl),
        .sda(sda),
        .\sdp_bl.ramb18_dp_bl.ram18_bl ({audio_codec_n_49,audio_codec_n_50,audio_codec_n_51,audio_codec_n_52,audio_codec_n_53,audio_codec_n_54,audio_codec_n_55}),
        .sw(sw[2]),
        .switch(switch[3]),
        .\trigger_volt_s_reg[9] (L_bus_l),
        .\trigger_volt_s_reg[9]_0 ({\trigger_volt_s_reg_n_0_[9] ,\trigger_volt_s_reg_n_0_[8] ,\trigger_volt_s_reg_n_0_[7] ,\trigger_volt_s_reg_n_0_[6] ,\trigger_volt_s_reg_n_0_[5] ,\trigger_volt_s_reg_n_0_[4] ,\trigger_volt_s_reg_n_0_[3] ,\trigger_volt_s_reg_n_0_[2] ,\trigger_volt_s_reg_n_0_[1] ,\trigger_volt_s_reg_n_0_[0] }));
  LUT1 #(
    .INIT(2'h1)) 
    \clock_divider[0]_i_2 
       (.I0(\clock_divider_reg_n_0_[0] ),
        .O(\clock_divider[0]_i_2_n_0 ));
  FDRE \clock_divider_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[0]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[0] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\clock_divider_reg[0]_i_1_n_0 ,\clock_divider_reg[0]_i_1_n_1 ,\clock_divider_reg[0]_i_1_n_2 ,\clock_divider_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\clock_divider_reg[0]_i_1_n_4 ,\clock_divider_reg[0]_i_1_n_5 ,\clock_divider_reg[0]_i_1_n_6 ,\clock_divider_reg[0]_i_1_n_7 }),
        .S({\clock_divider_reg_n_0_[3] ,\clock_divider_reg_n_0_[2] ,\clock_divider_reg_n_0_[1] ,\clock_divider[0]_i_2_n_0 }));
  FDRE \clock_divider_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[8]_i_1_n_5 ),
        .Q(\clock_divider_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \clock_divider_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[8]_i_1_n_4 ),
        .Q(\clock_divider_reg_n_0_[11] ),
        .R(1'b0));
  FDRE \clock_divider_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[12]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[12] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[12]_i_1 
       (.CI(\clock_divider_reg[8]_i_1_n_0 ),
        .CO({\clock_divider_reg[12]_i_1_n_0 ,\clock_divider_reg[12]_i_1_n_1 ,\clock_divider_reg[12]_i_1_n_2 ,\clock_divider_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clock_divider_reg[12]_i_1_n_4 ,\clock_divider_reg[12]_i_1_n_5 ,\clock_divider_reg[12]_i_1_n_6 ,\clock_divider_reg[12]_i_1_n_7 }),
        .S({\clock_divider_reg_n_0_[15] ,\clock_divider_reg_n_0_[14] ,\clock_divider_reg_n_0_[13] ,\clock_divider_reg_n_0_[12] }));
  FDRE \clock_divider_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[12]_i_1_n_6 ),
        .Q(\clock_divider_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \clock_divider_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[12]_i_1_n_5 ),
        .Q(\clock_divider_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \clock_divider_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[12]_i_1_n_4 ),
        .Q(\clock_divider_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \clock_divider_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[16]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[16] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[16]_i_1 
       (.CI(\clock_divider_reg[12]_i_1_n_0 ),
        .CO({\clock_divider_reg[16]_i_1_n_0 ,\clock_divider_reg[16]_i_1_n_1 ,\clock_divider_reg[16]_i_1_n_2 ,\clock_divider_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clock_divider_reg[16]_i_1_n_4 ,\clock_divider_reg[16]_i_1_n_5 ,\clock_divider_reg[16]_i_1_n_6 ,\clock_divider_reg[16]_i_1_n_7 }),
        .S({\clock_divider_reg_n_0_[19] ,\clock_divider_reg_n_0_[18] ,\clock_divider_reg_n_0_[17] ,\clock_divider_reg_n_0_[16] }));
  FDRE \clock_divider_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[16]_i_1_n_6 ),
        .Q(\clock_divider_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \clock_divider_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[16]_i_1_n_5 ),
        .Q(\clock_divider_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \clock_divider_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[16]_i_1_n_4 ),
        .Q(\clock_divider_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \clock_divider_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[0]_i_1_n_6 ),
        .Q(\clock_divider_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \clock_divider_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[20]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[20] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[20]_i_1 
       (.CI(\clock_divider_reg[16]_i_1_n_0 ),
        .CO({\NLW_clock_divider_reg[20]_i_1_CO_UNCONNECTED [3:2],\clock_divider_reg[20]_i_1_n_2 ,\clock_divider_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_clock_divider_reg[20]_i_1_O_UNCONNECTED [3],\clock_divider_reg[20]_i_1_n_5 ,\clock_divider_reg[20]_i_1_n_6 ,\clock_divider_reg[20]_i_1_n_7 }),
        .S({1'b0,clock_divider_reg,\clock_divider_reg_n_0_[20] }));
  FDRE \clock_divider_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[20]_i_1_n_6 ),
        .Q(clock_divider_reg[21]),
        .R(1'b0));
  FDRE \clock_divider_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[20]_i_1_n_5 ),
        .Q(clock_divider_reg[22]),
        .R(1'b0));
  FDRE \clock_divider_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[0]_i_1_n_5 ),
        .Q(\clock_divider_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \clock_divider_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[0]_i_1_n_4 ),
        .Q(\clock_divider_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \clock_divider_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[4]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[4] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[4]_i_1 
       (.CI(\clock_divider_reg[0]_i_1_n_0 ),
        .CO({\clock_divider_reg[4]_i_1_n_0 ,\clock_divider_reg[4]_i_1_n_1 ,\clock_divider_reg[4]_i_1_n_2 ,\clock_divider_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clock_divider_reg[4]_i_1_n_4 ,\clock_divider_reg[4]_i_1_n_5 ,\clock_divider_reg[4]_i_1_n_6 ,\clock_divider_reg[4]_i_1_n_7 }),
        .S({\clock_divider_reg_n_0_[7] ,\clock_divider_reg_n_0_[6] ,\clock_divider_reg_n_0_[5] ,\clock_divider_reg_n_0_[4] }));
  FDRE \clock_divider_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[4]_i_1_n_6 ),
        .Q(\clock_divider_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \clock_divider_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[4]_i_1_n_5 ),
        .Q(\clock_divider_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \clock_divider_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[4]_i_1_n_4 ),
        .Q(\clock_divider_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \clock_divider_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[8]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[8] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[8]_i_1 
       (.CI(\clock_divider_reg[4]_i_1_n_0 ),
        .CO({\clock_divider_reg[8]_i_1_n_0 ,\clock_divider_reg[8]_i_1_n_1 ,\clock_divider_reg[8]_i_1_n_2 ,\clock_divider_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clock_divider_reg[8]_i_1_n_4 ,\clock_divider_reg[8]_i_1_n_5 ,\clock_divider_reg[8]_i_1_n_6 ,\clock_divider_reg[8]_i_1_n_7 }),
        .S({\clock_divider_reg_n_0_[11] ,\clock_divider_reg_n_0_[10] ,\clock_divider_reg_n_0_[9] ,\clock_divider_reg_n_0_[8] }));
  FDRE \clock_divider_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[8]_i_1_n_6 ),
        .Q(\clock_divider_reg_n_0_[9] ),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i___0_i_12
       (.CI(1'b0),
        .CO({i___0_i_12_n_0,i___0_i_12_n_1,i___0_i_12_n_2,i___0_i_12_n_3}),
        .CYINIT(1'b1),
        .DI({i___0_i_35_n_0,i___0_i_36_n_0,i___0_i_37_n_0,i___0_i_38_n_0}),
        .O(NLW_i___0_i_12_O_UNCONNECTED[3:0]),
        .S({i___0_i_39_n_0,i___0_i_40_n_0,i___0_i_41_n_0,i___0_i_42_n_0}));
  LUT4 #(
    .INIT(16'h22B2)) 
    i___0_i_13
       (.I0(\trigger_volt_s_reg_n_0_[9] ),
        .I1(L_unsigned_data_prev[9]),
        .I2(\trigger_volt_s_reg_n_0_[8] ),
        .I3(L_unsigned_data_prev[8]),
        .O(i___0_i_13_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_14
       (.I0(L_unsigned_data_prev[9]),
        .I1(\trigger_volt_s_reg_n_0_[9] ),
        .I2(L_unsigned_data_prev[8]),
        .I3(\trigger_volt_s_reg_n_0_[8] ),
        .O(i___0_i_14_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i___0_i_18
       (.I0(\trigger_volt_s_reg_n_0_[7] ),
        .I1(R_unsigned_data_prev[7]),
        .I2(\trigger_volt_s_reg_n_0_[6] ),
        .I3(R_unsigned_data_prev[6]),
        .O(i___0_i_18_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i___0_i_19
       (.I0(\trigger_volt_s_reg_n_0_[5] ),
        .I1(R_unsigned_data_prev[5]),
        .I2(\trigger_volt_s_reg_n_0_[4] ),
        .I3(R_unsigned_data_prev[4]),
        .O(i___0_i_19_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i___0_i_2
       (.CI(i___0_i_6_n_0),
        .CO({NLW_i___0_i_2_CO_UNCONNECTED[3:1],R_bus_l}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,i___0_i_7_n_0}),
        .O(NLW_i___0_i_2_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,i___0_i_8_n_0}));
  LUT4 #(
    .INIT(16'h22B2)) 
    i___0_i_20
       (.I0(\trigger_volt_s_reg_n_0_[3] ),
        .I1(R_unsigned_data_prev[3]),
        .I2(\trigger_volt_s_reg_n_0_[2] ),
        .I3(R_unsigned_data_prev[2]),
        .O(i___0_i_20_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i___0_i_21
       (.I0(\trigger_volt_s_reg_n_0_[1] ),
        .I1(R_unsigned_data_prev[1]),
        .I2(\trigger_volt_s_reg_n_0_[0] ),
        .I3(R_unsigned_data_prev[0]),
        .O(i___0_i_21_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_22
       (.I0(R_unsigned_data_prev[7]),
        .I1(\trigger_volt_s_reg_n_0_[7] ),
        .I2(R_unsigned_data_prev[6]),
        .I3(\trigger_volt_s_reg_n_0_[6] ),
        .O(i___0_i_22_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_23
       (.I0(R_unsigned_data_prev[5]),
        .I1(\trigger_volt_s_reg_n_0_[5] ),
        .I2(R_unsigned_data_prev[4]),
        .I3(\trigger_volt_s_reg_n_0_[4] ),
        .O(i___0_i_23_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_24
       (.I0(R_unsigned_data_prev[3]),
        .I1(\trigger_volt_s_reg_n_0_[3] ),
        .I2(R_unsigned_data_prev[2]),
        .I3(\trigger_volt_s_reg_n_0_[2] ),
        .O(i___0_i_24_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_25
       (.I0(R_unsigned_data_prev[1]),
        .I1(\trigger_volt_s_reg_n_0_[1] ),
        .I2(R_unsigned_data_prev[0]),
        .I3(\trigger_volt_s_reg_n_0_[0] ),
        .O(i___0_i_25_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i___0_i_35
       (.I0(\trigger_volt_s_reg_n_0_[7] ),
        .I1(L_unsigned_data_prev[7]),
        .I2(\trigger_volt_s_reg_n_0_[6] ),
        .I3(L_unsigned_data_prev[6]),
        .O(i___0_i_35_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i___0_i_36
       (.I0(\trigger_volt_s_reg_n_0_[5] ),
        .I1(L_unsigned_data_prev[5]),
        .I2(\trigger_volt_s_reg_n_0_[4] ),
        .I3(L_unsigned_data_prev[4]),
        .O(i___0_i_36_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i___0_i_37
       (.I0(\trigger_volt_s_reg_n_0_[3] ),
        .I1(L_unsigned_data_prev[3]),
        .I2(\trigger_volt_s_reg_n_0_[2] ),
        .I3(L_unsigned_data_prev[2]),
        .O(i___0_i_37_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i___0_i_38
       (.I0(\trigger_volt_s_reg_n_0_[1] ),
        .I1(L_unsigned_data_prev[1]),
        .I2(\trigger_volt_s_reg_n_0_[0] ),
        .I3(L_unsigned_data_prev[0]),
        .O(i___0_i_38_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_39
       (.I0(L_unsigned_data_prev[7]),
        .I1(\trigger_volt_s_reg_n_0_[7] ),
        .I2(L_unsigned_data_prev[6]),
        .I3(\trigger_volt_s_reg_n_0_[6] ),
        .O(i___0_i_39_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i___0_i_4
       (.CI(i___0_i_12_n_0),
        .CO({NLW_i___0_i_4_CO_UNCONNECTED[3:1],L_bus_l}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,i___0_i_13_n_0}),
        .O(NLW_i___0_i_4_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,i___0_i_14_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_40
       (.I0(L_unsigned_data_prev[5]),
        .I1(\trigger_volt_s_reg_n_0_[5] ),
        .I2(L_unsigned_data_prev[4]),
        .I3(\trigger_volt_s_reg_n_0_[4] ),
        .O(i___0_i_40_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_41
       (.I0(L_unsigned_data_prev[3]),
        .I1(\trigger_volt_s_reg_n_0_[3] ),
        .I2(L_unsigned_data_prev[2]),
        .I3(\trigger_volt_s_reg_n_0_[2] ),
        .O(i___0_i_41_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_42
       (.I0(L_unsigned_data_prev[1]),
        .I1(\trigger_volt_s_reg_n_0_[1] ),
        .I2(L_unsigned_data_prev[0]),
        .I3(\trigger_volt_s_reg_n_0_[0] ),
        .O(i___0_i_42_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i___0_i_6
       (.CI(1'b0),
        .CO({i___0_i_6_n_0,i___0_i_6_n_1,i___0_i_6_n_2,i___0_i_6_n_3}),
        .CYINIT(1'b1),
        .DI({i___0_i_18_n_0,i___0_i_19_n_0,i___0_i_20_n_0,i___0_i_21_n_0}),
        .O(NLW_i___0_i_6_O_UNCONNECTED[3:0]),
        .S({i___0_i_22_n_0,i___0_i_23_n_0,i___0_i_24_n_0,i___0_i_25_n_0}));
  LUT4 #(
    .INIT(16'h22B2)) 
    i___0_i_7
       (.I0(\trigger_volt_s_reg_n_0_[9] ),
        .I1(R_unsigned_data_prev[9]),
        .I2(\trigger_volt_s_reg_n_0_[8] ),
        .I3(R_unsigned_data_prev[8]),
        .O(i___0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i___0_i_8
       (.I0(R_unsigned_data_prev[9]),
        .I1(\trigger_volt_s_reg_n_0_[9] ),
        .I2(R_unsigned_data_prev[8]),
        .I3(\trigger_volt_s_reg_n_0_[8] ),
        .O(i___0_i_8_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO lBRAM
       (.ADDRARDADDR({video_inst_n_8,video_inst_n_9,video_inst_n_10,video_inst_n_11,video_inst_n_12,video_inst_n_13,video_inst_n_14,video_inst_n_15,column__0}),
        .CO(ch1),
        .D({audio_codec_n_49,audio_codec_n_50,audio_codec_n_51,audio_codec_n_52,audio_codec_n_53,audio_codec_n_54,audio_codec_n_55,audio_codec_n_37,audio_codec_n_38,audio_codec_n_39}),
        .Q(write_cntr),
        .clk(clk),
        .cw(cw),
        .\dc_bias_reg[1] (lBRAM_n_0),
        .\processQ_reg[9] (row),
        .reset_n(SR),
        .switch(switch[0]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0 rBRAM
       (.ADDRARDADDR({video_inst_n_8,video_inst_n_9,video_inst_n_10,video_inst_n_11,video_inst_n_12,video_inst_n_13,video_inst_n_14,video_inst_n_15,column__0}),
        .CO(ch2),
        .D({audio_codec_n_24,audio_codec_n_25,audio_codec_n_26,audio_codec_n_27,audio_codec_n_28,audio_codec_n_29,audio_codec_n_30,audio_codec_n_12,audio_codec_n_13,audio_codec_n_14}),
        .Q(write_cntr),
        .clk(clk),
        .cw(cw),
        .\dc_bias_reg[1] (rBRAM_n_0),
        .\processQ_reg[9] (row),
        .reset_n(SR),
        .switch(switch[1]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample_counter sampCounter
       (.E(E),
        .Q(write_cntr),
        .clk(clk),
        .\state_reg[1] (\state_reg[1] ),
        .sw(sw[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \trigger_time_s[0]_i_1 
       (.I0(\trigger_time_s_reg_n_0_[0] ),
        .O(\trigger_time_s[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \trigger_time_s[4]_i_2 
       (.I0(\trigger_time_s_reg_n_0_[1] ),
        .O(\trigger_time_s[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_time_s[4]_i_3 
       (.I0(\trigger_time_s_reg_n_0_[3] ),
        .I1(\trigger_time_s_reg_n_0_[4] ),
        .O(\trigger_time_s[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_time_s[4]_i_4 
       (.I0(\trigger_time_s_reg_n_0_[2] ),
        .I1(\trigger_time_s_reg_n_0_[3] ),
        .O(\trigger_time_s[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_time_s[4]_i_5 
       (.I0(\trigger_time_s_reg_n_0_[2] ),
        .I1(\trigger_time_s_reg_n_0_[1] ),
        .O(\trigger_time_s[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0F0F0F0EF0F0F0F0)) 
    \trigger_time_s[4]_i_6 
       (.I0(\trigger_time_s_reg_n_0_[3] ),
        .I1(\trigger_time_s_reg_n_0_[2] ),
        .I2(\trigger_time_s_reg_n_0_[1] ),
        .I3(\trigger_time_s_reg_n_0_[0] ),
        .I4(\trigger_time_s[9]_i_8_n_0 ),
        .I5(btn[1]),
        .O(\trigger_time_s[4]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_time_s[8]_i_2 
       (.I0(\trigger_time_s_reg_n_0_[7] ),
        .I1(\trigger_time_s_reg_n_0_[8] ),
        .O(\trigger_time_s[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_time_s[8]_i_3 
       (.I0(\trigger_time_s_reg_n_0_[6] ),
        .I1(\trigger_time_s_reg_n_0_[7] ),
        .O(\trigger_time_s[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_time_s[8]_i_4 
       (.I0(\trigger_time_s_reg_n_0_[5] ),
        .I1(\trigger_time_s_reg_n_0_[6] ),
        .O(\trigger_time_s[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_time_s[8]_i_5 
       (.I0(\trigger_time_s_reg_n_0_[4] ),
        .I1(\trigger_time_s_reg_n_0_[5] ),
        .O(\trigger_time_s[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h04550000FFFFFFFF)) 
    \trigger_time_s[9]_i_1 
       (.I0(\trigger_time_s[9]_i_5_n_0 ),
        .I1(\trigger_time_s_reg_n_0_[9] ),
        .I2(\trigger_time_s[9]_i_6_n_0 ),
        .I3(btn[3]),
        .I4(btn[4]),
        .I5(reset_n),
        .O(\trigger_time_s[9]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFBAA)) 
    \trigger_time_s[9]_i_2 
       (.I0(\trigger_time_s[9]_i_5_n_0 ),
        .I1(\trigger_time_s_reg_n_0_[9] ),
        .I2(\trigger_time_s[9]_i_6_n_0 ),
        .I3(btn[3]),
        .O(\trigger_time_s[9]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \trigger_time_s[9]_i_4 
       (.I0(clock_divider_reg[21]),
        .I1(switch[2]),
        .I2(clock_divider_reg[22]),
        .O(trigger_clock));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    \trigger_time_s[9]_i_5 
       (.I0(btn[1]),
        .I1(\trigger_time_s[9]_i_8_n_0 ),
        .I2(\trigger_time_s_reg_n_0_[0] ),
        .I3(\trigger_time_s_reg_n_0_[1] ),
        .I4(\trigger_time_s_reg_n_0_[2] ),
        .I5(\trigger_time_s_reg_n_0_[3] ),
        .O(\trigger_time_s[9]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0001010111111111)) 
    \trigger_time_s[9]_i_6 
       (.I0(\trigger_time_s_reg_n_0_[8] ),
        .I1(\trigger_time_s_reg_n_0_[7] ),
        .I2(\trigger_time_s_reg_n_0_[5] ),
        .I3(\trigger_time_s_reg_n_0_[4] ),
        .I4(\trigger_time_s_reg_n_0_[3] ),
        .I5(\trigger_time_s_reg_n_0_[6] ),
        .O(\trigger_time_s[9]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_time_s[9]_i_7 
       (.I0(\trigger_time_s_reg_n_0_[8] ),
        .I1(\trigger_time_s_reg_n_0_[9] ),
        .O(\trigger_time_s[9]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \trigger_time_s[9]_i_8 
       (.I0(\trigger_time_s_reg_n_0_[8] ),
        .I1(\trigger_time_s_reg_n_0_[7] ),
        .I2(\trigger_time_s_reg_n_0_[5] ),
        .I3(\trigger_time_s_reg_n_0_[6] ),
        .I4(\trigger_time_s_reg_n_0_[4] ),
        .I5(\trigger_time_s_reg_n_0_[9] ),
        .O(\trigger_time_s[9]_i_8_n_0 ));
  FDRE \trigger_time_s_reg[0] 
       (.C(trigger_clock),
        .CE(\trigger_time_s[9]_i_2_n_0 ),
        .D(\trigger_time_s[0]_i_1_n_0 ),
        .Q(\trigger_time_s_reg_n_0_[0] ),
        .R(\trigger_time_s[9]_i_1_n_0 ));
  FDRE \trigger_time_s_reg[1] 
       (.C(trigger_clock),
        .CE(\trigger_time_s[9]_i_2_n_0 ),
        .D(\trigger_time_s_reg[4]_i_1_n_7 ),
        .Q(\trigger_time_s_reg_n_0_[1] ),
        .R(\trigger_time_s[9]_i_1_n_0 ));
  FDSE \trigger_time_s_reg[2] 
       (.C(trigger_clock),
        .CE(\trigger_time_s[9]_i_2_n_0 ),
        .D(\trigger_time_s_reg[4]_i_1_n_6 ),
        .Q(\trigger_time_s_reg_n_0_[2] ),
        .S(\trigger_time_s[9]_i_1_n_0 ));
  FDSE \trigger_time_s_reg[3] 
       (.C(trigger_clock),
        .CE(\trigger_time_s[9]_i_2_n_0 ),
        .D(\trigger_time_s_reg[4]_i_1_n_5 ),
        .Q(\trigger_time_s_reg_n_0_[3] ),
        .S(\trigger_time_s[9]_i_1_n_0 ));
  FDRE \trigger_time_s_reg[4] 
       (.C(trigger_clock),
        .CE(\trigger_time_s[9]_i_2_n_0 ),
        .D(\trigger_time_s_reg[4]_i_1_n_4 ),
        .Q(\trigger_time_s_reg_n_0_[4] ),
        .R(\trigger_time_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \trigger_time_s_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\trigger_time_s_reg[4]_i_1_n_0 ,\trigger_time_s_reg[4]_i_1_n_1 ,\trigger_time_s_reg[4]_i_1_n_2 ,\trigger_time_s_reg[4]_i_1_n_3 }),
        .CYINIT(\trigger_time_s_reg_n_0_[0] ),
        .DI({\trigger_time_s_reg_n_0_[3] ,\trigger_time_s_reg_n_0_[2] ,\trigger_time_s_reg_n_0_[1] ,\trigger_time_s[4]_i_2_n_0 }),
        .O({\trigger_time_s_reg[4]_i_1_n_4 ,\trigger_time_s_reg[4]_i_1_n_5 ,\trigger_time_s_reg[4]_i_1_n_6 ,\trigger_time_s_reg[4]_i_1_n_7 }),
        .S({\trigger_time_s[4]_i_3_n_0 ,\trigger_time_s[4]_i_4_n_0 ,\trigger_time_s[4]_i_5_n_0 ,\trigger_time_s[4]_i_6_n_0 }));
  FDSE \trigger_time_s_reg[5] 
       (.C(trigger_clock),
        .CE(\trigger_time_s[9]_i_2_n_0 ),
        .D(\trigger_time_s_reg[8]_i_1_n_7 ),
        .Q(\trigger_time_s_reg_n_0_[5] ),
        .S(\trigger_time_s[9]_i_1_n_0 ));
  FDRE \trigger_time_s_reg[6] 
       (.C(trigger_clock),
        .CE(\trigger_time_s[9]_i_2_n_0 ),
        .D(\trigger_time_s_reg[8]_i_1_n_6 ),
        .Q(\trigger_time_s_reg_n_0_[6] ),
        .R(\trigger_time_s[9]_i_1_n_0 ));
  FDRE \trigger_time_s_reg[7] 
       (.C(trigger_clock),
        .CE(\trigger_time_s[9]_i_2_n_0 ),
        .D(\trigger_time_s_reg[8]_i_1_n_5 ),
        .Q(\trigger_time_s_reg_n_0_[7] ),
        .R(\trigger_time_s[9]_i_1_n_0 ));
  FDSE \trigger_time_s_reg[8] 
       (.C(trigger_clock),
        .CE(\trigger_time_s[9]_i_2_n_0 ),
        .D(\trigger_time_s_reg[8]_i_1_n_4 ),
        .Q(\trigger_time_s_reg_n_0_[8] ),
        .S(\trigger_time_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \trigger_time_s_reg[8]_i_1 
       (.CI(\trigger_time_s_reg[4]_i_1_n_0 ),
        .CO({\trigger_time_s_reg[8]_i_1_n_0 ,\trigger_time_s_reg[8]_i_1_n_1 ,\trigger_time_s_reg[8]_i_1_n_2 ,\trigger_time_s_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\trigger_time_s_reg_n_0_[7] ,\trigger_time_s_reg_n_0_[6] ,\trigger_time_s_reg_n_0_[5] ,\trigger_time_s_reg_n_0_[4] }),
        .O({\trigger_time_s_reg[8]_i_1_n_4 ,\trigger_time_s_reg[8]_i_1_n_5 ,\trigger_time_s_reg[8]_i_1_n_6 ,\trigger_time_s_reg[8]_i_1_n_7 }),
        .S({\trigger_time_s[8]_i_2_n_0 ,\trigger_time_s[8]_i_3_n_0 ,\trigger_time_s[8]_i_4_n_0 ,\trigger_time_s[8]_i_5_n_0 }));
  FDRE \trigger_time_s_reg[9] 
       (.C(trigger_clock),
        .CE(\trigger_time_s[9]_i_2_n_0 ),
        .D(\trigger_time_s_reg[9]_i_3_n_7 ),
        .Q(\trigger_time_s_reg_n_0_[9] ),
        .R(\trigger_time_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \trigger_time_s_reg[9]_i_3 
       (.CI(\trigger_time_s_reg[8]_i_1_n_0 ),
        .CO(\NLW_trigger_time_s_reg[9]_i_3_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_trigger_time_s_reg[9]_i_3_O_UNCONNECTED [3:1],\trigger_time_s_reg[9]_i_3_n_7 }),
        .S({1'b0,1'b0,1'b0,\trigger_time_s[9]_i_7_n_0 }));
  LUT1 #(
    .INIT(2'h1)) 
    \trigger_volt_s[0]_i_1 
       (.I0(\trigger_volt_s_reg_n_0_[0] ),
        .O(\trigger_volt_s[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \trigger_volt_s[4]_i_2 
       (.I0(\trigger_volt_s_reg_n_0_[1] ),
        .O(\trigger_volt_s[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_volt_s[4]_i_3 
       (.I0(\trigger_volt_s_reg_n_0_[3] ),
        .I1(\trigger_volt_s_reg_n_0_[4] ),
        .O(\trigger_volt_s[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_volt_s[4]_i_4 
       (.I0(\trigger_volt_s_reg_n_0_[3] ),
        .I1(\trigger_volt_s_reg_n_0_[2] ),
        .O(\trigger_volt_s[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_volt_s[4]_i_5 
       (.I0(\trigger_volt_s_reg_n_0_[2] ),
        .I1(\trigger_volt_s_reg_n_0_[1] ),
        .O(\trigger_volt_s[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00FF00FEFF00FF00)) 
    \trigger_volt_s[4]_i_6 
       (.I0(\trigger_volt_s_reg_n_0_[2] ),
        .I1(\trigger_volt_s_reg_n_0_[3] ),
        .I2(\trigger_volt_s_reg_n_0_[0] ),
        .I3(\trigger_volt_s_reg_n_0_[1] ),
        .I4(\trigger_volt_s[9]_i_7_n_0 ),
        .I5(btn[0]),
        .O(\trigger_volt_s[4]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_volt_s[8]_i_2 
       (.I0(\trigger_volt_s_reg_n_0_[7] ),
        .I1(\trigger_volt_s_reg_n_0_[8] ),
        .O(\trigger_volt_s[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_volt_s[8]_i_3 
       (.I0(\trigger_volt_s_reg_n_0_[6] ),
        .I1(\trigger_volt_s_reg_n_0_[7] ),
        .O(\trigger_volt_s[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_volt_s[8]_i_4 
       (.I0(\trigger_volt_s_reg_n_0_[5] ),
        .I1(\trigger_volt_s_reg_n_0_[6] ),
        .O(\trigger_volt_s[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_volt_s[8]_i_5 
       (.I0(\trigger_volt_s_reg_n_0_[4] ),
        .I1(\trigger_volt_s_reg_n_0_[5] ),
        .O(\trigger_volt_s[8]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h4F)) 
    \trigger_volt_s[9]_i_1 
       (.I0(\trigger_volt_s[9]_i_2_n_0 ),
        .I1(btn[4]),
        .I2(reset_n),
        .O(\trigger_volt_s[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBAAABABABABABABA)) 
    \trigger_volt_s[9]_i_2 
       (.I0(\trigger_volt_s[9]_i_4_n_0 ),
        .I1(\trigger_volt_s_reg_n_0_[9] ),
        .I2(btn[2]),
        .I3(\trigger_volt_s[9]_i_5_n_0 ),
        .I4(\trigger_volt_s_reg_n_0_[7] ),
        .I5(\trigger_volt_s_reg_n_0_[8] ),
        .O(\trigger_volt_s[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    \trigger_volt_s[9]_i_4 
       (.I0(btn[0]),
        .I1(\trigger_volt_s[9]_i_7_n_0 ),
        .I2(\trigger_volt_s_reg_n_0_[1] ),
        .I3(\trigger_volt_s_reg_n_0_[0] ),
        .I4(\trigger_volt_s_reg_n_0_[3] ),
        .I5(\trigger_volt_s_reg_n_0_[2] ),
        .O(\trigger_volt_s[9]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \trigger_volt_s[9]_i_5 
       (.I0(\trigger_volt_s_reg_n_0_[6] ),
        .I1(\trigger_volt_s_reg_n_0_[5] ),
        .I2(\trigger_volt_s_reg_n_0_[4] ),
        .O(\trigger_volt_s[9]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \trigger_volt_s[9]_i_6 
       (.I0(\trigger_volt_s_reg_n_0_[8] ),
        .I1(\trigger_volt_s_reg_n_0_[9] ),
        .O(\trigger_volt_s[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \trigger_volt_s[9]_i_7 
       (.I0(\trigger_volt_s_reg_n_0_[4] ),
        .I1(\trigger_volt_s_reg_n_0_[5] ),
        .I2(\trigger_volt_s_reg_n_0_[6] ),
        .I3(\trigger_volt_s_reg_n_0_[8] ),
        .I4(\trigger_volt_s_reg_n_0_[9] ),
        .I5(\trigger_volt_s_reg_n_0_[7] ),
        .O(\trigger_volt_s[9]_i_7_n_0 ));
  FDRE \trigger_volt_s_reg[0] 
       (.C(trigger_clock),
        .CE(\trigger_volt_s[9]_i_2_n_0 ),
        .D(\trigger_volt_s[0]_i_1_n_0 ),
        .Q(\trigger_volt_s_reg_n_0_[0] ),
        .R(\trigger_volt_s[9]_i_1_n_0 ));
  FDRE \trigger_volt_s_reg[1] 
       (.C(trigger_clock),
        .CE(\trigger_volt_s[9]_i_2_n_0 ),
        .D(\trigger_volt_s_reg[4]_i_1_n_7 ),
        .Q(\trigger_volt_s_reg_n_0_[1] ),
        .R(\trigger_volt_s[9]_i_1_n_0 ));
  FDRE \trigger_volt_s_reg[2] 
       (.C(trigger_clock),
        .CE(\trigger_volt_s[9]_i_2_n_0 ),
        .D(\trigger_volt_s_reg[4]_i_1_n_6 ),
        .Q(\trigger_volt_s_reg_n_0_[2] ),
        .R(\trigger_volt_s[9]_i_1_n_0 ));
  FDSE \trigger_volt_s_reg[3] 
       (.C(trigger_clock),
        .CE(\trigger_volt_s[9]_i_2_n_0 ),
        .D(\trigger_volt_s_reg[4]_i_1_n_5 ),
        .Q(\trigger_volt_s_reg_n_0_[3] ),
        .S(\trigger_volt_s[9]_i_1_n_0 ));
  FDRE \trigger_volt_s_reg[4] 
       (.C(trigger_clock),
        .CE(\trigger_volt_s[9]_i_2_n_0 ),
        .D(\trigger_volt_s_reg[4]_i_1_n_4 ),
        .Q(\trigger_volt_s_reg_n_0_[4] ),
        .R(\trigger_volt_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \trigger_volt_s_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\trigger_volt_s_reg[4]_i_1_n_0 ,\trigger_volt_s_reg[4]_i_1_n_1 ,\trigger_volt_s_reg[4]_i_1_n_2 ,\trigger_volt_s_reg[4]_i_1_n_3 }),
        .CYINIT(\trigger_volt_s_reg_n_0_[0] ),
        .DI({\trigger_volt_s_reg_n_0_[3] ,\trigger_volt_s_reg_n_0_[2] ,\trigger_volt_s_reg_n_0_[1] ,\trigger_volt_s[4]_i_2_n_0 }),
        .O({\trigger_volt_s_reg[4]_i_1_n_4 ,\trigger_volt_s_reg[4]_i_1_n_5 ,\trigger_volt_s_reg[4]_i_1_n_6 ,\trigger_volt_s_reg[4]_i_1_n_7 }),
        .S({\trigger_volt_s[4]_i_3_n_0 ,\trigger_volt_s[4]_i_4_n_0 ,\trigger_volt_s[4]_i_5_n_0 ,\trigger_volt_s[4]_i_6_n_0 }));
  FDRE \trigger_volt_s_reg[5] 
       (.C(trigger_clock),
        .CE(\trigger_volt_s[9]_i_2_n_0 ),
        .D(\trigger_volt_s_reg[8]_i_1_n_7 ),
        .Q(\trigger_volt_s_reg_n_0_[5] ),
        .R(\trigger_volt_s[9]_i_1_n_0 ));
  FDSE \trigger_volt_s_reg[6] 
       (.C(trigger_clock),
        .CE(\trigger_volt_s[9]_i_2_n_0 ),
        .D(\trigger_volt_s_reg[8]_i_1_n_6 ),
        .Q(\trigger_volt_s_reg_n_0_[6] ),
        .S(\trigger_volt_s[9]_i_1_n_0 ));
  FDSE \trigger_volt_s_reg[7] 
       (.C(trigger_clock),
        .CE(\trigger_volt_s[9]_i_2_n_0 ),
        .D(\trigger_volt_s_reg[8]_i_1_n_5 ),
        .Q(\trigger_volt_s_reg_n_0_[7] ),
        .S(\trigger_volt_s[9]_i_1_n_0 ));
  FDRE \trigger_volt_s_reg[8] 
       (.C(trigger_clock),
        .CE(\trigger_volt_s[9]_i_2_n_0 ),
        .D(\trigger_volt_s_reg[8]_i_1_n_4 ),
        .Q(\trigger_volt_s_reg_n_0_[8] ),
        .R(\trigger_volt_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \trigger_volt_s_reg[8]_i_1 
       (.CI(\trigger_volt_s_reg[4]_i_1_n_0 ),
        .CO({\trigger_volt_s_reg[8]_i_1_n_0 ,\trigger_volt_s_reg[8]_i_1_n_1 ,\trigger_volt_s_reg[8]_i_1_n_2 ,\trigger_volt_s_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\trigger_volt_s_reg_n_0_[7] ,\trigger_volt_s_reg_n_0_[6] ,\trigger_volt_s_reg_n_0_[5] ,\trigger_volt_s_reg_n_0_[4] }),
        .O({\trigger_volt_s_reg[8]_i_1_n_4 ,\trigger_volt_s_reg[8]_i_1_n_5 ,\trigger_volt_s_reg[8]_i_1_n_6 ,\trigger_volt_s_reg[8]_i_1_n_7 }),
        .S({\trigger_volt_s[8]_i_2_n_0 ,\trigger_volt_s[8]_i_3_n_0 ,\trigger_volt_s[8]_i_4_n_0 ,\trigger_volt_s[8]_i_5_n_0 }));
  FDRE \trigger_volt_s_reg[9] 
       (.C(trigger_clock),
        .CE(\trigger_volt_s[9]_i_2_n_0 ),
        .D(\trigger_volt_s_reg[9]_i_3_n_7 ),
        .Q(\trigger_volt_s_reg_n_0_[9] ),
        .R(\trigger_volt_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \trigger_volt_s_reg[9]_i_3 
       (.CI(\trigger_volt_s_reg[8]_i_1_n_0 ),
        .CO(\NLW_trigger_volt_s_reg[9]_i_3_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_trigger_volt_s_reg[9]_i_3_O_UNCONNECTED [3:1],\trigger_volt_s_reg[9]_i_3_n_7 }),
        .S({1'b0,1'b0,1'b0,\trigger_volt_s[9]_i_6_n_0 }));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video video_inst
       (.ADDRARDADDR({video_inst_n_8,video_inst_n_9,video_inst_n_10,video_inst_n_11,video_inst_n_12,video_inst_n_13,video_inst_n_14,video_inst_n_15,column__0}),
        .CO(ch1),
        .Q({\trigger_time_s_reg_n_0_[9] ,\trigger_time_s_reg_n_0_[8] ,\trigger_time_s_reg_n_0_[7] ,\trigger_time_s_reg_n_0_[6] ,\trigger_time_s_reg_n_0_[5] ,\trigger_time_s_reg_n_0_[4] ,\trigger_time_s_reg_n_0_[3] ,\trigger_time_s_reg_n_0_[2] ,\trigger_time_s_reg_n_0_[1] ,\trigger_time_s_reg_n_0_[0] }),
        .clk(clk),
        .lopt(lopt),
        .\processQ_reg[9] (row),
        .\processQ_reg[9]_0 (lBRAM_n_0),
        .\processQ_reg[9]_1 (rBRAM_n_0),
        .\processQ_reg[9]_2 (ch2),
        .reset_n(reset_n),
        .switch(switch[1:0]),
        .tmds(tmds),
        .tmdsb(tmdsb),
        .\trigger_volt_s_reg[9] ({\trigger_volt_s_reg_n_0_[9] ,\trigger_volt_s_reg_n_0_[8] ,\trigger_volt_s_reg_n_0_[7] ,\trigger_volt_s_reg_n_0_[6] ,\trigger_volt_s_reg_n_0_[5] ,\trigger_volt_s_reg_n_0_[4] ,\trigger_volt_s_reg_n_0_[3] ,\trigger_volt_s_reg_n_0_[2] ,\trigger_volt_s_reg_n_0_[1] ,\trigger_volt_s_reg_n_0_[0] }));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm
   (\state_reg[0]_0 ,
    \state_reg[0]_1 ,
    SR,
    E,
    sw,
    reset_n,
    reset_n_0,
    clk);
  output \state_reg[0]_0 ;
  output \state_reg[0]_1 ;
  output [0:0]SR;
  output [0:0]E;
  input [2:0]sw;
  input reset_n;
  input [0:0]reset_n_0;
  input clk;

  wire [0:0]E;
  wire [0:0]SR;
  wire clk;
  wire reset_n;
  wire [0:0]reset_n_0;
  wire \state_inferred__0/i___0_n_0 ;
  wire \state_inferred__0/i__n_0 ;
  wire \state_reg[0]_0 ;
  wire \state_reg[0]_1 ;
  wire [2:0]sw;

  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h51FF)) 
    \processQ[9]_i_1__0 
       (.I0(\state_reg[0]_0 ),
        .I1(\state_reg[0]_1 ),
        .I2(sw[1]),
        .I3(reset_n),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \processQ[9]_i_2 
       (.I0(sw[1]),
        .I1(\state_reg[0]_1 ),
        .I2(\state_reg[0]_0 ),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hAB02)) 
    \state_inferred__0/i_ 
       (.I0(\state_reg[0]_0 ),
        .I1(sw[1]),
        .I2(sw[0]),
        .I3(\state_reg[0]_1 ),
        .O(\state_inferred__0/i__n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h56F556A0)) 
    \state_inferred__0/i___0 
       (.I0(\state_reg[0]_0 ),
        .I1(sw[1]),
        .I2(sw[0]),
        .I3(\state_reg[0]_1 ),
        .I4(sw[2]),
        .O(\state_inferred__0/i___0_n_0 ));
  FDRE \state_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\state_inferred__0/i___0_n_0 ),
        .Q(\state_reg[0]_1 ),
        .R(reset_n_0));
  FDRE \state_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\state_inferred__0/i__n_0 ),
        .Q(\state_reg[0]_0 ),
        .R(reset_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0
   (S_AXI_ARREADY,
    s00_axi_rvalid,
    tmds,
    tmdsb,
    ac_mclk,
    ac_dac_sdata,
    ac_lrclk,
    ac_bclk,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    s00_axi_rdata,
    s00_axi_bvalid,
    scl,
    sda,
    clk,
    ac_adc_sdata,
    reset_n,
    btn,
    s00_axi_arvalid,
    s00_axi_wstrb,
    switch,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_ARREADY;
  output s00_axi_rvalid;
  output [3:0]tmds;
  output [3:0]tmdsb;
  output ac_mclk;
  output ac_dac_sdata;
  output ac_lrclk;
  output ac_bclk;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_bvalid;
  inout scl;
  inout sda;
  input clk;
  input ac_adc_sdata;
  input reset_n;
  input [4:0]btn;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input [3:0]switch;
  input s00_axi_aclk;
  input [4:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [4:0]s00_axi_araddr;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire [4:0]btn;
  wire clk;
  wire [1:0]\control/state ;
  wire [0:0]cw;
  wire reset_n;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire scl;
  wire sda;
  wire [3:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;

  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h7444444444444444)) 
    axi_bvalid_i_1
       (.I0(s00_axi_bready),
        .I1(s00_axi_bvalid),
        .I2(S_AXI_WREADY),
        .I3(S_AXI_AWREADY),
        .I4(s00_axi_awvalid),
        .I5(s00_axi_wvalid),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0_S00_AXI my_oscope_ip_v1_0_S00_AXI_inst
       (.SR(axi_awready_i_1_n_0),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .axi_arready_reg_0(axi_rvalid_i_1_n_0),
        .axi_bvalid_reg_0(axi_bvalid_i_1_n_0),
        .btn(btn),
        .clk(clk),
        .cw(cw),
        .reset_n(reset_n),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_arready(S_AXI_ARREADY),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awready(S_AXI_AWREADY),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(S_AXI_WREADY),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .scl(scl),
        .sda(sda),
        .state(\control/state ),
        .switch(switch),
        .tmds(tmds),
        .tmdsb(tmdsb));
  LUT2 #(
    .INIT(4'h4)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_1__0 
       (.I0(\control/state [1]),
        .I1(\control/state [0]),
        .O(cw));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0_S00_AXI
   (s00_axi_wready,
    s00_axi_awready,
    s00_axi_arready,
    s00_axi_bvalid,
    s00_axi_rvalid,
    state,
    s00_axi_rdata,
    tmds,
    tmdsb,
    ac_mclk,
    ac_dac_sdata,
    ac_lrclk,
    ac_bclk,
    scl,
    sda,
    clk,
    ac_adc_sdata,
    SR,
    s00_axi_aclk,
    axi_bvalid_reg_0,
    axi_arready_reg_0,
    reset_n,
    btn,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_awvalid,
    s00_axi_wvalid,
    cw,
    switch);
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_arready;
  output s00_axi_bvalid;
  output s00_axi_rvalid;
  output [1:0]state;
  output [31:0]s00_axi_rdata;
  output [3:0]tmds;
  output [3:0]tmdsb;
  output ac_mclk;
  output ac_dac_sdata;
  output ac_lrclk;
  output ac_bclk;
  inout scl;
  inout sda;
  input clk;
  input ac_adc_sdata;
  input [0:0]SR;
  input s00_axi_aclk;
  input axi_bvalid_reg_0;
  input axi_arready_reg_0;
  input reset_n;
  input [4:0]btn;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input [4:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [4:0]s00_axi_araddr;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [0:0]cw;
  input [3:0]switch;

  wire RST;
  wire [0:0]SR;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire \axi_araddr_reg[2]_rep_n_0 ;
  wire \axi_araddr_reg[3]_rep_n_0 ;
  wire axi_arready_i_1_n_0;
  wire axi_arready_reg_0;
  wire axi_awready0;
  wire axi_bvalid_reg_0;
  wire \axi_rdata[0]_i_10_n_0 ;
  wire \axi_rdata[0]_i_11_n_0 ;
  wire \axi_rdata[0]_i_12_n_0 ;
  wire \axi_rdata[0]_i_13_n_0 ;
  wire \axi_rdata[0]_i_6_n_0 ;
  wire \axi_rdata[0]_i_7_n_0 ;
  wire \axi_rdata[0]_i_8_n_0 ;
  wire \axi_rdata[0]_i_9_n_0 ;
  wire \axi_rdata[10]_i_10_n_0 ;
  wire \axi_rdata[10]_i_11_n_0 ;
  wire \axi_rdata[10]_i_12_n_0 ;
  wire \axi_rdata[10]_i_13_n_0 ;
  wire \axi_rdata[10]_i_6_n_0 ;
  wire \axi_rdata[10]_i_7_n_0 ;
  wire \axi_rdata[10]_i_8_n_0 ;
  wire \axi_rdata[10]_i_9_n_0 ;
  wire \axi_rdata[11]_i_10_n_0 ;
  wire \axi_rdata[11]_i_11_n_0 ;
  wire \axi_rdata[11]_i_12_n_0 ;
  wire \axi_rdata[11]_i_13_n_0 ;
  wire \axi_rdata[11]_i_6_n_0 ;
  wire \axi_rdata[11]_i_7_n_0 ;
  wire \axi_rdata[11]_i_8_n_0 ;
  wire \axi_rdata[11]_i_9_n_0 ;
  wire \axi_rdata[12]_i_10_n_0 ;
  wire \axi_rdata[12]_i_11_n_0 ;
  wire \axi_rdata[12]_i_12_n_0 ;
  wire \axi_rdata[12]_i_13_n_0 ;
  wire \axi_rdata[12]_i_6_n_0 ;
  wire \axi_rdata[12]_i_7_n_0 ;
  wire \axi_rdata[12]_i_8_n_0 ;
  wire \axi_rdata[12]_i_9_n_0 ;
  wire \axi_rdata[13]_i_10_n_0 ;
  wire \axi_rdata[13]_i_11_n_0 ;
  wire \axi_rdata[13]_i_12_n_0 ;
  wire \axi_rdata[13]_i_13_n_0 ;
  wire \axi_rdata[13]_i_6_n_0 ;
  wire \axi_rdata[13]_i_7_n_0 ;
  wire \axi_rdata[13]_i_8_n_0 ;
  wire \axi_rdata[13]_i_9_n_0 ;
  wire \axi_rdata[14]_i_10_n_0 ;
  wire \axi_rdata[14]_i_11_n_0 ;
  wire \axi_rdata[14]_i_12_n_0 ;
  wire \axi_rdata[14]_i_13_n_0 ;
  wire \axi_rdata[14]_i_6_n_0 ;
  wire \axi_rdata[14]_i_7_n_0 ;
  wire \axi_rdata[14]_i_8_n_0 ;
  wire \axi_rdata[14]_i_9_n_0 ;
  wire \axi_rdata[15]_i_10_n_0 ;
  wire \axi_rdata[15]_i_11_n_0 ;
  wire \axi_rdata[15]_i_12_n_0 ;
  wire \axi_rdata[15]_i_13_n_0 ;
  wire \axi_rdata[15]_i_6_n_0 ;
  wire \axi_rdata[15]_i_7_n_0 ;
  wire \axi_rdata[15]_i_8_n_0 ;
  wire \axi_rdata[15]_i_9_n_0 ;
  wire \axi_rdata[16]_i_10_n_0 ;
  wire \axi_rdata[16]_i_11_n_0 ;
  wire \axi_rdata[16]_i_12_n_0 ;
  wire \axi_rdata[16]_i_13_n_0 ;
  wire \axi_rdata[16]_i_6_n_0 ;
  wire \axi_rdata[16]_i_7_n_0 ;
  wire \axi_rdata[16]_i_8_n_0 ;
  wire \axi_rdata[16]_i_9_n_0 ;
  wire \axi_rdata[17]_i_10_n_0 ;
  wire \axi_rdata[17]_i_11_n_0 ;
  wire \axi_rdata[17]_i_12_n_0 ;
  wire \axi_rdata[17]_i_13_n_0 ;
  wire \axi_rdata[17]_i_6_n_0 ;
  wire \axi_rdata[17]_i_7_n_0 ;
  wire \axi_rdata[17]_i_8_n_0 ;
  wire \axi_rdata[17]_i_9_n_0 ;
  wire \axi_rdata[18]_i_10_n_0 ;
  wire \axi_rdata[18]_i_11_n_0 ;
  wire \axi_rdata[18]_i_12_n_0 ;
  wire \axi_rdata[18]_i_13_n_0 ;
  wire \axi_rdata[18]_i_6_n_0 ;
  wire \axi_rdata[18]_i_7_n_0 ;
  wire \axi_rdata[18]_i_8_n_0 ;
  wire \axi_rdata[18]_i_9_n_0 ;
  wire \axi_rdata[19]_i_10_n_0 ;
  wire \axi_rdata[19]_i_11_n_0 ;
  wire \axi_rdata[19]_i_12_n_0 ;
  wire \axi_rdata[19]_i_13_n_0 ;
  wire \axi_rdata[19]_i_6_n_0 ;
  wire \axi_rdata[19]_i_7_n_0 ;
  wire \axi_rdata[19]_i_8_n_0 ;
  wire \axi_rdata[19]_i_9_n_0 ;
  wire \axi_rdata[1]_i_10_n_0 ;
  wire \axi_rdata[1]_i_11_n_0 ;
  wire \axi_rdata[1]_i_12_n_0 ;
  wire \axi_rdata[1]_i_13_n_0 ;
  wire \axi_rdata[1]_i_6_n_0 ;
  wire \axi_rdata[1]_i_7_n_0 ;
  wire \axi_rdata[1]_i_8_n_0 ;
  wire \axi_rdata[1]_i_9_n_0 ;
  wire \axi_rdata[20]_i_10_n_0 ;
  wire \axi_rdata[20]_i_11_n_0 ;
  wire \axi_rdata[20]_i_12_n_0 ;
  wire \axi_rdata[20]_i_13_n_0 ;
  wire \axi_rdata[20]_i_6_n_0 ;
  wire \axi_rdata[20]_i_7_n_0 ;
  wire \axi_rdata[20]_i_8_n_0 ;
  wire \axi_rdata[20]_i_9_n_0 ;
  wire \axi_rdata[21]_i_10_n_0 ;
  wire \axi_rdata[21]_i_11_n_0 ;
  wire \axi_rdata[21]_i_12_n_0 ;
  wire \axi_rdata[21]_i_13_n_0 ;
  wire \axi_rdata[21]_i_6_n_0 ;
  wire \axi_rdata[21]_i_7_n_0 ;
  wire \axi_rdata[21]_i_8_n_0 ;
  wire \axi_rdata[21]_i_9_n_0 ;
  wire \axi_rdata[22]_i_10_n_0 ;
  wire \axi_rdata[22]_i_11_n_0 ;
  wire \axi_rdata[22]_i_12_n_0 ;
  wire \axi_rdata[22]_i_13_n_0 ;
  wire \axi_rdata[22]_i_6_n_0 ;
  wire \axi_rdata[22]_i_7_n_0 ;
  wire \axi_rdata[22]_i_8_n_0 ;
  wire \axi_rdata[22]_i_9_n_0 ;
  wire \axi_rdata[23]_i_10_n_0 ;
  wire \axi_rdata[23]_i_11_n_0 ;
  wire \axi_rdata[23]_i_12_n_0 ;
  wire \axi_rdata[23]_i_13_n_0 ;
  wire \axi_rdata[23]_i_6_n_0 ;
  wire \axi_rdata[23]_i_7_n_0 ;
  wire \axi_rdata[23]_i_8_n_0 ;
  wire \axi_rdata[23]_i_9_n_0 ;
  wire \axi_rdata[24]_i_10_n_0 ;
  wire \axi_rdata[24]_i_11_n_0 ;
  wire \axi_rdata[24]_i_12_n_0 ;
  wire \axi_rdata[24]_i_13_n_0 ;
  wire \axi_rdata[24]_i_6_n_0 ;
  wire \axi_rdata[24]_i_7_n_0 ;
  wire \axi_rdata[24]_i_8_n_0 ;
  wire \axi_rdata[24]_i_9_n_0 ;
  wire \axi_rdata[25]_i_10_n_0 ;
  wire \axi_rdata[25]_i_11_n_0 ;
  wire \axi_rdata[25]_i_12_n_0 ;
  wire \axi_rdata[25]_i_13_n_0 ;
  wire \axi_rdata[25]_i_6_n_0 ;
  wire \axi_rdata[25]_i_7_n_0 ;
  wire \axi_rdata[25]_i_8_n_0 ;
  wire \axi_rdata[25]_i_9_n_0 ;
  wire \axi_rdata[26]_i_10_n_0 ;
  wire \axi_rdata[26]_i_11_n_0 ;
  wire \axi_rdata[26]_i_12_n_0 ;
  wire \axi_rdata[26]_i_13_n_0 ;
  wire \axi_rdata[26]_i_6_n_0 ;
  wire \axi_rdata[26]_i_7_n_0 ;
  wire \axi_rdata[26]_i_8_n_0 ;
  wire \axi_rdata[26]_i_9_n_0 ;
  wire \axi_rdata[27]_i_10_n_0 ;
  wire \axi_rdata[27]_i_11_n_0 ;
  wire \axi_rdata[27]_i_12_n_0 ;
  wire \axi_rdata[27]_i_13_n_0 ;
  wire \axi_rdata[27]_i_6_n_0 ;
  wire \axi_rdata[27]_i_7_n_0 ;
  wire \axi_rdata[27]_i_8_n_0 ;
  wire \axi_rdata[27]_i_9_n_0 ;
  wire \axi_rdata[28]_i_10_n_0 ;
  wire \axi_rdata[28]_i_11_n_0 ;
  wire \axi_rdata[28]_i_12_n_0 ;
  wire \axi_rdata[28]_i_13_n_0 ;
  wire \axi_rdata[28]_i_6_n_0 ;
  wire \axi_rdata[28]_i_7_n_0 ;
  wire \axi_rdata[28]_i_8_n_0 ;
  wire \axi_rdata[28]_i_9_n_0 ;
  wire \axi_rdata[29]_i_10_n_0 ;
  wire \axi_rdata[29]_i_11_n_0 ;
  wire \axi_rdata[29]_i_12_n_0 ;
  wire \axi_rdata[29]_i_13_n_0 ;
  wire \axi_rdata[29]_i_6_n_0 ;
  wire \axi_rdata[29]_i_7_n_0 ;
  wire \axi_rdata[29]_i_8_n_0 ;
  wire \axi_rdata[29]_i_9_n_0 ;
  wire \axi_rdata[2]_i_10_n_0 ;
  wire \axi_rdata[2]_i_11_n_0 ;
  wire \axi_rdata[2]_i_12_n_0 ;
  wire \axi_rdata[2]_i_13_n_0 ;
  wire \axi_rdata[2]_i_6_n_0 ;
  wire \axi_rdata[2]_i_7_n_0 ;
  wire \axi_rdata[2]_i_8_n_0 ;
  wire \axi_rdata[2]_i_9_n_0 ;
  wire \axi_rdata[30]_i_10_n_0 ;
  wire \axi_rdata[30]_i_11_n_0 ;
  wire \axi_rdata[30]_i_12_n_0 ;
  wire \axi_rdata[30]_i_13_n_0 ;
  wire \axi_rdata[30]_i_6_n_0 ;
  wire \axi_rdata[30]_i_7_n_0 ;
  wire \axi_rdata[30]_i_8_n_0 ;
  wire \axi_rdata[30]_i_9_n_0 ;
  wire \axi_rdata[31]_i_10_n_0 ;
  wire \axi_rdata[31]_i_11_n_0 ;
  wire \axi_rdata[31]_i_12_n_0 ;
  wire \axi_rdata[31]_i_13_n_0 ;
  wire \axi_rdata[31]_i_14_n_0 ;
  wire \axi_rdata[31]_i_1_n_0 ;
  wire \axi_rdata[31]_i_7_n_0 ;
  wire \axi_rdata[31]_i_8_n_0 ;
  wire \axi_rdata[31]_i_9_n_0 ;
  wire \axi_rdata[3]_i_10_n_0 ;
  wire \axi_rdata[3]_i_11_n_0 ;
  wire \axi_rdata[3]_i_12_n_0 ;
  wire \axi_rdata[3]_i_13_n_0 ;
  wire \axi_rdata[3]_i_6_n_0 ;
  wire \axi_rdata[3]_i_7_n_0 ;
  wire \axi_rdata[3]_i_8_n_0 ;
  wire \axi_rdata[3]_i_9_n_0 ;
  wire \axi_rdata[4]_i_10_n_0 ;
  wire \axi_rdata[4]_i_11_n_0 ;
  wire \axi_rdata[4]_i_12_n_0 ;
  wire \axi_rdata[4]_i_13_n_0 ;
  wire \axi_rdata[4]_i_6_n_0 ;
  wire \axi_rdata[4]_i_7_n_0 ;
  wire \axi_rdata[4]_i_8_n_0 ;
  wire \axi_rdata[4]_i_9_n_0 ;
  wire \axi_rdata[5]_i_10_n_0 ;
  wire \axi_rdata[5]_i_11_n_0 ;
  wire \axi_rdata[5]_i_12_n_0 ;
  wire \axi_rdata[5]_i_13_n_0 ;
  wire \axi_rdata[5]_i_6_n_0 ;
  wire \axi_rdata[5]_i_7_n_0 ;
  wire \axi_rdata[5]_i_8_n_0 ;
  wire \axi_rdata[5]_i_9_n_0 ;
  wire \axi_rdata[6]_i_10_n_0 ;
  wire \axi_rdata[6]_i_11_n_0 ;
  wire \axi_rdata[6]_i_12_n_0 ;
  wire \axi_rdata[6]_i_13_n_0 ;
  wire \axi_rdata[6]_i_6_n_0 ;
  wire \axi_rdata[6]_i_7_n_0 ;
  wire \axi_rdata[6]_i_8_n_0 ;
  wire \axi_rdata[6]_i_9_n_0 ;
  wire \axi_rdata[7]_i_10_n_0 ;
  wire \axi_rdata[7]_i_11_n_0 ;
  wire \axi_rdata[7]_i_12_n_0 ;
  wire \axi_rdata[7]_i_13_n_0 ;
  wire \axi_rdata[7]_i_6_n_0 ;
  wire \axi_rdata[7]_i_7_n_0 ;
  wire \axi_rdata[7]_i_8_n_0 ;
  wire \axi_rdata[7]_i_9_n_0 ;
  wire \axi_rdata[8]_i_10_n_0 ;
  wire \axi_rdata[8]_i_11_n_0 ;
  wire \axi_rdata[8]_i_12_n_0 ;
  wire \axi_rdata[8]_i_13_n_0 ;
  wire \axi_rdata[8]_i_6_n_0 ;
  wire \axi_rdata[8]_i_7_n_0 ;
  wire \axi_rdata[8]_i_8_n_0 ;
  wire \axi_rdata[8]_i_9_n_0 ;
  wire \axi_rdata[9]_i_10_n_0 ;
  wire \axi_rdata[9]_i_11_n_0 ;
  wire \axi_rdata[9]_i_12_n_0 ;
  wire \axi_rdata[9]_i_13_n_0 ;
  wire \axi_rdata[9]_i_6_n_0 ;
  wire \axi_rdata[9]_i_7_n_0 ;
  wire \axi_rdata[9]_i_8_n_0 ;
  wire \axi_rdata[9]_i_9_n_0 ;
  wire \axi_rdata_reg[0]_i_2_n_0 ;
  wire \axi_rdata_reg[0]_i_3_n_0 ;
  wire \axi_rdata_reg[0]_i_4_n_0 ;
  wire \axi_rdata_reg[0]_i_5_n_0 ;
  wire \axi_rdata_reg[10]_i_2_n_0 ;
  wire \axi_rdata_reg[10]_i_3_n_0 ;
  wire \axi_rdata_reg[10]_i_4_n_0 ;
  wire \axi_rdata_reg[10]_i_5_n_0 ;
  wire \axi_rdata_reg[11]_i_2_n_0 ;
  wire \axi_rdata_reg[11]_i_3_n_0 ;
  wire \axi_rdata_reg[11]_i_4_n_0 ;
  wire \axi_rdata_reg[11]_i_5_n_0 ;
  wire \axi_rdata_reg[12]_i_2_n_0 ;
  wire \axi_rdata_reg[12]_i_3_n_0 ;
  wire \axi_rdata_reg[12]_i_4_n_0 ;
  wire \axi_rdata_reg[12]_i_5_n_0 ;
  wire \axi_rdata_reg[13]_i_2_n_0 ;
  wire \axi_rdata_reg[13]_i_3_n_0 ;
  wire \axi_rdata_reg[13]_i_4_n_0 ;
  wire \axi_rdata_reg[13]_i_5_n_0 ;
  wire \axi_rdata_reg[14]_i_2_n_0 ;
  wire \axi_rdata_reg[14]_i_3_n_0 ;
  wire \axi_rdata_reg[14]_i_4_n_0 ;
  wire \axi_rdata_reg[14]_i_5_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_3_n_0 ;
  wire \axi_rdata_reg[15]_i_4_n_0 ;
  wire \axi_rdata_reg[15]_i_5_n_0 ;
  wire \axi_rdata_reg[16]_i_2_n_0 ;
  wire \axi_rdata_reg[16]_i_3_n_0 ;
  wire \axi_rdata_reg[16]_i_4_n_0 ;
  wire \axi_rdata_reg[16]_i_5_n_0 ;
  wire \axi_rdata_reg[17]_i_2_n_0 ;
  wire \axi_rdata_reg[17]_i_3_n_0 ;
  wire \axi_rdata_reg[17]_i_4_n_0 ;
  wire \axi_rdata_reg[17]_i_5_n_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[18]_i_3_n_0 ;
  wire \axi_rdata_reg[18]_i_4_n_0 ;
  wire \axi_rdata_reg[18]_i_5_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_3_n_0 ;
  wire \axi_rdata_reg[19]_i_4_n_0 ;
  wire \axi_rdata_reg[19]_i_5_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_3_n_0 ;
  wire \axi_rdata_reg[1]_i_4_n_0 ;
  wire \axi_rdata_reg[1]_i_5_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_3_n_0 ;
  wire \axi_rdata_reg[20]_i_4_n_0 ;
  wire \axi_rdata_reg[20]_i_5_n_0 ;
  wire \axi_rdata_reg[21]_i_2_n_0 ;
  wire \axi_rdata_reg[21]_i_3_n_0 ;
  wire \axi_rdata_reg[21]_i_4_n_0 ;
  wire \axi_rdata_reg[21]_i_5_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_3_n_0 ;
  wire \axi_rdata_reg[22]_i_4_n_0 ;
  wire \axi_rdata_reg[22]_i_5_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_3_n_0 ;
  wire \axi_rdata_reg[23]_i_4_n_0 ;
  wire \axi_rdata_reg[23]_i_5_n_0 ;
  wire \axi_rdata_reg[24]_i_2_n_0 ;
  wire \axi_rdata_reg[24]_i_3_n_0 ;
  wire \axi_rdata_reg[24]_i_4_n_0 ;
  wire \axi_rdata_reg[24]_i_5_n_0 ;
  wire \axi_rdata_reg[25]_i_2_n_0 ;
  wire \axi_rdata_reg[25]_i_3_n_0 ;
  wire \axi_rdata_reg[25]_i_4_n_0 ;
  wire \axi_rdata_reg[25]_i_5_n_0 ;
  wire \axi_rdata_reg[26]_i_2_n_0 ;
  wire \axi_rdata_reg[26]_i_3_n_0 ;
  wire \axi_rdata_reg[26]_i_4_n_0 ;
  wire \axi_rdata_reg[26]_i_5_n_0 ;
  wire \axi_rdata_reg[27]_i_2_n_0 ;
  wire \axi_rdata_reg[27]_i_3_n_0 ;
  wire \axi_rdata_reg[27]_i_4_n_0 ;
  wire \axi_rdata_reg[27]_i_5_n_0 ;
  wire \axi_rdata_reg[28]_i_2_n_0 ;
  wire \axi_rdata_reg[28]_i_3_n_0 ;
  wire \axi_rdata_reg[28]_i_4_n_0 ;
  wire \axi_rdata_reg[28]_i_5_n_0 ;
  wire \axi_rdata_reg[29]_i_2_n_0 ;
  wire \axi_rdata_reg[29]_i_3_n_0 ;
  wire \axi_rdata_reg[29]_i_4_n_0 ;
  wire \axi_rdata_reg[29]_i_5_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_3_n_0 ;
  wire \axi_rdata_reg[2]_i_4_n_0 ;
  wire \axi_rdata_reg[2]_i_5_n_0 ;
  wire \axi_rdata_reg[30]_i_2_n_0 ;
  wire \axi_rdata_reg[30]_i_3_n_0 ;
  wire \axi_rdata_reg[30]_i_4_n_0 ;
  wire \axi_rdata_reg[30]_i_5_n_0 ;
  wire \axi_rdata_reg[31]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_4_n_0 ;
  wire \axi_rdata_reg[31]_i_5_n_0 ;
  wire \axi_rdata_reg[31]_i_6_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[3]_i_3_n_0 ;
  wire \axi_rdata_reg[3]_i_4_n_0 ;
  wire \axi_rdata_reg[3]_i_5_n_0 ;
  wire \axi_rdata_reg[4]_i_2_n_0 ;
  wire \axi_rdata_reg[4]_i_3_n_0 ;
  wire \axi_rdata_reg[4]_i_4_n_0 ;
  wire \axi_rdata_reg[4]_i_5_n_0 ;
  wire \axi_rdata_reg[5]_i_2_n_0 ;
  wire \axi_rdata_reg[5]_i_3_n_0 ;
  wire \axi_rdata_reg[5]_i_4_n_0 ;
  wire \axi_rdata_reg[5]_i_5_n_0 ;
  wire \axi_rdata_reg[6]_i_2_n_0 ;
  wire \axi_rdata_reg[6]_i_3_n_0 ;
  wire \axi_rdata_reg[6]_i_4_n_0 ;
  wire \axi_rdata_reg[6]_i_5_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_3_n_0 ;
  wire \axi_rdata_reg[7]_i_4_n_0 ;
  wire \axi_rdata_reg[7]_i_5_n_0 ;
  wire \axi_rdata_reg[8]_i_2_n_0 ;
  wire \axi_rdata_reg[8]_i_3_n_0 ;
  wire \axi_rdata_reg[8]_i_4_n_0 ;
  wire \axi_rdata_reg[8]_i_5_n_0 ;
  wire \axi_rdata_reg[9]_i_2_n_0 ;
  wire \axi_rdata_reg[9]_i_3_n_0 ;
  wire \axi_rdata_reg[9]_i_4_n_0 ;
  wire \axi_rdata_reg[9]_i_5_n_0 ;
  wire axi_wready0;
  wire [4:0]btn;
  wire clk;
  wire control_n_2;
  wire [0:0]cw;
  wire [4:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out__0;
  wire reset_n;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire \sampCounter/processQ0 ;
  wire scl;
  wire sda;
  wire [4:0]sel0;
  wire [31:0]slv_reg0;
  wire \slv_reg0[31]_i_2_n_0 ;
  wire [31:0]slv_reg1;
  wire [31:0]slv_reg10;
  wire \slv_reg10[15]_i_1_n_0 ;
  wire \slv_reg10[23]_i_1_n_0 ;
  wire \slv_reg10[31]_i_1_n_0 ;
  wire \slv_reg10[7]_i_1_n_0 ;
  wire [31:0]slv_reg11;
  wire \slv_reg11[15]_i_1_n_0 ;
  wire \slv_reg11[23]_i_1_n_0 ;
  wire \slv_reg11[31]_i_1_n_0 ;
  wire \slv_reg11[7]_i_1_n_0 ;
  wire [31:0]slv_reg12;
  wire \slv_reg12[15]_i_1_n_0 ;
  wire \slv_reg12[23]_i_1_n_0 ;
  wire \slv_reg12[31]_i_1_n_0 ;
  wire \slv_reg12[7]_i_1_n_0 ;
  wire [31:0]slv_reg13;
  wire \slv_reg13[15]_i_1_n_0 ;
  wire \slv_reg13[23]_i_1_n_0 ;
  wire \slv_reg13[31]_i_1_n_0 ;
  wire \slv_reg13[7]_i_1_n_0 ;
  wire [31:0]slv_reg14;
  wire \slv_reg14[15]_i_1_n_0 ;
  wire \slv_reg14[23]_i_1_n_0 ;
  wire \slv_reg14[31]_i_1_n_0 ;
  wire \slv_reg14[7]_i_1_n_0 ;
  wire [31:0]slv_reg15;
  wire \slv_reg15[15]_i_1_n_0 ;
  wire \slv_reg15[23]_i_1_n_0 ;
  wire \slv_reg15[31]_i_1_n_0 ;
  wire \slv_reg15[7]_i_1_n_0 ;
  wire [31:0]slv_reg16;
  wire \slv_reg16[15]_i_1_n_0 ;
  wire \slv_reg16[23]_i_1_n_0 ;
  wire \slv_reg16[31]_i_1_n_0 ;
  wire \slv_reg16[7]_i_1_n_0 ;
  wire [31:0]slv_reg17;
  wire \slv_reg17[15]_i_1_n_0 ;
  wire \slv_reg17[23]_i_1_n_0 ;
  wire \slv_reg17[31]_i_1_n_0 ;
  wire \slv_reg17[7]_i_1_n_0 ;
  wire [31:0]slv_reg18;
  wire \slv_reg18[15]_i_1_n_0 ;
  wire \slv_reg18[23]_i_1_n_0 ;
  wire \slv_reg18[31]_i_1_n_0 ;
  wire \slv_reg18[7]_i_1_n_0 ;
  wire [31:0]slv_reg19;
  wire \slv_reg19[15]_i_1_n_0 ;
  wire \slv_reg19[23]_i_1_n_0 ;
  wire \slv_reg19[31]_i_1_n_0 ;
  wire \slv_reg19[7]_i_1_n_0 ;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[31]_i_2_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg2;
  wire [31:0]slv_reg20;
  wire \slv_reg20[15]_i_1_n_0 ;
  wire \slv_reg20[23]_i_1_n_0 ;
  wire \slv_reg20[31]_i_1_n_0 ;
  wire \slv_reg20[7]_i_1_n_0 ;
  wire [31:0]slv_reg21;
  wire \slv_reg21[15]_i_1_n_0 ;
  wire \slv_reg21[23]_i_1_n_0 ;
  wire \slv_reg21[31]_i_1_n_0 ;
  wire \slv_reg21[7]_i_1_n_0 ;
  wire [31:0]slv_reg22;
  wire \slv_reg22[15]_i_1_n_0 ;
  wire \slv_reg22[23]_i_1_n_0 ;
  wire \slv_reg22[31]_i_1_n_0 ;
  wire \slv_reg22[7]_i_1_n_0 ;
  wire [31:0]slv_reg23;
  wire \slv_reg23[15]_i_1_n_0 ;
  wire \slv_reg23[23]_i_1_n_0 ;
  wire \slv_reg23[31]_i_1_n_0 ;
  wire \slv_reg23[7]_i_1_n_0 ;
  wire [31:0]slv_reg24;
  wire \slv_reg24[15]_i_1_n_0 ;
  wire \slv_reg24[23]_i_1_n_0 ;
  wire \slv_reg24[31]_i_1_n_0 ;
  wire \slv_reg24[7]_i_1_n_0 ;
  wire [31:0]slv_reg25;
  wire \slv_reg25[15]_i_1_n_0 ;
  wire \slv_reg25[23]_i_1_n_0 ;
  wire \slv_reg25[31]_i_1_n_0 ;
  wire \slv_reg25[7]_i_1_n_0 ;
  wire [31:0]slv_reg26;
  wire \slv_reg26[15]_i_1_n_0 ;
  wire \slv_reg26[23]_i_1_n_0 ;
  wire \slv_reg26[31]_i_1_n_0 ;
  wire \slv_reg26[7]_i_1_n_0 ;
  wire [31:0]slv_reg27;
  wire \slv_reg27[15]_i_1_n_0 ;
  wire \slv_reg27[23]_i_1_n_0 ;
  wire \slv_reg27[31]_i_1_n_0 ;
  wire \slv_reg27[7]_i_1_n_0 ;
  wire [31:0]slv_reg28;
  wire \slv_reg28[15]_i_1_n_0 ;
  wire \slv_reg28[23]_i_1_n_0 ;
  wire \slv_reg28[31]_i_1_n_0 ;
  wire \slv_reg28[7]_i_1_n_0 ;
  wire [31:0]slv_reg29;
  wire \slv_reg29[15]_i_1_n_0 ;
  wire \slv_reg29[23]_i_1_n_0 ;
  wire \slv_reg29[31]_i_1_n_0 ;
  wire \slv_reg29[7]_i_1_n_0 ;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire [31:0]slv_reg30;
  wire \slv_reg30[15]_i_1_n_0 ;
  wire \slv_reg30[23]_i_1_n_0 ;
  wire \slv_reg30[31]_i_1_n_0 ;
  wire \slv_reg30[7]_i_1_n_0 ;
  wire [31:0]slv_reg31;
  wire \slv_reg31[15]_i_1_n_0 ;
  wire \slv_reg31[23]_i_1_n_0 ;
  wire \slv_reg31[31]_i_1_n_0 ;
  wire \slv_reg31[7]_i_1_n_0 ;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire [31:0]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [31:0]slv_reg5;
  wire \slv_reg5[15]_i_1_n_0 ;
  wire \slv_reg5[23]_i_1_n_0 ;
  wire \slv_reg5[31]_i_1_n_0 ;
  wire \slv_reg5[7]_i_1_n_0 ;
  wire [31:0]slv_reg6;
  wire \slv_reg6[15]_i_1_n_0 ;
  wire \slv_reg6[23]_i_1_n_0 ;
  wire \slv_reg6[31]_i_1_n_0 ;
  wire \slv_reg6[7]_i_1_n_0 ;
  wire [31:0]slv_reg7;
  wire \slv_reg7[15]_i_1_n_0 ;
  wire \slv_reg7[23]_i_1_n_0 ;
  wire \slv_reg7[31]_i_1_n_0 ;
  wire \slv_reg7[7]_i_1_n_0 ;
  wire [31:0]slv_reg8;
  wire \slv_reg8[15]_i_1_n_0 ;
  wire \slv_reg8[23]_i_1_n_0 ;
  wire \slv_reg8[31]_i_1_n_0 ;
  wire \slv_reg8[7]_i_1_n_0 ;
  wire [31:0]slv_reg9;
  wire \slv_reg9[15]_i_1_n_0 ;
  wire \slv_reg9[23]_i_1_n_0 ;
  wire \slv_reg9[31]_i_1_n_0 ;
  wire \slv_reg9[7]_i_1_n_0 ;
  wire [1:0]state;
  wire [2:0]sw;
  wire [3:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;

  (* ORIG_CELL_NAME = "axi_araddr_reg[2]" *) 
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .S(SR));
  (* ORIG_CELL_NAME = "axi_araddr_reg[2]" *) 
  FDSE \axi_araddr_reg[2]_rep 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[0]),
        .Q(\axi_araddr_reg[2]_rep_n_0 ),
        .S(SR));
  (* ORIG_CELL_NAME = "axi_araddr_reg[3]" *) 
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .S(SR));
  (* ORIG_CELL_NAME = "axi_araddr_reg[3]" *) 
  FDSE \axi_araddr_reg[3]_rep 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[1]),
        .Q(\axi_araddr_reg[3]_rep_n_0 ),
        .S(SR));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .S(SR));
  FDSE \axi_araddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[3]),
        .Q(sel0[3]),
        .S(SR));
  FDSE \axi_araddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[4]),
        .Q(sel0[4]),
        .S(SR));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_arready),
        .O(axi_arready_i_1_n_0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready_i_1_n_0),
        .Q(s00_axi_arready),
        .R(SR));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(SR));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(SR));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(SR));
  FDRE \axi_awaddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(SR));
  FDRE \axi_awaddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[4]),
        .Q(p_0_in[4]),
        .R(SR));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awready),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(s00_axi_awready),
        .R(SR));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_reg_0),
        .Q(s00_axi_bvalid),
        .R(SR));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata_reg[0]_i_2_n_0 ),
        .I1(\axi_rdata_reg[0]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[0]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[0]_i_5_n_0 ),
        .O(reg_data_out__0[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_10 
       (.I0(slv_reg11[0]),
        .I1(slv_reg10[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[0]),
        .O(\axi_rdata[0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_11 
       (.I0(slv_reg15[0]),
        .I1(slv_reg14[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[0]),
        .O(\axi_rdata[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_12 
       (.I0(slv_reg3[0]),
        .I1(slv_reg2[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[0]),
        .O(\axi_rdata[0]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_13 
       (.I0(slv_reg7[0]),
        .I1(slv_reg6[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[0]),
        .O(\axi_rdata[0]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_6 
       (.I0(slv_reg27[0]),
        .I1(slv_reg26[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[0]),
        .O(\axi_rdata[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_7 
       (.I0(slv_reg31[0]),
        .I1(slv_reg30[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[0]),
        .O(\axi_rdata[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_8 
       (.I0(slv_reg19[0]),
        .I1(slv_reg18[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[0]),
        .O(\axi_rdata[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_9 
       (.I0(slv_reg23[0]),
        .I1(slv_reg22[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[0]),
        .O(\axi_rdata[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata_reg[10]_i_2_n_0 ),
        .I1(\axi_rdata_reg[10]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[10]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[10]_i_5_n_0 ),
        .O(reg_data_out__0[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_10 
       (.I0(slv_reg11[10]),
        .I1(slv_reg10[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[10]),
        .O(\axi_rdata[10]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_11 
       (.I0(slv_reg15[10]),
        .I1(slv_reg14[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[10]),
        .O(\axi_rdata[10]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_12 
       (.I0(slv_reg3[10]),
        .I1(slv_reg2[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[10]),
        .O(\axi_rdata[10]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_13 
       (.I0(slv_reg7[10]),
        .I1(slv_reg6[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[10]),
        .O(\axi_rdata[10]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_6 
       (.I0(slv_reg27[10]),
        .I1(slv_reg26[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[10]),
        .O(\axi_rdata[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_7 
       (.I0(slv_reg31[10]),
        .I1(slv_reg30[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[10]),
        .O(\axi_rdata[10]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_8 
       (.I0(slv_reg19[10]),
        .I1(slv_reg18[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[10]),
        .O(\axi_rdata[10]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_9 
       (.I0(slv_reg23[10]),
        .I1(slv_reg22[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[10]),
        .O(\axi_rdata[10]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata_reg[11]_i_2_n_0 ),
        .I1(\axi_rdata_reg[11]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[11]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[11]_i_5_n_0 ),
        .O(reg_data_out__0[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_10 
       (.I0(slv_reg11[11]),
        .I1(slv_reg10[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[11]),
        .O(\axi_rdata[11]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_11 
       (.I0(slv_reg15[11]),
        .I1(slv_reg14[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[11]),
        .O(\axi_rdata[11]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_12 
       (.I0(slv_reg3[11]),
        .I1(slv_reg2[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[11]),
        .O(\axi_rdata[11]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_13 
       (.I0(slv_reg7[11]),
        .I1(slv_reg6[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[11]),
        .O(\axi_rdata[11]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_6 
       (.I0(slv_reg27[11]),
        .I1(slv_reg26[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[11]),
        .O(\axi_rdata[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_7 
       (.I0(slv_reg31[11]),
        .I1(slv_reg30[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[11]),
        .O(\axi_rdata[11]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_8 
       (.I0(slv_reg19[11]),
        .I1(slv_reg18[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[11]),
        .O(\axi_rdata[11]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_9 
       (.I0(slv_reg23[11]),
        .I1(slv_reg22[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[11]),
        .O(\axi_rdata[11]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata_reg[12]_i_2_n_0 ),
        .I1(\axi_rdata_reg[12]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[12]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[12]_i_5_n_0 ),
        .O(reg_data_out__0[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_10 
       (.I0(slv_reg11[12]),
        .I1(slv_reg10[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[12]),
        .O(\axi_rdata[12]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_11 
       (.I0(slv_reg15[12]),
        .I1(slv_reg14[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[12]),
        .O(\axi_rdata[12]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_12 
       (.I0(slv_reg3[12]),
        .I1(slv_reg2[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[12]),
        .O(\axi_rdata[12]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_13 
       (.I0(slv_reg7[12]),
        .I1(slv_reg6[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[12]),
        .O(\axi_rdata[12]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_6 
       (.I0(slv_reg27[12]),
        .I1(slv_reg26[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[12]),
        .O(\axi_rdata[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_7 
       (.I0(slv_reg31[12]),
        .I1(slv_reg30[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[12]),
        .O(\axi_rdata[12]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_8 
       (.I0(slv_reg19[12]),
        .I1(slv_reg18[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[12]),
        .O(\axi_rdata[12]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_9 
       (.I0(slv_reg23[12]),
        .I1(slv_reg22[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[12]),
        .O(\axi_rdata[12]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata_reg[13]_i_2_n_0 ),
        .I1(\axi_rdata_reg[13]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[13]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[13]_i_5_n_0 ),
        .O(reg_data_out__0[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_10 
       (.I0(slv_reg11[13]),
        .I1(slv_reg10[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[13]),
        .O(\axi_rdata[13]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_11 
       (.I0(slv_reg15[13]),
        .I1(slv_reg14[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[13]),
        .O(\axi_rdata[13]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_12 
       (.I0(slv_reg3[13]),
        .I1(slv_reg2[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[13]),
        .O(\axi_rdata[13]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_13 
       (.I0(slv_reg7[13]),
        .I1(slv_reg6[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[13]),
        .O(\axi_rdata[13]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_6 
       (.I0(slv_reg27[13]),
        .I1(slv_reg26[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[13]),
        .O(\axi_rdata[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_7 
       (.I0(slv_reg31[13]),
        .I1(slv_reg30[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[13]),
        .O(\axi_rdata[13]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_8 
       (.I0(slv_reg19[13]),
        .I1(slv_reg18[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[13]),
        .O(\axi_rdata[13]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_9 
       (.I0(slv_reg23[13]),
        .I1(slv_reg22[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[13]),
        .O(\axi_rdata[13]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata_reg[14]_i_2_n_0 ),
        .I1(\axi_rdata_reg[14]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[14]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[14]_i_5_n_0 ),
        .O(reg_data_out__0[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_10 
       (.I0(slv_reg11[14]),
        .I1(slv_reg10[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[14]),
        .O(\axi_rdata[14]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_11 
       (.I0(slv_reg15[14]),
        .I1(slv_reg14[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[14]),
        .O(\axi_rdata[14]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_12 
       (.I0(slv_reg3[14]),
        .I1(slv_reg2[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[14]),
        .O(\axi_rdata[14]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_13 
       (.I0(slv_reg7[14]),
        .I1(slv_reg6[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[14]),
        .O(\axi_rdata[14]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_6 
       (.I0(slv_reg27[14]),
        .I1(slv_reg26[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[14]),
        .O(\axi_rdata[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_7 
       (.I0(slv_reg31[14]),
        .I1(slv_reg30[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[14]),
        .O(\axi_rdata[14]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_8 
       (.I0(slv_reg19[14]),
        .I1(slv_reg18[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[14]),
        .O(\axi_rdata[14]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_9 
       (.I0(slv_reg23[14]),
        .I1(slv_reg22[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[14]),
        .O(\axi_rdata[14]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata_reg[15]_i_2_n_0 ),
        .I1(\axi_rdata_reg[15]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[15]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[15]_i_5_n_0 ),
        .O(reg_data_out__0[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_10 
       (.I0(slv_reg11[15]),
        .I1(slv_reg10[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[15]),
        .O(\axi_rdata[15]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_11 
       (.I0(slv_reg15[15]),
        .I1(slv_reg14[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[15]),
        .O(\axi_rdata[15]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_12 
       (.I0(slv_reg3[15]),
        .I1(slv_reg2[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[15]),
        .O(\axi_rdata[15]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_13 
       (.I0(slv_reg7[15]),
        .I1(slv_reg6[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[15]),
        .O(\axi_rdata[15]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_6 
       (.I0(slv_reg27[15]),
        .I1(slv_reg26[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[15]),
        .O(\axi_rdata[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_7 
       (.I0(slv_reg31[15]),
        .I1(slv_reg30[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[15]),
        .O(\axi_rdata[15]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_8 
       (.I0(slv_reg19[15]),
        .I1(slv_reg18[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[15]),
        .O(\axi_rdata[15]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_9 
       (.I0(slv_reg23[15]),
        .I1(slv_reg22[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[15]),
        .O(\axi_rdata[15]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata_reg[16]_i_2_n_0 ),
        .I1(\axi_rdata_reg[16]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[16]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[16]_i_5_n_0 ),
        .O(reg_data_out__0[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_10 
       (.I0(slv_reg11[16]),
        .I1(slv_reg10[16]),
        .I2(sel0[1]),
        .I3(slv_reg9[16]),
        .I4(sel0[0]),
        .I5(slv_reg8[16]),
        .O(\axi_rdata[16]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_11 
       (.I0(slv_reg15[16]),
        .I1(slv_reg14[16]),
        .I2(sel0[1]),
        .I3(slv_reg13[16]),
        .I4(sel0[0]),
        .I5(slv_reg12[16]),
        .O(\axi_rdata[16]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_12 
       (.I0(slv_reg3[16]),
        .I1(slv_reg2[16]),
        .I2(sel0[1]),
        .I3(slv_reg1[16]),
        .I4(sel0[0]),
        .I5(slv_reg0[16]),
        .O(\axi_rdata[16]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_13 
       (.I0(slv_reg7[16]),
        .I1(slv_reg6[16]),
        .I2(sel0[1]),
        .I3(slv_reg5[16]),
        .I4(sel0[0]),
        .I5(slv_reg4[16]),
        .O(\axi_rdata[16]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_6 
       (.I0(slv_reg27[16]),
        .I1(slv_reg26[16]),
        .I2(sel0[1]),
        .I3(slv_reg25[16]),
        .I4(sel0[0]),
        .I5(slv_reg24[16]),
        .O(\axi_rdata[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_7 
       (.I0(slv_reg31[16]),
        .I1(slv_reg30[16]),
        .I2(sel0[1]),
        .I3(slv_reg29[16]),
        .I4(sel0[0]),
        .I5(slv_reg28[16]),
        .O(\axi_rdata[16]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_8 
       (.I0(slv_reg19[16]),
        .I1(slv_reg18[16]),
        .I2(sel0[1]),
        .I3(slv_reg17[16]),
        .I4(sel0[0]),
        .I5(slv_reg16[16]),
        .O(\axi_rdata[16]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_9 
       (.I0(slv_reg23[16]),
        .I1(slv_reg22[16]),
        .I2(sel0[1]),
        .I3(slv_reg21[16]),
        .I4(sel0[0]),
        .I5(slv_reg20[16]),
        .O(\axi_rdata[16]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata_reg[17]_i_2_n_0 ),
        .I1(\axi_rdata_reg[17]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[17]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[17]_i_5_n_0 ),
        .O(reg_data_out__0[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_10 
       (.I0(slv_reg11[17]),
        .I1(slv_reg10[17]),
        .I2(sel0[1]),
        .I3(slv_reg9[17]),
        .I4(sel0[0]),
        .I5(slv_reg8[17]),
        .O(\axi_rdata[17]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_11 
       (.I0(slv_reg15[17]),
        .I1(slv_reg14[17]),
        .I2(sel0[1]),
        .I3(slv_reg13[17]),
        .I4(sel0[0]),
        .I5(slv_reg12[17]),
        .O(\axi_rdata[17]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_12 
       (.I0(slv_reg3[17]),
        .I1(slv_reg2[17]),
        .I2(sel0[1]),
        .I3(slv_reg1[17]),
        .I4(sel0[0]),
        .I5(slv_reg0[17]),
        .O(\axi_rdata[17]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_13 
       (.I0(slv_reg7[17]),
        .I1(slv_reg6[17]),
        .I2(sel0[1]),
        .I3(slv_reg5[17]),
        .I4(sel0[0]),
        .I5(slv_reg4[17]),
        .O(\axi_rdata[17]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_6 
       (.I0(slv_reg27[17]),
        .I1(slv_reg26[17]),
        .I2(sel0[1]),
        .I3(slv_reg25[17]),
        .I4(sel0[0]),
        .I5(slv_reg24[17]),
        .O(\axi_rdata[17]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_7 
       (.I0(slv_reg31[17]),
        .I1(slv_reg30[17]),
        .I2(sel0[1]),
        .I3(slv_reg29[17]),
        .I4(sel0[0]),
        .I5(slv_reg28[17]),
        .O(\axi_rdata[17]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_8 
       (.I0(slv_reg19[17]),
        .I1(slv_reg18[17]),
        .I2(sel0[1]),
        .I3(slv_reg17[17]),
        .I4(sel0[0]),
        .I5(slv_reg16[17]),
        .O(\axi_rdata[17]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_9 
       (.I0(slv_reg23[17]),
        .I1(slv_reg22[17]),
        .I2(sel0[1]),
        .I3(slv_reg21[17]),
        .I4(sel0[0]),
        .I5(slv_reg20[17]),
        .O(\axi_rdata[17]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata_reg[18]_i_2_n_0 ),
        .I1(\axi_rdata_reg[18]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[18]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[18]_i_5_n_0 ),
        .O(reg_data_out__0[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_10 
       (.I0(slv_reg11[18]),
        .I1(slv_reg10[18]),
        .I2(sel0[1]),
        .I3(slv_reg9[18]),
        .I4(sel0[0]),
        .I5(slv_reg8[18]),
        .O(\axi_rdata[18]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_11 
       (.I0(slv_reg15[18]),
        .I1(slv_reg14[18]),
        .I2(sel0[1]),
        .I3(slv_reg13[18]),
        .I4(sel0[0]),
        .I5(slv_reg12[18]),
        .O(\axi_rdata[18]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_12 
       (.I0(slv_reg3[18]),
        .I1(slv_reg2[18]),
        .I2(sel0[1]),
        .I3(slv_reg1[18]),
        .I4(sel0[0]),
        .I5(slv_reg0[18]),
        .O(\axi_rdata[18]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_13 
       (.I0(slv_reg7[18]),
        .I1(slv_reg6[18]),
        .I2(sel0[1]),
        .I3(slv_reg5[18]),
        .I4(sel0[0]),
        .I5(slv_reg4[18]),
        .O(\axi_rdata[18]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_6 
       (.I0(slv_reg27[18]),
        .I1(slv_reg26[18]),
        .I2(sel0[1]),
        .I3(slv_reg25[18]),
        .I4(sel0[0]),
        .I5(slv_reg24[18]),
        .O(\axi_rdata[18]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_7 
       (.I0(slv_reg31[18]),
        .I1(slv_reg30[18]),
        .I2(sel0[1]),
        .I3(slv_reg29[18]),
        .I4(sel0[0]),
        .I5(slv_reg28[18]),
        .O(\axi_rdata[18]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_8 
       (.I0(slv_reg19[18]),
        .I1(slv_reg18[18]),
        .I2(sel0[1]),
        .I3(slv_reg17[18]),
        .I4(sel0[0]),
        .I5(slv_reg16[18]),
        .O(\axi_rdata[18]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_9 
       (.I0(slv_reg23[18]),
        .I1(slv_reg22[18]),
        .I2(sel0[1]),
        .I3(slv_reg21[18]),
        .I4(sel0[0]),
        .I5(slv_reg20[18]),
        .O(\axi_rdata[18]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata_reg[19]_i_2_n_0 ),
        .I1(\axi_rdata_reg[19]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[19]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[19]_i_5_n_0 ),
        .O(reg_data_out__0[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_10 
       (.I0(slv_reg11[19]),
        .I1(slv_reg10[19]),
        .I2(sel0[1]),
        .I3(slv_reg9[19]),
        .I4(sel0[0]),
        .I5(slv_reg8[19]),
        .O(\axi_rdata[19]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_11 
       (.I0(slv_reg15[19]),
        .I1(slv_reg14[19]),
        .I2(sel0[1]),
        .I3(slv_reg13[19]),
        .I4(sel0[0]),
        .I5(slv_reg12[19]),
        .O(\axi_rdata[19]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_12 
       (.I0(slv_reg3[19]),
        .I1(slv_reg2[19]),
        .I2(sel0[1]),
        .I3(slv_reg1[19]),
        .I4(sel0[0]),
        .I5(slv_reg0[19]),
        .O(\axi_rdata[19]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_13 
       (.I0(slv_reg7[19]),
        .I1(slv_reg6[19]),
        .I2(sel0[1]),
        .I3(slv_reg5[19]),
        .I4(sel0[0]),
        .I5(slv_reg4[19]),
        .O(\axi_rdata[19]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_6 
       (.I0(slv_reg27[19]),
        .I1(slv_reg26[19]),
        .I2(sel0[1]),
        .I3(slv_reg25[19]),
        .I4(sel0[0]),
        .I5(slv_reg24[19]),
        .O(\axi_rdata[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_7 
       (.I0(slv_reg31[19]),
        .I1(slv_reg30[19]),
        .I2(sel0[1]),
        .I3(slv_reg29[19]),
        .I4(sel0[0]),
        .I5(slv_reg28[19]),
        .O(\axi_rdata[19]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_8 
       (.I0(slv_reg19[19]),
        .I1(slv_reg18[19]),
        .I2(sel0[1]),
        .I3(slv_reg17[19]),
        .I4(sel0[0]),
        .I5(slv_reg16[19]),
        .O(\axi_rdata[19]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_9 
       (.I0(slv_reg23[19]),
        .I1(slv_reg22[19]),
        .I2(sel0[1]),
        .I3(slv_reg21[19]),
        .I4(sel0[0]),
        .I5(slv_reg20[19]),
        .O(\axi_rdata[19]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata_reg[1]_i_2_n_0 ),
        .I1(\axi_rdata_reg[1]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[1]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[1]_i_5_n_0 ),
        .O(reg_data_out__0[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_10 
       (.I0(slv_reg11[1]),
        .I1(slv_reg10[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[1]),
        .O(\axi_rdata[1]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_11 
       (.I0(slv_reg15[1]),
        .I1(slv_reg14[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[1]),
        .O(\axi_rdata[1]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_12 
       (.I0(slv_reg3[1]),
        .I1(slv_reg2[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[1]),
        .O(\axi_rdata[1]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_13 
       (.I0(slv_reg7[1]),
        .I1(slv_reg6[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[1]),
        .O(\axi_rdata[1]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_6 
       (.I0(slv_reg27[1]),
        .I1(slv_reg26[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[1]),
        .O(\axi_rdata[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_7 
       (.I0(slv_reg31[1]),
        .I1(slv_reg30[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[1]),
        .O(\axi_rdata[1]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_8 
       (.I0(slv_reg19[1]),
        .I1(slv_reg18[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[1]),
        .O(\axi_rdata[1]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_9 
       (.I0(slv_reg23[1]),
        .I1(slv_reg22[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[1]),
        .O(\axi_rdata[1]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata_reg[20]_i_2_n_0 ),
        .I1(\axi_rdata_reg[20]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[20]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[20]_i_5_n_0 ),
        .O(reg_data_out__0[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_10 
       (.I0(slv_reg11[20]),
        .I1(slv_reg10[20]),
        .I2(sel0[1]),
        .I3(slv_reg9[20]),
        .I4(sel0[0]),
        .I5(slv_reg8[20]),
        .O(\axi_rdata[20]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_11 
       (.I0(slv_reg15[20]),
        .I1(slv_reg14[20]),
        .I2(sel0[1]),
        .I3(slv_reg13[20]),
        .I4(sel0[0]),
        .I5(slv_reg12[20]),
        .O(\axi_rdata[20]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_12 
       (.I0(slv_reg3[20]),
        .I1(slv_reg2[20]),
        .I2(sel0[1]),
        .I3(slv_reg1[20]),
        .I4(sel0[0]),
        .I5(slv_reg0[20]),
        .O(\axi_rdata[20]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_13 
       (.I0(slv_reg7[20]),
        .I1(slv_reg6[20]),
        .I2(sel0[1]),
        .I3(slv_reg5[20]),
        .I4(sel0[0]),
        .I5(slv_reg4[20]),
        .O(\axi_rdata[20]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_6 
       (.I0(slv_reg27[20]),
        .I1(slv_reg26[20]),
        .I2(sel0[1]),
        .I3(slv_reg25[20]),
        .I4(sel0[0]),
        .I5(slv_reg24[20]),
        .O(\axi_rdata[20]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_7 
       (.I0(slv_reg31[20]),
        .I1(slv_reg30[20]),
        .I2(sel0[1]),
        .I3(slv_reg29[20]),
        .I4(sel0[0]),
        .I5(slv_reg28[20]),
        .O(\axi_rdata[20]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_8 
       (.I0(slv_reg19[20]),
        .I1(slv_reg18[20]),
        .I2(sel0[1]),
        .I3(slv_reg17[20]),
        .I4(sel0[0]),
        .I5(slv_reg16[20]),
        .O(\axi_rdata[20]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_9 
       (.I0(slv_reg23[20]),
        .I1(slv_reg22[20]),
        .I2(sel0[1]),
        .I3(slv_reg21[20]),
        .I4(sel0[0]),
        .I5(slv_reg20[20]),
        .O(\axi_rdata[20]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata_reg[21]_i_2_n_0 ),
        .I1(\axi_rdata_reg[21]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[21]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[21]_i_5_n_0 ),
        .O(reg_data_out__0[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_10 
       (.I0(slv_reg11[21]),
        .I1(slv_reg10[21]),
        .I2(sel0[1]),
        .I3(slv_reg9[21]),
        .I4(sel0[0]),
        .I5(slv_reg8[21]),
        .O(\axi_rdata[21]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_11 
       (.I0(slv_reg15[21]),
        .I1(slv_reg14[21]),
        .I2(sel0[1]),
        .I3(slv_reg13[21]),
        .I4(sel0[0]),
        .I5(slv_reg12[21]),
        .O(\axi_rdata[21]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_12 
       (.I0(slv_reg3[21]),
        .I1(slv_reg2[21]),
        .I2(sel0[1]),
        .I3(slv_reg1[21]),
        .I4(sel0[0]),
        .I5(slv_reg0[21]),
        .O(\axi_rdata[21]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_13 
       (.I0(slv_reg7[21]),
        .I1(slv_reg6[21]),
        .I2(sel0[1]),
        .I3(slv_reg5[21]),
        .I4(sel0[0]),
        .I5(slv_reg4[21]),
        .O(\axi_rdata[21]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_6 
       (.I0(slv_reg27[21]),
        .I1(slv_reg26[21]),
        .I2(sel0[1]),
        .I3(slv_reg25[21]),
        .I4(sel0[0]),
        .I5(slv_reg24[21]),
        .O(\axi_rdata[21]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_7 
       (.I0(slv_reg31[21]),
        .I1(slv_reg30[21]),
        .I2(sel0[1]),
        .I3(slv_reg29[21]),
        .I4(sel0[0]),
        .I5(slv_reg28[21]),
        .O(\axi_rdata[21]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_8 
       (.I0(slv_reg19[21]),
        .I1(slv_reg18[21]),
        .I2(sel0[1]),
        .I3(slv_reg17[21]),
        .I4(sel0[0]),
        .I5(slv_reg16[21]),
        .O(\axi_rdata[21]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_9 
       (.I0(slv_reg23[21]),
        .I1(slv_reg22[21]),
        .I2(sel0[1]),
        .I3(slv_reg21[21]),
        .I4(sel0[0]),
        .I5(slv_reg20[21]),
        .O(\axi_rdata[21]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata_reg[22]_i_2_n_0 ),
        .I1(\axi_rdata_reg[22]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[22]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[22]_i_5_n_0 ),
        .O(reg_data_out__0[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_10 
       (.I0(slv_reg11[22]),
        .I1(slv_reg10[22]),
        .I2(sel0[1]),
        .I3(slv_reg9[22]),
        .I4(sel0[0]),
        .I5(slv_reg8[22]),
        .O(\axi_rdata[22]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_11 
       (.I0(slv_reg15[22]),
        .I1(slv_reg14[22]),
        .I2(sel0[1]),
        .I3(slv_reg13[22]),
        .I4(sel0[0]),
        .I5(slv_reg12[22]),
        .O(\axi_rdata[22]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_12 
       (.I0(slv_reg3[22]),
        .I1(slv_reg2[22]),
        .I2(sel0[1]),
        .I3(slv_reg1[22]),
        .I4(sel0[0]),
        .I5(slv_reg0[22]),
        .O(\axi_rdata[22]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_13 
       (.I0(slv_reg7[22]),
        .I1(slv_reg6[22]),
        .I2(sel0[1]),
        .I3(slv_reg5[22]),
        .I4(sel0[0]),
        .I5(slv_reg4[22]),
        .O(\axi_rdata[22]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_6 
       (.I0(slv_reg27[22]),
        .I1(slv_reg26[22]),
        .I2(sel0[1]),
        .I3(slv_reg25[22]),
        .I4(sel0[0]),
        .I5(slv_reg24[22]),
        .O(\axi_rdata[22]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_7 
       (.I0(slv_reg31[22]),
        .I1(slv_reg30[22]),
        .I2(sel0[1]),
        .I3(slv_reg29[22]),
        .I4(sel0[0]),
        .I5(slv_reg28[22]),
        .O(\axi_rdata[22]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_8 
       (.I0(slv_reg19[22]),
        .I1(slv_reg18[22]),
        .I2(sel0[1]),
        .I3(slv_reg17[22]),
        .I4(sel0[0]),
        .I5(slv_reg16[22]),
        .O(\axi_rdata[22]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_9 
       (.I0(slv_reg23[22]),
        .I1(slv_reg22[22]),
        .I2(sel0[1]),
        .I3(slv_reg21[22]),
        .I4(sel0[0]),
        .I5(slv_reg20[22]),
        .O(\axi_rdata[22]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata_reg[23]_i_2_n_0 ),
        .I1(\axi_rdata_reg[23]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[23]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[23]_i_5_n_0 ),
        .O(reg_data_out__0[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_10 
       (.I0(slv_reg11[23]),
        .I1(slv_reg10[23]),
        .I2(sel0[1]),
        .I3(slv_reg9[23]),
        .I4(sel0[0]),
        .I5(slv_reg8[23]),
        .O(\axi_rdata[23]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_11 
       (.I0(slv_reg15[23]),
        .I1(slv_reg14[23]),
        .I2(sel0[1]),
        .I3(slv_reg13[23]),
        .I4(sel0[0]),
        .I5(slv_reg12[23]),
        .O(\axi_rdata[23]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_12 
       (.I0(slv_reg3[23]),
        .I1(slv_reg2[23]),
        .I2(sel0[1]),
        .I3(slv_reg1[23]),
        .I4(sel0[0]),
        .I5(slv_reg0[23]),
        .O(\axi_rdata[23]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_13 
       (.I0(slv_reg7[23]),
        .I1(slv_reg6[23]),
        .I2(sel0[1]),
        .I3(slv_reg5[23]),
        .I4(sel0[0]),
        .I5(slv_reg4[23]),
        .O(\axi_rdata[23]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_6 
       (.I0(slv_reg27[23]),
        .I1(slv_reg26[23]),
        .I2(sel0[1]),
        .I3(slv_reg25[23]),
        .I4(sel0[0]),
        .I5(slv_reg24[23]),
        .O(\axi_rdata[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_7 
       (.I0(slv_reg31[23]),
        .I1(slv_reg30[23]),
        .I2(sel0[1]),
        .I3(slv_reg29[23]),
        .I4(sel0[0]),
        .I5(slv_reg28[23]),
        .O(\axi_rdata[23]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_8 
       (.I0(slv_reg19[23]),
        .I1(slv_reg18[23]),
        .I2(sel0[1]),
        .I3(slv_reg17[23]),
        .I4(sel0[0]),
        .I5(slv_reg16[23]),
        .O(\axi_rdata[23]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_9 
       (.I0(slv_reg23[23]),
        .I1(slv_reg22[23]),
        .I2(sel0[1]),
        .I3(slv_reg21[23]),
        .I4(sel0[0]),
        .I5(slv_reg20[23]),
        .O(\axi_rdata[23]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata_reg[24]_i_2_n_0 ),
        .I1(\axi_rdata_reg[24]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[24]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[24]_i_5_n_0 ),
        .O(reg_data_out__0[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_10 
       (.I0(slv_reg11[24]),
        .I1(slv_reg10[24]),
        .I2(sel0[1]),
        .I3(slv_reg9[24]),
        .I4(sel0[0]),
        .I5(slv_reg8[24]),
        .O(\axi_rdata[24]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_11 
       (.I0(slv_reg15[24]),
        .I1(slv_reg14[24]),
        .I2(sel0[1]),
        .I3(slv_reg13[24]),
        .I4(sel0[0]),
        .I5(slv_reg12[24]),
        .O(\axi_rdata[24]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_12 
       (.I0(slv_reg3[24]),
        .I1(slv_reg2[24]),
        .I2(sel0[1]),
        .I3(slv_reg1[24]),
        .I4(sel0[0]),
        .I5(slv_reg0[24]),
        .O(\axi_rdata[24]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_13 
       (.I0(slv_reg7[24]),
        .I1(slv_reg6[24]),
        .I2(sel0[1]),
        .I3(slv_reg5[24]),
        .I4(sel0[0]),
        .I5(slv_reg4[24]),
        .O(\axi_rdata[24]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_6 
       (.I0(slv_reg27[24]),
        .I1(slv_reg26[24]),
        .I2(sel0[1]),
        .I3(slv_reg25[24]),
        .I4(sel0[0]),
        .I5(slv_reg24[24]),
        .O(\axi_rdata[24]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_7 
       (.I0(slv_reg31[24]),
        .I1(slv_reg30[24]),
        .I2(sel0[1]),
        .I3(slv_reg29[24]),
        .I4(sel0[0]),
        .I5(slv_reg28[24]),
        .O(\axi_rdata[24]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_8 
       (.I0(slv_reg19[24]),
        .I1(slv_reg18[24]),
        .I2(sel0[1]),
        .I3(slv_reg17[24]),
        .I4(sel0[0]),
        .I5(slv_reg16[24]),
        .O(\axi_rdata[24]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_9 
       (.I0(slv_reg23[24]),
        .I1(slv_reg22[24]),
        .I2(sel0[1]),
        .I3(slv_reg21[24]),
        .I4(sel0[0]),
        .I5(slv_reg20[24]),
        .O(\axi_rdata[24]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata_reg[25]_i_2_n_0 ),
        .I1(\axi_rdata_reg[25]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[25]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[25]_i_5_n_0 ),
        .O(reg_data_out__0[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_10 
       (.I0(slv_reg11[25]),
        .I1(slv_reg10[25]),
        .I2(sel0[1]),
        .I3(slv_reg9[25]),
        .I4(sel0[0]),
        .I5(slv_reg8[25]),
        .O(\axi_rdata[25]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_11 
       (.I0(slv_reg15[25]),
        .I1(slv_reg14[25]),
        .I2(sel0[1]),
        .I3(slv_reg13[25]),
        .I4(sel0[0]),
        .I5(slv_reg12[25]),
        .O(\axi_rdata[25]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_12 
       (.I0(slv_reg3[25]),
        .I1(slv_reg2[25]),
        .I2(sel0[1]),
        .I3(slv_reg1[25]),
        .I4(sel0[0]),
        .I5(slv_reg0[25]),
        .O(\axi_rdata[25]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_13 
       (.I0(slv_reg7[25]),
        .I1(slv_reg6[25]),
        .I2(sel0[1]),
        .I3(slv_reg5[25]),
        .I4(sel0[0]),
        .I5(slv_reg4[25]),
        .O(\axi_rdata[25]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_6 
       (.I0(slv_reg27[25]),
        .I1(slv_reg26[25]),
        .I2(sel0[1]),
        .I3(slv_reg25[25]),
        .I4(sel0[0]),
        .I5(slv_reg24[25]),
        .O(\axi_rdata[25]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_7 
       (.I0(slv_reg31[25]),
        .I1(slv_reg30[25]),
        .I2(sel0[1]),
        .I3(slv_reg29[25]),
        .I4(sel0[0]),
        .I5(slv_reg28[25]),
        .O(\axi_rdata[25]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_8 
       (.I0(slv_reg19[25]),
        .I1(slv_reg18[25]),
        .I2(sel0[1]),
        .I3(slv_reg17[25]),
        .I4(sel0[0]),
        .I5(slv_reg16[25]),
        .O(\axi_rdata[25]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_9 
       (.I0(slv_reg23[25]),
        .I1(slv_reg22[25]),
        .I2(sel0[1]),
        .I3(slv_reg21[25]),
        .I4(sel0[0]),
        .I5(slv_reg20[25]),
        .O(\axi_rdata[25]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata_reg[26]_i_2_n_0 ),
        .I1(\axi_rdata_reg[26]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[26]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[26]_i_5_n_0 ),
        .O(reg_data_out__0[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_10 
       (.I0(slv_reg11[26]),
        .I1(slv_reg10[26]),
        .I2(sel0[1]),
        .I3(slv_reg9[26]),
        .I4(sel0[0]),
        .I5(slv_reg8[26]),
        .O(\axi_rdata[26]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_11 
       (.I0(slv_reg15[26]),
        .I1(slv_reg14[26]),
        .I2(sel0[1]),
        .I3(slv_reg13[26]),
        .I4(sel0[0]),
        .I5(slv_reg12[26]),
        .O(\axi_rdata[26]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_12 
       (.I0(slv_reg3[26]),
        .I1(slv_reg2[26]),
        .I2(sel0[1]),
        .I3(slv_reg1[26]),
        .I4(sel0[0]),
        .I5(slv_reg0[26]),
        .O(\axi_rdata[26]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_13 
       (.I0(slv_reg7[26]),
        .I1(slv_reg6[26]),
        .I2(sel0[1]),
        .I3(slv_reg5[26]),
        .I4(sel0[0]),
        .I5(slv_reg4[26]),
        .O(\axi_rdata[26]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_6 
       (.I0(slv_reg27[26]),
        .I1(slv_reg26[26]),
        .I2(sel0[1]),
        .I3(slv_reg25[26]),
        .I4(sel0[0]),
        .I5(slv_reg24[26]),
        .O(\axi_rdata[26]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_7 
       (.I0(slv_reg31[26]),
        .I1(slv_reg30[26]),
        .I2(sel0[1]),
        .I3(slv_reg29[26]),
        .I4(sel0[0]),
        .I5(slv_reg28[26]),
        .O(\axi_rdata[26]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_8 
       (.I0(slv_reg19[26]),
        .I1(slv_reg18[26]),
        .I2(sel0[1]),
        .I3(slv_reg17[26]),
        .I4(sel0[0]),
        .I5(slv_reg16[26]),
        .O(\axi_rdata[26]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_9 
       (.I0(slv_reg23[26]),
        .I1(slv_reg22[26]),
        .I2(sel0[1]),
        .I3(slv_reg21[26]),
        .I4(sel0[0]),
        .I5(slv_reg20[26]),
        .O(\axi_rdata[26]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata_reg[27]_i_2_n_0 ),
        .I1(\axi_rdata_reg[27]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[27]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[27]_i_5_n_0 ),
        .O(reg_data_out__0[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_10 
       (.I0(slv_reg11[27]),
        .I1(slv_reg10[27]),
        .I2(sel0[1]),
        .I3(slv_reg9[27]),
        .I4(sel0[0]),
        .I5(slv_reg8[27]),
        .O(\axi_rdata[27]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_11 
       (.I0(slv_reg15[27]),
        .I1(slv_reg14[27]),
        .I2(sel0[1]),
        .I3(slv_reg13[27]),
        .I4(sel0[0]),
        .I5(slv_reg12[27]),
        .O(\axi_rdata[27]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_12 
       (.I0(slv_reg3[27]),
        .I1(slv_reg2[27]),
        .I2(sel0[1]),
        .I3(slv_reg1[27]),
        .I4(sel0[0]),
        .I5(slv_reg0[27]),
        .O(\axi_rdata[27]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_13 
       (.I0(slv_reg7[27]),
        .I1(slv_reg6[27]),
        .I2(sel0[1]),
        .I3(slv_reg5[27]),
        .I4(sel0[0]),
        .I5(slv_reg4[27]),
        .O(\axi_rdata[27]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_6 
       (.I0(slv_reg27[27]),
        .I1(slv_reg26[27]),
        .I2(sel0[1]),
        .I3(slv_reg25[27]),
        .I4(sel0[0]),
        .I5(slv_reg24[27]),
        .O(\axi_rdata[27]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_7 
       (.I0(slv_reg31[27]),
        .I1(slv_reg30[27]),
        .I2(sel0[1]),
        .I3(slv_reg29[27]),
        .I4(sel0[0]),
        .I5(slv_reg28[27]),
        .O(\axi_rdata[27]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_8 
       (.I0(slv_reg19[27]),
        .I1(slv_reg18[27]),
        .I2(sel0[1]),
        .I3(slv_reg17[27]),
        .I4(sel0[0]),
        .I5(slv_reg16[27]),
        .O(\axi_rdata[27]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_9 
       (.I0(slv_reg23[27]),
        .I1(slv_reg22[27]),
        .I2(sel0[1]),
        .I3(slv_reg21[27]),
        .I4(sel0[0]),
        .I5(slv_reg20[27]),
        .O(\axi_rdata[27]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata_reg[28]_i_2_n_0 ),
        .I1(\axi_rdata_reg[28]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[28]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[28]_i_5_n_0 ),
        .O(reg_data_out__0[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_10 
       (.I0(slv_reg11[28]),
        .I1(slv_reg10[28]),
        .I2(sel0[1]),
        .I3(slv_reg9[28]),
        .I4(sel0[0]),
        .I5(slv_reg8[28]),
        .O(\axi_rdata[28]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_11 
       (.I0(slv_reg15[28]),
        .I1(slv_reg14[28]),
        .I2(sel0[1]),
        .I3(slv_reg13[28]),
        .I4(sel0[0]),
        .I5(slv_reg12[28]),
        .O(\axi_rdata[28]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_12 
       (.I0(slv_reg3[28]),
        .I1(slv_reg2[28]),
        .I2(sel0[1]),
        .I3(slv_reg1[28]),
        .I4(sel0[0]),
        .I5(slv_reg0[28]),
        .O(\axi_rdata[28]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_13 
       (.I0(slv_reg7[28]),
        .I1(slv_reg6[28]),
        .I2(sel0[1]),
        .I3(slv_reg5[28]),
        .I4(sel0[0]),
        .I5(slv_reg4[28]),
        .O(\axi_rdata[28]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_6 
       (.I0(slv_reg27[28]),
        .I1(slv_reg26[28]),
        .I2(sel0[1]),
        .I3(slv_reg25[28]),
        .I4(sel0[0]),
        .I5(slv_reg24[28]),
        .O(\axi_rdata[28]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_7 
       (.I0(slv_reg31[28]),
        .I1(slv_reg30[28]),
        .I2(sel0[1]),
        .I3(slv_reg29[28]),
        .I4(sel0[0]),
        .I5(slv_reg28[28]),
        .O(\axi_rdata[28]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_8 
       (.I0(slv_reg19[28]),
        .I1(slv_reg18[28]),
        .I2(sel0[1]),
        .I3(slv_reg17[28]),
        .I4(sel0[0]),
        .I5(slv_reg16[28]),
        .O(\axi_rdata[28]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_9 
       (.I0(slv_reg23[28]),
        .I1(slv_reg22[28]),
        .I2(sel0[1]),
        .I3(slv_reg21[28]),
        .I4(sel0[0]),
        .I5(slv_reg20[28]),
        .O(\axi_rdata[28]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata_reg[29]_i_2_n_0 ),
        .I1(\axi_rdata_reg[29]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[29]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[29]_i_5_n_0 ),
        .O(reg_data_out__0[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_10 
       (.I0(slv_reg11[29]),
        .I1(slv_reg10[29]),
        .I2(sel0[1]),
        .I3(slv_reg9[29]),
        .I4(sel0[0]),
        .I5(slv_reg8[29]),
        .O(\axi_rdata[29]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_11 
       (.I0(slv_reg15[29]),
        .I1(slv_reg14[29]),
        .I2(sel0[1]),
        .I3(slv_reg13[29]),
        .I4(sel0[0]),
        .I5(slv_reg12[29]),
        .O(\axi_rdata[29]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_12 
       (.I0(slv_reg3[29]),
        .I1(slv_reg2[29]),
        .I2(sel0[1]),
        .I3(slv_reg1[29]),
        .I4(sel0[0]),
        .I5(slv_reg0[29]),
        .O(\axi_rdata[29]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_13 
       (.I0(slv_reg7[29]),
        .I1(slv_reg6[29]),
        .I2(sel0[1]),
        .I3(slv_reg5[29]),
        .I4(sel0[0]),
        .I5(slv_reg4[29]),
        .O(\axi_rdata[29]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_6 
       (.I0(slv_reg27[29]),
        .I1(slv_reg26[29]),
        .I2(sel0[1]),
        .I3(slv_reg25[29]),
        .I4(sel0[0]),
        .I5(slv_reg24[29]),
        .O(\axi_rdata[29]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_7 
       (.I0(slv_reg31[29]),
        .I1(slv_reg30[29]),
        .I2(sel0[1]),
        .I3(slv_reg29[29]),
        .I4(sel0[0]),
        .I5(slv_reg28[29]),
        .O(\axi_rdata[29]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_8 
       (.I0(slv_reg19[29]),
        .I1(slv_reg18[29]),
        .I2(sel0[1]),
        .I3(slv_reg17[29]),
        .I4(sel0[0]),
        .I5(slv_reg16[29]),
        .O(\axi_rdata[29]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_9 
       (.I0(slv_reg23[29]),
        .I1(slv_reg22[29]),
        .I2(sel0[1]),
        .I3(slv_reg21[29]),
        .I4(sel0[0]),
        .I5(slv_reg20[29]),
        .O(\axi_rdata[29]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata_reg[2]_i_2_n_0 ),
        .I1(\axi_rdata_reg[2]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[2]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[2]_i_5_n_0 ),
        .O(reg_data_out__0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_10 
       (.I0(slv_reg11[2]),
        .I1(slv_reg10[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[2]),
        .O(\axi_rdata[2]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_11 
       (.I0(slv_reg15[2]),
        .I1(slv_reg14[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[2]),
        .O(\axi_rdata[2]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_12 
       (.I0(slv_reg3[2]),
        .I1(slv_reg2[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[2]),
        .O(\axi_rdata[2]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_13 
       (.I0(slv_reg7[2]),
        .I1(slv_reg6[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[2]),
        .O(\axi_rdata[2]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_6 
       (.I0(slv_reg27[2]),
        .I1(slv_reg26[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[2]),
        .O(\axi_rdata[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_7 
       (.I0(slv_reg31[2]),
        .I1(slv_reg30[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[2]),
        .O(\axi_rdata[2]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_8 
       (.I0(slv_reg19[2]),
        .I1(slv_reg18[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[2]),
        .O(\axi_rdata[2]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_9 
       (.I0(slv_reg23[2]),
        .I1(slv_reg22[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[2]),
        .O(\axi_rdata[2]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata_reg[30]_i_2_n_0 ),
        .I1(\axi_rdata_reg[30]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[30]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[30]_i_5_n_0 ),
        .O(reg_data_out__0[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_10 
       (.I0(slv_reg11[30]),
        .I1(slv_reg10[30]),
        .I2(sel0[1]),
        .I3(slv_reg9[30]),
        .I4(sel0[0]),
        .I5(slv_reg8[30]),
        .O(\axi_rdata[30]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_11 
       (.I0(slv_reg15[30]),
        .I1(slv_reg14[30]),
        .I2(sel0[1]),
        .I3(slv_reg13[30]),
        .I4(sel0[0]),
        .I5(slv_reg12[30]),
        .O(\axi_rdata[30]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_12 
       (.I0(slv_reg3[30]),
        .I1(slv_reg2[30]),
        .I2(sel0[1]),
        .I3(slv_reg1[30]),
        .I4(sel0[0]),
        .I5(slv_reg0[30]),
        .O(\axi_rdata[30]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_13 
       (.I0(slv_reg7[30]),
        .I1(slv_reg6[30]),
        .I2(sel0[1]),
        .I3(slv_reg5[30]),
        .I4(sel0[0]),
        .I5(slv_reg4[30]),
        .O(\axi_rdata[30]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_6 
       (.I0(slv_reg27[30]),
        .I1(slv_reg26[30]),
        .I2(sel0[1]),
        .I3(slv_reg25[30]),
        .I4(sel0[0]),
        .I5(slv_reg24[30]),
        .O(\axi_rdata[30]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_7 
       (.I0(slv_reg31[30]),
        .I1(slv_reg30[30]),
        .I2(sel0[1]),
        .I3(slv_reg29[30]),
        .I4(sel0[0]),
        .I5(slv_reg28[30]),
        .O(\axi_rdata[30]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_8 
       (.I0(slv_reg19[30]),
        .I1(slv_reg18[30]),
        .I2(sel0[1]),
        .I3(slv_reg17[30]),
        .I4(sel0[0]),
        .I5(slv_reg16[30]),
        .O(\axi_rdata[30]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_9 
       (.I0(slv_reg23[30]),
        .I1(slv_reg22[30]),
        .I2(sel0[1]),
        .I3(slv_reg21[30]),
        .I4(sel0[0]),
        .I5(slv_reg20[30]),
        .O(\axi_rdata[30]_i_9_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(s00_axi_arready),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(\axi_rdata[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_10 
       (.I0(slv_reg23[31]),
        .I1(slv_reg22[31]),
        .I2(sel0[1]),
        .I3(slv_reg21[31]),
        .I4(sel0[0]),
        .I5(slv_reg20[31]),
        .O(\axi_rdata[31]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_11 
       (.I0(slv_reg11[31]),
        .I1(slv_reg10[31]),
        .I2(sel0[1]),
        .I3(slv_reg9[31]),
        .I4(sel0[0]),
        .I5(slv_reg8[31]),
        .O(\axi_rdata[31]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_12 
       (.I0(slv_reg15[31]),
        .I1(slv_reg14[31]),
        .I2(sel0[1]),
        .I3(slv_reg13[31]),
        .I4(sel0[0]),
        .I5(slv_reg12[31]),
        .O(\axi_rdata[31]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_13 
       (.I0(slv_reg3[31]),
        .I1(slv_reg2[31]),
        .I2(sel0[1]),
        .I3(slv_reg1[31]),
        .I4(sel0[0]),
        .I5(slv_reg0[31]),
        .O(\axi_rdata[31]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_14 
       (.I0(slv_reg7[31]),
        .I1(slv_reg6[31]),
        .I2(sel0[1]),
        .I3(slv_reg5[31]),
        .I4(sel0[0]),
        .I5(slv_reg4[31]),
        .O(\axi_rdata[31]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_2 
       (.I0(\axi_rdata_reg[31]_i_3_n_0 ),
        .I1(\axi_rdata_reg[31]_i_4_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[31]_i_5_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[31]_i_6_n_0 ),
        .O(reg_data_out__0[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_7 
       (.I0(slv_reg27[31]),
        .I1(slv_reg26[31]),
        .I2(sel0[1]),
        .I3(slv_reg25[31]),
        .I4(sel0[0]),
        .I5(slv_reg24[31]),
        .O(\axi_rdata[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_8 
       (.I0(slv_reg31[31]),
        .I1(slv_reg30[31]),
        .I2(sel0[1]),
        .I3(slv_reg29[31]),
        .I4(sel0[0]),
        .I5(slv_reg28[31]),
        .O(\axi_rdata[31]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_9 
       (.I0(slv_reg19[31]),
        .I1(slv_reg18[31]),
        .I2(sel0[1]),
        .I3(slv_reg17[31]),
        .I4(sel0[0]),
        .I5(slv_reg16[31]),
        .O(\axi_rdata[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata_reg[3]_i_2_n_0 ),
        .I1(\axi_rdata_reg[3]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[3]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[3]_i_5_n_0 ),
        .O(reg_data_out__0[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_10 
       (.I0(slv_reg11[3]),
        .I1(slv_reg10[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[3]),
        .O(\axi_rdata[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_11 
       (.I0(slv_reg15[3]),
        .I1(slv_reg14[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[3]),
        .O(\axi_rdata[3]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_12 
       (.I0(slv_reg3[3]),
        .I1(slv_reg2[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[3]),
        .O(\axi_rdata[3]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_13 
       (.I0(slv_reg7[3]),
        .I1(slv_reg6[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[3]),
        .O(\axi_rdata[3]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_6 
       (.I0(slv_reg27[3]),
        .I1(slv_reg26[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[3]),
        .O(\axi_rdata[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_7 
       (.I0(slv_reg31[3]),
        .I1(slv_reg30[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[3]),
        .O(\axi_rdata[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_8 
       (.I0(slv_reg19[3]),
        .I1(slv_reg18[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[3]),
        .O(\axi_rdata[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_9 
       (.I0(slv_reg23[3]),
        .I1(slv_reg22[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[3]),
        .O(\axi_rdata[3]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata_reg[4]_i_2_n_0 ),
        .I1(\axi_rdata_reg[4]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[4]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[4]_i_5_n_0 ),
        .O(reg_data_out__0[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_10 
       (.I0(slv_reg11[4]),
        .I1(slv_reg10[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[4]),
        .O(\axi_rdata[4]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_11 
       (.I0(slv_reg15[4]),
        .I1(slv_reg14[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[4]),
        .O(\axi_rdata[4]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_12 
       (.I0(slv_reg3[4]),
        .I1(slv_reg2[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[4]),
        .O(\axi_rdata[4]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_13 
       (.I0(slv_reg7[4]),
        .I1(slv_reg6[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[4]),
        .O(\axi_rdata[4]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_6 
       (.I0(slv_reg27[4]),
        .I1(slv_reg26[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[4]),
        .O(\axi_rdata[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_7 
       (.I0(slv_reg31[4]),
        .I1(slv_reg30[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[4]),
        .O(\axi_rdata[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_8 
       (.I0(slv_reg19[4]),
        .I1(slv_reg18[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[4]),
        .O(\axi_rdata[4]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_9 
       (.I0(slv_reg23[4]),
        .I1(slv_reg22[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[4]),
        .O(\axi_rdata[4]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata_reg[5]_i_2_n_0 ),
        .I1(\axi_rdata_reg[5]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[5]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[5]_i_5_n_0 ),
        .O(reg_data_out__0[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_10 
       (.I0(slv_reg11[5]),
        .I1(slv_reg10[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[5]),
        .O(\axi_rdata[5]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_11 
       (.I0(slv_reg15[5]),
        .I1(slv_reg14[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[5]),
        .O(\axi_rdata[5]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_12 
       (.I0(slv_reg3[5]),
        .I1(slv_reg2[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[5]),
        .O(\axi_rdata[5]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_13 
       (.I0(slv_reg7[5]),
        .I1(slv_reg6[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[5]),
        .O(\axi_rdata[5]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_6 
       (.I0(slv_reg27[5]),
        .I1(slv_reg26[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[5]),
        .O(\axi_rdata[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_7 
       (.I0(slv_reg31[5]),
        .I1(slv_reg30[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[5]),
        .O(\axi_rdata[5]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_8 
       (.I0(slv_reg19[5]),
        .I1(slv_reg18[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[5]),
        .O(\axi_rdata[5]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_9 
       (.I0(slv_reg23[5]),
        .I1(slv_reg22[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[5]),
        .O(\axi_rdata[5]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata_reg[6]_i_2_n_0 ),
        .I1(\axi_rdata_reg[6]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[6]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[6]_i_5_n_0 ),
        .O(reg_data_out__0[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_10 
       (.I0(slv_reg11[6]),
        .I1(slv_reg10[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[6]),
        .O(\axi_rdata[6]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_11 
       (.I0(slv_reg15[6]),
        .I1(slv_reg14[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[6]),
        .O(\axi_rdata[6]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_12 
       (.I0(slv_reg3[6]),
        .I1(slv_reg2[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[6]),
        .O(\axi_rdata[6]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_13 
       (.I0(slv_reg7[6]),
        .I1(slv_reg6[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[6]),
        .O(\axi_rdata[6]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_6 
       (.I0(slv_reg27[6]),
        .I1(slv_reg26[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[6]),
        .O(\axi_rdata[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_7 
       (.I0(slv_reg31[6]),
        .I1(slv_reg30[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[6]),
        .O(\axi_rdata[6]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_8 
       (.I0(slv_reg19[6]),
        .I1(slv_reg18[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[6]),
        .O(\axi_rdata[6]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_9 
       (.I0(slv_reg23[6]),
        .I1(slv_reg22[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[6]),
        .O(\axi_rdata[6]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata_reg[7]_i_2_n_0 ),
        .I1(\axi_rdata_reg[7]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[7]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[7]_i_5_n_0 ),
        .O(reg_data_out__0[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_10 
       (.I0(slv_reg11[7]),
        .I1(slv_reg10[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[7]),
        .O(\axi_rdata[7]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_11 
       (.I0(slv_reg15[7]),
        .I1(slv_reg14[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[7]),
        .O(\axi_rdata[7]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_12 
       (.I0(slv_reg3[7]),
        .I1(slv_reg2[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[7]),
        .O(\axi_rdata[7]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_13 
       (.I0(slv_reg7[7]),
        .I1(slv_reg6[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[7]),
        .O(\axi_rdata[7]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_6 
       (.I0(slv_reg27[7]),
        .I1(slv_reg26[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[7]),
        .O(\axi_rdata[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_7 
       (.I0(slv_reg31[7]),
        .I1(slv_reg30[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[7]),
        .O(\axi_rdata[7]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_8 
       (.I0(slv_reg19[7]),
        .I1(slv_reg18[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[7]),
        .O(\axi_rdata[7]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_9 
       (.I0(slv_reg23[7]),
        .I1(slv_reg22[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[7]),
        .O(\axi_rdata[7]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata_reg[8]_i_2_n_0 ),
        .I1(\axi_rdata_reg[8]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[8]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[8]_i_5_n_0 ),
        .O(reg_data_out__0[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_10 
       (.I0(slv_reg11[8]),
        .I1(slv_reg10[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[8]),
        .O(\axi_rdata[8]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_11 
       (.I0(slv_reg15[8]),
        .I1(slv_reg14[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[8]),
        .O(\axi_rdata[8]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_12 
       (.I0(slv_reg3[8]),
        .I1(slv_reg2[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[8]),
        .O(\axi_rdata[8]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_13 
       (.I0(slv_reg7[8]),
        .I1(slv_reg6[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[8]),
        .O(\axi_rdata[8]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_6 
       (.I0(slv_reg27[8]),
        .I1(slv_reg26[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[8]),
        .O(\axi_rdata[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_7 
       (.I0(slv_reg31[8]),
        .I1(slv_reg30[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[8]),
        .O(\axi_rdata[8]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_8 
       (.I0(slv_reg19[8]),
        .I1(slv_reg18[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[8]),
        .O(\axi_rdata[8]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_9 
       (.I0(slv_reg23[8]),
        .I1(slv_reg22[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[8]),
        .O(\axi_rdata[8]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata_reg[9]_i_2_n_0 ),
        .I1(\axi_rdata_reg[9]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata_reg[9]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[9]_i_5_n_0 ),
        .O(reg_data_out__0[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_10 
       (.I0(slv_reg11[9]),
        .I1(slv_reg10[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg9[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg8[9]),
        .O(\axi_rdata[9]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_11 
       (.I0(slv_reg15[9]),
        .I1(slv_reg14[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[9]),
        .O(\axi_rdata[9]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_12 
       (.I0(slv_reg3[9]),
        .I1(slv_reg2[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[9]),
        .O(\axi_rdata[9]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_13 
       (.I0(slv_reg7[9]),
        .I1(slv_reg6[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg5[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg4[9]),
        .O(\axi_rdata[9]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_6 
       (.I0(slv_reg27[9]),
        .I1(slv_reg26[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[9]),
        .O(\axi_rdata[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_7 
       (.I0(slv_reg31[9]),
        .I1(slv_reg30[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[9]),
        .O(\axi_rdata[9]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_8 
       (.I0(slv_reg19[9]),
        .I1(slv_reg18[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[9]),
        .O(\axi_rdata[9]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_9 
       (.I0(slv_reg23[9]),
        .I1(slv_reg22[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[9]),
        .O(\axi_rdata[9]_i_9_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[0]),
        .Q(s00_axi_rdata[0]),
        .R(SR));
  MUXF7 \axi_rdata_reg[0]_i_2 
       (.I0(\axi_rdata[0]_i_6_n_0 ),
        .I1(\axi_rdata[0]_i_7_n_0 ),
        .O(\axi_rdata_reg[0]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_3 
       (.I0(\axi_rdata[0]_i_8_n_0 ),
        .I1(\axi_rdata[0]_i_9_n_0 ),
        .O(\axi_rdata_reg[0]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_4 
       (.I0(\axi_rdata[0]_i_10_n_0 ),
        .I1(\axi_rdata[0]_i_11_n_0 ),
        .O(\axi_rdata_reg[0]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_5 
       (.I0(\axi_rdata[0]_i_12_n_0 ),
        .I1(\axi_rdata[0]_i_13_n_0 ),
        .O(\axi_rdata_reg[0]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[10]),
        .Q(s00_axi_rdata[10]),
        .R(SR));
  MUXF7 \axi_rdata_reg[10]_i_2 
       (.I0(\axi_rdata[10]_i_6_n_0 ),
        .I1(\axi_rdata[10]_i_7_n_0 ),
        .O(\axi_rdata_reg[10]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_3 
       (.I0(\axi_rdata[10]_i_8_n_0 ),
        .I1(\axi_rdata[10]_i_9_n_0 ),
        .O(\axi_rdata_reg[10]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_4 
       (.I0(\axi_rdata[10]_i_10_n_0 ),
        .I1(\axi_rdata[10]_i_11_n_0 ),
        .O(\axi_rdata_reg[10]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_5 
       (.I0(\axi_rdata[10]_i_12_n_0 ),
        .I1(\axi_rdata[10]_i_13_n_0 ),
        .O(\axi_rdata_reg[10]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[11]),
        .Q(s00_axi_rdata[11]),
        .R(SR));
  MUXF7 \axi_rdata_reg[11]_i_2 
       (.I0(\axi_rdata[11]_i_6_n_0 ),
        .I1(\axi_rdata[11]_i_7_n_0 ),
        .O(\axi_rdata_reg[11]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_3 
       (.I0(\axi_rdata[11]_i_8_n_0 ),
        .I1(\axi_rdata[11]_i_9_n_0 ),
        .O(\axi_rdata_reg[11]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_4 
       (.I0(\axi_rdata[11]_i_10_n_0 ),
        .I1(\axi_rdata[11]_i_11_n_0 ),
        .O(\axi_rdata_reg[11]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_5 
       (.I0(\axi_rdata[11]_i_12_n_0 ),
        .I1(\axi_rdata[11]_i_13_n_0 ),
        .O(\axi_rdata_reg[11]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[12]),
        .Q(s00_axi_rdata[12]),
        .R(SR));
  MUXF7 \axi_rdata_reg[12]_i_2 
       (.I0(\axi_rdata[12]_i_6_n_0 ),
        .I1(\axi_rdata[12]_i_7_n_0 ),
        .O(\axi_rdata_reg[12]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_3 
       (.I0(\axi_rdata[12]_i_8_n_0 ),
        .I1(\axi_rdata[12]_i_9_n_0 ),
        .O(\axi_rdata_reg[12]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_4 
       (.I0(\axi_rdata[12]_i_10_n_0 ),
        .I1(\axi_rdata[12]_i_11_n_0 ),
        .O(\axi_rdata_reg[12]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_5 
       (.I0(\axi_rdata[12]_i_12_n_0 ),
        .I1(\axi_rdata[12]_i_13_n_0 ),
        .O(\axi_rdata_reg[12]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[13]),
        .Q(s00_axi_rdata[13]),
        .R(SR));
  MUXF7 \axi_rdata_reg[13]_i_2 
       (.I0(\axi_rdata[13]_i_6_n_0 ),
        .I1(\axi_rdata[13]_i_7_n_0 ),
        .O(\axi_rdata_reg[13]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_3 
       (.I0(\axi_rdata[13]_i_8_n_0 ),
        .I1(\axi_rdata[13]_i_9_n_0 ),
        .O(\axi_rdata_reg[13]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_4 
       (.I0(\axi_rdata[13]_i_10_n_0 ),
        .I1(\axi_rdata[13]_i_11_n_0 ),
        .O(\axi_rdata_reg[13]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_5 
       (.I0(\axi_rdata[13]_i_12_n_0 ),
        .I1(\axi_rdata[13]_i_13_n_0 ),
        .O(\axi_rdata_reg[13]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[14]),
        .Q(s00_axi_rdata[14]),
        .R(SR));
  MUXF7 \axi_rdata_reg[14]_i_2 
       (.I0(\axi_rdata[14]_i_6_n_0 ),
        .I1(\axi_rdata[14]_i_7_n_0 ),
        .O(\axi_rdata_reg[14]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_3 
       (.I0(\axi_rdata[14]_i_8_n_0 ),
        .I1(\axi_rdata[14]_i_9_n_0 ),
        .O(\axi_rdata_reg[14]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_4 
       (.I0(\axi_rdata[14]_i_10_n_0 ),
        .I1(\axi_rdata[14]_i_11_n_0 ),
        .O(\axi_rdata_reg[14]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_5 
       (.I0(\axi_rdata[14]_i_12_n_0 ),
        .I1(\axi_rdata[14]_i_13_n_0 ),
        .O(\axi_rdata_reg[14]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[15]),
        .Q(s00_axi_rdata[15]),
        .R(SR));
  MUXF7 \axi_rdata_reg[15]_i_2 
       (.I0(\axi_rdata[15]_i_6_n_0 ),
        .I1(\axi_rdata[15]_i_7_n_0 ),
        .O(\axi_rdata_reg[15]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_3 
       (.I0(\axi_rdata[15]_i_8_n_0 ),
        .I1(\axi_rdata[15]_i_9_n_0 ),
        .O(\axi_rdata_reg[15]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_4 
       (.I0(\axi_rdata[15]_i_10_n_0 ),
        .I1(\axi_rdata[15]_i_11_n_0 ),
        .O(\axi_rdata_reg[15]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_5 
       (.I0(\axi_rdata[15]_i_12_n_0 ),
        .I1(\axi_rdata[15]_i_13_n_0 ),
        .O(\axi_rdata_reg[15]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[16]),
        .Q(s00_axi_rdata[16]),
        .R(SR));
  MUXF7 \axi_rdata_reg[16]_i_2 
       (.I0(\axi_rdata[16]_i_6_n_0 ),
        .I1(\axi_rdata[16]_i_7_n_0 ),
        .O(\axi_rdata_reg[16]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_3 
       (.I0(\axi_rdata[16]_i_8_n_0 ),
        .I1(\axi_rdata[16]_i_9_n_0 ),
        .O(\axi_rdata_reg[16]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_4 
       (.I0(\axi_rdata[16]_i_10_n_0 ),
        .I1(\axi_rdata[16]_i_11_n_0 ),
        .O(\axi_rdata_reg[16]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_5 
       (.I0(\axi_rdata[16]_i_12_n_0 ),
        .I1(\axi_rdata[16]_i_13_n_0 ),
        .O(\axi_rdata_reg[16]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[17]),
        .Q(s00_axi_rdata[17]),
        .R(SR));
  MUXF7 \axi_rdata_reg[17]_i_2 
       (.I0(\axi_rdata[17]_i_6_n_0 ),
        .I1(\axi_rdata[17]_i_7_n_0 ),
        .O(\axi_rdata_reg[17]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_3 
       (.I0(\axi_rdata[17]_i_8_n_0 ),
        .I1(\axi_rdata[17]_i_9_n_0 ),
        .O(\axi_rdata_reg[17]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_4 
       (.I0(\axi_rdata[17]_i_10_n_0 ),
        .I1(\axi_rdata[17]_i_11_n_0 ),
        .O(\axi_rdata_reg[17]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_5 
       (.I0(\axi_rdata[17]_i_12_n_0 ),
        .I1(\axi_rdata[17]_i_13_n_0 ),
        .O(\axi_rdata_reg[17]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[18]),
        .Q(s00_axi_rdata[18]),
        .R(SR));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_6_n_0 ),
        .I1(\axi_rdata[18]_i_7_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_3 
       (.I0(\axi_rdata[18]_i_8_n_0 ),
        .I1(\axi_rdata[18]_i_9_n_0 ),
        .O(\axi_rdata_reg[18]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_4 
       (.I0(\axi_rdata[18]_i_10_n_0 ),
        .I1(\axi_rdata[18]_i_11_n_0 ),
        .O(\axi_rdata_reg[18]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_5 
       (.I0(\axi_rdata[18]_i_12_n_0 ),
        .I1(\axi_rdata[18]_i_13_n_0 ),
        .O(\axi_rdata_reg[18]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[19]),
        .Q(s00_axi_rdata[19]),
        .R(SR));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_6_n_0 ),
        .I1(\axi_rdata[19]_i_7_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_3 
       (.I0(\axi_rdata[19]_i_8_n_0 ),
        .I1(\axi_rdata[19]_i_9_n_0 ),
        .O(\axi_rdata_reg[19]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_4 
       (.I0(\axi_rdata[19]_i_10_n_0 ),
        .I1(\axi_rdata[19]_i_11_n_0 ),
        .O(\axi_rdata_reg[19]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_5 
       (.I0(\axi_rdata[19]_i_12_n_0 ),
        .I1(\axi_rdata[19]_i_13_n_0 ),
        .O(\axi_rdata_reg[19]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[1]),
        .Q(s00_axi_rdata[1]),
        .R(SR));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_6_n_0 ),
        .I1(\axi_rdata[1]_i_7_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_3 
       (.I0(\axi_rdata[1]_i_8_n_0 ),
        .I1(\axi_rdata[1]_i_9_n_0 ),
        .O(\axi_rdata_reg[1]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_4 
       (.I0(\axi_rdata[1]_i_10_n_0 ),
        .I1(\axi_rdata[1]_i_11_n_0 ),
        .O(\axi_rdata_reg[1]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_5 
       (.I0(\axi_rdata[1]_i_12_n_0 ),
        .I1(\axi_rdata[1]_i_13_n_0 ),
        .O(\axi_rdata_reg[1]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[20]),
        .Q(s00_axi_rdata[20]),
        .R(SR));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_6_n_0 ),
        .I1(\axi_rdata[20]_i_7_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_3 
       (.I0(\axi_rdata[20]_i_8_n_0 ),
        .I1(\axi_rdata[20]_i_9_n_0 ),
        .O(\axi_rdata_reg[20]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_4 
       (.I0(\axi_rdata[20]_i_10_n_0 ),
        .I1(\axi_rdata[20]_i_11_n_0 ),
        .O(\axi_rdata_reg[20]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_5 
       (.I0(\axi_rdata[20]_i_12_n_0 ),
        .I1(\axi_rdata[20]_i_13_n_0 ),
        .O(\axi_rdata_reg[20]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[21]),
        .Q(s00_axi_rdata[21]),
        .R(SR));
  MUXF7 \axi_rdata_reg[21]_i_2 
       (.I0(\axi_rdata[21]_i_6_n_0 ),
        .I1(\axi_rdata[21]_i_7_n_0 ),
        .O(\axi_rdata_reg[21]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_3 
       (.I0(\axi_rdata[21]_i_8_n_0 ),
        .I1(\axi_rdata[21]_i_9_n_0 ),
        .O(\axi_rdata_reg[21]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_4 
       (.I0(\axi_rdata[21]_i_10_n_0 ),
        .I1(\axi_rdata[21]_i_11_n_0 ),
        .O(\axi_rdata_reg[21]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_5 
       (.I0(\axi_rdata[21]_i_12_n_0 ),
        .I1(\axi_rdata[21]_i_13_n_0 ),
        .O(\axi_rdata_reg[21]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[22]),
        .Q(s00_axi_rdata[22]),
        .R(SR));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_6_n_0 ),
        .I1(\axi_rdata[22]_i_7_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_3 
       (.I0(\axi_rdata[22]_i_8_n_0 ),
        .I1(\axi_rdata[22]_i_9_n_0 ),
        .O(\axi_rdata_reg[22]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_4 
       (.I0(\axi_rdata[22]_i_10_n_0 ),
        .I1(\axi_rdata[22]_i_11_n_0 ),
        .O(\axi_rdata_reg[22]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_5 
       (.I0(\axi_rdata[22]_i_12_n_0 ),
        .I1(\axi_rdata[22]_i_13_n_0 ),
        .O(\axi_rdata_reg[22]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[23]),
        .Q(s00_axi_rdata[23]),
        .R(SR));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_6_n_0 ),
        .I1(\axi_rdata[23]_i_7_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_3 
       (.I0(\axi_rdata[23]_i_8_n_0 ),
        .I1(\axi_rdata[23]_i_9_n_0 ),
        .O(\axi_rdata_reg[23]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_4 
       (.I0(\axi_rdata[23]_i_10_n_0 ),
        .I1(\axi_rdata[23]_i_11_n_0 ),
        .O(\axi_rdata_reg[23]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_5 
       (.I0(\axi_rdata[23]_i_12_n_0 ),
        .I1(\axi_rdata[23]_i_13_n_0 ),
        .O(\axi_rdata_reg[23]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[24]),
        .Q(s00_axi_rdata[24]),
        .R(SR));
  MUXF7 \axi_rdata_reg[24]_i_2 
       (.I0(\axi_rdata[24]_i_6_n_0 ),
        .I1(\axi_rdata[24]_i_7_n_0 ),
        .O(\axi_rdata_reg[24]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_3 
       (.I0(\axi_rdata[24]_i_8_n_0 ),
        .I1(\axi_rdata[24]_i_9_n_0 ),
        .O(\axi_rdata_reg[24]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_4 
       (.I0(\axi_rdata[24]_i_10_n_0 ),
        .I1(\axi_rdata[24]_i_11_n_0 ),
        .O(\axi_rdata_reg[24]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_5 
       (.I0(\axi_rdata[24]_i_12_n_0 ),
        .I1(\axi_rdata[24]_i_13_n_0 ),
        .O(\axi_rdata_reg[24]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[25]),
        .Q(s00_axi_rdata[25]),
        .R(SR));
  MUXF7 \axi_rdata_reg[25]_i_2 
       (.I0(\axi_rdata[25]_i_6_n_0 ),
        .I1(\axi_rdata[25]_i_7_n_0 ),
        .O(\axi_rdata_reg[25]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_3 
       (.I0(\axi_rdata[25]_i_8_n_0 ),
        .I1(\axi_rdata[25]_i_9_n_0 ),
        .O(\axi_rdata_reg[25]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_4 
       (.I0(\axi_rdata[25]_i_10_n_0 ),
        .I1(\axi_rdata[25]_i_11_n_0 ),
        .O(\axi_rdata_reg[25]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_5 
       (.I0(\axi_rdata[25]_i_12_n_0 ),
        .I1(\axi_rdata[25]_i_13_n_0 ),
        .O(\axi_rdata_reg[25]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[26]),
        .Q(s00_axi_rdata[26]),
        .R(SR));
  MUXF7 \axi_rdata_reg[26]_i_2 
       (.I0(\axi_rdata[26]_i_6_n_0 ),
        .I1(\axi_rdata[26]_i_7_n_0 ),
        .O(\axi_rdata_reg[26]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_3 
       (.I0(\axi_rdata[26]_i_8_n_0 ),
        .I1(\axi_rdata[26]_i_9_n_0 ),
        .O(\axi_rdata_reg[26]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_4 
       (.I0(\axi_rdata[26]_i_10_n_0 ),
        .I1(\axi_rdata[26]_i_11_n_0 ),
        .O(\axi_rdata_reg[26]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_5 
       (.I0(\axi_rdata[26]_i_12_n_0 ),
        .I1(\axi_rdata[26]_i_13_n_0 ),
        .O(\axi_rdata_reg[26]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[27]),
        .Q(s00_axi_rdata[27]),
        .R(SR));
  MUXF7 \axi_rdata_reg[27]_i_2 
       (.I0(\axi_rdata[27]_i_6_n_0 ),
        .I1(\axi_rdata[27]_i_7_n_0 ),
        .O(\axi_rdata_reg[27]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_3 
       (.I0(\axi_rdata[27]_i_8_n_0 ),
        .I1(\axi_rdata[27]_i_9_n_0 ),
        .O(\axi_rdata_reg[27]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_4 
       (.I0(\axi_rdata[27]_i_10_n_0 ),
        .I1(\axi_rdata[27]_i_11_n_0 ),
        .O(\axi_rdata_reg[27]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_5 
       (.I0(\axi_rdata[27]_i_12_n_0 ),
        .I1(\axi_rdata[27]_i_13_n_0 ),
        .O(\axi_rdata_reg[27]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[28]),
        .Q(s00_axi_rdata[28]),
        .R(SR));
  MUXF7 \axi_rdata_reg[28]_i_2 
       (.I0(\axi_rdata[28]_i_6_n_0 ),
        .I1(\axi_rdata[28]_i_7_n_0 ),
        .O(\axi_rdata_reg[28]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_3 
       (.I0(\axi_rdata[28]_i_8_n_0 ),
        .I1(\axi_rdata[28]_i_9_n_0 ),
        .O(\axi_rdata_reg[28]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_4 
       (.I0(\axi_rdata[28]_i_10_n_0 ),
        .I1(\axi_rdata[28]_i_11_n_0 ),
        .O(\axi_rdata_reg[28]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_5 
       (.I0(\axi_rdata[28]_i_12_n_0 ),
        .I1(\axi_rdata[28]_i_13_n_0 ),
        .O(\axi_rdata_reg[28]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[29]),
        .Q(s00_axi_rdata[29]),
        .R(SR));
  MUXF7 \axi_rdata_reg[29]_i_2 
       (.I0(\axi_rdata[29]_i_6_n_0 ),
        .I1(\axi_rdata[29]_i_7_n_0 ),
        .O(\axi_rdata_reg[29]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_3 
       (.I0(\axi_rdata[29]_i_8_n_0 ),
        .I1(\axi_rdata[29]_i_9_n_0 ),
        .O(\axi_rdata_reg[29]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_4 
       (.I0(\axi_rdata[29]_i_10_n_0 ),
        .I1(\axi_rdata[29]_i_11_n_0 ),
        .O(\axi_rdata_reg[29]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_5 
       (.I0(\axi_rdata[29]_i_12_n_0 ),
        .I1(\axi_rdata[29]_i_13_n_0 ),
        .O(\axi_rdata_reg[29]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[2]),
        .Q(s00_axi_rdata[2]),
        .R(SR));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_6_n_0 ),
        .I1(\axi_rdata[2]_i_7_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_3 
       (.I0(\axi_rdata[2]_i_8_n_0 ),
        .I1(\axi_rdata[2]_i_9_n_0 ),
        .O(\axi_rdata_reg[2]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_4 
       (.I0(\axi_rdata[2]_i_10_n_0 ),
        .I1(\axi_rdata[2]_i_11_n_0 ),
        .O(\axi_rdata_reg[2]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_5 
       (.I0(\axi_rdata[2]_i_12_n_0 ),
        .I1(\axi_rdata[2]_i_13_n_0 ),
        .O(\axi_rdata_reg[2]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[30]),
        .Q(s00_axi_rdata[30]),
        .R(SR));
  MUXF7 \axi_rdata_reg[30]_i_2 
       (.I0(\axi_rdata[30]_i_6_n_0 ),
        .I1(\axi_rdata[30]_i_7_n_0 ),
        .O(\axi_rdata_reg[30]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_3 
       (.I0(\axi_rdata[30]_i_8_n_0 ),
        .I1(\axi_rdata[30]_i_9_n_0 ),
        .O(\axi_rdata_reg[30]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_4 
       (.I0(\axi_rdata[30]_i_10_n_0 ),
        .I1(\axi_rdata[30]_i_11_n_0 ),
        .O(\axi_rdata_reg[30]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_5 
       (.I0(\axi_rdata[30]_i_12_n_0 ),
        .I1(\axi_rdata[30]_i_13_n_0 ),
        .O(\axi_rdata_reg[30]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[31]),
        .Q(s00_axi_rdata[31]),
        .R(SR));
  MUXF7 \axi_rdata_reg[31]_i_3 
       (.I0(\axi_rdata[31]_i_7_n_0 ),
        .I1(\axi_rdata[31]_i_8_n_0 ),
        .O(\axi_rdata_reg[31]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_4 
       (.I0(\axi_rdata[31]_i_9_n_0 ),
        .I1(\axi_rdata[31]_i_10_n_0 ),
        .O(\axi_rdata_reg[31]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_5 
       (.I0(\axi_rdata[31]_i_11_n_0 ),
        .I1(\axi_rdata[31]_i_12_n_0 ),
        .O(\axi_rdata_reg[31]_i_5_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_6 
       (.I0(\axi_rdata[31]_i_13_n_0 ),
        .I1(\axi_rdata[31]_i_14_n_0 ),
        .O(\axi_rdata_reg[31]_i_6_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[3]),
        .Q(s00_axi_rdata[3]),
        .R(SR));
  MUXF7 \axi_rdata_reg[3]_i_2 
       (.I0(\axi_rdata[3]_i_6_n_0 ),
        .I1(\axi_rdata[3]_i_7_n_0 ),
        .O(\axi_rdata_reg[3]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_3 
       (.I0(\axi_rdata[3]_i_8_n_0 ),
        .I1(\axi_rdata[3]_i_9_n_0 ),
        .O(\axi_rdata_reg[3]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_4 
       (.I0(\axi_rdata[3]_i_10_n_0 ),
        .I1(\axi_rdata[3]_i_11_n_0 ),
        .O(\axi_rdata_reg[3]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_5 
       (.I0(\axi_rdata[3]_i_12_n_0 ),
        .I1(\axi_rdata[3]_i_13_n_0 ),
        .O(\axi_rdata_reg[3]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[4]),
        .Q(s00_axi_rdata[4]),
        .R(SR));
  MUXF7 \axi_rdata_reg[4]_i_2 
       (.I0(\axi_rdata[4]_i_6_n_0 ),
        .I1(\axi_rdata[4]_i_7_n_0 ),
        .O(\axi_rdata_reg[4]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_3 
       (.I0(\axi_rdata[4]_i_8_n_0 ),
        .I1(\axi_rdata[4]_i_9_n_0 ),
        .O(\axi_rdata_reg[4]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_4 
       (.I0(\axi_rdata[4]_i_10_n_0 ),
        .I1(\axi_rdata[4]_i_11_n_0 ),
        .O(\axi_rdata_reg[4]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_5 
       (.I0(\axi_rdata[4]_i_12_n_0 ),
        .I1(\axi_rdata[4]_i_13_n_0 ),
        .O(\axi_rdata_reg[4]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[5]),
        .Q(s00_axi_rdata[5]),
        .R(SR));
  MUXF7 \axi_rdata_reg[5]_i_2 
       (.I0(\axi_rdata[5]_i_6_n_0 ),
        .I1(\axi_rdata[5]_i_7_n_0 ),
        .O(\axi_rdata_reg[5]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_3 
       (.I0(\axi_rdata[5]_i_8_n_0 ),
        .I1(\axi_rdata[5]_i_9_n_0 ),
        .O(\axi_rdata_reg[5]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_4 
       (.I0(\axi_rdata[5]_i_10_n_0 ),
        .I1(\axi_rdata[5]_i_11_n_0 ),
        .O(\axi_rdata_reg[5]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_5 
       (.I0(\axi_rdata[5]_i_12_n_0 ),
        .I1(\axi_rdata[5]_i_13_n_0 ),
        .O(\axi_rdata_reg[5]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[6]),
        .Q(s00_axi_rdata[6]),
        .R(SR));
  MUXF7 \axi_rdata_reg[6]_i_2 
       (.I0(\axi_rdata[6]_i_6_n_0 ),
        .I1(\axi_rdata[6]_i_7_n_0 ),
        .O(\axi_rdata_reg[6]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_3 
       (.I0(\axi_rdata[6]_i_8_n_0 ),
        .I1(\axi_rdata[6]_i_9_n_0 ),
        .O(\axi_rdata_reg[6]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_4 
       (.I0(\axi_rdata[6]_i_10_n_0 ),
        .I1(\axi_rdata[6]_i_11_n_0 ),
        .O(\axi_rdata_reg[6]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_5 
       (.I0(\axi_rdata[6]_i_12_n_0 ),
        .I1(\axi_rdata[6]_i_13_n_0 ),
        .O(\axi_rdata_reg[6]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[7]),
        .Q(s00_axi_rdata[7]),
        .R(SR));
  MUXF7 \axi_rdata_reg[7]_i_2 
       (.I0(\axi_rdata[7]_i_6_n_0 ),
        .I1(\axi_rdata[7]_i_7_n_0 ),
        .O(\axi_rdata_reg[7]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_3 
       (.I0(\axi_rdata[7]_i_8_n_0 ),
        .I1(\axi_rdata[7]_i_9_n_0 ),
        .O(\axi_rdata_reg[7]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_4 
       (.I0(\axi_rdata[7]_i_10_n_0 ),
        .I1(\axi_rdata[7]_i_11_n_0 ),
        .O(\axi_rdata_reg[7]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_5 
       (.I0(\axi_rdata[7]_i_12_n_0 ),
        .I1(\axi_rdata[7]_i_13_n_0 ),
        .O(\axi_rdata_reg[7]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[8]),
        .Q(s00_axi_rdata[8]),
        .R(SR));
  MUXF7 \axi_rdata_reg[8]_i_2 
       (.I0(\axi_rdata[8]_i_6_n_0 ),
        .I1(\axi_rdata[8]_i_7_n_0 ),
        .O(\axi_rdata_reg[8]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_3 
       (.I0(\axi_rdata[8]_i_8_n_0 ),
        .I1(\axi_rdata[8]_i_9_n_0 ),
        .O(\axi_rdata_reg[8]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_4 
       (.I0(\axi_rdata[8]_i_10_n_0 ),
        .I1(\axi_rdata[8]_i_11_n_0 ),
        .O(\axi_rdata_reg[8]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_5 
       (.I0(\axi_rdata[8]_i_12_n_0 ),
        .I1(\axi_rdata[8]_i_13_n_0 ),
        .O(\axi_rdata_reg[8]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out__0[9]),
        .Q(s00_axi_rdata[9]),
        .R(SR));
  MUXF7 \axi_rdata_reg[9]_i_2 
       (.I0(\axi_rdata[9]_i_6_n_0 ),
        .I1(\axi_rdata[9]_i_7_n_0 ),
        .O(\axi_rdata_reg[9]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_3 
       (.I0(\axi_rdata[9]_i_8_n_0 ),
        .I1(\axi_rdata[9]_i_9_n_0 ),
        .O(\axi_rdata_reg[9]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_4 
       (.I0(\axi_rdata[9]_i_10_n_0 ),
        .I1(\axi_rdata[9]_i_11_n_0 ),
        .O(\axi_rdata_reg[9]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_5 
       (.I0(\axi_rdata[9]_i_12_n_0 ),
        .I1(\axi_rdata[9]_i_13_n_0 ),
        .O(\axi_rdata_reg[9]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready_reg_0),
        .Q(s00_axi_rvalid),
        .R(SR));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_wready),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(s00_axi_wready),
        .R(SR));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm control
       (.E(\sampCounter/processQ0 ),
        .SR(control_n_2),
        .clk(clk),
        .reset_n(reset_n),
        .reset_n_0(RST),
        .\state_reg[0]_0 (state[1]),
        .\state_reg[0]_1 (state[0]),
        .sw(sw));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath datapath
       (.E(\sampCounter/processQ0 ),
        .SR(RST),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .btn(btn),
        .clk(clk),
        .cw(cw),
        .reset_n(reset_n),
        .scl(scl),
        .sda(sda),
        .\state_reg[1] (control_n_2),
        .sw(sw),
        .switch(switch),
        .tmds(tmds),
        .tmdsb(tmdsb));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg0[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg0[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg0[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(p_1_in[31]));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg0[31]_i_2 
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_awready),
        .I3(s00_axi_wready),
        .I4(p_0_in[0]),
        .O(\slv_reg0[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg0[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg0[0]),
        .R(SR));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg0[10]),
        .R(SR));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg0[11]),
        .R(SR));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg0[12]),
        .R(SR));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg0[13]),
        .R(SR));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg0[14]),
        .R(SR));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg0[15]),
        .R(SR));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg0[16]),
        .R(SR));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg0[17]),
        .R(SR));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg0[18]),
        .R(SR));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg0[19]),
        .R(SR));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg0[1]),
        .R(SR));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg0[20]),
        .R(SR));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg0[21]),
        .R(SR));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg0[22]),
        .R(SR));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg0[23]),
        .R(SR));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg0[24]),
        .R(SR));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg0[25]),
        .R(SR));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg0[26]),
        .R(SR));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg0[27]),
        .R(SR));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg0[28]),
        .R(SR));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg0[29]),
        .R(SR));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg0[2]),
        .R(SR));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg0[30]),
        .R(SR));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg0[31]),
        .R(SR));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg0[3]),
        .R(SR));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg0[4]),
        .R(SR));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg0[5]),
        .R(SR));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg0[6]),
        .R(SR));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg0[7]),
        .R(SR));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg0[8]),
        .R(SR));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg0[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg10[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg10[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg10[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg10[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg10[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg10[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg10[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg10[7]_i_1_n_0 ));
  FDRE \slv_reg10_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg10[0]),
        .R(SR));
  FDRE \slv_reg10_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg10[10]),
        .R(SR));
  FDRE \slv_reg10_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg10[11]),
        .R(SR));
  FDRE \slv_reg10_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg10[12]),
        .R(SR));
  FDRE \slv_reg10_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg10[13]),
        .R(SR));
  FDRE \slv_reg10_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg10[14]),
        .R(SR));
  FDRE \slv_reg10_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg10[15]),
        .R(SR));
  FDRE \slv_reg10_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg10[16]),
        .R(SR));
  FDRE \slv_reg10_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg10[17]),
        .R(SR));
  FDRE \slv_reg10_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg10[18]),
        .R(SR));
  FDRE \slv_reg10_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg10[19]),
        .R(SR));
  FDRE \slv_reg10_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg10[1]),
        .R(SR));
  FDRE \slv_reg10_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg10[20]),
        .R(SR));
  FDRE \slv_reg10_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg10[21]),
        .R(SR));
  FDRE \slv_reg10_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg10[22]),
        .R(SR));
  FDRE \slv_reg10_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg10[23]),
        .R(SR));
  FDRE \slv_reg10_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg10[24]),
        .R(SR));
  FDRE \slv_reg10_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg10[25]),
        .R(SR));
  FDRE \slv_reg10_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg10[26]),
        .R(SR));
  FDRE \slv_reg10_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg10[27]),
        .R(SR));
  FDRE \slv_reg10_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg10[28]),
        .R(SR));
  FDRE \slv_reg10_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg10[29]),
        .R(SR));
  FDRE \slv_reg10_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg10[2]),
        .R(SR));
  FDRE \slv_reg10_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg10[30]),
        .R(SR));
  FDRE \slv_reg10_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg10[31]),
        .R(SR));
  FDRE \slv_reg10_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg10[3]),
        .R(SR));
  FDRE \slv_reg10_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg10[4]),
        .R(SR));
  FDRE \slv_reg10_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg10[5]),
        .R(SR));
  FDRE \slv_reg10_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg10[6]),
        .R(SR));
  FDRE \slv_reg10_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg10[7]),
        .R(SR));
  FDRE \slv_reg10_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg10[8]),
        .R(SR));
  FDRE \slv_reg10_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg10[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg11[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg11[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg11[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg11[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg11[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg11[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg11[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg11[7]_i_1_n_0 ));
  FDRE \slv_reg11_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg11[0]),
        .R(SR));
  FDRE \slv_reg11_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg11[10]),
        .R(SR));
  FDRE \slv_reg11_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg11[11]),
        .R(SR));
  FDRE \slv_reg11_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg11[12]),
        .R(SR));
  FDRE \slv_reg11_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg11[13]),
        .R(SR));
  FDRE \slv_reg11_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg11[14]),
        .R(SR));
  FDRE \slv_reg11_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg11[15]),
        .R(SR));
  FDRE \slv_reg11_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg11[16]),
        .R(SR));
  FDRE \slv_reg11_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg11[17]),
        .R(SR));
  FDRE \slv_reg11_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg11[18]),
        .R(SR));
  FDRE \slv_reg11_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg11[19]),
        .R(SR));
  FDRE \slv_reg11_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg11[1]),
        .R(SR));
  FDRE \slv_reg11_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg11[20]),
        .R(SR));
  FDRE \slv_reg11_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg11[21]),
        .R(SR));
  FDRE \slv_reg11_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg11[22]),
        .R(SR));
  FDRE \slv_reg11_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg11[23]),
        .R(SR));
  FDRE \slv_reg11_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg11[24]),
        .R(SR));
  FDRE \slv_reg11_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg11[25]),
        .R(SR));
  FDRE \slv_reg11_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg11[26]),
        .R(SR));
  FDRE \slv_reg11_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg11[27]),
        .R(SR));
  FDRE \slv_reg11_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg11[28]),
        .R(SR));
  FDRE \slv_reg11_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg11[29]),
        .R(SR));
  FDRE \slv_reg11_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg11[2]),
        .R(SR));
  FDRE \slv_reg11_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg11[30]),
        .R(SR));
  FDRE \slv_reg11_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg11[31]),
        .R(SR));
  FDRE \slv_reg11_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg11[3]),
        .R(SR));
  FDRE \slv_reg11_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg11[4]),
        .R(SR));
  FDRE \slv_reg11_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg11[5]),
        .R(SR));
  FDRE \slv_reg11_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg11[6]),
        .R(SR));
  FDRE \slv_reg11_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg11[7]),
        .R(SR));
  FDRE \slv_reg11_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg11[8]),
        .R(SR));
  FDRE \slv_reg11_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg11[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg12[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg12[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg12[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg12[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg12[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg12[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg12[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg12[7]_i_1_n_0 ));
  FDRE \slv_reg12_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg12[0]),
        .R(SR));
  FDRE \slv_reg12_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg12[10]),
        .R(SR));
  FDRE \slv_reg12_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg12[11]),
        .R(SR));
  FDRE \slv_reg12_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg12[12]),
        .R(SR));
  FDRE \slv_reg12_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg12[13]),
        .R(SR));
  FDRE \slv_reg12_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg12[14]),
        .R(SR));
  FDRE \slv_reg12_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg12[15]),
        .R(SR));
  FDRE \slv_reg12_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg12[16]),
        .R(SR));
  FDRE \slv_reg12_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg12[17]),
        .R(SR));
  FDRE \slv_reg12_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg12[18]),
        .R(SR));
  FDRE \slv_reg12_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg12[19]),
        .R(SR));
  FDRE \slv_reg12_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg12[1]),
        .R(SR));
  FDRE \slv_reg12_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg12[20]),
        .R(SR));
  FDRE \slv_reg12_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg12[21]),
        .R(SR));
  FDRE \slv_reg12_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg12[22]),
        .R(SR));
  FDRE \slv_reg12_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg12[23]),
        .R(SR));
  FDRE \slv_reg12_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg12[24]),
        .R(SR));
  FDRE \slv_reg12_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg12[25]),
        .R(SR));
  FDRE \slv_reg12_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg12[26]),
        .R(SR));
  FDRE \slv_reg12_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg12[27]),
        .R(SR));
  FDRE \slv_reg12_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg12[28]),
        .R(SR));
  FDRE \slv_reg12_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg12[29]),
        .R(SR));
  FDRE \slv_reg12_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg12[2]),
        .R(SR));
  FDRE \slv_reg12_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg12[30]),
        .R(SR));
  FDRE \slv_reg12_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg12[31]),
        .R(SR));
  FDRE \slv_reg12_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg12[3]),
        .R(SR));
  FDRE \slv_reg12_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg12[4]),
        .R(SR));
  FDRE \slv_reg12_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg12[5]),
        .R(SR));
  FDRE \slv_reg12_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg12[6]),
        .R(SR));
  FDRE \slv_reg12_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg12[7]),
        .R(SR));
  FDRE \slv_reg12_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg12[8]),
        .R(SR));
  FDRE \slv_reg12_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg12[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg13[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg13[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg13[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg13[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg13[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg13[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg13[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg13[7]_i_1_n_0 ));
  FDRE \slv_reg13_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg13[0]),
        .R(SR));
  FDRE \slv_reg13_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg13[10]),
        .R(SR));
  FDRE \slv_reg13_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg13[11]),
        .R(SR));
  FDRE \slv_reg13_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg13[12]),
        .R(SR));
  FDRE \slv_reg13_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg13[13]),
        .R(SR));
  FDRE \slv_reg13_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg13[14]),
        .R(SR));
  FDRE \slv_reg13_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg13[15]),
        .R(SR));
  FDRE \slv_reg13_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg13[16]),
        .R(SR));
  FDRE \slv_reg13_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg13[17]),
        .R(SR));
  FDRE \slv_reg13_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg13[18]),
        .R(SR));
  FDRE \slv_reg13_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg13[19]),
        .R(SR));
  FDRE \slv_reg13_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg13[1]),
        .R(SR));
  FDRE \slv_reg13_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg13[20]),
        .R(SR));
  FDRE \slv_reg13_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg13[21]),
        .R(SR));
  FDRE \slv_reg13_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg13[22]),
        .R(SR));
  FDRE \slv_reg13_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg13[23]),
        .R(SR));
  FDRE \slv_reg13_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg13[24]),
        .R(SR));
  FDRE \slv_reg13_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg13[25]),
        .R(SR));
  FDRE \slv_reg13_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg13[26]),
        .R(SR));
  FDRE \slv_reg13_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg13[27]),
        .R(SR));
  FDRE \slv_reg13_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg13[28]),
        .R(SR));
  FDRE \slv_reg13_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg13[29]),
        .R(SR));
  FDRE \slv_reg13_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg13[2]),
        .R(SR));
  FDRE \slv_reg13_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg13[30]),
        .R(SR));
  FDRE \slv_reg13_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg13[31]),
        .R(SR));
  FDRE \slv_reg13_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg13[3]),
        .R(SR));
  FDRE \slv_reg13_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg13[4]),
        .R(SR));
  FDRE \slv_reg13_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg13[5]),
        .R(SR));
  FDRE \slv_reg13_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg13[6]),
        .R(SR));
  FDRE \slv_reg13_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg13[7]),
        .R(SR));
  FDRE \slv_reg13_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg13[8]),
        .R(SR));
  FDRE \slv_reg13_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg13[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg14[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg14[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg14[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg14[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg14[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg14[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg14[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg14[7]_i_1_n_0 ));
  FDRE \slv_reg14_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg14[0]),
        .R(SR));
  FDRE \slv_reg14_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg14[10]),
        .R(SR));
  FDRE \slv_reg14_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg14[11]),
        .R(SR));
  FDRE \slv_reg14_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg14[12]),
        .R(SR));
  FDRE \slv_reg14_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg14[13]),
        .R(SR));
  FDRE \slv_reg14_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg14[14]),
        .R(SR));
  FDRE \slv_reg14_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg14[15]),
        .R(SR));
  FDRE \slv_reg14_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg14[16]),
        .R(SR));
  FDRE \slv_reg14_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg14[17]),
        .R(SR));
  FDRE \slv_reg14_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg14[18]),
        .R(SR));
  FDRE \slv_reg14_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg14[19]),
        .R(SR));
  FDRE \slv_reg14_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg14[1]),
        .R(SR));
  FDRE \slv_reg14_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg14[20]),
        .R(SR));
  FDRE \slv_reg14_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg14[21]),
        .R(SR));
  FDRE \slv_reg14_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg14[22]),
        .R(SR));
  FDRE \slv_reg14_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg14[23]),
        .R(SR));
  FDRE \slv_reg14_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg14[24]),
        .R(SR));
  FDRE \slv_reg14_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg14[25]),
        .R(SR));
  FDRE \slv_reg14_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg14[26]),
        .R(SR));
  FDRE \slv_reg14_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg14[27]),
        .R(SR));
  FDRE \slv_reg14_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg14[28]),
        .R(SR));
  FDRE \slv_reg14_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg14[29]),
        .R(SR));
  FDRE \slv_reg14_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg14[2]),
        .R(SR));
  FDRE \slv_reg14_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg14[30]),
        .R(SR));
  FDRE \slv_reg14_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg14[31]),
        .R(SR));
  FDRE \slv_reg14_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg14[3]),
        .R(SR));
  FDRE \slv_reg14_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg14[4]),
        .R(SR));
  FDRE \slv_reg14_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg14[5]),
        .R(SR));
  FDRE \slv_reg14_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg14[6]),
        .R(SR));
  FDRE \slv_reg14_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg14[7]),
        .R(SR));
  FDRE \slv_reg14_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg14[8]),
        .R(SR));
  FDRE \slv_reg14_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg14[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg15[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg15[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg15[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg15[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg15[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg15[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg15[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg15[7]_i_1_n_0 ));
  FDRE \slv_reg15_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg15[0]),
        .R(SR));
  FDRE \slv_reg15_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg15[10]),
        .R(SR));
  FDRE \slv_reg15_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg15[11]),
        .R(SR));
  FDRE \slv_reg15_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg15[12]),
        .R(SR));
  FDRE \slv_reg15_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg15[13]),
        .R(SR));
  FDRE \slv_reg15_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg15[14]),
        .R(SR));
  FDRE \slv_reg15_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg15[15]),
        .R(SR));
  FDRE \slv_reg15_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg15[16]),
        .R(SR));
  FDRE \slv_reg15_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg15[17]),
        .R(SR));
  FDRE \slv_reg15_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg15[18]),
        .R(SR));
  FDRE \slv_reg15_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg15[19]),
        .R(SR));
  FDRE \slv_reg15_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg15[1]),
        .R(SR));
  FDRE \slv_reg15_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg15[20]),
        .R(SR));
  FDRE \slv_reg15_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg15[21]),
        .R(SR));
  FDRE \slv_reg15_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg15[22]),
        .R(SR));
  FDRE \slv_reg15_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg15[23]),
        .R(SR));
  FDRE \slv_reg15_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg15[24]),
        .R(SR));
  FDRE \slv_reg15_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg15[25]),
        .R(SR));
  FDRE \slv_reg15_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg15[26]),
        .R(SR));
  FDRE \slv_reg15_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg15[27]),
        .R(SR));
  FDRE \slv_reg15_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg15[28]),
        .R(SR));
  FDRE \slv_reg15_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg15[29]),
        .R(SR));
  FDRE \slv_reg15_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg15[2]),
        .R(SR));
  FDRE \slv_reg15_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg15[30]),
        .R(SR));
  FDRE \slv_reg15_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg15[31]),
        .R(SR));
  FDRE \slv_reg15_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg15[3]),
        .R(SR));
  FDRE \slv_reg15_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg15[4]),
        .R(SR));
  FDRE \slv_reg15_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg15[5]),
        .R(SR));
  FDRE \slv_reg15_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg15[6]),
        .R(SR));
  FDRE \slv_reg15_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg15[7]),
        .R(SR));
  FDRE \slv_reg15_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg15[8]),
        .R(SR));
  FDRE \slv_reg15_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg15[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg16[15]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg16[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg16[23]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg16[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg16[31]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg16[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg16[7]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg16[7]_i_1_n_0 ));
  FDRE \slv_reg16_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg16[0]),
        .R(SR));
  FDRE \slv_reg16_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg16[10]),
        .R(SR));
  FDRE \slv_reg16_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg16[11]),
        .R(SR));
  FDRE \slv_reg16_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg16[12]),
        .R(SR));
  FDRE \slv_reg16_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg16[13]),
        .R(SR));
  FDRE \slv_reg16_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg16[14]),
        .R(SR));
  FDRE \slv_reg16_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg16[15]),
        .R(SR));
  FDRE \slv_reg16_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg16[16]),
        .R(SR));
  FDRE \slv_reg16_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg16[17]),
        .R(SR));
  FDRE \slv_reg16_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg16[18]),
        .R(SR));
  FDRE \slv_reg16_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg16[19]),
        .R(SR));
  FDRE \slv_reg16_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg16[1]),
        .R(SR));
  FDRE \slv_reg16_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg16[20]),
        .R(SR));
  FDRE \slv_reg16_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg16[21]),
        .R(SR));
  FDRE \slv_reg16_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg16[22]),
        .R(SR));
  FDRE \slv_reg16_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg16[23]),
        .R(SR));
  FDRE \slv_reg16_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg16[24]),
        .R(SR));
  FDRE \slv_reg16_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg16[25]),
        .R(SR));
  FDRE \slv_reg16_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg16[26]),
        .R(SR));
  FDRE \slv_reg16_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg16[27]),
        .R(SR));
  FDRE \slv_reg16_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg16[28]),
        .R(SR));
  FDRE \slv_reg16_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg16[29]),
        .R(SR));
  FDRE \slv_reg16_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg16[2]),
        .R(SR));
  FDRE \slv_reg16_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg16[30]),
        .R(SR));
  FDRE \slv_reg16_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg16[31]),
        .R(SR));
  FDRE \slv_reg16_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg16[3]),
        .R(SR));
  FDRE \slv_reg16_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg16[4]),
        .R(SR));
  FDRE \slv_reg16_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg16[5]),
        .R(SR));
  FDRE \slv_reg16_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg16[6]),
        .R(SR));
  FDRE \slv_reg16_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg16[7]),
        .R(SR));
  FDRE \slv_reg16_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg16[8]),
        .R(SR));
  FDRE \slv_reg16_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg16[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg17[15]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg17[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg17[23]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg17[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg17[31]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg17[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg17[7]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg17[7]_i_1_n_0 ));
  FDRE \slv_reg17_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg17[0]),
        .R(SR));
  FDRE \slv_reg17_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg17[10]),
        .R(SR));
  FDRE \slv_reg17_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg17[11]),
        .R(SR));
  FDRE \slv_reg17_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg17[12]),
        .R(SR));
  FDRE \slv_reg17_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg17[13]),
        .R(SR));
  FDRE \slv_reg17_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg17[14]),
        .R(SR));
  FDRE \slv_reg17_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg17[15]),
        .R(SR));
  FDRE \slv_reg17_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg17[16]),
        .R(SR));
  FDRE \slv_reg17_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg17[17]),
        .R(SR));
  FDRE \slv_reg17_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg17[18]),
        .R(SR));
  FDRE \slv_reg17_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg17[19]),
        .R(SR));
  FDRE \slv_reg17_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg17[1]),
        .R(SR));
  FDRE \slv_reg17_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg17[20]),
        .R(SR));
  FDRE \slv_reg17_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg17[21]),
        .R(SR));
  FDRE \slv_reg17_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg17[22]),
        .R(SR));
  FDRE \slv_reg17_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg17[23]),
        .R(SR));
  FDRE \slv_reg17_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg17[24]),
        .R(SR));
  FDRE \slv_reg17_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg17[25]),
        .R(SR));
  FDRE \slv_reg17_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg17[26]),
        .R(SR));
  FDRE \slv_reg17_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg17[27]),
        .R(SR));
  FDRE \slv_reg17_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg17[28]),
        .R(SR));
  FDRE \slv_reg17_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg17[29]),
        .R(SR));
  FDRE \slv_reg17_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg17[2]),
        .R(SR));
  FDRE \slv_reg17_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg17[30]),
        .R(SR));
  FDRE \slv_reg17_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg17[31]),
        .R(SR));
  FDRE \slv_reg17_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg17[3]),
        .R(SR));
  FDRE \slv_reg17_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg17[4]),
        .R(SR));
  FDRE \slv_reg17_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg17[5]),
        .R(SR));
  FDRE \slv_reg17_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg17[6]),
        .R(SR));
  FDRE \slv_reg17_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg17[7]),
        .R(SR));
  FDRE \slv_reg17_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg17[8]),
        .R(SR));
  FDRE \slv_reg17_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg17[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg18[15]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg18[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg18[23]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg18[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg18[31]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg18[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg18[7]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg18[7]_i_1_n_0 ));
  FDRE \slv_reg18_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg18[0]),
        .R(SR));
  FDRE \slv_reg18_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg18[10]),
        .R(SR));
  FDRE \slv_reg18_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg18[11]),
        .R(SR));
  FDRE \slv_reg18_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg18[12]),
        .R(SR));
  FDRE \slv_reg18_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg18[13]),
        .R(SR));
  FDRE \slv_reg18_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg18[14]),
        .R(SR));
  FDRE \slv_reg18_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg18[15]),
        .R(SR));
  FDRE \slv_reg18_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg18[16]),
        .R(SR));
  FDRE \slv_reg18_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg18[17]),
        .R(SR));
  FDRE \slv_reg18_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg18[18]),
        .R(SR));
  FDRE \slv_reg18_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg18[19]),
        .R(SR));
  FDRE \slv_reg18_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg18[1]),
        .R(SR));
  FDRE \slv_reg18_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg18[20]),
        .R(SR));
  FDRE \slv_reg18_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg18[21]),
        .R(SR));
  FDRE \slv_reg18_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg18[22]),
        .R(SR));
  FDRE \slv_reg18_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg18[23]),
        .R(SR));
  FDRE \slv_reg18_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg18[24]),
        .R(SR));
  FDRE \slv_reg18_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg18[25]),
        .R(SR));
  FDRE \slv_reg18_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg18[26]),
        .R(SR));
  FDRE \slv_reg18_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg18[27]),
        .R(SR));
  FDRE \slv_reg18_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg18[28]),
        .R(SR));
  FDRE \slv_reg18_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg18[29]),
        .R(SR));
  FDRE \slv_reg18_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg18[2]),
        .R(SR));
  FDRE \slv_reg18_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg18[30]),
        .R(SR));
  FDRE \slv_reg18_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg18[31]),
        .R(SR));
  FDRE \slv_reg18_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg18[3]),
        .R(SR));
  FDRE \slv_reg18_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg18[4]),
        .R(SR));
  FDRE \slv_reg18_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg18[5]),
        .R(SR));
  FDRE \slv_reg18_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg18[6]),
        .R(SR));
  FDRE \slv_reg18_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg18[7]),
        .R(SR));
  FDRE \slv_reg18_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg18[8]),
        .R(SR));
  FDRE \slv_reg18_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg18[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg19[15]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg19[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg19[23]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg19[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg19[31]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg19[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg19[7]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg19[7]_i_1_n_0 ));
  FDRE \slv_reg19_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg19[0]),
        .R(SR));
  FDRE \slv_reg19_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg19[10]),
        .R(SR));
  FDRE \slv_reg19_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg19[11]),
        .R(SR));
  FDRE \slv_reg19_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg19[12]),
        .R(SR));
  FDRE \slv_reg19_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg19[13]),
        .R(SR));
  FDRE \slv_reg19_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg19[14]),
        .R(SR));
  FDRE \slv_reg19_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg19[15]),
        .R(SR));
  FDRE \slv_reg19_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg19[16]),
        .R(SR));
  FDRE \slv_reg19_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg19[17]),
        .R(SR));
  FDRE \slv_reg19_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg19[18]),
        .R(SR));
  FDRE \slv_reg19_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg19[19]),
        .R(SR));
  FDRE \slv_reg19_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg19[1]),
        .R(SR));
  FDRE \slv_reg19_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg19[20]),
        .R(SR));
  FDRE \slv_reg19_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg19[21]),
        .R(SR));
  FDRE \slv_reg19_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg19[22]),
        .R(SR));
  FDRE \slv_reg19_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg19[23]),
        .R(SR));
  FDRE \slv_reg19_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg19[24]),
        .R(SR));
  FDRE \slv_reg19_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg19[25]),
        .R(SR));
  FDRE \slv_reg19_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg19[26]),
        .R(SR));
  FDRE \slv_reg19_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg19[27]),
        .R(SR));
  FDRE \slv_reg19_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg19[28]),
        .R(SR));
  FDRE \slv_reg19_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg19[29]),
        .R(SR));
  FDRE \slv_reg19_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg19[2]),
        .R(SR));
  FDRE \slv_reg19_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg19[30]),
        .R(SR));
  FDRE \slv_reg19_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg19[31]),
        .R(SR));
  FDRE \slv_reg19_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg19[3]),
        .R(SR));
  FDRE \slv_reg19_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg19[4]),
        .R(SR));
  FDRE \slv_reg19_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg19[5]),
        .R(SR));
  FDRE \slv_reg19_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg19[6]),
        .R(SR));
  FDRE \slv_reg19_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg19[7]),
        .R(SR));
  FDRE \slv_reg19_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg19[8]),
        .R(SR));
  FDRE \slv_reg19_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg19[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg1[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg1[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg1[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg1[31]_i_2 
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_awready),
        .I3(s00_axi_wready),
        .I4(p_0_in[0]),
        .O(\slv_reg1[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg1[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(SR));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(SR));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(SR));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(SR));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(SR));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(SR));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(SR));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(SR));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(SR));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(SR));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(SR));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(SR));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(SR));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(SR));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(SR));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(SR));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(SR));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(SR));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(SR));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(SR));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(SR));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(SR));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(SR));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(SR));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(SR));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(SR));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(SR));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(SR));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(SR));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(SR));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(SR));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg20[15]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg20[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg20[23]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg20[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg20[31]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg20[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg20[7]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg20[7]_i_1_n_0 ));
  FDRE \slv_reg20_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg20[0]),
        .R(SR));
  FDRE \slv_reg20_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg20[10]),
        .R(SR));
  FDRE \slv_reg20_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg20[11]),
        .R(SR));
  FDRE \slv_reg20_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg20[12]),
        .R(SR));
  FDRE \slv_reg20_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg20[13]),
        .R(SR));
  FDRE \slv_reg20_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg20[14]),
        .R(SR));
  FDRE \slv_reg20_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg20[15]),
        .R(SR));
  FDRE \slv_reg20_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg20[16]),
        .R(SR));
  FDRE \slv_reg20_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg20[17]),
        .R(SR));
  FDRE \slv_reg20_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg20[18]),
        .R(SR));
  FDRE \slv_reg20_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg20[19]),
        .R(SR));
  FDRE \slv_reg20_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg20[1]),
        .R(SR));
  FDRE \slv_reg20_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg20[20]),
        .R(SR));
  FDRE \slv_reg20_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg20[21]),
        .R(SR));
  FDRE \slv_reg20_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg20[22]),
        .R(SR));
  FDRE \slv_reg20_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg20[23]),
        .R(SR));
  FDRE \slv_reg20_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg20[24]),
        .R(SR));
  FDRE \slv_reg20_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg20[25]),
        .R(SR));
  FDRE \slv_reg20_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg20[26]),
        .R(SR));
  FDRE \slv_reg20_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg20[27]),
        .R(SR));
  FDRE \slv_reg20_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg20[28]),
        .R(SR));
  FDRE \slv_reg20_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg20[29]),
        .R(SR));
  FDRE \slv_reg20_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg20[2]),
        .R(SR));
  FDRE \slv_reg20_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg20[30]),
        .R(SR));
  FDRE \slv_reg20_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg20[31]),
        .R(SR));
  FDRE \slv_reg20_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg20[3]),
        .R(SR));
  FDRE \slv_reg20_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg20[4]),
        .R(SR));
  FDRE \slv_reg20_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg20[5]),
        .R(SR));
  FDRE \slv_reg20_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg20[6]),
        .R(SR));
  FDRE \slv_reg20_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg20[7]),
        .R(SR));
  FDRE \slv_reg20_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg20[8]),
        .R(SR));
  FDRE \slv_reg20_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg20[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg21[15]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg21[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg21[23]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg21[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg21[31]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg21[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg21[7]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg21[7]_i_1_n_0 ));
  FDRE \slv_reg21_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg21[0]),
        .R(SR));
  FDRE \slv_reg21_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg21[10]),
        .R(SR));
  FDRE \slv_reg21_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg21[11]),
        .R(SR));
  FDRE \slv_reg21_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg21[12]),
        .R(SR));
  FDRE \slv_reg21_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg21[13]),
        .R(SR));
  FDRE \slv_reg21_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg21[14]),
        .R(SR));
  FDRE \slv_reg21_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg21[15]),
        .R(SR));
  FDRE \slv_reg21_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg21[16]),
        .R(SR));
  FDRE \slv_reg21_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg21[17]),
        .R(SR));
  FDRE \slv_reg21_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg21[18]),
        .R(SR));
  FDRE \slv_reg21_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg21[19]),
        .R(SR));
  FDRE \slv_reg21_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg21[1]),
        .R(SR));
  FDRE \slv_reg21_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg21[20]),
        .R(SR));
  FDRE \slv_reg21_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg21[21]),
        .R(SR));
  FDRE \slv_reg21_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg21[22]),
        .R(SR));
  FDRE \slv_reg21_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg21[23]),
        .R(SR));
  FDRE \slv_reg21_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg21[24]),
        .R(SR));
  FDRE \slv_reg21_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg21[25]),
        .R(SR));
  FDRE \slv_reg21_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg21[26]),
        .R(SR));
  FDRE \slv_reg21_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg21[27]),
        .R(SR));
  FDRE \slv_reg21_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg21[28]),
        .R(SR));
  FDRE \slv_reg21_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg21[29]),
        .R(SR));
  FDRE \slv_reg21_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg21[2]),
        .R(SR));
  FDRE \slv_reg21_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg21[30]),
        .R(SR));
  FDRE \slv_reg21_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg21[31]),
        .R(SR));
  FDRE \slv_reg21_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg21[3]),
        .R(SR));
  FDRE \slv_reg21_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg21[4]),
        .R(SR));
  FDRE \slv_reg21_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg21[5]),
        .R(SR));
  FDRE \slv_reg21_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg21[6]),
        .R(SR));
  FDRE \slv_reg21_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg21[7]),
        .R(SR));
  FDRE \slv_reg21_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg21[8]),
        .R(SR));
  FDRE \slv_reg21_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg21[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg22[15]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg22[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg22[23]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg22[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg22[31]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg22[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg22[7]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg22[7]_i_1_n_0 ));
  FDRE \slv_reg22_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg22[0]),
        .R(SR));
  FDRE \slv_reg22_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg22[10]),
        .R(SR));
  FDRE \slv_reg22_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg22[11]),
        .R(SR));
  FDRE \slv_reg22_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg22[12]),
        .R(SR));
  FDRE \slv_reg22_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg22[13]),
        .R(SR));
  FDRE \slv_reg22_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg22[14]),
        .R(SR));
  FDRE \slv_reg22_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg22[15]),
        .R(SR));
  FDRE \slv_reg22_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg22[16]),
        .R(SR));
  FDRE \slv_reg22_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg22[17]),
        .R(SR));
  FDRE \slv_reg22_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg22[18]),
        .R(SR));
  FDRE \slv_reg22_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg22[19]),
        .R(SR));
  FDRE \slv_reg22_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg22[1]),
        .R(SR));
  FDRE \slv_reg22_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg22[20]),
        .R(SR));
  FDRE \slv_reg22_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg22[21]),
        .R(SR));
  FDRE \slv_reg22_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg22[22]),
        .R(SR));
  FDRE \slv_reg22_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg22[23]),
        .R(SR));
  FDRE \slv_reg22_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg22[24]),
        .R(SR));
  FDRE \slv_reg22_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg22[25]),
        .R(SR));
  FDRE \slv_reg22_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg22[26]),
        .R(SR));
  FDRE \slv_reg22_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg22[27]),
        .R(SR));
  FDRE \slv_reg22_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg22[28]),
        .R(SR));
  FDRE \slv_reg22_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg22[29]),
        .R(SR));
  FDRE \slv_reg22_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg22[2]),
        .R(SR));
  FDRE \slv_reg22_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg22[30]),
        .R(SR));
  FDRE \slv_reg22_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg22[31]),
        .R(SR));
  FDRE \slv_reg22_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg22[3]),
        .R(SR));
  FDRE \slv_reg22_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg22[4]),
        .R(SR));
  FDRE \slv_reg22_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg22[5]),
        .R(SR));
  FDRE \slv_reg22_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg22[6]),
        .R(SR));
  FDRE \slv_reg22_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg22[7]),
        .R(SR));
  FDRE \slv_reg22_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg22[8]),
        .R(SR));
  FDRE \slv_reg22_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg22[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg23[15]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg23[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg23[23]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg23[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg23[31]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg23[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg23[7]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[4]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg23[7]_i_1_n_0 ));
  FDRE \slv_reg23_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg23[0]),
        .R(SR));
  FDRE \slv_reg23_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg23[10]),
        .R(SR));
  FDRE \slv_reg23_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg23[11]),
        .R(SR));
  FDRE \slv_reg23_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg23[12]),
        .R(SR));
  FDRE \slv_reg23_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg23[13]),
        .R(SR));
  FDRE \slv_reg23_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg23[14]),
        .R(SR));
  FDRE \slv_reg23_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg23[15]),
        .R(SR));
  FDRE \slv_reg23_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg23[16]),
        .R(SR));
  FDRE \slv_reg23_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg23[17]),
        .R(SR));
  FDRE \slv_reg23_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg23[18]),
        .R(SR));
  FDRE \slv_reg23_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg23[19]),
        .R(SR));
  FDRE \slv_reg23_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg23[1]),
        .R(SR));
  FDRE \slv_reg23_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg23[20]),
        .R(SR));
  FDRE \slv_reg23_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg23[21]),
        .R(SR));
  FDRE \slv_reg23_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg23[22]),
        .R(SR));
  FDRE \slv_reg23_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg23[23]),
        .R(SR));
  FDRE \slv_reg23_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg23[24]),
        .R(SR));
  FDRE \slv_reg23_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg23[25]),
        .R(SR));
  FDRE \slv_reg23_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg23[26]),
        .R(SR));
  FDRE \slv_reg23_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg23[27]),
        .R(SR));
  FDRE \slv_reg23_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg23[28]),
        .R(SR));
  FDRE \slv_reg23_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg23[29]),
        .R(SR));
  FDRE \slv_reg23_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg23[2]),
        .R(SR));
  FDRE \slv_reg23_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg23[30]),
        .R(SR));
  FDRE \slv_reg23_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg23[31]),
        .R(SR));
  FDRE \slv_reg23_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg23[3]),
        .R(SR));
  FDRE \slv_reg23_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg23[4]),
        .R(SR));
  FDRE \slv_reg23_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg23[5]),
        .R(SR));
  FDRE \slv_reg23_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg23[6]),
        .R(SR));
  FDRE \slv_reg23_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg23[7]),
        .R(SR));
  FDRE \slv_reg23_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg23[8]),
        .R(SR));
  FDRE \slv_reg23_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg23[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg24[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg24[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg24[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg24[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg24[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg24[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg24[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg24[7]_i_1_n_0 ));
  FDRE \slv_reg24_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg24[0]),
        .R(SR));
  FDRE \slv_reg24_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg24[10]),
        .R(SR));
  FDRE \slv_reg24_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg24[11]),
        .R(SR));
  FDRE \slv_reg24_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg24[12]),
        .R(SR));
  FDRE \slv_reg24_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg24[13]),
        .R(SR));
  FDRE \slv_reg24_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg24[14]),
        .R(SR));
  FDRE \slv_reg24_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg24[15]),
        .R(SR));
  FDRE \slv_reg24_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg24[16]),
        .R(SR));
  FDRE \slv_reg24_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg24[17]),
        .R(SR));
  FDRE \slv_reg24_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg24[18]),
        .R(SR));
  FDRE \slv_reg24_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg24[19]),
        .R(SR));
  FDRE \slv_reg24_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg24[1]),
        .R(SR));
  FDRE \slv_reg24_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg24[20]),
        .R(SR));
  FDRE \slv_reg24_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg24[21]),
        .R(SR));
  FDRE \slv_reg24_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg24[22]),
        .R(SR));
  FDRE \slv_reg24_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg24[23]),
        .R(SR));
  FDRE \slv_reg24_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg24[24]),
        .R(SR));
  FDRE \slv_reg24_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg24[25]),
        .R(SR));
  FDRE \slv_reg24_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg24[26]),
        .R(SR));
  FDRE \slv_reg24_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg24[27]),
        .R(SR));
  FDRE \slv_reg24_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg24[28]),
        .R(SR));
  FDRE \slv_reg24_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg24[29]),
        .R(SR));
  FDRE \slv_reg24_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg24[2]),
        .R(SR));
  FDRE \slv_reg24_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg24[30]),
        .R(SR));
  FDRE \slv_reg24_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg24[31]),
        .R(SR));
  FDRE \slv_reg24_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg24[3]),
        .R(SR));
  FDRE \slv_reg24_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg24[4]),
        .R(SR));
  FDRE \slv_reg24_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg24[5]),
        .R(SR));
  FDRE \slv_reg24_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg24[6]),
        .R(SR));
  FDRE \slv_reg24_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg24[7]),
        .R(SR));
  FDRE \slv_reg24_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg24[8]),
        .R(SR));
  FDRE \slv_reg24_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg24[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg25[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg25[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg25[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg25[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg25[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg25[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg25[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg25[7]_i_1_n_0 ));
  FDRE \slv_reg25_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg25[0]),
        .R(SR));
  FDRE \slv_reg25_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg25[10]),
        .R(SR));
  FDRE \slv_reg25_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg25[11]),
        .R(SR));
  FDRE \slv_reg25_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg25[12]),
        .R(SR));
  FDRE \slv_reg25_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg25[13]),
        .R(SR));
  FDRE \slv_reg25_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg25[14]),
        .R(SR));
  FDRE \slv_reg25_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg25[15]),
        .R(SR));
  FDRE \slv_reg25_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg25[16]),
        .R(SR));
  FDRE \slv_reg25_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg25[17]),
        .R(SR));
  FDRE \slv_reg25_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg25[18]),
        .R(SR));
  FDRE \slv_reg25_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg25[19]),
        .R(SR));
  FDRE \slv_reg25_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg25[1]),
        .R(SR));
  FDRE \slv_reg25_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg25[20]),
        .R(SR));
  FDRE \slv_reg25_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg25[21]),
        .R(SR));
  FDRE \slv_reg25_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg25[22]),
        .R(SR));
  FDRE \slv_reg25_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg25[23]),
        .R(SR));
  FDRE \slv_reg25_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg25[24]),
        .R(SR));
  FDRE \slv_reg25_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg25[25]),
        .R(SR));
  FDRE \slv_reg25_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg25[26]),
        .R(SR));
  FDRE \slv_reg25_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg25[27]),
        .R(SR));
  FDRE \slv_reg25_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg25[28]),
        .R(SR));
  FDRE \slv_reg25_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg25[29]),
        .R(SR));
  FDRE \slv_reg25_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg25[2]),
        .R(SR));
  FDRE \slv_reg25_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg25[30]),
        .R(SR));
  FDRE \slv_reg25_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg25[31]),
        .R(SR));
  FDRE \slv_reg25_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg25[3]),
        .R(SR));
  FDRE \slv_reg25_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg25[4]),
        .R(SR));
  FDRE \slv_reg25_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg25[5]),
        .R(SR));
  FDRE \slv_reg25_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg25[6]),
        .R(SR));
  FDRE \slv_reg25_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg25[7]),
        .R(SR));
  FDRE \slv_reg25_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg25[8]),
        .R(SR));
  FDRE \slv_reg25_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg25[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg26[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg26[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg26[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg26[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg26[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg26[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg26[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg26[7]_i_1_n_0 ));
  FDRE \slv_reg26_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg26[0]),
        .R(SR));
  FDRE \slv_reg26_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg26[10]),
        .R(SR));
  FDRE \slv_reg26_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg26[11]),
        .R(SR));
  FDRE \slv_reg26_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg26[12]),
        .R(SR));
  FDRE \slv_reg26_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg26[13]),
        .R(SR));
  FDRE \slv_reg26_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg26[14]),
        .R(SR));
  FDRE \slv_reg26_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg26[15]),
        .R(SR));
  FDRE \slv_reg26_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg26[16]),
        .R(SR));
  FDRE \slv_reg26_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg26[17]),
        .R(SR));
  FDRE \slv_reg26_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg26[18]),
        .R(SR));
  FDRE \slv_reg26_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg26[19]),
        .R(SR));
  FDRE \slv_reg26_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg26[1]),
        .R(SR));
  FDRE \slv_reg26_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg26[20]),
        .R(SR));
  FDRE \slv_reg26_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg26[21]),
        .R(SR));
  FDRE \slv_reg26_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg26[22]),
        .R(SR));
  FDRE \slv_reg26_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg26[23]),
        .R(SR));
  FDRE \slv_reg26_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg26[24]),
        .R(SR));
  FDRE \slv_reg26_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg26[25]),
        .R(SR));
  FDRE \slv_reg26_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg26[26]),
        .R(SR));
  FDRE \slv_reg26_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg26[27]),
        .R(SR));
  FDRE \slv_reg26_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg26[28]),
        .R(SR));
  FDRE \slv_reg26_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg26[29]),
        .R(SR));
  FDRE \slv_reg26_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg26[2]),
        .R(SR));
  FDRE \slv_reg26_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg26[30]),
        .R(SR));
  FDRE \slv_reg26_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg26[31]),
        .R(SR));
  FDRE \slv_reg26_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg26[3]),
        .R(SR));
  FDRE \slv_reg26_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg26[4]),
        .R(SR));
  FDRE \slv_reg26_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg26[5]),
        .R(SR));
  FDRE \slv_reg26_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg26[6]),
        .R(SR));
  FDRE \slv_reg26_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg26[7]),
        .R(SR));
  FDRE \slv_reg26_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg26[8]),
        .R(SR));
  FDRE \slv_reg26_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg26[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg27[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg27[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg27[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg27[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg27[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg27[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg27[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg27[7]_i_1_n_0 ));
  FDRE \slv_reg27_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg27[0]),
        .R(SR));
  FDRE \slv_reg27_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg27[10]),
        .R(SR));
  FDRE \slv_reg27_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg27[11]),
        .R(SR));
  FDRE \slv_reg27_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg27[12]),
        .R(SR));
  FDRE \slv_reg27_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg27[13]),
        .R(SR));
  FDRE \slv_reg27_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg27[14]),
        .R(SR));
  FDRE \slv_reg27_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg27[15]),
        .R(SR));
  FDRE \slv_reg27_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg27[16]),
        .R(SR));
  FDRE \slv_reg27_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg27[17]),
        .R(SR));
  FDRE \slv_reg27_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg27[18]),
        .R(SR));
  FDRE \slv_reg27_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg27[19]),
        .R(SR));
  FDRE \slv_reg27_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg27[1]),
        .R(SR));
  FDRE \slv_reg27_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg27[20]),
        .R(SR));
  FDRE \slv_reg27_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg27[21]),
        .R(SR));
  FDRE \slv_reg27_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg27[22]),
        .R(SR));
  FDRE \slv_reg27_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg27[23]),
        .R(SR));
  FDRE \slv_reg27_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg27[24]),
        .R(SR));
  FDRE \slv_reg27_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg27[25]),
        .R(SR));
  FDRE \slv_reg27_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg27[26]),
        .R(SR));
  FDRE \slv_reg27_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg27[27]),
        .R(SR));
  FDRE \slv_reg27_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg27[28]),
        .R(SR));
  FDRE \slv_reg27_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg27[29]),
        .R(SR));
  FDRE \slv_reg27_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg27[2]),
        .R(SR));
  FDRE \slv_reg27_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg27[30]),
        .R(SR));
  FDRE \slv_reg27_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg27[31]),
        .R(SR));
  FDRE \slv_reg27_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg27[3]),
        .R(SR));
  FDRE \slv_reg27_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg27[4]),
        .R(SR));
  FDRE \slv_reg27_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg27[5]),
        .R(SR));
  FDRE \slv_reg27_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg27[6]),
        .R(SR));
  FDRE \slv_reg27_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg27[7]),
        .R(SR));
  FDRE \slv_reg27_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg27[8]),
        .R(SR));
  FDRE \slv_reg27_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg27[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg28[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg28[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg28[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg28[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg28[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg28[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg28[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg28[7]_i_1_n_0 ));
  FDRE \slv_reg28_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg28[0]),
        .R(SR));
  FDRE \slv_reg28_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg28[10]),
        .R(SR));
  FDRE \slv_reg28_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg28[11]),
        .R(SR));
  FDRE \slv_reg28_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg28[12]),
        .R(SR));
  FDRE \slv_reg28_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg28[13]),
        .R(SR));
  FDRE \slv_reg28_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg28[14]),
        .R(SR));
  FDRE \slv_reg28_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg28[15]),
        .R(SR));
  FDRE \slv_reg28_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg28[16]),
        .R(SR));
  FDRE \slv_reg28_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg28[17]),
        .R(SR));
  FDRE \slv_reg28_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg28[18]),
        .R(SR));
  FDRE \slv_reg28_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg28[19]),
        .R(SR));
  FDRE \slv_reg28_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg28[1]),
        .R(SR));
  FDRE \slv_reg28_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg28[20]),
        .R(SR));
  FDRE \slv_reg28_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg28[21]),
        .R(SR));
  FDRE \slv_reg28_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg28[22]),
        .R(SR));
  FDRE \slv_reg28_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg28[23]),
        .R(SR));
  FDRE \slv_reg28_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg28[24]),
        .R(SR));
  FDRE \slv_reg28_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg28[25]),
        .R(SR));
  FDRE \slv_reg28_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg28[26]),
        .R(SR));
  FDRE \slv_reg28_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg28[27]),
        .R(SR));
  FDRE \slv_reg28_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg28[28]),
        .R(SR));
  FDRE \slv_reg28_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg28[29]),
        .R(SR));
  FDRE \slv_reg28_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg28[2]),
        .R(SR));
  FDRE \slv_reg28_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg28[30]),
        .R(SR));
  FDRE \slv_reg28_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg28[31]),
        .R(SR));
  FDRE \slv_reg28_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg28[3]),
        .R(SR));
  FDRE \slv_reg28_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg28[4]),
        .R(SR));
  FDRE \slv_reg28_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg28[5]),
        .R(SR));
  FDRE \slv_reg28_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg28[6]),
        .R(SR));
  FDRE \slv_reg28_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg28[7]),
        .R(SR));
  FDRE \slv_reg28_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg28[8]),
        .R(SR));
  FDRE \slv_reg28_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg28[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg29[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg29[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg29[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg29[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg29[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg29[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg29[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg29[7]_i_1_n_0 ));
  FDRE \slv_reg29_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg29[0]),
        .R(SR));
  FDRE \slv_reg29_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg29[10]),
        .R(SR));
  FDRE \slv_reg29_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg29[11]),
        .R(SR));
  FDRE \slv_reg29_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg29[12]),
        .R(SR));
  FDRE \slv_reg29_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg29[13]),
        .R(SR));
  FDRE \slv_reg29_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg29[14]),
        .R(SR));
  FDRE \slv_reg29_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg29[15]),
        .R(SR));
  FDRE \slv_reg29_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg29[16]),
        .R(SR));
  FDRE \slv_reg29_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg29[17]),
        .R(SR));
  FDRE \slv_reg29_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg29[18]),
        .R(SR));
  FDRE \slv_reg29_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg29[19]),
        .R(SR));
  FDRE \slv_reg29_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg29[1]),
        .R(SR));
  FDRE \slv_reg29_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg29[20]),
        .R(SR));
  FDRE \slv_reg29_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg29[21]),
        .R(SR));
  FDRE \slv_reg29_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg29[22]),
        .R(SR));
  FDRE \slv_reg29_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg29[23]),
        .R(SR));
  FDRE \slv_reg29_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg29[24]),
        .R(SR));
  FDRE \slv_reg29_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg29[25]),
        .R(SR));
  FDRE \slv_reg29_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg29[26]),
        .R(SR));
  FDRE \slv_reg29_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg29[27]),
        .R(SR));
  FDRE \slv_reg29_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg29[28]),
        .R(SR));
  FDRE \slv_reg29_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg29[29]),
        .R(SR));
  FDRE \slv_reg29_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg29[2]),
        .R(SR));
  FDRE \slv_reg29_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg29[30]),
        .R(SR));
  FDRE \slv_reg29_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg29[31]),
        .R(SR));
  FDRE \slv_reg29_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg29[3]),
        .R(SR));
  FDRE \slv_reg29_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg29[4]),
        .R(SR));
  FDRE \slv_reg29_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg29[5]),
        .R(SR));
  FDRE \slv_reg29_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg29[6]),
        .R(SR));
  FDRE \slv_reg29_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg29[7]),
        .R(SR));
  FDRE \slv_reg29_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg29[8]),
        .R(SR));
  FDRE \slv_reg29_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg29[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg2[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg2[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg2[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg2[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(SR));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(SR));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(SR));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(SR));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(SR));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(SR));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(SR));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(SR));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(SR));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(SR));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(SR));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(SR));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(SR));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(SR));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(SR));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(SR));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(SR));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(SR));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(SR));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(SR));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(SR));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(SR));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(SR));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(SR));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(SR));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(SR));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(SR));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(SR));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(SR));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(SR));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(SR));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg30[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg30[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg30[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg30[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg30[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg30[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg30[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg30[7]_i_1_n_0 ));
  FDRE \slv_reg30_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg30[0]),
        .R(SR));
  FDRE \slv_reg30_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg30[10]),
        .R(SR));
  FDRE \slv_reg30_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg30[11]),
        .R(SR));
  FDRE \slv_reg30_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg30[12]),
        .R(SR));
  FDRE \slv_reg30_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg30[13]),
        .R(SR));
  FDRE \slv_reg30_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg30[14]),
        .R(SR));
  FDRE \slv_reg30_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg30[15]),
        .R(SR));
  FDRE \slv_reg30_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg30[16]),
        .R(SR));
  FDRE \slv_reg30_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg30[17]),
        .R(SR));
  FDRE \slv_reg30_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg30[18]),
        .R(SR));
  FDRE \slv_reg30_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg30[19]),
        .R(SR));
  FDRE \slv_reg30_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg30[1]),
        .R(SR));
  FDRE \slv_reg30_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg30[20]),
        .R(SR));
  FDRE \slv_reg30_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg30[21]),
        .R(SR));
  FDRE \slv_reg30_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg30[22]),
        .R(SR));
  FDRE \slv_reg30_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg30[23]),
        .R(SR));
  FDRE \slv_reg30_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg30[24]),
        .R(SR));
  FDRE \slv_reg30_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg30[25]),
        .R(SR));
  FDRE \slv_reg30_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg30[26]),
        .R(SR));
  FDRE \slv_reg30_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg30[27]),
        .R(SR));
  FDRE \slv_reg30_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg30[28]),
        .R(SR));
  FDRE \slv_reg30_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg30[29]),
        .R(SR));
  FDRE \slv_reg30_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg30[2]),
        .R(SR));
  FDRE \slv_reg30_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg30[30]),
        .R(SR));
  FDRE \slv_reg30_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg30[31]),
        .R(SR));
  FDRE \slv_reg30_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg30[3]),
        .R(SR));
  FDRE \slv_reg30_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg30[4]),
        .R(SR));
  FDRE \slv_reg30_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg30[5]),
        .R(SR));
  FDRE \slv_reg30_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg30[6]),
        .R(SR));
  FDRE \slv_reg30_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg30[7]),
        .R(SR));
  FDRE \slv_reg30_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg30[8]),
        .R(SR));
  FDRE \slv_reg30_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg30[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg31[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg31[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg31[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg31[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg31[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg31[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg31[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg31[7]_i_1_n_0 ));
  FDRE \slv_reg31_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg31[0]),
        .R(SR));
  FDRE \slv_reg31_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg31[10]),
        .R(SR));
  FDRE \slv_reg31_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg31[11]),
        .R(SR));
  FDRE \slv_reg31_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg31[12]),
        .R(SR));
  FDRE \slv_reg31_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg31[13]),
        .R(SR));
  FDRE \slv_reg31_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg31[14]),
        .R(SR));
  FDRE \slv_reg31_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg31[15]),
        .R(SR));
  FDRE \slv_reg31_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg31[16]),
        .R(SR));
  FDRE \slv_reg31_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg31[17]),
        .R(SR));
  FDRE \slv_reg31_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg31[18]),
        .R(SR));
  FDRE \slv_reg31_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg31[19]),
        .R(SR));
  FDRE \slv_reg31_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg31[1]),
        .R(SR));
  FDRE \slv_reg31_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg31[20]),
        .R(SR));
  FDRE \slv_reg31_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg31[21]),
        .R(SR));
  FDRE \slv_reg31_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg31[22]),
        .R(SR));
  FDRE \slv_reg31_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg31[23]),
        .R(SR));
  FDRE \slv_reg31_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg31[24]),
        .R(SR));
  FDRE \slv_reg31_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg31[25]),
        .R(SR));
  FDRE \slv_reg31_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg31[26]),
        .R(SR));
  FDRE \slv_reg31_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg31[27]),
        .R(SR));
  FDRE \slv_reg31_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg31[28]),
        .R(SR));
  FDRE \slv_reg31_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg31[29]),
        .R(SR));
  FDRE \slv_reg31_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg31[2]),
        .R(SR));
  FDRE \slv_reg31_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg31[30]),
        .R(SR));
  FDRE \slv_reg31_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg31[31]),
        .R(SR));
  FDRE \slv_reg31_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg31[3]),
        .R(SR));
  FDRE \slv_reg31_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg31[4]),
        .R(SR));
  FDRE \slv_reg31_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg31[5]),
        .R(SR));
  FDRE \slv_reg31_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg31[6]),
        .R(SR));
  FDRE \slv_reg31_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg31[7]),
        .R(SR));
  FDRE \slv_reg31_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg31[8]),
        .R(SR));
  FDRE \slv_reg31_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg31[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg3[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg3[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg3[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg3[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(SR));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(SR));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(SR));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(SR));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(SR));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(SR));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(SR));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(SR));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(SR));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(SR));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(SR));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(SR));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(SR));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(SR));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(SR));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(SR));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(SR));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(SR));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(SR));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(SR));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(SR));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(SR));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(SR));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(SR));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(SR));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(SR));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(SR));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(SR));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(SR));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(SR));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(SR));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg4[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg4[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg4[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg4[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg4[0]),
        .R(SR));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4[10]),
        .R(SR));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4[11]),
        .R(SR));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4[12]),
        .R(SR));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4[13]),
        .R(SR));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4[14]),
        .R(SR));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4[15]),
        .R(SR));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4[16]),
        .R(SR));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4[17]),
        .R(SR));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4[18]),
        .R(SR));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4[19]),
        .R(SR));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg4[1]),
        .R(SR));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4[20]),
        .R(SR));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4[21]),
        .R(SR));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4[22]),
        .R(SR));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4[23]),
        .R(SR));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(SR));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(SR));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(SR));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(SR));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(SR));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(SR));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg4[2]),
        .R(SR));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(SR));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(SR));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg4[3]),
        .R(SR));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg4[4]),
        .R(SR));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg4[5]),
        .R(SR));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(SR));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(SR));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4[8]),
        .R(SR));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg5[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg5[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg5[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg5[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg5[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg5[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \slv_reg5[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg5[7]_i_1_n_0 ));
  FDRE \slv_reg5_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg5[0]),
        .R(SR));
  FDRE \slv_reg5_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg5[10]),
        .R(SR));
  FDRE \slv_reg5_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg5[11]),
        .R(SR));
  FDRE \slv_reg5_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg5[12]),
        .R(SR));
  FDRE \slv_reg5_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg5[13]),
        .R(SR));
  FDRE \slv_reg5_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg5[14]),
        .R(SR));
  FDRE \slv_reg5_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg5[15]),
        .R(SR));
  FDRE \slv_reg5_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg5[16]),
        .R(SR));
  FDRE \slv_reg5_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg5[17]),
        .R(SR));
  FDRE \slv_reg5_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg5[18]),
        .R(SR));
  FDRE \slv_reg5_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg5[19]),
        .R(SR));
  FDRE \slv_reg5_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg5[1]),
        .R(SR));
  FDRE \slv_reg5_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg5[20]),
        .R(SR));
  FDRE \slv_reg5_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg5[21]),
        .R(SR));
  FDRE \slv_reg5_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg5[22]),
        .R(SR));
  FDRE \slv_reg5_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg5[23]),
        .R(SR));
  FDRE \slv_reg5_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg5[24]),
        .R(SR));
  FDRE \slv_reg5_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg5[25]),
        .R(SR));
  FDRE \slv_reg5_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg5[26]),
        .R(SR));
  FDRE \slv_reg5_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg5[27]),
        .R(SR));
  FDRE \slv_reg5_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg5[28]),
        .R(SR));
  FDRE \slv_reg5_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg5[29]),
        .R(SR));
  FDRE \slv_reg5_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg5[2]),
        .R(SR));
  FDRE \slv_reg5_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg5[30]),
        .R(SR));
  FDRE \slv_reg5_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg5[31]),
        .R(SR));
  FDRE \slv_reg5_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg5[3]),
        .R(SR));
  FDRE \slv_reg5_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg5[4]),
        .R(SR));
  FDRE \slv_reg5_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg5[5]),
        .R(SR));
  FDRE \slv_reg5_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg5[6]),
        .R(SR));
  FDRE \slv_reg5_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg5[7]),
        .R(SR));
  FDRE \slv_reg5_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg5[8]),
        .R(SR));
  FDRE \slv_reg5_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg5[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg6[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg6[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg6[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg6[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg6[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg6[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg6[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg6[7]_i_1_n_0 ));
  FDRE \slv_reg6_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg6[0]),
        .R(SR));
  FDRE \slv_reg6_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg6[10]),
        .R(SR));
  FDRE \slv_reg6_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg6[11]),
        .R(SR));
  FDRE \slv_reg6_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg6[12]),
        .R(SR));
  FDRE \slv_reg6_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg6[13]),
        .R(SR));
  FDRE \slv_reg6_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg6[14]),
        .R(SR));
  FDRE \slv_reg6_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg6[15]),
        .R(SR));
  FDRE \slv_reg6_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg6[16]),
        .R(SR));
  FDRE \slv_reg6_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg6[17]),
        .R(SR));
  FDRE \slv_reg6_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg6[18]),
        .R(SR));
  FDRE \slv_reg6_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg6[19]),
        .R(SR));
  FDRE \slv_reg6_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg6[1]),
        .R(SR));
  FDRE \slv_reg6_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg6[20]),
        .R(SR));
  FDRE \slv_reg6_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg6[21]),
        .R(SR));
  FDRE \slv_reg6_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg6[22]),
        .R(SR));
  FDRE \slv_reg6_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg6[23]),
        .R(SR));
  FDRE \slv_reg6_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg6[24]),
        .R(SR));
  FDRE \slv_reg6_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg6[25]),
        .R(SR));
  FDRE \slv_reg6_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg6[26]),
        .R(SR));
  FDRE \slv_reg6_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg6[27]),
        .R(SR));
  FDRE \slv_reg6_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg6[28]),
        .R(SR));
  FDRE \slv_reg6_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg6[29]),
        .R(SR));
  FDRE \slv_reg6_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg6[2]),
        .R(SR));
  FDRE \slv_reg6_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg6[30]),
        .R(SR));
  FDRE \slv_reg6_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg6[31]),
        .R(SR));
  FDRE \slv_reg6_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg6[3]),
        .R(SR));
  FDRE \slv_reg6_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg6[4]),
        .R(SR));
  FDRE \slv_reg6_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg6[5]),
        .R(SR));
  FDRE \slv_reg6_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg6[6]),
        .R(SR));
  FDRE \slv_reg6_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg6[7]),
        .R(SR));
  FDRE \slv_reg6_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg6[8]),
        .R(SR));
  FDRE \slv_reg6_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg6[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg7[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg7[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg7[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg7[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg7[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg7[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \slv_reg7[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg7[7]_i_1_n_0 ));
  FDRE \slv_reg7_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg7[0]),
        .R(SR));
  FDRE \slv_reg7_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg7[10]),
        .R(SR));
  FDRE \slv_reg7_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg7[11]),
        .R(SR));
  FDRE \slv_reg7_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg7[12]),
        .R(SR));
  FDRE \slv_reg7_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg7[13]),
        .R(SR));
  FDRE \slv_reg7_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg7[14]),
        .R(SR));
  FDRE \slv_reg7_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg7[15]),
        .R(SR));
  FDRE \slv_reg7_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg7[16]),
        .R(SR));
  FDRE \slv_reg7_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg7[17]),
        .R(SR));
  FDRE \slv_reg7_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg7[18]),
        .R(SR));
  FDRE \slv_reg7_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg7[19]),
        .R(SR));
  FDRE \slv_reg7_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg7[1]),
        .R(SR));
  FDRE \slv_reg7_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg7[20]),
        .R(SR));
  FDRE \slv_reg7_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg7[21]),
        .R(SR));
  FDRE \slv_reg7_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg7[22]),
        .R(SR));
  FDRE \slv_reg7_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg7[23]),
        .R(SR));
  FDRE \slv_reg7_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg7[24]),
        .R(SR));
  FDRE \slv_reg7_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg7[25]),
        .R(SR));
  FDRE \slv_reg7_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg7[26]),
        .R(SR));
  FDRE \slv_reg7_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg7[27]),
        .R(SR));
  FDRE \slv_reg7_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg7[28]),
        .R(SR));
  FDRE \slv_reg7_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg7[29]),
        .R(SR));
  FDRE \slv_reg7_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg7[2]),
        .R(SR));
  FDRE \slv_reg7_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg7[30]),
        .R(SR));
  FDRE \slv_reg7_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg7[31]),
        .R(SR));
  FDRE \slv_reg7_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg7[3]),
        .R(SR));
  FDRE \slv_reg7_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg7[4]),
        .R(SR));
  FDRE \slv_reg7_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg7[5]),
        .R(SR));
  FDRE \slv_reg7_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg7[6]),
        .R(SR));
  FDRE \slv_reg7_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg7[7]),
        .R(SR));
  FDRE \slv_reg7_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg7[8]),
        .R(SR));
  FDRE \slv_reg7_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg7[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg8[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg8[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg8[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg8[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg8[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg8[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg8[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg8[7]_i_1_n_0 ));
  FDRE \slv_reg8_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg8[0]),
        .R(SR));
  FDRE \slv_reg8_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg8[10]),
        .R(SR));
  FDRE \slv_reg8_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg8[11]),
        .R(SR));
  FDRE \slv_reg8_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg8[12]),
        .R(SR));
  FDRE \slv_reg8_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg8[13]),
        .R(SR));
  FDRE \slv_reg8_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg8[14]),
        .R(SR));
  FDRE \slv_reg8_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg8[15]),
        .R(SR));
  FDRE \slv_reg8_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg8[16]),
        .R(SR));
  FDRE \slv_reg8_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg8[17]),
        .R(SR));
  FDRE \slv_reg8_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg8[18]),
        .R(SR));
  FDRE \slv_reg8_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg8[19]),
        .R(SR));
  FDRE \slv_reg8_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg8[1]),
        .R(SR));
  FDRE \slv_reg8_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg8[20]),
        .R(SR));
  FDRE \slv_reg8_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg8[21]),
        .R(SR));
  FDRE \slv_reg8_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg8[22]),
        .R(SR));
  FDRE \slv_reg8_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg8[23]),
        .R(SR));
  FDRE \slv_reg8_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg8[24]),
        .R(SR));
  FDRE \slv_reg8_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg8[25]),
        .R(SR));
  FDRE \slv_reg8_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg8[26]),
        .R(SR));
  FDRE \slv_reg8_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg8[27]),
        .R(SR));
  FDRE \slv_reg8_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg8[28]),
        .R(SR));
  FDRE \slv_reg8_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg8[29]),
        .R(SR));
  FDRE \slv_reg8_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg8[2]),
        .R(SR));
  FDRE \slv_reg8_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg8[30]),
        .R(SR));
  FDRE \slv_reg8_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg8[31]),
        .R(SR));
  FDRE \slv_reg8_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg8[3]),
        .R(SR));
  FDRE \slv_reg8_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg8[4]),
        .R(SR));
  FDRE \slv_reg8_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg8[5]),
        .R(SR));
  FDRE \slv_reg8_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg8[6]),
        .R(SR));
  FDRE \slv_reg8_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg8[7]),
        .R(SR));
  FDRE \slv_reg8_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg8[8]),
        .R(SR));
  FDRE \slv_reg8_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg8[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg9[15]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg9[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg9[23]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg9[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg9[31]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg9[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg9[7]_i_1 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg1[31]_i_2_n_0 ),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\slv_reg9[7]_i_1_n_0 ));
  FDRE \slv_reg9_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg9[0]),
        .R(SR));
  FDRE \slv_reg9_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg9[10]),
        .R(SR));
  FDRE \slv_reg9_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg9[11]),
        .R(SR));
  FDRE \slv_reg9_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg9[12]),
        .R(SR));
  FDRE \slv_reg9_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg9[13]),
        .R(SR));
  FDRE \slv_reg9_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg9[14]),
        .R(SR));
  FDRE \slv_reg9_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg9[15]),
        .R(SR));
  FDRE \slv_reg9_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg9[16]),
        .R(SR));
  FDRE \slv_reg9_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg9[17]),
        .R(SR));
  FDRE \slv_reg9_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg9[18]),
        .R(SR));
  FDRE \slv_reg9_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg9[19]),
        .R(SR));
  FDRE \slv_reg9_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg9[1]),
        .R(SR));
  FDRE \slv_reg9_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg9[20]),
        .R(SR));
  FDRE \slv_reg9_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg9[21]),
        .R(SR));
  FDRE \slv_reg9_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg9[22]),
        .R(SR));
  FDRE \slv_reg9_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg9[23]),
        .R(SR));
  FDRE \slv_reg9_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg9[24]),
        .R(SR));
  FDRE \slv_reg9_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg9[25]),
        .R(SR));
  FDRE \slv_reg9_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg9[26]),
        .R(SR));
  FDRE \slv_reg9_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg9[27]),
        .R(SR));
  FDRE \slv_reg9_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg9[28]),
        .R(SR));
  FDRE \slv_reg9_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg9[29]),
        .R(SR));
  FDRE \slv_reg9_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg9[2]),
        .R(SR));
  FDRE \slv_reg9_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg9[30]),
        .R(SR));
  FDRE \slv_reg9_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg9[31]),
        .R(SR));
  FDRE \slv_reg9_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg9[3]),
        .R(SR));
  FDRE \slv_reg9_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg9[4]),
        .R(SR));
  FDRE \slv_reg9_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg9[5]),
        .R(SR));
  FDRE \slv_reg9_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg9[6]),
        .R(SR));
  FDRE \slv_reg9_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg9[7]),
        .R(SR));
  FDRE \slv_reg9_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg9[8]),
        .R(SR));
  FDRE \slv_reg9_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg9[9]),
        .R(SR));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample_counter
   (Q,
    sw,
    \state_reg[1] ,
    E,
    clk);
  output [9:0]Q;
  output [0:0]sw;
  input [0:0]\state_reg[1] ;
  input [0:0]E;
  input clk;

  wire [0:0]E;
  wire [9:0]Q;
  wire clk;
  wire [9:0]plusOp__0;
  wire \processQ[9]_i_4_n_0 ;
  wire [0:0]\state_reg[1] ;
  wire [0:0]sw;

  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    i__i_1
       (.I0(Q[9]),
        .I1(Q[7]),
        .I2(\processQ[9]_i_4_n_0 ),
        .I3(Q[6]),
        .I4(Q[8]),
        .O(sw));
  LUT1 #(
    .INIT(2'h1)) 
    \processQ[0]_i_1 
       (.I0(Q[0]),
        .O(plusOp__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \processQ[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(plusOp__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \processQ[2]_i_1 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(plusOp__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processQ[3]_i_1 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(plusOp__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[4]_i_1 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[3]),
        .O(plusOp__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processQ[5]_i_1 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(plusOp__0[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \processQ[6]_i_1 
       (.I0(Q[6]),
        .I1(\processQ[9]_i_4_n_0 ),
        .O(plusOp__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \processQ[7]_i_1 
       (.I0(Q[7]),
        .I1(\processQ[9]_i_4_n_0 ),
        .I2(Q[6]),
        .O(plusOp__0[7]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processQ[8]_i_1 
       (.I0(Q[8]),
        .I1(Q[6]),
        .I2(\processQ[9]_i_4_n_0 ),
        .I3(Q[7]),
        .O(plusOp__0[8]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[9]_i_3 
       (.I0(Q[9]),
        .I1(Q[7]),
        .I2(\processQ[9]_i_4_n_0 ),
        .I3(Q[6]),
        .I4(Q[8]),
        .O(plusOp__0[9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \processQ[9]_i_4 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\processQ[9]_i_4_n_0 ));
  FDRE \processQ_reg[0] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[0]),
        .Q(Q[0]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[1] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[1]),
        .Q(Q[1]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[2] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[2]),
        .Q(Q[2]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[3] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[3]),
        .Q(Q[3]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[4] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[4]),
        .Q(Q[4]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[5] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[5]),
        .Q(Q[5]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[6] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[6]),
        .Q(Q[6]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[7] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[7]),
        .Q(Q[7]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[8] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[8]),
        .Q(Q[8]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[9] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[9]),
        .Q(Q[9]),
        .R(\state_reg[1] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_scopeFace
   (\dc_bias_reg[1] ,
    \dc_bias_reg[1]_0 ,
    CO,
    \dc_bias_reg[1]_1 ,
    \dc_bias_reg[1]_2 ,
    \dc_bias_reg[1]_3 ,
    \dc_bias_reg[1]_4 ,
    \dc_bias_reg[1]_5 ,
    \dc_bias_reg[1]_6 ,
    \dc_bias_reg[1]_7 ,
    \dc_bias_reg[1]_8 ,
    \dc_bias_reg[1]_9 ,
    \dc_bias_reg[1]_10 ,
    \dc_bias_reg[1]_11 ,
    \dc_bias_reg[1]_12 ,
    \dc_bias_reg[1]_13 ,
    \dc_bias_reg[1]_14 ,
    \dc_bias_reg[1]_15 ,
    \dc_bias_reg[1]_16 ,
    \dc_bias_reg[1]_17 ,
    \dc_bias_reg[1]_18 ,
    \dc_bias_reg[1]_19 ,
    \dc_bias_reg[1]_20 ,
    \dc_bias_reg[1]_21 ,
    \dc_bias_reg[1]_22 ,
    \dc_bias_reg[1]_23 ,
    \dc_bias_reg[1]_24 ,
    \dc_bias_reg[1]_25 ,
    \dc_bias_reg[1]_26 ,
    \dc_bias_reg[1]_27 ,
    \dc_bias_reg[1]_28 ,
    \dc_bias_reg[1]_29 ,
    \dc_bias_reg[1]_30 ,
    \dc_bias_reg[1]_31 ,
    \dc_bias_reg[1]_32 ,
    \dc_bias_reg[1]_33 ,
    \dc_bias_reg[1]_34 ,
    \dc_bias_reg[1]_35 ,
    \dc_bias_reg[1]_36 ,
    DI,
    S,
    \processQ_reg[9] ,
    \trigger_volt_s_reg[9] ,
    \processQ_reg[7] ,
    \trigger_volt_s_reg[7] ,
    \processQ_reg[9]_0 ,
    \trigger_volt_s_reg[9]_0 ,
    \processQ_reg[7]_0 ,
    \trigger_volt_s_reg[7]_0 ,
    \processQ_reg[9]_1 ,
    \trigger_volt_s_reg[9]_1 ,
    \processQ_reg[7]_1 ,
    \trigger_volt_s_reg[7]_1 ,
    \processQ_reg[9]_2 ,
    \trigger_volt_s_reg[9]_2 ,
    \trigger_volt_s_reg[7]_2 ,
    \processQ_reg[7]_2 ,
    \trigger_volt_s_reg[7]_3 ,
    \processQ_reg[9]_3 ,
    \trigger_volt_s_reg[9]_3 ,
    \processQ_reg[7]_3 ,
    \trigger_volt_s_reg[7]_4 ,
    \processQ_reg[9]_4 ,
    \trigger_volt_s_reg[9]_4 ,
    \processQ_reg[7]_4 ,
    \trigger_time_s_reg[7] ,
    \processQ_reg[9]_5 ,
    \trigger_time_s_reg[9] ,
    \processQ_reg[7]_5 ,
    \trigger_time_s_reg[7]_0 ,
    \processQ_reg[9]_6 ,
    \trigger_time_s_reg[9]_0 ,
    \processQ_reg[7]_6 ,
    \trigger_time_s_reg[7]_1 ,
    \processQ_reg[9]_7 ,
    \trigger_time_s_reg[9]_1 ,
    \processQ_reg[7]_7 ,
    \trigger_time_s_reg[7]_2 ,
    \processQ_reg[9]_8 ,
    \trigger_time_s_reg[9]_2 ,
    \trigger_time_s_reg[8] ,
    \processQ_reg[7]_8 ,
    \trigger_time_s_reg[7]_3 ,
    \processQ_reg[9]_9 ,
    \trigger_time_s_reg[9]_3 ,
    \processQ_reg[7]_9 ,
    \trigger_time_s_reg[7]_4 ,
    \processQ_reg[9]_10 ,
    \trigger_time_s_reg[9]_4 ,
    Q,
    \trigger_volt_s_reg[6] ,
    \processQ_reg[1] );
  output [0:0]\dc_bias_reg[1] ;
  output [0:0]\dc_bias_reg[1]_0 ;
  output [0:0]CO;
  output [0:0]\dc_bias_reg[1]_1 ;
  output [0:0]\dc_bias_reg[1]_2 ;
  output [0:0]\dc_bias_reg[1]_3 ;
  output [0:0]\dc_bias_reg[1]_4 ;
  output [0:0]\dc_bias_reg[1]_5 ;
  output [0:0]\dc_bias_reg[1]_6 ;
  output \dc_bias_reg[1]_7 ;
  output \dc_bias_reg[1]_8 ;
  output \dc_bias_reg[1]_9 ;
  output \dc_bias_reg[1]_10 ;
  output \dc_bias_reg[1]_11 ;
  output \dc_bias_reg[1]_12 ;
  output \dc_bias_reg[1]_13 ;
  output \dc_bias_reg[1]_14 ;
  output \dc_bias_reg[1]_15 ;
  output \dc_bias_reg[1]_16 ;
  output \dc_bias_reg[1]_17 ;
  output \dc_bias_reg[1]_18 ;
  output \dc_bias_reg[1]_19 ;
  output \dc_bias_reg[1]_20 ;
  output \dc_bias_reg[1]_21 ;
  output \dc_bias_reg[1]_22 ;
  output \dc_bias_reg[1]_23 ;
  output \dc_bias_reg[1]_24 ;
  output \dc_bias_reg[1]_25 ;
  output \dc_bias_reg[1]_26 ;
  output \dc_bias_reg[1]_27 ;
  output \dc_bias_reg[1]_28 ;
  output \dc_bias_reg[1]_29 ;
  output \dc_bias_reg[1]_30 ;
  output \dc_bias_reg[1]_31 ;
  output \dc_bias_reg[1]_32 ;
  output \dc_bias_reg[1]_33 ;
  output \dc_bias_reg[1]_34 ;
  output \dc_bias_reg[1]_35 ;
  output \dc_bias_reg[1]_36 ;
  input [3:0]DI;
  input [3:0]S;
  input [0:0]\processQ_reg[9] ;
  input [0:0]\trigger_volt_s_reg[9] ;
  input [3:0]\processQ_reg[7] ;
  input [3:0]\trigger_volt_s_reg[7] ;
  input [0:0]\processQ_reg[9]_0 ;
  input [0:0]\trigger_volt_s_reg[9]_0 ;
  input [3:0]\processQ_reg[7]_0 ;
  input [3:0]\trigger_volt_s_reg[7]_0 ;
  input [0:0]\processQ_reg[9]_1 ;
  input [0:0]\trigger_volt_s_reg[9]_1 ;
  input [3:0]\processQ_reg[7]_1 ;
  input [3:0]\trigger_volt_s_reg[7]_1 ;
  input [0:0]\processQ_reg[9]_2 ;
  input [0:0]\trigger_volt_s_reg[9]_2 ;
  input [3:0]\trigger_volt_s_reg[7]_2 ;
  input [3:0]\processQ_reg[7]_2 ;
  input [3:0]\trigger_volt_s_reg[7]_3 ;
  input [0:0]\processQ_reg[9]_3 ;
  input [0:0]\trigger_volt_s_reg[9]_3 ;
  input [3:0]\processQ_reg[7]_3 ;
  input [3:0]\trigger_volt_s_reg[7]_4 ;
  input [0:0]\processQ_reg[9]_4 ;
  input [0:0]\trigger_volt_s_reg[9]_4 ;
  input [3:0]\processQ_reg[7]_4 ;
  input [3:0]\trigger_time_s_reg[7] ;
  input [0:0]\processQ_reg[9]_5 ;
  input [0:0]\trigger_time_s_reg[9] ;
  input [3:0]\processQ_reg[7]_5 ;
  input [3:0]\trigger_time_s_reg[7]_0 ;
  input [0:0]\processQ_reg[9]_6 ;
  input [0:0]\trigger_time_s_reg[9]_0 ;
  input [3:0]\processQ_reg[7]_6 ;
  input [3:0]\trigger_time_s_reg[7]_1 ;
  input [0:0]\processQ_reg[9]_7 ;
  input [0:0]\trigger_time_s_reg[9]_1 ;
  input [3:0]\processQ_reg[7]_7 ;
  input [3:0]\trigger_time_s_reg[7]_2 ;
  input [0:0]\processQ_reg[9]_8 ;
  input [0:0]\trigger_time_s_reg[9]_2 ;
  input [3:0]\trigger_time_s_reg[8] ;
  input [3:0]\processQ_reg[7]_8 ;
  input [3:0]\trigger_time_s_reg[7]_3 ;
  input [0:0]\processQ_reg[9]_9 ;
  input [0:0]\trigger_time_s_reg[9]_3 ;
  input [3:0]\processQ_reg[7]_9 ;
  input [3:0]\trigger_time_s_reg[7]_4 ;
  input [0:0]\processQ_reg[9]_10 ;
  input [0:0]\trigger_time_s_reg[9]_4 ;
  input [7:0]Q;
  input [6:0]\trigger_volt_s_reg[6] ;
  input [1:0]\processQ_reg[1] ;

  wire [0:0]CO;
  wire [3:0]DI;
  wire [7:0]Q;
  wire [3:0]S;
  wire \dc_bias[3]_i_51_n_0 ;
  wire \dc_bias[3]_i_52_n_0 ;
  wire [0:0]\dc_bias_reg[1] ;
  wire [0:0]\dc_bias_reg[1]_0 ;
  wire [0:0]\dc_bias_reg[1]_1 ;
  wire \dc_bias_reg[1]_10 ;
  wire \dc_bias_reg[1]_11 ;
  wire \dc_bias_reg[1]_12 ;
  wire \dc_bias_reg[1]_13 ;
  wire \dc_bias_reg[1]_14 ;
  wire \dc_bias_reg[1]_15 ;
  wire \dc_bias_reg[1]_16 ;
  wire \dc_bias_reg[1]_17 ;
  wire \dc_bias_reg[1]_18 ;
  wire \dc_bias_reg[1]_19 ;
  wire [0:0]\dc_bias_reg[1]_2 ;
  wire \dc_bias_reg[1]_20 ;
  wire \dc_bias_reg[1]_21 ;
  wire \dc_bias_reg[1]_22 ;
  wire \dc_bias_reg[1]_23 ;
  wire \dc_bias_reg[1]_24 ;
  wire \dc_bias_reg[1]_25 ;
  wire \dc_bias_reg[1]_26 ;
  wire \dc_bias_reg[1]_27 ;
  wire \dc_bias_reg[1]_28 ;
  wire \dc_bias_reg[1]_29 ;
  wire [0:0]\dc_bias_reg[1]_3 ;
  wire \dc_bias_reg[1]_30 ;
  wire \dc_bias_reg[1]_31 ;
  wire \dc_bias_reg[1]_32 ;
  wire \dc_bias_reg[1]_33 ;
  wire \dc_bias_reg[1]_34 ;
  wire \dc_bias_reg[1]_35 ;
  wire \dc_bias_reg[1]_36 ;
  wire [0:0]\dc_bias_reg[1]_4 ;
  wire [0:0]\dc_bias_reg[1]_5 ;
  wire [0:0]\dc_bias_reg[1]_6 ;
  wire \dc_bias_reg[1]_7 ;
  wire \dc_bias_reg[1]_8 ;
  wire \dc_bias_reg[1]_9 ;
  wire i__carry__0_i_4_n_0;
  wire pixel_color3;
  wire \pixel_color3_inferred__0/i__carry_n_1 ;
  wire \pixel_color3_inferred__0/i__carry_n_2 ;
  wire \pixel_color3_inferred__0/i__carry_n_3 ;
  wire \pixel_color3_inferred__1/i__carry_n_1 ;
  wire \pixel_color3_inferred__1/i__carry_n_2 ;
  wire \pixel_color3_inferred__1/i__carry_n_3 ;
  wire pixel_color49_in;
  wire pixel_color4_carry__0_i_4_n_0;
  wire pixel_color4_carry_n_0;
  wire pixel_color4_carry_n_1;
  wire pixel_color4_carry_n_2;
  wire pixel_color4_carry_n_3;
  wire \pixel_color4_inferred__0/i__carry_n_0 ;
  wire \pixel_color4_inferred__0/i__carry_n_1 ;
  wire \pixel_color4_inferred__0/i__carry_n_2 ;
  wire \pixel_color4_inferred__0/i__carry_n_3 ;
  wire pixel_color513_in;
  wire pixel_color58_in;
  wire pixel_color5_carry_n_0;
  wire pixel_color5_carry_n_1;
  wire pixel_color5_carry_n_2;
  wire pixel_color5_carry_n_3;
  wire \pixel_color5_inferred__0/i__carry_n_0 ;
  wire \pixel_color5_inferred__0/i__carry_n_1 ;
  wire \pixel_color5_inferred__0/i__carry_n_2 ;
  wire \pixel_color5_inferred__0/i__carry_n_3 ;
  wire \pixel_color5_inferred__1/i__carry_n_0 ;
  wire \pixel_color5_inferred__1/i__carry_n_1 ;
  wire \pixel_color5_inferred__1/i__carry_n_2 ;
  wire \pixel_color5_inferred__1/i__carry_n_3 ;
  wire \pixel_color5_inferred__2/i__carry_n_0 ;
  wire \pixel_color5_inferred__2/i__carry_n_1 ;
  wire \pixel_color5_inferred__2/i__carry_n_2 ;
  wire \pixel_color5_inferred__2/i__carry_n_3 ;
  wire \pixel_color5_inferred__3/i__carry_n_0 ;
  wire \pixel_color5_inferred__3/i__carry_n_1 ;
  wire \pixel_color5_inferred__3/i__carry_n_2 ;
  wire \pixel_color5_inferred__3/i__carry_n_3 ;
  wire \pixel_color5_inferred__4/i__carry_n_0 ;
  wire \pixel_color5_inferred__4/i__carry_n_1 ;
  wire \pixel_color5_inferred__4/i__carry_n_2 ;
  wire \pixel_color5_inferred__4/i__carry_n_3 ;
  wire pixel_color612_in;
  wire \pixel_color6_inferred__1/i__carry_n_0 ;
  wire \pixel_color6_inferred__1/i__carry_n_1 ;
  wire \pixel_color6_inferred__1/i__carry_n_2 ;
  wire \pixel_color6_inferred__1/i__carry_n_3 ;
  wire \pixel_color6_inferred__2/i__carry_n_0 ;
  wire \pixel_color6_inferred__2/i__carry_n_1 ;
  wire \pixel_color6_inferred__2/i__carry_n_2 ;
  wire \pixel_color6_inferred__2/i__carry_n_3 ;
  wire \pixel_color6_inferred__3/i__carry_n_0 ;
  wire \pixel_color6_inferred__3/i__carry_n_1 ;
  wire \pixel_color6_inferred__3/i__carry_n_2 ;
  wire \pixel_color6_inferred__3/i__carry_n_3 ;
  wire \pixel_color6_inferred__4/i__carry_n_0 ;
  wire \pixel_color6_inferred__4/i__carry_n_1 ;
  wire \pixel_color6_inferred__4/i__carry_n_2 ;
  wire \pixel_color6_inferred__4/i__carry_n_3 ;
  wire [1:0]\processQ_reg[1] ;
  wire [3:0]\processQ_reg[7] ;
  wire [3:0]\processQ_reg[7]_0 ;
  wire [3:0]\processQ_reg[7]_1 ;
  wire [3:0]\processQ_reg[7]_2 ;
  wire [3:0]\processQ_reg[7]_3 ;
  wire [3:0]\processQ_reg[7]_4 ;
  wire [3:0]\processQ_reg[7]_5 ;
  wire [3:0]\processQ_reg[7]_6 ;
  wire [3:0]\processQ_reg[7]_7 ;
  wire [3:0]\processQ_reg[7]_8 ;
  wire [3:0]\processQ_reg[7]_9 ;
  wire [0:0]\processQ_reg[9] ;
  wire [0:0]\processQ_reg[9]_0 ;
  wire [0:0]\processQ_reg[9]_1 ;
  wire [0:0]\processQ_reg[9]_10 ;
  wire [0:0]\processQ_reg[9]_2 ;
  wire [0:0]\processQ_reg[9]_3 ;
  wire [0:0]\processQ_reg[9]_4 ;
  wire [0:0]\processQ_reg[9]_5 ;
  wire [0:0]\processQ_reg[9]_6 ;
  wire [0:0]\processQ_reg[9]_7 ;
  wire [0:0]\processQ_reg[9]_8 ;
  wire [0:0]\processQ_reg[9]_9 ;
  wire [3:0]\trigger_time_s_reg[7] ;
  wire [3:0]\trigger_time_s_reg[7]_0 ;
  wire [3:0]\trigger_time_s_reg[7]_1 ;
  wire [3:0]\trigger_time_s_reg[7]_2 ;
  wire [3:0]\trigger_time_s_reg[7]_3 ;
  wire [3:0]\trigger_time_s_reg[7]_4 ;
  wire [3:0]\trigger_time_s_reg[8] ;
  wire [0:0]\trigger_time_s_reg[9] ;
  wire [0:0]\trigger_time_s_reg[9]_0 ;
  wire [0:0]\trigger_time_s_reg[9]_1 ;
  wire [0:0]\trigger_time_s_reg[9]_2 ;
  wire [0:0]\trigger_time_s_reg[9]_3 ;
  wire [0:0]\trigger_time_s_reg[9]_4 ;
  wire [6:0]\trigger_volt_s_reg[6] ;
  wire [3:0]\trigger_volt_s_reg[7] ;
  wire [3:0]\trigger_volt_s_reg[7]_0 ;
  wire [3:0]\trigger_volt_s_reg[7]_1 ;
  wire [3:0]\trigger_volt_s_reg[7]_2 ;
  wire [3:0]\trigger_volt_s_reg[7]_3 ;
  wire [3:0]\trigger_volt_s_reg[7]_4 ;
  wire [0:0]\trigger_volt_s_reg[9] ;
  wire [0:0]\trigger_volt_s_reg[9]_0 ;
  wire [0:0]\trigger_volt_s_reg[9]_1 ;
  wire [0:0]\trigger_volt_s_reg[9]_2 ;
  wire [0:0]\trigger_volt_s_reg[9]_3 ;
  wire [0:0]\trigger_volt_s_reg[9]_4 ;
  wire [3:0]\NLW_pixel_color3_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color3_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:0]NLW_pixel_color4_carry_O_UNCONNECTED;
  wire [3:1]NLW_pixel_color4_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_pixel_color4_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_pixel_color4_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color4_inferred__0/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color4_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:0]NLW_pixel_color5_carry_O_UNCONNECTED;
  wire [3:1]NLW_pixel_color5_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_pixel_color5_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_pixel_color5_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color5_inferred__0/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color5_inferred__1/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__1/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__2/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color5_inferred__2/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__2/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__3/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color5_inferred__3/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__3/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__4/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color5_inferred__4/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__4/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color6_inferred__1/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__1/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__2/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color6_inferred__2/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__2/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__3/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color6_inferred__3/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__3/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__4/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color6_inferred__4/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__4/i__carry__0_O_UNCONNECTED ;

  LUT3 #(
    .INIT(8'h7F)) 
    \dc_bias[3]_i_51 
       (.I0(pixel_color49_in),
        .I1(pixel_color58_in),
        .I2(\processQ_reg[1] [1]),
        .O(\dc_bias[3]_i_51_n_0 ));
  LUT4 #(
    .INIT(16'h0F77)) 
    \dc_bias[3]_i_52 
       (.I0(pixel_color612_in),
        .I1(pixel_color513_in),
        .I2(pixel_color3),
        .I3(\processQ_reg[1] [1]),
        .O(\dc_bias[3]_i_52_n_0 ));
  MUXF7 \dc_bias_reg[3]_i_27 
       (.I0(\dc_bias[3]_i_51_n_0 ),
        .I1(\dc_bias[3]_i_52_n_0 ),
        .O(\dc_bias_reg[1]_36 ),
        .S(\processQ_reg[1] [0]));
  LUT6 #(
    .INIT(64'hFEAA000000000000)) 
    i__carry__0_i_3
       (.I0(Q[4]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[5]),
        .I5(Q[6]),
        .O(\dc_bias_reg[1]_7 ));
  LUT6 #(
    .INIT(64'h8888008000000000)) 
    i__carry__0_i_3__0
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[3]),
        .I3(\dc_bias_reg[1]_9 ),
        .I4(Q[4]),
        .I5(Q[7]),
        .O(\dc_bias_reg[1]_8 ));
  LUT6 #(
    .INIT(64'hFEAA000000000000)) 
    i__carry__0_i_3__1
       (.I0(\trigger_volt_s_reg[6] [4]),
        .I1(\trigger_volt_s_reg[6] [0]),
        .I2(\trigger_volt_s_reg[6] [1]),
        .I3(\dc_bias_reg[1]_11 ),
        .I4(\trigger_volt_s_reg[6] [5]),
        .I5(\trigger_volt_s_reg[6] [6]),
        .O(\dc_bias_reg[1]_10 ));
  LUT6 #(
    .INIT(64'hEAAA000000000000)) 
    i__carry__0_i_3__2
       (.I0(\trigger_volt_s_reg[6] [4]),
        .I1(\trigger_volt_s_reg[6] [1]),
        .I2(\trigger_volt_s_reg[6] [2]),
        .I3(\trigger_volt_s_reg[6] [3]),
        .I4(\trigger_volt_s_reg[6] [5]),
        .I5(\trigger_volt_s_reg[6] [6]),
        .O(\dc_bias_reg[1]_12 ));
  LUT6 #(
    .INIT(64'hEAAA000000000000)) 
    i__carry__0_i_3__3
       (.I0(\trigger_volt_s_reg[6] [4]),
        .I1(\trigger_volt_s_reg[6] [0]),
        .I2(\trigger_volt_s_reg[6] [1]),
        .I3(\dc_bias_reg[1]_11 ),
        .I4(\trigger_volt_s_reg[6] [5]),
        .I5(\trigger_volt_s_reg[6] [6]),
        .O(\dc_bias_reg[1]_13 ));
  LUT6 #(
    .INIT(64'hFBAA000000000000)) 
    i__carry__0_i_3__4
       (.I0(\trigger_volt_s_reg[6] [4]),
        .I1(\dc_bias_reg[1]_15 ),
        .I2(\trigger_volt_s_reg[6] [0]),
        .I3(\trigger_volt_s_reg[6] [3]),
        .I4(\trigger_volt_s_reg[6] [5]),
        .I5(\trigger_volt_s_reg[6] [6]),
        .O(\dc_bias_reg[1]_14 ));
  LUT6 #(
    .INIT(64'hEAAA000000000000)) 
    i__carry__0_i_3__5
       (.I0(Q[4]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[5]),
        .I5(Q[6]),
        .O(\dc_bias_reg[1]_18 ));
  LUT6 #(
    .INIT(64'hFEAA000000000000)) 
    i__carry__0_i_3__8
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(i__carry__0_i_4_n_0),
        .I3(Q[3]),
        .I4(Q[5]),
        .I5(Q[6]),
        .O(\dc_bias_reg[1]_19 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h8)) 
    i__carry__0_i_4
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(i__carry__0_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h01)) 
    i__carry_i_10
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(\dc_bias_reg[1]_9 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_10__0
       (.I0(Q[1]),
        .I1(Q[2]),
        .O(\dc_bias_reg[1]_22 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_10__1
       (.I0(\trigger_volt_s_reg[6] [0]),
        .I1(\trigger_volt_s_reg[6] [1]),
        .O(\dc_bias_reg[1]_24 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    i__carry_i_10__2
       (.I0(\trigger_volt_s_reg[6] [3]),
        .I1(\trigger_volt_s_reg[6] [2]),
        .I2(\trigger_volt_s_reg[6] [1]),
        .I3(\trigger_volt_s_reg[6] [0]),
        .O(\dc_bias_reg[1]_27 ));
  LUT2 #(
    .INIT(4'h8)) 
    i__carry_i_10__3
       (.I0(\trigger_volt_s_reg[6] [2]),
        .I1(\trigger_volt_s_reg[6] [3]),
        .O(\dc_bias_reg[1]_11 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT4 #(
    .INIT(16'hAAA8)) 
    i__carry_i_10__4
       (.I0(\trigger_volt_s_reg[6] [3]),
        .I1(\trigger_volt_s_reg[6] [0]),
        .I2(\trigger_volt_s_reg[6] [1]),
        .I3(\trigger_volt_s_reg[6] [2]),
        .O(\dc_bias_reg[1]_29 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT4 #(
    .INIT(16'hAA80)) 
    i__carry_i_10__8
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(\dc_bias_reg[1]_35 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT5 #(
    .INIT(32'hAAAA8880)) 
    i__carry_i_9
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[4]),
        .O(\dc_bias_reg[1]_20 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA88888880)) 
    i__carry_i_9__0
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\dc_bias_reg[1]_21 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA80808000)) 
    i__carry_i_9__1
       (.I0(\trigger_volt_s_reg[6] [5]),
        .I1(\trigger_volt_s_reg[6] [3]),
        .I2(\trigger_volt_s_reg[6] [2]),
        .I3(\trigger_volt_s_reg[6] [1]),
        .I4(\trigger_volt_s_reg[6] [0]),
        .I5(\trigger_volt_s_reg[6] [4]),
        .O(\dc_bias_reg[1]_23 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT5 #(
    .INIT(32'hAAAA8000)) 
    i__carry_i_9__2
       (.I0(\trigger_volt_s_reg[6] [5]),
        .I1(\trigger_volt_s_reg[6] [3]),
        .I2(\trigger_volt_s_reg[6] [2]),
        .I3(\trigger_volt_s_reg[6] [1]),
        .I4(\trigger_volt_s_reg[6] [4]),
        .O(\dc_bias_reg[1]_25 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA80000000)) 
    i__carry_i_9__3
       (.I0(\trigger_volt_s_reg[6] [5]),
        .I1(\trigger_volt_s_reg[6] [3]),
        .I2(\trigger_volt_s_reg[6] [2]),
        .I3(\trigger_volt_s_reg[6] [1]),
        .I4(\trigger_volt_s_reg[6] [0]),
        .I5(\trigger_volt_s_reg[6] [4]),
        .O(\dc_bias_reg[1]_26 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA88888880)) 
    i__carry_i_9__4
       (.I0(\trigger_volt_s_reg[6] [5]),
        .I1(\trigger_volt_s_reg[6] [3]),
        .I2(\trigger_volt_s_reg[6] [0]),
        .I3(\trigger_volt_s_reg[6] [1]),
        .I4(\trigger_volt_s_reg[6] [2]),
        .I5(\trigger_volt_s_reg[6] [4]),
        .O(\dc_bias_reg[1]_28 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT5 #(
    .INIT(32'hAAAA8000)) 
    i__carry_i_9__5
       (.I0(Q[5]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[1]),
        .I4(Q[4]),
        .O(\dc_bias_reg[1]_33 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA88888000)) 
    i__carry_i_9__8
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\dc_bias_reg[1]_34 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color3_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({pixel_color3,\pixel_color3_inferred__0/i__carry_n_1 ,\pixel_color3_inferred__0/i__carry_n_2 ,\pixel_color3_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_pixel_color3_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_volt_s_reg[7]_2 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color3_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\dc_bias_reg[1]_4 ,\pixel_color3_inferred__1/i__carry_n_1 ,\pixel_color3_inferred__1/i__carry_n_2 ,\pixel_color3_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_pixel_color3_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_time_s_reg[8] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 pixel_color4_carry
       (.CI(1'b0),
        .CO({pixel_color4_carry_n_0,pixel_color4_carry_n_1,pixel_color4_carry_n_2,pixel_color4_carry_n_3}),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_1 ),
        .O(NLW_pixel_color4_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_volt_s_reg[7]_1 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 pixel_color4_carry__0
       (.CI(pixel_color4_carry_n_0),
        .CO({NLW_pixel_color4_carry__0_CO_UNCONNECTED[3:1],pixel_color49_in}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_2 }),
        .O(NLW_pixel_color4_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_volt_s_reg[9]_2 }));
  LUT6 #(
    .INIT(64'hFEAA000000000000)) 
    pixel_color4_carry__0_i_3
       (.I0(\trigger_volt_s_reg[6] [4]),
        .I1(\trigger_volt_s_reg[6] [2]),
        .I2(pixel_color4_carry__0_i_4_n_0),
        .I3(\trigger_volt_s_reg[6] [3]),
        .I4(\trigger_volt_s_reg[6] [5]),
        .I5(\trigger_volt_s_reg[6] [6]),
        .O(\dc_bias_reg[1]_17 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    pixel_color4_carry__0_i_4
       (.I0(\trigger_volt_s_reg[6] [0]),
        .I1(\trigger_volt_s_reg[6] [1]),
        .O(pixel_color4_carry__0_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hAA80)) 
    pixel_color4_carry_i_10
       (.I0(\trigger_volt_s_reg[6] [3]),
        .I1(\trigger_volt_s_reg[6] [0]),
        .I2(\trigger_volt_s_reg[6] [1]),
        .I3(\trigger_volt_s_reg[6] [2]),
        .O(\dc_bias_reg[1]_32 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA88888000)) 
    pixel_color4_carry_i_9
       (.I0(\trigger_volt_s_reg[6] [5]),
        .I1(\trigger_volt_s_reg[6] [3]),
        .I2(\trigger_volt_s_reg[6] [0]),
        .I3(\trigger_volt_s_reg[6] [1]),
        .I4(\trigger_volt_s_reg[6] [2]),
        .I5(\trigger_volt_s_reg[6] [4]),
        .O(\dc_bias_reg[1]_31 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color4_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color4_inferred__0/i__carry_n_0 ,\pixel_color4_inferred__0/i__carry_n_1 ,\pixel_color4_inferred__0/i__carry_n_2 ,\pixel_color4_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_7 ),
        .O(\NLW_pixel_color4_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_time_s_reg[7]_2 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color4_inferred__0/i__carry__0 
       (.CI(\pixel_color4_inferred__0/i__carry_n_0 ),
        .CO({\NLW_pixel_color4_inferred__0/i__carry__0_CO_UNCONNECTED [3:1],\dc_bias_reg[1]_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_8 }),
        .O(\NLW_pixel_color4_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_time_s_reg[9]_2 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 pixel_color5_carry
       (.CI(1'b0),
        .CO({pixel_color5_carry_n_0,pixel_color5_carry_n_1,pixel_color5_carry_n_2,pixel_color5_carry_n_3}),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7] ),
        .O(NLW_pixel_color5_carry_O_UNCONNECTED[3:0]),
        .S(\trigger_volt_s_reg[7] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 pixel_color5_carry__0
       (.CI(pixel_color5_carry_n_0),
        .CO({NLW_pixel_color5_carry__0_CO_UNCONNECTED[3:1],pixel_color513_in}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_0 }),
        .O(NLW_pixel_color5_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_volt_s_reg[9]_0 }));
  LUT6 #(
    .INIT(64'hFEAA000000000000)) 
    pixel_color5_carry__0_i_3
       (.I0(\trigger_volt_s_reg[6] [4]),
        .I1(\trigger_volt_s_reg[6] [1]),
        .I2(\trigger_volt_s_reg[6] [2]),
        .I3(\trigger_volt_s_reg[6] [3]),
        .I4(\trigger_volt_s_reg[6] [5]),
        .I5(\trigger_volt_s_reg[6] [6]),
        .O(\dc_bias_reg[1]_16 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h1)) 
    pixel_color5_carry_i_10
       (.I0(\trigger_volt_s_reg[6] [1]),
        .I1(\trigger_volt_s_reg[6] [2]),
        .O(\dc_bias_reg[1]_15 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT5 #(
    .INIT(32'hAAAA8880)) 
    pixel_color5_carry_i_9
       (.I0(\trigger_volt_s_reg[6] [5]),
        .I1(\trigger_volt_s_reg[6] [3]),
        .I2(\trigger_volt_s_reg[6] [2]),
        .I3(\trigger_volt_s_reg[6] [1]),
        .I4(\trigger_volt_s_reg[6] [4]),
        .O(\dc_bias_reg[1]_30 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color5_inferred__0/i__carry_n_0 ,\pixel_color5_inferred__0/i__carry_n_1 ,\pixel_color5_inferred__0/i__carry_n_2 ,\pixel_color5_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_0 ),
        .O(\NLW_pixel_color5_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_volt_s_reg[7]_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__0/i__carry__0 
       (.CI(\pixel_color5_inferred__0/i__carry_n_0 ),
        .CO({\NLW_pixel_color5_inferred__0/i__carry__0_CO_UNCONNECTED [3:1],pixel_color58_in}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_1 }),
        .O(\NLW_pixel_color5_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_volt_s_reg[9]_1 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color5_inferred__1/i__carry_n_0 ,\pixel_color5_inferred__1/i__carry_n_1 ,\pixel_color5_inferred__1/i__carry_n_2 ,\pixel_color5_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_3 ),
        .O(\NLW_pixel_color5_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_volt_s_reg[7]_4 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__1/i__carry__0 
       (.CI(\pixel_color5_inferred__1/i__carry_n_0 ),
        .CO({\NLW_pixel_color5_inferred__1/i__carry__0_CO_UNCONNECTED [3:1],\dc_bias_reg[1]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_4 }),
        .O(\NLW_pixel_color5_inferred__1/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_volt_s_reg[9]_4 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color5_inferred__2/i__carry_n_0 ,\pixel_color5_inferred__2/i__carry_n_1 ,\pixel_color5_inferred__2/i__carry_n_2 ,\pixel_color5_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_5 ),
        .O(\NLW_pixel_color5_inferred__2/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_time_s_reg[7]_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__2/i__carry__0 
       (.CI(\pixel_color5_inferred__2/i__carry_n_0 ),
        .CO({\NLW_pixel_color5_inferred__2/i__carry__0_CO_UNCONNECTED [3:1],\dc_bias_reg[1]_1 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_6 }),
        .O(\NLW_pixel_color5_inferred__2/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_time_s_reg[9]_0 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__3/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color5_inferred__3/i__carry_n_0 ,\pixel_color5_inferred__3/i__carry_n_1 ,\pixel_color5_inferred__3/i__carry_n_2 ,\pixel_color5_inferred__3/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_6 ),
        .O(\NLW_pixel_color5_inferred__3/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_time_s_reg[7]_1 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__3/i__carry__0 
       (.CI(\pixel_color5_inferred__3/i__carry_n_0 ),
        .CO({\NLW_pixel_color5_inferred__3/i__carry__0_CO_UNCONNECTED [3:1],\dc_bias_reg[1]_2 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_7 }),
        .O(\NLW_pixel_color5_inferred__3/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_time_s_reg[9]_1 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__4/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color5_inferred__4/i__carry_n_0 ,\pixel_color5_inferred__4/i__carry_n_1 ,\pixel_color5_inferred__4/i__carry_n_2 ,\pixel_color5_inferred__4/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_9 ),
        .O(\NLW_pixel_color5_inferred__4/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_time_s_reg[7]_4 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__4/i__carry__0 
       (.CI(\pixel_color5_inferred__4/i__carry_n_0 ),
        .CO({\NLW_pixel_color5_inferred__4/i__carry__0_CO_UNCONNECTED [3:1],\dc_bias_reg[1]_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_10 }),
        .O(\NLW_pixel_color5_inferred__4/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_time_s_reg[9]_4 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color6_inferred__1/i__carry_n_0 ,\pixel_color6_inferred__1/i__carry_n_1 ,\pixel_color6_inferred__1/i__carry_n_2 ,\pixel_color6_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(DI),
        .O(\NLW_pixel_color6_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S(S));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__1/i__carry__0 
       (.CI(\pixel_color6_inferred__1/i__carry_n_0 ),
        .CO({\NLW_pixel_color6_inferred__1/i__carry__0_CO_UNCONNECTED [3:1],pixel_color612_in}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9] }),
        .O(\NLW_pixel_color6_inferred__1/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_volt_s_reg[9] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color6_inferred__2/i__carry_n_0 ,\pixel_color6_inferred__2/i__carry_n_1 ,\pixel_color6_inferred__2/i__carry_n_2 ,\pixel_color6_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_2 ),
        .O(\NLW_pixel_color6_inferred__2/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_volt_s_reg[7]_3 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__2/i__carry__0 
       (.CI(\pixel_color6_inferred__2/i__carry_n_0 ),
        .CO({\NLW_pixel_color6_inferred__2/i__carry__0_CO_UNCONNECTED [3:1],\dc_bias_reg[1] }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_3 }),
        .O(\NLW_pixel_color6_inferred__2/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_volt_s_reg[9]_3 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__3/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color6_inferred__3/i__carry_n_0 ,\pixel_color6_inferred__3/i__carry_n_1 ,\pixel_color6_inferred__3/i__carry_n_2 ,\pixel_color6_inferred__3/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_4 ),
        .O(\NLW_pixel_color6_inferred__3/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_time_s_reg[7] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__3/i__carry__0 
       (.CI(\pixel_color6_inferred__3/i__carry_n_0 ),
        .CO({\NLW_pixel_color6_inferred__3/i__carry__0_CO_UNCONNECTED [3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_5 }),
        .O(\NLW_pixel_color6_inferred__3/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_time_s_reg[9] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__4/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color6_inferred__4/i__carry_n_0 ,\pixel_color6_inferred__4/i__carry_n_1 ,\pixel_color6_inferred__4/i__carry_n_2 ,\pixel_color6_inferred__4/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_8 ),
        .O(\NLW_pixel_color6_inferred__4/i__carry_O_UNCONNECTED [3:0]),
        .S(\trigger_time_s_reg[7]_3 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__4/i__carry__0 
       (.CI(\pixel_color6_inferred__4/i__carry_n_0 ),
        .CO({\NLW_pixel_color6_inferred__4/i__carry__0_CO_UNCONNECTED [3:1],\dc_bias_reg[1]_5 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_9 }),
        .O(\NLW_pixel_color6_inferred__4/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\trigger_time_s_reg[9]_3 }));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO
   (\dc_bias_reg[1] ,
    CO,
    clk,
    cw,
    reset_n,
    ADDRARDADDR,
    Q,
    D,
    \processQ_reg[9] ,
    switch);
  output \dc_bias_reg[1] ;
  output [0:0]CO;
  input clk;
  input [0:0]cw;
  input reset_n;
  input [9:0]ADDRARDADDR;
  input [9:0]Q;
  input [9:0]D;
  input [9:0]\processQ_reg[9] ;
  input [0:0]switch;

  wire [9:0]ADDRARDADDR;
  wire [0:0]CO;
  wire [9:0]D;
  wire [9:0]L_out_bram;
  wire [9:0]Q;
  wire clk;
  wire [0:0]cw;
  wire \dc_bias[3]_i_14__0_n_0 ;
  wire \dc_bias[3]_i_15_n_0 ;
  wire \dc_bias[3]_i_16_n_0 ;
  wire \dc_bias[3]_i_17_n_0 ;
  wire \dc_bias[3]_i_42_n_0 ;
  wire \dc_bias[3]_i_43_n_0 ;
  wire \dc_bias[3]_i_44_n_0 ;
  wire \dc_bias[3]_i_45_n_0 ;
  wire \dc_bias_reg[1] ;
  wire \dc_bias_reg[3]_i_7_n_1 ;
  wire \dc_bias_reg[3]_i_7_n_2 ;
  wire \dc_bias_reg[3]_i_7_n_3 ;
  wire [9:0]\processQ_reg[9] ;
  wire reset_n;
  wire [0:0]switch;
  wire [3:0]\NLW_dc_bias_reg[3]_i_7_O_UNCONNECTED ;
  wire [15:10]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED ;
  wire [15:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h6A95)) 
    \dc_bias[3]_i_14__0 
       (.I0(\processQ_reg[9] [9]),
        .I1(L_out_bram[8]),
        .I2(\dc_bias[3]_i_42_n_0 ),
        .I3(L_out_bram[9]),
        .O(\dc_bias[3]_i_14__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000006909009)) 
    \dc_bias[3]_i_15 
       (.I0(L_out_bram[8]),
        .I1(\processQ_reg[9] [8]),
        .I2(\processQ_reg[9] [7]),
        .I3(\dc_bias[3]_i_43_n_0 ),
        .I4(L_out_bram[7]),
        .I5(\dc_bias[3]_i_44_n_0 ),
        .O(\dc_bias[3]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h00000000556AAA95)) 
    \dc_bias[3]_i_16 
       (.I0(L_out_bram[5]),
        .I1(L_out_bram[2]),
        .I2(L_out_bram[3]),
        .I3(L_out_bram[4]),
        .I4(\processQ_reg[9] [5]),
        .I5(\dc_bias[3]_i_45_n_0 ),
        .O(\dc_bias[3]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    \dc_bias[3]_i_17 
       (.I0(L_out_bram[2]),
        .I1(\processQ_reg[9] [2]),
        .I2(L_out_bram[1]),
        .I3(\processQ_reg[9] [1]),
        .I4(L_out_bram[0]),
        .I5(\processQ_reg[9] [0]),
        .O(\dc_bias[3]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h8888800000000000)) 
    \dc_bias[3]_i_42 
       (.I0(L_out_bram[7]),
        .I1(L_out_bram[5]),
        .I2(L_out_bram[2]),
        .I3(L_out_bram[3]),
        .I4(L_out_bram[4]),
        .I5(L_out_bram[6]),
        .O(\dc_bias[3]_i_42_n_0 ));
  LUT5 #(
    .INIT(32'hA8880000)) 
    \dc_bias[3]_i_43 
       (.I0(L_out_bram[6]),
        .I1(L_out_bram[4]),
        .I2(L_out_bram[3]),
        .I3(L_out_bram[2]),
        .I4(L_out_bram[5]),
        .O(\dc_bias[3]_i_43_n_0 ));
  LUT6 #(
    .INIT(64'h9999955566666AAA)) 
    \dc_bias[3]_i_44 
       (.I0(\processQ_reg[9] [6]),
        .I1(L_out_bram[5]),
        .I2(L_out_bram[2]),
        .I3(L_out_bram[3]),
        .I4(L_out_bram[4]),
        .I5(L_out_bram[6]),
        .O(\dc_bias[3]_i_44_n_0 ));
  LUT5 #(
    .INIT(32'hBDDEE77B)) 
    \dc_bias[3]_i_45 
       (.I0(\processQ_reg[9] [3]),
        .I1(L_out_bram[4]),
        .I2(L_out_bram[3]),
        .I3(L_out_bram[2]),
        .I4(\processQ_reg[9] [4]),
        .O(\dc_bias[3]_i_45_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \dc_bias[3]_i_8__0 
       (.I0(switch),
        .I1(CO),
        .O(\dc_bias_reg[1] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \dc_bias_reg[3]_i_7 
       (.CI(1'b0),
        .CO({CO,\dc_bias_reg[3]_i_7_n_1 ,\dc_bias_reg[3]_i_7_n_2 ,\dc_bias_reg[3]_i_7_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_dc_bias_reg[3]_i_7_O_UNCONNECTED [3:0]),
        .S({\dc_bias[3]_i_14__0_n_0 ,\dc_bias[3]_i_15_n_0 ,\dc_bias[3]_i_16_n_0 ,\dc_bias[3]_i_17_n_0 }));
  (* BOX_TYPE = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("NONE"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(18)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl 
       (.ADDRARDADDR({ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({Q,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,D}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED [15:10],L_out_bram}),
        .DOBDO(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(cw),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(reset_n),
        .RSTRAMB(reset_n),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b1,1'b1,1'b1,1'b1}));
endmodule

(* ORIG_REF_NAME = "unimacro_BRAM_SDP_MACRO" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0
   (\dc_bias_reg[1] ,
    CO,
    clk,
    cw,
    reset_n,
    ADDRARDADDR,
    Q,
    D,
    \processQ_reg[9] ,
    switch);
  output \dc_bias_reg[1] ;
  output [0:0]CO;
  input clk;
  input [0:0]cw;
  input reset_n;
  input [9:0]ADDRARDADDR;
  input [9:0]Q;
  input [9:0]D;
  input [9:0]\processQ_reg[9] ;
  input [0:0]switch;

  wire [9:0]ADDRARDADDR;
  wire [0:0]CO;
  wire [9:0]D;
  wire [9:0]Q;
  wire [9:0]R_out_bram;
  wire clk;
  wire [0:0]cw;
  wire \dc_bias[3]_i_47_n_0 ;
  wire \dc_bias[3]_i_48_n_0 ;
  wire \dc_bias[3]_i_49_n_0 ;
  wire \dc_bias[3]_i_50_n_0 ;
  wire \dc_bias[3]_i_78_n_0 ;
  wire \dc_bias[3]_i_79_n_0 ;
  wire \dc_bias[3]_i_80_n_0 ;
  wire \dc_bias[3]_i_81_n_0 ;
  wire \dc_bias_reg[1] ;
  wire \dc_bias_reg[3]_i_21_n_1 ;
  wire \dc_bias_reg[3]_i_21_n_2 ;
  wire \dc_bias_reg[3]_i_21_n_3 ;
  wire [9:0]\processQ_reg[9] ;
  wire reset_n;
  wire [0:0]switch;
  wire [3:0]\NLW_dc_bias_reg[3]_i_21_O_UNCONNECTED ;
  wire [15:10]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED ;
  wire [15:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h7)) 
    \dc_bias[3]_i_10__0 
       (.I0(switch),
        .I1(CO),
        .O(\dc_bias_reg[1] ));
  LUT4 #(
    .INIT(16'h6A95)) 
    \dc_bias[3]_i_47 
       (.I0(\processQ_reg[9] [9]),
        .I1(R_out_bram[8]),
        .I2(\dc_bias[3]_i_78_n_0 ),
        .I3(R_out_bram[9]),
        .O(\dc_bias[3]_i_47_n_0 ));
  LUT6 #(
    .INIT(64'h0000000006909009)) 
    \dc_bias[3]_i_48 
       (.I0(R_out_bram[8]),
        .I1(\processQ_reg[9] [8]),
        .I2(\processQ_reg[9] [7]),
        .I3(\dc_bias[3]_i_79_n_0 ),
        .I4(R_out_bram[7]),
        .I5(\dc_bias[3]_i_80_n_0 ),
        .O(\dc_bias[3]_i_48_n_0 ));
  LUT6 #(
    .INIT(64'h00000000556AAA95)) 
    \dc_bias[3]_i_49 
       (.I0(R_out_bram[5]),
        .I1(R_out_bram[2]),
        .I2(R_out_bram[3]),
        .I3(R_out_bram[4]),
        .I4(\processQ_reg[9] [5]),
        .I5(\dc_bias[3]_i_81_n_0 ),
        .O(\dc_bias[3]_i_49_n_0 ));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    \dc_bias[3]_i_50 
       (.I0(R_out_bram[2]),
        .I1(\processQ_reg[9] [2]),
        .I2(R_out_bram[1]),
        .I3(\processQ_reg[9] [1]),
        .I4(R_out_bram[0]),
        .I5(\processQ_reg[9] [0]),
        .O(\dc_bias[3]_i_50_n_0 ));
  LUT6 #(
    .INIT(64'h8888800000000000)) 
    \dc_bias[3]_i_78 
       (.I0(R_out_bram[7]),
        .I1(R_out_bram[5]),
        .I2(R_out_bram[2]),
        .I3(R_out_bram[3]),
        .I4(R_out_bram[4]),
        .I5(R_out_bram[6]),
        .O(\dc_bias[3]_i_78_n_0 ));
  LUT5 #(
    .INIT(32'hA8880000)) 
    \dc_bias[3]_i_79 
       (.I0(R_out_bram[6]),
        .I1(R_out_bram[4]),
        .I2(R_out_bram[3]),
        .I3(R_out_bram[2]),
        .I4(R_out_bram[5]),
        .O(\dc_bias[3]_i_79_n_0 ));
  LUT6 #(
    .INIT(64'h9999955566666AAA)) 
    \dc_bias[3]_i_80 
       (.I0(\processQ_reg[9] [6]),
        .I1(R_out_bram[5]),
        .I2(R_out_bram[2]),
        .I3(R_out_bram[3]),
        .I4(R_out_bram[4]),
        .I5(R_out_bram[6]),
        .O(\dc_bias[3]_i_80_n_0 ));
  LUT5 #(
    .INIT(32'hBDDEE77B)) 
    \dc_bias[3]_i_81 
       (.I0(\processQ_reg[9] [3]),
        .I1(R_out_bram[4]),
        .I2(R_out_bram[3]),
        .I3(R_out_bram[2]),
        .I4(\processQ_reg[9] [4]),
        .O(\dc_bias[3]_i_81_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \dc_bias_reg[3]_i_21 
       (.CI(1'b0),
        .CO({CO,\dc_bias_reg[3]_i_21_n_1 ,\dc_bias_reg[3]_i_21_n_2 ,\dc_bias_reg[3]_i_21_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_dc_bias_reg[3]_i_21_O_UNCONNECTED [3:0]),
        .S({\dc_bias[3]_i_47_n_0 ,\dc_bias[3]_i_48_n_0 ,\dc_bias[3]_i_49_n_0 ,\dc_bias[3]_i_50_n_0 }));
  (* BOX_TYPE = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("NONE"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(18)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl 
       (.ADDRARDADDR({ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({Q,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,D}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED [15:10],R_out_bram}),
        .DOBDO(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(cw),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(reset_n),
        .RSTRAMB(reset_n),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b1,1'b1,1'b1,1'b1}));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vcounter
   (\encoded_reg[8] ,
    \encoded_reg[9] ,
    \encoded_reg[8]_0 ,
    Q,
    \dc_bias_reg[1] ,
    \dc_bias_reg[1]_0 ,
    \dc_bias_reg[1]_1 ,
    \dc_bias_reg[1]_2 ,
    \dc_bias_reg[1]_3 ,
    \dc_bias_reg[1]_4 ,
    D,
    \encoded_reg[0] ,
    \dc_bias_reg[3] ,
    \encoded_reg[9]_0 ,
    \dc_bias_reg[1]_5 ,
    encoded1_in,
    \encoded_reg[3] ,
    \dc_bias_reg[0] ,
    \encoded_reg[8]_1 ,
    \encoded_reg[4] ,
    \encoded_reg[0]_0 ,
    \dc_bias_reg[0]_0 ,
    \dc_bias_reg[1]_6 ,
    \dc_bias_reg[1]_7 ,
    DI,
    \dc_bias_reg[1]_8 ,
    \dc_bias_reg[1]_9 ,
    \dc_bias_reg[1]_10 ,
    S,
    \dc_bias_reg[1]_11 ,
    \dc_bias_reg[1]_12 ,
    \dc_bias_reg[1]_13 ,
    \dc_bias_reg[1]_14 ,
    \dc_bias_reg[1]_15 ,
    \dc_bias_reg[1]_16 ,
    \dc_bias_reg[1]_17 ,
    \dc_bias_reg[1]_18 ,
    \dc_bias_reg[1]_19 ,
    \dc_bias_reg[1]_20 ,
    \dc_bias_reg[1]_21 ,
    \dc_bias_reg[1]_22 ,
    \dc_bias_reg[1]_23 ,
    \dc_bias_reg[1]_24 ,
    \encoded_reg[9]_1 ,
    \encoded_reg[9]_2 ,
    \encoded_reg[9]_3 ,
    \processQ_reg[6]_0 ,
    \dc_bias_reg[0]_1 ,
    \trigger_volt_s_reg[9] ,
    \trigger_volt_s_reg[0] ,
    \trigger_volt_s_reg[2] ,
    \trigger_volt_s_reg[5] ,
    \trigger_volt_s_reg[4] ,
    \dc_bias_reg[0]_2 ,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[0]_3 ,
    \processQ_reg[1]_0 ,
    \processQ_reg[9]_0 ,
    \processQ_reg[9]_1 ,
    \processQ_reg[1]_1 ,
    \dc_bias_reg[3]_2 ,
    \dc_bias_reg[3]_3 ,
    \processQ_reg[1]_2 ,
    \processQ_reg[1]_3 ,
    \processQ_reg[1]_4 ,
    \processQ_reg[5]_0 ,
    \processQ_reg[1]_5 ,
    \processQ_reg[1]_6 ,
    \processQ_reg[2]_0 ,
    SR,
    reset_n,
    \processQ_reg[6]_1 ,
    CO,
    switch,
    \processQ_reg[0]_0 ,
    \processQ_reg[9]_2 ,
    \processQ_reg[8]_0 ,
    \processQ_reg[7]_0 ,
    \processQ_reg[5]_1 ,
    \processQ_reg[7]_1 ,
    \trigger_volt_s_reg[4]_0 ,
    \trigger_volt_s_reg[5]_0 ,
    \trigger_volt_s_reg[4]_1 ,
    \trigger_volt_s_reg[3] ,
    \trigger_volt_s_reg[5]_1 ,
    \trigger_volt_s_reg[4]_2 ,
    \trigger_volt_s_reg[4]_3 ,
    \trigger_volt_s_reg[4]_4 ,
    \trigger_volt_s_reg[4]_5 ,
    \trigger_volt_s_reg[3]_0 ,
    \trigger_volt_s_reg[5]_2 ,
    \trigger_volt_s_reg[1] ,
    \trigger_volt_s_reg[5]_3 ,
    \trigger_volt_s_reg[3]_1 ,
    \trigger_volt_s_reg[5]_4 ,
    \processQ_reg[9]_3 ,
    \processQ_reg[9]_4 ,
    \trigger_time_s_reg[8] ,
    \dc_bias_reg[0]_4 ,
    \dc_bias_reg[3]_4 ,
    CLK);
  output \encoded_reg[8] ;
  output \encoded_reg[9] ;
  output \encoded_reg[8]_0 ;
  output [9:0]Q;
  output [3:0]\dc_bias_reg[1] ;
  output [3:0]\dc_bias_reg[1]_0 ;
  output [3:0]\dc_bias_reg[1]_1 ;
  output [3:0]\dc_bias_reg[1]_2 ;
  output [0:0]\dc_bias_reg[1]_3 ;
  output [0:0]\dc_bias_reg[1]_4 ;
  output [0:0]D;
  output \encoded_reg[0] ;
  output \dc_bias_reg[3] ;
  output \encoded_reg[9]_0 ;
  output \dc_bias_reg[1]_5 ;
  output [0:0]encoded1_in;
  output \encoded_reg[3] ;
  output \dc_bias_reg[0] ;
  output \encoded_reg[8]_1 ;
  output \encoded_reg[4] ;
  output \encoded_reg[0]_0 ;
  output [0:0]\dc_bias_reg[0]_0 ;
  output [3:0]\dc_bias_reg[1]_6 ;
  output [3:0]\dc_bias_reg[1]_7 ;
  output [3:0]DI;
  output [3:0]\dc_bias_reg[1]_8 ;
  output [3:0]\dc_bias_reg[1]_9 ;
  output [3:0]\dc_bias_reg[1]_10 ;
  output [3:0]S;
  output [3:0]\dc_bias_reg[1]_11 ;
  output [0:0]\dc_bias_reg[1]_12 ;
  output [0:0]\dc_bias_reg[1]_13 ;
  output [0:0]\dc_bias_reg[1]_14 ;
  output [0:0]\dc_bias_reg[1]_15 ;
  output [0:0]\dc_bias_reg[1]_16 ;
  output [0:0]\dc_bias_reg[1]_17 ;
  output [0:0]\dc_bias_reg[1]_18 ;
  output [0:0]\dc_bias_reg[1]_19 ;
  output [0:0]\dc_bias_reg[1]_20 ;
  output [0:0]\dc_bias_reg[1]_21 ;
  output [3:0]\dc_bias_reg[1]_22 ;
  output \dc_bias_reg[1]_23 ;
  output \dc_bias_reg[1]_24 ;
  output \encoded_reg[9]_1 ;
  output \encoded_reg[9]_2 ;
  output \encoded_reg[9]_3 ;
  input \processQ_reg[6]_0 ;
  input \dc_bias_reg[0]_1 ;
  input [9:0]\trigger_volt_s_reg[9] ;
  input \trigger_volt_s_reg[0] ;
  input \trigger_volt_s_reg[2] ;
  input \trigger_volt_s_reg[5] ;
  input \trigger_volt_s_reg[4] ;
  input \dc_bias_reg[0]_2 ;
  input \dc_bias_reg[3]_0 ;
  input [2:0]\dc_bias_reg[3]_1 ;
  input \dc_bias_reg[0]_3 ;
  input \processQ_reg[1]_0 ;
  input \processQ_reg[9]_0 ;
  input \processQ_reg[9]_1 ;
  input \processQ_reg[1]_1 ;
  input \dc_bias_reg[3]_2 ;
  input [0:0]\dc_bias_reg[3]_3 ;
  input \processQ_reg[1]_2 ;
  input \processQ_reg[1]_3 ;
  input \processQ_reg[1]_4 ;
  input \processQ_reg[5]_0 ;
  input \processQ_reg[1]_5 ;
  input \processQ_reg[1]_6 ;
  input \processQ_reg[2]_0 ;
  input [0:0]SR;
  input reset_n;
  input \processQ_reg[6]_1 ;
  input [0:0]CO;
  input [1:0]switch;
  input \processQ_reg[0]_0 ;
  input [0:0]\processQ_reg[9]_2 ;
  input \processQ_reg[8]_0 ;
  input \processQ_reg[7]_0 ;
  input \processQ_reg[5]_1 ;
  input \processQ_reg[7]_1 ;
  input \trigger_volt_s_reg[4]_0 ;
  input \trigger_volt_s_reg[5]_0 ;
  input \trigger_volt_s_reg[4]_1 ;
  input \trigger_volt_s_reg[3] ;
  input \trigger_volt_s_reg[5]_1 ;
  input \trigger_volt_s_reg[4]_2 ;
  input \trigger_volt_s_reg[4]_3 ;
  input \trigger_volt_s_reg[4]_4 ;
  input \trigger_volt_s_reg[4]_5 ;
  input \trigger_volt_s_reg[3]_0 ;
  input \trigger_volt_s_reg[5]_2 ;
  input \trigger_volt_s_reg[1] ;
  input \trigger_volt_s_reg[5]_3 ;
  input \trigger_volt_s_reg[3]_1 ;
  input \trigger_volt_s_reg[5]_4 ;
  input [0:0]\processQ_reg[9]_3 ;
  input [0:0]\processQ_reg[9]_4 ;
  input [0:0]\trigger_time_s_reg[8] ;
  input \dc_bias_reg[0]_4 ;
  input [0:0]\dc_bias_reg[3]_4 ;
  input CLK;

  wire CLK;
  wire [0:0]CO;
  wire [0:0]D;
  wire [3:0]DI;
  wire [9:0]Q;
  wire [3:0]S;
  wire [0:0]SR;
  wire \dc_bias[1]_i_2_n_0 ;
  wire \dc_bias[3]_i_10_n_0 ;
  wire \dc_bias[3]_i_11__0_n_0 ;
  wire \dc_bias[3]_i_12__0_n_0 ;
  wire \dc_bias[3]_i_13__0_n_0 ;
  wire \dc_bias[3]_i_14_n_0 ;
  wire \dc_bias[3]_i_16__0_n_0 ;
  wire \dc_bias[3]_i_17__0_n_0 ;
  wire \dc_bias[3]_i_19__0_n_0 ;
  wire \dc_bias[3]_i_20__0_n_0 ;
  wire \dc_bias[3]_i_20_n_0 ;
  wire \dc_bias[3]_i_21_n_0 ;
  wire \dc_bias[3]_i_22_n_0 ;
  wire \dc_bias[3]_i_26_n_0 ;
  wire \dc_bias[3]_i_27_n_0 ;
  wire \dc_bias[3]_i_28_n_0 ;
  wire \dc_bias[3]_i_29_n_0 ;
  wire \dc_bias[3]_i_33_n_0 ;
  wire \dc_bias[3]_i_34_n_0 ;
  wire \dc_bias[3]_i_36_n_0 ;
  wire \dc_bias[3]_i_37_n_0 ;
  wire \dc_bias[3]_i_46_n_0 ;
  wire \dc_bias[3]_i_53_n_0 ;
  wire \dc_bias[3]_i_54_n_0 ;
  wire \dc_bias[3]_i_55_n_0 ;
  wire \dc_bias[3]_i_58_n_0 ;
  wire \dc_bias[3]_i_60_n_0 ;
  wire \dc_bias[3]_i_61_n_0 ;
  wire \dc_bias[3]_i_62_n_0 ;
  wire \dc_bias[3]_i_63_n_0 ;
  wire \dc_bias[3]_i_66_n_0 ;
  wire \dc_bias[3]_i_67_n_0 ;
  wire \dc_bias[3]_i_68_n_0 ;
  wire \dc_bias[3]_i_77_n_0 ;
  wire \dc_bias[3]_i_7_n_0 ;
  wire \dc_bias[3]_i_82_n_0 ;
  wire \dc_bias[3]_i_83_n_0 ;
  wire \dc_bias[3]_i_84_n_0 ;
  wire \dc_bias[3]_i_85_n_0 ;
  wire \dc_bias[3]_i_8_n_0 ;
  wire \dc_bias[3]_i_90_n_0 ;
  wire \dc_bias[3]_i_91_n_0 ;
  wire \dc_bias[3]_i_93_n_0 ;
  wire \dc_bias[3]_i_94_n_0 ;
  wire \dc_bias[3]_i_95_n_0 ;
  wire \dc_bias[3]_i_96_n_0 ;
  wire \dc_bias_reg[0] ;
  wire [0:0]\dc_bias_reg[0]_0 ;
  wire \dc_bias_reg[0]_1 ;
  wire \dc_bias_reg[0]_2 ;
  wire \dc_bias_reg[0]_3 ;
  wire \dc_bias_reg[0]_4 ;
  wire [3:0]\dc_bias_reg[1] ;
  wire [3:0]\dc_bias_reg[1]_0 ;
  wire [3:0]\dc_bias_reg[1]_1 ;
  wire [3:0]\dc_bias_reg[1]_10 ;
  wire [3:0]\dc_bias_reg[1]_11 ;
  wire [0:0]\dc_bias_reg[1]_12 ;
  wire [0:0]\dc_bias_reg[1]_13 ;
  wire [0:0]\dc_bias_reg[1]_14 ;
  wire [0:0]\dc_bias_reg[1]_15 ;
  wire [0:0]\dc_bias_reg[1]_16 ;
  wire [0:0]\dc_bias_reg[1]_17 ;
  wire [0:0]\dc_bias_reg[1]_18 ;
  wire [0:0]\dc_bias_reg[1]_19 ;
  wire [3:0]\dc_bias_reg[1]_2 ;
  wire [0:0]\dc_bias_reg[1]_20 ;
  wire [0:0]\dc_bias_reg[1]_21 ;
  wire [3:0]\dc_bias_reg[1]_22 ;
  wire \dc_bias_reg[1]_23 ;
  wire \dc_bias_reg[1]_24 ;
  wire [0:0]\dc_bias_reg[1]_3 ;
  wire [0:0]\dc_bias_reg[1]_4 ;
  wire \dc_bias_reg[1]_5 ;
  wire [3:0]\dc_bias_reg[1]_6 ;
  wire [3:0]\dc_bias_reg[1]_7 ;
  wire [3:0]\dc_bias_reg[1]_8 ;
  wire [3:0]\dc_bias_reg[1]_9 ;
  wire \dc_bias_reg[3] ;
  wire \dc_bias_reg[3]_0 ;
  wire [2:0]\dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire [0:0]\dc_bias_reg[3]_3 ;
  wire [0:0]\dc_bias_reg[3]_4 ;
  wire [0:0]encoded1_in;
  wire \encoded[9]_i_2_n_0 ;
  wire \encoded[9]_i_3_n_0 ;
  wire \encoded_reg[0] ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[3] ;
  wire \encoded_reg[4] ;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \encoded_reg[9] ;
  wire \encoded_reg[9]_0 ;
  wire \encoded_reg[9]_1 ;
  wire \encoded_reg[9]_2 ;
  wire \encoded_reg[9]_3 ;
  wire i__carry_i_6__2_n_0;
  wire i__carry_i_7__5_n_0;
  wire [9:0]plusOp__2;
  wire processQ0;
  wire \processQ[2]_i_1__1_n_0 ;
  wire \processQ[6]_i_2__0_n_0 ;
  wire \processQ[9]_i_1_n_0 ;
  wire \processQ[9]_i_4__1_n_0 ;
  wire \processQ[9]_i_6__0_n_0 ;
  wire \processQ[9]_i_7_n_0 ;
  wire \processQ[9]_i_8_n_0 ;
  wire \processQ_reg[0]_0 ;
  wire \processQ_reg[1]_0 ;
  wire \processQ_reg[1]_1 ;
  wire \processQ_reg[1]_2 ;
  wire \processQ_reg[1]_3 ;
  wire \processQ_reg[1]_4 ;
  wire \processQ_reg[1]_5 ;
  wire \processQ_reg[1]_6 ;
  wire \processQ_reg[2]_0 ;
  wire \processQ_reg[5]_0 ;
  wire \processQ_reg[5]_1 ;
  wire \processQ_reg[6]_0 ;
  wire \processQ_reg[6]_1 ;
  wire \processQ_reg[7]_0 ;
  wire \processQ_reg[7]_1 ;
  wire \processQ_reg[8]_0 ;
  wire \processQ_reg[9]_0 ;
  wire \processQ_reg[9]_1 ;
  wire [0:0]\processQ_reg[9]_2 ;
  wire [0:0]\processQ_reg[9]_3 ;
  wire [0:0]\processQ_reg[9]_4 ;
  wire reset_n;
  wire [1:0]switch;
  wire [0:0]\trigger_time_s_reg[8] ;
  wire \trigger_volt_s_reg[0] ;
  wire \trigger_volt_s_reg[1] ;
  wire \trigger_volt_s_reg[2] ;
  wire \trigger_volt_s_reg[3] ;
  wire \trigger_volt_s_reg[3]_0 ;
  wire \trigger_volt_s_reg[3]_1 ;
  wire \trigger_volt_s_reg[4] ;
  wire \trigger_volt_s_reg[4]_0 ;
  wire \trigger_volt_s_reg[4]_1 ;
  wire \trigger_volt_s_reg[4]_2 ;
  wire \trigger_volt_s_reg[4]_3 ;
  wire \trigger_volt_s_reg[4]_4 ;
  wire \trigger_volt_s_reg[4]_5 ;
  wire \trigger_volt_s_reg[5] ;
  wire \trigger_volt_s_reg[5]_0 ;
  wire \trigger_volt_s_reg[5]_1 ;
  wire \trigger_volt_s_reg[5]_2 ;
  wire \trigger_volt_s_reg[5]_3 ;
  wire \trigger_volt_s_reg[5]_4 ;
  wire [9:0]\trigger_volt_s_reg[9] ;

  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'h0000F227)) 
    \TDMS_encoder_blue/encoded[9]_i_1 
       (.I0(\encoded_reg[9] ),
        .I1(\dc_bias_reg[3]_0 ),
        .I2(\encoded[9]_i_2_n_0 ),
        .I3(\processQ_reg[6]_0 ),
        .I4(\dc_bias_reg[0]_4 ),
        .O(\encoded_reg[9]_1 ));
  LUT3 #(
    .INIT(8'hD7)) 
    \TDMS_encoder_green/encoded[9]_i_1 
       (.I0(\encoded_reg[9] ),
        .I1(\dc_bias_reg[3]_4 ),
        .I2(\encoded_reg[9]_0 ),
        .O(\encoded_reg[9]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'h5FFF60FF)) 
    \TDMS_encoder_red/encoded[9]_i_1 
       (.I0(\dc_bias_reg[3]_3 ),
        .I1(\encoded_reg[0] ),
        .I2(\dc_bias_reg[3]_2 ),
        .I3(\encoded_reg[9] ),
        .I4(\encoded_reg[3] ),
        .O(\encoded_reg[9]_3 ));
  LUT2 #(
    .INIT(4'h1)) 
    \dc_bias[0]_i_2 
       (.I0(\encoded_reg[0] ),
        .I1(\encoded_reg[3] ),
        .O(\dc_bias_reg[0] ));
  LUT6 #(
    .INIT(64'h4FE0B01F40EFBF10)) 
    \dc_bias[1]_i_1 
       (.I0(\dc_bias[1]_i_2_n_0 ),
        .I1(\dc_bias_reg[0]_2 ),
        .I2(\dc_bias_reg[3]_0 ),
        .I3(\encoded_reg[0] ),
        .I4(\dc_bias_reg[3]_1 [1]),
        .I5(\dc_bias_reg[0]_3 ),
        .O(D));
  LUT6 #(
    .INIT(64'hAAAAAAAAFAAAEFAA)) 
    \dc_bias[1]_i_2 
       (.I0(\encoded_reg[9]_0 ),
        .I1(\dc_bias_reg[3]_1 [0]),
        .I2(\processQ_reg[9]_0 ),
        .I3(\dc_bias_reg[1]_5 ),
        .I4(\processQ_reg[9]_1 ),
        .I5(\processQ_reg[1]_1 ),
        .O(\dc_bias[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000F4F4FFF4)) 
    \dc_bias[3]_i_10 
       (.I0(\dc_bias[3]_i_28_n_0 ),
        .I1(\dc_bias[3]_i_29_n_0 ),
        .I2(\processQ_reg[1]_5 ),
        .I3(\processQ_reg[1]_6 ),
        .I4(\processQ_reg[2]_0 ),
        .I5(\dc_bias[3]_i_33_n_0 ),
        .O(\dc_bias[3]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hEFFFFFFF)) 
    \dc_bias[3]_i_11__0 
       (.I0(\processQ_reg[0]_0 ),
        .I1(\dc_bias[3]_i_19__0_n_0 ),
        .I2(\dc_bias[3]_i_20_n_0 ),
        .I3(CO),
        .I4(switch[0]),
        .O(\dc_bias[3]_i_11__0_n_0 ));
  LUT6 #(
    .INIT(64'h0E0E0E0E00000E00)) 
    \dc_bias[3]_i_12__0 
       (.I0(\dc_bias[3]_i_34_n_0 ),
        .I1(Q[1]),
        .I2(\processQ_reg[5]_0 ),
        .I3(Q[2]),
        .I4(\dc_bias[3]_i_36_n_0 ),
        .I5(\dc_bias[3]_i_37_n_0 ),
        .O(\dc_bias[3]_i_12__0_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000D0DD)) 
    \dc_bias[3]_i_13__0 
       (.I0(Q[2]),
        .I1(\dc_bias[3]_i_36_n_0 ),
        .I2(\dc_bias[3]_i_68_n_0 ),
        .I3(\dc_bias[3]_i_20__0_n_0 ),
        .I4(\dc_bias[3]_i_67_n_0 ),
        .I5(\dc_bias[3]_i_21_n_0 ),
        .O(\dc_bias[3]_i_13__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \dc_bias[3]_i_14 
       (.I0(Q[1]),
        .I1(\dc_bias[3]_i_63_n_0 ),
        .I2(\dc_bias[3]_i_22_n_0 ),
        .O(\dc_bias[3]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT5 #(
    .INIT(32'h00000100)) 
    \dc_bias[3]_i_16__0 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[5]),
        .I3(Q[2]),
        .I4(Q[3]),
        .O(\dc_bias[3]_i_16__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \dc_bias[3]_i_17__0 
       (.I0(Q[8]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(\dc_bias[3]_i_17__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAAAAAAE)) 
    \dc_bias[3]_i_19__0 
       (.I0(\processQ_reg[8]_0 ),
        .I1(\dc_bias[3]_i_16__0_n_0 ),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[8]),
        .I5(\processQ_reg[7]_0 ),
        .O(\dc_bias[3]_i_19__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \dc_bias[3]_i_1__1 
       (.I0(\encoded_reg[9] ),
        .O(\dc_bias_reg[0]_0 ));
  LUT5 #(
    .INIT(32'h00FCFE00)) 
    \dc_bias[3]_i_2 
       (.I0(\dc_bias_reg[3]_1 [0]),
        .I1(\encoded_reg[9]_0 ),
        .I2(\processQ_reg[1]_0 ),
        .I3(\encoded_reg[0] ),
        .I4(\dc_bias_reg[3]_1 [1]),
        .O(\dc_bias_reg[3] ));
  LUT6 #(
    .INIT(64'h55FF75FF55FFFFAE)) 
    \dc_bias[3]_i_20 
       (.I0(Q[8]),
        .I1(Q[4]),
        .I2(\dc_bias[3]_i_46_n_0 ),
        .I3(Q[7]),
        .I4(Q[6]),
        .I5(Q[5]),
        .O(\dc_bias[3]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \dc_bias[3]_i_20__0 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[4]),
        .O(\dc_bias[3]_i_20__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010820200)) 
    \dc_bias[3]_i_21 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[7]),
        .I3(Q[8]),
        .I4(Q[4]),
        .I5(\dc_bias[3]_i_26_n_0 ),
        .O(\dc_bias[3]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFCCEECCFC)) 
    \dc_bias[3]_i_22 
       (.I0(\dc_bias[3]_i_62_n_0 ),
        .I1(Q[2]),
        .I2(\dc_bias[3]_i_27_n_0 ),
        .I3(Q[3]),
        .I4(Q[4]),
        .I5(\dc_bias[3]_i_60_n_0 ),
        .O(\dc_bias[3]_i_22_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'h557FFF7F)) 
    \dc_bias[3]_i_23__0 
       (.I0(Q[0]),
        .I1(\processQ_reg[9]_3 ),
        .I2(\processQ_reg[9]_4 ),
        .I3(Q[1]),
        .I4(\trigger_time_s_reg[8] ),
        .O(\dc_bias_reg[1]_24 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT4 #(
    .INIT(16'hFDFF)) 
    \dc_bias[3]_i_25__0 
       (.I0(Q[4]),
        .I1(Q[8]),
        .I2(Q[9]),
        .I3(\dc_bias[3]_i_16__0_n_0 ),
        .O(\dc_bias_reg[1]_23 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \dc_bias[3]_i_26 
       (.I0(Q[2]),
        .I1(Q[3]),
        .O(\dc_bias[3]_i_26_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \dc_bias[3]_i_27 
       (.I0(Q[7]),
        .I1(Q[8]),
        .I2(Q[5]),
        .I3(Q[6]),
        .O(\dc_bias[3]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'hBFFFAAAAAAAAAAAA)) 
    \dc_bias[3]_i_28 
       (.I0(Q[0]),
        .I1(Q[7]),
        .I2(Q[8]),
        .I3(Q[5]),
        .I4(Q[2]),
        .I5(\dc_bias[3]_i_53_n_0 ),
        .O(\dc_bias[3]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAAAABEA)) 
    \dc_bias[3]_i_29 
       (.I0(\dc_bias[3]_i_54_n_0 ),
        .I1(Q[6]),
        .I2(Q[4]),
        .I3(Q[3]),
        .I4(\dc_bias[3]_i_55_n_0 ),
        .I5(Q[2]),
        .O(\dc_bias[3]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \dc_bias[3]_i_2__1 
       (.I0(\dc_bias[3]_i_8_n_0 ),
        .I1(\processQ_reg[1]_2 ),
        .I2(\dc_bias[3]_i_12__0_n_0 ),
        .I3(\dc_bias[3]_i_11__0_n_0 ),
        .I4(\processQ_reg[1]_1 ),
        .I5(\dc_bias[3]_i_10_n_0 ),
        .O(\encoded_reg[9]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h15555555)) 
    \dc_bias[3]_i_3 
       (.I0(\processQ_reg[7]_0 ),
        .I1(Q[5]),
        .I2(Q[8]),
        .I3(Q[6]),
        .I4(Q[7]),
        .O(\encoded_reg[9] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEEEEEEE)) 
    \dc_bias[3]_i_33 
       (.I0(\dc_bias[3]_i_58_n_0 ),
        .I1(\processQ_reg[5]_1 ),
        .I2(Q[7]),
        .I3(Q[6]),
        .I4(Q[8]),
        .I5(\processQ_reg[7]_0 ),
        .O(\dc_bias[3]_i_33_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EFEEEEEE)) 
    \dc_bias[3]_i_34 
       (.I0(\dc_bias[3]_i_60_n_0 ),
        .I1(\dc_bias[3]_i_61_n_0 ),
        .I2(Q[3]),
        .I3(\dc_bias[3]_i_62_n_0 ),
        .I4(Q[4]),
        .I5(\dc_bias[3]_i_63_n_0 ),
        .O(\dc_bias[3]_i_34_n_0 ));
  LUT6 #(
    .INIT(64'hFEF7FFEBFDBF7FF7)) 
    \dc_bias[3]_i_36 
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[8]),
        .I3(Q[6]),
        .I4(Q[5]),
        .I5(Q[7]),
        .O(\dc_bias[3]_i_36_n_0 ));
  LUT6 #(
    .INIT(64'hCCEECCCCCCEECCCF)) 
    \dc_bias[3]_i_37 
       (.I0(\dc_bias[3]_i_66_n_0 ),
        .I1(\dc_bias[3]_i_67_n_0 ),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(Q[3]),
        .I5(\dc_bias[3]_i_68_n_0 ),
        .O(\dc_bias[3]_i_37_n_0 ));
  LUT6 #(
    .INIT(64'hF0FEFEFEF0F0FEFE)) 
    \dc_bias[3]_i_4 
       (.I0(\dc_bias[3]_i_7_n_0 ),
        .I1(\dc_bias[3]_i_10_n_0 ),
        .I2(\processQ_reg[1]_1 ),
        .I3(\processQ_reg[9]_0 ),
        .I4(\dc_bias_reg[1]_5 ),
        .I5(\processQ_reg[9]_1 ),
        .O(\encoded_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hFFEFEFEFEFEFEFEF)) 
    \dc_bias[3]_i_41 
       (.I0(\dc_bias[3]_i_77_n_0 ),
        .I1(Q[5]),
        .I2(\processQ[6]_i_2__0_n_0 ),
        .I3(Q[2]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\encoded_reg[8]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \dc_bias[3]_i_46 
       (.I0(Q[2]),
        .I1(Q[3]),
        .O(\dc_bias[3]_i_46_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \dc_bias[3]_i_4__0 
       (.I0(CO),
        .I1(switch[0]),
        .I2(\dc_bias[3]_i_8_n_0 ),
        .I3(\processQ_reg[1]_1 ),
        .O(\encoded_reg[0] ));
  LUT6 #(
    .INIT(64'hFBFBFBFBF8FBFBFB)) 
    \dc_bias[3]_i_53 
       (.I0(\dc_bias[3]_i_82_n_0 ),
        .I1(Q[1]),
        .I2(Q[5]),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(\dc_bias[3]_i_83_n_0 ),
        .O(\dc_bias[3]_i_53_n_0 ));
  LUT6 #(
    .INIT(64'h0000000070000007)) 
    \dc_bias[3]_i_54 
       (.I0(\dc_bias[3]_i_84_n_0 ),
        .I1(\dc_bias[3]_i_85_n_0 ),
        .I2(Q[5]),
        .I3(Q[3]),
        .I4(Q[4]),
        .I5(Q[1]),
        .O(\dc_bias[3]_i_54_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT5 #(
    .INIT(32'h27FFFFFF)) 
    \dc_bias[3]_i_55 
       (.I0(Q[6]),
        .I1(Q[8]),
        .I2(Q[7]),
        .I3(Q[5]),
        .I4(Q[1]),
        .O(\dc_bias[3]_i_55_n_0 ));
  LUT6 #(
    .INIT(64'h8282C00082820300)) 
    \dc_bias[3]_i_58 
       (.I0(\dc_bias[3]_i_90_n_0 ),
        .I1(Q[8]),
        .I2(Q[7]),
        .I3(\dc_bias[3]_i_91_n_0 ),
        .I4(Q[5]),
        .I5(Q[6]),
        .O(\dc_bias[3]_i_58_n_0 ));
  LUT6 #(
    .INIT(64'hCCCCCCCCFFFFFFEF)) 
    \dc_bias[3]_i_5__0 
       (.I0(\dc_bias[3]_i_10_n_0 ),
        .I1(\processQ_reg[1]_1 ),
        .I2(\dc_bias[3]_i_11__0_n_0 ),
        .I3(\dc_bias[3]_i_12__0_n_0 ),
        .I4(\processQ_reg[1]_2 ),
        .I5(\dc_bias[3]_i_8_n_0 ),
        .O(\encoded_reg[3] ));
  LUT6 #(
    .INIT(64'h0000020020080020)) 
    \dc_bias[3]_i_60 
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(Q[6]),
        .I4(Q[8]),
        .I5(Q[7]),
        .O(\dc_bias[3]_i_60_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAABA)) 
    \dc_bias[3]_i_61 
       (.I0(Q[2]),
        .I1(\dc_bias[3]_i_93_n_0 ),
        .I2(Q[5]),
        .I3(Q[6]),
        .I4(Q[3]),
        .I5(Q[4]),
        .O(\dc_bias[3]_i_61_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h0920)) 
    \dc_bias[3]_i_62 
       (.I0(Q[7]),
        .I1(Q[5]),
        .I2(Q[8]),
        .I3(Q[6]),
        .O(\dc_bias[3]_i_62_n_0 ));
  LUT6 #(
    .INIT(64'h2AAA000022A20000)) 
    \dc_bias[3]_i_63 
       (.I0(\dc_bias[3]_i_94_n_0 ),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(\dc_bias[3]_i_95_n_0 ),
        .I4(Q[2]),
        .I5(\dc_bias[3]_i_68_n_0 ),
        .O(\dc_bias[3]_i_63_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT5 #(
    .INIT(32'h20060080)) 
    \dc_bias[3]_i_66 
       (.I0(Q[4]),
        .I1(Q[8]),
        .I2(Q[7]),
        .I3(Q[5]),
        .I4(Q[6]),
        .O(\dc_bias[3]_i_66_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'h0018FFFF)) 
    \dc_bias[3]_i_67 
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[5]),
        .I3(\dc_bias[3]_i_96_n_0 ),
        .I4(Q[1]),
        .O(\dc_bias[3]_i_67_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT4 #(
    .INIT(16'hFEBF)) 
    \dc_bias[3]_i_68 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[8]),
        .I3(Q[7]),
        .O(\dc_bias[3]_i_68_n_0 ));
  LUT6 #(
    .INIT(64'h545454FF54545454)) 
    \dc_bias[3]_i_7 
       (.I0(\encoded_reg[8]_1 ),
        .I1(\processQ_reg[1]_3 ),
        .I2(\processQ_reg[1]_4 ),
        .I3(\dc_bias[3]_i_13__0_n_0 ),
        .I4(\processQ_reg[5]_0 ),
        .I5(\dc_bias[3]_i_14_n_0 ),
        .O(\dc_bias[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFEFEFF)) 
    \dc_bias[3]_i_77 
       (.I0(Q[8]),
        .I1(Q[9]),
        .I2(\dc_bias[3]_i_83_n_0 ),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(\processQ_reg[7]_1 ),
        .O(\dc_bias[3]_i_77_n_0 ));
  LUT5 #(
    .INIT(32'h10000000)) 
    \dc_bias[3]_i_8 
       (.I0(\processQ_reg[0]_0 ),
        .I1(\dc_bias[3]_i_19__0_n_0 ),
        .I2(\dc_bias[3]_i_20_n_0 ),
        .I3(\processQ_reg[9]_2 ),
        .I4(switch[1]),
        .O(\dc_bias[3]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \dc_bias[3]_i_82 
       (.I0(Q[7]),
        .I1(Q[4]),
        .I2(Q[8]),
        .I3(Q[3]),
        .I4(Q[6]),
        .O(\dc_bias[3]_i_82_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \dc_bias[3]_i_83 
       (.I0(Q[6]),
        .I1(Q[7]),
        .O(\dc_bias[3]_i_83_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT4 #(
    .INIT(16'hFBFF)) 
    \dc_bias[3]_i_84 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[8]),
        .I3(Q[5]),
        .O(\dc_bias[3]_i_84_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'hFDFF)) 
    \dc_bias[3]_i_85 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[7]),
        .I3(Q[8]),
        .O(\dc_bias[3]_i_85_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000002202)) 
    \dc_bias[3]_i_9 
       (.I0(\dc_bias[3]_i_20_n_0 ),
        .I1(\processQ_reg[8]_0 ),
        .I2(\dc_bias[3]_i_16__0_n_0 ),
        .I3(\dc_bias[3]_i_17__0_n_0 ),
        .I4(\processQ_reg[7]_0 ),
        .I5(\processQ_reg[0]_0 ),
        .O(\dc_bias_reg[1]_5 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAA8880)) 
    \dc_bias[3]_i_90 
       (.I0(Q[7]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(Q[4]),
        .O(\dc_bias[3]_i_90_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'h1F)) 
    \dc_bias[3]_i_91 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[4]),
        .O(\dc_bias[3]_i_91_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \dc_bias[3]_i_93 
       (.I0(Q[8]),
        .I1(Q[7]),
        .O(\dc_bias[3]_i_93_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFBFFFFBFFEFF)) 
    \dc_bias[3]_i_94 
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[7]),
        .I3(Q[8]),
        .I4(Q[5]),
        .I5(Q[6]),
        .O(\dc_bias[3]_i_94_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h0082)) 
    \dc_bias[3]_i_95 
       (.I0(Q[5]),
        .I1(Q[6]),
        .I2(Q[8]),
        .I3(Q[7]),
        .O(\dc_bias[3]_i_95_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    \dc_bias[3]_i_96 
       (.I0(Q[8]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .O(\dc_bias[3]_i_96_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'h1DD1)) 
    \encoded[0]_i_1 
       (.I0(\processQ_reg[6]_0 ),
        .I1(\encoded_reg[9] ),
        .I2(\dc_bias_reg[3]_1 [2]),
        .I3(\encoded_reg[0] ),
        .O(\encoded_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'h80088080)) 
    \encoded[3]_i_1 
       (.I0(\dc_bias_reg[3]_2 ),
        .I1(\encoded_reg[9] ),
        .I2(\dc_bias_reg[3]_3 ),
        .I3(\encoded_reg[3] ),
        .I4(\encoded_reg[0] ),
        .O(encoded1_in));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'hAEEA)) 
    \encoded[4]_i_1 
       (.I0(\processQ_reg[6]_0 ),
        .I1(\encoded_reg[9] ),
        .I2(\dc_bias_reg[3]_1 [2]),
        .I3(\encoded_reg[0] ),
        .O(\encoded_reg[4] ));
  LUT4 #(
    .INIT(16'h2AAE)) 
    \encoded[8]_i_2__1 
       (.I0(\processQ_reg[6]_0 ),
        .I1(\encoded_reg[9] ),
        .I2(\dc_bias_reg[0]_1 ),
        .I3(\encoded_reg[8]_0 ),
        .O(\encoded_reg[8] ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \encoded[9]_i_2 
       (.I0(Q[4]),
        .I1(Q[8]),
        .I2(Q[9]),
        .I3(\encoded[9]_i_3_n_0 ),
        .O(\encoded[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF7FFFFFFFFFFFFFF)) 
    \encoded[9]_i_3 
       (.I0(Q[5]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(\encoded[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hC02AAABF80002AAA)) 
    i__carry__0_i_1__1
       (.I0(Q[9]),
        .I1(\trigger_volt_s_reg[4] ),
        .I2(\trigger_volt_s_reg[9] [7]),
        .I3(\trigger_volt_s_reg[9] [8]),
        .I4(\trigger_volt_s_reg[9] [9]),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_3 ));
  LUT6 #(
    .INIT(64'hC02AAABF80002AAA)) 
    i__carry__0_i_1__2
       (.I0(Q[9]),
        .I1(\trigger_volt_s_reg[4]_1 ),
        .I2(\trigger_volt_s_reg[9] [7]),
        .I3(\trigger_volt_s_reg[9] [8]),
        .I4(\trigger_volt_s_reg[9] [9]),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_12 ));
  LUT6 #(
    .INIT(64'hC02AAABF80002AAA)) 
    i__carry__0_i_1__3
       (.I0(Q[9]),
        .I1(\trigger_volt_s_reg[4]_2 ),
        .I2(\trigger_volt_s_reg[9] [7]),
        .I3(\trigger_volt_s_reg[9] [8]),
        .I4(\trigger_volt_s_reg[9] [9]),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_14 ));
  LUT6 #(
    .INIT(64'h155540003FD55540)) 
    i__carry__0_i_1__4
       (.I0(Q[9]),
        .I1(\trigger_volt_s_reg[4]_3 ),
        .I2(\trigger_volt_s_reg[9] [7]),
        .I3(\trigger_volt_s_reg[9] [8]),
        .I4(\trigger_volt_s_reg[9] [9]),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_16 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__1
       (.I0(\trigger_volt_s_reg[9] [9]),
        .I1(Q[9]),
        .I2(\trigger_volt_s_reg[9] [8]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(\trigger_volt_s_reg[4] ),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_4 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__2
       (.I0(\trigger_volt_s_reg[9] [9]),
        .I1(Q[9]),
        .I2(\trigger_volt_s_reg[9] [8]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(\trigger_volt_s_reg[4]_1 ),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_13 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__3
       (.I0(\trigger_volt_s_reg[9] [9]),
        .I1(Q[9]),
        .I2(\trigger_volt_s_reg[9] [8]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(\trigger_volt_s_reg[4]_2 ),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_15 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__4
       (.I0(\trigger_volt_s_reg[9] [9]),
        .I1(Q[9]),
        .I2(\trigger_volt_s_reg[9] [8]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(\trigger_volt_s_reg[4]_3 ),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_17 ));
  LUT5 #(
    .INIT(32'h7F80807F)) 
    i__carry_i_1__10
       (.I0(\trigger_volt_s_reg[4]_0 ),
        .I1(\trigger_volt_s_reg[9] [7]),
        .I2(\trigger_volt_s_reg[9] [8]),
        .I3(\trigger_volt_s_reg[9] [9]),
        .I4(Q[9]),
        .O(\dc_bias_reg[1]_11 [3]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_1__2
       (.I0(Q[7]),
        .I1(\trigger_volt_s_reg[5] ),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(Q[6]),
        .O(\dc_bias_reg[1] [3]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_1__3
       (.I0(Q[7]),
        .I1(\trigger_volt_s_reg[5]_0 ),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(Q[6]),
        .O(DI[3]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_1__4
       (.I0(Q[7]),
        .I1(\trigger_volt_s_reg[5]_1 ),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(Q[6]),
        .O(\dc_bias_reg[1]_8 [3]));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i__carry_i_1__5
       (.I0(Q[7]),
        .I1(\trigger_volt_s_reg[5]_2 ),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(Q[6]),
        .O(\dc_bias_reg[1]_2 [3]));
  LUT6 #(
    .INIT(64'hCBCCA2AA8A882022)) 
    i__carry_i_2__1
       (.I0(Q[5]),
        .I1(\trigger_volt_s_reg[9] [4]),
        .I2(\trigger_volt_s_reg[0] ),
        .I3(\trigger_volt_s_reg[2] ),
        .I4(\trigger_volt_s_reg[9] [5]),
        .I5(Q[4]),
        .O(\dc_bias_reg[1] [2]));
  LUT6 #(
    .INIT(64'h0000000006909009)) 
    i__carry_i_2__2
       (.I0(\trigger_volt_s_reg[9] [8]),
        .I1(Q[8]),
        .I2(Q[7]),
        .I3(\trigger_volt_s_reg[4]_0 ),
        .I4(\trigger_volt_s_reg[9] [7]),
        .I5(i__carry_i_6__2_n_0),
        .O(\dc_bias_reg[1]_11 [2]));
  LUT6 #(
    .INIT(64'hBCCC2AAAA8880222)) 
    i__carry_i_2__3
       (.I0(Q[5]),
        .I1(\trigger_volt_s_reg[9] [4]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(\trigger_volt_s_reg[2] ),
        .I4(\trigger_volt_s_reg[9] [5]),
        .I5(Q[4]),
        .O(DI[2]));
  LUT5 #(
    .INIT(32'hBC2AA802)) 
    i__carry_i_2__4
       (.I0(Q[5]),
        .I1(\trigger_volt_s_reg[9] [4]),
        .I2(\trigger_volt_s_reg[3] ),
        .I3(\trigger_volt_s_reg[9] [5]),
        .I4(Q[4]),
        .O(\dc_bias_reg[1]_8 [2]));
  LUT5 #(
    .INIT(32'h015443D5)) 
    i__carry_i_2__5
       (.I0(Q[5]),
        .I1(\trigger_volt_s_reg[9] [4]),
        .I2(\trigger_volt_s_reg[3]_0 ),
        .I3(\trigger_volt_s_reg[9] [5]),
        .I4(Q[4]),
        .O(\dc_bias_reg[1]_2 [2]));
  LUT6 #(
    .INIT(64'hE1E0E000FF1F0100)) 
    i__carry_i_3__1
       (.I0(\trigger_volt_s_reg[9] [0]),
        .I1(\trigger_volt_s_reg[9] [1]),
        .I2(\trigger_volt_s_reg[9] [2]),
        .I3(Q[2]),
        .I4(Q[3]),
        .I5(\trigger_volt_s_reg[9] [3]),
        .O(\dc_bias_reg[1] [1]));
  LUT6 #(
    .INIT(64'h000155544443DDD5)) 
    i__carry_i_3__2
       (.I0(Q[3]),
        .I1(\trigger_volt_s_reg[9] [2]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(\trigger_volt_s_reg[9] [0]),
        .I4(\trigger_volt_s_reg[9] [3]),
        .I5(Q[2]),
        .O(\dc_bias_reg[1]_2 [1]));
  LUT6 #(
    .INIT(64'hC222ABBB80002AAA)) 
    i__carry_i_3__3
       (.I0(Q[3]),
        .I1(\trigger_volt_s_reg[9] [2]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(\trigger_volt_s_reg[9] [0]),
        .I4(\trigger_volt_s_reg[9] [3]),
        .I5(Q[2]),
        .O(\dc_bias_reg[1]_8 [1]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_3__4
       (.I0(Q[3]),
        .I1(\trigger_volt_s_reg[9] [1]),
        .I2(\trigger_volt_s_reg[9] [2]),
        .I3(\trigger_volt_s_reg[9] [3]),
        .I4(Q[2]),
        .O(DI[1]));
  LUT6 #(
    .INIT(64'h00000000556AAA95)) 
    i__carry_i_3__5
       (.I0(\trigger_volt_s_reg[9] [5]),
        .I1(\trigger_volt_s_reg[9] [2]),
        .I2(\trigger_volt_s_reg[9] [3]),
        .I3(\trigger_volt_s_reg[9] [4]),
        .I4(Q[5]),
        .I5(i__carry_i_7__5_n_0),
        .O(\dc_bias_reg[1]_11 [1]));
  LUT4 #(
    .INIT(16'h4147)) 
    i__carry_i_4__0
       (.I0(Q[1]),
        .I1(\trigger_volt_s_reg[9] [1]),
        .I2(\trigger_volt_s_reg[9] [0]),
        .I3(Q[0]),
        .O(\dc_bias_reg[1]_2 [0]));
  LUT4 #(
    .INIT(16'hF220)) 
    i__carry_i_4__1
       (.I0(Q[0]),
        .I1(\trigger_volt_s_reg[9] [0]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(Q[1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'hE640)) 
    i__carry_i_4__2
       (.I0(\trigger_volt_s_reg[9] [1]),
        .I1(\trigger_volt_s_reg[9] [0]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\dc_bias_reg[1] [0]));
  LUT4 #(
    .INIT(16'hE822)) 
    i__carry_i_4__3
       (.I0(Q[1]),
        .I1(\trigger_volt_s_reg[9] [1]),
        .I2(Q[0]),
        .I3(\trigger_volt_s_reg[9] [0]),
        .O(\dc_bias_reg[1]_8 [0]));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    i__carry_i_4__4
       (.I0(Q[0]),
        .I1(\trigger_volt_s_reg[9] [0]),
        .I2(\trigger_volt_s_reg[9] [2]),
        .I3(Q[2]),
        .I4(\trigger_volt_s_reg[9] [1]),
        .I5(Q[1]),
        .O(\dc_bias_reg[1]_11 [0]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__1
       (.I0(\trigger_volt_s_reg[9] [7]),
        .I1(Q[7]),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[5] ),
        .I4(Q[6]),
        .O(\dc_bias_reg[1]_1 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__2
       (.I0(\trigger_volt_s_reg[9] [7]),
        .I1(Q[7]),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[5]_0 ),
        .I4(Q[6]),
        .O(S[3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__3
       (.I0(\trigger_volt_s_reg[9] [7]),
        .I1(Q[7]),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[5]_1 ),
        .I4(Q[6]),
        .O(\dc_bias_reg[1]_10 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__4
       (.I0(\trigger_volt_s_reg[9] [7]),
        .I1(Q[7]),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[5]_2 ),
        .I4(Q[6]),
        .O(\dc_bias_reg[1]_0 [3]));
  LUT6 #(
    .INIT(64'h0960090960066060)) 
    i__carry_i_6__1
       (.I0(\trigger_volt_s_reg[9] [5]),
        .I1(Q[5]),
        .I2(\trigger_volt_s_reg[9] [4]),
        .I3(\trigger_volt_s_reg[0] ),
        .I4(\trigger_volt_s_reg[2] ),
        .I5(Q[4]),
        .O(\dc_bias_reg[1]_1 [2]));
  LUT6 #(
    .INIT(64'h9999955566666AAA)) 
    i__carry_i_6__2
       (.I0(Q[6]),
        .I1(\trigger_volt_s_reg[9] [5]),
        .I2(\trigger_volt_s_reg[9] [2]),
        .I3(\trigger_volt_s_reg[9] [3]),
        .I4(\trigger_volt_s_reg[9] [4]),
        .I5(\trigger_volt_s_reg[9] [6]),
        .O(i__carry_i_6__2_n_0));
  LUT6 #(
    .INIT(64'h6009090906606060)) 
    i__carry_i_6__3
       (.I0(\trigger_volt_s_reg[9] [5]),
        .I1(Q[5]),
        .I2(\trigger_volt_s_reg[9] [4]),
        .I3(\trigger_volt_s_reg[9] [1]),
        .I4(\trigger_volt_s_reg[2] ),
        .I5(Q[4]),
        .O(S[2]));
  LUT5 #(
    .INIT(32'h60090660)) 
    i__carry_i_6__4
       (.I0(\trigger_volt_s_reg[9] [5]),
        .I1(Q[5]),
        .I2(\trigger_volt_s_reg[9] [4]),
        .I3(\trigger_volt_s_reg[3] ),
        .I4(Q[4]),
        .O(\dc_bias_reg[1]_10 [2]));
  LUT5 #(
    .INIT(32'h60090660)) 
    i__carry_i_6__5
       (.I0(\trigger_volt_s_reg[9] [5]),
        .I1(Q[5]),
        .I2(\trigger_volt_s_reg[9] [4]),
        .I3(\trigger_volt_s_reg[3]_0 ),
        .I4(Q[4]),
        .O(\dc_bias_reg[1]_0 [2]));
  LUT6 #(
    .INIT(64'h2221111888844442)) 
    i__carry_i_7__1
       (.I0(Q[2]),
        .I1(\trigger_volt_s_reg[9] [3]),
        .I2(\trigger_volt_s_reg[9] [0]),
        .I3(\trigger_volt_s_reg[9] [1]),
        .I4(\trigger_volt_s_reg[9] [2]),
        .I5(Q[3]),
        .O(\dc_bias_reg[1]_0 [1]));
  LUT6 #(
    .INIT(64'h0909099060606009)) 
    i__carry_i_7__2
       (.I0(\trigger_volt_s_reg[9] [3]),
        .I1(Q[3]),
        .I2(\trigger_volt_s_reg[9] [2]),
        .I3(\trigger_volt_s_reg[9] [1]),
        .I4(\trigger_volt_s_reg[9] [0]),
        .I5(Q[2]),
        .O(\dc_bias_reg[1]_1 [1]));
  LUT6 #(
    .INIT(64'h1888844442222111)) 
    i__carry_i_7__3
       (.I0(Q[2]),
        .I1(\trigger_volt_s_reg[9] [3]),
        .I2(\trigger_volt_s_reg[9] [0]),
        .I3(\trigger_volt_s_reg[9] [1]),
        .I4(\trigger_volt_s_reg[9] [2]),
        .I5(Q[3]),
        .O(\dc_bias_reg[1]_10 [1]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_7__4
       (.I0(\trigger_volt_s_reg[9] [3]),
        .I1(Q[3]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(\trigger_volt_s_reg[9] [2]),
        .I4(Q[2]),
        .O(S[1]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hBDDEE77B)) 
    i__carry_i_7__5
       (.I0(Q[3]),
        .I1(\trigger_volt_s_reg[9] [4]),
        .I2(\trigger_volt_s_reg[9] [3]),
        .I3(\trigger_volt_s_reg[9] [2]),
        .I4(Q[4]),
        .O(i__carry_i_7__5_n_0));
  LUT4 #(
    .INIT(16'h4224)) 
    i__carry_i_8
       (.I0(Q[0]),
        .I1(\trigger_volt_s_reg[9] [0]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(Q[1]),
        .O(\dc_bias_reg[1]_0 [0]));
  LUT4 #(
    .INIT(16'h0690)) 
    i__carry_i_8__0
       (.I0(\trigger_volt_s_reg[9] [1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\trigger_volt_s_reg[9] [0]),
        .O(\dc_bias_reg[1]_10 [0]));
  LUT4 #(
    .INIT(16'h0990)) 
    i__carry_i_8__5
       (.I0(\trigger_volt_s_reg[9] [0]),
        .I1(Q[0]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(Q[1]),
        .O(S[0]));
  LUT4 #(
    .INIT(16'h4224)) 
    i__carry_i_8__6
       (.I0(Q[0]),
        .I1(\trigger_volt_s_reg[9] [0]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(Q[1]),
        .O(\dc_bias_reg[1]_1 [0]));
  LUT6 #(
    .INIT(64'h155540003FD55540)) 
    pixel_color4_carry__0_i_1
       (.I0(Q[9]),
        .I1(\trigger_volt_s_reg[4]_4 ),
        .I2(\trigger_volt_s_reg[9] [7]),
        .I3(\trigger_volt_s_reg[9] [8]),
        .I4(\trigger_volt_s_reg[9] [9]),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_18 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    pixel_color4_carry__0_i_2
       (.I0(\trigger_volt_s_reg[9] [9]),
        .I1(Q[9]),
        .I2(\trigger_volt_s_reg[9] [8]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(\trigger_volt_s_reg[4]_4 ),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_19 ));
  LUT5 #(
    .INIT(32'h15403D54)) 
    pixel_color4_carry_i_1
       (.I0(Q[7]),
        .I1(\trigger_volt_s_reg[5]_4 ),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(Q[6]),
        .O(\dc_bias_reg[1]_6 [3]));
  LUT5 #(
    .INIT(32'h015443D5)) 
    pixel_color4_carry_i_2
       (.I0(Q[5]),
        .I1(\trigger_volt_s_reg[9] [4]),
        .I2(\trigger_volt_s_reg[3]_1 ),
        .I3(\trigger_volt_s_reg[9] [5]),
        .I4(Q[4]),
        .O(\dc_bias_reg[1]_6 [2]));
  LUT6 #(
    .INIT(64'h011154444333D555)) 
    pixel_color4_carry_i_3
       (.I0(Q[3]),
        .I1(\trigger_volt_s_reg[9] [2]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(\trigger_volt_s_reg[9] [0]),
        .I4(\trigger_volt_s_reg[9] [3]),
        .I5(Q[2]),
        .O(\dc_bias_reg[1]_6 [1]));
  LUT4 #(
    .INIT(16'h141D)) 
    pixel_color4_carry_i_4
       (.I0(Q[1]),
        .I1(\trigger_volt_s_reg[9] [1]),
        .I2(\trigger_volt_s_reg[9] [0]),
        .I3(Q[0]),
        .O(\dc_bias_reg[1]_6 [0]));
  LUT5 #(
    .INIT(32'h09906009)) 
    pixel_color4_carry_i_5
       (.I0(\trigger_volt_s_reg[9] [7]),
        .I1(Q[7]),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[5]_4 ),
        .I4(Q[6]),
        .O(\dc_bias_reg[1]_22 [3]));
  LUT5 #(
    .INIT(32'h60090660)) 
    pixel_color4_carry_i_6
       (.I0(\trigger_volt_s_reg[9] [5]),
        .I1(Q[5]),
        .I2(\trigger_volt_s_reg[9] [4]),
        .I3(\trigger_volt_s_reg[3]_1 ),
        .I4(Q[4]),
        .O(\dc_bias_reg[1]_22 [2]));
  LUT6 #(
    .INIT(64'h6009090906606060)) 
    pixel_color4_carry_i_7
       (.I0(\trigger_volt_s_reg[9] [3]),
        .I1(Q[3]),
        .I2(\trigger_volt_s_reg[9] [2]),
        .I3(\trigger_volt_s_reg[9] [1]),
        .I4(\trigger_volt_s_reg[9] [0]),
        .I5(Q[2]),
        .O(\dc_bias_reg[1]_22 [1]));
  LUT4 #(
    .INIT(16'h0690)) 
    pixel_color4_carry_i_8
       (.I0(\trigger_volt_s_reg[9] [1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\trigger_volt_s_reg[9] [0]),
        .O(\dc_bias_reg[1]_22 [0]));
  LUT6 #(
    .INIT(64'h155540003FD55540)) 
    pixel_color5_carry__0_i_1
       (.I0(Q[9]),
        .I1(\trigger_volt_s_reg[4]_5 ),
        .I2(\trigger_volt_s_reg[9] [7]),
        .I3(\trigger_volt_s_reg[9] [8]),
        .I4(\trigger_volt_s_reg[9] [9]),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_20 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    pixel_color5_carry__0_i_2
       (.I0(\trigger_volt_s_reg[9] [9]),
        .I1(Q[9]),
        .I2(\trigger_volt_s_reg[9] [8]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(\trigger_volt_s_reg[4]_5 ),
        .I5(Q[8]),
        .O(\dc_bias_reg[1]_21 ));
  LUT5 #(
    .INIT(32'h15403D54)) 
    pixel_color5_carry_i_1
       (.I0(Q[7]),
        .I1(\trigger_volt_s_reg[5]_3 ),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[9] [7]),
        .I4(Q[6]),
        .O(\dc_bias_reg[1]_9 [3]));
  LUT6 #(
    .INIT(64'h1011454434335D55)) 
    pixel_color5_carry_i_2
       (.I0(Q[5]),
        .I1(\trigger_volt_s_reg[9] [4]),
        .I2(\trigger_volt_s_reg[1] ),
        .I3(\trigger_volt_s_reg[9] [3]),
        .I4(\trigger_volt_s_reg[9] [5]),
        .I5(Q[4]),
        .O(\dc_bias_reg[1]_9 [2]));
  LUT5 #(
    .INIT(32'h015443D5)) 
    pixel_color5_carry_i_3
       (.I0(Q[3]),
        .I1(\trigger_volt_s_reg[9] [1]),
        .I2(\trigger_volt_s_reg[9] [2]),
        .I3(\trigger_volt_s_reg[9] [3]),
        .I4(Q[2]),
        .O(\dc_bias_reg[1]_9 [1]));
  LUT4 #(
    .INIT(16'h022F)) 
    pixel_color5_carry_i_4
       (.I0(\trigger_volt_s_reg[9] [0]),
        .I1(Q[0]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(Q[1]),
        .O(\dc_bias_reg[1]_9 [0]));
  LUT5 #(
    .INIT(32'h09906009)) 
    pixel_color5_carry_i_5
       (.I0(\trigger_volt_s_reg[9] [7]),
        .I1(Q[7]),
        .I2(\trigger_volt_s_reg[9] [6]),
        .I3(\trigger_volt_s_reg[5]_3 ),
        .I4(Q[6]),
        .O(\dc_bias_reg[1]_7 [3]));
  LUT6 #(
    .INIT(64'h0960090960066060)) 
    pixel_color5_carry_i_6
       (.I0(\trigger_volt_s_reg[9] [5]),
        .I1(Q[5]),
        .I2(\trigger_volt_s_reg[9] [4]),
        .I3(\trigger_volt_s_reg[1] ),
        .I4(\trigger_volt_s_reg[9] [3]),
        .I5(Q[4]),
        .O(\dc_bias_reg[1]_7 [2]));
  LUT5 #(
    .INIT(32'h21188442)) 
    pixel_color5_carry_i_7
       (.I0(Q[2]),
        .I1(\trigger_volt_s_reg[9] [3]),
        .I2(\trigger_volt_s_reg[9] [2]),
        .I3(\trigger_volt_s_reg[9] [1]),
        .I4(Q[3]),
        .O(\dc_bias_reg[1]_7 [1]));
  LUT4 #(
    .INIT(16'h0990)) 
    pixel_color5_carry_i_8
       (.I0(\trigger_volt_s_reg[9] [0]),
        .I1(Q[0]),
        .I2(\trigger_volt_s_reg[9] [1]),
        .I3(Q[1]),
        .O(\dc_bias_reg[1]_7 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    \processQ[0]_i_1__0 
       (.I0(Q[0]),
        .O(plusOp__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \processQ[1]_i_1__1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(plusOp__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \processQ[2]_i_1__1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(\processQ[2]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processQ[3]_i_1__1 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .O(plusOp__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[4]_i_1__1 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[1]),
        .O(plusOp__2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processQ[5]_i_1__1 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(plusOp__2[5]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processQ[6]_i_1__1 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(\processQ[6]_i_2__0_n_0 ),
        .O(plusOp__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \processQ[6]_i_2__0 
       (.I0(Q[3]),
        .I1(Q[4]),
        .O(\processQ[6]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h6A)) 
    \processQ[7]_i_1__0 
       (.I0(Q[7]),
        .I1(\processQ[9]_i_7_n_0 ),
        .I2(Q[6]),
        .O(plusOp__2[7]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processQ[8]_i_1__1 
       (.I0(Q[8]),
        .I1(Q[6]),
        .I2(Q[7]),
        .I3(\processQ[9]_i_7_n_0 ),
        .O(plusOp__2[8]));
  LUT6 #(
    .INIT(64'h00008000AAAAAAAA)) 
    \processQ[9]_i_1 
       (.I0(SR),
        .I1(Q[9]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(\processQ[9]_i_4__1_n_0 ),
        .I5(reset_n),
        .O(\processQ[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000888AAAAAAAA)) 
    \processQ[9]_i_2__0 
       (.I0(\processQ_reg[6]_1 ),
        .I1(\processQ[9]_i_6__0_n_0 ),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[8]),
        .I5(Q[9]),
        .O(processQ0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[9]_i_3__1 
       (.I0(Q[9]),
        .I1(Q[8]),
        .I2(Q[6]),
        .I3(Q[7]),
        .I4(\processQ[9]_i_7_n_0 ),
        .O(plusOp__2[9]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    \processQ[9]_i_4__1 
       (.I0(\processQ[9]_i_8_n_0 ),
        .I1(Q[8]),
        .I2(Q[4]),
        .I3(Q[7]),
        .I4(Q[5]),
        .I5(Q[6]),
        .O(\processQ[9]_i_4__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \processQ[9]_i_6__0 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[7]),
        .I3(Q[4]),
        .O(\processQ[9]_i_6__0_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \processQ[9]_i_7 
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[1]),
        .I5(Q[5]),
        .O(\processQ[9]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \processQ[9]_i_8 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(\processQ[9]_i_8_n_0 ));
  FDRE \processQ_reg[0] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[0]),
        .Q(Q[0]),
        .R(\processQ[9]_i_1_n_0 ));
  FDRE \processQ_reg[1] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[1]),
        .Q(Q[1]),
        .R(\processQ[9]_i_1_n_0 ));
  FDRE \processQ_reg[2] 
       (.C(CLK),
        .CE(processQ0),
        .D(\processQ[2]_i_1__1_n_0 ),
        .Q(Q[2]),
        .R(\processQ[9]_i_1_n_0 ));
  FDRE \processQ_reg[3] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[3]),
        .Q(Q[3]),
        .R(\processQ[9]_i_1_n_0 ));
  FDRE \processQ_reg[4] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[4]),
        .Q(Q[4]),
        .R(\processQ[9]_i_1_n_0 ));
  FDRE \processQ_reg[5] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[5]),
        .Q(Q[5]),
        .R(\processQ[9]_i_1_n_0 ));
  FDRE \processQ_reg[6] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[6]),
        .Q(Q[6]),
        .R(\processQ[9]_i_1_n_0 ));
  FDRE \processQ_reg[7] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[7]),
        .Q(Q[7]),
        .R(\processQ[9]_i_1_n_0 ));
  FDRE \processQ_reg[8] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[8]),
        .Q(Q[8]),
        .R(\processQ[9]_i_1_n_0 ));
  FDRE \processQ_reg[9] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[9]),
        .Q(Q[9]),
        .R(\processQ[9]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga
   (\encoded_reg[8] ,
    \encoded_reg[9] ,
    \encoded_reg[8]_0 ,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    \processQ_reg[9] ,
    ADDRARDADDR,
    D,
    \encoded_reg[0] ,
    \dc_bias_reg[3] ,
    \encoded_reg[9]_0 ,
    \dc_bias_reg[1] ,
    encoded1_in,
    \encoded_reg[3] ,
    \dc_bias_reg[0] ,
    \encoded_reg[2] ,
    \encoded_reg[4] ,
    \encoded_reg[0]_0 ,
    \encoded_reg[1] ,
    SR,
    \encoded_reg[9]_1 ,
    \encoded_reg[9]_2 ,
    \encoded_reg[9]_3 ,
    Q,
    \trigger_volt_s_reg[9] ,
    \dc_bias_reg[0]_0 ,
    reset_n,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[0]_1 ,
    \processQ_reg[9]_0 ,
    \processQ_reg[9]_1 ,
    \dc_bias_reg[3]_2 ,
    \dc_bias_reg[3]_3 ,
    switch,
    CO,
    \processQ_reg[9]_2 ,
    \trigger_volt_s_reg[4] ,
    \trigger_time_s_reg[3] ,
    \trigger_time_s_reg[4] ,
    \trigger_time_s_reg[2] ,
    \trigger_time_s_reg[4]_0 ,
    \trigger_time_s_reg[5] ,
    \trigger_time_s_reg[2]_0 ,
    \trigger_time_s_reg[4]_1 ,
    \trigger_time_s_reg[5]_0 ,
    \trigger_time_s_reg[6] ,
    \dc_bias_reg[0]_2 ,
    \dc_bias_reg[3]_4 ,
    CLK);
  output \encoded_reg[8] ;
  output \encoded_reg[9] ;
  output \encoded_reg[8]_0 ;
  output [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  output [9:0]\processQ_reg[9] ;
  output [7:0]ADDRARDADDR;
  output [0:0]D;
  output \encoded_reg[0] ;
  output \dc_bias_reg[3] ;
  output \encoded_reg[9]_0 ;
  output \dc_bias_reg[1] ;
  output [0:0]encoded1_in;
  output \encoded_reg[3] ;
  output \dc_bias_reg[0] ;
  output \encoded_reg[2] ;
  output \encoded_reg[4] ;
  output \encoded_reg[0]_0 ;
  output \encoded_reg[1] ;
  output [0:0]SR;
  output \encoded_reg[9]_1 ;
  output \encoded_reg[9]_2 ;
  output \encoded_reg[9]_3 ;
  input [9:0]Q;
  input [9:0]\trigger_volt_s_reg[9] ;
  input \dc_bias_reg[0]_0 ;
  input reset_n;
  input \dc_bias_reg[3]_0 ;
  input [2:0]\dc_bias_reg[3]_1 ;
  input \dc_bias_reg[0]_1 ;
  input \processQ_reg[9]_0 ;
  input \processQ_reg[9]_1 ;
  input \dc_bias_reg[3]_2 ;
  input [0:0]\dc_bias_reg[3]_3 ;
  input [1:0]switch;
  input [0:0]CO;
  input [0:0]\processQ_reg[9]_2 ;
  input \trigger_volt_s_reg[4] ;
  input \trigger_time_s_reg[3] ;
  input \trigger_time_s_reg[4] ;
  input \trigger_time_s_reg[2] ;
  input \trigger_time_s_reg[4]_0 ;
  input \trigger_time_s_reg[5] ;
  input \trigger_time_s_reg[2]_0 ;
  input \trigger_time_s_reg[4]_1 ;
  input \trigger_time_s_reg[5]_0 ;
  input \trigger_time_s_reg[6] ;
  input \dc_bias_reg[0]_2 ;
  input [0:0]\dc_bias_reg[3]_4 ;
  input CLK;

  wire [7:0]ADDRARDADDR;
  wire CLK;
  wire [0:0]CO;
  wire [0:0]D;
  wire [9:0]Q;
  wire [0:0]SR;
  wire \dc_bias_reg[0] ;
  wire \dc_bias_reg[0]_0 ;
  wire \dc_bias_reg[0]_1 ;
  wire \dc_bias_reg[0]_2 ;
  wire \dc_bias_reg[1] ;
  wire \dc_bias_reg[3] ;
  wire \dc_bias_reg[3]_0 ;
  wire [2:0]\dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire [0:0]\dc_bias_reg[3]_3 ;
  wire [0:0]\dc_bias_reg[3]_4 ;
  wire [0:0]encoded1_in;
  wire \encoded_reg[0] ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[1] ;
  wire \encoded_reg[2] ;
  wire \encoded_reg[3] ;
  wire \encoded_reg[4] ;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[9] ;
  wire \encoded_reg[9]_0 ;
  wire \encoded_reg[9]_1 ;
  wire \encoded_reg[9]_2 ;
  wire \encoded_reg[9]_3 ;
  wire horiz_counter_n_11;
  wire horiz_counter_n_12;
  wire horiz_counter_n_13;
  wire horiz_counter_n_14;
  wire horiz_counter_n_15;
  wire horiz_counter_n_16;
  wire horiz_counter_n_17;
  wire horiz_counter_n_18;
  wire horiz_counter_n_19;
  wire horiz_counter_n_2;
  wire horiz_counter_n_20;
  wire horiz_counter_n_21;
  wire horiz_counter_n_22;
  wire horiz_counter_n_23;
  wire horiz_counter_n_24;
  wire horiz_counter_n_25;
  wire horiz_counter_n_26;
  wire horiz_counter_n_27;
  wire horiz_counter_n_28;
  wire horiz_counter_n_29;
  wire horiz_counter_n_30;
  wire horiz_counter_n_31;
  wire horiz_counter_n_33;
  wire horiz_counter_n_34;
  wire horiz_counter_n_35;
  wire horiz_counter_n_36;
  wire horiz_counter_n_38;
  wire horiz_counter_n_40;
  wire horiz_counter_n_41;
  wire horiz_counter_n_42;
  wire horiz_counter_n_43;
  wire horiz_counter_n_44;
  wire horiz_counter_n_45;
  wire horiz_counter_n_46;
  wire horiz_counter_n_47;
  wire horiz_counter_n_48;
  wire horiz_counter_n_49;
  wire horiz_counter_n_50;
  wire horiz_counter_n_51;
  wire horiz_counter_n_52;
  wire horiz_counter_n_53;
  wire horiz_counter_n_54;
  wire horiz_counter_n_55;
  wire horiz_counter_n_56;
  wire horiz_counter_n_57;
  wire horiz_counter_n_58;
  wire horiz_counter_n_59;
  wire horiz_counter_n_60;
  wire horiz_counter_n_61;
  wire horiz_counter_n_62;
  wire horiz_counter_n_63;
  wire horiz_counter_n_64;
  wire horiz_counter_n_65;
  wire horiz_counter_n_66;
  wire horiz_counter_n_67;
  wire horiz_counter_n_68;
  wire horiz_counter_n_69;
  wire horiz_counter_n_70;
  wire horiz_counter_n_71;
  wire horiz_counter_n_72;
  wire horiz_counter_n_73;
  wire horiz_counter_n_74;
  wire horiz_counter_n_75;
  wire horiz_counter_n_76;
  wire horiz_counter_n_77;
  wire horiz_counter_n_78;
  wire horiz_counter_n_79;
  wire horiz_counter_n_80;
  wire horiz_counter_n_81;
  wire horiz_counter_n_82;
  wire horiz_counter_n_83;
  wire horiz_counter_n_84;
  wire horiz_counter_n_85;
  wire horiz_counter_n_86;
  wire horiz_counter_n_87;
  wire horiz_counter_n_88;
  wire horiz_counter_n_89;
  wire horiz_counter_n_90;
  wire horiz_counter_n_91;
  wire horiz_counter_n_92;
  wire horiz_counter_n_93;
  wire pixel_color31_out;
  wire pixel_color424_in;
  wire pixel_color517_in;
  wire pixel_color523_in;
  wire pixel_color528_in;
  wire pixel_color532_in;
  wire pixel_color616_in;
  wire pixel_color627_in;
  wire pixel_color631_in;
  wire [9:0]\processQ_reg[9] ;
  wire \processQ_reg[9]_0 ;
  wire \processQ_reg[9]_1 ;
  wire [0:0]\processQ_reg[9]_2 ;
  wire reset_n;
  wire sF_n_10;
  wire sF_n_11;
  wire sF_n_12;
  wire sF_n_13;
  wire sF_n_14;
  wire sF_n_15;
  wire sF_n_16;
  wire sF_n_17;
  wire sF_n_18;
  wire sF_n_19;
  wire sF_n_20;
  wire sF_n_21;
  wire sF_n_22;
  wire sF_n_23;
  wire sF_n_24;
  wire sF_n_25;
  wire sF_n_26;
  wire sF_n_27;
  wire sF_n_28;
  wire sF_n_29;
  wire sF_n_30;
  wire sF_n_31;
  wire sF_n_32;
  wire sF_n_33;
  wire sF_n_34;
  wire sF_n_35;
  wire sF_n_36;
  wire sF_n_37;
  wire sF_n_38;
  wire sF_n_9;
  wire [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire [1:0]switch;
  wire \trigger_time_s_reg[2] ;
  wire \trigger_time_s_reg[2]_0 ;
  wire \trigger_time_s_reg[3] ;
  wire \trigger_time_s_reg[4] ;
  wire \trigger_time_s_reg[4]_0 ;
  wire \trigger_time_s_reg[4]_1 ;
  wire \trigger_time_s_reg[5] ;
  wire \trigger_time_s_reg[5]_0 ;
  wire \trigger_time_s_reg[6] ;
  wire \trigger_volt_s_reg[4] ;
  wire [9:0]\trigger_volt_s_reg[9] ;
  wire vert_counter_n_13;
  wire vert_counter_n_14;
  wire vert_counter_n_15;
  wire vert_counter_n_16;
  wire vert_counter_n_17;
  wire vert_counter_n_18;
  wire vert_counter_n_19;
  wire vert_counter_n_20;
  wire vert_counter_n_21;
  wire vert_counter_n_22;
  wire vert_counter_n_23;
  wire vert_counter_n_24;
  wire vert_counter_n_25;
  wire vert_counter_n_26;
  wire vert_counter_n_27;
  wire vert_counter_n_28;
  wire vert_counter_n_29;
  wire vert_counter_n_30;
  wire vert_counter_n_35;
  wire vert_counter_n_39;
  wire vert_counter_n_43;
  wire vert_counter_n_44;
  wire vert_counter_n_45;
  wire vert_counter_n_46;
  wire vert_counter_n_47;
  wire vert_counter_n_48;
  wire vert_counter_n_49;
  wire vert_counter_n_50;
  wire vert_counter_n_51;
  wire vert_counter_n_52;
  wire vert_counter_n_53;
  wire vert_counter_n_54;
  wire vert_counter_n_55;
  wire vert_counter_n_56;
  wire vert_counter_n_57;
  wire vert_counter_n_58;
  wire vert_counter_n_59;
  wire vert_counter_n_60;
  wire vert_counter_n_61;
  wire vert_counter_n_62;
  wire vert_counter_n_63;
  wire vert_counter_n_64;
  wire vert_counter_n_65;
  wire vert_counter_n_66;
  wire vert_counter_n_67;
  wire vert_counter_n_68;
  wire vert_counter_n_69;
  wire vert_counter_n_70;
  wire vert_counter_n_71;
  wire vert_counter_n_72;
  wire vert_counter_n_73;
  wire vert_counter_n_74;
  wire vert_counter_n_75;
  wire vert_counter_n_76;
  wire vert_counter_n_77;
  wire vert_counter_n_78;
  wire vert_counter_n_79;
  wire vert_counter_n_80;
  wire vert_counter_n_81;
  wire vert_counter_n_82;
  wire vert_counter_n_83;
  wire vert_counter_n_84;
  wire vert_counter_n_85;
  wire vert_counter_n_86;
  wire vert_counter_n_87;
  wire vert_counter_n_88;
  wire vert_counter_n_89;
  wire vert_counter_n_90;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_hcounter horiz_counter
       (.ADDRARDADDR(ADDRARDADDR),
        .CLK(CLK),
        .CO(CO),
        .DI({horiz_counter_n_15,horiz_counter_n_16,horiz_counter_n_17,horiz_counter_n_18}),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl ),
        .S({horiz_counter_n_11,horiz_counter_n_12,horiz_counter_n_13,horiz_counter_n_14}),
        .SR(horiz_counter_n_2),
        .\dc_bias_reg[1] (horiz_counter_n_19),
        .\dc_bias_reg[1]_0 (horiz_counter_n_20),
        .\dc_bias_reg[1]_1 ({horiz_counter_n_21,horiz_counter_n_22,horiz_counter_n_23,horiz_counter_n_24}),
        .\dc_bias_reg[1]_10 ({horiz_counter_n_50,horiz_counter_n_51,horiz_counter_n_52,horiz_counter_n_53}),
        .\dc_bias_reg[1]_11 ({horiz_counter_n_54,horiz_counter_n_55,horiz_counter_n_56,horiz_counter_n_57}),
        .\dc_bias_reg[1]_12 ({horiz_counter_n_58,horiz_counter_n_59,horiz_counter_n_60,horiz_counter_n_61}),
        .\dc_bias_reg[1]_13 ({horiz_counter_n_62,horiz_counter_n_63,horiz_counter_n_64,horiz_counter_n_65}),
        .\dc_bias_reg[1]_14 (horiz_counter_n_66),
        .\dc_bias_reg[1]_15 (horiz_counter_n_67),
        .\dc_bias_reg[1]_16 ({horiz_counter_n_68,horiz_counter_n_69,horiz_counter_n_70,horiz_counter_n_71}),
        .\dc_bias_reg[1]_17 (horiz_counter_n_72),
        .\dc_bias_reg[1]_18 (horiz_counter_n_73),
        .\dc_bias_reg[1]_19 ({horiz_counter_n_74,horiz_counter_n_75,horiz_counter_n_76,horiz_counter_n_77}),
        .\dc_bias_reg[1]_2 ({horiz_counter_n_25,horiz_counter_n_26,horiz_counter_n_27,horiz_counter_n_28}),
        .\dc_bias_reg[1]_20 ({horiz_counter_n_78,horiz_counter_n_79,horiz_counter_n_80,horiz_counter_n_81}),
        .\dc_bias_reg[1]_21 (horiz_counter_n_82),
        .\dc_bias_reg[1]_22 (horiz_counter_n_83),
        .\dc_bias_reg[1]_23 ({horiz_counter_n_84,horiz_counter_n_85,horiz_counter_n_86,horiz_counter_n_87}),
        .\dc_bias_reg[1]_24 (horiz_counter_n_88),
        .\dc_bias_reg[1]_25 (horiz_counter_n_89),
        .\dc_bias_reg[1]_3 (horiz_counter_n_29),
        .\dc_bias_reg[1]_4 (horiz_counter_n_30),
        .\dc_bias_reg[1]_5 (horiz_counter_n_31),
        .\dc_bias_reg[1]_6 (\dc_bias_reg[1] ),
        .\dc_bias_reg[1]_7 ({horiz_counter_n_41,horiz_counter_n_42,horiz_counter_n_43,horiz_counter_n_44}),
        .\dc_bias_reg[1]_8 (horiz_counter_n_46),
        .\dc_bias_reg[1]_9 (horiz_counter_n_47),
        .\dc_bias_reg[3] ({\dc_bias_reg[3]_1 [2],\dc_bias_reg[3]_1 [0]}),
        .\encoded_reg[0] (horiz_counter_n_92),
        .\encoded_reg[1] (\encoded_reg[1] ),
        .\encoded_reg[2] (\encoded_reg[2] ),
        .\encoded_reg[2]_0 (horiz_counter_n_38),
        .\encoded_reg[3] (horiz_counter_n_33),
        .\encoded_reg[8] (horiz_counter_n_34),
        .\encoded_reg[8]_0 (horiz_counter_n_35),
        .\encoded_reg[8]_1 (horiz_counter_n_36),
        .\encoded_reg[8]_2 (horiz_counter_n_91),
        .\encoded_reg[9] (horiz_counter_n_40),
        .\encoded_reg[9]_0 (horiz_counter_n_48),
        .\encoded_reg[9]_1 (horiz_counter_n_49),
        .\encoded_reg[9]_2 (horiz_counter_n_90),
        .\encoded_reg[9]_3 (horiz_counter_n_93),
        .\processQ_reg[0]_0 (horiz_counter_n_45),
        .\processQ_reg[0]_1 (vert_counter_n_90),
        .\processQ_reg[0]_2 (sF_n_38),
        .\processQ_reg[1]_0 (\encoded_reg[9]_0 ),
        .\processQ_reg[4]_0 (vert_counter_n_89),
        .\processQ_reg[5]_0 (vert_counter_n_39),
        .\processQ_reg[5]_1 (\encoded_reg[9] ),
        .\processQ_reg[8]_0 (vert_counter_n_35),
        .\processQ_reg[9]_0 (\processQ_reg[9]_2 ),
        .\processQ_reg[9]_1 (pixel_color523_in),
        .\processQ_reg[9]_2 (pixel_color424_in),
        .\processQ_reg[9]_3 (pixel_color631_in),
        .\processQ_reg[9]_4 (pixel_color532_in),
        .\processQ_reg[9]_5 ({\processQ_reg[9] [9],\processQ_reg[9] [1:0]}),
        .\processQ_reg[9]_6 (pixel_color517_in),
        .\processQ_reg[9]_7 (pixel_color616_in),
        .reset_n(reset_n),
        .switch(switch),
        .\trigger_time_s_reg[1] (sF_n_24),
        .\trigger_time_s_reg[2] (sF_n_11),
        .\trigger_time_s_reg[2]_0 (\trigger_time_s_reg[2] ),
        .\trigger_time_s_reg[2]_1 (\trigger_time_s_reg[2]_0 ),
        .\trigger_time_s_reg[3] (\trigger_time_s_reg[3] ),
        .\trigger_time_s_reg[3]_0 (sF_n_37),
        .\trigger_time_s_reg[4] (sF_n_9),
        .\trigger_time_s_reg[4]_0 (sF_n_20),
        .\trigger_time_s_reg[4]_1 (\trigger_time_s_reg[4] ),
        .\trigger_time_s_reg[4]_2 (\trigger_time_s_reg[4]_0 ),
        .\trigger_time_s_reg[4]_3 (\trigger_time_s_reg[4]_1 ),
        .\trigger_time_s_reg[4]_4 (sF_n_21),
        .\trigger_time_s_reg[5] (sF_n_22),
        .\trigger_time_s_reg[5]_0 (sF_n_23),
        .\trigger_time_s_reg[5]_1 (sF_n_35),
        .\trigger_time_s_reg[5]_2 (\trigger_time_s_reg[5] ),
        .\trigger_time_s_reg[5]_3 (\trigger_time_s_reg[5]_0 ),
        .\trigger_time_s_reg[5]_4 (sF_n_36),
        .\trigger_time_s_reg[6] (sF_n_10),
        .\trigger_time_s_reg[6]_0 (\trigger_time_s_reg[6] ),
        .\trigger_time_s_reg[9] (Q));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_scopeFace sF
       (.CO(pixel_color627_in),
        .DI({vert_counter_n_51,vert_counter_n_52,vert_counter_n_53,vert_counter_n_54}),
        .Q(Q[7:0]),
        .S({vert_counter_n_67,vert_counter_n_68,vert_counter_n_69,vert_counter_n_70}),
        .\dc_bias_reg[1] (pixel_color616_in),
        .\dc_bias_reg[1]_0 (pixel_color517_in),
        .\dc_bias_reg[1]_1 (pixel_color528_in),
        .\dc_bias_reg[1]_10 (sF_n_12),
        .\dc_bias_reg[1]_11 (sF_n_13),
        .\dc_bias_reg[1]_12 (sF_n_14),
        .\dc_bias_reg[1]_13 (sF_n_15),
        .\dc_bias_reg[1]_14 (sF_n_16),
        .\dc_bias_reg[1]_15 (sF_n_17),
        .\dc_bias_reg[1]_16 (sF_n_18),
        .\dc_bias_reg[1]_17 (sF_n_19),
        .\dc_bias_reg[1]_18 (sF_n_20),
        .\dc_bias_reg[1]_19 (sF_n_21),
        .\dc_bias_reg[1]_2 (pixel_color523_in),
        .\dc_bias_reg[1]_20 (sF_n_22),
        .\dc_bias_reg[1]_21 (sF_n_23),
        .\dc_bias_reg[1]_22 (sF_n_24),
        .\dc_bias_reg[1]_23 (sF_n_25),
        .\dc_bias_reg[1]_24 (sF_n_26),
        .\dc_bias_reg[1]_25 (sF_n_27),
        .\dc_bias_reg[1]_26 (sF_n_28),
        .\dc_bias_reg[1]_27 (sF_n_29),
        .\dc_bias_reg[1]_28 (sF_n_30),
        .\dc_bias_reg[1]_29 (sF_n_31),
        .\dc_bias_reg[1]_3 (pixel_color424_in),
        .\dc_bias_reg[1]_30 (sF_n_32),
        .\dc_bias_reg[1]_31 (sF_n_33),
        .\dc_bias_reg[1]_32 (sF_n_34),
        .\dc_bias_reg[1]_33 (sF_n_35),
        .\dc_bias_reg[1]_34 (sF_n_36),
        .\dc_bias_reg[1]_35 (sF_n_37),
        .\dc_bias_reg[1]_36 (sF_n_38),
        .\dc_bias_reg[1]_4 (pixel_color31_out),
        .\dc_bias_reg[1]_5 (pixel_color631_in),
        .\dc_bias_reg[1]_6 (pixel_color532_in),
        .\dc_bias_reg[1]_7 (sF_n_9),
        .\dc_bias_reg[1]_8 (sF_n_10),
        .\dc_bias_reg[1]_9 (sF_n_11),
        .\processQ_reg[1] (\sdp_bl.ramb18_dp_bl.ram18_bl ),
        .\processQ_reg[7] ({vert_counter_n_59,vert_counter_n_60,vert_counter_n_61,vert_counter_n_62}),
        .\processQ_reg[7]_0 ({vert_counter_n_13,vert_counter_n_14,vert_counter_n_15,vert_counter_n_16}),
        .\processQ_reg[7]_1 ({vert_counter_n_43,vert_counter_n_44,vert_counter_n_45,vert_counter_n_46}),
        .\processQ_reg[7]_2 ({vert_counter_n_55,vert_counter_n_56,vert_counter_n_57,vert_counter_n_58}),
        .\processQ_reg[7]_3 ({vert_counter_n_25,vert_counter_n_26,vert_counter_n_27,vert_counter_n_28}),
        .\processQ_reg[7]_4 ({horiz_counter_n_50,horiz_counter_n_51,horiz_counter_n_52,horiz_counter_n_53}),
        .\processQ_reg[7]_5 ({horiz_counter_n_15,horiz_counter_n_16,horiz_counter_n_17,horiz_counter_n_18}),
        .\processQ_reg[7]_6 ({horiz_counter_n_74,horiz_counter_n_75,horiz_counter_n_76,horiz_counter_n_77}),
        .\processQ_reg[7]_7 ({horiz_counter_n_41,horiz_counter_n_42,horiz_counter_n_43,horiz_counter_n_44}),
        .\processQ_reg[7]_8 ({horiz_counter_n_54,horiz_counter_n_55,horiz_counter_n_56,horiz_counter_n_57}),
        .\processQ_reg[7]_9 ({horiz_counter_n_25,horiz_counter_n_26,horiz_counter_n_27,horiz_counter_n_28}),
        .\processQ_reg[9] (vert_counter_n_75),
        .\processQ_reg[9]_0 (vert_counter_n_83),
        .\processQ_reg[9]_1 (vert_counter_n_29),
        .\processQ_reg[9]_10 (horiz_counter_n_29),
        .\processQ_reg[9]_2 (vert_counter_n_81),
        .\processQ_reg[9]_3 (vert_counter_n_77),
        .\processQ_reg[9]_4 (vert_counter_n_79),
        .\processQ_reg[9]_5 (horiz_counter_n_66),
        .\processQ_reg[9]_6 (horiz_counter_n_19),
        .\processQ_reg[9]_7 (horiz_counter_n_82),
        .\processQ_reg[9]_8 (horiz_counter_n_88),
        .\processQ_reg[9]_9 (horiz_counter_n_72),
        .\trigger_time_s_reg[7] ({horiz_counter_n_62,horiz_counter_n_63,horiz_counter_n_64,horiz_counter_n_65}),
        .\trigger_time_s_reg[7]_0 ({horiz_counter_n_11,horiz_counter_n_12,horiz_counter_n_13,horiz_counter_n_14}),
        .\trigger_time_s_reg[7]_1 ({horiz_counter_n_78,horiz_counter_n_79,horiz_counter_n_80,horiz_counter_n_81}),
        .\trigger_time_s_reg[7]_2 ({horiz_counter_n_84,horiz_counter_n_85,horiz_counter_n_86,horiz_counter_n_87}),
        .\trigger_time_s_reg[7]_3 ({horiz_counter_n_58,horiz_counter_n_59,horiz_counter_n_60,horiz_counter_n_61}),
        .\trigger_time_s_reg[7]_4 ({horiz_counter_n_21,horiz_counter_n_22,horiz_counter_n_23,horiz_counter_n_24}),
        .\trigger_time_s_reg[8] ({horiz_counter_n_68,horiz_counter_n_69,horiz_counter_n_70,horiz_counter_n_71}),
        .\trigger_time_s_reg[9] (horiz_counter_n_67),
        .\trigger_time_s_reg[9]_0 (horiz_counter_n_20),
        .\trigger_time_s_reg[9]_1 (horiz_counter_n_83),
        .\trigger_time_s_reg[9]_2 (horiz_counter_n_89),
        .\trigger_time_s_reg[9]_3 (horiz_counter_n_73),
        .\trigger_time_s_reg[9]_4 (horiz_counter_n_30),
        .\trigger_volt_s_reg[6] (\trigger_volt_s_reg[9] [6:0]),
        .\trigger_volt_s_reg[7] ({vert_counter_n_47,vert_counter_n_48,vert_counter_n_49,vert_counter_n_50}),
        .\trigger_volt_s_reg[7]_0 ({vert_counter_n_21,vert_counter_n_22,vert_counter_n_23,vert_counter_n_24}),
        .\trigger_volt_s_reg[7]_1 ({vert_counter_n_85,vert_counter_n_86,vert_counter_n_87,vert_counter_n_88}),
        .\trigger_volt_s_reg[7]_2 ({vert_counter_n_71,vert_counter_n_72,vert_counter_n_73,vert_counter_n_74}),
        .\trigger_volt_s_reg[7]_3 ({vert_counter_n_63,vert_counter_n_64,vert_counter_n_65,vert_counter_n_66}),
        .\trigger_volt_s_reg[7]_4 ({vert_counter_n_17,vert_counter_n_18,vert_counter_n_19,vert_counter_n_20}),
        .\trigger_volt_s_reg[9] (vert_counter_n_76),
        .\trigger_volt_s_reg[9]_0 (vert_counter_n_84),
        .\trigger_volt_s_reg[9]_1 (vert_counter_n_30),
        .\trigger_volt_s_reg[9]_2 (vert_counter_n_82),
        .\trigger_volt_s_reg[9]_3 (vert_counter_n_78),
        .\trigger_volt_s_reg[9]_4 (vert_counter_n_80));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vcounter vert_counter
       (.CLK(CLK),
        .CO(CO),
        .D(D),
        .DI({vert_counter_n_51,vert_counter_n_52,vert_counter_n_53,vert_counter_n_54}),
        .Q(\processQ_reg[9] ),
        .S({vert_counter_n_67,vert_counter_n_68,vert_counter_n_69,vert_counter_n_70}),
        .SR(horiz_counter_n_2),
        .\dc_bias_reg[0] (\dc_bias_reg[0] ),
        .\dc_bias_reg[0]_0 (SR),
        .\dc_bias_reg[0]_1 (\dc_bias_reg[0]_0 ),
        .\dc_bias_reg[0]_2 (horiz_counter_n_31),
        .\dc_bias_reg[0]_3 (\dc_bias_reg[0]_1 ),
        .\dc_bias_reg[0]_4 (\dc_bias_reg[0]_2 ),
        .\dc_bias_reg[1] ({vert_counter_n_13,vert_counter_n_14,vert_counter_n_15,vert_counter_n_16}),
        .\dc_bias_reg[1]_0 ({vert_counter_n_17,vert_counter_n_18,vert_counter_n_19,vert_counter_n_20}),
        .\dc_bias_reg[1]_1 ({vert_counter_n_21,vert_counter_n_22,vert_counter_n_23,vert_counter_n_24}),
        .\dc_bias_reg[1]_10 ({vert_counter_n_63,vert_counter_n_64,vert_counter_n_65,vert_counter_n_66}),
        .\dc_bias_reg[1]_11 ({vert_counter_n_71,vert_counter_n_72,vert_counter_n_73,vert_counter_n_74}),
        .\dc_bias_reg[1]_12 (vert_counter_n_75),
        .\dc_bias_reg[1]_13 (vert_counter_n_76),
        .\dc_bias_reg[1]_14 (vert_counter_n_77),
        .\dc_bias_reg[1]_15 (vert_counter_n_78),
        .\dc_bias_reg[1]_16 (vert_counter_n_79),
        .\dc_bias_reg[1]_17 (vert_counter_n_80),
        .\dc_bias_reg[1]_18 (vert_counter_n_81),
        .\dc_bias_reg[1]_19 (vert_counter_n_82),
        .\dc_bias_reg[1]_2 ({vert_counter_n_25,vert_counter_n_26,vert_counter_n_27,vert_counter_n_28}),
        .\dc_bias_reg[1]_20 (vert_counter_n_83),
        .\dc_bias_reg[1]_21 (vert_counter_n_84),
        .\dc_bias_reg[1]_22 ({vert_counter_n_85,vert_counter_n_86,vert_counter_n_87,vert_counter_n_88}),
        .\dc_bias_reg[1]_23 (vert_counter_n_89),
        .\dc_bias_reg[1]_24 (vert_counter_n_90),
        .\dc_bias_reg[1]_3 (vert_counter_n_29),
        .\dc_bias_reg[1]_4 (vert_counter_n_30),
        .\dc_bias_reg[1]_5 (vert_counter_n_35),
        .\dc_bias_reg[1]_6 ({vert_counter_n_43,vert_counter_n_44,vert_counter_n_45,vert_counter_n_46}),
        .\dc_bias_reg[1]_7 ({vert_counter_n_47,vert_counter_n_48,vert_counter_n_49,vert_counter_n_50}),
        .\dc_bias_reg[1]_8 ({vert_counter_n_55,vert_counter_n_56,vert_counter_n_57,vert_counter_n_58}),
        .\dc_bias_reg[1]_9 ({vert_counter_n_59,vert_counter_n_60,vert_counter_n_61,vert_counter_n_62}),
        .\dc_bias_reg[3] (\dc_bias_reg[3] ),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_0 ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_1 ),
        .\dc_bias_reg[3]_2 (\dc_bias_reg[3]_2 ),
        .\dc_bias_reg[3]_3 (\dc_bias_reg[3]_3 ),
        .\dc_bias_reg[3]_4 (\dc_bias_reg[3]_4 ),
        .encoded1_in(encoded1_in),
        .\encoded_reg[0] (\encoded_reg[0] ),
        .\encoded_reg[0]_0 (\encoded_reg[0]_0 ),
        .\encoded_reg[3] (\encoded_reg[3] ),
        .\encoded_reg[4] (\encoded_reg[4] ),
        .\encoded_reg[8] (\encoded_reg[8] ),
        .\encoded_reg[8]_0 (\encoded_reg[8]_0 ),
        .\encoded_reg[8]_1 (vert_counter_n_39),
        .\encoded_reg[9] (\encoded_reg[9] ),
        .\encoded_reg[9]_0 (\encoded_reg[9]_0 ),
        .\encoded_reg[9]_1 (\encoded_reg[9]_1 ),
        .\encoded_reg[9]_2 (\encoded_reg[9]_2 ),
        .\encoded_reg[9]_3 (\encoded_reg[9]_3 ),
        .\processQ_reg[0]_0 (horiz_counter_n_92),
        .\processQ_reg[1]_0 (\dc_bias_reg[1] ),
        .\processQ_reg[1]_1 (horiz_counter_n_46),
        .\processQ_reg[1]_2 (horiz_counter_n_33),
        .\processQ_reg[1]_3 (horiz_counter_n_35),
        .\processQ_reg[1]_4 (horiz_counter_n_34),
        .\processQ_reg[1]_5 (horiz_counter_n_49),
        .\processQ_reg[1]_6 (horiz_counter_n_93),
        .\processQ_reg[2]_0 (horiz_counter_n_40),
        .\processQ_reg[5]_0 (horiz_counter_n_36),
        .\processQ_reg[5]_1 (horiz_counter_n_48),
        .\processQ_reg[6]_0 (horiz_counter_n_38),
        .\processQ_reg[6]_1 (horiz_counter_n_45),
        .\processQ_reg[7]_0 (horiz_counter_n_90),
        .\processQ_reg[7]_1 (horiz_counter_n_91),
        .\processQ_reg[8]_0 (horiz_counter_n_47),
        .\processQ_reg[9]_0 (\processQ_reg[9]_0 ),
        .\processQ_reg[9]_1 (\processQ_reg[9]_1 ),
        .\processQ_reg[9]_2 (\processQ_reg[9]_2 ),
        .\processQ_reg[9]_3 (pixel_color528_in),
        .\processQ_reg[9]_4 (pixel_color627_in),
        .reset_n(reset_n),
        .switch(switch),
        .\trigger_time_s_reg[8] (pixel_color31_out),
        .\trigger_volt_s_reg[0] (sF_n_26),
        .\trigger_volt_s_reg[1] (sF_n_17),
        .\trigger_volt_s_reg[2] (sF_n_13),
        .\trigger_volt_s_reg[3] (sF_n_29),
        .\trigger_volt_s_reg[3]_0 (sF_n_31),
        .\trigger_volt_s_reg[3]_1 (sF_n_34),
        .\trigger_volt_s_reg[4] (sF_n_12),
        .\trigger_volt_s_reg[4]_0 (\trigger_volt_s_reg[4] ),
        .\trigger_volt_s_reg[4]_1 (sF_n_14),
        .\trigger_volt_s_reg[4]_2 (sF_n_15),
        .\trigger_volt_s_reg[4]_3 (sF_n_16),
        .\trigger_volt_s_reg[4]_4 (sF_n_19),
        .\trigger_volt_s_reg[4]_5 (sF_n_18),
        .\trigger_volt_s_reg[5] (sF_n_25),
        .\trigger_volt_s_reg[5]_0 (sF_n_27),
        .\trigger_volt_s_reg[5]_1 (sF_n_28),
        .\trigger_volt_s_reg[5]_2 (sF_n_30),
        .\trigger_volt_s_reg[5]_3 (sF_n_32),
        .\trigger_volt_s_reg[5]_4 (sF_n_33),
        .\trigger_volt_s_reg[9] (\trigger_volt_s_reg[9] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video
   (tmds,
    tmdsb,
    ADDRARDADDR,
    \processQ_reg[9] ,
    reset_n,
    clk,
    Q,
    \trigger_volt_s_reg[9] ,
    \processQ_reg[9]_0 ,
    \processQ_reg[9]_1 ,
    switch,
    CO,
    \processQ_reg[9]_2 ,
    lopt);
  output [3:0]tmds;
  output [3:0]tmdsb;
  output [9:0]ADDRARDADDR;
  output [9:0]\processQ_reg[9] ;
  input reset_n;
  input clk;
  input [9:0]Q;
  input [9:0]\trigger_volt_s_reg[9] ;
  input \processQ_reg[9]_0 ;
  input \processQ_reg[9]_1 ;
  input [1:0]switch;
  input [0:0]CO;
  input [0:0]\processQ_reg[9]_2 ;
  input lopt;

  wire [9:0]ADDRARDADDR;
  wire [0:0]CO;
  wire Inst_vga_n_0;
  wire Inst_vga_n_1;
  wire Inst_vga_n_2;
  wire Inst_vga_n_23;
  wire Inst_vga_n_24;
  wire Inst_vga_n_25;
  wire Inst_vga_n_26;
  wire Inst_vga_n_27;
  wire Inst_vga_n_29;
  wire Inst_vga_n_30;
  wire Inst_vga_n_31;
  wire Inst_vga_n_32;
  wire Inst_vga_n_33;
  wire Inst_vga_n_34;
  wire Inst_vga_n_36;
  wire Inst_vga_n_37;
  wire Inst_vga_n_38;
  wire [9:0]Q;
  wire \TDMS_encoder_blue/p_1_in ;
  wire \TDMS_encoder_green/p_1_in ;
  wire \TDMS_encoder_red/p_1_in ;
  wire blank;
  wire blue_s;
  wire clk;
  wire clock_s;
  wire [3:3]encoded1_in;
  wire green_s;
  wire inst_dvid_n_10;
  wire inst_dvid_n_11;
  wire inst_dvid_n_12;
  wire inst_dvid_n_13;
  wire inst_dvid_n_14;
  wire inst_dvid_n_15;
  wire inst_dvid_n_16;
  wire inst_dvid_n_17;
  wire inst_dvid_n_18;
  wire inst_dvid_n_19;
  wire inst_dvid_n_20;
  wire inst_dvid_n_21;
  wire inst_dvid_n_22;
  wire inst_dvid_n_23;
  wire inst_dvid_n_4;
  wire inst_dvid_n_7;
  wire inst_dvid_n_8;
  wire lopt;
  wire pixel_clk;
  wire [9:0]\processQ_reg[9] ;
  wire \processQ_reg[9]_0 ;
  wire \processQ_reg[9]_1 ;
  wire [0:0]\processQ_reg[9]_2 ;
  wire red_s;
  wire reset_n;
  wire serialize_clk;
  wire serialize_clk_n;
  wire [1:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;
  wire [9:0]\trigger_volt_s_reg[9] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga Inst_vga
       (.ADDRARDADDR(ADDRARDADDR[9:2]),
        .CLK(pixel_clk),
        .CO(CO),
        .D(Inst_vga_n_23),
        .Q(Q),
        .SR(blank),
        .\dc_bias_reg[0] (Inst_vga_n_30),
        .\dc_bias_reg[0]_0 (inst_dvid_n_23),
        .\dc_bias_reg[0]_1 (inst_dvid_n_17),
        .\dc_bias_reg[0]_2 (inst_dvid_n_4),
        .\dc_bias_reg[1] (Inst_vga_n_27),
        .\dc_bias_reg[3] (Inst_vga_n_25),
        .\dc_bias_reg[3]_0 (inst_dvid_n_10),
        .\dc_bias_reg[3]_1 ({\TDMS_encoder_blue/p_1_in ,inst_dvid_n_7,inst_dvid_n_8}),
        .\dc_bias_reg[3]_2 (inst_dvid_n_22),
        .\dc_bias_reg[3]_3 (\TDMS_encoder_red/p_1_in ),
        .\dc_bias_reg[3]_4 (\TDMS_encoder_green/p_1_in ),
        .encoded1_in(encoded1_in),
        .\encoded_reg[0] (Inst_vga_n_24),
        .\encoded_reg[0]_0 (Inst_vga_n_33),
        .\encoded_reg[1] (Inst_vga_n_34),
        .\encoded_reg[2] (Inst_vga_n_31),
        .\encoded_reg[3] (Inst_vga_n_29),
        .\encoded_reg[4] (Inst_vga_n_32),
        .\encoded_reg[8] (Inst_vga_n_0),
        .\encoded_reg[8]_0 (Inst_vga_n_2),
        .\encoded_reg[9] (Inst_vga_n_1),
        .\encoded_reg[9]_0 (Inst_vga_n_26),
        .\encoded_reg[9]_1 (Inst_vga_n_36),
        .\encoded_reg[9]_2 (Inst_vga_n_37),
        .\encoded_reg[9]_3 (Inst_vga_n_38),
        .\processQ_reg[9] (\processQ_reg[9] ),
        .\processQ_reg[9]_0 (\processQ_reg[9]_0 ),
        .\processQ_reg[9]_1 (\processQ_reg[9]_1 ),
        .\processQ_reg[9]_2 (\processQ_reg[9]_2 ),
        .reset_n(reset_n),
        .\sdp_bl.ramb18_dp_bl.ram18_bl (ADDRARDADDR[1:0]),
        .switch(switch),
        .\trigger_time_s_reg[2] (inst_dvid_n_19),
        .\trigger_time_s_reg[2]_0 (inst_dvid_n_21),
        .\trigger_time_s_reg[3] (inst_dvid_n_15),
        .\trigger_time_s_reg[4] (inst_dvid_n_13),
        .\trigger_time_s_reg[4]_0 (inst_dvid_n_14),
        .\trigger_time_s_reg[4]_1 (inst_dvid_n_16),
        .\trigger_time_s_reg[5] (inst_dvid_n_18),
        .\trigger_time_s_reg[5]_0 (inst_dvid_n_20),
        .\trigger_time_s_reg[6] (inst_dvid_n_12),
        .\trigger_volt_s_reg[4] (inst_dvid_n_11),
        .\trigger_volt_s_reg[9] (\trigger_volt_s_reg[9] ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_blue
       (.I(blue_s),
        .O(tmds[0]),
        .OB(tmdsb[0]));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_clock
       (.I(clock_s),
        .O(tmds[3]),
        .OB(tmdsb[3]));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_green
       (.I(red_s),
        .O(tmds[2]),
        .OB(tmdsb[2]));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_red
       (.I(green_s),
        .O(tmds[1]),
        .OB(tmdsb[1]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid inst_dvid
       (.CLK(pixel_clk),
        .D(Inst_vga_n_23),
        .Q(\TDMS_encoder_green/p_1_in ),
        .SR(blank),
        .blue_s(blue_s),
        .clk_out2(serialize_clk),
        .clk_out3(serialize_clk_n),
        .clock_s(clock_s),
        .\dc_bias_reg[0] ({\TDMS_encoder_blue/p_1_in ,inst_dvid_n_7,inst_dvid_n_8}),
        .\dc_bias_reg[0]_0 (\TDMS_encoder_red/p_1_in ),
        .\dc_bias_reg[0]_1 (Inst_vga_n_25),
        .\dc_bias_reg[1] (inst_dvid_n_11),
        .\dc_bias_reg[1]_0 (inst_dvid_n_12),
        .\dc_bias_reg[1]_1 (inst_dvid_n_13),
        .\dc_bias_reg[1]_2 (inst_dvid_n_14),
        .\dc_bias_reg[1]_3 (inst_dvid_n_15),
        .\dc_bias_reg[1]_4 (inst_dvid_n_16),
        .\dc_bias_reg[1]_5 (inst_dvid_n_18),
        .\dc_bias_reg[1]_6 (inst_dvid_n_19),
        .\dc_bias_reg[1]_7 (inst_dvid_n_20),
        .\dc_bias_reg[1]_8 (inst_dvid_n_21),
        .\dc_bias_reg[2] (inst_dvid_n_10),
        .\dc_bias_reg[2]_0 (inst_dvid_n_17),
        .\dc_bias_reg[3] (encoded1_in),
        .\dc_bias_reg[3]_0 (Inst_vga_n_38),
        .\dc_bias_reg[3]_1 (Inst_vga_n_37),
        .\dc_bias_reg[3]_2 (Inst_vga_n_32),
        .\dc_bias_reg[3]_3 (Inst_vga_n_31),
        .\dc_bias_reg[3]_4 (Inst_vga_n_34),
        .\dc_bias_reg[3]_5 (Inst_vga_n_33),
        .\encoded_reg[3] (inst_dvid_n_22),
        .\encoded_reg[8] (inst_dvid_n_4),
        .\encoded_reg[8]_0 (inst_dvid_n_23),
        .green_s(green_s),
        .\processQ_reg[1] (Inst_vga_n_26),
        .\processQ_reg[1]_0 (Inst_vga_n_27),
        .\processQ_reg[1]_1 (Inst_vga_n_29),
        .\processQ_reg[1]_2 (Inst_vga_n_30),
        .\processQ_reg[5] (Inst_vga_n_36),
        .\processQ_reg[5]_0 (Inst_vga_n_1),
        .\processQ_reg[5]_1 (Inst_vga_n_2),
        .\processQ_reg[6] (Inst_vga_n_0),
        .\processQ_reg[9] (Inst_vga_n_24),
        .red_s(red_s),
        .\trigger_time_s_reg[7] (Q[7:0]),
        .\trigger_volt_s_reg[6] (\trigger_volt_s_reg[9] [6:2]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 mmcm_adv_inst_display_clocks
       (.clk_in1(clk),
        .clk_out1(pixel_clk),
        .clk_out2(serialize_clk),
        .clk_out3(serialize_clk_n),
        .lopt(lopt),
        .resetn(reset_n));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
