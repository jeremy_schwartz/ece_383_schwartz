-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.3 (win64) Build 2018833 Wed Oct  4 19:58:22 MDT 2017
-- Date        : Mon Mar  5 22:04:08 2018
-- Host        : C19JMSCHWARTZ running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_my_oscope_0_0_sim_netlist.vhdl
-- Design      : design_1_my_oscope_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a200tsbg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder is
  port (
    D : out STD_LOGIC_VECTOR ( 5 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \encoded_reg[8]_1\ : out STD_LOGIC;
    \encoded_reg[8]_2\ : out STD_LOGIC;
    \encoded_reg[8]_3\ : out STD_LOGIC;
    \encoded_reg[8]_4\ : out STD_LOGIC;
    \encoded_reg[8]_5\ : out STD_LOGIC;
    \encoded_reg[8]_6\ : out STD_LOGIC;
    \encoded_reg[9]_0\ : out STD_LOGIC;
    \processQ_reg[6]\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC;
    \dc_bias_reg[3]_1\ : in STD_LOGIC;
    \dc_bias_reg[3]_2\ : in STD_LOGIC;
    \dc_bias_reg[3]_3\ : in STD_LOGIC;
    \dc_bias_reg[3]_4\ : in STD_LOGIC;
    \processQ_reg[8]\ : in STD_LOGIC;
    \processQ_reg[5]\ : in STD_LOGIC;
    \processQ_reg[8]_0\ : in STD_LOGIC;
    \processQ_reg[5]_0\ : in STD_LOGIC;
    \slv_reg11_reg[7]\ : in STD_LOGIC;
    \slv_reg11_reg[5]\ : in STD_LOGIC;
    \slv_reg11_reg[3]\ : in STD_LOGIC;
    \slv_reg11_reg[4]\ : in STD_LOGIC;
    \slv_reg11_reg[6]\ : in STD_LOGIC;
    \slv_reg11_reg[1]\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[3]\ : in STD_LOGIC;
    \slv_reg11_reg[2]\ : in STD_LOGIC;
    \slv_reg11_reg[1]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \slv_reg5_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \int_trigger_volt_s_reg[1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder is
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dc_bias[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \dc_bias[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \dc_bias[2]_i_1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[0]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[1]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[2]\ : STD_LOGIC;
  signal \encoded[8]_i_1_n_0\ : STD_LOGIC;
  signal \^encoded_reg[8]_1\ : STD_LOGIC;
  signal \^encoded_reg[8]_3\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \dc_bias[0]_i_1__0\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \dc_bias[1]_i_1__0\ : label is "soft_lutpair74";
begin
  Q(0) <= \^q\(0);
  \encoded_reg[8]_1\ <= \^encoded_reg[8]_1\;
  \encoded_reg[8]_3\ <= \^encoded_reg[8]_3\;
\dc_bias[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \dc_bias_reg_n_0_[0]\,
      O => \dc_bias[0]_i_1__0_n_0\
    );
\dc_bias[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966666"
    )
        port map (
      I0 => \processQ_reg[5]\,
      I1 => \dc_bias_reg_n_0_[1]\,
      I2 => \dc_bias_reg_n_0_[0]\,
      I3 => \processQ_reg[8]_0\,
      I4 => \^q\(0),
      O => \dc_bias[1]_i_1__0_n_0\
    );
\dc_bias[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"18E710EF00FFA25D"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processQ_reg[8]\,
      I2 => \processQ_reg[5]\,
      I3 => \dc_bias_reg_n_0_[2]\,
      I4 => \dc_bias_reg_n_0_[0]\,
      I5 => \dc_bias_reg_n_0_[1]\,
      O => \dc_bias[2]_i_1_n_0\
    );
\dc_bias[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000A2E7EFFFFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processQ_reg[8]\,
      I2 => \processQ_reg[5]\,
      I3 => \dc_bias_reg_n_0_[0]\,
      I4 => \dc_bias_reg_n_0_[1]\,
      I5 => \dc_bias_reg_n_0_[2]\,
      O => \dc_bias[3]_i_1__0_n_0\
    );
\dc_bias_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[0]_i_1__0_n_0\,
      Q => \dc_bias_reg_n_0_[0]\,
      R => SR(0)
    );
\dc_bias_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[1]_i_1__0_n_0\,
      Q => \dc_bias_reg_n_0_[1]\,
      R => SR(0)
    );
\dc_bias_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[2]_i_1_n_0\,
      Q => \dc_bias_reg_n_0_[2]\,
      R => SR(0)
    );
\dc_bias_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[3]_i_1__0_n_0\,
      Q => \^q\(0),
      R => SR(0)
    );
\encoded[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100000000"
    )
        port map (
      I0 => \processQ_reg[8]_0\,
      I1 => \dc_bias_reg_n_0_[1]\,
      I2 => \dc_bias_reg_n_0_[0]\,
      I3 => \dc_bias_reg_n_0_[2]\,
      I4 => \^q\(0),
      I5 => \processQ_reg[5]_0\,
      O => \encoded[8]_i_1_n_0\
    );
\encoded[8]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[1]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \dc_bias_reg_n_0_[2]\,
      I3 => \^q\(0),
      O => \encoded_reg[9]_0\
    );
\encoded_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_3\,
      Q => D(0),
      R => '0'
    );
\encoded_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_2\,
      Q => D(1),
      R => '0'
    );
\encoded_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_1\,
      Q => D(2),
      R => '0'
    );
\encoded_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_0\,
      Q => D(3),
      R => '0'
    );
\encoded_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => CLK,
      CE => '1',
      D => \processQ_reg[6]\,
      Q => D(4),
      S => \encoded[8]_i_1_n_0\
    );
\encoded_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_4\,
      Q => D(5),
      R => '0'
    );
\i__carry__0_i_3__5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888008000000000"
    )
        port map (
      I0 => \slv_reg11_reg[7]\,
      I1 => \slv_reg11_reg[5]\,
      I2 => \slv_reg11_reg[3]\,
      I3 => \^encoded_reg[8]_1\,
      I4 => \slv_reg11_reg[4]\,
      I5 => \slv_reg11_reg[6]\,
      O => \encoded_reg[8]_0\
    );
\i__carry__0_i_3__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A888888800000000"
    )
        port map (
      I0 => \slv_reg11_reg[6]\,
      I1 => \slv_reg11_reg[4]\,
      I2 => \slv_reg11_reg[1]\,
      I3 => \^encoded_reg[8]_3\,
      I4 => \int_trigger_volt_s_reg[3]\,
      I5 => \slv_reg11_reg[5]\,
      O => \encoded_reg[8]_2\
    );
\i__carry_i_10__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8088800000000000"
    )
        port map (
      I0 => \slv_reg11_reg[2]\,
      I1 => \slv_reg11_reg[3]\,
      I2 => \slv_reg11_reg[1]_0\(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \int_trigger_volt_s_reg[1]\(0),
      I5 => \slv_reg11_reg[1]\,
      O => \encoded_reg[8]_6\
    );
\i__carry_i_10__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000440347"
    )
        port map (
      I0 => \slv_reg11_reg[1]_0\(0),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[1]\(0),
      I3 => \slv_reg11_reg[1]_0\(1),
      I4 => \int_trigger_volt_s_reg[1]\(1),
      I5 => \slv_reg11_reg[2]\,
      O => \^encoded_reg[8]_1\
    );
\i__carry_i_11__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg11_reg[1]_0\(0),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[1]\(0),
      O => \^encoded_reg[8]_3\
    );
\i__carry_i_9__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA88888880"
    )
        port map (
      I0 => \slv_reg11_reg[5]\,
      I1 => \slv_reg11_reg[3]\,
      I2 => \slv_reg11_reg[2]\,
      I3 => \slv_reg11_reg[1]\,
      I4 => \^encoded_reg[8]_3\,
      I5 => \slv_reg11_reg[4]\,
      O => \encoded_reg[8]_4\
    );
\i__carry_i_9__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA80000000"
    )
        port map (
      I0 => \slv_reg11_reg[5]\,
      I1 => \slv_reg11_reg[2]\,
      I2 => \slv_reg11_reg[3]\,
      I3 => \^encoded_reg[8]_3\,
      I4 => \slv_reg11_reg[1]\,
      I5 => \slv_reg11_reg[4]\,
      O => \encoded_reg[8]_5\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1 is
  port (
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC;
    \dc_bias_reg[3]_1\ : in STD_LOGIC;
    \processQ_reg[9]\ : in STD_LOGIC;
    \processQ_reg[5]\ : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1 : entity is "TDMS_encoder";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1 is
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dc_bias[0]_i_1_n_0\ : STD_LOGIC;
  signal \dc_bias[1]_i_1_n_0\ : STD_LOGIC;
  signal \dc_bias[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_1_n_0\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[0]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[1]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[2]\ : STD_LOGIC;
  signal \encoded[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_1__1_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_2_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \dc_bias[0]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \dc_bias[1]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \dc_bias[2]_i_1__1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_1\ : label is "soft_lutpair75";
begin
  Q(0) <= \^q\(0);
\dc_bias[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \dc_bias_reg_n_0_[0]\,
      O => \dc_bias[0]_i_1_n_0\
    );
\dc_bias[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"69AA"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[1]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \processQ_reg[9]\,
      I3 => \^q\(0),
      O => \dc_bias[1]_i_1_n_0\
    );
\dc_bias[2]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"95565555"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[2]\,
      I1 => \dc_bias_reg_n_0_[1]\,
      I2 => \dc_bias_reg_n_0_[0]\,
      I3 => \processQ_reg[9]\,
      I4 => \^q\(0),
      O => \dc_bias[2]_i_1__1_n_0\
    );
\dc_bias[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00027FFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processQ_reg[9]\,
      I2 => \dc_bias_reg_n_0_[0]\,
      I3 => \dc_bias_reg_n_0_[1]\,
      I4 => \dc_bias_reg_n_0_[2]\,
      O => \dc_bias[3]_i_1_n_0\
    );
\dc_bias_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[0]_i_1_n_0\,
      Q => \dc_bias_reg_n_0_[0]\,
      R => SR(0)
    );
\dc_bias_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[1]_i_1_n_0\,
      Q => \dc_bias_reg_n_0_[1]\,
      R => SR(0)
    );
\dc_bias_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[2]_i_1__1_n_0\,
      Q => \dc_bias_reg_n_0_[2]\,
      R => SR(0)
    );
\dc_bias_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[3]_i_1_n_0\,
      Q => \^q\(0),
      R => SR(0)
    );
\encoded[2]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processQ_reg[5]\,
      O => \encoded[2]_i_1__1_n_0\
    );
\encoded[8]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \processQ_reg[9]\,
      I1 => \dc_bias_reg_n_0_[2]\,
      I2 => \dc_bias_reg_n_0_[0]\,
      I3 => \dc_bias_reg_n_0_[1]\,
      I4 => \^q\(0),
      I5 => \processQ_reg[5]\,
      O => \encoded[8]_i_1__1_n_0\
    );
\encoded[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBBBBBBBBBB3"
    )
        port map (
      I0 => \processQ_reg[9]\,
      I1 => \processQ_reg[5]\,
      I2 => \^q\(0),
      I3 => \dc_bias_reg_n_0_[1]\,
      I4 => \dc_bias_reg_n_0_[0]\,
      I5 => \dc_bias_reg_n_0_[2]\,
      O => \encoded[8]_i_2_n_0\
    );
\encoded_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_0\,
      Q => D(0),
      R => '0'
    );
\encoded_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \encoded[2]_i_1__1_n_0\,
      Q => D(1),
      R => '0'
    );
\encoded_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => CLK,
      CE => '1',
      D => \encoded[8]_i_2_n_0\,
      Q => D(2),
      S => \encoded[8]_i_1__1_n_0\
    );
\encoded_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_1\,
      Q => D(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2 is
  port (
    D : out STD_LOGIC_VECTOR ( 4 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[0]_0\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_0\ : out STD_LOGIC;
    \encoded_reg[0]_1\ : out STD_LOGIC;
    \encoded_reg[0]_2\ : out STD_LOGIC;
    \encoded_reg[0]_3\ : out STD_LOGIC;
    \encoded_reg[0]_4\ : out STD_LOGIC;
    \encoded_reg[0]_5\ : out STD_LOGIC;
    \encoded_reg[0]_6\ : out STD_LOGIC;
    \encoded_reg[0]_7\ : out STD_LOGIC;
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \encoded_reg[8]_1\ : out STD_LOGIC;
    \state_reg[0]\ : out STD_LOGIC;
    \state_reg[0]_0\ : out STD_LOGIC;
    \encoded_reg[8]_2\ : out STD_LOGIC;
    \state_reg[0]_1\ : out STD_LOGIC;
    \state_reg[0]_2\ : out STD_LOGIC;
    \encoded_reg[8]_3\ : out STD_LOGIC;
    \encoded_reg[8]_4\ : out STD_LOGIC;
    \state_reg[0]_3\ : out STD_LOGIC;
    \encoded_reg[8]_5\ : out STD_LOGIC;
    \encoded_reg[8]_6\ : out STD_LOGIC;
    \encoded_reg[8]_7\ : out STD_LOGIC;
    \encoded_reg[8]_8\ : out STD_LOGIC;
    \encoded_reg[8]_9\ : out STD_LOGIC;
    \encoded_reg[8]_10\ : out STD_LOGIC;
    \encoded_reg[8]_11\ : out STD_LOGIC;
    \encoded_reg[8]_12\ : out STD_LOGIC;
    \encoded_reg[8]_13\ : out STD_LOGIC;
    \encoded_reg[8]_14\ : out STD_LOGIC;
    \state_reg[0]_4\ : out STD_LOGIC;
    \encoded_reg[8]_15\ : out STD_LOGIC;
    \processQ_reg[5]\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC;
    \processQ_reg[5]_0\ : in STD_LOGIC;
    \processQ_reg[5]_1\ : in STD_LOGIC;
    \slv_reg10_reg[7]\ : in STD_LOGIC;
    \slv_reg10_reg[2]\ : in STD_LOGIC;
    \slv_reg10_reg[4]\ : in STD_LOGIC;
    \slv_reg10_reg[6]\ : in STD_LOGIC;
    \slv_reg10_reg[1]\ : in STD_LOGIC;
    \slv_reg10_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg5_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \int_trigger_time_s_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]\ : in STD_LOGIC;
    \slv_reg11_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \int_trigger_volt_s_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg11_reg[0]\ : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2 : entity is "TDMS_encoder";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2 is
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dc_bias[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \dc_bias[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \dc_bias[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_2__0_n_0\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[0]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[1]\ : STD_LOGIC;
  signal \dc_bias_reg_n_0_[2]\ : STD_LOGIC;
  signal encoded0_in : STD_LOGIC_VECTOR ( 8 to 8 );
  signal encoded1_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^encoded_reg[8]_1\ : STD_LOGIC;
  signal \^encoded_reg[8]_2\ : STD_LOGIC;
  signal \^encoded_reg[8]_5\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_0\ : STD_LOGIC;
  signal \^state_reg[0]\ : STD_LOGIC;
  signal \^state_reg[0]_0\ : STD_LOGIC;
  signal \^state_reg[0]_1\ : STD_LOGIC;
  signal \^state_reg[0]_2\ : STD_LOGIC;
  signal \^state_reg[0]_3\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \dc_bias[0]_i_1__1\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \encoded[8]_i_3__0\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \i__carry__0_i_5__0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \i__carry_i_11\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \i__carry_i_11__1\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \i__carry_i_12__1\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \i__carry_i_13__0\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \i__carry_i_14__0\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \i__carry_i_15__0\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \i__carry_i_16__0\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \i__carry_i_17\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \i__carry_i_17__0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \i__carry_i_8__3\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \i__carry_i_9__5\ : label is "soft_lutpair80";
begin
  Q(0) <= \^q\(0);
  \encoded_reg[8]_1\ <= \^encoded_reg[8]_1\;
  \encoded_reg[8]_2\ <= \^encoded_reg[8]_2\;
  \encoded_reg[8]_5\ <= \^encoded_reg[8]_5\;
  \int_trigger_time_s_reg[0]\ <= \^int_trigger_time_s_reg[0]\;
  \int_trigger_time_s_reg[0]_0\ <= \^int_trigger_time_s_reg[0]_0\;
  \state_reg[0]\ <= \^state_reg[0]\;
  \state_reg[0]_0\ <= \^state_reg[0]_0\;
  \state_reg[0]_1\ <= \^state_reg[0]_1\;
  \state_reg[0]_2\ <= \^state_reg[0]_2\;
  \state_reg[0]_3\ <= \^state_reg[0]_3\;
\dc_bias[0]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99996662"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processQ_reg[5]_0\,
      I2 => \dc_bias_reg_n_0_[1]\,
      I3 => \dc_bias_reg_n_0_[2]\,
      I4 => \dc_bias_reg_n_0_[0]\,
      O => \dc_bias[0]_i_1__1_n_0\
    );
\dc_bias[1]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"659A596A"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[1]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \^q\(0),
      I3 => \processQ_reg[5]_0\,
      I4 => \processQ_reg[9]\,
      O => \dc_bias[1]_i_1__1_n_0\
    );
\dc_bias[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"63696363C3C39983"
    )
        port map (
      I0 => \^q\(0),
      I1 => \dc_bias_reg_n_0_[2]\,
      I2 => \processQ_reg[5]_0\,
      I3 => \processQ_reg[9]\,
      I4 => \dc_bias_reg_n_0_[0]\,
      I5 => \dc_bias_reg_n_0_[1]\,
      O => \dc_bias[2]_i_1__0_n_0\
    );
\dc_bias[3]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3F1F3F07000F000F"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[0]\,
      I1 => \dc_bias_reg_n_0_[1]\,
      I2 => \dc_bias_reg_n_0_[2]\,
      I3 => \processQ_reg[5]_0\,
      I4 => \processQ_reg[9]\,
      I5 => \^q\(0),
      O => \dc_bias[3]_i_2__0_n_0\
    );
\dc_bias_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[0]_i_1__1_n_0\,
      Q => \dc_bias_reg_n_0_[0]\,
      R => SR(0)
    );
\dc_bias_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[1]_i_1__1_n_0\,
      Q => \dc_bias_reg_n_0_[1]\,
      R => SR(0)
    );
\dc_bias_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[2]_i_1__0_n_0\,
      Q => \dc_bias_reg_n_0_[2]\,
      R => SR(0)
    );
\dc_bias_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \dc_bias[3]_i_2__0_n_0\,
      Q => \^q\(0),
      R => SR(0)
    );
\encoded[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88888888888888A8"
    )
        port map (
      I0 => \processQ_reg[5]_1\,
      I1 => \^q\(0),
      I2 => \processQ_reg[5]_0\,
      I3 => \dc_bias_reg_n_0_[2]\,
      I4 => \dc_bias_reg_n_0_[0]\,
      I5 => \dc_bias_reg_n_0_[1]\,
      O => encoded1_in(0)
    );
\encoded[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDDDDDDDDDDDFD"
    )
        port map (
      I0 => \processQ_reg[5]_1\,
      I1 => \^q\(0),
      I2 => \processQ_reg[5]_0\,
      I3 => \dc_bias_reg_n_0_[2]\,
      I4 => \dc_bias_reg_n_0_[0]\,
      I5 => \dc_bias_reg_n_0_[1]\,
      O => encoded1_in(2)
    );
\encoded[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6666666200000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \processQ_reg[5]_0\,
      I2 => \dc_bias_reg_n_0_[2]\,
      I3 => \dc_bias_reg_n_0_[0]\,
      I4 => \dc_bias_reg_n_0_[1]\,
      I5 => \processQ_reg[5]_1\,
      O => encoded1_in(3)
    );
\encoded[8]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000010000"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[2]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \dc_bias_reg_n_0_[1]\,
      I3 => \^q\(0),
      I4 => \processQ_reg[5]_1\,
      I5 => \processQ_reg[9]\,
      O => encoded0_in(8)
    );
\encoded[8]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \dc_bias_reg_n_0_[2]\,
      I1 => \dc_bias_reg_n_0_[0]\,
      I2 => \dc_bias_reg_n_0_[1]\,
      I3 => \^q\(0),
      O => \encoded_reg[8]_15\
    );
\encoded_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => encoded1_in(0),
      Q => D(0),
      R => '0'
    );
\encoded_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => encoded1_in(2),
      Q => D(1),
      R => '0'
    );
\encoded_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => encoded1_in(3),
      Q => D(2),
      R => '0'
    );
\encoded_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => CLK,
      CE => '1',
      D => \processQ_reg[5]\,
      Q => D(3),
      S => encoded0_in(8)
    );
\encoded_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \dc_bias_reg[3]_0\,
      Q => D(4),
      R => '0'
    );
\i__carry__0_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8888800000000"
    )
        port map (
      I0 => \slv_reg10_reg[6]\,
      I1 => \slv_reg10_reg[4]\,
      I2 => \slv_reg10_reg[1]\,
      I3 => \slv_reg10_reg[2]\,
      I4 => \^int_trigger_time_s_reg[0]_0\,
      I5 => \^int_trigger_time_s_reg[0]\,
      O => \encoded_reg[0]_2\
    );
\i__carry__0_i_3__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8888800000000"
    )
        port map (
      I0 => \^state_reg[0]_2\,
      I1 => \^state_reg[0]_1\,
      I2 => \^state_reg[0]_3\,
      I3 => \slv_reg11_reg[0]\,
      I4 => \^encoded_reg[8]_5\,
      I5 => \^state_reg[0]\,
      O => \encoded_reg[8]_4\
    );
\i__carry__0_i_3__8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(8),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[9]\(8),
      O => \state_reg[0]_4\
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A888888800000000"
    )
        port map (
      I0 => \slv_reg10_reg[6]\,
      I1 => \slv_reg10_reg[4]\,
      I2 => \slv_reg10_reg[1]\,
      I3 => \slv_reg10_reg[2]\,
      I4 => \^int_trigger_time_s_reg[0]_0\,
      I5 => \^int_trigger_time_s_reg[0]\,
      O => \encoded_reg[0]_3\
    );
\i__carry__0_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A888888800000000"
    )
        port map (
      I0 => \^state_reg[0]_2\,
      I1 => \^state_reg[0]_1\,
      I2 => \^encoded_reg[8]_2\,
      I3 => \^state_reg[0]_3\,
      I4 => \^state_reg[0]_0\,
      I5 => \^state_reg[0]\,
      O => \encoded_reg[8]_7\
    );
\i__carry__0_i_5__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(9),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[9]\(9),
      O => \encoded_reg[8]_14\
    );
\i__carry_i_10__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0151555555555555"
    )
        port map (
      I0 => \slv_reg10_reg[4]\,
      I1 => \int_trigger_time_s_reg[5]\(0),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => \slv_reg10_reg[5]\(0),
      I4 => \slv_reg10_reg[2]\,
      I5 => \^int_trigger_time_s_reg[0]_0\,
      O => \encoded_reg[0]_5\
    );
\i__carry_i_10__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00053305"
    )
        port map (
      I0 => \int_trigger_time_s_reg[5]\(0),
      I1 => \slv_reg10_reg[5]\(0),
      I2 => \int_trigger_time_s_reg[5]\(1),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \slv_reg10_reg[5]\(1),
      O => \encoded_reg[0]_7\
    );
\i__carry_i_10__5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0151555555555555"
    )
        port map (
      I0 => \^state_reg[0]_1\,
      I1 => \int_trigger_volt_s_reg[9]\(2),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => \slv_reg11_reg[9]\(2),
      I4 => \^state_reg[0]_3\,
      I5 => \^state_reg[0]_0\,
      O => \encoded_reg[8]_10\
    );
\i__carry_i_10__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888880888000"
    )
        port map (
      I0 => \^encoded_reg[8]_2\,
      I1 => \^state_reg[0]_0\,
      I2 => \slv_reg11_reg[9]\(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \int_trigger_volt_s_reg[9]\(0),
      I5 => \^state_reg[0]_3\,
      O => \encoded_reg[8]_11\
    );
\i__carry_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[5]\(3),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_time_s_reg[5]\(3),
      O => \^int_trigger_time_s_reg[0]\
    );
\i__carry_i_11__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(5),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[9]\(5),
      O => \^state_reg[0]\
    );
\i__carry_i_12__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(7),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[9]\(7),
      O => \^encoded_reg[8]_1\
    );
\i__carry_i_13__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(4),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[9]\(4),
      O => \^state_reg[0]_1\
    );
\i__carry_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A800A0080800000"
    )
        port map (
      I0 => \^int_trigger_time_s_reg[0]_0\,
      I1 => \slv_reg10_reg[5]\(1),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => \int_trigger_time_s_reg[5]\(1),
      I4 => \slv_reg10_reg[5]\(0),
      I5 => \int_trigger_time_s_reg[5]\(0),
      O => \encoded_reg[0]_6\
    );
\i__carry_i_14__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA000A0"
    )
        port map (
      I0 => \int_trigger_volt_s_reg[9]\(2),
      I1 => \slv_reg11_reg[9]\(2),
      I2 => \int_trigger_volt_s_reg[9]\(1),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \slv_reg11_reg[9]\(1),
      O => \encoded_reg[8]_13\
    );
\i__carry_i_15__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(3),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[9]\(3),
      O => \^state_reg[0]_0\
    );
\i__carry_i_16__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(2),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[9]\(2),
      O => \^encoded_reg[8]_2\
    );
\i__carry_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[5]\(2),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_time_s_reg[5]\(2),
      O => \^int_trigger_time_s_reg[0]_0\
    );
\i__carry_i_17__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(1),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[9]\(1),
      O => \^state_reg[0]_3\
    );
\i__carry_i_5__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888800000000000"
    )
        port map (
      I0 => \slv_reg10_reg[7]\,
      I1 => \^int_trigger_time_s_reg[0]\,
      I2 => \slv_reg10_reg[2]\,
      I3 => \^int_trigger_time_s_reg[0]_0\,
      I4 => \slv_reg10_reg[4]\,
      I5 => \slv_reg10_reg[6]\,
      O => \encoded_reg[0]_0\
    );
\i__carry_i_5__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888800000000000"
    )
        port map (
      I0 => \^encoded_reg[8]_1\,
      I1 => \^state_reg[0]\,
      I2 => \^state_reg[0]_0\,
      I3 => \^encoded_reg[8]_2\,
      I4 => \^state_reg[0]_1\,
      I5 => \^state_reg[0]_2\,
      O => \encoded_reg[8]_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8880000"
    )
        port map (
      I0 => \slv_reg10_reg[6]\,
      I1 => \slv_reg10_reg[4]\,
      I2 => \^int_trigger_time_s_reg[0]_0\,
      I3 => \slv_reg10_reg[2]\,
      I4 => \^int_trigger_time_s_reg[0]\,
      O => \encoded_reg[0]_1\
    );
\i__carry_i_6__6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8880000"
    )
        port map (
      I0 => \^state_reg[0]_2\,
      I1 => \^state_reg[0]_1\,
      I2 => \^encoded_reg[8]_2\,
      I3 => \^state_reg[0]_0\,
      I4 => \^state_reg[0]\,
      O => \encoded_reg[8]_3\
    );
\i__carry_i_8__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA000A0"
    )
        port map (
      I0 => \int_trigger_volt_s_reg[9]\(3),
      I1 => \slv_reg11_reg[9]\(3),
      I2 => \int_trigger_volt_s_reg[9]\(2),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \slv_reg11_reg[9]\(2),
      O => \^encoded_reg[8]_5\
    );
\i__carry_i_9__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000015155555555"
    )
        port map (
      I0 => \slv_reg10_reg[4]\,
      I1 => \int_trigger_time_s_reg[5]\(0),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => \slv_reg10_reg[5]\(0),
      I4 => \slv_reg10_reg[2]\,
      I5 => \^int_trigger_time_s_reg[0]_0\,
      O => \encoded_reg[0]_4\
    );
\i__carry_i_9__5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(6),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \int_trigger_volt_s_reg[9]\(6),
      O => \^state_reg[0]_2\
    );
\i__carry_i_9__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA80808000"
    )
        port map (
      I0 => \^state_reg[0]\,
      I1 => \^encoded_reg[8]_2\,
      I2 => \^state_reg[0]_0\,
      I3 => \slv_reg11_reg[0]\,
      I4 => \^state_reg[0]_3\,
      I5 => \^state_reg[0]_1\,
      O => \encoded_reg[8]_8\
    );
\pixel_color5_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8888800000000"
    )
        port map (
      I0 => \^state_reg[0]_2\,
      I1 => \^state_reg[0]_1\,
      I2 => \^encoded_reg[8]_2\,
      I3 => \^state_reg[0]_3\,
      I4 => \^state_reg[0]_0\,
      I5 => \^state_reg[0]\,
      O => \encoded_reg[8]_6\
    );
pixel_color5_carry_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAA8A8AAAA08A80"
    )
        port map (
      I0 => \^state_reg[0]_0\,
      I1 => \slv_reg11_reg[9]\(1),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => \int_trigger_volt_s_reg[9]\(1),
      I4 => \slv_reg11_reg[9]\(2),
      I5 => \int_trigger_volt_s_reg[9]\(2),
      O => \encoded_reg[8]_12\
    );
pixel_color5_carry_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000015155555555"
    )
        port map (
      I0 => \^state_reg[0]_1\,
      I1 => \int_trigger_volt_s_reg[9]\(2),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => \slv_reg11_reg[9]\(2),
      I4 => \^state_reg[0]_3\,
      I5 => \^state_reg[0]_0\,
      O => \encoded_reg[8]_9\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl is
  port (
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \initA_reg[6]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    initEn_reg : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    clk_out2 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    data_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    reset_n : in STD_LOGIC;
    stb : in STD_LOGIC;
    \state_reg[3]\ : in STD_LOGIC;
    \delaycnt_reg[12]\ : in STD_LOGIC;
    \state_reg[3]_0\ : in STD_LOGIC;
    \state_reg[1]\ : in STD_LOGIC;
    \initWord_reg[6]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \initA_reg[6]_0\ : in STD_LOGIC;
    initEn : in STD_LOGIC;
    msg : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl is
  signal DONE_O_i_1_n_0 : STD_LOGIC;
  signal DONE_O_i_3_n_0 : STD_LOGIC;
  signal DONE_O_i_4_n_0 : STD_LOGIC;
  signal DONE_O_i_5_n_0 : STD_LOGIC;
  signal ERR_O_i_1_n_0 : STD_LOGIC;
  signal ERR_O_i_2_n_0 : STD_LOGIC;
  signal \FSM_gray_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_gray_state[3]_i_9_n_0\ : STD_LOGIC;
  signal \FSM_gray_state_reg[3]_i_5_n_0\ : STD_LOGIC;
  signal addrNData_i_1_n_0 : STD_LOGIC;
  signal addrNData_reg_n_0 : STD_LOGIC;
  signal arbLost : STD_LOGIC;
  signal bitCount : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \bitCount[0]_i_1_n_0\ : STD_LOGIC;
  signal \bitCount[1]_i_1_n_0\ : STD_LOGIC;
  signal \bitCount[2]_i_1_n_0\ : STD_LOGIC;
  signal busFreeCnt0 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal busFreeCnt0_1 : STD_LOGIC;
  signal \busFreeCnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \busFreeCnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \busFreeCnt_reg__0\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal busState0 : STD_LOGIC;
  signal \busState[0]_i_1_n_0\ : STD_LOGIC;
  signal \busState[1]_i_1_n_0\ : STD_LOGIC;
  signal \busState_reg_n_0_[0]\ : STD_LOGIC;
  signal \busState_reg_n_0_[1]\ : STD_LOGIC;
  signal dScl : STD_LOGIC;
  signal dataByte0 : STD_LOGIC;
  signal dataByte1 : STD_LOGIC;
  signal \dataByte[7]_i_1_n_0\ : STD_LOGIC;
  signal \dataByte[7]_i_4_n_0\ : STD_LOGIC;
  signal \dataByte[7]_i_5_n_0\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[0]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[1]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[2]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[3]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[4]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[5]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[6]\ : STD_LOGIC;
  signal \dataByte_reg_n_0_[7]\ : STD_LOGIC;
  signal ddSda : STD_LOGIC;
  signal done : STD_LOGIC;
  signal error : STD_LOGIC;
  signal initEn_i_2_n_0 : STD_LOGIC;
  signal int_Rst : STD_LOGIC;
  signal int_Rst_i_1_n_0 : STD_LOGIC;
  signal nstate122_out : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 0 to 0 );
  signal p_0_in7_in : STD_LOGIC;
  signal p_19_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_1_in4_in : STD_LOGIC;
  signal rScl : STD_LOGIC;
  signal rScl_i_1_n_0 : STD_LOGIC;
  signal rScl_i_2_n_0 : STD_LOGIC;
  signal rSda : STD_LOGIC;
  signal rSda_i_1_n_0 : STD_LOGIC;
  signal rSda_i_2_n_0 : STD_LOGIC;
  signal rSda_i_3_n_0 : STD_LOGIC;
  signal rSda_i_4_n_0 : STD_LOGIC;
  signal rSda_i_5_n_0 : STD_LOGIC;
  signal rSda_i_6_n_0 : STD_LOGIC;
  signal rSda_i_7_n_0 : STD_LOGIC;
  signal rSda_i_8_n_0 : STD_LOGIC;
  signal sclCnt0 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal sclCnt0_0 : STD_LOGIC;
  signal \sclCnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \sclCnt[6]_i_2_n_0\ : STD_LOGIC;
  signal \sclCnt[6]_i_4_n_0\ : STD_LOGIC;
  signal \sclCnt_reg__0\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal scl_INST_0_i_1_n_0 : STD_LOGIC;
  signal sda_INST_0_i_1_n_0 : STD_LOGIC;
  signal state : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of state : signal is "yes";
  signal \state[1]_i_2_n_0\ : STD_LOGIC;
  signal \subState[0]_i_1_n_0\ : STD_LOGIC;
  signal \subState[1]_i_1_n_0\ : STD_LOGIC;
  signal \subState[1]_i_2_n_0\ : STD_LOGIC;
  signal \subState[1]_i_3_n_0\ : STD_LOGIC;
  signal \subState_reg_n_0_[0]\ : STD_LOGIC;
  signal \subState_reg_n_0_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of ERR_O_i_2 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \FSM_gray_state[2]_i_3\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \FSM_gray_state[3]_i_10\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \FSM_gray_state[3]_i_3\ : label is "soft_lutpair17";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_gray_state_reg[0]\ : label is "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100";
  attribute KEEP : string;
  attribute KEEP of \FSM_gray_state_reg[0]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_gray_state_reg[1]\ : label is "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100";
  attribute KEEP of \FSM_gray_state_reg[1]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_gray_state_reg[2]\ : label is "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100";
  attribute KEEP of \FSM_gray_state_reg[2]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_gray_state_reg[3]\ : label is "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100";
  attribute KEEP of \FSM_gray_state_reg[3]\ : label is "yes";
  attribute SOFT_HLUTNM of \bitCount[0]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \bitCount[1]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \bitCount[2]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \busFreeCnt[0]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \busFreeCnt[2]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \busFreeCnt[3]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \busFreeCnt[4]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of rScl_i_1 : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of rSda_i_6 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \sclCnt[1]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \sclCnt[2]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \sclCnt[3]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \sclCnt[4]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \sclCnt[6]_i_3\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of sda_INST_0_i_1 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \state[0]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \state[1]_i_2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \state[2]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \state[3]_i_2\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \subState[1]_i_2\ : label is "soft_lutpair23";
begin
DONE_O_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888888888F888888"
    )
        port map (
      I0 => arbLost,
      I1 => p_0_in7_in,
      I2 => DONE_O_i_3_n_0,
      I3 => \subState[1]_i_2_n_0\,
      I4 => \subState_reg_n_0_[0]\,
      I5 => \subState_reg_n_0_[1]\,
      O => DONE_O_i_1_n_0
    );
DONE_O_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => state(1),
      I1 => state(0),
      I2 => state(2),
      I3 => state(3),
      O => p_0_in7_in
    );
DONE_O_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAAFF0CFF"
    )
        port map (
      I0 => DONE_O_i_4_n_0,
      I1 => addrNData_reg_n_0,
      I2 => p_0_in(0),
      I3 => DONE_O_i_5_n_0,
      I4 => state(2),
      I5 => state(3),
      O => DONE_O_i_3_n_0
    );
DONE_O_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => bitCount(2),
      I1 => bitCount(0),
      I2 => bitCount(1),
      O => DONE_O_i_4_n_0
    );
DONE_O_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => state(1),
      I1 => state(0),
      O => DONE_O_i_5_n_0
    );
DONE_O_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => DONE_O_i_1_n_0,
      Q => done,
      R => '0'
    );
ERR_O_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F0008800"
    )
        port map (
      I0 => ERR_O_i_2_n_0,
      I1 => p_0_in(0),
      I2 => arbLost,
      I3 => state(1),
      I4 => state(0),
      I5 => \subState[1]_i_3_n_0\,
      O => ERR_O_i_1_n_0
    );
ERR_O_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \subState[1]_i_2_n_0\,
      I1 => \subState_reg_n_0_[0]\,
      I2 => \subState_reg_n_0_[1]\,
      O => ERR_O_i_2_n_0
    );
ERR_O_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => ERR_O_i_1_n_0,
      Q => error,
      R => '0'
    );
\FSM_gray_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4445444544555555"
    )
        port map (
      I0 => state(3),
      I1 => \FSM_gray_state[0]_i_2_n_0\,
      I2 => state(0),
      I3 => state(2),
      I4 => arbLost,
      I5 => state(1),
      O => \FSM_gray_state[0]_i_1_n_0\
    );
\FSM_gray_state[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => int_Rst,
      I1 => stb,
      I2 => state(0),
      I3 => state(1),
      O => \FSM_gray_state[0]_i_2_n_0\
    );
\FSM_gray_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A2AAAAAA"
    )
        port map (
      I0 => \FSM_gray_state[1]_i_2_n_0\,
      I1 => msg,
      I2 => state(0),
      I3 => state(1),
      I4 => \FSM_gray_state[1]_i_3_n_0\,
      O => \FSM_gray_state[1]_i_1_n_0\
    );
\FSM_gray_state[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000A000517A017A"
    )
        port map (
      I0 => state(0),
      I1 => arbLost,
      I2 => state(1),
      I3 => state(2),
      I4 => nstate122_out,
      I5 => state(3),
      O => \FSM_gray_state[1]_i_2_n_0\
    );
\FSM_gray_state[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAABA"
    )
        port map (
      I0 => state(2),
      I1 => addrNData_reg_n_0,
      I2 => stb,
      I3 => \dataByte_reg_n_0_[0]\,
      I4 => int_Rst,
      O => \FSM_gray_state[1]_i_3_n_0\
    );
\FSM_gray_state[1]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => stb,
      I1 => int_Rst,
      O => nstate122_out
    );
\FSM_gray_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EE0000000000EE0E"
    )
        port map (
      I0 => \FSM_gray_state[2]_i_2_n_0\,
      I1 => state(2),
      I2 => arbLost,
      I3 => state(1),
      I4 => state(0),
      I5 => state(3),
      O => \FSM_gray_state[2]_i_1_n_0\
    );
\FSM_gray_state[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4440444044404444"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => int_Rst,
      I3 => \dataByte_reg_n_0_[0]\,
      I4 => addrNData_reg_n_0,
      I5 => stb,
      O => \FSM_gray_state[2]_i_2_n_0\
    );
\FSM_gray_state[2]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rSda,
      I1 => dScl,
      I2 => p_0_in(0),
      O => arbLost
    );
\FSM_gray_state[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_19_in,
      I1 => state(3),
      I2 => \FSM_gray_state[3]_i_4_n_0\,
      I3 => state(2),
      I4 => \FSM_gray_state_reg[3]_i_5_n_0\,
      O => \FSM_gray_state[3]_i_1_n_0\
    );
\FSM_gray_state[3]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => bitCount(0),
      I1 => bitCount(1),
      I2 => bitCount(2),
      O => \FSM_gray_state[3]_i_10_n_0\
    );
\FSM_gray_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => state(1),
      I1 => state(0),
      I2 => stb,
      I3 => int_Rst,
      I4 => \FSM_gray_state[3]_i_6_n_0\,
      I5 => msg,
      O => \FSM_gray_state[3]_i_2_n_0\
    );
\FSM_gray_state[3]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \subState[1]_i_2_n_0\,
      I1 => \subState_reg_n_0_[1]\,
      I2 => \subState_reg_n_0_[0]\,
      O => p_19_in
    );
\FSM_gray_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB8B8B8B8B8B8"
    )
        port map (
      I0 => \FSM_gray_state[3]_i_7_n_0\,
      I1 => state(1),
      I2 => p_19_in,
      I3 => p_0_in(0),
      I4 => dScl,
      I5 => rSda,
      O => \FSM_gray_state[3]_i_4_n_0\
    );
\FSM_gray_state[3]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      O => \FSM_gray_state[3]_i_6_n_0\
    );
\FSM_gray_state[3]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20006000"
    )
        port map (
      I0 => state(0),
      I1 => \subState_reg_n_0_[0]\,
      I2 => \subState_reg_n_0_[1]\,
      I3 => \subState[1]_i_2_n_0\,
      I4 => \FSM_gray_state[3]_i_10_n_0\,
      O => \FSM_gray_state[3]_i_7_n_0\
    );
\FSM_gray_state[3]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88888888B8888888"
    )
        port map (
      I0 => p_19_in,
      I1 => state(0),
      I2 => \busState_reg_n_0_[0]\,
      I3 => reset_n,
      I4 => stb,
      I5 => \busState_reg_n_0_[1]\,
      O => \FSM_gray_state[3]_i_8_n_0\
    );
\FSM_gray_state[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAABFFFFAAAA0000"
    )
        port map (
      I0 => arbLost,
      I1 => bitCount(2),
      I2 => bitCount(0),
      I3 => bitCount(1),
      I4 => state(0),
      I5 => p_19_in,
      O => \FSM_gray_state[3]_i_9_n_0\
    );
\FSM_gray_state_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \FSM_gray_state[3]_i_1_n_0\,
      D => \FSM_gray_state[0]_i_1_n_0\,
      Q => state(0),
      R => '0'
    );
\FSM_gray_state_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \FSM_gray_state[3]_i_1_n_0\,
      D => \FSM_gray_state[1]_i_1_n_0\,
      Q => state(1),
      R => '0'
    );
\FSM_gray_state_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \FSM_gray_state[3]_i_1_n_0\,
      D => \FSM_gray_state[2]_i_1_n_0\,
      Q => state(2),
      R => '0'
    );
\FSM_gray_state_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \FSM_gray_state[3]_i_1_n_0\,
      D => \FSM_gray_state[3]_i_2_n_0\,
      Q => state(3),
      R => '0'
    );
\FSM_gray_state_reg[3]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \FSM_gray_state[3]_i_8_n_0\,
      I1 => \FSM_gray_state[3]_i_9_n_0\,
      O => \FSM_gray_state_reg[3]_i_5_n_0\,
      S => state(1)
    );
addrNData_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0AEAEAEAEAEAEAEA"
    )
        port map (
      I0 => addrNData_reg_n_0,
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => \subState[1]_i_2_n_0\,
      I3 => \subState_reg_n_0_[1]\,
      I4 => \subState_reg_n_0_[0]\,
      I5 => p_1_in4_in,
      O => addrNData_i_1_n_0
    );
addrNData_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => state(2),
      I3 => state(3),
      O => p_1_in4_in
    );
addrNData_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => addrNData_i_1_n_0,
      Q => addrNData_reg_n_0,
      R => '0'
    );
\bitCount[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F6"
    )
        port map (
      I0 => bitCount(0),
      I1 => dataByte0,
      I2 => dataByte1,
      O => \bitCount[0]_i_1_n_0\
    );
\bitCount[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFA6"
    )
        port map (
      I0 => bitCount(1),
      I1 => dataByte0,
      I2 => bitCount(0),
      I3 => dataByte1,
      O => \bitCount[1]_i_1_n_0\
    );
\bitCount[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFAAA6"
    )
        port map (
      I0 => bitCount(2),
      I1 => dataByte0,
      I2 => bitCount(1),
      I3 => bitCount(0),
      I4 => dataByte1,
      O => \bitCount[2]_i_1_n_0\
    );
\bitCount[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002030000020000"
    )
        port map (
      I0 => p_19_in,
      I1 => state(2),
      I2 => state(3),
      I3 => state(0),
      I4 => state(1),
      I5 => \subState[1]_i_2_n_0\,
      O => dataByte1
    );
\bitCount_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \bitCount[0]_i_1_n_0\,
      Q => bitCount(0),
      R => '0'
    );
\bitCount_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \bitCount[1]_i_1_n_0\,
      Q => bitCount(1),
      R => '0'
    );
\bitCount_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \bitCount[2]_i_1_n_0\,
      Q => bitCount(2),
      R => '0'
    );
\busFreeCnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(0),
      O => busFreeCnt0(0)
    );
\busFreeCnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(1),
      I1 => \busFreeCnt_reg__0\(0),
      O => \busFreeCnt[1]_i_1_n_0\
    );
\busFreeCnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(2),
      I1 => \busFreeCnt_reg__0\(0),
      I2 => \busFreeCnt_reg__0\(1),
      O => busFreeCnt0(2)
    );
\busFreeCnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA9"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(3),
      I1 => \busFreeCnt_reg__0\(2),
      I2 => \busFreeCnt_reg__0\(1),
      I3 => \busFreeCnt_reg__0\(0),
      O => busFreeCnt0(3)
    );
\busFreeCnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA9"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(4),
      I1 => \busFreeCnt_reg__0\(3),
      I2 => \busFreeCnt_reg__0\(0),
      I3 => \busFreeCnt_reg__0\(1),
      I4 => \busFreeCnt_reg__0\(2),
      O => busFreeCnt0(4)
    );
\busFreeCnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA9"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(5),
      I1 => \busFreeCnt_reg__0\(4),
      I2 => \busFreeCnt_reg__0\(2),
      I3 => \busFreeCnt_reg__0\(1),
      I4 => \busFreeCnt_reg__0\(0),
      I5 => \busFreeCnt_reg__0\(3),
      O => busFreeCnt0(5)
    );
\busFreeCnt[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => int_Rst,
      I1 => p_0_in(0),
      I2 => dScl,
      O => busFreeCnt0_1
    );
\busFreeCnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(6),
      I1 => \busFreeCnt[6]_i_3_n_0\,
      O => busFreeCnt0(6)
    );
\busFreeCnt[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(4),
      I1 => \busFreeCnt_reg__0\(2),
      I2 => \busFreeCnt_reg__0\(1),
      I3 => \busFreeCnt_reg__0\(0),
      I4 => \busFreeCnt_reg__0\(3),
      I5 => \busFreeCnt_reg__0\(5),
      O => \busFreeCnt[6]_i_3_n_0\
    );
\busFreeCnt_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(0),
      Q => \busFreeCnt_reg__0\(0),
      S => busFreeCnt0_1
    );
\busFreeCnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \busFreeCnt[1]_i_1_n_0\,
      Q => \busFreeCnt_reg__0\(1),
      R => busFreeCnt0_1
    );
\busFreeCnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(2),
      Q => \busFreeCnt_reg__0\(2),
      S => busFreeCnt0_1
    );
\busFreeCnt_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(3),
      Q => \busFreeCnt_reg__0\(3),
      S => busFreeCnt0_1
    );
\busFreeCnt_reg[4]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(4),
      Q => \busFreeCnt_reg__0\(4),
      S => busFreeCnt0_1
    );
\busFreeCnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(5),
      Q => \busFreeCnt_reg__0\(5),
      S => busFreeCnt0_1
    );
\busFreeCnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => busFreeCnt0(6),
      Q => \busFreeCnt_reg__0\(6),
      S => busFreeCnt0_1
    );
\busState[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4555FFFF45550000"
    )
        port map (
      I0 => int_Rst,
      I1 => p_0_in(0),
      I2 => dScl,
      I3 => ddSda,
      I4 => busState0,
      I5 => \busState_reg_n_0_[0]\,
      O => \busState[0]_i_1_n_0\
    );
\busState[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => p_0_in(0),
      I1 => dScl,
      I2 => ddSda,
      I3 => int_Rst,
      I4 => busState0,
      I5 => \busState_reg_n_0_[1]\,
      O => \busState[1]_i_1_n_0\
    );
\busState[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF4444F444"
    )
        port map (
      I0 => \busFreeCnt_reg__0\(6),
      I1 => \busFreeCnt[6]_i_3_n_0\,
      I2 => ddSda,
      I3 => dScl,
      I4 => p_0_in(0),
      I5 => int_Rst,
      O => busState0
    );
\busState_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \busState[0]_i_1_n_0\,
      Q => \busState_reg_n_0_[0]\,
      R => '0'
    );
\busState_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \busState[1]_i_1_n_0\,
      Q => \busState_reg_n_0_[1]\,
      R => '0'
    );
dScl_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => scl,
      Q => dScl,
      R => '0'
    );
dSda_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => sda,
      Q => p_0_in(0),
      R => '0'
    );
\dataByte[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F20"
    )
        port map (
      I0 => data_i(0),
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => \dataByte[7]_i_4_n_0\,
      I3 => p_0_in(0),
      O => p_1_in(0)
    );
\dataByte[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \dataByte[7]_i_5_n_0\,
      I1 => data_i(1),
      I2 => \dataByte[7]_i_4_n_0\,
      I3 => \dataByte_reg_n_0_[0]\,
      O => p_1_in(1)
    );
\dataByte[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \dataByte[7]_i_5_n_0\,
      I1 => data_i(2),
      I2 => \dataByte[7]_i_4_n_0\,
      I3 => \dataByte_reg_n_0_[1]\,
      O => p_1_in(2)
    );
\dataByte[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F20"
    )
        port map (
      I0 => data_i(3),
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => \dataByte[7]_i_4_n_0\,
      I3 => \dataByte_reg_n_0_[2]\,
      O => p_1_in(3)
    );
\dataByte[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \dataByte[7]_i_5_n_0\,
      I1 => data_i(4),
      I2 => \dataByte[7]_i_4_n_0\,
      I3 => \dataByte_reg_n_0_[3]\,
      O => p_1_in(4)
    );
\dataByte[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \dataByte[7]_i_5_n_0\,
      I1 => data_i(5),
      I2 => \dataByte[7]_i_4_n_0\,
      I3 => \dataByte_reg_n_0_[4]\,
      O => p_1_in(5)
    );
\dataByte[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \dataByte[7]_i_5_n_0\,
      I1 => data_i(6),
      I2 => \dataByte[7]_i_4_n_0\,
      I3 => \dataByte_reg_n_0_[5]\,
      O => p_1_in(6)
    );
\dataByte[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => dataByte0,
      I1 => \dataByte[7]_i_4_n_0\,
      O => \dataByte[7]_i_1_n_0\
    );
\dataByte[7]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F20"
    )
        port map (
      I0 => data_i(7),
      I1 => \dataByte[7]_i_5_n_0\,
      I2 => \dataByte[7]_i_4_n_0\,
      I3 => \dataByte_reg_n_0_[6]\,
      O => p_1_in(7)
    );
\dataByte[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0200330002000000"
    )
        port map (
      I0 => p_19_in,
      I1 => state(3),
      I2 => state(2),
      I3 => state(1),
      I4 => state(0),
      I5 => ERR_O_i_2_n_0,
      O => dataByte0
    );
\dataByte[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0220020002000200"
    )
        port map (
      I0 => \subState[1]_i_2_n_0\,
      I1 => \subState[1]_i_3_n_0\,
      I2 => state(1),
      I3 => state(0),
      I4 => \subState_reg_n_0_[1]\,
      I5 => \subState_reg_n_0_[0]\,
      O => \dataByte[7]_i_4_n_0\
    );
\dataByte[7]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => state(2),
      I1 => state(3),
      I2 => state(0),
      I3 => state(1),
      O => \dataByte[7]_i_5_n_0\
    );
\dataByte_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => p_1_in(0),
      Q => \dataByte_reg_n_0_[0]\,
      R => '0'
    );
\dataByte_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => p_1_in(1),
      Q => \dataByte_reg_n_0_[1]\,
      R => '0'
    );
\dataByte_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => p_1_in(2),
      Q => \dataByte_reg_n_0_[2]\,
      R => '0'
    );
\dataByte_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => p_1_in(3),
      Q => \dataByte_reg_n_0_[3]\,
      R => '0'
    );
\dataByte_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => p_1_in(4),
      Q => \dataByte_reg_n_0_[4]\,
      R => '0'
    );
\dataByte_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => p_1_in(5),
      Q => \dataByte_reg_n_0_[5]\,
      R => '0'
    );
\dataByte_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => p_1_in(6),
      Q => \dataByte_reg_n_0_[6]\,
      R => '0'
    );
\dataByte_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \dataByte[7]_i_1_n_0\,
      D => p_1_in(7),
      Q => \dataByte_reg_n_0_[7]\,
      R => '0'
    );
ddSda_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => p_0_in(0),
      Q => ddSda,
      R => '0'
    );
\initA[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAABAAAAAAAAA"
    )
        port map (
      I0 => \state_reg[3]_0\,
      I1 => Q(3),
      I2 => done,
      I3 => Q(2),
      I4 => error,
      I5 => \state_reg[1]\,
      O => \initA_reg[6]\(0)
    );
initEn_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F777F7F40444040"
    )
        port map (
      I0 => Q(2),
      I1 => reset_n,
      I2 => \state_reg[3]_0\,
      I3 => initEn_i_2_n_0,
      I4 => \state_reg[3]\,
      I5 => initEn,
      O => initEn_reg
    );
initEn_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => error,
      I1 => Q(2),
      I2 => done,
      O => initEn_i_2_n_0
    );
int_Rst_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBBBBBBBBBB3"
    )
        port map (
      I0 => int_Rst,
      I1 => reset_n,
      I2 => state(1),
      I3 => state(0),
      I4 => state(2),
      I5 => state(3),
      O => int_Rst_i_1_n_0
    );
int_Rst_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => int_Rst_i_1_n_0,
      Q => int_Rst,
      R => '0'
    );
rScl_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F70"
    )
        port map (
      I0 => \subState_reg_n_0_[1]\,
      I1 => \subState_reg_n_0_[0]\,
      I2 => rScl_i_2_n_0,
      I3 => rScl,
      O => rScl_i_1_n_0
    );
rScl_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9554D554D5540000"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      I2 => state(1),
      I3 => state(0),
      I4 => \subState_reg_n_0_[0]\,
      I5 => \subState_reg_n_0_[1]\,
      O => rScl_i_2_n_0
    );
rScl_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => rScl_i_1_n_0,
      Q => rScl,
      R => '0'
    );
rSda_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAABFFFFAAAB0000"
    )
        port map (
      I0 => rSda_i_2_n_0,
      I1 => rSda_i_3_n_0,
      I2 => \subState_reg_n_0_[0]\,
      I3 => \subState_reg_n_0_[1]\,
      I4 => rSda_i_4_n_0,
      I5 => rSda,
      O => rSda_i_1_n_0
    );
rSda_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFC303030F430F4"
    )
        port map (
      I0 => rSda_i_5_n_0,
      I1 => rSda_i_6_n_0,
      I2 => rSda_i_7_n_0,
      I3 => rSda_i_8_n_0,
      I4 => \dataByte_reg_n_0_[7]\,
      I5 => p_0_in7_in,
      O => rSda_i_2_n_0
    );
rSda_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      I2 => state(1),
      O => rSda_i_3_n_0
    );
rSda_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"010041004100D554"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      I2 => state(1),
      I3 => state(0),
      I4 => \subState_reg_n_0_[0]\,
      I5 => \subState_reg_n_0_[1]\,
      O => rSda_i_4_n_0
    );
rSda_i_5: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => state(3),
      I1 => state(1),
      I2 => state(0),
      O => rSda_i_5_n_0
    );
rSda_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \subState_reg_n_0_[0]\,
      I1 => \subState_reg_n_0_[1]\,
      O => rSda_i_6_n_0
    );
rSda_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00004000FFFF3FFF"
    )
        port map (
      I0 => \subState_reg_n_0_[0]\,
      I1 => state(1),
      I2 => state(0),
      I3 => state(2),
      I4 => state(3),
      I5 => \subState_reg_n_0_[1]\,
      O => rSda_i_7_n_0
    );
rSda_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => state(2),
      I1 => state(0),
      I2 => state(1),
      I3 => state(3),
      O => rSda_i_8_n_0
    );
rSda_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => rSda_i_1_n_0,
      Q => rSda,
      R => '0'
    );
\sclCnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \sclCnt_reg__0\(0),
      O => sclCnt0(0)
    );
\sclCnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \sclCnt_reg__0\(1),
      I1 => \sclCnt_reg__0\(0),
      O => \sclCnt[1]_i_1_n_0\
    );
\sclCnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => \sclCnt_reg__0\(2),
      I1 => \sclCnt_reg__0\(0),
      I2 => \sclCnt_reg__0\(1),
      O => sclCnt0(2)
    );
\sclCnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA9"
    )
        port map (
      I0 => \sclCnt_reg__0\(3),
      I1 => \sclCnt_reg__0\(2),
      I2 => \sclCnt_reg__0\(1),
      I3 => \sclCnt_reg__0\(0),
      O => sclCnt0(3)
    );
\sclCnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA9"
    )
        port map (
      I0 => \sclCnt_reg__0\(4),
      I1 => \sclCnt_reg__0\(3),
      I2 => \sclCnt_reg__0\(0),
      I3 => \sclCnt_reg__0\(1),
      I4 => \sclCnt_reg__0\(2),
      O => sclCnt0(4)
    );
\sclCnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA9"
    )
        port map (
      I0 => \sclCnt_reg__0\(5),
      I1 => \sclCnt_reg__0\(4),
      I2 => \sclCnt_reg__0\(2),
      I3 => \sclCnt_reg__0\(1),
      I4 => \sclCnt_reg__0\(0),
      I5 => \sclCnt_reg__0\(3),
      O => sclCnt0(5)
    );
\sclCnt[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      I2 => state(0),
      I3 => state(1),
      I4 => \subState[1]_i_2_n_0\,
      O => sclCnt0_0
    );
\sclCnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => dScl,
      I1 => rScl,
      O => \sclCnt[6]_i_2_n_0\
    );
\sclCnt[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sclCnt_reg__0\(6),
      I1 => \sclCnt[6]_i_4_n_0\,
      O => sclCnt0(6)
    );
\sclCnt[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \sclCnt_reg__0\(4),
      I1 => \sclCnt_reg__0\(2),
      I2 => \sclCnt_reg__0\(1),
      I3 => \sclCnt_reg__0\(0),
      I4 => \sclCnt_reg__0\(3),
      I5 => \sclCnt_reg__0\(5),
      O => \sclCnt[6]_i_4_n_0\
    );
\sclCnt_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(0),
      Q => \sclCnt_reg__0\(0),
      S => sclCnt0_0
    );
\sclCnt_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => \sclCnt[1]_i_1_n_0\,
      Q => \sclCnt_reg__0\(1),
      S => sclCnt0_0
    );
\sclCnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(2),
      Q => \sclCnt_reg__0\(2),
      S => sclCnt0_0
    );
\sclCnt_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(3),
      Q => \sclCnt_reg__0\(3),
      S => sclCnt0_0
    );
\sclCnt_reg[4]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(4),
      Q => \sclCnt_reg__0\(4),
      S => sclCnt0_0
    );
\sclCnt_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(5),
      Q => \sclCnt_reg__0\(5),
      R => sclCnt0_0
    );
\sclCnt_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => \sclCnt[6]_i_2_n_0\,
      D => sclCnt0(6),
      Q => \sclCnt_reg__0\(6),
      R => sclCnt0_0
    );
scl_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => '0',
      I1 => scl_INST_0_i_1_n_0,
      I2 => '0',
      I3 => '0',
      I4 => '0',
      I5 => '0',
      O => scl
    );
scl_INST_0_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rScl,
      O => scl_INST_0_i_1_n_0
    );
sda_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => '0',
      I1 => sda_INST_0_i_1_n_0,
      I2 => '0',
      I3 => '0',
      I4 => '0',
      I5 => '0',
      O => sda
    );
sda_INST_0_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rSda,
      O => sda_INST_0_i_1_n_0
    );
\state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FF0D"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(2),
      I3 => Q(3),
      I4 => error,
      O => D(0)
    );
\state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00A8AA00AAAAAA00"
    )
        port map (
      I0 => \state[1]_i_2_n_0\,
      I1 => \initWord_reg[6]\(1),
      I2 => \initWord_reg[6]\(0),
      I3 => Q(0),
      I4 => Q(1),
      I5 => \initA_reg[6]_0\,
      O => D(1)
    );
\state[1]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => Q(3),
      I1 => error,
      I2 => Q(2),
      O => \state[1]_i_2_n_0\
    );
\state[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E0EE"
    )
        port map (
      I0 => \state_reg[3]\,
      I1 => error,
      I2 => Q(3),
      I3 => Q(2),
      O => D(2)
    );
\state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80CF00AA800F00AA"
    )
        port map (
      I0 => done,
      I1 => Q(1),
      I2 => Q(0),
      I3 => Q(3),
      I4 => Q(2),
      I5 => \delaycnt_reg[12]\,
      O => E(0)
    );
\state[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => Q(2),
      I1 => error,
      I2 => Q(3),
      I3 => \state_reg[1]\,
      O => D(3)
    );
\subState[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6666666666666660"
    )
        port map (
      I0 => \subState_reg_n_0_[0]\,
      I1 => \subState[1]_i_2_n_0\,
      I2 => state(3),
      I3 => state(2),
      I4 => state(0),
      I5 => state(1),
      O => \subState[0]_i_1_n_0\
    );
\subState[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6A6A6A6A6A6A6A00"
    )
        port map (
      I0 => \subState_reg_n_0_[1]\,
      I1 => \subState[1]_i_2_n_0\,
      I2 => \subState_reg_n_0_[0]\,
      I3 => \subState[1]_i_3_n_0\,
      I4 => state(0),
      I5 => state(1),
      O => \subState[1]_i_1_n_0\
    );
\subState[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \sclCnt[6]_i_4_n_0\,
      I1 => \sclCnt_reg__0\(6),
      O => \subState[1]_i_2_n_0\
    );
\subState[1]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => state(3),
      I1 => state(2),
      O => \subState[1]_i_3_n_0\
    );
\subState_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \subState[0]_i_1_n_0\,
      Q => \subState_reg_n_0_[0]\,
      R => '0'
    );
\subState_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \subState[1]_i_1_n_0\,
      Q => \subState_reg_n_0_[1]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    lopt : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz is
  signal clk_out1_clk_wiz_0 : STD_LOGIC;
  signal clk_out2_clk_wiz_0 : STD_LOGIC;
  signal clk_out3_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_buf_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_clk_wiz_0 : STD_LOGIC;
  signal reset_high : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_LOCKED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of clkf_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout1_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout3_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of mmcm_adv_inst : label is "PRIMITIVE";
begin
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_clk_wiz_0,
      O => clkfbout_buf_clk_wiz_0
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out1_clk_wiz_0,
      O => clk_out1
    );
clkout2_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out2_clk_wiz_0,
      O => clk_out2
    );
clkout3_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out3_clk_wiz_0,
      O => clk_out3
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 10.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 10.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 40.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 8,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout_buf_clk_wiz_0,
      CLKFBOUT => clkfbout_clk_wiz_0,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => lopt,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clk_out1_clk_wiz_0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clk_out2_clk_wiz_0,
      CLKOUT1B => clk_out3_clk_wiz_0,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => NLW_mmcm_adv_inst_LOCKED_UNCONNECTED,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => reset_high
    );
mmcm_adv_inst_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => resetn,
      O => reset_high
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    lopt : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz is
  signal clk_in1_clk_wiz_1 : STD_LOGIC;
  signal clk_out1_clk_wiz_1 : STD_LOGIC;
  signal clk_out2_clk_wiz_1 : STD_LOGIC;
  signal clkfbout_buf_clk_wiz_1 : STD_LOGIC;
  signal clkfbout_clk_wiz_1 : STD_LOGIC;
  signal reset_high : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_LOCKED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of clkf_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkin1_ibufg : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clkin1_ibufg : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clkin1_ibufg : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clkin1_ibufg : label is "AUTO";
  attribute BOX_TYPE of clkout1_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of mmcm_adv_inst : label is "PRIMITIVE";
begin
  lopt <= clk_in1_clk_wiz_1;
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_clk_wiz_1,
      O => clkfbout_buf_clk_wiz_1
    );
clkin1_ibufg: unisim.vcomponents.IBUF
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clk_in1,
      O => clk_in1_clk_wiz_1
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out1_clk_wiz_1,
      O => clk_out1
    );
clkout2_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out2_clk_wiz_1,
      O => clk_out2
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 10.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 10.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 81.375000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 20,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout_buf_clk_wiz_1,
      CLKFBOUT => clkfbout_clk_wiz_1,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => clk_in1_clk_wiz_1,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clk_out1_clk_wiz_1,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clk_out2_clk_wiz_1,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => NLW_mmcm_adv_inst_LOCKED_UNCONNECTED,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => reset_high
    );
mmcm_adv_inst_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => resetn,
      O => reset_high
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_flagRegister is
  port (
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Q_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_araddr_reg[4]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_0\ : in STD_LOGIC;
    \axi_araddr_reg[6]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_araddr_reg[4]_1\ : in STD_LOGIC;
    \slv_reg15_reg[2]\ : in STD_LOGIC;
    \slv_reg11_reg[2]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg10_reg[2]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_araddr_reg[3]_rep\ : in STD_LOGIC;
    \axi_araddr_reg[2]_rep\ : in STD_LOGIC;
    \slv_reg3_reg[2]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \processQ_reg[9]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    \^clk\ : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_flagRegister;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_flagRegister is
  signal \Q[0]_i_1_n_0\ : STD_LOGIC;
  signal \Q[2]_i_1_n_0\ : STD_LOGIC;
  signal \^q_reg[0]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \axi_rdata[2]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_4_n_0\ : STD_LOGIC;
  signal flagQ : STD_LOGIC_VECTOR ( 2 to 2 );
begin
  \Q_reg[0]_0\(0) <= \^q_reg[0]_0\(0);
\Q[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => \slv_reg3_reg[2]\(0),
      I1 => CLK,
      I2 => \^q_reg[0]_0\(0),
      O => \Q[0]_i_1_n_0\
    );
\Q[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => \slv_reg3_reg[2]\(1),
      I1 => \processQ_reg[9]\(0),
      I2 => flagQ(2),
      O => \Q[2]_i_1_n_0\
    );
\Q_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^clk\,
      CE => '1',
      D => \Q[0]_i_1_n_0\,
      Q => \^q_reg[0]_0\(0),
      R => reset_n
    );
\Q_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^clk\,
      CE => '1',
      D => \Q[2]_i_1_n_0\,
      Q => flagQ(2),
      R => reset_n
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]\,
      I1 => \axi_araddr_reg[4]_0\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_rdata_reg[2]_i_4_n_0\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_araddr_reg[4]_1\,
      O => D(0)
    );
\axi_rdata[2]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_reg11_reg[2]\(0),
      I1 => \slv_reg10_reg[2]\(0),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => flagQ(2),
      I4 => \axi_araddr_reg[2]_rep\,
      O => \axi_rdata[2]_i_10_n_0\
    );
\axi_rdata_reg[2]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_10_n_0\,
      I1 => \slv_reg15_reg[2]\,
      O => \axi_rdata_reg[2]_i_4_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_hcounter is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \encoded_reg[8]\ : out STD_LOGIC;
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \encoded_reg[8]_1\ : out STD_LOGIC;
    \processQ_reg[0]_0\ : out STD_LOGIC;
    ADDRARDADDR : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \encoded_reg[8]_2\ : out STD_LOGIC;
    \encoded_reg[8]_3\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_4\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_5\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_6\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_7\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_8\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_9\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_10\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[0]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_11\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_12\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_13\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_14\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_15\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_16\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_17\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[0]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[0]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[0]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[0]_3\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[0]_4\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[0]_5\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[0]_6\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[0]_7\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_18\ : out STD_LOGIC;
    \dc_bias_reg[3]\ : out STD_LOGIC;
    \encoded_reg[8]_19\ : out STD_LOGIC;
    \encoded_reg[2]\ : out STD_LOGIC;
    \encoded_reg[1]\ : out STD_LOGIC;
    \encoded_reg[4]\ : out STD_LOGIC;
    \encoded_reg[0]_8\ : out STD_LOGIC;
    \encoded_reg[0]_9\ : out STD_LOGIC;
    \encoded_reg[8]_20\ : out STD_LOGIC;
    \encoded_reg[0]_10\ : out STD_LOGIC;
    \encoded_reg[0]_11\ : out STD_LOGIC;
    \encoded_reg[8]_21\ : out STD_LOGIC;
    \encoded_reg[8]_22\ : out STD_LOGIC;
    \encoded_reg[8]_23\ : out STD_LOGIC;
    \encoded_reg[8]_24\ : out STD_LOGIC;
    \encoded_reg[9]\ : out STD_LOGIC;
    \encoded_reg[8]_25\ : out STD_LOGIC;
    \encoded_reg[8]_26\ : out STD_LOGIC;
    \encoded_reg[9]_0\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg5_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg10_reg[9]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg10_reg[1]\ : in STD_LOGIC;
    \processQ_reg[5]_0\ : in STD_LOGIC;
    \dc_bias_reg[1]\ : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    \dc_bias_reg[2]\ : in STD_LOGIC;
    \slv_reg10_reg[7]\ : in STD_LOGIC;
    \slv_reg10_reg[6]\ : in STD_LOGIC;
    \int_trigger_time_s_reg[5]\ : in STD_LOGIC;
    \slv_reg10_reg[9]_0\ : in STD_LOGIC;
    \slv_reg10_reg[8]\ : in STD_LOGIC;
    \slv_reg10_reg[7]_0\ : in STD_LOGIC;
    \slv_reg10_reg[5]\ : in STD_LOGIC;
    \slv_reg10_reg[6]_0\ : in STD_LOGIC;
    \slv_reg10_reg[6]_1\ : in STD_LOGIC;
    \slv_reg10_reg[5]_0\ : in STD_LOGIC;
    \slv_reg10_reg[2]\ : in STD_LOGIC;
    \slv_reg10_reg[3]\ : in STD_LOGIC;
    \slv_reg10_reg[4]\ : in STD_LOGIC;
    \slv_reg10_reg[5]_1\ : in STD_LOGIC;
    \slv_reg10_reg[6]_2\ : in STD_LOGIC;
    \slv_reg10_reg[5]_2\ : in STD_LOGIC;
    \int_trigger_time_s_reg[7]\ : in STD_LOGIC;
    \int_trigger_time_s_reg[1]\ : in STD_LOGIC;
    \slv_reg10_reg[6]_3\ : in STD_LOGIC;
    \int_trigger_time_s_reg[1]_0\ : in STD_LOGIC;
    \slv_reg10_reg[6]_4\ : in STD_LOGIC;
    \slv_reg10_reg[0]\ : in STD_LOGIC;
    \int_trigger_time_s_reg[1]_1\ : in STD_LOGIC;
    \int_trigger_time_s_reg[2]\ : in STD_LOGIC;
    \int_trigger_time_s_reg[3]\ : in STD_LOGIC;
    \int_trigger_time_s_reg[1]_2\ : in STD_LOGIC;
    \int_trigger_time_s_reg[1]_3\ : in STD_LOGIC;
    \slv_reg10_reg[2]_0\ : in STD_LOGIC;
    \slv_reg10_reg[0]_0\ : in STD_LOGIC;
    \processQ_reg[4]_0\ : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    switch : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \processQ_reg[9]_0\ : in STD_LOGIC;
    \processQ_reg[8]_0\ : in STD_LOGIC;
    \processQ_reg[9]_1\ : in STD_LOGIC;
    \processQ_reg[2]_0\ : in STD_LOGIC;
    \processQ_reg[8]_1\ : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_2\ : in STD_LOGIC;
    \processQ_reg[2]_1\ : in STD_LOGIC;
    \processQ_reg[4]_1\ : in STD_LOGIC;
    \processQ_reg[9]_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_4\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[0]_1\ : in STD_LOGIC;
    \processQ_reg[9]_5\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[8]_2\ : in STD_LOGIC;
    \processQ_reg[8]_3\ : in STD_LOGIC;
    \processQ_reg[0]_2\ : in STD_LOGIC;
    \slv_reg10_reg[9]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_6\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \processQ_reg[9]_7\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_8\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_9\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_10\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg10_reg[7]_1\ : in STD_LOGIC;
    \dc_bias_reg[3]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_hcounter;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_hcounter is
  signal \column__0\ : STD_LOGIC_VECTOR ( 9 downto 2 );
  signal \dc_bias[3]_i_11__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_12_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_14__1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_15__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_16__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_16__1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_17__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_19_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_23__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_23_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_24__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_24_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_25__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_25_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_26__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_27_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_33_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_34_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_35_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_36_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_37_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_38_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_39_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_3__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_40_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_41_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_42_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_50_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_51_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_56_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_57_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_58_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_5__1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_6_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_7_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_8__1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_9__0_n_0\ : STD_LOGIC;
  signal \encoded[8]_i_4__0_n_0\ : STD_LOGIC;
  signal \encoded[9]_i_2__0_n_0\ : STD_LOGIC;
  signal \^encoded_reg[0]_10\ : STD_LOGIC;
  signal \^encoded_reg[0]_11\ : STD_LOGIC;
  signal \^encoded_reg[0]_8\ : STD_LOGIC;
  signal \^encoded_reg[8]_0\ : STD_LOGIC;
  signal \^encoded_reg[8]_1\ : STD_LOGIC;
  signal \^encoded_reg[8]_18\ : STD_LOGIC;
  signal \^encoded_reg[8]_21\ : STD_LOGIC;
  signal \^encoded_reg[8]_22\ : STD_LOGIC;
  signal \^encoded_reg[8]_3\ : STD_LOGIC;
  signal \i__carry_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_8_n_0\ : STD_LOGIC;
  signal \plusOp__1\ : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal processQ0 : STD_LOGIC;
  signal \processQ[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \processQ[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \processQ[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \processQ[9]_i_1__1_n_0\ : STD_LOGIC;
  signal \processQ[9]_i_5__0_n_0\ : STD_LOGIC;
  signal \processQ[9]_i_6__0_n_0\ : STD_LOGIC;
  signal \^processq_reg[0]_0\ : STD_LOGIC;
  signal \^sdp_bl.ramb18_dp_bl.ram18_bl\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_i_30_n_0\ : STD_LOGIC;
  signal \sdp_bl.ramb18_dp_bl.ram18_bl_i_31_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \dc_bias[3]_i_17__0\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_19\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_23\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_24\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_25\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_25__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_27\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_34\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_35\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_37\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_40\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_41\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_5\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_50\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_55\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_56\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_57\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_58\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_6__0\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_8__1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \encoded[0]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \encoded[2]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \encoded[4]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \encoded[8]_i_2__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \encoded[8]_i_2__1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \processQ[1]_i_1__1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \processQ[2]_i_1__1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \processQ[3]_i_1__1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \processQ[4]_i_1__1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \processQ[6]_i_2__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \processQ[7]_i_1__1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \processQ[8]_i_1__1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \processQ[9]_i_3__1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \processQ[9]_i_5__0\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \sdp_bl.ramb18_dp_bl.ram18_bl_i_30\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \sdp_bl.ramb18_dp_bl.ram18_bl_i_31\ : label is "soft_lutpair55";
begin
  \encoded_reg[0]_10\ <= \^encoded_reg[0]_10\;
  \encoded_reg[0]_11\ <= \^encoded_reg[0]_11\;
  \encoded_reg[0]_8\ <= \^encoded_reg[0]_8\;
  \encoded_reg[8]_0\ <= \^encoded_reg[8]_0\;
  \encoded_reg[8]_1\ <= \^encoded_reg[8]_1\;
  \encoded_reg[8]_18\ <= \^encoded_reg[8]_18\;
  \encoded_reg[8]_21\ <= \^encoded_reg[8]_21\;
  \encoded_reg[8]_22\ <= \^encoded_reg[8]_22\;
  \encoded_reg[8]_3\ <= \^encoded_reg[8]_3\;
  \processQ_reg[0]_0\ <= \^processq_reg[0]_0\;
  \sdp_bl.ramb18_dp_bl.ram18_bl\(1 downto 0) <= \^sdp_bl.ramb18_dp_bl.ram18_bl\(1 downto 0);
\TDMS_encoder_red/encoded[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0003FFFF6665FFFF"
    )
        port map (
      I0 => \dc_bias_reg[3]_1\(0),
      I1 => \^encoded_reg[0]_8\,
      I2 => \encoded[9]_i_2__0_n_0\,
      I3 => \^encoded_reg[8]_3\,
      I4 => \processQ_reg[5]_0\,
      I5 => \dc_bias_reg[2]\,
      O => \encoded_reg[9]_0\
    );
\dc_bias[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EFFFEEEE"
    )
        port map (
      I0 => \processQ_reg[4]_0\,
      I1 => \^encoded_reg[8]_18\,
      I2 => CO(0),
      I3 => switch(0),
      I4 => \processQ_reg[9]_0\,
      I5 => \^encoded_reg[8]_3\,
      O => \^encoded_reg[8]_1\
    );
\dc_bias[3]_i_10__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BAAAAAAA"
    )
        port map (
      I0 => \processQ_reg[0]_1\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \processQ_reg[9]_4\(0),
      I4 => \processQ_reg[9]_3\(0),
      I5 => \dc_bias[3]_i_16__1_n_0\,
      O => \^encoded_reg[0]_10\
    );
\dc_bias[3]_i_10__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \dc_bias[3]_i_23_n_0\,
      I1 => \dc_bias[3]_i_25__0_n_0\,
      I2 => \dc_bias[3]_i_24_n_0\,
      I3 => \dc_bias[3]_i_19_n_0\,
      I4 => \column__0\(9),
      I5 => \column__0\(6),
      O => \^encoded_reg[8]_21\
    );
\dc_bias[3]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F80000000000001F"
    )
        port map (
      I0 => \column__0\(2),
      I1 => \column__0\(3),
      I2 => \column__0\(4),
      I3 => \column__0\(6),
      I4 => \column__0\(9),
      I5 => \column__0\(5),
      O => \^encoded_reg[8]_22\
    );
\dc_bias[3]_i_11__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A2AAAAAAAAAAAA2A"
    )
        port map (
      I0 => \dc_bias[3]_i_23__0_n_0\,
      I1 => \column__0\(7),
      I2 => \column__0\(6),
      I3 => \column__0\(8),
      I4 => \column__0\(5),
      I5 => \column__0\(4),
      O => \dc_bias[3]_i_11__0_n_0\
    );
\dc_bias[3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4F4F0F0FFF0F0F0"
    )
        port map (
      I0 => \dc_bias[3]_i_24__0_n_0\,
      I1 => \dc_bias[3]_i_25_n_0\,
      I2 => \dc_bias[3]_i_26__0_n_0\,
      I3 => \dc_bias[3]_i_27_n_0\,
      I4 => \column__0\(2),
      I5 => \column__0\(3),
      O => \dc_bias[3]_i_12_n_0\
    );
\dc_bias[3]_i_12__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEAEAEAAAAAAAAA"
    )
        port map (
      I0 => \processQ_reg[0]_2\,
      I1 => \slv_reg10_reg[9]_1\(0),
      I2 => \processQ_reg[9]_6\(1),
      I3 => \processQ_reg[9]_7\(0),
      I4 => \processQ_reg[9]_8\(0),
      I5 => \processQ_reg[9]_6\(0),
      O => \^encoded_reg[0]_11\
    );
\dc_bias[3]_i_14__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFBFF"
    )
        port map (
      I0 => \sdp_bl.ramb18_dp_bl.ram18_bl_i_30_n_0\,
      I1 => \column__0\(4),
      I2 => \column__0\(8),
      I3 => \dc_bias[3]_i_23_n_0\,
      I4 => \column__0\(9),
      I5 => \dc_bias[3]_i_25__0_n_0\,
      O => \encoded_reg[8]_23\
    );
\dc_bias[3]_i_14__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFBFFFFFFFFFF"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \column__0\(2),
      I2 => \column__0\(9),
      I3 => \dc_bias[3]_i_23_n_0\,
      I4 => \column__0\(8),
      I5 => \column__0\(4),
      O => \dc_bias[3]_i_14__1_n_0\
    );
\dc_bias[3]_i_15__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAEAEAEFFAEAEAE"
    )
        port map (
      I0 => \dc_bias[3]_i_33_n_0\,
      I1 => \dc_bias[3]_i_34_n_0\,
      I2 => \dc_bias[3]_i_35_n_0\,
      I3 => \dc_bias[3]_i_36_n_0\,
      I4 => \dc_bias[3]_i_37_n_0\,
      I5 => \column__0\(5),
      O => \dc_bias[3]_i_15__0_n_0\
    );
\dc_bias[3]_i_16__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4440444044404444"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \dc_bias[3]_i_38_n_0\,
      I3 => \dc_bias[3]_i_39_n_0\,
      I4 => \dc_bias[3]_i_40_n_0\,
      I5 => \dc_bias[3]_i_41_n_0\,
      O => \dc_bias[3]_i_16__0_n_0\
    );
\dc_bias[3]_i_16__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => \column__0\(6),
      I1 => \column__0\(9),
      I2 => \column__0\(5),
      I3 => \column__0\(4),
      I4 => \dc_bias[3]_i_24_n_0\,
      I5 => \dc_bias[3]_i_25__0_n_0\,
      O => \dc_bias[3]_i_16__1_n_0\
    );
\dc_bias[3]_i_17__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40404044"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \dc_bias[3]_i_42_n_0\,
      I3 => \dc_bias[3]_i_35_n_0\,
      I4 => \dc_bias[3]_i_40_n_0\,
      O => \dc_bias[3]_i_17__0_n_0\
    );
\dc_bias[3]_i_19\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \column__0\(4),
      O => \dc_bias[3]_i_19_n_0\
    );
\dc_bias[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAEBBB"
    )
        port map (
      I0 => \^encoded_reg[8]_3\,
      I1 => \processQ_reg[9]_0\,
      I2 => switch(0),
      I3 => CO(0),
      I4 => \^encoded_reg[8]_18\,
      I5 => \processQ_reg[4]_0\,
      O => \dc_bias_reg[3]\
    );
\dc_bias[3]_i_23\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      O => \dc_bias[3]_i_23_n_0\
    );
\dc_bias[3]_i_23__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \column__0\(2),
      O => \dc_bias[3]_i_23__0_n_0\
    );
\dc_bias[3]_i_24\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \column__0\(8),
      I1 => \column__0\(7),
      O => \dc_bias[3]_i_24_n_0\
    );
\dc_bias[3]_i_24__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F8000000"
    )
        port map (
      I0 => \column__0\(8),
      I1 => \column__0\(4),
      I2 => \column__0\(9),
      I3 => \column__0\(5),
      I4 => \column__0\(6),
      I5 => \column__0\(7),
      O => \dc_bias[3]_i_24__0_n_0\
    );
\dc_bias[3]_i_25\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \column__0\(6),
      I2 => \column__0\(8),
      I3 => \column__0\(4),
      I4 => \column__0\(5),
      O => \dc_bias[3]_i_25_n_0\
    );
\dc_bias[3]_i_25__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \column__0\(2),
      O => \dc_bias[3]_i_25__0_n_0\
    );
\dc_bias[3]_i_26__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F5F5F4F5"
    )
        port map (
      I0 => \dc_bias[3]_i_50_n_0\,
      I1 => \column__0\(8),
      I2 => \column__0\(7),
      I3 => \column__0\(4),
      I4 => \dc_bias[3]_i_51_n_0\,
      I5 => \sdp_bl.ramb18_dp_bl.ram18_bl_i_31_n_0\,
      O => \dc_bias[3]_i_26__0_n_0\
    );
\dc_bias[3]_i_27\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFD"
    )
        port map (
      I0 => \column__0\(8),
      I1 => \column__0\(7),
      I2 => \column__0\(6),
      I3 => \column__0\(4),
      I4 => \column__0\(5),
      O => \dc_bias[3]_i_27_n_0\
    );
\dc_bias[3]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001000000"
    )
        port map (
      I0 => \processQ_reg[8]_0\,
      I1 => \dc_bias[3]_i_3__0_n_0\,
      I2 => \processQ_reg[9]_1\,
      I3 => \dc_bias[3]_i_5__1_n_0\,
      I4 => \dc_bias[3]_i_6_n_0\,
      I5 => \dc_bias[3]_i_7_n_0\,
      O => \^encoded_reg[8]_3\
    );
\dc_bias[3]_i_30\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004FFFF00040004"
    )
        port map (
      I0 => \column__0\(4),
      I1 => \column__0\(5),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I4 => \column__0\(9),
      I5 => \dc_bias[3]_i_24_n_0\,
      O => \encoded_reg[8]_24\
    );
\dc_bias[3]_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800140000"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \column__0\(4),
      I2 => \column__0\(8),
      I3 => \dc_bias[3]_i_56_n_0\,
      I4 => \column__0\(6),
      I5 => \column__0\(9),
      O => \dc_bias[3]_i_33_n_0\
    );
\dc_bias[3]_i_34\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0412"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \column__0\(4),
      I2 => \column__0\(9),
      I3 => \column__0\(8),
      O => \dc_bias[3]_i_34_n_0\
    );
\dc_bias[3]_i_35\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \column__0\(2),
      I1 => \column__0\(3),
      I2 => \column__0\(7),
      I3 => \column__0\(6),
      O => \dc_bias[3]_i_35_n_0\
    );
\dc_bias[3]_i_36\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \column__0\(8),
      I1 => \column__0\(4),
      O => \dc_bias[3]_i_36_n_0\
    );
\dc_bias[3]_i_37\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4200"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \column__0\(2),
      I2 => \column__0\(6),
      I3 => \column__0\(7),
      O => \dc_bias[3]_i_37_n_0\
    );
\dc_bias[3]_i_38\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000018"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \column__0\(9),
      I2 => \column__0\(6),
      I3 => \dc_bias[3]_i_24_n_0\,
      I4 => \column__0\(4),
      I5 => \sdp_bl.ramb18_dp_bl.ram18_bl_i_31_n_0\,
      O => \dc_bias[3]_i_38_n_0\
    );
\dc_bias[3]_i_39\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A00A0003A00A000"
    )
        port map (
      I0 => \dc_bias[3]_i_37_n_0\,
      I1 => \dc_bias[3]_i_35_n_0\,
      I2 => \column__0\(4),
      I3 => \column__0\(5),
      I4 => \column__0\(8),
      I5 => \column__0\(9),
      O => \dc_bias[3]_i_39_n_0\
    );
\dc_bias[3]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF55554000"
    )
        port map (
      I0 => \dc_bias[3]_i_16__1_n_0\,
      I1 => \processQ_reg[9]_3\(0),
      I2 => \processQ_reg[9]_4\(0),
      I3 => \dc_bias[3]_i_8__1_n_0\,
      I4 => \processQ_reg[0]_1\,
      I5 => \dc_bias[3]_i_9__0_n_0\,
      O => \dc_bias[3]_i_3__0_n_0\
    );
\dc_bias[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000000040004"
    )
        port map (
      I0 => \processQ_reg[9]_0\,
      I1 => \processQ_reg[8]_0\,
      I2 => \dc_bias[3]_i_9__0_n_0\,
      I3 => \^encoded_reg[0]_10\,
      I4 => \processQ_reg[4]_1\,
      I5 => \^encoded_reg[0]_11\,
      O => \^encoded_reg[0]_8\
    );
\dc_bias[3]_i_40\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBD7"
    )
        port map (
      I0 => \column__0\(4),
      I1 => \column__0\(5),
      I2 => \column__0\(9),
      I3 => \column__0\(8),
      O => \dc_bias[3]_i_40_n_0\
    );
\dc_bias[3]_i_41\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F7FF"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \column__0\(2),
      I2 => \column__0\(7),
      I3 => \column__0\(6),
      O => \dc_bias[3]_i_41_n_0\
    );
\dc_bias[3]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A000B0000A300B0"
    )
        port map (
      I0 => \dc_bias[3]_i_37_n_0\,
      I1 => \dc_bias[3]_i_41_n_0\,
      I2 => \column__0\(5),
      I3 => \column__0\(4),
      I4 => \column__0\(9),
      I5 => \column__0\(8),
      O => \dc_bias[3]_i_42_n_0\
    );
\dc_bias[3]_i_45\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFDFFB"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \column__0\(6),
      I2 => \column__0\(3),
      I3 => \column__0\(4),
      I4 => \dc_bias[3]_i_57_n_0\,
      I5 => \dc_bias[3]_i_58_n_0\,
      O => \encoded_reg[8]_20\
    );
\dc_bias[3]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFABAAFFFFFFFF"
    )
        port map (
      I0 => \^encoded_reg[8]_21\,
      I1 => \column__0\(8),
      I2 => \column__0\(7),
      I3 => \^encoded_reg[8]_22\,
      I4 => \processQ_reg[8]_2\,
      I5 => \processQ_reg[8]_3\,
      O => \^encoded_reg[8]_18\
    );
\dc_bias[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \encoded[8]_i_4__0_n_0\,
      I1 => \^encoded_reg[8]_3\,
      O => \encoded_reg[8]_19\
    );
\dc_bias[3]_i_50\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => \column__0\(6),
      I1 => \column__0\(8),
      I2 => \column__0\(4),
      I3 => \column__0\(5),
      O => \dc_bias[3]_i_50_n_0\
    );
\dc_bias[3]_i_51\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \column__0\(9),
      O => \dc_bias[3]_i_51_n_0\
    );
\dc_bias[3]_i_55\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \column__0\(9),
      O => \encoded_reg[8]_25\
    );
\dc_bias[3]_i_56\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \column__0\(2),
      I2 => \column__0\(3),
      O => \dc_bias[3]_i_56_n_0\
    );
\dc_bias[3]_i_57\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
        port map (
      I0 => \column__0\(8),
      I1 => \column__0\(9),
      I2 => \column__0\(7),
      I3 => \processQ_reg[9]_6\(2),
      I4 => \processQ_reg[9]_6\(0),
      O => \dc_bias[3]_i_57_n_0\
    );
\dc_bias[3]_i_58\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"76EE"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \column__0\(2),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      O => \dc_bias[3]_i_58_n_0\
    );
\dc_bias[3]_i_5__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \^encoded_reg[8]_18\,
      I1 => switch(1),
      I2 => \processQ_reg[9]_5\(0),
      O => \dc_bias[3]_i_5__1_n_0\
    );
\dc_bias[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF55555554"
    )
        port map (
      I0 => \processQ_reg[2]_0\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \dc_bias[3]_i_11__0_n_0\,
      I4 => \dc_bias[3]_i_12_n_0\,
      I5 => \processQ_reg[8]_1\,
      O => \dc_bias[3]_i_6_n_0\
    );
\dc_bias[3]_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFE0"
    )
        port map (
      I0 => \column__0\(8),
      I1 => \column__0\(7),
      I2 => \column__0\(9),
      I3 => \processQ_reg[9]_6\(2),
      O => \encoded_reg[9]\
    );
\dc_bias[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF55555510"
    )
        port map (
      I0 => \processQ_reg[9]_2\,
      I1 => \processQ[6]_i_2__0_n_0\,
      I2 => \dc_bias[3]_i_15__0_n_0\,
      I3 => \dc_bias[3]_i_16__0_n_0\,
      I4 => \dc_bias[3]_i_17__0_n_0\,
      I5 => \processQ_reg[2]_1\,
      O => \dc_bias[3]_i_7_n_0\
    );
\dc_bias[3]_i_8__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      O => \dc_bias[3]_i_8__1_n_0\
    );
\dc_bias[3]_i_9__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => \processQ_reg[9]_9\(0),
      I1 => \processQ_reg[9]_10\(0),
      I2 => \dc_bias[3]_i_14__1_n_0\,
      I3 => \column__0\(5),
      I4 => \column__0\(7),
      I5 => \column__0\(6),
      O => \dc_bias[3]_i_9__0_n_0\
    );
\dc_bias[3]_i_9__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"557FFF7F"
    )
        port map (
      I0 => \processQ_reg[9]_6\(0),
      I1 => \processQ_reg[9]_8\(0),
      I2 => \processQ_reg[9]_7\(0),
      I3 => \processQ_reg[9]_6\(1),
      I4 => \slv_reg10_reg[9]_1\(0),
      O => \encoded_reg[8]_26\
    );
\encoded[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"660F"
    )
        port map (
      I0 => \dc_bias_reg[3]_0\(0),
      I1 => \^encoded_reg[0]_8\,
      I2 => \^encoded_reg[8]_0\,
      I3 => \processQ_reg[5]_0\,
      O => \encoded_reg[0]_9\
    );
\encoded[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A3"
    )
        port map (
      I0 => \dc_bias_reg[3]_0\(0),
      I1 => \^encoded_reg[8]_0\,
      I2 => \processQ_reg[5]_0\,
      O => \encoded_reg[1]\
    );
\encoded[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => \^encoded_reg[8]_0\,
      I1 => \dc_bias_reg[3]_0\(0),
      I2 => \processQ_reg[5]_0\,
      O => \encoded_reg[2]\
    );
\encoded[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AEEA"
    )
        port map (
      I0 => \^encoded_reg[8]_0\,
      I1 => \processQ_reg[5]_0\,
      I2 => \dc_bias_reg[3]_0\(0),
      I3 => \^encoded_reg[0]_8\,
      O => \encoded_reg[4]\
    );
\encoded[8]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAE"
    )
        port map (
      I0 => \^encoded_reg[8]_0\,
      I1 => \processQ_reg[5]_0\,
      I2 => \dc_bias_reg[1]\,
      I3 => \^encoded_reg[8]_1\,
      O => \encoded_reg[8]\
    );
\encoded[8]_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3B3F"
    )
        port map (
      I0 => \^encoded_reg[8]_3\,
      I1 => \processQ_reg[5]_0\,
      I2 => \dc_bias_reg[2]\,
      I3 => \encoded[8]_i_4__0_n_0\,
      O => \encoded_reg[8]_2\
    );
\encoded[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000007E000000"
    )
        port map (
      I0 => \column__0\(6),
      I1 => \column__0\(4),
      I2 => \column__0\(5),
      I3 => \column__0\(7),
      I4 => \column__0\(9),
      I5 => \column__0\(8),
      O => \^encoded_reg[8]_0\
    );
\encoded[8]_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \processQ_reg[9]_1\,
      I1 => \dc_bias[3]_i_3__0_n_0\,
      I2 => \^encoded_reg[8]_18\,
      I3 => \processQ_reg[9]_0\,
      O => \encoded[8]_i_4__0_n_0\
    );
\encoded[9]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001000101"
    )
        port map (
      I0 => \processQ_reg[8]_0\,
      I1 => \dc_bias[3]_i_9__0_n_0\,
      I2 => \^encoded_reg[0]_10\,
      I3 => \processQ_reg[4]_1\,
      I4 => \^encoded_reg[0]_11\,
      I5 => \dc_bias[3]_i_5__1_n_0\,
      O => \encoded[9]_i_2__0_n_0\
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15403D54"
    )
        port map (
      I0 => \column__0\(9),
      I1 => \slv_reg10_reg[8]\,
      I2 => \slv_reg10_reg[7]_0\,
      I3 => \slv_reg10_reg[9]_0\,
      I4 => \column__0\(8),
      O => \encoded_reg[8]_6\(0)
    );
\i__carry__0_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C222ABBB80002AAA"
    )
        port map (
      I0 => \column__0\(9),
      I1 => \slv_reg10_reg[8]\,
      I2 => \slv_reg10_reg[6]_0\,
      I3 => \slv_reg10_reg[7]\,
      I4 => \slv_reg10_reg[9]_0\,
      I5 => \column__0\(8),
      O => \encoded_reg[8]_10\(0)
    );
\i__carry__0_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"155540003DDD5444"
    )
        port map (
      I0 => \column__0\(9),
      I1 => \slv_reg10_reg[8]\,
      I2 => \slv_reg10_reg[6]_2\,
      I3 => \slv_reg10_reg[7]\,
      I4 => \slv_reg10_reg[9]_0\,
      I5 => \column__0\(8),
      O => \encoded_reg[8]_14\(0)
    );
\i__carry__0_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C2AB802A"
    )
        port map (
      I0 => \column__0\(9),
      I1 => \slv_reg10_reg[8]\,
      I2 => \int_trigger_time_s_reg[7]\,
      I3 => \slv_reg10_reg[9]_0\,
      I4 => \column__0\(8),
      O => \encoded_reg[8]_17\(0)
    );
\i__carry__0_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"155540003DDD5444"
    )
        port map (
      I0 => \column__0\(9),
      I1 => \slv_reg10_reg[8]\,
      I2 => \slv_reg10_reg[6]_3\,
      I3 => \slv_reg10_reg[7]\,
      I4 => \slv_reg10_reg[9]_0\,
      I5 => \column__0\(8),
      O => \encoded_reg[0]_3\(0)
    );
\i__carry__0_i_1__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C222ABBB80002AAA"
    )
        port map (
      I0 => \column__0\(9),
      I1 => \slv_reg10_reg[8]\,
      I2 => \slv_reg10_reg[6]_4\,
      I3 => \slv_reg10_reg[7]\,
      I4 => \slv_reg10_reg[9]_0\,
      I5 => \column__0\(8),
      O => \encoded_reg[0]_7\(0)
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\,
      I1 => \column__0\(9),
      I2 => \slv_reg10_reg[8]\,
      I3 => \slv_reg10_reg[7]_0\,
      I4 => \column__0\(8),
      O => \encoded_reg[8]_5\(0)
    );
\i__carry__0_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0990909060090909"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\,
      I1 => \column__0\(9),
      I2 => \slv_reg10_reg[8]\,
      I3 => \slv_reg10_reg[6]_0\,
      I4 => \slv_reg10_reg[7]\,
      I5 => \column__0\(8),
      O => \encoded_reg[8]_9\(0)
    );
\i__carry__0_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0990909060090909"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\,
      I1 => \column__0\(9),
      I2 => \slv_reg10_reg[8]\,
      I3 => \slv_reg10_reg[6]_2\,
      I4 => \slv_reg10_reg[7]\,
      I5 => \column__0\(8),
      O => \encoded_reg[8]_13\(0)
    );
\i__carry__0_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\,
      I1 => \column__0\(9),
      I2 => \slv_reg10_reg[8]\,
      I3 => \int_trigger_time_s_reg[7]\,
      I4 => \column__0\(8),
      O => \encoded_reg[8]_16\(0)
    );
\i__carry__0_i_2__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0990909060090909"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\,
      I1 => \column__0\(9),
      I2 => \slv_reg10_reg[8]\,
      I3 => \slv_reg10_reg[6]_3\,
      I4 => \slv_reg10_reg[7]\,
      I5 => \column__0\(8),
      O => \encoded_reg[0]_2\(0)
    );
\i__carry__0_i_2__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0990909060090909"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\,
      I1 => \column__0\(9),
      I2 => \slv_reg10_reg[8]\,
      I3 => \slv_reg10_reg[6]_4\,
      I4 => \slv_reg10_reg[7]\,
      I5 => \column__0\(8),
      O => \encoded_reg[0]_6\(0)
    );
\i__carry_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15403D54"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \slv_reg10_reg[6]\,
      I2 => \int_trigger_time_s_reg[5]\,
      I3 => \slv_reg10_reg[7]\,
      I4 => \column__0\(6),
      O => \encoded_reg[8]_4\(3)
    );
\i__carry_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C2AB802A"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \slv_reg10_reg[6]\,
      I2 => \slv_reg10_reg[5]\,
      I3 => \slv_reg10_reg[7]\,
      I4 => \column__0\(6),
      O => \encoded_reg[8]_8\(3)
    );
\i__carry_i_1__11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7877788887888777"
    )
        port map (
      I0 => \slv_reg10_reg[8]\,
      I1 => \slv_reg10_reg[7]_1\,
      I2 => \slv_reg10_reg[9]\(3),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => Q(3),
      I5 => \column__0\(9),
      O => \encoded_reg[0]\(3)
    );
\i__carry_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15403D54"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \slv_reg10_reg[6]\,
      I2 => \slv_reg10_reg[5]_1\,
      I3 => \slv_reg10_reg[7]\,
      I4 => \column__0\(6),
      O => \encoded_reg[8]_12\(3)
    );
\i__carry_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C2AB802A"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \slv_reg10_reg[6]\,
      I2 => \slv_reg10_reg[5]_2\,
      I3 => \slv_reg10_reg[7]\,
      I4 => \column__0\(6),
      O => DI(3)
    );
\i__carry_i_1__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"51550400D3DD4544"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \slv_reg10_reg[6]\,
      I2 => \int_trigger_time_s_reg[1]\,
      I3 => \slv_reg10_reg[5]_0\,
      I4 => \slv_reg10_reg[7]\,
      I5 => \column__0\(6),
      O => \encoded_reg[0]_1\(3)
    );
\i__carry_i_1__5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2C22BABB0800A2AA"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \slv_reg10_reg[6]\,
      I2 => \int_trigger_time_s_reg[1]_0\,
      I3 => \slv_reg10_reg[5]_0\,
      I4 => \slv_reg10_reg[7]\,
      I5 => \column__0\(6),
      O => \encoded_reg[0]_5\(3)
    );
\i__carry_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000006909009"
    )
        port map (
      I0 => \slv_reg10_reg[8]\,
      I1 => \column__0\(8),
      I2 => \column__0\(7),
      I3 => \slv_reg10_reg[6]_1\,
      I4 => \slv_reg10_reg[7]\,
      I5 => \i__carry_i_7_n_0\,
      O => \encoded_reg[0]\(2)
    );
\i__carry_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"015443D5"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \slv_reg10_reg[4]\,
      I2 => \slv_reg10_reg[0]\,
      I3 => \slv_reg10_reg[5]_0\,
      I4 => \column__0\(4),
      O => \encoded_reg[8]_4\(2)
    );
\i__carry_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BCCC2AAAA8880222"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \slv_reg10_reg[4]\,
      I2 => \int_trigger_time_s_reg[1]_1\,
      I3 => \int_trigger_time_s_reg[2]\,
      I4 => \slv_reg10_reg[5]_0\,
      I5 => \column__0\(4),
      O => \encoded_reg[8]_8\(2)
    );
\i__carry_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"015443D5"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \slv_reg10_reg[4]\,
      I2 => \int_trigger_time_s_reg[3]\,
      I3 => \slv_reg10_reg[5]_0\,
      I4 => \column__0\(4),
      O => \encoded_reg[8]_12\(2)
    );
\i__carry_i_2__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CBCCA2AA8A882022"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \slv_reg10_reg[4]\,
      I2 => \int_trigger_time_s_reg[1]_2\,
      I3 => \int_trigger_time_s_reg[2]\,
      I4 => \slv_reg10_reg[5]_0\,
      I5 => \column__0\(4),
      O => DI(2)
    );
\i__carry_i_2__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011454434335D55"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \slv_reg10_reg[4]\,
      I2 => \int_trigger_time_s_reg[1]_3\,
      I3 => \slv_reg10_reg[3]\,
      I4 => \slv_reg10_reg[5]_0\,
      I5 => \column__0\(4),
      O => \encoded_reg[0]_1\(2)
    );
\i__carry_i_2__5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BC2AA802"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \slv_reg10_reg[4]\,
      I2 => \slv_reg10_reg[2]_0\,
      I3 => \slv_reg10_reg[5]_0\,
      I4 => \column__0\(4),
      O => \encoded_reg[0]_5\(2)
    );
\i__carry_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000060060690"
    )
        port map (
      I0 => \slv_reg10_reg[5]_0\,
      I1 => \column__0\(5),
      I2 => \column__0\(4),
      I3 => \int_trigger_time_s_reg[2]\,
      I4 => \slv_reg10_reg[4]\,
      I5 => \i__carry_i_8_n_0\,
      O => \encoded_reg[0]\(1)
    );
\i__carry_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"111111147771111D"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \slv_reg10_reg[3]\,
      I2 => \slv_reg10_reg[0]_0\,
      I3 => \slv_reg10_reg[1]\,
      I4 => \slv_reg10_reg[2]\,
      I5 => \column__0\(2),
      O => \encoded_reg[8]_4\(1)
    );
\i__carry_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C222ABBB80002AAA"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \slv_reg10_reg[2]\,
      I2 => \slv_reg10_reg[0]_0\,
      I3 => \slv_reg10_reg[1]\,
      I4 => \slv_reg10_reg[3]\,
      I5 => \column__0\(2),
      O => \encoded_reg[8]_8\(1)
    );
\i__carry_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"011154444333D555"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \slv_reg10_reg[2]\,
      I2 => \slv_reg10_reg[0]_0\,
      I3 => \slv_reg10_reg[1]\,
      I4 => \slv_reg10_reg[3]\,
      I5 => \column__0\(2),
      O => \encoded_reg[8]_12\(1)
    );
\i__carry_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC2AAAB8880222A"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \slv_reg10_reg[2]\,
      I2 => \slv_reg10_reg[0]_0\,
      I3 => \slv_reg10_reg[1]\,
      I4 => \slv_reg10_reg[3]\,
      I5 => \column__0\(2),
      O => DI(1)
    );
\i__carry_i_3__4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"015443D5"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \slv_reg10_reg[1]\,
      I2 => \slv_reg10_reg[2]\,
      I3 => \slv_reg10_reg[3]\,
      I4 => \column__0\(2),
      O => \encoded_reg[0]_1\(1)
    );
\i__carry_i_3__5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C2AB802A"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \slv_reg10_reg[1]\,
      I2 => \slv_reg10_reg[2]\,
      I3 => \slv_reg10_reg[3]\,
      I4 => \column__0\(2),
      O => \encoded_reg[0]_5\(1)
    );
\i__carry_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => \slv_reg10_reg[2]\,
      I1 => \column__0\(2),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I3 => \slv_reg10_reg[0]_0\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I5 => \slv_reg10_reg[1]\,
      O => \encoded_reg[0]\(0)
    );
\i__carry_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5017505050171717"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \slv_reg10_reg[1]\,
      I3 => \slv_reg10_reg[9]\(0),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => Q(0),
      O => \encoded_reg[8]_4\(0)
    );
\i__carry_i_4__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF1D001D000000"
    )
        port map (
      I0 => Q(0),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \slv_reg10_reg[9]\(0),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I4 => \slv_reg10_reg[1]\,
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      O => \encoded_reg[0]_5\(0)
    );
\i__carry_i_4__10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFE200E21DE20000"
    )
        port map (
      I0 => Q(0),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \slv_reg10_reg[9]\(0),
      I3 => \slv_reg10_reg[1]\,
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      O => DI(0)
    );
\i__carry_i_4__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000056A602A257F7"
    )
        port map (
      I0 => \slv_reg10_reg[1]\,
      I1 => Q(0),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => \slv_reg10_reg[9]\(0),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      O => \encoded_reg[8]_12\(0)
    );
\i__carry_i_4__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E0EEE0008A888AAA"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \slv_reg10_reg[9]\(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => Q(0),
      I5 => \slv_reg10_reg[1]\,
      O => \encoded_reg[8]_8\(0)
    );
\i__carry_i_4__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000E200E2FFFF"
    )
        port map (
      I0 => Q(0),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \slv_reg10_reg[9]\(0),
      I3 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I5 => \slv_reg10_reg[1]\,
      O => \encoded_reg[0]_1\(0)
    );
\i__carry_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg10_reg[7]\,
      I1 => \column__0\(7),
      I2 => \slv_reg10_reg[6]\,
      I3 => \int_trigger_time_s_reg[5]\,
      I4 => \column__0\(6),
      O => S(3)
    );
\i__carry_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg10_reg[7]\,
      I1 => \column__0\(7),
      I2 => \slv_reg10_reg[6]\,
      I3 => \slv_reg10_reg[5]\,
      I4 => \column__0\(6),
      O => \encoded_reg[8]_7\(3)
    );
\i__carry_i_5__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg10_reg[7]\,
      I1 => \column__0\(7),
      I2 => \slv_reg10_reg[6]\,
      I3 => \slv_reg10_reg[5]_1\,
      I4 => \column__0\(6),
      O => \encoded_reg[8]_11\(3)
    );
\i__carry_i_5__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg10_reg[7]\,
      I1 => \column__0\(7),
      I2 => \slv_reg10_reg[6]\,
      I3 => \slv_reg10_reg[5]_2\,
      I4 => \column__0\(6),
      O => \encoded_reg[8]_15\(3)
    );
\i__carry_i_5__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009909009600909"
    )
        port map (
      I0 => \slv_reg10_reg[7]\,
      I1 => \column__0\(7),
      I2 => \slv_reg10_reg[6]\,
      I3 => \int_trigger_time_s_reg[1]\,
      I4 => \slv_reg10_reg[5]_0\,
      I5 => \column__0\(6),
      O => \encoded_reg[0]_0\(3)
    );
\i__carry_i_5__5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009909009600909"
    )
        port map (
      I0 => \slv_reg10_reg[7]\,
      I1 => \column__0\(7),
      I2 => \slv_reg10_reg[6]\,
      I3 => \int_trigger_time_s_reg[1]_0\,
      I4 => \slv_reg10_reg[5]_0\,
      I5 => \column__0\(6),
      O => \encoded_reg[0]_4\(3)
    );
\i__carry_i_6__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60090660"
    )
        port map (
      I0 => \slv_reg10_reg[5]_0\,
      I1 => \column__0\(5),
      I2 => \slv_reg10_reg[4]\,
      I3 => \slv_reg10_reg[0]\,
      I4 => \column__0\(4),
      O => S(2)
    );
\i__carry_i_6__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6009090906606060"
    )
        port map (
      I0 => \slv_reg10_reg[5]_0\,
      I1 => \column__0\(5),
      I2 => \slv_reg10_reg[4]\,
      I3 => \int_trigger_time_s_reg[1]_1\,
      I4 => \int_trigger_time_s_reg[2]\,
      I5 => \column__0\(4),
      O => \encoded_reg[8]_7\(2)
    );
\i__carry_i_6__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60090660"
    )
        port map (
      I0 => \slv_reg10_reg[5]_0\,
      I1 => \column__0\(5),
      I2 => \slv_reg10_reg[4]\,
      I3 => \int_trigger_time_s_reg[3]\,
      I4 => \column__0\(4),
      O => \encoded_reg[8]_11\(2)
    );
\i__carry_i_6__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0960090960066060"
    )
        port map (
      I0 => \slv_reg10_reg[5]_0\,
      I1 => \column__0\(5),
      I2 => \slv_reg10_reg[4]\,
      I3 => \int_trigger_time_s_reg[1]_2\,
      I4 => \int_trigger_time_s_reg[2]\,
      I5 => \column__0\(4),
      O => \encoded_reg[8]_15\(2)
    );
\i__carry_i_6__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0960090960066060"
    )
        port map (
      I0 => \slv_reg10_reg[5]_0\,
      I1 => \column__0\(5),
      I2 => \slv_reg10_reg[4]\,
      I3 => \int_trigger_time_s_reg[1]_3\,
      I4 => \slv_reg10_reg[3]\,
      I5 => \column__0\(4),
      O => \encoded_reg[0]_0\(2)
    );
\i__carry_i_6__5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60090660"
    )
        port map (
      I0 => \slv_reg10_reg[5]_0\,
      I1 => \column__0\(5),
      I2 => \slv_reg10_reg[4]\,
      I3 => \slv_reg10_reg[2]_0\,
      I4 => \column__0\(4),
      O => \encoded_reg[0]_4\(2)
    );
\i__carry_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999955566666AAA"
    )
        port map (
      I0 => \column__0\(6),
      I1 => \slv_reg10_reg[5]_0\,
      I2 => \slv_reg10_reg[2]\,
      I3 => \slv_reg10_reg[3]\,
      I4 => \slv_reg10_reg[4]\,
      I5 => \slv_reg10_reg[6]\,
      O => \i__carry_i_7_n_0\
    );
\i__carry_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002999499940002"
    )
        port map (
      I0 => \column__0\(2),
      I1 => \slv_reg10_reg[2]\,
      I2 => \slv_reg10_reg[1]\,
      I3 => \slv_reg10_reg[0]_0\,
      I4 => \slv_reg10_reg[3]\,
      I5 => \column__0\(3),
      O => S(1)
    );
\i__carry_i_7__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1888844442222111"
    )
        port map (
      I0 => \column__0\(2),
      I1 => \slv_reg10_reg[3]\,
      I2 => \slv_reg10_reg[1]\,
      I3 => \slv_reg10_reg[0]_0\,
      I4 => \slv_reg10_reg[2]\,
      I5 => \column__0\(3),
      O => \encoded_reg[8]_7\(1)
    );
\i__carry_i_7__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6009090906606060"
    )
        port map (
      I0 => \slv_reg10_reg[3]\,
      I1 => \column__0\(3),
      I2 => \slv_reg10_reg[2]\,
      I3 => \slv_reg10_reg[0]_0\,
      I4 => \slv_reg10_reg[1]\,
      I5 => \column__0\(2),
      O => \encoded_reg[8]_11\(1)
    );
\i__carry_i_7__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0009999066600009"
    )
        port map (
      I0 => \slv_reg10_reg[3]\,
      I1 => \column__0\(3),
      I2 => \slv_reg10_reg[0]_0\,
      I3 => \slv_reg10_reg[1]\,
      I4 => \slv_reg10_reg[2]\,
      I5 => \column__0\(2),
      O => \encoded_reg[8]_15\(1)
    );
\i__carry_i_7__4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"21188442"
    )
        port map (
      I0 => \column__0\(2),
      I1 => \slv_reg10_reg[3]\,
      I2 => \slv_reg10_reg[2]\,
      I3 => \slv_reg10_reg[1]\,
      I4 => \column__0\(3),
      O => \encoded_reg[0]_0\(1)
    );
\i__carry_i_7__5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg10_reg[3]\,
      I1 => \column__0\(3),
      I2 => \slv_reg10_reg[1]\,
      I3 => \slv_reg10_reg[2]\,
      I4 => \column__0\(2),
      O => \encoded_reg[0]_4\(1)
    );
\i__carry_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A956A65959A656A"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \slv_reg10_reg[9]\(2),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => Q(2),
      I4 => \slv_reg10_reg[9]\(1),
      I5 => Q(1),
      O => \i__carry_i_8_n_0\
    );
\i__carry_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1482141414828282"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \slv_reg10_reg[1]\,
      I3 => \slv_reg10_reg[9]\(0),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => Q(0),
      O => \encoded_reg[8]_7\(0)
    );
\i__carry_i_8__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4128414141282828"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \slv_reg10_reg[1]\,
      I3 => \slv_reg10_reg[9]\(0),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => Q(0),
      O => S(0)
    );
\i__carry_i_8__10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1482141414828282"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \slv_reg10_reg[1]\,
      I3 => \slv_reg10_reg[9]\(0),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => Q(0),
      O => \encoded_reg[8]_11\(0)
    );
\i__carry_i_8__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6066600006000666"
    )
        port map (
      I0 => \slv_reg10_reg[1]\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \slv_reg10_reg[9]\(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => Q(0),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      O => \encoded_reg[0]_0\(0)
    );
\i__carry_i_8__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6066600006000666"
    )
        port map (
      I0 => \slv_reg10_reg[1]\,
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \slv_reg10_reg[9]\(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => Q(0),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      O => \encoded_reg[0]_4\(0)
    );
\i__carry_i_8__9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4128414141282828"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \slv_reg10_reg[1]\,
      I3 => \slv_reg10_reg[9]\(0),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => Q(0),
      O => \encoded_reg[8]_15\(0)
    );
\processQ[0]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      O => \processQ[0]_i_1__1_n_0\
    );
\processQ[1]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      O => \plusOp__1\(1)
    );
\processQ[2]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \column__0\(2),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      O => \processQ[2]_i_1__1_n_0\
    );
\processQ[3]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \column__0\(2),
      O => \plusOp__1\(3)
    );
\processQ[4]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \column__0\(4),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I2 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I3 => \column__0\(3),
      I4 => \column__0\(2),
      O => \plusOp__1\(4)
    );
\processQ[5]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \column__0\(4),
      I2 => \column__0\(2),
      I3 => \column__0\(3),
      I4 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I5 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      O => \plusOp__1\(5)
    );
\processQ[6]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA6AAAAAAA"
    )
        port map (
      I0 => \column__0\(6),
      I1 => \column__0\(5),
      I2 => \column__0\(4),
      I3 => \column__0\(2),
      I4 => \column__0\(3),
      I5 => \processQ[6]_i_2__0_n_0\,
      O => \plusOp__1\(6)
    );
\processQ[6]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      O => \processQ[6]_i_2__0_n_0\
    );
\processQ[7]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \column__0\(6),
      I2 => \processQ[9]_i_6__0_n_0\,
      O => \plusOp__1\(7)
    );
\processQ[8]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \column__0\(8),
      I1 => \column__0\(6),
      I2 => \column__0\(7),
      I3 => \processQ[9]_i_6__0_n_0\,
      O => \plusOp__1\(8)
    );
\processQ[9]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^processq_reg[0]_0\,
      I1 => reset_n,
      O => \processQ[9]_i_1__1_n_0\
    );
\processQ[9]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"777777777777777F"
    )
        port map (
      I0 => \column__0\(9),
      I1 => \column__0\(8),
      I2 => \column__0\(5),
      I3 => \column__0\(7),
      I4 => \column__0\(6),
      I5 => \processQ[9]_i_5__0_n_0\,
      O => processQ0
    );
\processQ[9]_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \column__0\(9),
      I1 => \column__0\(8),
      I2 => \processQ[9]_i_6__0_n_0\,
      I3 => \column__0\(7),
      I4 => \column__0\(6),
      O => \plusOp__1\(9)
    );
\processQ[9]_i_4__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000080"
    )
        port map (
      I0 => \processQ[9]_i_5__0_n_0\,
      I1 => \column__0\(8),
      I2 => \column__0\(9),
      I3 => \column__0\(5),
      I4 => \column__0\(7),
      I5 => \column__0\(6),
      O => \^processq_reg[0]_0\
    );
\processQ[9]_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \column__0\(3),
      I3 => \column__0\(2),
      I4 => \column__0\(4),
      O => \processQ[9]_i_5__0_n_0\
    );
\processQ[9]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      I1 => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      I2 => \column__0\(3),
      I3 => \column__0\(2),
      I4 => \column__0\(4),
      I5 => \column__0\(5),
      O => \processQ[9]_i_6__0_n_0\
    );
\processQ_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \processQ[0]_i_1__1_n_0\,
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(0),
      R => \processQ[9]_i_1__1_n_0\
    );
\processQ_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__1\(1),
      Q => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1),
      R => \processQ[9]_i_1__1_n_0\
    );
\processQ_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \processQ[2]_i_1__1_n_0\,
      Q => \column__0\(2),
      R => \processQ[9]_i_1__1_n_0\
    );
\processQ_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__1\(3),
      Q => \column__0\(3),
      R => \processQ[9]_i_1__1_n_0\
    );
\processQ_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__1\(4),
      Q => \column__0\(4),
      R => \processQ[9]_i_1__1_n_0\
    );
\processQ_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__1\(5),
      Q => \column__0\(5),
      R => \processQ[9]_i_1__1_n_0\
    );
\processQ_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__1\(6),
      Q => \column__0\(6),
      R => \processQ[9]_i_1__1_n_0\
    );
\processQ_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__1\(7),
      Q => \column__0\(7),
      R => \processQ[9]_i_1__1_n_0\
    );
\processQ_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__1\(8),
      Q => \column__0\(8),
      R => \processQ[9]_i_1__1_n_0\
    );
\processQ_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__1\(9),
      Q => \column__0\(9),
      R => \processQ[9]_i_1__1_n_0\
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAA9A9A999"
    )
        port map (
      I0 => \column__0\(9),
      I1 => \column__0\(8),
      I2 => \column__0\(4),
      I3 => \column__0\(3),
      I4 => \column__0\(2),
      I5 => \sdp_bl.ramb18_dp_bl.ram18_bl_i_30_n_0\,
      O => ADDRARDADDR(7)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_30\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \column__0\(7),
      I2 => \column__0\(6),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl_i_30_n_0\
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_31\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \column__0\(2),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl_i_31_n_0\
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA9AAA9AAA9"
    )
        port map (
      I0 => \column__0\(8),
      I1 => \column__0\(5),
      I2 => \column__0\(7),
      I3 => \column__0\(6),
      I4 => \sdp_bl.ramb18_dp_bl.ram18_bl_i_31_n_0\,
      I5 => \column__0\(4),
      O => ADDRARDADDR(6)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA9A9A9A9A9"
    )
        port map (
      I0 => \column__0\(7),
      I1 => \column__0\(5),
      I2 => \column__0\(6),
      I3 => \column__0\(2),
      I4 => \column__0\(3),
      I5 => \column__0\(4),
      O => ADDRARDADDR(5)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAA955"
    )
        port map (
      I0 => \column__0\(6),
      I1 => \column__0\(2),
      I2 => \column__0\(3),
      I3 => \column__0\(4),
      I4 => \column__0\(5),
      O => ADDRARDADDR(4)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => \column__0\(5),
      I1 => \column__0\(4),
      I2 => \column__0\(3),
      I3 => \column__0\(2),
      O => ADDRARDADDR(3)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_7__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"56"
    )
        port map (
      I0 => \column__0\(4),
      I1 => \column__0\(2),
      I2 => \column__0\(3),
      O => ADDRARDADDR(2)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_8__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \column__0\(3),
      I1 => \column__0\(2),
      O => ADDRARDADDR(1)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \column__0\(2),
      O => ADDRARDADDR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl is
  port (
    ac_bclk : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    ac_lrclk : out STD_LOGIC;
    \ac_lrclk_count_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \R_unsigned_data_prev_reg[9]\ : out STD_LOGIC;
    \R_bus_in_s_reg[17]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    \R_unsigned_data_prev_reg[8]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \L_unsigned_data_prev_reg[9]\ : out STD_LOGIC;
    \L_bus_in_s_reg[17]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    \L_unsigned_data_prev_reg[8]\ : out STD_LOGIC;
    \L_unsigned_data_prev_reg[7]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \state_reg[0]\ : out STD_LOGIC;
    \axi_rdata_reg[15]\ : out STD_LOGIC_VECTOR ( 14 downto 0 );
    \axi_rdata_reg[2]\ : out STD_LOGIC;
    ac_lrclk_sig_prev_reg : out STD_LOGIC;
    ready_sig_reg : out STD_LOGIC;
    DIBDI : out STD_LOGIC_VECTOR ( 9 downto 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl\ : out STD_LOGIC_VECTOR ( 9 downto 0 );
    ac_dac_sdata : out STD_LOGIC;
    clk : in STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_n : in STD_LOGIC;
    state : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \processQ_reg[9]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    ready_sig_reg_0 : in STD_LOGIC;
    \axi_araddr_reg[4]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_0\ : in STD_LOGIC;
    \axi_araddr_reg[6]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_araddr_reg[4]_1\ : in STD_LOGIC;
    \slv_reg3_reg[0]\ : in STD_LOGIC;
    \axi_araddr_reg[3]_rep\ : in STD_LOGIC;
    \slv_reg5_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \axi_araddr_reg[2]_rep\ : in STD_LOGIC;
    \slv_reg4_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \axi_araddr_reg[4]_2\ : in STD_LOGIC;
    \axi_araddr_reg[4]_3\ : in STD_LOGIC;
    \axi_araddr_reg[4]_4\ : in STD_LOGIC;
    \slv_reg3_reg[1]\ : in STD_LOGIC;
    \slv_reg3_reg[2]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_5\ : in STD_LOGIC;
    \axi_araddr_reg[4]_6\ : in STD_LOGIC;
    \axi_araddr_reg[4]_7\ : in STD_LOGIC;
    \slv_reg3_reg[3]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_8\ : in STD_LOGIC;
    \axi_araddr_reg[4]_9\ : in STD_LOGIC;
    \axi_araddr_reg[4]_10\ : in STD_LOGIC;
    \slv_reg3_reg[4]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_11\ : in STD_LOGIC;
    \axi_araddr_reg[4]_12\ : in STD_LOGIC;
    \axi_araddr_reg[4]_13\ : in STD_LOGIC;
    \slv_reg3_reg[5]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_14\ : in STD_LOGIC;
    \axi_araddr_reg[4]_15\ : in STD_LOGIC;
    \axi_araddr_reg[4]_16\ : in STD_LOGIC;
    \slv_reg3_reg[6]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_17\ : in STD_LOGIC;
    \axi_araddr_reg[4]_18\ : in STD_LOGIC;
    \axi_araddr_reg[4]_19\ : in STD_LOGIC;
    \slv_reg3_reg[7]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_20\ : in STD_LOGIC;
    \axi_araddr_reg[4]_21\ : in STD_LOGIC;
    \axi_araddr_reg[4]_22\ : in STD_LOGIC;
    \slv_reg3_reg[8]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_23\ : in STD_LOGIC;
    \axi_araddr_reg[4]_24\ : in STD_LOGIC;
    \axi_araddr_reg[4]_25\ : in STD_LOGIC;
    \slv_reg3_reg[9]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_26\ : in STD_LOGIC;
    \axi_araddr_reg[4]_27\ : in STD_LOGIC;
    \axi_araddr_reg[4]_28\ : in STD_LOGIC;
    \slv_reg3_reg[10]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_29\ : in STD_LOGIC;
    \axi_araddr_reg[4]_30\ : in STD_LOGIC;
    \axi_araddr_reg[4]_31\ : in STD_LOGIC;
    \slv_reg3_reg[11]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_32\ : in STD_LOGIC;
    \axi_araddr_reg[4]_33\ : in STD_LOGIC;
    \axi_araddr_reg[4]_34\ : in STD_LOGIC;
    \slv_reg3_reg[12]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_35\ : in STD_LOGIC;
    \axi_araddr_reg[4]_36\ : in STD_LOGIC;
    \axi_araddr_reg[4]_37\ : in STD_LOGIC;
    \slv_reg3_reg[13]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_38\ : in STD_LOGIC;
    \axi_araddr_reg[4]_39\ : in STD_LOGIC;
    \axi_araddr_reg[4]_40\ : in STD_LOGIC;
    \slv_reg3_reg[14]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_41\ : in STD_LOGIC;
    \axi_araddr_reg[4]_42\ : in STD_LOGIC;
    \axi_araddr_reg[4]_43\ : in STD_LOGIC;
    \slv_reg3_reg[15]\ : in STD_LOGIC;
    ac_lrclk_sig_prev_reg_0 : in STD_LOGIC;
    \L_bus_in_s_reg[17]_0\ : in STD_LOGIC_VECTOR ( 17 downto 0 );
    \R_bus_in_s_reg[17]_0\ : in STD_LOGIC_VECTOR ( 17 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    switch : in STD_LOGIC_VECTOR ( 0 to 0 );
    \L_unsigned_data_prev_reg[9]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg11_reg[7]\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[9]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \slv_reg11_reg[9]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \slv_reg11_reg[6]\ : in STD_LOGIC;
    \slv_reg11_reg[5]\ : in STD_LOGIC;
    \slv_reg11_reg[4]\ : in STD_LOGIC;
    \slv_reg11_reg[3]\ : in STD_LOGIC;
    \slv_reg11_reg[1]\ : in STD_LOGIC;
    \slv_reg11_reg[8]\ : in STD_LOGIC;
    \slv_reg2_reg[15]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg1_reg[15]\ : in STD_LOGIC_VECTOR ( 9 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl is
  signal BCLK_Fall_int : STD_LOGIC;
  signal BCLK_int_i_2_n_0 : STD_LOGIC;
  signal Cnt_Bclk0 : STD_LOGIC;
  signal \Cnt_Bclk0_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \Cnt_Bclk[4]_i_1_n_0\ : STD_LOGIC;
  signal \Cnt_Bclk_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal Cnt_Lrclk : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \Cnt_Lrclk[0]_i_1_n_0\ : STD_LOGIC;
  signal \Cnt_Lrclk[1]_i_1_n_0\ : STD_LOGIC;
  signal \Cnt_Lrclk[2]_i_1_n_0\ : STD_LOGIC;
  signal \Cnt_Lrclk[3]_i_1_n_0\ : STD_LOGIC;
  signal \Cnt_Lrclk[4]_i_2_n_0\ : STD_LOGIC;
  signal \^d\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal D_L_O_int : STD_LOGIC;
  signal \D_R_O_int[23]_i_1_n_0\ : STD_LOGIC;
  signal Data_In_int : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal \Data_In_int[31]_i_1_n_0\ : STD_LOGIC;
  signal \Data_In_int[31]_i_3_n_0\ : STD_LOGIC;
  signal \Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11_n_0\ : STD_LOGIC;
  signal \Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12_n_0\ : STD_LOGIC;
  signal Data_In_int_reg_gate_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_0_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_10_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_11_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_12_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_1_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_2_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_3_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_4_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_5_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_6_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_7_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_8_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_9_n_0 : STD_LOGIC;
  signal Data_In_int_reg_r_n_0 : STD_LOGIC;
  signal \Data_Out_int[13]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[14]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[15]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[16]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[17]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[18]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[19]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[20]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[21]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[22]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[23]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[24]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[25]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[26]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[27]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[28]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[29]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[30]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[31]_i_1_n_0\ : STD_LOGIC;
  signal \Data_Out_int[31]_i_2_n_0\ : STD_LOGIC;
  signal \Data_Out_int[31]_i_3_n_0\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[13]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[14]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[15]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[16]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[17]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[18]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[19]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[20]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[21]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[22]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[23]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[24]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[25]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[26]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[27]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[28]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[29]\ : STD_LOGIC;
  signal \Data_Out_int_reg_n_0_[30]\ : STD_LOGIC;
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal LRCLK_i_1_n_0 : STD_LOGIC;
  signal LRCLK_i_2_n_0 : STD_LOGIC;
  signal L_bus_g : STD_LOGIC;
  signal \^l_bus_in_s_reg[17]\ : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \^l_unsigned_data_prev_reg[7]\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^l_unsigned_data_prev_reg[8]\ : STD_LOGIC;
  signal \^l_unsigned_data_prev_reg[9]\ : STD_LOGIC;
  signal R_bus_g : STD_LOGIC;
  signal \^r_bus_in_s_reg[17]\ : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \^r_unsigned_data_prev_reg[8]\ : STD_LOGIC;
  signal \^r_unsigned_data_prev_reg[9]\ : STD_LOGIC;
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^ac_bclk\ : STD_LOGIC;
  signal \^ac_lrclk\ : STD_LOGIC;
  signal \axi_rdata[0]_i_13_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_13_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[12]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[13]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[14]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[5]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[6]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2__11_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal p_17_in : STD_LOGIC;
  signal ready_sig_i_2_n_0 : STD_LOGIC;
  signal \state[0]_i_14_n_0\ : STD_LOGIC;
  signal \state[0]_i_15_n_0\ : STD_LOGIC;
  signal \state[0]_i_19_n_0\ : STD_LOGIC;
  signal \state[0]_i_20_n_0\ : STD_LOGIC;
  signal \state[0]_i_21_n_0\ : STD_LOGIC;
  signal \state[0]_i_22_n_0\ : STD_LOGIC;
  signal \state[0]_i_23_n_0\ : STD_LOGIC;
  signal \state[0]_i_24_n_0\ : STD_LOGIC;
  signal \state[0]_i_25_n_0\ : STD_LOGIC;
  signal \state[0]_i_26_n_0\ : STD_LOGIC;
  signal \state[0]_i_35_n_0\ : STD_LOGIC;
  signal \state[0]_i_36_n_0\ : STD_LOGIC;
  signal \state[0]_i_37_n_0\ : STD_LOGIC;
  signal \state[0]_i_38_n_0\ : STD_LOGIC;
  signal \state[0]_i_39_n_0\ : STD_LOGIC;
  signal \state[0]_i_40_n_0\ : STD_LOGIC;
  signal \state[0]_i_41_n_0\ : STD_LOGIC;
  signal \state[0]_i_42_n_0\ : STD_LOGIC;
  signal \state[0]_i_8_n_0\ : STD_LOGIC;
  signal \state[0]_i_9_n_0\ : STD_LOGIC;
  signal \state_reg[0]_i_13_n_0\ : STD_LOGIC;
  signal \state_reg[0]_i_13_n_1\ : STD_LOGIC;
  signal \state_reg[0]_i_13_n_2\ : STD_LOGIC;
  signal \state_reg[0]_i_13_n_3\ : STD_LOGIC;
  signal \state_reg[0]_i_7_n_0\ : STD_LOGIC;
  signal \state_reg[0]_i_7_n_1\ : STD_LOGIC;
  signal \state_reg[0]_i_7_n_2\ : STD_LOGIC;
  signal \state_reg[0]_i_7_n_3\ : STD_LOGIC;
  signal sw : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_state_reg[0]_i_13_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_state_reg[0]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_state_reg[0]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_state_reg[0]_i_5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_state_reg[0]_i_5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_state_reg[0]_i_7_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of BCLK_int_i_2 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \Cnt_Bclk[1]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \Cnt_Bclk[2]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \Cnt_Bclk[3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \Cnt_Bclk[4]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \Cnt_Lrclk[0]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \Cnt_Lrclk[1]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \Cnt_Lrclk[2]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \Cnt_Lrclk[3]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \Cnt_Lrclk[4]_i_2\ : label is "soft_lutpair3";
  attribute srl_bus_name : string;
  attribute srl_bus_name of \Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11\ : label is "\U0/my_oscope_ip_v1_0_S00_AXI_inst/datapath/audio_codec/audio_inout/Data_In_int_reg ";
  attribute srl_name : string;
  attribute srl_name of \Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11\ : label is "\U0/my_oscope_ip_v1_0_S00_AXI_inst/datapath/audio_codec/audio_inout/Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11 ";
  attribute SOFT_HLUTNM of LRCLK_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of LRCLK_i_2 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \L_unsigned_data_prev[4]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \L_unsigned_data_prev[5]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \L_unsigned_data_prev[6]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \L_unsigned_data_prev[7]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \R_unsigned_data_prev[4]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \R_unsigned_data_prev[5]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \R_unsigned_data_prev[6]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \R_unsigned_data_prev[7]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of ac_lrclk_sig_prev_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of ready_sig_i_1 : label is "soft_lutpair1";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of \state_reg[0]_i_13\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \state_reg[0]_i_3\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \state_reg[0]_i_5\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \state_reg[0]_i_7\ : label is "{SYNTH-8 {cell *THIS*}}";
begin
  D(4 downto 0) <= \^d\(4 downto 0);
  E(0) <= \^e\(0);
  \L_bus_in_s_reg[17]\(17 downto 0) <= \^l_bus_in_s_reg[17]\(17 downto 0);
  \L_unsigned_data_prev_reg[7]\(4 downto 0) <= \^l_unsigned_data_prev_reg[7]\(4 downto 0);
  \L_unsigned_data_prev_reg[8]\ <= \^l_unsigned_data_prev_reg[8]\;
  \L_unsigned_data_prev_reg[9]\ <= \^l_unsigned_data_prev_reg[9]\;
  \R_bus_in_s_reg[17]\(17 downto 0) <= \^r_bus_in_s_reg[17]\(17 downto 0);
  \R_unsigned_data_prev_reg[8]\ <= \^r_unsigned_data_prev_reg[8]\;
  \R_unsigned_data_prev_reg[9]\ <= \^r_unsigned_data_prev_reg[9]\;
  SR(0) <= \^sr\(0);
  ac_bclk <= \^ac_bclk\;
  ac_lrclk <= \^ac_lrclk\;
BCLK_int_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset_n,
      O => \^sr\(0)
    );
BCLK_int_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Cnt_Bclk0,
      I1 => \^ac_bclk\,
      O => BCLK_int_i_2_n_0
    );
BCLK_int_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => BCLK_int_i_2_n_0,
      Q => \^ac_bclk\,
      R => \^sr\(0)
    );
\Cnt_Bclk0_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED\(3 downto 2),
      CO(1) => Cnt_Bclk0,
      CO(0) => \Cnt_Bclk0_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \i__carry_i_1_n_0\,
      S(0) => \i__carry_i_2__11_n_0\
    );
\Cnt_Bclk[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(0),
      O => p_0_in(0)
    );
\Cnt_Bclk[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(1),
      I1 => \Cnt_Bclk_reg__0\(0),
      O => p_0_in(1)
    );
\Cnt_Bclk[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(2),
      I1 => \Cnt_Bclk_reg__0\(0),
      I2 => \Cnt_Bclk_reg__0\(1),
      O => p_0_in(2)
    );
\Cnt_Bclk[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(3),
      I1 => \Cnt_Bclk_reg__0\(2),
      I2 => \Cnt_Bclk_reg__0\(1),
      I3 => \Cnt_Bclk_reg__0\(0),
      O => p_0_in(3)
    );
\Cnt_Bclk[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => Cnt_Bclk0,
      I1 => reset_n,
      O => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Bclk[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(4),
      I1 => \Cnt_Bclk_reg__0\(0),
      I2 => \Cnt_Bclk_reg__0\(1),
      I3 => \Cnt_Bclk_reg__0\(2),
      I4 => \Cnt_Bclk_reg__0\(3),
      O => p_0_in(4)
    );
\Cnt_Bclk_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(0),
      Q => \Cnt_Bclk_reg__0\(0),
      R => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Bclk_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(1),
      Q => \Cnt_Bclk_reg__0\(1),
      R => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Bclk_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(2),
      Q => \Cnt_Bclk_reg__0\(2),
      R => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Bclk_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(3),
      Q => \Cnt_Bclk_reg__0\(3),
      R => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Bclk_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(4),
      Q => \Cnt_Bclk_reg__0\(4),
      R => \Cnt_Bclk[4]_i_1_n_0\
    );
\Cnt_Lrclk[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Cnt_Lrclk(0),
      O => \Cnt_Lrclk[0]_i_1_n_0\
    );
\Cnt_Lrclk[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Cnt_Lrclk(1),
      I1 => Cnt_Lrclk(0),
      O => \Cnt_Lrclk[1]_i_1_n_0\
    );
\Cnt_Lrclk[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => Cnt_Lrclk(2),
      I1 => Cnt_Lrclk(0),
      I2 => Cnt_Lrclk(1),
      O => \Cnt_Lrclk[2]_i_1_n_0\
    );
\Cnt_Lrclk[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => Cnt_Lrclk(3),
      I1 => Cnt_Lrclk(1),
      I2 => Cnt_Lrclk(0),
      I3 => Cnt_Lrclk(2),
      O => \Cnt_Lrclk[3]_i_1_n_0\
    );
\Cnt_Lrclk[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => Cnt_Bclk0,
      I1 => \^ac_bclk\,
      O => BCLK_Fall_int
    );
\Cnt_Lrclk[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => Cnt_Lrclk(4),
      I1 => Cnt_Lrclk(2),
      I2 => Cnt_Lrclk(0),
      I3 => Cnt_Lrclk(1),
      I4 => Cnt_Lrclk(3),
      O => \Cnt_Lrclk[4]_i_2_n_0\
    );
\Cnt_Lrclk_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => BCLK_Fall_int,
      D => \Cnt_Lrclk[0]_i_1_n_0\,
      Q => Cnt_Lrclk(0),
      R => \^sr\(0)
    );
\Cnt_Lrclk_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => BCLK_Fall_int,
      D => \Cnt_Lrclk[1]_i_1_n_0\,
      Q => Cnt_Lrclk(1),
      R => \^sr\(0)
    );
\Cnt_Lrclk_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => BCLK_Fall_int,
      D => \Cnt_Lrclk[2]_i_1_n_0\,
      Q => Cnt_Lrclk(2),
      R => \^sr\(0)
    );
\Cnt_Lrclk_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => BCLK_Fall_int,
      D => \Cnt_Lrclk[3]_i_1_n_0\,
      Q => Cnt_Lrclk(3),
      R => \^sr\(0)
    );
\Cnt_Lrclk_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => BCLK_Fall_int,
      D => \Cnt_Lrclk[4]_i_2_n_0\,
      Q => Cnt_Lrclk(4),
      R => \^sr\(0)
    );
\D_L_O_int[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^ac_lrclk\,
      I1 => \Data_In_int[31]_i_3_n_0\,
      O => D_L_O_int
    );
\D_L_O_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(18),
      Q => \^l_bus_in_s_reg[17]\(4),
      R => \^sr\(0)
    );
\D_L_O_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(19),
      Q => \^l_bus_in_s_reg[17]\(5),
      R => \^sr\(0)
    );
\D_L_O_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(20),
      Q => \^l_bus_in_s_reg[17]\(6),
      R => \^sr\(0)
    );
\D_L_O_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(21),
      Q => \^l_bus_in_s_reg[17]\(7),
      R => \^sr\(0)
    );
\D_L_O_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(22),
      Q => \^l_bus_in_s_reg[17]\(8),
      R => \^sr\(0)
    );
\D_L_O_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(23),
      Q => \^l_bus_in_s_reg[17]\(9),
      R => \^sr\(0)
    );
\D_L_O_int_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(24),
      Q => \^l_bus_in_s_reg[17]\(10),
      R => \^sr\(0)
    );
\D_L_O_int_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(25),
      Q => \^l_bus_in_s_reg[17]\(11),
      R => \^sr\(0)
    );
\D_L_O_int_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(26),
      Q => \^l_bus_in_s_reg[17]\(12),
      R => \^sr\(0)
    );
\D_L_O_int_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(27),
      Q => \^l_bus_in_s_reg[17]\(13),
      R => \^sr\(0)
    );
\D_L_O_int_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(28),
      Q => \^l_bus_in_s_reg[17]\(14),
      R => \^sr\(0)
    );
\D_L_O_int_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(29),
      Q => \^l_bus_in_s_reg[17]\(15),
      R => \^sr\(0)
    );
\D_L_O_int_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(30),
      Q => \^l_bus_in_s_reg[17]\(16),
      R => \^sr\(0)
    );
\D_L_O_int_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(31),
      Q => \^l_bus_in_s_reg[17]\(17),
      R => \^sr\(0)
    );
\D_L_O_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(14),
      Q => \^l_bus_in_s_reg[17]\(0),
      R => \^sr\(0)
    );
\D_L_O_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(15),
      Q => \^l_bus_in_s_reg[17]\(1),
      R => \^sr\(0)
    );
\D_L_O_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(16),
      Q => \^l_bus_in_s_reg[17]\(2),
      R => \^sr\(0)
    );
\D_L_O_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => D_L_O_int,
      D => Data_In_int(17),
      Q => \^l_bus_in_s_reg[17]\(3),
      R => \^sr\(0)
    );
\D_R_O_int[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^ac_lrclk\,
      I1 => \Data_In_int[31]_i_3_n_0\,
      O => \D_R_O_int[23]_i_1_n_0\
    );
\D_R_O_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(18),
      Q => \^r_bus_in_s_reg[17]\(4),
      R => \^sr\(0)
    );
\D_R_O_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(19),
      Q => \^r_bus_in_s_reg[17]\(5),
      R => \^sr\(0)
    );
\D_R_O_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(20),
      Q => \^r_bus_in_s_reg[17]\(6),
      R => \^sr\(0)
    );
\D_R_O_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(21),
      Q => \^r_bus_in_s_reg[17]\(7),
      R => \^sr\(0)
    );
\D_R_O_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(22),
      Q => \^r_bus_in_s_reg[17]\(8),
      R => \^sr\(0)
    );
\D_R_O_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(23),
      Q => \^r_bus_in_s_reg[17]\(9),
      R => \^sr\(0)
    );
\D_R_O_int_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(24),
      Q => \^r_bus_in_s_reg[17]\(10),
      R => \^sr\(0)
    );
\D_R_O_int_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(25),
      Q => \^r_bus_in_s_reg[17]\(11),
      R => \^sr\(0)
    );
\D_R_O_int_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(26),
      Q => \^r_bus_in_s_reg[17]\(12),
      R => \^sr\(0)
    );
\D_R_O_int_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(27),
      Q => \^r_bus_in_s_reg[17]\(13),
      R => \^sr\(0)
    );
\D_R_O_int_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(28),
      Q => \^r_bus_in_s_reg[17]\(14),
      R => \^sr\(0)
    );
\D_R_O_int_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(29),
      Q => \^r_bus_in_s_reg[17]\(15),
      R => \^sr\(0)
    );
\D_R_O_int_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(30),
      Q => \^r_bus_in_s_reg[17]\(16),
      R => \^sr\(0)
    );
\D_R_O_int_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(31),
      Q => \^r_bus_in_s_reg[17]\(17),
      R => \^sr\(0)
    );
\D_R_O_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(14),
      Q => \^r_bus_in_s_reg[17]\(0),
      R => \^sr\(0)
    );
\D_R_O_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(15),
      Q => \^r_bus_in_s_reg[17]\(1),
      R => \^sr\(0)
    );
\D_R_O_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(16),
      Q => \^r_bus_in_s_reg[17]\(2),
      R => \^sr\(0)
    );
\D_R_O_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \D_R_O_int[23]_i_1_n_0\,
      D => Data_In_int(17),
      Q => \^r_bus_in_s_reg[17]\(3),
      R => \^sr\(0)
    );
\Data_In_int[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => reset_n,
      I1 => \Data_In_int[31]_i_3_n_0\,
      O => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Cnt_Bclk0,
      I1 => \^ac_bclk\,
      O => p_17_in
    );
\Data_In_int[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => Cnt_Lrclk(3),
      I1 => Cnt_Lrclk(4),
      I2 => Cnt_Lrclk(2),
      I3 => Cnt_Lrclk(1),
      I4 => Cnt_Lrclk(0),
      I5 => BCLK_Fall_int,
      O => \Data_In_int[31]_i_3_n_0\
    );
\Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '0',
      A2 => '1',
      A3 => '1',
      CE => p_17_in,
      CLK => clk,
      D => ac_adc_sdata,
      Q => \Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11_n_0\
    );
\Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => \Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11_n_0\,
      Q => \Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12_n_0\,
      R => '0'
    );
\Data_In_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_gate_n_0,
      Q => Data_In_int(14),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(14),
      Q => Data_In_int(15),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(15),
      Q => Data_In_int(16),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(16),
      Q => Data_In_int(17),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(17),
      Q => Data_In_int(18),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(18),
      Q => Data_In_int(19),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(19),
      Q => Data_In_int(20),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(20),
      Q => Data_In_int(21),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(21),
      Q => Data_In_int(22),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(22),
      Q => Data_In_int(23),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(23),
      Q => Data_In_int(24),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(24),
      Q => Data_In_int(25),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(25),
      Q => Data_In_int(26),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(26),
      Q => Data_In_int(27),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(27),
      Q => Data_In_int(28),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(28),
      Q => Data_In_int(29),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(29),
      Q => Data_In_int(30),
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_In_int_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int(30),
      Q => Data_In_int(31),
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_gate: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12_n_0\,
      I1 => Data_In_int_reg_r_12_n_0,
      O => Data_In_int_reg_gate_n_0
    );
Data_In_int_reg_r: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => '1',
      Q => Data_In_int_reg_r_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_0: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_n_0,
      Q => Data_In_int_reg_r_0_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_1: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_0_n_0,
      Q => Data_In_int_reg_r_1_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_10: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_9_n_0,
      Q => Data_In_int_reg_r_10_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_11: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_10_n_0,
      Q => Data_In_int_reg_r_11_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_12: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_11_n_0,
      Q => Data_In_int_reg_r_12_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_2: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_1_n_0,
      Q => Data_In_int_reg_r_2_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_3: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_2_n_0,
      Q => Data_In_int_reg_r_3_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_4: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_3_n_0,
      Q => Data_In_int_reg_r_4_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_5: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_4_n_0,
      Q => Data_In_int_reg_r_5_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_6: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_5_n_0,
      Q => Data_In_int_reg_r_6_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_7: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_6_n_0,
      Q => Data_In_int_reg_r_7_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_8: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_7_n_0,
      Q => Data_In_int_reg_r_8_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
Data_In_int_reg_r_9: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_17_in,
      D => Data_In_int_reg_r_8_n_0,
      Q => Data_In_int_reg_r_9_n_0,
      R => \Data_In_int[31]_i_1_n_0\
    );
\Data_Out_int[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8CC08CC"
    )
        port map (
      I0 => \Data_Out_int[31]_i_3_n_0\,
      I1 => \L_bus_in_s_reg[17]_0\(0),
      I2 => \^ac_lrclk\,
      I3 => reset_n,
      I4 => \R_bus_in_s_reg[17]_0\(0),
      O => \Data_Out_int[13]_i_1_n_0\
    );
\Data_Out_int[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(1),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[13]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(1),
      O => \Data_Out_int[14]_i_1_n_0\
    );
\Data_Out_int[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(2),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[14]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(2),
      O => \Data_Out_int[15]_i_1_n_0\
    );
\Data_Out_int[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(3),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[15]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(3),
      O => \Data_Out_int[16]_i_1_n_0\
    );
\Data_Out_int[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(4),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[16]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(4),
      O => \Data_Out_int[17]_i_1_n_0\
    );
\Data_Out_int[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(5),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[17]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(5),
      O => \Data_Out_int[18]_i_1_n_0\
    );
\Data_Out_int[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(6),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[18]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(6),
      O => \Data_Out_int[19]_i_1_n_0\
    );
\Data_Out_int[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(7),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[19]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(7),
      O => \Data_Out_int[20]_i_1_n_0\
    );
\Data_Out_int[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(8),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[20]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(8),
      O => \Data_Out_int[21]_i_1_n_0\
    );
\Data_Out_int[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(9),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[21]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(9),
      O => \Data_Out_int[22]_i_1_n_0\
    );
\Data_Out_int[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(10),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[22]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(10),
      O => \Data_Out_int[23]_i_1_n_0\
    );
\Data_Out_int[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(11),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[23]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(11),
      O => \Data_Out_int[24]_i_1_n_0\
    );
\Data_Out_int[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(12),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[24]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(12),
      O => \Data_Out_int[25]_i_1_n_0\
    );
\Data_Out_int[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(13),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[25]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(13),
      O => \Data_Out_int[26]_i_1_n_0\
    );
\Data_Out_int[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(14),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[26]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(14),
      O => \Data_Out_int[27]_i_1_n_0\
    );
\Data_Out_int[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(15),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[27]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(15),
      O => \Data_Out_int[28]_i_1_n_0\
    );
\Data_Out_int[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(16),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[28]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(16),
      O => \Data_Out_int[29]_i_1_n_0\
    );
\Data_Out_int[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0FFFF8F800000"
    )
        port map (
      I0 => \R_bus_in_s_reg[17]_0\(17),
      I1 => \^ac_lrclk\,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      I3 => \Data_Out_int_reg_n_0_[29]\,
      I4 => reset_n,
      I5 => \L_bus_in_s_reg[17]_0\(17),
      O => \Data_Out_int[30]_i_1_n_0\
    );
\Data_Out_int[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF8F"
    )
        port map (
      I0 => \^ac_bclk\,
      I1 => Cnt_Bclk0,
      I2 => reset_n,
      I3 => \Data_Out_int[31]_i_3_n_0\,
      O => \Data_Out_int[31]_i_1_n_0\
    );
\Data_Out_int[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \Data_Out_int_reg_n_0_[30]\,
      I1 => reset_n,
      I2 => \Data_Out_int[31]_i_3_n_0\,
      O => \Data_Out_int[31]_i_2_n_0\
    );
\Data_Out_int[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => p_17_in,
      I1 => Cnt_Lrclk(3),
      I2 => Cnt_Lrclk(4),
      I3 => Cnt_Lrclk(2),
      I4 => Cnt_Lrclk(1),
      I5 => Cnt_Lrclk(0),
      O => \Data_Out_int[31]_i_3_n_0\
    );
\Data_Out_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[13]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[13]\,
      R => '0'
    );
\Data_Out_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[14]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[14]\,
      R => '0'
    );
\Data_Out_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[15]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[15]\,
      R => '0'
    );
\Data_Out_int_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[16]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[16]\,
      R => '0'
    );
\Data_Out_int_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[17]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[17]\,
      R => '0'
    );
\Data_Out_int_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[18]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[18]\,
      R => '0'
    );
\Data_Out_int_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[19]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[19]\,
      R => '0'
    );
\Data_Out_int_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[20]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[20]\,
      R => '0'
    );
\Data_Out_int_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[21]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[21]\,
      R => '0'
    );
\Data_Out_int_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[22]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[22]\,
      R => '0'
    );
\Data_Out_int_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[23]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[23]\,
      R => '0'
    );
\Data_Out_int_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[24]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[24]\,
      R => '0'
    );
\Data_Out_int_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[25]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[25]\,
      R => '0'
    );
\Data_Out_int_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[26]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[26]\,
      R => '0'
    );
\Data_Out_int_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[27]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[27]\,
      R => '0'
    );
\Data_Out_int_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[28]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[28]\,
      R => '0'
    );
\Data_Out_int_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[29]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[29]\,
      R => '0'
    );
\Data_Out_int_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[30]_i_1_n_0\,
      Q => \Data_Out_int_reg_n_0_[30]\,
      R => '0'
    );
\Data_Out_int_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \Data_Out_int[31]_i_1_n_0\,
      D => \Data_Out_int[31]_i_2_n_0\,
      Q => ac_dac_sdata,
      R => '0'
    );
LRCLK_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => Cnt_Lrclk(4),
      I1 => LRCLK_i_2_n_0,
      I2 => Cnt_Bclk0,
      I3 => \^ac_bclk\,
      I4 => \^ac_lrclk\,
      O => LRCLK_i_1_n_0
    );
LRCLK_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => Cnt_Lrclk(3),
      I1 => Cnt_Lrclk(1),
      I2 => Cnt_Lrclk(0),
      I3 => Cnt_Lrclk(2),
      O => LRCLK_i_2_n_0
    );
LRCLK_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => LRCLK_i_1_n_0,
      Q => \^ac_lrclk\,
      R => \^sr\(0)
    );
\L_unsigned_data_prev[3]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^l_bus_in_s_reg[17]\(12),
      O => \^l_unsigned_data_prev_reg[7]\(0)
    );
\L_unsigned_data_prev[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^l_bus_in_s_reg[17]\(12),
      I1 => \^l_bus_in_s_reg[17]\(13),
      O => \^l_unsigned_data_prev_reg[7]\(1)
    );
\L_unsigned_data_prev[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^l_bus_in_s_reg[17]\(14),
      I1 => \^l_bus_in_s_reg[17]\(13),
      I2 => \^l_bus_in_s_reg[17]\(12),
      O => \^l_unsigned_data_prev_reg[7]\(2)
    );
\L_unsigned_data_prev[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9555"
    )
        port map (
      I0 => \^l_bus_in_s_reg[17]\(15),
      I1 => \^l_bus_in_s_reg[17]\(12),
      I2 => \^l_bus_in_s_reg[17]\(13),
      I3 => \^l_bus_in_s_reg[17]\(14),
      O => \^l_unsigned_data_prev_reg[7]\(3)
    );
\L_unsigned_data_prev[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA9555"
    )
        port map (
      I0 => \^l_bus_in_s_reg[17]\(16),
      I1 => \^l_bus_in_s_reg[17]\(14),
      I2 => \^l_bus_in_s_reg[17]\(13),
      I3 => \^l_bus_in_s_reg[17]\(12),
      I4 => \^l_bus_in_s_reg[17]\(15),
      O => \^l_unsigned_data_prev_reg[7]\(4)
    );
\L_unsigned_data_prev[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5556565656565656"
    )
        port map (
      I0 => \^l_bus_in_s_reg[17]\(17),
      I1 => \^l_bus_in_s_reg[17]\(16),
      I2 => \^l_bus_in_s_reg[17]\(15),
      I3 => \^l_bus_in_s_reg[17]\(12),
      I4 => \^l_bus_in_s_reg[17]\(13),
      I5 => \^l_bus_in_s_reg[17]\(14),
      O => \^l_unsigned_data_prev_reg[8]\
    );
\L_unsigned_data_prev[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0111111100000000"
    )
        port map (
      I0 => \^l_bus_in_s_reg[17]\(16),
      I1 => \^l_bus_in_s_reg[17]\(15),
      I2 => \^l_bus_in_s_reg[17]\(12),
      I3 => \^l_bus_in_s_reg[17]\(13),
      I4 => \^l_bus_in_s_reg[17]\(14),
      I5 => \^l_bus_in_s_reg[17]\(17),
      O => \^l_unsigned_data_prev_reg[9]\
    );
\R_unsigned_data_prev[3]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(12),
      O => \^d\(0)
    );
\R_unsigned_data_prev[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(12),
      I1 => \^r_bus_in_s_reg[17]\(13),
      O => \^d\(1)
    );
\R_unsigned_data_prev[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(14),
      I1 => \^r_bus_in_s_reg[17]\(13),
      I2 => \^r_bus_in_s_reg[17]\(12),
      O => \^d\(2)
    );
\R_unsigned_data_prev[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9555"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(15),
      I1 => \^r_bus_in_s_reg[17]\(12),
      I2 => \^r_bus_in_s_reg[17]\(13),
      I3 => \^r_bus_in_s_reg[17]\(14),
      O => \^d\(3)
    );
\R_unsigned_data_prev[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA9555"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(16),
      I1 => \^r_bus_in_s_reg[17]\(14),
      I2 => \^r_bus_in_s_reg[17]\(13),
      I3 => \^r_bus_in_s_reg[17]\(12),
      I4 => \^r_bus_in_s_reg[17]\(15),
      O => \^d\(4)
    );
\R_unsigned_data_prev[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5556565656565656"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(17),
      I1 => \^r_bus_in_s_reg[17]\(16),
      I2 => \^r_bus_in_s_reg[17]\(15),
      I3 => \^r_bus_in_s_reg[17]\(12),
      I4 => \^r_bus_in_s_reg[17]\(13),
      I5 => \^r_bus_in_s_reg[17]\(14),
      O => \^r_unsigned_data_prev_reg[8]\
    );
\R_unsigned_data_prev[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0111111100000000"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(16),
      I1 => \^r_bus_in_s_reg[17]\(15),
      I2 => \^r_bus_in_s_reg[17]\(12),
      I3 => \^r_bus_in_s_reg[17]\(13),
      I4 => \^r_bus_in_s_reg[17]\(14),
      I5 => \^r_bus_in_s_reg[17]\(17),
      O => \^r_unsigned_data_prev_reg[9]\
    );
\ac_lrclk_count[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAAA0000FFFFFFFF"
    )
        port map (
      I0 => Q(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => Q(2),
      I4 => \^e\(0),
      I5 => reset_n,
      O => \ac_lrclk_count_reg[0]\(0)
    );
\ac_lrclk_count[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^ac_lrclk\,
      I1 => ac_lrclk_sig_prev_reg_0,
      O => \^e\(0)
    );
ac_lrclk_sig_prev_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D8"
    )
        port map (
      I0 => reset_n,
      I1 => \^ac_lrclk\,
      I2 => ac_lrclk_sig_prev_reg_0,
      O => ac_lrclk_sig_prev_reg
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]\,
      I1 => \axi_araddr_reg[4]_0\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_1\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[0]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(0)
    );
\axi_rdata[0]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(2),
      I1 => \^l_bus_in_s_reg[17]\(2),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(0),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(0),
      O => \axi_rdata[0]_i_13_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_26\,
      I1 => \axi_araddr_reg[4]_27\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_28\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[10]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(9)
    );
\axi_rdata[10]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(12),
      I1 => \^l_bus_in_s_reg[17]\(12),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(10),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(10),
      O => \axi_rdata[10]_i_12_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_29\,
      I1 => \axi_araddr_reg[4]_30\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_31\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[11]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(10)
    );
\axi_rdata[11]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(13),
      I1 => \^l_bus_in_s_reg[17]\(13),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(11),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(11),
      O => \axi_rdata[11]_i_12_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_32\,
      I1 => \axi_araddr_reg[4]_33\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_34\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[12]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(11)
    );
\axi_rdata[12]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(14),
      I1 => \^l_bus_in_s_reg[17]\(14),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(12),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(12),
      O => \axi_rdata[12]_i_12_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_35\,
      I1 => \axi_araddr_reg[4]_36\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_37\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[13]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(12)
    );
\axi_rdata[13]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(15),
      I1 => \^l_bus_in_s_reg[17]\(15),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(13),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(13),
      O => \axi_rdata[13]_i_12_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_38\,
      I1 => \axi_araddr_reg[4]_39\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_40\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[14]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(13)
    );
\axi_rdata[14]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(16),
      I1 => \^l_bus_in_s_reg[17]\(16),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(14),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(14),
      O => \axi_rdata[14]_i_12_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_41\,
      I1 => \axi_araddr_reg[4]_42\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_43\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[15]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(14)
    );
\axi_rdata[15]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(17),
      I1 => \^l_bus_in_s_reg[17]\(17),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(15),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(15),
      O => \axi_rdata[15]_i_12_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_2\,
      I1 => \axi_araddr_reg[4]_3\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_4\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[1]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(1)
    );
\axi_rdata[1]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(3),
      I1 => \^l_bus_in_s_reg[17]\(3),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(1),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(1),
      O => \axi_rdata[1]_i_12_n_0\
    );
\axi_rdata[2]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(4),
      I1 => \^l_bus_in_s_reg[17]\(4),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(2),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(2),
      O => \axi_rdata[2]_i_13_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_5\,
      I1 => \axi_araddr_reg[4]_6\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_7\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[3]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(2)
    );
\axi_rdata[3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(5),
      I1 => \^l_bus_in_s_reg[17]\(5),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(3),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(3),
      O => \axi_rdata[3]_i_12_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_8\,
      I1 => \axi_araddr_reg[4]_9\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_10\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[4]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(3)
    );
\axi_rdata[4]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(6),
      I1 => \^l_bus_in_s_reg[17]\(6),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(4),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(4),
      O => \axi_rdata[4]_i_12_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_11\,
      I1 => \axi_araddr_reg[4]_12\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_13\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[5]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(4)
    );
\axi_rdata[5]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(7),
      I1 => \^l_bus_in_s_reg[17]\(7),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(5),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(5),
      O => \axi_rdata[5]_i_12_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_14\,
      I1 => \axi_araddr_reg[4]_15\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_16\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[6]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(5)
    );
\axi_rdata[6]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(8),
      I1 => \^l_bus_in_s_reg[17]\(8),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(6),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(6),
      O => \axi_rdata[6]_i_12_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_17\,
      I1 => \axi_araddr_reg[4]_18\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_19\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[7]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(6)
    );
\axi_rdata[7]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(9),
      I1 => \^l_bus_in_s_reg[17]\(9),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(7),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(7),
      O => \axi_rdata[7]_i_12_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_20\,
      I1 => \axi_araddr_reg[4]_21\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_22\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[8]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(7)
    );
\axi_rdata[8]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(10),
      I1 => \^l_bus_in_s_reg[17]\(10),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(8),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(8),
      O => \axi_rdata[8]_i_12_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_araddr_reg[4]_23\,
      I1 => \axi_araddr_reg[4]_24\,
      I2 => \axi_araddr_reg[6]\(2),
      I3 => \axi_araddr_reg[4]_25\,
      I4 => \axi_araddr_reg[6]\(1),
      I5 => \axi_rdata_reg[9]_i_5_n_0\,
      O => \axi_rdata_reg[15]\(8)
    );
\axi_rdata[9]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(11),
      I1 => \^l_bus_in_s_reg[17]\(11),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \slv_reg5_reg[15]\(9),
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => \slv_reg4_reg[15]\(9),
      O => \axi_rdata[9]_i_12_n_0\
    );
\axi_rdata_reg[0]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[0]\,
      I1 => \axi_rdata[0]_i_13_n_0\,
      O => \axi_rdata_reg[0]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[10]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[10]\,
      I1 => \axi_rdata[10]_i_12_n_0\,
      O => \axi_rdata_reg[10]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[11]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[11]\,
      I1 => \axi_rdata[11]_i_12_n_0\,
      O => \axi_rdata_reg[11]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[12]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[12]\,
      I1 => \axi_rdata[12]_i_12_n_0\,
      O => \axi_rdata_reg[12]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[13]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[13]\,
      I1 => \axi_rdata[13]_i_12_n_0\,
      O => \axi_rdata_reg[13]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[14]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[14]\,
      I1 => \axi_rdata[14]_i_12_n_0\,
      O => \axi_rdata_reg[14]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[15]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[15]\,
      I1 => \axi_rdata[15]_i_12_n_0\,
      O => \axi_rdata_reg[15]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[1]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[1]\,
      I1 => \axi_rdata[1]_i_12_n_0\,
      O => \axi_rdata_reg[1]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[2]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[2]\,
      I1 => \axi_rdata[2]_i_13_n_0\,
      O => \axi_rdata_reg[2]\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[3]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[3]\,
      I1 => \axi_rdata[3]_i_12_n_0\,
      O => \axi_rdata_reg[3]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[4]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[4]\,
      I1 => \axi_rdata[4]_i_12_n_0\,
      O => \axi_rdata_reg[4]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[5]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[5]\,
      I1 => \axi_rdata[5]_i_12_n_0\,
      O => \axi_rdata_reg[5]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[6]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[6]\,
      I1 => \axi_rdata[6]_i_12_n_0\,
      O => \axi_rdata_reg[6]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[7]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[7]\,
      I1 => \axi_rdata[7]_i_12_n_0\,
      O => \axi_rdata_reg[7]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[8]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[8]\,
      I1 => \axi_rdata[8]_i_12_n_0\,
      O => \axi_rdata_reg[8]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\axi_rdata_reg[9]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \slv_reg3_reg[9]\,
      I1 => \axi_rdata[9]_i_12_n_0\,
      O => \axi_rdata_reg[9]_i_5_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
\i__carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(3),
      I1 => \Cnt_Bclk_reg__0\(4),
      O => \i__carry_i_1_n_0\
    );
\i__carry_i_2__11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \Cnt_Bclk_reg__0\(2),
      I1 => \Cnt_Bclk_reg__0\(0),
      I2 => \Cnt_Bclk_reg__0\(1),
      O => \i__carry_i_2__11_n_0\
    );
ready_sig_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF040F0"
    )
        port map (
      I0 => ac_lrclk_sig_prev_reg_0,
      I1 => \^ac_lrclk\,
      I2 => ready_sig_reg_0,
      I3 => reset_n,
      I4 => ready_sig_i_2_n_0,
      O => ready_sig_reg
    );
ready_sig_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444440000000"
    )
        port map (
      I0 => ac_lrclk_sig_prev_reg_0,
      I1 => \^ac_lrclk\,
      I2 => Q(2),
      I3 => Q(0),
      I4 => Q(1),
      I5 => Q(3),
      O => ready_sig_i_2_n_0
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_10__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg2_reg[15]\(0),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^r_bus_in_s_reg[17]\(9),
      O => DIBDI(0)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg2_reg[15]\(9),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^r_unsigned_data_prev_reg[9]\,
      O => DIBDI(9)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg2_reg[15]\(8),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^r_unsigned_data_prev_reg[8]\,
      O => DIBDI(8)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_20\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg1_reg[15]\(9),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^l_unsigned_data_prev_reg[9]\,
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(9)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_21\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg1_reg[15]\(8),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^l_unsigned_data_prev_reg[8]\,
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(8)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg1_reg[15]\(7),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^l_unsigned_data_prev_reg[7]\(4),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(7)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B88B8B8B8B8B8B8B"
    )
        port map (
      I0 => \slv_reg1_reg[15]\(6),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^l_bus_in_s_reg[17]\(15),
      I3 => \^l_bus_in_s_reg[17]\(12),
      I4 => \^l_bus_in_s_reg[17]\(13),
      I5 => \^l_bus_in_s_reg[17]\(14),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(6)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_24\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8BB8B8B8"
    )
        port map (
      I0 => \slv_reg1_reg[15]\(5),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^l_bus_in_s_reg[17]\(14),
      I3 => \^l_bus_in_s_reg[17]\(13),
      I4 => \^l_bus_in_s_reg[17]\(12),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(5)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_25\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8BB8"
    )
        port map (
      I0 => \slv_reg1_reg[15]\(4),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^l_bus_in_s_reg[17]\(12),
      I3 => \^l_bus_in_s_reg[17]\(13),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(4)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_26\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8B"
    )
        port map (
      I0 => \slv_reg1_reg[15]\(3),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^l_bus_in_s_reg[17]\(12),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(3)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_27\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg1_reg[15]\(2),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^l_bus_in_s_reg[17]\(11),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(2)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_28\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg1_reg[15]\(1),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^l_bus_in_s_reg[17]\(10),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(1)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_29\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg1_reg[15]\(0),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^l_bus_in_s_reg[17]\(9),
      O => \sdp_bl.ramb18_dp_bl.ram18_bl\(0)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg2_reg[15]\(7),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^d\(4),
      O => DIBDI(7)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B88B8B8B8B8B8B8B"
    )
        port map (
      I0 => \slv_reg2_reg[15]\(6),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^r_bus_in_s_reg[17]\(15),
      I3 => \^r_bus_in_s_reg[17]\(12),
      I4 => \^r_bus_in_s_reg[17]\(13),
      I5 => \^r_bus_in_s_reg[17]\(14),
      O => DIBDI(6)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8BB8B8B8"
    )
        port map (
      I0 => \slv_reg2_reg[15]\(5),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^r_bus_in_s_reg[17]\(14),
      I3 => \^r_bus_in_s_reg[17]\(13),
      I4 => \^r_bus_in_s_reg[17]\(12),
      O => DIBDI(5)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8BB8"
    )
        port map (
      I0 => \slv_reg2_reg[15]\(4),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^r_bus_in_s_reg[17]\(12),
      I3 => \^r_bus_in_s_reg[17]\(13),
      O => DIBDI(4)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8B"
    )
        port map (
      I0 => \slv_reg2_reg[15]\(3),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^r_bus_in_s_reg[17]\(12),
      O => DIBDI(3)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg2_reg[15]\(2),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^r_bus_in_s_reg[17]\(11),
      O => DIBDI(2)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_9__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg2_reg[15]\(1),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \^r_bus_in_s_reg[17]\(10),
      O => DIBDI(1)
    );
\state[0]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1D001D00FF1D1D00"
    )
        port map (
      I0 => \int_trigger_volt_s_reg[9]\(4),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \slv_reg11_reg[9]\(4),
      I3 => \^l_unsigned_data_prev_reg[9]\,
      I4 => \^l_unsigned_data_prev_reg[8]\,
      I5 => \slv_reg11_reg[8]\,
      O => \state[0]_i_14_n_0\
    );
\state[0]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A95000000009A95"
    )
        port map (
      I0 => \^l_unsigned_data_prev_reg[9]\,
      I1 => \slv_reg11_reg[9]\(4),
      I2 => \slv_reg5_reg[15]\(0),
      I3 => \int_trigger_volt_s_reg[9]\(4),
      I4 => \^l_unsigned_data_prev_reg[8]\,
      I5 => \slv_reg11_reg[8]\,
      O => \state[0]_i_15_n_0\
    );
\state[0]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"444444D4D4D444D4"
    )
        port map (
      I0 => \slv_reg11_reg[7]\,
      I1 => \^d\(4),
      I2 => \^d\(3),
      I3 => \int_trigger_volt_s_reg[9]\(2),
      I4 => \slv_reg5_reg[15]\(0),
      I5 => \slv_reg11_reg[9]\(2),
      O => \state[0]_i_19_n_0\
    );
\state[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"56F556A0"
    )
        port map (
      I0 => state(1),
      I1 => \processQ_reg[9]\(0),
      I2 => ready_sig_reg_0,
      I3 => state(0),
      I4 => sw(2),
      O => \state_reg[0]\
    );
\state[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8F808080"
    )
        port map (
      I0 => R_bus_g,
      I1 => CO(0),
      I2 => switch(0),
      I3 => L_bus_g,
      I4 => \L_unsigned_data_prev_reg[9]_0\(0),
      O => sw(2)
    );
\state[0]_i_20\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15403D54"
    )
        port map (
      I0 => \slv_reg11_reg[5]\,
      I1 => \^r_bus_in_s_reg[17]\(12),
      I2 => \^r_bus_in_s_reg[17]\(13),
      I3 => \^r_bus_in_s_reg[17]\(14),
      I4 => \slv_reg11_reg[4]\,
      O => \state[0]_i_20_n_0\
    );
\state[0]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111117171711171"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(12),
      I1 => \slv_reg11_reg[3]\,
      I2 => \^r_bus_in_s_reg[17]\(11),
      I3 => \int_trigger_volt_s_reg[9]\(1),
      I4 => \slv_reg5_reg[15]\(0),
      I5 => \slv_reg11_reg[9]\(1),
      O => \state[0]_i_21_n_0\
    );
\state[0]_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"222222B2B2B222B2"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(10),
      I1 => \slv_reg11_reg[1]\,
      I2 => \^r_bus_in_s_reg[17]\(9),
      I3 => \int_trigger_volt_s_reg[9]\(0),
      I4 => \slv_reg5_reg[15]\(0),
      I5 => \slv_reg11_reg[9]\(0),
      O => \state[0]_i_22_n_0\
    );
\state[0]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A95000000009A95"
    )
        port map (
      I0 => \^d\(4),
      I1 => \slv_reg11_reg[9]\(3),
      I2 => \slv_reg5_reg[15]\(0),
      I3 => \int_trigger_volt_s_reg[9]\(3),
      I4 => \^d\(3),
      I5 => \slv_reg11_reg[6]\,
      O => \state[0]_i_23_n_0\
    );
\state[0]_i_24\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \^r_bus_in_s_reg[17]\(14),
      I1 => \slv_reg11_reg[5]\,
      I2 => \^r_bus_in_s_reg[17]\(12),
      I3 => \^r_bus_in_s_reg[17]\(13),
      I4 => \slv_reg11_reg[4]\,
      O => \state[0]_i_24_n_0\
    );
\state[0]_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6066600006000666"
    )
        port map (
      I0 => \slv_reg11_reg[3]\,
      I1 => \^r_bus_in_s_reg[17]\(12),
      I2 => \slv_reg11_reg[9]\(1),
      I3 => \slv_reg5_reg[15]\(0),
      I4 => \int_trigger_volt_s_reg[9]\(1),
      I5 => \^r_bus_in_s_reg[17]\(11),
      O => \state[0]_i_25_n_0\
    );
\state[0]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9099900009000999"
    )
        port map (
      I0 => \slv_reg11_reg[1]\,
      I1 => \^r_bus_in_s_reg[17]\(10),
      I2 => \slv_reg11_reg[9]\(0),
      I3 => \slv_reg5_reg[15]\(0),
      I4 => \int_trigger_volt_s_reg[9]\(0),
      I5 => \^r_bus_in_s_reg[17]\(9),
      O => \state[0]_i_26_n_0\
    );
\state[0]_i_35\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"444444D4D4D444D4"
    )
        port map (
      I0 => \slv_reg11_reg[7]\,
      I1 => \^l_unsigned_data_prev_reg[7]\(4),
      I2 => \^l_unsigned_data_prev_reg[7]\(3),
      I3 => \int_trigger_volt_s_reg[9]\(2),
      I4 => \slv_reg5_reg[15]\(0),
      I5 => \slv_reg11_reg[9]\(2),
      O => \state[0]_i_35_n_0\
    );
\state[0]_i_36\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15403D54"
    )
        port map (
      I0 => \slv_reg11_reg[5]\,
      I1 => \^l_bus_in_s_reg[17]\(12),
      I2 => \^l_bus_in_s_reg[17]\(13),
      I3 => \^l_bus_in_s_reg[17]\(14),
      I4 => \slv_reg11_reg[4]\,
      O => \state[0]_i_36_n_0\
    );
\state[0]_i_37\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111117171711171"
    )
        port map (
      I0 => \^l_bus_in_s_reg[17]\(12),
      I1 => \slv_reg11_reg[3]\,
      I2 => \^l_bus_in_s_reg[17]\(11),
      I3 => \int_trigger_volt_s_reg[9]\(1),
      I4 => \slv_reg5_reg[15]\(0),
      I5 => \slv_reg11_reg[9]\(1),
      O => \state[0]_i_37_n_0\
    );
\state[0]_i_38\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"222222B2B2B222B2"
    )
        port map (
      I0 => \^l_bus_in_s_reg[17]\(10),
      I1 => \slv_reg11_reg[1]\,
      I2 => \^l_bus_in_s_reg[17]\(9),
      I3 => \int_trigger_volt_s_reg[9]\(0),
      I4 => \slv_reg5_reg[15]\(0),
      I5 => \slv_reg11_reg[9]\(0),
      O => \state[0]_i_38_n_0\
    );
\state[0]_i_39\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A95000000009A95"
    )
        port map (
      I0 => \^l_unsigned_data_prev_reg[7]\(4),
      I1 => \slv_reg11_reg[9]\(3),
      I2 => \slv_reg5_reg[15]\(0),
      I3 => \int_trigger_volt_s_reg[9]\(3),
      I4 => \^l_unsigned_data_prev_reg[7]\(3),
      I5 => \slv_reg11_reg[6]\,
      O => \state[0]_i_39_n_0\
    );
\state[0]_i_40\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \^l_bus_in_s_reg[17]\(14),
      I1 => \slv_reg11_reg[5]\,
      I2 => \^l_bus_in_s_reg[17]\(12),
      I3 => \^l_bus_in_s_reg[17]\(13),
      I4 => \slv_reg11_reg[4]\,
      O => \state[0]_i_40_n_0\
    );
\state[0]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6066600006000666"
    )
        port map (
      I0 => \slv_reg11_reg[3]\,
      I1 => \^l_bus_in_s_reg[17]\(12),
      I2 => \slv_reg11_reg[9]\(1),
      I3 => \slv_reg5_reg[15]\(0),
      I4 => \int_trigger_volt_s_reg[9]\(1),
      I5 => \^l_bus_in_s_reg[17]\(11),
      O => \state[0]_i_41_n_0\
    );
\state[0]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9099900009000999"
    )
        port map (
      I0 => \slv_reg11_reg[1]\,
      I1 => \^l_bus_in_s_reg[17]\(10),
      I2 => \slv_reg11_reg[9]\(0),
      I3 => \slv_reg5_reg[15]\(0),
      I4 => \int_trigger_volt_s_reg[9]\(0),
      I5 => \^l_bus_in_s_reg[17]\(9),
      O => \state[0]_i_42_n_0\
    );
\state[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1D001D00FF1D1D00"
    )
        port map (
      I0 => \int_trigger_volt_s_reg[9]\(4),
      I1 => \slv_reg5_reg[15]\(0),
      I2 => \slv_reg11_reg[9]\(4),
      I3 => \^r_unsigned_data_prev_reg[9]\,
      I4 => \^r_unsigned_data_prev_reg[8]\,
      I5 => \slv_reg11_reg[8]\,
      O => \state[0]_i_8_n_0\
    );
\state[0]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A95000000009A95"
    )
        port map (
      I0 => \^r_unsigned_data_prev_reg[9]\,
      I1 => \slv_reg11_reg[9]\(4),
      I2 => \slv_reg5_reg[15]\(0),
      I3 => \int_trigger_volt_s_reg[9]\(4),
      I4 => \^r_unsigned_data_prev_reg[8]\,
      I5 => \slv_reg11_reg[8]\,
      O => \state[0]_i_9_n_0\
    );
\state_reg[0]_i_13\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \state_reg[0]_i_13_n_0\,
      CO(2) => \state_reg[0]_i_13_n_1\,
      CO(1) => \state_reg[0]_i_13_n_2\,
      CO(0) => \state_reg[0]_i_13_n_3\,
      CYINIT => '1',
      DI(3) => \state[0]_i_35_n_0\,
      DI(2) => \state[0]_i_36_n_0\,
      DI(1) => \state[0]_i_37_n_0\,
      DI(0) => \state[0]_i_38_n_0\,
      O(3 downto 0) => \NLW_state_reg[0]_i_13_O_UNCONNECTED\(3 downto 0),
      S(3) => \state[0]_i_39_n_0\,
      S(2) => \state[0]_i_40_n_0\,
      S(1) => \state[0]_i_41_n_0\,
      S(0) => \state[0]_i_42_n_0\
    );
\state_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \state_reg[0]_i_7_n_0\,
      CO(3 downto 1) => \NLW_state_reg[0]_i_3_CO_UNCONNECTED\(3 downto 1),
      CO(0) => R_bus_g,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \state[0]_i_8_n_0\,
      O(3 downto 0) => \NLW_state_reg[0]_i_3_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \state[0]_i_9_n_0\
    );
\state_reg[0]_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => \state_reg[0]_i_13_n_0\,
      CO(3 downto 1) => \NLW_state_reg[0]_i_5_CO_UNCONNECTED\(3 downto 1),
      CO(0) => L_bus_g,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \state[0]_i_14_n_0\,
      O(3 downto 0) => \NLW_state_reg[0]_i_5_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \state[0]_i_15_n_0\
    );
\state_reg[0]_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \state_reg[0]_i_7_n_0\,
      CO(2) => \state_reg[0]_i_7_n_1\,
      CO(1) => \state_reg[0]_i_7_n_2\,
      CO(0) => \state_reg[0]_i_7_n_3\,
      CYINIT => '1',
      DI(3) => \state[0]_i_19_n_0\,
      DI(2) => \state[0]_i_20_n_0\,
      DI(1) => \state[0]_i_21_n_0\,
      DI(0) => \state[0]_i_22_n_0\,
      O(3 downto 0) => \NLW_state_reg[0]_i_7_O_UNCONNECTED\(3 downto 0),
      S(3) => \state[0]_i_23_n_0\,
      S(2) => \state[0]_i_24_n_0\,
      S(1) => \state[0]_i_25_n_0\,
      S(0) => \state[0]_i_26_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    state : out STD_LOGIC_VECTOR ( 1 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    WREN : out STD_LOGIC;
    sw : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_n : in STD_LOGIC;
    ready_sig_reg : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg5_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_n_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC;
    \state_reg[1]_0\ : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm is
  signal \^state\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \state[1]_i_1__0_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \processQ[9]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \processQ[9]_i_2\ : label is "soft_lutpair0";
begin
  state(1 downto 0) <= \^state\(1 downto 0);
\processQ[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"51FF"
    )
        port map (
      I0 => \^state\(1),
      I1 => \^state\(0),
      I2 => sw(0),
      I3 => reset_n,
      O => SR(0)
    );
\processQ[9]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => sw(0),
      I1 => \^state\(0),
      I2 => \^state\(1),
      O => E(0)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B88"
    )
        port map (
      I0 => Q(0),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \^state\(1),
      I3 => \^state\(0),
      O => WREN
    );
\state[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB02"
    )
        port map (
      I0 => \^state\(1),
      I1 => sw(0),
      I2 => ready_sig_reg,
      I3 => \^state\(0),
      O => \state[1]_i_1__0_n_0\
    );
\state_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \state_reg[1]_0\,
      Q => \^state\(0),
      R => reset_n_0(0)
    );
\state_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \state[1]_i_1__0_n_0\,
      Q => \^state\(1),
      R => reset_n_0(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample_counter is
  port (
    \Q_reg[2]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    ADDRBWRADDR : out STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg0_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \state_reg[1]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample_counter;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample_counter is
  signal \plusOp__0\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \processQ[9]_i_4_n_0\ : STD_LOGIC;
  signal write_cntr : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Q[2]_i_2\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \processQ[1]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \processQ[2]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \processQ[3]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \processQ[4]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \processQ[7]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \processQ[8]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \processQ[9]_i_3\ : label is "soft_lutpair35";
begin
\Q[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => write_cntr(9),
      I1 => write_cntr(7),
      I2 => \processQ[9]_i_4_n_0\,
      I3 => write_cntr(6),
      I4 => write_cntr(8),
      O => \Q_reg[2]\(0)
    );
\processQ[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_cntr(0),
      O => \plusOp__0\(0)
    );
\processQ[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_cntr(1),
      I1 => write_cntr(0),
      O => \plusOp__0\(1)
    );
\processQ[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => write_cntr(2),
      I1 => write_cntr(0),
      I2 => write_cntr(1),
      O => \plusOp__0\(2)
    );
\processQ[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => write_cntr(3),
      I1 => write_cntr(1),
      I2 => write_cntr(0),
      I3 => write_cntr(2),
      O => \plusOp__0\(3)
    );
\processQ[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => write_cntr(4),
      I1 => write_cntr(2),
      I2 => write_cntr(0),
      I3 => write_cntr(1),
      I4 => write_cntr(3),
      O => \plusOp__0\(4)
    );
\processQ[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => write_cntr(5),
      I1 => write_cntr(3),
      I2 => write_cntr(1),
      I3 => write_cntr(0),
      I4 => write_cntr(2),
      I5 => write_cntr(4),
      O => \plusOp__0\(5)
    );
\processQ[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_cntr(6),
      I1 => \processQ[9]_i_4_n_0\,
      O => \plusOp__0\(6)
    );
\processQ[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => write_cntr(7),
      I1 => \processQ[9]_i_4_n_0\,
      I2 => write_cntr(6),
      O => \plusOp__0\(7)
    );
\processQ[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => write_cntr(8),
      I1 => write_cntr(6),
      I2 => \processQ[9]_i_4_n_0\,
      I3 => write_cntr(7),
      O => \plusOp__0\(8)
    );
\processQ[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => write_cntr(9),
      I1 => write_cntr(7),
      I2 => \processQ[9]_i_4_n_0\,
      I3 => write_cntr(6),
      I4 => write_cntr(8),
      O => \plusOp__0\(9)
    );
\processQ[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => write_cntr(5),
      I1 => write_cntr(3),
      I2 => write_cntr(1),
      I3 => write_cntr(0),
      I4 => write_cntr(2),
      I5 => write_cntr(4),
      O => \processQ[9]_i_4_n_0\
    );
\processQ_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \plusOp__0\(0),
      Q => write_cntr(0),
      R => \state_reg[1]\(0)
    );
\processQ_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \plusOp__0\(1),
      Q => write_cntr(1),
      R => \state_reg[1]\(0)
    );
\processQ_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \plusOp__0\(2),
      Q => write_cntr(2),
      R => \state_reg[1]\(0)
    );
\processQ_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \plusOp__0\(3),
      Q => write_cntr(3),
      R => \state_reg[1]\(0)
    );
\processQ_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \plusOp__0\(4),
      Q => write_cntr(4),
      R => \state_reg[1]\(0)
    );
\processQ_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \plusOp__0\(5),
      Q => write_cntr(5),
      R => \state_reg[1]\(0)
    );
\processQ_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \plusOp__0\(6),
      Q => write_cntr(6),
      R => \state_reg[1]\(0)
    );
\processQ_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \plusOp__0\(7),
      Q => write_cntr(7),
      R => \state_reg[1]\(0)
    );
\processQ_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \plusOp__0\(8),
      Q => write_cntr(8),
      R => \state_reg[1]\(0)
    );
\processQ_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \plusOp__0\(9),
      Q => write_cntr(9),
      R => \state_reg[1]\(0)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg0_reg[9]\(9),
      I1 => Q(0),
      I2 => write_cntr(9),
      O => ADDRBWRADDR(9)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg0_reg[9]\(8),
      I1 => Q(0),
      I2 => write_cntr(8),
      O => ADDRBWRADDR(8)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg0_reg[9]\(7),
      I1 => Q(0),
      I2 => write_cntr(7),
      O => ADDRBWRADDR(7)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg0_reg[9]\(6),
      I1 => Q(0),
      I2 => write_cntr(6),
      O => ADDRBWRADDR(6)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg0_reg[9]\(5),
      I1 => Q(0),
      I2 => write_cntr(5),
      O => ADDRBWRADDR(5)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg0_reg[9]\(4),
      I1 => Q(0),
      I2 => write_cntr(4),
      O => ADDRBWRADDR(4)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg0_reg[9]\(3),
      I1 => Q(0),
      I2 => write_cntr(3),
      O => ADDRBWRADDR(3)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg0_reg[9]\(2),
      I1 => Q(0),
      I2 => write_cntr(2),
      O => ADDRBWRADDR(2)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg0_reg[9]\(1),
      I1 => Q(0),
      I2 => write_cntr(1),
      O => ADDRBWRADDR(1)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg0_reg[9]\(0),
      I1 => Q(0),
      I2 => write_cntr(0),
      O => ADDRBWRADDR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_scopeFace is
  port (
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_3\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_4\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[0]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_5\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]\ : out STD_LOGIC;
    \encoded_reg[8]_6\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_0\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_1\ : out STD_LOGIC;
    \encoded_reg[8]_7\ : out STD_LOGIC;
    \encoded_reg[8]_8\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_2\ : out STD_LOGIC;
    \encoded_reg[8]_9\ : out STD_LOGIC;
    \encoded_reg[8]_10\ : out STD_LOGIC;
    \encoded_reg[8]_11\ : out STD_LOGIC;
    \encoded_reg[8]_12\ : out STD_LOGIC;
    \encoded_reg[8]_13\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_3\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_4\ : out STD_LOGIC;
    \encoded_reg[8]_14\ : out STD_LOGIC;
    \encoded_reg[8]_15\ : out STD_LOGIC;
    \encoded_reg[0]_2\ : out STD_LOGIC;
    \encoded_reg[8]_16\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_5\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_6\ : out STD_LOGIC;
    \encoded_reg[8]_17\ : out STD_LOGIC;
    \encoded_reg[8]_18\ : out STD_LOGIC;
    \encoded_reg[8]_19\ : out STD_LOGIC;
    \encoded_reg[8]_20\ : out STD_LOGIC;
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[7]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_4\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[7]_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_5\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_6\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg11_reg[9]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_6\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_7\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_8\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[7]_7\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_8\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_9\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_10\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[7]_9\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_10\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_11\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_12\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[7]_11\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_12\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_13\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_14\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[7]_13\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_14\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_15\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_16\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[7]_15\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_16\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_17\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_18\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg10_reg[9]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_17\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_18\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_19\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_20\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[7]_19\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[7]_20\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[9]_21\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_22\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg10_reg[5]\ : in STD_LOGIC;
    \slv_reg10_reg[3]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg10_reg[9]_0\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg5_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \int_trigger_volt_s_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg11_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg11_reg[5]\ : in STD_LOGIC;
    \slv_reg11_reg[3]\ : in STD_LOGIC;
    \slv_reg11_reg[1]\ : in STD_LOGIC;
    \slv_reg11_reg[0]\ : in STD_LOGIC;
    \slv_reg11_reg[2]\ : in STD_LOGIC;
    \slv_reg11_reg[4]\ : in STD_LOGIC;
    \processQ_reg[1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_scopeFace;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_scopeFace is
  signal \^encoded_reg[8]_11\ : STD_LOGIC;
  signal \^encoded_reg[8]_18\ : STD_LOGIC;
  signal \^encoded_reg[8]_6\ : STD_LOGIC;
  signal \^encoded_reg[8]_8\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_0\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_1\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_2\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_3\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_4\ : STD_LOGIC;
  signal \pixel_color3_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color3_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color3_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \pixel_color3_inferred__1/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color3_inferred__1/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color3_inferred__1/i__carry_n_3\ : STD_LOGIC;
  signal pixel_color424_in : STD_LOGIC;
  signal pixel_color4_carry_n_0 : STD_LOGIC;
  signal pixel_color4_carry_n_1 : STD_LOGIC;
  signal pixel_color4_carry_n_2 : STD_LOGIC;
  signal pixel_color4_carry_n_3 : STD_LOGIC;
  signal \pixel_color4_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \pixel_color4_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color4_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color4_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal pixel_color523_in : STD_LOGIC;
  signal pixel_color532_in : STD_LOGIC;
  signal pixel_color5_carry_n_0 : STD_LOGIC;
  signal pixel_color5_carry_n_1 : STD_LOGIC;
  signal pixel_color5_carry_n_2 : STD_LOGIC;
  signal pixel_color5_carry_n_3 : STD_LOGIC;
  signal \pixel_color5_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \pixel_color5_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color5_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color5_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \pixel_color5_inferred__1/i__carry_n_0\ : STD_LOGIC;
  signal \pixel_color5_inferred__1/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color5_inferred__1/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color5_inferred__1/i__carry_n_3\ : STD_LOGIC;
  signal \pixel_color5_inferred__2/i__carry_n_0\ : STD_LOGIC;
  signal \pixel_color5_inferred__2/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color5_inferred__2/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color5_inferred__2/i__carry_n_3\ : STD_LOGIC;
  signal \pixel_color5_inferred__3/i__carry_n_0\ : STD_LOGIC;
  signal \pixel_color5_inferred__3/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color5_inferred__3/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color5_inferred__3/i__carry_n_3\ : STD_LOGIC;
  signal \pixel_color5_inferred__4/i__carry_n_0\ : STD_LOGIC;
  signal \pixel_color5_inferred__4/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color5_inferred__4/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color5_inferred__4/i__carry_n_3\ : STD_LOGIC;
  signal pixel_color631_in : STD_LOGIC;
  signal \pixel_color6_inferred__1/i__carry_n_0\ : STD_LOGIC;
  signal \pixel_color6_inferred__1/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color6_inferred__1/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color6_inferred__1/i__carry_n_3\ : STD_LOGIC;
  signal \pixel_color6_inferred__2/i__carry_n_0\ : STD_LOGIC;
  signal \pixel_color6_inferred__2/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color6_inferred__2/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color6_inferred__2/i__carry_n_3\ : STD_LOGIC;
  signal \pixel_color6_inferred__3/i__carry_n_0\ : STD_LOGIC;
  signal \pixel_color6_inferred__3/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color6_inferred__3/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color6_inferred__3/i__carry_n_3\ : STD_LOGIC;
  signal \pixel_color6_inferred__4/i__carry_n_0\ : STD_LOGIC;
  signal \pixel_color6_inferred__4/i__carry_n_1\ : STD_LOGIC;
  signal \pixel_color6_inferred__4/i__carry_n_2\ : STD_LOGIC;
  signal \pixel_color6_inferred__4/i__carry_n_3\ : STD_LOGIC;
  signal \NLW_pixel_color3_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color3_inferred__1/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pixel_color4_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color4_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color4_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color4_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color4_inferred__0/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color4_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pixel_color5_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color5_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_inferred__0/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color5_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_inferred__1/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_inferred__1/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color5_inferred__1/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_inferred__2/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_inferred__2/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color5_inferred__2/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_inferred__3/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_inferred__3/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color5_inferred__3/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_inferred__4/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color5_inferred__4/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color5_inferred__4/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color6_inferred__1/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color6_inferred__1/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color6_inferred__1/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color6_inferred__2/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color6_inferred__2/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color6_inferred__2/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color6_inferred__3/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color6_inferred__3/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color6_inferred__3/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color6_inferred__4/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pixel_color6_inferred__4/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_pixel_color6_inferred__4/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \i__carry__0_i_3__4\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \i__carry__0_i_5\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \i__carry_i_10__3\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \i__carry_i_10__4\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \i__carry_i_11__0\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \i__carry_i_12\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \i__carry_i_13\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \i__carry_i_15\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \i__carry_i_16\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \i__carry_i_9\ : label is "soft_lutpair59";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of \pixel_color3_inferred__0/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color3_inferred__1/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of pixel_color4_carry : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color4_carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color4_inferred__0/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color4_inferred__0/i__carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of pixel_color5_carry : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_inferred__0/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_inferred__0/i__carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_inferred__1/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_inferred__1/i__carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_inferred__2/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_inferred__2/i__carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_inferred__3/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_inferred__3/i__carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_inferred__4/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color5_inferred__4/i__carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color6_inferred__1/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color6_inferred__1/i__carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color6_inferred__2/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color6_inferred__2/i__carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color6_inferred__3/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color6_inferred__3/i__carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color6_inferred__4/i__carry\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \pixel_color6_inferred__4/i__carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
begin
  \encoded_reg[8]_11\ <= \^encoded_reg[8]_11\;
  \encoded_reg[8]_18\ <= \^encoded_reg[8]_18\;
  \encoded_reg[8]_6\ <= \^encoded_reg[8]_6\;
  \encoded_reg[8]_8\ <= \^encoded_reg[8]_8\;
  \int_trigger_time_s_reg[0]\ <= \^int_trigger_time_s_reg[0]\;
  \int_trigger_time_s_reg[0]_0\ <= \^int_trigger_time_s_reg[0]_0\;
  \int_trigger_time_s_reg[0]_1\ <= \^int_trigger_time_s_reg[0]_1\;
  \int_trigger_time_s_reg[0]_2\ <= \^int_trigger_time_s_reg[0]_2\;
  \int_trigger_time_s_reg[0]_3\ <= \^int_trigger_time_s_reg[0]_3\;
  \int_trigger_time_s_reg[0]_4\ <= \^int_trigger_time_s_reg[0]_4\;
\dc_bias[3]_i_18__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000088880000F000"
    )
        port map (
      I0 => pixel_color424_in,
      I1 => pixel_color523_in,
      I2 => pixel_color532_in,
      I3 => pixel_color631_in,
      I4 => \processQ_reg[1]\(0),
      I5 => \processQ_reg[1]\(1),
      O => \encoded_reg[8]_20\
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88800000"
    )
        port map (
      I0 => \^int_trigger_time_s_reg[0]\,
      I1 => \slv_reg10_reg[5]\,
      I2 => \^encoded_reg[8]_6\,
      I3 => \^int_trigger_time_s_reg[0]_0\,
      I4 => \^int_trigger_time_s_reg[0]_1\,
      O => \encoded_reg[8]_5\
    );
\i__carry__0_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A888888800000000"
    )
        port map (
      I0 => \^int_trigger_time_s_reg[0]_1\,
      I1 => \^int_trigger_time_s_reg[0]_0\,
      I2 => \^encoded_reg[8]_8\,
      I3 => \^int_trigger_time_s_reg[0]_2\,
      I4 => \slv_reg10_reg[3]\,
      I5 => \slv_reg10_reg[5]\,
      O => \encoded_reg[8]_7\
    );
\i__carry__0_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8888800000000"
    )
        port map (
      I0 => \^int_trigger_time_s_reg[0]_1\,
      I1 => \^int_trigger_time_s_reg[0]_0\,
      I2 => \^int_trigger_time_s_reg[0]_2\,
      I3 => \^encoded_reg[8]_8\,
      I4 => \slv_reg10_reg[3]\,
      I5 => \slv_reg10_reg[5]\,
      O => \encoded_reg[8]_9\
    );
\i__carry__0_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C000A0A0C0000000"
    )
        port map (
      I0 => Q(7),
      I1 => \slv_reg10_reg[9]_0\(7),
      I2 => \^encoded_reg[8]_11\,
      I3 => \slv_reg10_reg[9]_0\(6),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => Q(6),
      O => \encoded_reg[8]_10\
    );
\i__carry__0_i_3__4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\(8),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => Q(8),
      O => \int_trigger_time_s_reg[0]_6\
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\(9),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => Q(9),
      O => \int_trigger_time_s_reg[0]_5\
    );
\i__carry_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAA8A80"
    )
        port map (
      I0 => \slv_reg10_reg[3]\,
      I1 => \slv_reg10_reg[9]_0\(0),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => Q(0),
      I4 => \^int_trigger_time_s_reg[0]_4\,
      I5 => \^int_trigger_time_s_reg[0]_2\,
      O => \^encoded_reg[8]_6\
    );
\i__carry_i_10__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC0AAAACCC0A0A0"
    )
        port map (
      I0 => Q(3),
      I1 => \slv_reg10_reg[9]_0\(3),
      I2 => \^encoded_reg[8]_8\,
      I3 => \slv_reg10_reg[9]_0\(2),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => Q(2),
      O => \encoded_reg[8]_15\
    );
\i__carry_i_10__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA000A0"
    )
        port map (
      I0 => Q(1),
      I1 => \slv_reg10_reg[9]_0\(1),
      I2 => Q(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \slv_reg10_reg[9]_0\(0),
      O => \^encoded_reg[8]_8\
    );
\i__carry_i_10__4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00053305"
    )
        port map (
      I0 => Q(1),
      I1 => \slv_reg10_reg[9]_0\(1),
      I2 => Q(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \slv_reg10_reg[9]_0\(0),
      O => \encoded_reg[8]_16\
    );
\i__carry_i_11__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA000A0"
    )
        port map (
      I0 => Q(2),
      I1 => \slv_reg10_reg[9]_0\(2),
      I2 => Q(3),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \slv_reg10_reg[9]_0\(3),
      O => \encoded_reg[0]_2\
    );
\i__carry_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\(0),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => Q(0),
      O => \^int_trigger_time_s_reg[0]_3\
    );
\i__carry_i_12__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\(7),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => Q(7),
      O => \^int_trigger_time_s_reg[0]\
    );
\i__carry_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\(4),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => Q(4),
      O => \^int_trigger_time_s_reg[0]_0\
    );
\i__carry_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\(1),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => Q(1),
      O => \^int_trigger_time_s_reg[0]_4\
    );
\i__carry_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\(2),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => Q(2),
      O => \^int_trigger_time_s_reg[0]_2\
    );
\i__carry_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \slv_reg10_reg[9]_0\(6),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => Q(6),
      O => \^int_trigger_time_s_reg[0]_1\
    );
\i__carry_i_9__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC0AAAACCC0A0A0"
    )
        port map (
      I0 => Q(5),
      I1 => \slv_reg10_reg[9]_0\(5),
      I2 => \^encoded_reg[8]_6\,
      I3 => \slv_reg10_reg[9]_0\(4),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => Q(4),
      O => \encoded_reg[8]_12\
    );
\i__carry_i_9__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA80000000"
    )
        port map (
      I0 => \slv_reg10_reg[5]\,
      I1 => \slv_reg10_reg[3]\,
      I2 => \^int_trigger_time_s_reg[0]_2\,
      I3 => \^int_trigger_time_s_reg[0]_3\,
      I4 => \^int_trigger_time_s_reg[0]_4\,
      I5 => \^int_trigger_time_s_reg[0]_0\,
      O => \encoded_reg[8]_13\
    );
\i__carry_i_9__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA88888000"
    )
        port map (
      I0 => \slv_reg10_reg[5]\,
      I1 => \slv_reg10_reg[3]\,
      I2 => \^int_trigger_time_s_reg[0]_4\,
      I3 => \^int_trigger_time_s_reg[0]_3\,
      I4 => \^int_trigger_time_s_reg[0]_2\,
      I5 => \^int_trigger_time_s_reg[0]_0\,
      O => \encoded_reg[8]_14\
    );
\i__carry_i_9__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA80808000"
    )
        port map (
      I0 => \slv_reg10_reg[5]\,
      I1 => \slv_reg10_reg[3]\,
      I2 => \^int_trigger_time_s_reg[0]_2\,
      I3 => \^int_trigger_time_s_reg[0]_3\,
      I4 => \^int_trigger_time_s_reg[0]_4\,
      I5 => \^int_trigger_time_s_reg[0]_0\,
      O => \^encoded_reg[8]_11\
    );
\pixel_color3_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[8]_2\(0),
      CO(2) => \pixel_color3_inferred__0/i__carry_n_1\,
      CO(1) => \pixel_color3_inferred__0/i__carry_n_2\,
      CO(0) => \pixel_color3_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_pixel_color3_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \slv_reg11_reg[9]\(3 downto 0)
    );
\pixel_color3_inferred__1/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \encoded_reg[0]_1\(0),
      CO(2) => \pixel_color3_inferred__1/i__carry_n_1\,
      CO(1) => \pixel_color3_inferred__1/i__carry_n_2\,
      CO(0) => \pixel_color3_inferred__1/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_pixel_color3_inferred__1/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \slv_reg10_reg[9]\(3 downto 0)
    );
pixel_color4_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => pixel_color4_carry_n_0,
      CO(2) => pixel_color4_carry_n_1,
      CO(1) => pixel_color4_carry_n_2,
      CO(0) => pixel_color4_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]_3\(3 downto 0),
      O(3 downto 0) => NLW_pixel_color4_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_4\(3 downto 0)
    );
\pixel_color4_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => pixel_color4_carry_n_0,
      CO(3 downto 1) => \NLW_pixel_color4_carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \encoded_reg[8]_1\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_5\(0),
      O(3 downto 0) => \NLW_pixel_color4_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_6\(0)
    );
\pixel_color4_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C000A0A0C0000000"
    )
        port map (
      I0 => \int_trigger_volt_s_reg[7]\(3),
      I1 => \slv_reg11_reg[7]\(3),
      I2 => \^encoded_reg[8]_18\,
      I3 => \slv_reg11_reg[7]\(2),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => \int_trigger_volt_s_reg[7]\(2),
      O => \encoded_reg[8]_17\
    );
pixel_color4_carry_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0300035533553355"
    )
        port map (
      I0 => \int_trigger_volt_s_reg[7]\(1),
      I1 => \slv_reg11_reg[7]\(1),
      I2 => \slv_reg11_reg[7]\(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \int_trigger_volt_s_reg[7]\(0),
      I5 => \slv_reg11_reg[1]\,
      O => \encoded_reg[8]_19\
    );
pixel_color4_carry_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA88888000"
    )
        port map (
      I0 => \slv_reg11_reg[5]\,
      I1 => \slv_reg11_reg[3]\,
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[0]\,
      I4 => \slv_reg11_reg[2]\,
      I5 => \slv_reg11_reg[4]\,
      O => \^encoded_reg[8]_18\
    );
\pixel_color4_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_color4_inferred__0/i__carry_n_0\,
      CO(2) => \pixel_color4_inferred__0/i__carry_n_1\,
      CO(1) => \pixel_color4_inferred__0/i__carry_n_2\,
      CO(0) => \pixel_color4_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]_15\(3 downto 0),
      O(3 downto 0) => \NLW_pixel_color4_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_16\(3 downto 0)
    );
\pixel_color4_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_color4_inferred__0/i__carry_n_0\,
      CO(3 downto 1) => \NLW_pixel_color4_inferred__0/i__carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => pixel_color424_in,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_17\(0),
      O(3 downto 0) => \NLW_pixel_color4_inferred__0/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_18\(0)
    );
pixel_color5_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => pixel_color5_carry_n_0,
      CO(2) => pixel_color5_carry_n_1,
      CO(1) => pixel_color5_carry_n_2,
      CO(0) => pixel_color5_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]\(3 downto 0),
      O(3 downto 0) => NLW_pixel_color5_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_0\(3 downto 0)
    );
\pixel_color5_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => pixel_color5_carry_n_0,
      CO(3 downto 1) => \NLW_pixel_color5_carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \encoded_reg[8]\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_1\(0),
      O(3 downto 0) => \NLW_pixel_color5_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_2\(0)
    );
\pixel_color5_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_color5_inferred__0/i__carry_n_0\,
      CO(2) => \pixel_color5_inferred__0/i__carry_n_1\,
      CO(1) => \pixel_color5_inferred__0/i__carry_n_2\,
      CO(0) => \pixel_color5_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]_1\(3 downto 0),
      O(3 downto 0) => \NLW_pixel_color5_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_2\(3 downto 0)
    );
\pixel_color5_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_color5_inferred__0/i__carry_n_0\,
      CO(3 downto 1) => \NLW_pixel_color5_inferred__0/i__carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \encoded_reg[8]_0\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_3\(0),
      O(3 downto 0) => \NLW_pixel_color5_inferred__0/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_4\(0)
    );
\pixel_color5_inferred__1/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_color5_inferred__1/i__carry_n_0\,
      CO(2) => \pixel_color5_inferred__1/i__carry_n_1\,
      CO(1) => \pixel_color5_inferred__1/i__carry_n_2\,
      CO(0) => \pixel_color5_inferred__1/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]_7\(3 downto 0),
      O(3 downto 0) => \NLW_pixel_color5_inferred__1/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_8\(3 downto 0)
    );
\pixel_color5_inferred__1/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_color5_inferred__1/i__carry_n_0\,
      CO(3 downto 1) => \NLW_pixel_color5_inferred__1/i__carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \encoded_reg[8]_4\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_9\(0),
      O(3 downto 0) => \NLW_pixel_color5_inferred__1/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_10\(0)
    );
\pixel_color5_inferred__2/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_color5_inferred__2/i__carry_n_0\,
      CO(2) => \pixel_color5_inferred__2/i__carry_n_1\,
      CO(1) => \pixel_color5_inferred__2/i__carry_n_2\,
      CO(0) => \pixel_color5_inferred__2/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]_11\(3 downto 0),
      O(3 downto 0) => \NLW_pixel_color5_inferred__2/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_12\(3 downto 0)
    );
\pixel_color5_inferred__2/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_color5_inferred__2/i__carry_n_0\,
      CO(3 downto 1) => \NLW_pixel_color5_inferred__2/i__carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \encoded_reg[0]_0\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_13\(0),
      O(3 downto 0) => \NLW_pixel_color5_inferred__2/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_14\(0)
    );
\pixel_color5_inferred__3/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_color5_inferred__3/i__carry_n_0\,
      CO(2) => \pixel_color5_inferred__3/i__carry_n_1\,
      CO(1) => \pixel_color5_inferred__3/i__carry_n_2\,
      CO(0) => \pixel_color5_inferred__3/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]_13\(3 downto 0),
      O(3 downto 0) => \NLW_pixel_color5_inferred__3/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_14\(3 downto 0)
    );
\pixel_color5_inferred__3/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_color5_inferred__3/i__carry_n_0\,
      CO(3 downto 1) => \NLW_pixel_color5_inferred__3/i__carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => pixel_color523_in,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_15\(0),
      O(3 downto 0) => \NLW_pixel_color5_inferred__3/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_16\(0)
    );
\pixel_color5_inferred__4/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_color5_inferred__4/i__carry_n_0\,
      CO(2) => \pixel_color5_inferred__4/i__carry_n_1\,
      CO(1) => \pixel_color5_inferred__4/i__carry_n_2\,
      CO(0) => \pixel_color5_inferred__4/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]_19\(3 downto 0),
      O(3 downto 0) => \NLW_pixel_color5_inferred__4/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_20\(3 downto 0)
    );
\pixel_color5_inferred__4/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_color5_inferred__4/i__carry_n_0\,
      CO(3 downto 1) => \NLW_pixel_color5_inferred__4/i__carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => pixel_color532_in,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_21\(0),
      O(3 downto 0) => \NLW_pixel_color5_inferred__4/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_22\(0)
    );
\pixel_color6_inferred__1/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_color6_inferred__1/i__carry_n_0\,
      CO(2) => \pixel_color6_inferred__1/i__carry_n_1\,
      CO(1) => \pixel_color6_inferred__1/i__carry_n_2\,
      CO(0) => \pixel_color6_inferred__1/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => DI(3 downto 0),
      O(3 downto 0) => \NLW_pixel_color6_inferred__1/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\pixel_color6_inferred__1/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_color6_inferred__1/i__carry_n_0\,
      CO(3 downto 1) => \NLW_pixel_color6_inferred__1/i__carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => CO(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]\(0),
      O(3 downto 0) => \NLW_pixel_color6_inferred__1/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_0\(0)
    );
\pixel_color6_inferred__2/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_color6_inferred__2/i__carry_n_0\,
      CO(2) => \pixel_color6_inferred__2/i__carry_n_1\,
      CO(1) => \pixel_color6_inferred__2/i__carry_n_2\,
      CO(0) => \pixel_color6_inferred__2/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]_5\(3 downto 0),
      O(3 downto 0) => \NLW_pixel_color6_inferred__2/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_6\(3 downto 0)
    );
\pixel_color6_inferred__2/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_color6_inferred__2/i__carry_n_0\,
      CO(3 downto 1) => \NLW_pixel_color6_inferred__2/i__carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \encoded_reg[8]_3\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_7\(0),
      O(3 downto 0) => \NLW_pixel_color6_inferred__2/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_8\(0)
    );
\pixel_color6_inferred__3/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_color6_inferred__3/i__carry_n_0\,
      CO(2) => \pixel_color6_inferred__3/i__carry_n_1\,
      CO(1) => \pixel_color6_inferred__3/i__carry_n_2\,
      CO(0) => \pixel_color6_inferred__3/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]_9\(3 downto 0),
      O(3 downto 0) => \NLW_pixel_color6_inferred__3/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_10\(3 downto 0)
    );
\pixel_color6_inferred__3/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_color6_inferred__3/i__carry_n_0\,
      CO(3 downto 1) => \NLW_pixel_color6_inferred__3/i__carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \encoded_reg[0]\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_11\(0),
      O(3 downto 0) => \NLW_pixel_color6_inferred__3/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_12\(0)
    );
\pixel_color6_inferred__4/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_color6_inferred__4/i__carry_n_0\,
      CO(2) => \pixel_color6_inferred__4/i__carry_n_1\,
      CO(1) => \pixel_color6_inferred__4/i__carry_n_2\,
      CO(0) => \pixel_color6_inferred__4/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \processQ_reg[7]_17\(3 downto 0),
      O(3 downto 0) => \NLW_pixel_color6_inferred__4/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \processQ_reg[7]_18\(3 downto 0)
    );
\pixel_color6_inferred__4/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_color6_inferred__4/i__carry_n_0\,
      CO(3 downto 1) => \NLW_pixel_color6_inferred__4/i__carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => pixel_color631_in,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \processQ_reg[9]_19\(0),
      O(3 downto 0) => \NLW_pixel_color6_inferred__4/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \processQ_reg[9]_20\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO is
  port (
    DOADO : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \encoded_reg[8]\ : out STD_LOGIC;
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC;
    WREN : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 9 downto 0 );
    ADDRBWRADDR : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg1_reg[15]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 6 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[8]\ : in STD_LOGIC;
    S : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO is
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^doado\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal L_out_bram : STD_LOGIC_VECTOR ( 9 downto 3 );
  signal \dc_bias[3]_i_15_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_16_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_17_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_18__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_6__1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_7__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_8_n_0\ : STD_LOGIC;
  signal \dc_bias_reg[3]_i_3_n_1\ : STD_LOGIC;
  signal \dc_bias_reg[3]_i_3_n_2\ : STD_LOGIC;
  signal \dc_bias_reg[3]_i_3_n_3\ : STD_LOGIC;
  signal \NLW_dc_bias_reg[3]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 10 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of \dc_bias_reg[3]_i_3\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of \sdp_bl.ramb18_dp_bl.ram18_bl\ : label is "PRIMITIVE";
begin
  CO(0) <= \^co\(0);
  DOADO(2 downto 0) <= \^doado\(2 downto 0);
\dc_bias[3]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888800000000000"
    )
        port map (
      I0 => L_out_bram(7),
      I1 => L_out_bram(5),
      I2 => \^doado\(2),
      I3 => L_out_bram(3),
      I4 => L_out_bram(4),
      I5 => L_out_bram(6),
      O => \dc_bias[3]_i_15_n_0\
    );
\dc_bias[3]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8880000"
    )
        port map (
      I0 => L_out_bram(6),
      I1 => L_out_bram(4),
      I2 => L_out_bram(3),
      I3 => \^doado\(2),
      I4 => L_out_bram(5),
      O => \dc_bias[3]_i_16_n_0\
    );
\dc_bias[3]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999955566666AAA"
    )
        port map (
      I0 => Q(3),
      I1 => L_out_bram(5),
      I2 => \^doado\(2),
      I3 => L_out_bram(3),
      I4 => L_out_bram(4),
      I5 => L_out_bram(6),
      O => \dc_bias[3]_i_17_n_0\
    );
\dc_bias[3]_i_18__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BDDEE77B"
    )
        port map (
      I0 => Q(0),
      I1 => L_out_bram(4),
      I2 => L_out_bram(3),
      I3 => \^doado\(2),
      I4 => Q(1),
      O => \dc_bias[3]_i_18__0_n_0\
    );
\dc_bias[3]_i_6__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6A95"
    )
        port map (
      I0 => Q(6),
      I1 => L_out_bram(8),
      I2 => \dc_bias[3]_i_15_n_0\,
      I3 => L_out_bram(9),
      O => \dc_bias[3]_i_6__1_n_0\
    );
\dc_bias[3]_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000006909009"
    )
        port map (
      I0 => L_out_bram(8),
      I1 => Q(5),
      I2 => Q(4),
      I3 => \dc_bias[3]_i_16_n_0\,
      I4 => L_out_bram(7),
      I5 => \dc_bias[3]_i_17_n_0\,
      O => \dc_bias[3]_i_7__0_n_0\
    );
\dc_bias[3]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000556AAA95"
    )
        port map (
      I0 => L_out_bram(5),
      I1 => \^doado\(2),
      I2 => L_out_bram(3),
      I3 => L_out_bram(4),
      I4 => Q(2),
      I5 => \dc_bias[3]_i_18__0_n_0\,
      O => \dc_bias[3]_i_8_n_0\
    );
\dc_bias[3]_i_8__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => switch(0),
      I1 => \^co\(0),
      I2 => \processQ_reg[8]\,
      O => \encoded_reg[8]\
    );
\dc_bias_reg[3]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \^co\(0),
      CO(2) => \dc_bias_reg[3]_i_3_n_1\,
      CO(1) => \dc_bias_reg[3]_i_3_n_2\,
      CO(0) => \dc_bias_reg[3]_i_3_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_dc_bias_reg[3]_i_3_O_UNCONNECTED\(3 downto 0),
      S(3) => \dc_bias[3]_i_6__1_n_0\,
      S(2) => \dc_bias[3]_i_7__0_n_0\,
      S(1) => \dc_bias[3]_i_8_n_0\,
      S(0) => S(0)
    );
\sdp_bl.ramb18_dp_bl.ram18_bl\: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "NONE",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 0,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 4) => ADDRARDADDR(9 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(13 downto 4) => ADDRBWRADDR(9 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DIADI(15 downto 0) => B"0000000000000000",
      DIBDI(15 downto 10) => B"000000",
      DIBDI(9 downto 0) => \slv_reg1_reg[15]\(9 downto 0),
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 10) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED\(15 downto 10),
      DOADO(9 downto 3) => L_out_bram(9 downto 3),
      DOADO(2 downto 0) => \^doado\(2 downto 0),
      DOBDO(15 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED\(15 downto 0),
      DOPADOP(1 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED\(1 downto 0),
      DOPBDOP(1 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED\(1 downto 0),
      ENARDEN => '1',
      ENBWREN => WREN,
      REGCEAREGCE => '1',
      REGCEB => '0',
      RSTRAMARSTRAM => reset_n,
      RSTRAMB => reset_n,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"00",
      WEBWE(3 downto 0) => B"1111"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0 is
  port (
    \encoded_reg[8]\ : out STD_LOGIC;
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC;
    WREN : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 9 downto 0 );
    ADDRBWRADDR : in STD_LOGIC_VECTOR ( 9 downto 0 );
    DIBDI : in STD_LOGIC_VECTOR ( 9 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0 : entity is "unimacro_BRAM_SDP_MACRO";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0 is
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal R_out_bram : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \dc_bias[3]_i_19__1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_20__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_21_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_22_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_26_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_27__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_28__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_29__0_n_0\ : STD_LOGIC;
  signal \dc_bias_reg[3]_i_13_n_1\ : STD_LOGIC;
  signal \dc_bias_reg[3]_i_13_n_2\ : STD_LOGIC;
  signal \dc_bias_reg[3]_i_13_n_3\ : STD_LOGIC;
  signal \NLW_dc_bias_reg[3]_i_13_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 10 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of \dc_bias_reg[3]_i_13\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of \sdp_bl.ramb18_dp_bl.ram18_bl\ : label is "PRIMITIVE";
begin
  CO(0) <= \^co\(0);
\dc_bias[3]_i_19__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6A95"
    )
        port map (
      I0 => Q(9),
      I1 => R_out_bram(8),
      I2 => \dc_bias[3]_i_26_n_0\,
      I3 => R_out_bram(9),
      O => \dc_bias[3]_i_19__1_n_0\
    );
\dc_bias[3]_i_20__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0480804040080804"
    )
        port map (
      I0 => Q(7),
      I1 => \dc_bias[3]_i_27__0_n_0\,
      I2 => R_out_bram(8),
      I3 => \dc_bias[3]_i_28__0_n_0\,
      I4 => R_out_bram(7),
      I5 => Q(8),
      O => \dc_bias[3]_i_20__0_n_0\
    );
\dc_bias[3]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000556AAA95"
    )
        port map (
      I0 => R_out_bram(5),
      I1 => R_out_bram(2),
      I2 => R_out_bram(3),
      I3 => R_out_bram(4),
      I4 => Q(5),
      I5 => \dc_bias[3]_i_29__0_n_0\,
      O => \dc_bias[3]_i_21_n_0\
    );
\dc_bias[3]_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => R_out_bram(2),
      I1 => Q(2),
      I2 => Q(1),
      I3 => R_out_bram(1),
      I4 => Q(0),
      I5 => R_out_bram(0),
      O => \dc_bias[3]_i_22_n_0\
    );
\dc_bias[3]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888800000000000"
    )
        port map (
      I0 => R_out_bram(7),
      I1 => R_out_bram(5),
      I2 => R_out_bram(2),
      I3 => R_out_bram(3),
      I4 => R_out_bram(4),
      I5 => R_out_bram(6),
      O => \dc_bias[3]_i_26_n_0\
    );
\dc_bias[3]_i_27__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"66666AAA99999555"
    )
        port map (
      I0 => Q(6),
      I1 => R_out_bram(5),
      I2 => R_out_bram(2),
      I3 => R_out_bram(3),
      I4 => R_out_bram(4),
      I5 => R_out_bram(6),
      O => \dc_bias[3]_i_27__0_n_0\
    );
\dc_bias[3]_i_28__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8880000"
    )
        port map (
      I0 => R_out_bram(6),
      I1 => R_out_bram(4),
      I2 => R_out_bram(3),
      I3 => R_out_bram(2),
      I4 => R_out_bram(5),
      O => \dc_bias[3]_i_28__0_n_0\
    );
\dc_bias[3]_i_29__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BDDEE77B"
    )
        port map (
      I0 => Q(3),
      I1 => R_out_bram(4),
      I2 => R_out_bram(3),
      I3 => R_out_bram(2),
      I4 => Q(4),
      O => \dc_bias[3]_i_29__0_n_0\
    );
\dc_bias[3]_i_7__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^co\(0),
      I1 => switch(0),
      O => \encoded_reg[8]\
    );
\dc_bias_reg[3]_i_13\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \^co\(0),
      CO(2) => \dc_bias_reg[3]_i_13_n_1\,
      CO(1) => \dc_bias_reg[3]_i_13_n_2\,
      CO(0) => \dc_bias_reg[3]_i_13_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_dc_bias_reg[3]_i_13_O_UNCONNECTED\(3 downto 0),
      S(3) => \dc_bias[3]_i_19__1_n_0\,
      S(2) => \dc_bias[3]_i_20__0_n_0\,
      S(1) => \dc_bias[3]_i_21_n_0\,
      S(0) => \dc_bias[3]_i_22_n_0\
    );
\sdp_bl.ramb18_dp_bl.ram18_bl\: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "NONE",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 0,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 4) => ADDRARDADDR(9 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(13 downto 4) => ADDRBWRADDR(9 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DIADI(15 downto 0) => B"0000000000000000",
      DIBDI(15 downto 10) => B"000000",
      DIBDI(9 downto 0) => DIBDI(9 downto 0),
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 10) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED\(15 downto 10),
      DOADO(9 downto 0) => R_out_bram(9 downto 0),
      DOBDO(15 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED\(15 downto 0),
      DOPADOP(1 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED\(1 downto 0),
      DOPBDOP(1 downto 0) => \NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED\(1 downto 0),
      ENARDEN => '1',
      ENBWREN => WREN,
      REGCEAREGCE => '1',
      REGCEB => '0',
      RSTRAMARSTRAM => reset_n,
      RSTRAMB => reset_n,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"00",
      WEBWE(3 downto 0) => B"1111"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vcounter is
  port (
    \encoded_reg[0]\ : out STD_LOGIC;
    \encoded_reg[9]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 );
    \encoded_reg[8]\ : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \encoded_reg[8]_1\ : out STD_LOGIC;
    \encoded_reg[0]_0\ : out STD_LOGIC;
    \encoded_reg[8]_2\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_3\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_4\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_5\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_6\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_7\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_8\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_9\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_10\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_11\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_12\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_13\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_14\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_15\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_16\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_17\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_18\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_19\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_20\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_21\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_22\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_23\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_24\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]_25\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_26\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \encoded_reg[8]_27\ : out STD_LOGIC;
    \encoded_reg[8]_28\ : out STD_LOGIC;
    \encoded_reg[8]_29\ : out STD_LOGIC;
    \encoded_reg[8]_30\ : out STD_LOGIC;
    \encoded_reg[8]_31\ : out STD_LOGIC;
    \encoded_reg[9]_0\ : out STD_LOGIC;
    \encoded_reg[9]_1\ : out STD_LOGIC;
    \dc_bias_reg[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[8]_0\ : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    \processQ_reg[8]_1\ : in STD_LOGIC;
    \processQ_reg[2]_0\ : in STD_LOGIC;
    \processQ_reg[4]_0\ : in STD_LOGIC;
    \processQ_reg[5]_0\ : in STD_LOGIC;
    \processQ_reg[6]_0\ : in STD_LOGIC;
    \processQ_reg[9]_0\ : in STD_LOGIC;
    \slv_reg11_reg[1]\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[9]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg5_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg11_reg[9]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DOADO : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \slv_reg11_reg[9]_0\ : in STD_LOGIC;
    \slv_reg11_reg[8]\ : in STD_LOGIC;
    \slv_reg11_reg[7]\ : in STD_LOGIC;
    \slv_reg11_reg[6]\ : in STD_LOGIC;
    \slv_reg11_reg[5]\ : in STD_LOGIC;
    \slv_reg11_reg[7]_0\ : in STD_LOGIC;
    \slv_reg11_reg[6]_0\ : in STD_LOGIC;
    \slv_reg11_reg[5]_0\ : in STD_LOGIC;
    \slv_reg11_reg[6]_1\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[7]\ : in STD_LOGIC;
    \slv_reg11_reg[5]_1\ : in STD_LOGIC;
    \slv_reg11_reg[6]_2\ : in STD_LOGIC;
    \slv_reg11_reg[5]_2\ : in STD_LOGIC;
    \slv_reg11_reg[6]_3\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[2]\ : in STD_LOGIC;
    \slv_reg11_reg[5]_3\ : in STD_LOGIC;
    \slv_reg11_reg[6]_4\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[2]_0\ : in STD_LOGIC;
    \slv_reg11_reg[3]\ : in STD_LOGIC;
    \slv_reg11_reg[2]\ : in STD_LOGIC;
    \slv_reg11_reg[4]\ : in STD_LOGIC;
    \slv_reg11_reg[0]\ : in STD_LOGIC;
    \slv_reg11_reg[0]_0\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[3]\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[2]_1\ : in STD_LOGIC;
    \slv_reg11_reg[0]_1\ : in STD_LOGIC;
    \slv_reg11_reg[1]_0\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[2]_2\ : in STD_LOGIC;
    \slv_reg11_reg[0]_2\ : in STD_LOGIC;
    \processQ_reg[9]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[9]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[4]_1\ : in STD_LOGIC;
    \processQ_reg[0]_0\ : in STD_LOGIC;
    \processQ_reg[1]_0\ : in STD_LOGIC;
    \processQ_reg[9]_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[1]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \slv_reg11_reg[9]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \processQ_reg[0]_1\ : in STD_LOGIC;
    \processQ_reg[0]_2\ : in STD_LOGIC;
    \processQ_reg[7]_0\ : in STD_LOGIC;
    \slv_reg11_reg[7]_1\ : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \dc_bias_reg[1]\ : in STD_LOGIC;
    \processQ_reg[8]_2\ : in STD_LOGIC;
    \processQ_reg[9]_4\ : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vcounter;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vcounter is
  signal \^q\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \dc_bias[3]_i_17__1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_19__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_20__1_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_20_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_21__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_22__0_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_28_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_29_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_31_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_32_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_43_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_44_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_47_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_48_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_49_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_52_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_53_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_54_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_59_n_0\ : STD_LOGIC;
  signal \dc_bias[3]_i_60_n_0\ : STD_LOGIC;
  signal \dc_bias_reg[3]_i_46_n_0\ : STD_LOGIC;
  signal \encoded[9]_i_2_n_0\ : STD_LOGIC;
  signal \encoded[9]_i_3_n_0\ : STD_LOGIC;
  signal \^encoded_reg[0]_0\ : STD_LOGIC;
  signal \^encoded_reg[9]\ : STD_LOGIC;
  signal \i__carry_i_7__6_n_0\ : STD_LOGIC;
  signal \i__carry_i_9__9_n_0\ : STD_LOGIC;
  signal \plusOp__2\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal processQ0 : STD_LOGIC;
  signal \processQ[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \processQ[6]_i_1__1_n_0\ : STD_LOGIC;
  signal \processQ[6]_i_2_n_0\ : STD_LOGIC;
  signal \processQ[8]_i_2_n_0\ : STD_LOGIC;
  signal \processQ[9]_i_1__0_n_0\ : STD_LOGIC;
  signal \processQ[9]_i_4__0_n_0\ : STD_LOGIC;
  signal \processQ[9]_i_5_n_0\ : STD_LOGIC;
  signal \processQ[9]_i_6_n_0\ : STD_LOGIC;
  signal \processQ[9]_i_7_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \TDMS_encoder_blue/encoded[9]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \TDMS_encoder_green/encoded[9]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_11__1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_12__0\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_17__1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_1__1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_20\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_20__1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_22__0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_29\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_32\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_48\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_53\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \dc_bias[3]_i_54\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \encoded[0]_i_1__1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \processQ[1]_i_1__0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \processQ[2]_i_1__0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \processQ[3]_i_1__0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \processQ[4]_i_1__0\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \processQ[6]_i_2\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \processQ[7]_i_1__0\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \processQ[9]_i_5\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \processQ[9]_i_6\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \processQ[9]_i_7\ : label is "soft_lutpair67";
begin
  Q(9 downto 0) <= \^q\(9 downto 0);
  \encoded_reg[0]_0\ <= \^encoded_reg[0]_0\;
  \encoded_reg[9]\ <= \^encoded_reg[9]\;
\TDMS_encoder_blue/encoded[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"535303A3"
    )
        port map (
      I0 => \dc_bias_reg[3]_0\(0),
      I1 => \encoded[9]_i_2_n_0\,
      I2 => \^encoded_reg[9]\,
      I3 => \dc_bias_reg[1]\,
      I4 => \processQ_reg[8]_2\,
      O => \encoded_reg[9]_0\
    );
\TDMS_encoder_green/encoded[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D7"
    )
        port map (
      I0 => \^encoded_reg[9]\,
      I1 => \dc_bias_reg[3]\(0),
      I2 => \processQ_reg[9]_4\,
      O => \encoded_reg[9]_1\
    );
\dc_bias[3]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFD0FFD0FFFFFFD0"
    )
        port map (
      I0 => \dc_bias[3]_i_19__0_n_0\,
      I1 => \dc_bias[3]_i_20_n_0\,
      I2 => \dc_bias[3]_i_21__0_n_0\,
      I3 => \processQ_reg[9]_0\,
      I4 => \dc_bias[3]_i_22__0_n_0\,
      I5 => \^encoded_reg[0]_0\,
      O => \encoded_reg[8]_1\
    );
\dc_bias[3]_i_11__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDFF"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(8),
      I2 => \^q\(9),
      I3 => \dc_bias[3]_i_17__1_n_0\,
      O => \^encoded_reg[0]_0\
    );
\dc_bias[3]_i_12__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAABAAAA"
    )
        port map (
      I0 => \processQ_reg[8]_1\,
      I1 => \^q\(8),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \dc_bias[3]_i_17__1_n_0\,
      O => \encoded_reg[8]_29\
    );
\dc_bias[3]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEAFFEAFFFFFFEA"
    )
        port map (
      I0 => \dc_bias[3]_i_28_n_0\,
      I1 => \^q\(8),
      I2 => \dc_bias[3]_i_29_n_0\,
      I3 => \processQ_reg[8]_1\,
      I4 => \processQ_reg[2]_0\,
      I5 => \processQ_reg[4]_0\,
      O => \encoded_reg[8]\
    );
\dc_bias[3]_i_13__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"777777777F7E7EFE"
    )
        port map (
      I0 => \^q\(8),
      I1 => \^q\(7),
      I2 => \^q\(5),
      I3 => \^q\(4),
      I4 => \dc_bias[3]_i_20__1_n_0\,
      I5 => \^q\(6),
      O => \encoded_reg[8]_31\
    );
\dc_bias[3]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFFEFFFFFFFEFF"
    )
        port map (
      I0 => \dc_bias[3]_i_31_n_0\,
      I1 => \^q\(9),
      I2 => \^q\(8),
      I3 => \^q\(3),
      I4 => \^q\(2),
      I5 => \dc_bias[3]_i_32_n_0\,
      O => \encoded_reg[8]_3\
    );
\dc_bias[3]_i_15__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0800080"
    )
        port map (
      I0 => \processQ_reg[9]_3\(0),
      I1 => CO(0),
      I2 => \processQ_reg[1]_1\(0),
      I3 => \processQ_reg[1]_1\(1),
      I4 => \slv_reg11_reg[9]_1\(0),
      O => \encoded_reg[8]_28\
    );
\dc_bias[3]_i_17__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000100"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^q\(5),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(6),
      O => \dc_bias[3]_i_17__1_n_0\
    );
\dc_bias[3]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00B800FF00B80000"
    )
        port map (
      I0 => \dc_bias[3]_i_43_n_0\,
      I1 => \^q\(2),
      I2 => \dc_bias[3]_i_44_n_0\,
      I3 => \processQ_reg[5]_0\,
      I4 => \^q\(1),
      I5 => \dc_bias_reg[3]_i_46_n_0\,
      O => \encoded_reg[8]_0\
    );
\dc_bias[3]_i_19__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5155151555555555"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(3),
      I3 => \^q\(4),
      I4 => \^q\(6),
      I5 => \dc_bias[3]_i_47_n_0\,
      O => \dc_bias[3]_i_19__0_n_0\
    );
\dc_bias[3]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^encoded_reg[9]\,
      O => SR(0)
    );
\dc_bias[3]_i_20\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000440"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^q\(6),
      I2 => \^q\(8),
      I3 => \^q\(5),
      I4 => \dc_bias[3]_i_48_n_0\,
      O => \dc_bias[3]_i_20_n_0\
    );
\dc_bias[3]_i_20__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      O => \dc_bias[3]_i_20__1_n_0\
    );
\dc_bias[3]_i_21__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000555540555555"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(7),
      I2 => \^q\(8),
      I3 => \^q\(5),
      I4 => \^q\(2),
      I5 => \dc_bias[3]_i_49_n_0\,
      O => \dc_bias[3]_i_21__0_n_0\
    );
\dc_bias[3]_i_22__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \dc_bias[3]_i_22__0_n_0\
    );
\dc_bias[3]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2002300020020003"
    )
        port map (
      I0 => \dc_bias[3]_i_52_n_0\,
      I1 => \dc_bias[3]_i_53_n_0\,
      I2 => \^q\(8),
      I3 => \^q\(7),
      I4 => \^q\(5),
      I5 => \^q\(6),
      O => \dc_bias[3]_i_28_n_0\
    );
\dc_bias[3]_i_29\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^q\(6),
      O => \dc_bias[3]_i_29_n_0\
    );
\dc_bias[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00007FFF"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(6),
      I2 => \^q\(7),
      I3 => \^q\(8),
      I4 => \processQ_reg[8]_1\,
      O => \^encoded_reg[9]\
    );
\dc_bias[3]_i_31\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF1FFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \^q\(4),
      I3 => \^q\(6),
      I4 => \dc_bias[3]_i_54_n_0\,
      I5 => \processQ_reg[7]_0\,
      O => \dc_bias[3]_i_31_n_0\
    );
\dc_bias[3]_i_32\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \dc_bias[3]_i_32_n_0\
    );
\dc_bias[3]_i_43\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0012048008001048"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(4),
      I2 => \^q\(7),
      I3 => \^q\(8),
      I4 => \^q\(6),
      I5 => \^q\(5),
      O => \dc_bias[3]_i_43_n_0\
    );
\dc_bias[3]_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080010402488010"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(4),
      I2 => \^q\(7),
      I3 => \^q\(8),
      I4 => \^q\(6),
      I5 => \^q\(5),
      O => \dc_bias[3]_i_44_n_0\
    );
\dc_bias[3]_i_47\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0CCC088800000888"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^q\(5),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \^q\(6),
      I5 => \^q\(8),
      O => \dc_bias[3]_i_47_n_0\
    );
\dc_bias[3]_i_48\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BFFE"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(3),
      I2 => \^q\(4),
      I3 => \^q\(5),
      O => \dc_bias[3]_i_48_n_0\
    );
\dc_bias[3]_i_49\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFFFFFBFFFFFF3B"
    )
        port map (
      I0 => \^q\(8),
      I1 => \^q\(1),
      I2 => \^q\(3),
      I3 => \^q\(4),
      I4 => \^q\(7),
      I5 => \^q\(6),
      O => \dc_bias[3]_i_49_n_0\
    );
\dc_bias[3]_i_4__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000D000000000"
    )
        port map (
      I0 => \processQ_reg[0]_1\,
      I1 => \processQ_reg[0]_2\,
      I2 => \dc_bias[3]_i_17__1_n_0\,
      I3 => \^q\(9),
      I4 => \^q\(8),
      I5 => \^q\(4),
      O => \encoded_reg[8]_30\
    );
\dc_bias[3]_i_52\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA8A8A8A8A8"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \^q\(2),
      O => \dc_bias[3]_i_52_n_0\
    );
\dc_bias[3]_i_53\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E0"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(2),
      I2 => \^q\(4),
      I3 => \^q\(5),
      O => \dc_bias[3]_i_53_n_0\
    );
\dc_bias[3]_i_54\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(7),
      O => \dc_bias[3]_i_54_n_0\
    );
\dc_bias[3]_i_59\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0102401000802408"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(5),
      I2 => \^q\(6),
      I3 => \^q\(7),
      I4 => \^q\(8),
      I5 => \^q\(4),
      O => \dc_bias[3]_i_59_n_0\
    );
\dc_bias[3]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF08FFFFFF08FF08"
    )
        port map (
      I0 => \processQ_reg[9]_1\(0),
      I1 => \processQ_reg[9]_2\(0),
      I2 => \processQ_reg[4]_1\,
      I3 => \processQ_reg[0]_0\,
      I4 => \^encoded_reg[0]_0\,
      I5 => \processQ_reg[1]_0\,
      O => \encoded_reg[8]_27\
    );
\dc_bias[3]_i_60\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0812040001080024"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(8),
      I2 => \^q\(7),
      I3 => \^q\(6),
      I4 => \^q\(5),
      I5 => \^q\(4),
      O => \dc_bias[3]_i_60_n_0\
    );
\dc_bias[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => \^q\(2),
      I1 => DOADO(2),
      I2 => \^q\(1),
      I3 => DOADO(1),
      I4 => \^q\(0),
      I5 => DOADO(0),
      O => S(0)
    );
\dc_bias_reg[3]_i_46\: unisim.vcomponents.MUXF7
     port map (
      I0 => \dc_bias[3]_i_59_n_0\,
      I1 => \dc_bias[3]_i_60_n_0\,
      O => \dc_bias_reg[3]_i_46_n_0\,
      S => \^q\(2)
    );
\encoded[0]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^encoded_reg[9]\,
      I1 => \dc_bias_reg[3]\(0),
      O => \encoded_reg[0]\
    );
\encoded[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAA9AAAAAAAAAA"
    )
        port map (
      I0 => \processQ_reg[6]_0\,
      I1 => \^q\(9),
      I2 => \^q\(4),
      I3 => \^q\(8),
      I4 => \^q\(2),
      I5 => \encoded[9]_i_3_n_0\,
      O => \encoded[9]_i_2_n_0\
    );
\encoded[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(7),
      I2 => \^q\(3),
      I3 => \^q\(1),
      I4 => \^q\(5),
      O => \encoded[9]_i_3_n_0\
    );
\i__carry__0_i_1__5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15403D54"
    )
        port map (
      I0 => \^q\(9),
      I1 => \slv_reg11_reg[8]\,
      I2 => \slv_reg11_reg[7]\,
      I3 => \slv_reg11_reg[9]_0\,
      I4 => \^q\(8),
      O => \encoded_reg[8]_5\(0)
    );
\i__carry__0_i_1__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C222ABBB80002AAA"
    )
        port map (
      I0 => \^q\(9),
      I1 => \slv_reg11_reg[8]\,
      I2 => \slv_reg11_reg[6]_0\,
      I3 => \slv_reg11_reg[7]_0\,
      I4 => \slv_reg11_reg[9]_0\,
      I5 => \^q\(8),
      O => \encoded_reg[8]_9\(0)
    );
\i__carry__0_i_1__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C222ABBB80002AAA"
    )
        port map (
      I0 => \^q\(9),
      I1 => \slv_reg11_reg[8]\,
      I2 => \slv_reg11_reg[6]_2\,
      I3 => \slv_reg11_reg[7]_0\,
      I4 => \slv_reg11_reg[9]_0\,
      I5 => \^q\(8),
      O => \encoded_reg[8]_17\(0)
    );
\i__carry__0_i_1__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C222ABBB80002AAA"
    )
        port map (
      I0 => \^q\(9),
      I1 => \slv_reg11_reg[8]\,
      I2 => \slv_reg11_reg[6]_4\,
      I3 => \slv_reg11_reg[7]_0\,
      I4 => \slv_reg11_reg[9]_0\,
      I5 => \^q\(8),
      O => \encoded_reg[8]_25\(0)
    );
\i__carry__0_i_2__5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg11_reg[9]_0\,
      I1 => \^q\(9),
      I2 => \slv_reg11_reg[8]\,
      I3 => \slv_reg11_reg[7]\,
      I4 => \^q\(8),
      O => \encoded_reg[8]_4\(0)
    );
\i__carry__0_i_2__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0990909060090909"
    )
        port map (
      I0 => \slv_reg11_reg[9]_0\,
      I1 => \^q\(9),
      I2 => \slv_reg11_reg[8]\,
      I3 => \slv_reg11_reg[6]_0\,
      I4 => \slv_reg11_reg[7]_0\,
      I5 => \^q\(8),
      O => \encoded_reg[8]_8\(0)
    );
\i__carry__0_i_2__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0990909060090909"
    )
        port map (
      I0 => \slv_reg11_reg[9]_0\,
      I1 => \^q\(9),
      I2 => \slv_reg11_reg[8]\,
      I3 => \slv_reg11_reg[6]_2\,
      I4 => \slv_reg11_reg[7]_0\,
      I5 => \^q\(8),
      O => \encoded_reg[8]_16\(0)
    );
\i__carry__0_i_2__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0990909060090909"
    )
        port map (
      I0 => \slv_reg11_reg[9]_0\,
      I1 => \^q\(9),
      I2 => \slv_reg11_reg[8]\,
      I3 => \slv_reg11_reg[6]_4\,
      I4 => \slv_reg11_reg[7]_0\,
      I5 => \^q\(8),
      O => \encoded_reg[8]_24\(0)
    );
\i__carry_i_1__10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7877788887888777"
    )
        port map (
      I0 => \slv_reg11_reg[8]\,
      I1 => \slv_reg11_reg[7]_1\,
      I2 => \slv_reg11_reg[9]\(3),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \int_trigger_volt_s_reg[9]\(3),
      I5 => \^q\(9),
      O => \encoded_reg[8]_12\(3)
    );
\i__carry_i_1__6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15403D54"
    )
        port map (
      I0 => \^q\(7),
      I1 => \slv_reg11_reg[6]\,
      I2 => \slv_reg11_reg[5]\,
      I3 => \slv_reg11_reg[7]_0\,
      I4 => \^q\(6),
      O => \encoded_reg[8]_6\(3)
    );
\i__carry_i_1__7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C2AB802A"
    )
        port map (
      I0 => \^q\(7),
      I1 => \slv_reg11_reg[6]\,
      I2 => \slv_reg11_reg[5]_0\,
      I3 => \slv_reg11_reg[7]_0\,
      I4 => \^q\(6),
      O => \encoded_reg[8]_10\(3)
    );
\i__carry_i_1__8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C2AB802A"
    )
        port map (
      I0 => \^q\(7),
      I1 => \slv_reg11_reg[6]\,
      I2 => \slv_reg11_reg[5]_2\,
      I3 => \slv_reg11_reg[7]_0\,
      I4 => \^q\(6),
      O => \encoded_reg[8]_18\(3)
    );
\i__carry_i_1__9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2C22BABB0800A2AA"
    )
        port map (
      I0 => \^q\(7),
      I1 => \slv_reg11_reg[6]\,
      I2 => \int_trigger_volt_s_reg[2]_0\,
      I3 => \slv_reg11_reg[5]_3\,
      I4 => \slv_reg11_reg[7]_0\,
      I5 => \^q\(6),
      O => DI(3)
    );
\i__carry_i_2__10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BCCC2AAAA8880222"
    )
        port map (
      I0 => \^q\(5),
      I1 => \slv_reg11_reg[4]\,
      I2 => \int_trigger_volt_s_reg[2]_2\,
      I3 => \slv_reg11_reg[3]\,
      I4 => \slv_reg11_reg[5]_3\,
      I5 => \^q\(4),
      O => DI(2)
    );
\i__carry_i_2__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000006909009"
    )
        port map (
      I0 => \slv_reg11_reg[8]\,
      I1 => \^q\(8),
      I2 => \^q\(7),
      I3 => \slv_reg11_reg[6]_1\,
      I4 => \slv_reg11_reg[7]_0\,
      I5 => \i__carry_i_7__6_n_0\,
      O => \encoded_reg[8]_12\(2)
    );
\i__carry_i_2__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011454434335D55"
    )
        port map (
      I0 => \^q\(5),
      I1 => \slv_reg11_reg[4]\,
      I2 => \slv_reg11_reg[0]\,
      I3 => \slv_reg11_reg[3]\,
      I4 => \slv_reg11_reg[5]_3\,
      I5 => \^q\(4),
      O => \encoded_reg[8]_6\(2)
    );
\i__carry_i_2__8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BC2AA802"
    )
        port map (
      I0 => \^q\(5),
      I1 => \slv_reg11_reg[4]\,
      I2 => \slv_reg11_reg[0]_0\,
      I3 => \slv_reg11_reg[5]_3\,
      I4 => \^q\(4),
      O => \encoded_reg[8]_10\(2)
    );
\i__carry_i_2__9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BC2AA802"
    )
        port map (
      I0 => \^q\(5),
      I1 => \slv_reg11_reg[4]\,
      I2 => \slv_reg11_reg[0]_1\,
      I3 => \slv_reg11_reg[5]_3\,
      I4 => \^q\(4),
      O => \encoded_reg[8]_18\(2)
    );
\i__carry_i_3__10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C2AB802A"
    )
        port map (
      I0 => \^q\(3),
      I1 => \slv_reg11_reg[2]\,
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[3]\,
      I4 => \^q\(2),
      O => DI(1)
    );
\i__carry_i_3__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000060060690"
    )
        port map (
      I0 => \slv_reg11_reg[5]_3\,
      I1 => \^q\(5),
      I2 => \^q\(4),
      I3 => \int_trigger_volt_s_reg[3]\,
      I4 => \slv_reg11_reg[4]\,
      I5 => \i__carry_i_9__9_n_0\,
      O => \encoded_reg[8]_12\(1)
    );
\i__carry_i_3__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000155545403FD55"
    )
        port map (
      I0 => \^q\(3),
      I1 => \slv_reg11_reg[0]_2\,
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[2]\,
      I4 => \slv_reg11_reg[3]\,
      I5 => \^q\(2),
      O => \encoded_reg[8]_6\(1)
    );
\i__carry_i_3__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C02AAABF80002AAA"
    )
        port map (
      I0 => \^q\(3),
      I1 => \slv_reg11_reg[0]_2\,
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[2]\,
      I4 => \slv_reg11_reg[3]\,
      I5 => \^q\(2),
      O => \encoded_reg[8]_10\(1)
    );
\i__carry_i_3__9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC2AAAB8880222A"
    )
        port map (
      I0 => \^q\(3),
      I1 => \slv_reg11_reg[2]\,
      I2 => \slv_reg11_reg[0]_2\,
      I3 => \slv_reg11_reg[1]\,
      I4 => \slv_reg11_reg[3]\,
      I5 => \^q\(2),
      O => \encoded_reg[8]_18\(1)
    );
\i__carry_i_4__5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => \slv_reg11_reg[2]\,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \slv_reg11_reg[0]_2\,
      I4 => \^q\(1),
      I5 => \slv_reg11_reg[1]\,
      O => \encoded_reg[8]_12\(0)
    );
\i__carry_i_4__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5017505050171717"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[9]\(0),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => \int_trigger_volt_s_reg[9]\(0),
      O => \encoded_reg[8]_6\(0)
    );
\i__carry_i_4__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF1D001D000000"
    )
        port map (
      I0 => \int_trigger_volt_s_reg[9]\(0),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \slv_reg11_reg[9]\(0),
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => \slv_reg11_reg[1]\,
      O => DI(0)
    );
\i__carry_i_4__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFE200E21DE20000"
    )
        port map (
      I0 => \int_trigger_volt_s_reg[9]\(0),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \slv_reg11_reg[9]\(0),
      I3 => \slv_reg11_reg[1]\,
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => \encoded_reg[8]_18\(0)
    );
\i__carry_i_4__9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E0EEE0008A888AAA"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \slv_reg11_reg[9]\(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \int_trigger_volt_s_reg[9]\(0),
      I5 => \slv_reg11_reg[1]\,
      O => \encoded_reg[8]_10\(0)
    );
\i__carry_i_5__10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009909009600909"
    )
        port map (
      I0 => \slv_reg11_reg[7]_0\,
      I1 => \^q\(7),
      I2 => \slv_reg11_reg[6]\,
      I3 => \int_trigger_volt_s_reg[2]_0\,
      I4 => \slv_reg11_reg[5]_3\,
      I5 => \^q\(6),
      O => \encoded_reg[8]_26\(3)
    );
\i__carry_i_5__6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg11_reg[7]_0\,
      I1 => \^q\(7),
      I2 => \slv_reg11_reg[6]\,
      I3 => \slv_reg11_reg[5]\,
      I4 => \^q\(6),
      O => \encoded_reg[8]_7\(3)
    );
\i__carry_i_5__7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg11_reg[7]_0\,
      I1 => \^q\(7),
      I2 => \slv_reg11_reg[6]\,
      I3 => \slv_reg11_reg[5]_0\,
      I4 => \^q\(6),
      O => \encoded_reg[8]_11\(3)
    );
\i__carry_i_5__9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg11_reg[7]_0\,
      I1 => \^q\(7),
      I2 => \slv_reg11_reg[6]\,
      I3 => \slv_reg11_reg[5]_2\,
      I4 => \^q\(6),
      O => \encoded_reg[8]_19\(3)
    );
\i__carry_i_6__10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6009090906606060"
    )
        port map (
      I0 => \slv_reg11_reg[5]_3\,
      I1 => \^q\(5),
      I2 => \slv_reg11_reg[4]\,
      I3 => \int_trigger_volt_s_reg[2]_2\,
      I4 => \slv_reg11_reg[3]\,
      I5 => \^q\(4),
      O => \encoded_reg[8]_26\(2)
    );
\i__carry_i_6__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0960090960066060"
    )
        port map (
      I0 => \slv_reg11_reg[5]_3\,
      I1 => \^q\(5),
      I2 => \slv_reg11_reg[4]\,
      I3 => \slv_reg11_reg[0]\,
      I4 => \slv_reg11_reg[3]\,
      I5 => \^q\(4),
      O => \encoded_reg[8]_7\(2)
    );
\i__carry_i_6__8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60090660"
    )
        port map (
      I0 => \slv_reg11_reg[5]_3\,
      I1 => \^q\(5),
      I2 => \slv_reg11_reg[4]\,
      I3 => \slv_reg11_reg[0]_0\,
      I4 => \^q\(4),
      O => \encoded_reg[8]_11\(2)
    );
\i__carry_i_6__9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60090660"
    )
        port map (
      I0 => \slv_reg11_reg[5]_3\,
      I1 => \^q\(5),
      I2 => \slv_reg11_reg[4]\,
      I3 => \slv_reg11_reg[0]_1\,
      I4 => \^q\(4),
      O => \encoded_reg[8]_19\(2)
    );
\i__carry_i_7__10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg11_reg[3]\,
      I1 => \^q\(3),
      I2 => \slv_reg11_reg[2]\,
      I3 => \slv_reg11_reg[1]\,
      I4 => \^q\(2),
      O => \encoded_reg[8]_26\(1)
    );
\i__carry_i_7__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999955566666AAA"
    )
        port map (
      I0 => \^q\(6),
      I1 => \slv_reg11_reg[5]_3\,
      I2 => \slv_reg11_reg[3]\,
      I3 => \slv_reg11_reg[2]\,
      I4 => \slv_reg11_reg[4]\,
      I5 => \slv_reg11_reg[6]\,
      O => \i__carry_i_7__6_n_0\
    );
\i__carry_i_7__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2121211884848442"
    )
        port map (
      I0 => \^q\(2),
      I1 => \slv_reg11_reg[3]\,
      I2 => \slv_reg11_reg[2]\,
      I3 => \slv_reg11_reg[1]\,
      I4 => \slv_reg11_reg[0]_2\,
      I5 => \^q\(3),
      O => \encoded_reg[8]_7\(1)
    );
\i__carry_i_7__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1884848442212121"
    )
        port map (
      I0 => \^q\(2),
      I1 => \slv_reg11_reg[3]\,
      I2 => \slv_reg11_reg[2]\,
      I3 => \slv_reg11_reg[1]\,
      I4 => \slv_reg11_reg[0]_2\,
      I5 => \^q\(3),
      O => \encoded_reg[8]_11\(1)
    );
\i__carry_i_7__9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0909099060606009"
    )
        port map (
      I0 => \slv_reg11_reg[3]\,
      I1 => \^q\(3),
      I2 => \slv_reg11_reg[2]\,
      I3 => \slv_reg11_reg[1]\,
      I4 => \slv_reg11_reg[0]_2\,
      I5 => \^q\(2),
      O => \encoded_reg[8]_19\(1)
    );
\i__carry_i_8__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1482141414828282"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[9]\(0),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => \int_trigger_volt_s_reg[9]\(0),
      O => \encoded_reg[8]_11\(0)
    );
\i__carry_i_8__5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4128414141282828"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[9]\(0),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => \int_trigger_volt_s_reg[9]\(0),
      O => \encoded_reg[8]_7\(0)
    );
\i__carry_i_8__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6066600006000666"
    )
        port map (
      I0 => \slv_reg11_reg[1]\,
      I1 => \^q\(1),
      I2 => \slv_reg11_reg[9]\(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \int_trigger_volt_s_reg[9]\(0),
      I5 => \^q\(0),
      O => \encoded_reg[8]_26\(0)
    );
\i__carry_i_8__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4128414141282828"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[9]\(0),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => \int_trigger_volt_s_reg[9]\(0),
      O => \encoded_reg[8]_19\(0)
    );
\i__carry_i_9__9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A956A65959A656A"
    )
        port map (
      I0 => \^q\(3),
      I1 => \slv_reg11_reg[9]\(1),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => \int_trigger_volt_s_reg[9]\(1),
      I4 => \slv_reg11_reg[9]\(2),
      I5 => \int_trigger_volt_s_reg[9]\(2),
      O => \i__carry_i_9__9_n_0\
    );
\pixel_color4_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15403D54"
    )
        port map (
      I0 => \^q\(9),
      I1 => \slv_reg11_reg[8]\,
      I2 => \int_trigger_volt_s_reg[7]\,
      I3 => \slv_reg11_reg[9]_0\,
      I4 => \^q\(8),
      O => \encoded_reg[8]_14\(0)
    );
\pixel_color4_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg11_reg[9]_0\,
      I1 => \^q\(9),
      I2 => \slv_reg11_reg[8]\,
      I3 => \int_trigger_volt_s_reg[7]\,
      I4 => \^q\(8),
      O => \encoded_reg[8]_13\(0)
    );
pixel_color4_carry_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15403D54"
    )
        port map (
      I0 => \^q\(7),
      I1 => \slv_reg11_reg[6]\,
      I2 => \slv_reg11_reg[5]_1\,
      I3 => \slv_reg11_reg[7]_0\,
      I4 => \^q\(6),
      O => \encoded_reg[8]_2\(3)
    );
pixel_color4_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011454434335D55"
    )
        port map (
      I0 => \^q\(5),
      I1 => \slv_reg11_reg[4]\,
      I2 => \int_trigger_volt_s_reg[2]_1\,
      I3 => \slv_reg11_reg[3]\,
      I4 => \slv_reg11_reg[5]_3\,
      I5 => \^q\(4),
      O => \encoded_reg[8]_2\(2)
    );
pixel_color4_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0015403F4000FFD5"
    )
        port map (
      I0 => \^q\(2),
      I1 => \slv_reg11_reg[1]\,
      I2 => \slv_reg11_reg[0]_2\,
      I3 => \slv_reg11_reg[2]\,
      I4 => \^q\(3),
      I5 => \slv_reg11_reg[3]\,
      O => \encoded_reg[8]_2\(1)
    );
pixel_color4_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000056A602A257F7"
    )
        port map (
      I0 => \slv_reg11_reg[1]\,
      I1 => \int_trigger_volt_s_reg[9]\(0),
      I2 => \slv_reg5_reg[0]\(0),
      I3 => \slv_reg11_reg[9]\(0),
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => \encoded_reg[8]_2\(0)
    );
pixel_color4_carry_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09906009"
    )
        port map (
      I0 => \slv_reg11_reg[7]_0\,
      I1 => \^q\(7),
      I2 => \slv_reg11_reg[6]\,
      I3 => \slv_reg11_reg[5]_1\,
      I4 => \^q\(6),
      O => \encoded_reg[8]_15\(3)
    );
pixel_color4_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0960090960066060"
    )
        port map (
      I0 => \slv_reg11_reg[5]_3\,
      I1 => \^q\(5),
      I2 => \slv_reg11_reg[4]\,
      I3 => \int_trigger_volt_s_reg[2]_1\,
      I4 => \slv_reg11_reg[3]\,
      I5 => \^q\(4),
      O => \encoded_reg[8]_15\(2)
    );
pixel_color4_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6009090906606060"
    )
        port map (
      I0 => \slv_reg11_reg[3]\,
      I1 => \^q\(3),
      I2 => \slv_reg11_reg[2]\,
      I3 => \slv_reg11_reg[0]_2\,
      I4 => \slv_reg11_reg[1]\,
      I5 => \^q\(2),
      O => \encoded_reg[8]_15\(1)
    );
pixel_color4_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1482141414828282"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[9]\(0),
      I4 => \slv_reg5_reg[0]\(0),
      I5 => \int_trigger_volt_s_reg[9]\(0),
      O => \encoded_reg[8]_15\(0)
    );
\pixel_color5_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"155540003DDD5444"
    )
        port map (
      I0 => \^q\(9),
      I1 => \slv_reg11_reg[8]\,
      I2 => \slv_reg11_reg[6]_3\,
      I3 => \slv_reg11_reg[7]_0\,
      I4 => \slv_reg11_reg[9]_0\,
      I5 => \^q\(8),
      O => \encoded_reg[8]_21\(0)
    );
\pixel_color5_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0990909060090909"
    )
        port map (
      I0 => \slv_reg11_reg[9]_0\,
      I1 => \^q\(9),
      I2 => \slv_reg11_reg[8]\,
      I3 => \slv_reg11_reg[6]_3\,
      I4 => \slv_reg11_reg[7]_0\,
      I5 => \^q\(8),
      O => \encoded_reg[8]_20\(0)
    );
pixel_color5_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"51550400D3DD4544"
    )
        port map (
      I0 => \^q\(7),
      I1 => \slv_reg11_reg[6]\,
      I2 => \int_trigger_volt_s_reg[2]\,
      I3 => \slv_reg11_reg[5]_3\,
      I4 => \slv_reg11_reg[7]_0\,
      I5 => \^q\(6),
      O => \encoded_reg[8]_22\(3)
    );
pixel_color5_carry_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"015443D5"
    )
        port map (
      I0 => \^q\(5),
      I1 => \slv_reg11_reg[4]\,
      I2 => \slv_reg11_reg[1]_0\,
      I3 => \slv_reg11_reg[5]_3\,
      I4 => \^q\(4),
      O => \encoded_reg[8]_22\(2)
    );
pixel_color5_carry_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"015443D5"
    )
        port map (
      I0 => \^q\(3),
      I1 => \slv_reg11_reg[2]\,
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[3]\,
      I4 => \^q\(2),
      O => \encoded_reg[8]_22\(1)
    );
pixel_color5_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000E200E2FFFF"
    )
        port map (
      I0 => \int_trigger_volt_s_reg[9]\(0),
      I1 => \slv_reg5_reg[0]\(0),
      I2 => \slv_reg11_reg[9]\(0),
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => \slv_reg11_reg[1]\,
      O => \encoded_reg[8]_22\(0)
    );
pixel_color5_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009909009600909"
    )
        port map (
      I0 => \slv_reg11_reg[7]_0\,
      I1 => \^q\(7),
      I2 => \slv_reg11_reg[6]\,
      I3 => \int_trigger_volt_s_reg[2]\,
      I4 => \slv_reg11_reg[5]_3\,
      I5 => \^q\(6),
      O => \encoded_reg[8]_23\(3)
    );
pixel_color5_carry_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60090660"
    )
        port map (
      I0 => \slv_reg11_reg[5]_3\,
      I1 => \^q\(5),
      I2 => \slv_reg11_reg[4]\,
      I3 => \slv_reg11_reg[1]_0\,
      I4 => \^q\(4),
      O => \encoded_reg[8]_23\(2)
    );
pixel_color5_carry_i_7: unisim.vcomponents.LUT5
    generic map(
      INIT => X"21188442"
    )
        port map (
      I0 => \^q\(2),
      I1 => \slv_reg11_reg[3]\,
      I2 => \slv_reg11_reg[1]\,
      I3 => \slv_reg11_reg[2]\,
      I4 => \^q\(3),
      O => \encoded_reg[8]_23\(1)
    );
pixel_color5_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6066600006000666"
    )
        port map (
      I0 => \slv_reg11_reg[1]\,
      I1 => \^q\(1),
      I2 => \slv_reg11_reg[9]\(0),
      I3 => \slv_reg5_reg[0]\(0),
      I4 => \int_trigger_volt_s_reg[9]\(0),
      I5 => \^q\(0),
      O => \encoded_reg[8]_23\(0)
    );
\processQ[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \plusOp__2\(0)
    );
\processQ[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \plusOp__2\(1)
    );
\processQ[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \processQ[2]_i_1__0_n_0\
    );
\processQ[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(2),
      O => \plusOp__2\(3)
    );
\processQ[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(2),
      I4 => \^q\(3),
      O => \plusOp__2\(4)
    );
\processQ[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => \plusOp__2\(5)
    );
\processQ[6]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAA6AAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(2),
      I4 => \processQ[6]_i_2_n_0\,
      I5 => \^q\(5),
      O => \processQ[6]_i_1__1_n_0\
    );
\processQ[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(3),
      O => \processQ[6]_i_2_n_0\
    );
\processQ[7]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"9A"
    )
        port map (
      I0 => \^q\(7),
      I1 => \processQ[8]_i_2_n_0\,
      I2 => \^q\(6),
      O => \plusOp__2\(7)
    );
\processQ[8]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9AAA"
    )
        port map (
      I0 => \^q\(8),
      I1 => \processQ[8]_i_2_n_0\,
      I2 => \^q\(7),
      I3 => \^q\(6),
      O => \plusOp__2\(8)
    );
\processQ[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => \processQ[8]_i_2_n_0\
    );
\processQ[9]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00010000FFFFFFFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(8),
      I3 => \processQ[9]_i_4__0_n_0\,
      I4 => \processQ_reg[8]_0\,
      I5 => reset_n,
      O => \processQ[9]_i_1__0_n_0\
    );
\processQ[9]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000888AAAAAAAA"
    )
        port map (
      I0 => \processQ_reg[8]_0\,
      I1 => \processQ[9]_i_5_n_0\,
      I2 => \^q\(2),
      I3 => \^q\(3),
      I4 => \^q\(8),
      I5 => \^q\(9),
      O => processQ0
    );
\processQ[9]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(9),
      I1 => \^q\(8),
      I2 => \^q\(7),
      I3 => \^q\(6),
      I4 => \^q\(5),
      I5 => \processQ[9]_i_6_n_0\,
      O => \plusOp__2\(9)
    );
\processQ[9]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(6),
      I2 => \processQ[9]_i_7_n_0\,
      I3 => \^q\(9),
      I4 => \^q\(2),
      I5 => \^q\(3),
      O => \processQ[9]_i_4__0_n_0\
    );
\processQ[9]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^q\(4),
      I2 => \^q\(6),
      I3 => \^q\(5),
      O => \processQ[9]_i_5_n_0\
    );
\processQ[9]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(2),
      I3 => \^q\(3),
      I4 => \^q\(4),
      O => \processQ[9]_i_6_n_0\
    );
\processQ[9]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(7),
      O => \processQ[9]_i_7_n_0\
    );
\processQ_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__2\(0),
      Q => \^q\(0),
      R => \processQ[9]_i_1__0_n_0\
    );
\processQ_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__2\(1),
      Q => \^q\(1),
      R => \processQ[9]_i_1__0_n_0\
    );
\processQ_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \processQ[2]_i_1__0_n_0\,
      Q => \^q\(2),
      R => \processQ[9]_i_1__0_n_0\
    );
\processQ_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__2\(3),
      Q => \^q\(3),
      R => \processQ[9]_i_1__0_n_0\
    );
\processQ_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__2\(4),
      Q => \^q\(4),
      R => \processQ[9]_i_1__0_n_0\
    );
\processQ_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__2\(5),
      Q => \^q\(5),
      R => \processQ[9]_i_1__0_n_0\
    );
\processQ_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \processQ[6]_i_1__1_n_0\,
      Q => \^q\(6),
      R => \processQ[9]_i_1__0_n_0\
    );
\processQ_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__2\(7),
      Q => \^q\(7),
      R => \processQ[9]_i_1__0_n_0\
    );
\processQ_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__2\(8),
      Q => \^q\(8),
      R => \processQ[9]_i_1__0_n_0\
    );
\processQ_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => processQ0,
      D => \plusOp__2\(9),
      Q => \^q\(9),
      R => \processQ[9]_i_1__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init is
  port (
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    clk_out2 : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init is
  signal data0 : STD_LOGIC_VECTOR ( 6 to 6 );
  signal data1 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal data2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal data_i : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \data_i[0]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[1]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[2]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[3]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[4]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[4]_i_2_n_0\ : STD_LOGIC;
  signal \data_i[5]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[5]_i_2_n_0\ : STD_LOGIC;
  signal \data_i[6]_i_1_n_0\ : STD_LOGIC;
  signal \data_i[6]_i_2_n_0\ : STD_LOGIC;
  signal \data_i[7]_i_1_n_0\ : STD_LOGIC;
  signal delayEn : STD_LOGIC;
  signal delayEn_i_1_n_0 : STD_LOGIC;
  signal delayEn_i_2_n_0 : STD_LOGIC;
  signal delaycnt : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal delaycnt0 : STD_LOGIC;
  signal \delaycnt0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__0_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__1_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__2_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__3_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__4_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_1\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_4\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__5_n_7\ : STD_LOGIC;
  signal \delaycnt0_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \delaycnt0_carry__6_n_2\ : STD_LOGIC;
  signal \delaycnt0_carry__6_n_3\ : STD_LOGIC;
  signal \delaycnt0_carry__6_n_5\ : STD_LOGIC;
  signal \delaycnt0_carry__6_n_6\ : STD_LOGIC;
  signal \delaycnt0_carry__6_n_7\ : STD_LOGIC;
  signal delaycnt0_carry_i_1_n_0 : STD_LOGIC;
  signal delaycnt0_carry_i_2_n_0 : STD_LOGIC;
  signal delaycnt0_carry_i_3_n_0 : STD_LOGIC;
  signal delaycnt0_carry_i_4_n_0 : STD_LOGIC;
  signal delaycnt0_carry_n_0 : STD_LOGIC;
  signal delaycnt0_carry_n_1 : STD_LOGIC;
  signal delaycnt0_carry_n_2 : STD_LOGIC;
  signal delaycnt0_carry_n_3 : STD_LOGIC;
  signal delaycnt0_carry_n_4 : STD_LOGIC;
  signal delaycnt0_carry_n_5 : STD_LOGIC;
  signal delaycnt0_carry_n_6 : STD_LOGIC;
  signal delaycnt0_carry_n_7 : STD_LOGIC;
  signal \delaycnt[0]_i_1_n_0\ : STD_LOGIC;
  signal \initA[0]_i_1_n_0\ : STD_LOGIC;
  signal \initA[6]_i_3_n_0\ : STD_LOGIC;
  signal \initA[6]_i_4_n_0\ : STD_LOGIC;
  signal \initA_reg_n_0_[0]\ : STD_LOGIC;
  signal \initA_reg_n_0_[1]\ : STD_LOGIC;
  signal \initA_reg_n_0_[2]\ : STD_LOGIC;
  signal \initA_reg_n_0_[3]\ : STD_LOGIC;
  signal \initA_reg_n_0_[4]\ : STD_LOGIC;
  signal \initA_reg_n_0_[5]\ : STD_LOGIC;
  signal \initA_reg_n_0_[6]\ : STD_LOGIC;
  signal initEn : STD_LOGIC;
  signal \initWord[0]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[10]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[11]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[12]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[13]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[14]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[15]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[16]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[17]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[18]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[19]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[20]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[21]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[23]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[30]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[30]_i_2_n_0\ : STD_LOGIC;
  signal \initWord[30]_i_3_n_0\ : STD_LOGIC;
  signal \initWord[6]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[8]_i_1_n_0\ : STD_LOGIC;
  signal \initWord[9]_i_1_n_0\ : STD_LOGIC;
  signal \initWord_reg_n_0_[0]\ : STD_LOGIC;
  signal \initWord_reg_n_0_[6]\ : STD_LOGIC;
  signal msg : STD_LOGIC;
  signal msg0 : STD_LOGIC;
  signal \p_1_in__0\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \state[1]_i_3_n_0\ : STD_LOGIC;
  signal \state[1]_i_4_n_0\ : STD_LOGIC;
  signal \state[1]_i_5_n_0\ : STD_LOGIC;
  signal \state[1]_i_6_n_0\ : STD_LOGIC;
  signal \state[2]_i_2_n_0\ : STD_LOGIC;
  signal \state[3]_i_10_n_0\ : STD_LOGIC;
  signal \state[3]_i_11_n_0\ : STD_LOGIC;
  signal \state[3]_i_12_n_0\ : STD_LOGIC;
  signal \state[3]_i_13_n_0\ : STD_LOGIC;
  signal \state[3]_i_3_n_0\ : STD_LOGIC;
  signal \state[3]_i_4_n_0\ : STD_LOGIC;
  signal \state[3]_i_5_n_0\ : STD_LOGIC;
  signal \state[3]_i_6_n_0\ : STD_LOGIC;
  signal \state[3]_i_7_n_0\ : STD_LOGIC;
  signal \state[3]_i_8_n_0\ : STD_LOGIC;
  signal \state[3]_i_9_n_0\ : STD_LOGIC;
  signal \state_reg_n_0_[0]\ : STD_LOGIC;
  signal \state_reg_n_0_[1]\ : STD_LOGIC;
  signal \state_reg_n_0_[2]\ : STD_LOGIC;
  signal \state_reg_n_0_[3]\ : STD_LOGIC;
  signal stb : STD_LOGIC;
  signal stb_i_1_n_0 : STD_LOGIC;
  signal twi_controller_n_0 : STD_LOGIC;
  signal twi_controller_n_1 : STD_LOGIC;
  signal twi_controller_n_2 : STD_LOGIC;
  signal twi_controller_n_3 : STD_LOGIC;
  signal twi_controller_n_4 : STD_LOGIC;
  signal twi_controller_n_5 : STD_LOGIC;
  signal twi_controller_n_6 : STD_LOGIC;
  signal \NLW_delaycnt0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_delaycnt0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of delayEn_i_2 : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \delaycnt[0]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \initA[0]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \initA[1]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \initA[2]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \initA[3]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \initA[4]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \initA[6]_i_4\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \initWord[0]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \initWord[11]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \initWord[30]_i_2\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \initWord[30]_i_3\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \initWord[6]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of msg_i_1 : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \state[1]_i_5\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \state[2]_i_2\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \state[3]_i_11\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of stb_i_1 : label is "soft_lutpair27";
begin
\data_i[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AACC00FFFFFFFF"
    )
        port map (
      I0 => data2(0),
      I1 => data1(0),
      I2 => \initWord_reg_n_0_[0]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => \state_reg_n_0_[1]\,
      I5 => \data_i[4]_i_2_n_0\,
      O => \data_i[0]_i_1_n_0\
    );
\data_i[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010110000100000"
    )
        port map (
      I0 => \state_reg_n_0_[3]\,
      I1 => \state_reg_n_0_[2]\,
      I2 => data1(1),
      I3 => \state_reg_n_0_[1]\,
      I4 => \state_reg_n_0_[0]\,
      I5 => data2(1),
      O => \data_i[1]_i_1_n_0\
    );
\data_i[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0CCAA00FFFFFFFF"
    )
        port map (
      I0 => data1(2),
      I1 => data2(2),
      I2 => \initWord_reg_n_0_[6]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => \state_reg_n_0_[1]\,
      I5 => \data_i[4]_i_2_n_0\,
      O => \data_i[2]_i_1_n_0\
    );
\data_i[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0CCAA00FFFFFFFF"
    )
        port map (
      I0 => data1(3),
      I1 => data2(3),
      I2 => \initWord_reg_n_0_[6]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => \state_reg_n_0_[1]\,
      I5 => \data_i[4]_i_2_n_0\,
      O => \data_i[3]_i_1_n_0\
    );
\data_i[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8AA080A08A008000"
    )
        port map (
      I0 => \data_i[4]_i_2_n_0\,
      I1 => \initWord_reg_n_0_[6]\,
      I2 => \state_reg_n_0_[0]\,
      I3 => \state_reg_n_0_[1]\,
      I4 => data2(4),
      I5 => data1(4),
      O => \data_i[4]_i_1_n_0\
    );
\data_i[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \state_reg_n_0_[2]\,
      I1 => \state_reg_n_0_[3]\,
      O => \data_i[4]_i_2_n_0\
    );
\data_i[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \state_reg_n_0_[2]\,
      I1 => \state_reg_n_0_[0]\,
      I2 => \state_reg_n_0_[1]\,
      I3 => \state_reg_n_0_[3]\,
      I4 => reset_n,
      O => \data_i[5]_i_1_n_0\
    );
\data_i[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AACC00FFFFFFFF"
    )
        port map (
      I0 => data2(5),
      I1 => data1(5),
      I2 => \initWord_reg_n_0_[6]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => \state_reg_n_0_[1]\,
      I5 => \data_i[4]_i_2_n_0\,
      O => \data_i[5]_i_2_n_0\
    );
\data_i[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"800000FF80000010"
    )
        port map (
      I0 => \state_reg_n_0_[1]\,
      I1 => \state_reg_n_0_[0]\,
      I2 => data0(6),
      I3 => \state_reg_n_0_[2]\,
      I4 => \state_reg_n_0_[3]\,
      I5 => \data_i[6]_i_2_n_0\,
      O => \data_i[6]_i_1_n_0\
    );
\data_i[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => \initWord_reg_n_0_[6]\,
      I1 => data2(6),
      I2 => \state_reg_n_0_[1]\,
      I3 => \state_reg_n_0_[0]\,
      I4 => data1(7),
      O => \data_i[6]_i_2_n_0\
    );
\data_i[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000A000C0000"
    )
        port map (
      I0 => data1(7),
      I1 => data2(7),
      I2 => \state_reg_n_0_[2]\,
      I3 => \state_reg_n_0_[3]\,
      I4 => \state_reg_n_0_[1]\,
      I5 => \state_reg_n_0_[0]\,
      O => \data_i[7]_i_1_n_0\
    );
\data_i_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => reset_n,
      D => \data_i[0]_i_1_n_0\,
      Q => data_i(0),
      R => \data_i[5]_i_1_n_0\
    );
\data_i_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => reset_n,
      D => \data_i[1]_i_1_n_0\,
      Q => data_i(1),
      R => '0'
    );
\data_i_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => reset_n,
      D => \data_i[2]_i_1_n_0\,
      Q => data_i(2),
      R => \data_i[5]_i_1_n_0\
    );
\data_i_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => reset_n,
      D => \data_i[3]_i_1_n_0\,
      Q => data_i(3),
      R => \data_i[5]_i_1_n_0\
    );
\data_i_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => reset_n,
      D => \data_i[4]_i_1_n_0\,
      Q => data_i(4),
      R => '0'
    );
\data_i_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => reset_n,
      D => \data_i[5]_i_2_n_0\,
      Q => data_i(5),
      R => \data_i[5]_i_1_n_0\
    );
\data_i_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => reset_n,
      D => \data_i[6]_i_1_n_0\,
      Q => data_i(6),
      R => '0'
    );
\data_i_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => reset_n,
      D => \data_i[7]_i_1_n_0\,
      Q => data_i(7),
      R => '0'
    );
delayEn_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A2AAAEAA00000000"
    )
        port map (
      I0 => delayEn,
      I1 => \state_reg_n_0_[2]\,
      I2 => \state_reg_n_0_[3]\,
      I3 => delayEn_i_2_n_0,
      I4 => \state[3]_i_3_n_0\,
      I5 => reset_n,
      O => delayEn_i_1_n_0
    );
delayEn_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \state_reg_n_0_[1]\,
      I1 => \state_reg_n_0_[0]\,
      O => delayEn_i_2_n_0
    );
delayEn_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => delayEn_i_1_n_0,
      Q => delayEn,
      R => '0'
    );
delaycnt0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => delaycnt0_carry_n_0,
      CO(2) => delaycnt0_carry_n_1,
      CO(1) => delaycnt0_carry_n_2,
      CO(0) => delaycnt0_carry_n_3,
      CYINIT => delaycnt(0),
      DI(3 downto 0) => delaycnt(4 downto 1),
      O(3) => delaycnt0_carry_n_4,
      O(2) => delaycnt0_carry_n_5,
      O(1) => delaycnt0_carry_n_6,
      O(0) => delaycnt0_carry_n_7,
      S(3) => delaycnt0_carry_i_1_n_0,
      S(2) => delaycnt0_carry_i_2_n_0,
      S(1) => delaycnt0_carry_i_3_n_0,
      S(0) => delaycnt0_carry_i_4_n_0
    );
\delaycnt0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => delaycnt0_carry_n_0,
      CO(3) => \delaycnt0_carry__0_n_0\,
      CO(2) => \delaycnt0_carry__0_n_1\,
      CO(1) => \delaycnt0_carry__0_n_2\,
      CO(0) => \delaycnt0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(8 downto 5),
      O(3) => \delaycnt0_carry__0_n_4\,
      O(2) => \delaycnt0_carry__0_n_5\,
      O(1) => \delaycnt0_carry__0_n_6\,
      O(0) => \delaycnt0_carry__0_n_7\,
      S(3) => \delaycnt0_carry__0_i_1_n_0\,
      S(2) => \delaycnt0_carry__0_i_2_n_0\,
      S(1) => \delaycnt0_carry__0_i_3_n_0\,
      S(0) => \delaycnt0_carry__0_i_4_n_0\
    );
\delaycnt0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(8),
      O => \delaycnt0_carry__0_i_1_n_0\
    );
\delaycnt0_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(7),
      O => \delaycnt0_carry__0_i_2_n_0\
    );
\delaycnt0_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(6),
      O => \delaycnt0_carry__0_i_3_n_0\
    );
\delaycnt0_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(5),
      O => \delaycnt0_carry__0_i_4_n_0\
    );
\delaycnt0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__0_n_0\,
      CO(3) => \delaycnt0_carry__1_n_0\,
      CO(2) => \delaycnt0_carry__1_n_1\,
      CO(1) => \delaycnt0_carry__1_n_2\,
      CO(0) => \delaycnt0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(12 downto 9),
      O(3) => \delaycnt0_carry__1_n_4\,
      O(2) => \delaycnt0_carry__1_n_5\,
      O(1) => \delaycnt0_carry__1_n_6\,
      O(0) => \delaycnt0_carry__1_n_7\,
      S(3) => \delaycnt0_carry__1_i_1_n_0\,
      S(2) => \delaycnt0_carry__1_i_2_n_0\,
      S(1) => \delaycnt0_carry__1_i_3_n_0\,
      S(0) => \delaycnt0_carry__1_i_4_n_0\
    );
\delaycnt0_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(12),
      O => \delaycnt0_carry__1_i_1_n_0\
    );
\delaycnt0_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(11),
      O => \delaycnt0_carry__1_i_2_n_0\
    );
\delaycnt0_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(10),
      O => \delaycnt0_carry__1_i_3_n_0\
    );
\delaycnt0_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(9),
      O => \delaycnt0_carry__1_i_4_n_0\
    );
\delaycnt0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__1_n_0\,
      CO(3) => \delaycnt0_carry__2_n_0\,
      CO(2) => \delaycnt0_carry__2_n_1\,
      CO(1) => \delaycnt0_carry__2_n_2\,
      CO(0) => \delaycnt0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(16 downto 13),
      O(3) => \delaycnt0_carry__2_n_4\,
      O(2) => \delaycnt0_carry__2_n_5\,
      O(1) => \delaycnt0_carry__2_n_6\,
      O(0) => \delaycnt0_carry__2_n_7\,
      S(3) => \delaycnt0_carry__2_i_1_n_0\,
      S(2) => \delaycnt0_carry__2_i_2_n_0\,
      S(1) => \delaycnt0_carry__2_i_3_n_0\,
      S(0) => \delaycnt0_carry__2_i_4_n_0\
    );
\delaycnt0_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(16),
      O => \delaycnt0_carry__2_i_1_n_0\
    );
\delaycnt0_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(15),
      O => \delaycnt0_carry__2_i_2_n_0\
    );
\delaycnt0_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(14),
      O => \delaycnt0_carry__2_i_3_n_0\
    );
\delaycnt0_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(13),
      O => \delaycnt0_carry__2_i_4_n_0\
    );
\delaycnt0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__2_n_0\,
      CO(3) => \delaycnt0_carry__3_n_0\,
      CO(2) => \delaycnt0_carry__3_n_1\,
      CO(1) => \delaycnt0_carry__3_n_2\,
      CO(0) => \delaycnt0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(20 downto 17),
      O(3) => \delaycnt0_carry__3_n_4\,
      O(2) => \delaycnt0_carry__3_n_5\,
      O(1) => \delaycnt0_carry__3_n_6\,
      O(0) => \delaycnt0_carry__3_n_7\,
      S(3) => \delaycnt0_carry__3_i_1_n_0\,
      S(2) => \delaycnt0_carry__3_i_2_n_0\,
      S(1) => \delaycnt0_carry__3_i_3_n_0\,
      S(0) => \delaycnt0_carry__3_i_4_n_0\
    );
\delaycnt0_carry__3_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(20),
      O => \delaycnt0_carry__3_i_1_n_0\
    );
\delaycnt0_carry__3_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(19),
      O => \delaycnt0_carry__3_i_2_n_0\
    );
\delaycnt0_carry__3_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(18),
      O => \delaycnt0_carry__3_i_3_n_0\
    );
\delaycnt0_carry__3_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(17),
      O => \delaycnt0_carry__3_i_4_n_0\
    );
\delaycnt0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__3_n_0\,
      CO(3) => \delaycnt0_carry__4_n_0\,
      CO(2) => \delaycnt0_carry__4_n_1\,
      CO(1) => \delaycnt0_carry__4_n_2\,
      CO(0) => \delaycnt0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(24 downto 21),
      O(3) => \delaycnt0_carry__4_n_4\,
      O(2) => \delaycnt0_carry__4_n_5\,
      O(1) => \delaycnt0_carry__4_n_6\,
      O(0) => \delaycnt0_carry__4_n_7\,
      S(3) => \delaycnt0_carry__4_i_1_n_0\,
      S(2) => \delaycnt0_carry__4_i_2_n_0\,
      S(1) => \delaycnt0_carry__4_i_3_n_0\,
      S(0) => \delaycnt0_carry__4_i_4_n_0\
    );
\delaycnt0_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(24),
      O => \delaycnt0_carry__4_i_1_n_0\
    );
\delaycnt0_carry__4_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(23),
      O => \delaycnt0_carry__4_i_2_n_0\
    );
\delaycnt0_carry__4_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(22),
      O => \delaycnt0_carry__4_i_3_n_0\
    );
\delaycnt0_carry__4_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(21),
      O => \delaycnt0_carry__4_i_4_n_0\
    );
\delaycnt0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__4_n_0\,
      CO(3) => \delaycnt0_carry__5_n_0\,
      CO(2) => \delaycnt0_carry__5_n_1\,
      CO(1) => \delaycnt0_carry__5_n_2\,
      CO(0) => \delaycnt0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => delaycnt(28 downto 25),
      O(3) => \delaycnt0_carry__5_n_4\,
      O(2) => \delaycnt0_carry__5_n_5\,
      O(1) => \delaycnt0_carry__5_n_6\,
      O(0) => \delaycnt0_carry__5_n_7\,
      S(3) => \delaycnt0_carry__5_i_1_n_0\,
      S(2) => \delaycnt0_carry__5_i_2_n_0\,
      S(1) => \delaycnt0_carry__5_i_3_n_0\,
      S(0) => \delaycnt0_carry__5_i_4_n_0\
    );
\delaycnt0_carry__5_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(28),
      O => \delaycnt0_carry__5_i_1_n_0\
    );
\delaycnt0_carry__5_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(27),
      O => \delaycnt0_carry__5_i_2_n_0\
    );
\delaycnt0_carry__5_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(26),
      O => \delaycnt0_carry__5_i_3_n_0\
    );
\delaycnt0_carry__5_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(25),
      O => \delaycnt0_carry__5_i_4_n_0\
    );
\delaycnt0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \delaycnt0_carry__5_n_0\,
      CO(3 downto 2) => \NLW_delaycnt0_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \delaycnt0_carry__6_n_2\,
      CO(0) => \delaycnt0_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => delaycnt(30 downto 29),
      O(3) => \NLW_delaycnt0_carry__6_O_UNCONNECTED\(3),
      O(2) => \delaycnt0_carry__6_n_5\,
      O(1) => \delaycnt0_carry__6_n_6\,
      O(0) => \delaycnt0_carry__6_n_7\,
      S(3) => '0',
      S(2) => \delaycnt0_carry__6_i_1_n_0\,
      S(1) => \delaycnt0_carry__6_i_2_n_0\,
      S(0) => \delaycnt0_carry__6_i_3_n_0\
    );
\delaycnt0_carry__6_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(31),
      O => \delaycnt0_carry__6_i_1_n_0\
    );
\delaycnt0_carry__6_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(30),
      O => \delaycnt0_carry__6_i_2_n_0\
    );
\delaycnt0_carry__6_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(29),
      O => \delaycnt0_carry__6_i_3_n_0\
    );
delaycnt0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(4),
      O => delaycnt0_carry_i_1_n_0
    );
delaycnt0_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(3),
      O => delaycnt0_carry_i_2_n_0
    );
delaycnt0_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(2),
      O => delaycnt0_carry_i_3_n_0
    );
delaycnt0_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(1),
      O => delaycnt0_carry_i_4_n_0
    );
\delaycnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delaycnt(0),
      O => \delaycnt[0]_i_1_n_0\
    );
\delaycnt[31]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => delayEn,
      O => delaycnt0
    );
\delaycnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt[0]_i_1_n_0\,
      Q => delaycnt(0),
      R => delaycnt0
    );
\delaycnt_reg[10]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__1_n_6\,
      Q => delaycnt(10),
      S => delaycnt0
    );
\delaycnt_reg[11]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__1_n_5\,
      Q => delaycnt(11),
      S => delaycnt0
    );
\delaycnt_reg[12]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__1_n_4\,
      Q => delaycnt(12),
      S => delaycnt0
    );
\delaycnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__2_n_7\,
      Q => delaycnt(13),
      R => delaycnt0
    );
\delaycnt_reg[14]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__2_n_6\,
      Q => delaycnt(14),
      S => delaycnt0
    );
\delaycnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__2_n_5\,
      Q => delaycnt(15),
      R => delaycnt0
    );
\delaycnt_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__2_n_4\,
      Q => delaycnt(16),
      R => delaycnt0
    );
\delaycnt_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__3_n_7\,
      Q => delaycnt(17),
      R => delaycnt0
    );
\delaycnt_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__3_n_6\,
      Q => delaycnt(18),
      R => delaycnt0
    );
\delaycnt_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__3_n_5\,
      Q => delaycnt(19),
      R => delaycnt0
    );
\delaycnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => delaycnt0_carry_n_7,
      Q => delaycnt(1),
      R => delaycnt0
    );
\delaycnt_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__3_n_4\,
      Q => delaycnt(20),
      R => delaycnt0
    );
\delaycnt_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__4_n_7\,
      Q => delaycnt(21),
      R => delaycnt0
    );
\delaycnt_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__4_n_6\,
      Q => delaycnt(22),
      R => delaycnt0
    );
\delaycnt_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__4_n_5\,
      Q => delaycnt(23),
      R => delaycnt0
    );
\delaycnt_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__4_n_4\,
      Q => delaycnt(24),
      R => delaycnt0
    );
\delaycnt_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__5_n_7\,
      Q => delaycnt(25),
      R => delaycnt0
    );
\delaycnt_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__5_n_6\,
      Q => delaycnt(26),
      R => delaycnt0
    );
\delaycnt_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__5_n_5\,
      Q => delaycnt(27),
      R => delaycnt0
    );
\delaycnt_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__5_n_4\,
      Q => delaycnt(28),
      R => delaycnt0
    );
\delaycnt_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__6_n_7\,
      Q => delaycnt(29),
      R => delaycnt0
    );
\delaycnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => delaycnt0_carry_n_6,
      Q => delaycnt(2),
      R => delaycnt0
    );
\delaycnt_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__6_n_6\,
      Q => delaycnt(30),
      R => delaycnt0
    );
\delaycnt_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__6_n_5\,
      Q => delaycnt(31),
      R => delaycnt0
    );
\delaycnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => delaycnt0_carry_n_5,
      Q => delaycnt(3),
      R => delaycnt0
    );
\delaycnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => delaycnt0_carry_n_4,
      Q => delaycnt(4),
      R => delaycnt0
    );
\delaycnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__0_n_7\,
      Q => delaycnt(5),
      R => delaycnt0
    );
\delaycnt_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__0_n_6\,
      Q => delaycnt(6),
      S => delaycnt0
    );
\delaycnt_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__0_n_5\,
      Q => delaycnt(7),
      S => delaycnt0
    );
\delaycnt_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__0_n_4\,
      Q => delaycnt(8),
      S => delaycnt0
    );
\delaycnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => \delaycnt0_carry__1_n_7\,
      Q => delaycnt(9),
      R => delaycnt0
    );
\initA[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      O => \initA[0]_i_1_n_0\
    );
\initA[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      I1 => \initA_reg_n_0_[1]\,
      O => \p_1_in__0\(1)
    );
\initA[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[0]\,
      O => \p_1_in__0\(2)
    );
\initA[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[0]\,
      I2 => \initA_reg_n_0_[1]\,
      I3 => \initA_reg_n_0_[3]\,
      O => \p_1_in__0\(3)
    );
\initA[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \initA_reg_n_0_[4]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[0]\,
      I3 => \initA_reg_n_0_[2]\,
      I4 => \initA_reg_n_0_[1]\,
      O => \p_1_in__0\(4)
    );
\initA[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[4]\,
      I2 => \initA_reg_n_0_[1]\,
      I3 => \initA_reg_n_0_[2]\,
      I4 => \initA_reg_n_0_[0]\,
      I5 => \initA_reg_n_0_[3]\,
      O => \p_1_in__0\(5)
    );
\initA[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \initA_reg_n_0_[6]\,
      I1 => \initA_reg_n_0_[5]\,
      I2 => \initA[6]_i_4_n_0\,
      O => \p_1_in__0\(6)
    );
\initA[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \state[3]_i_3_n_0\,
      I1 => \state_reg_n_0_[3]\,
      I2 => initEn,
      I3 => \state_reg_n_0_[2]\,
      I4 => \state_reg_n_0_[0]\,
      I5 => \state_reg_n_0_[1]\,
      O => \initA[6]_i_3_n_0\
    );
\initA[6]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \initA_reg_n_0_[3]\,
      I1 => \initA_reg_n_0_[0]\,
      I2 => \initA_reg_n_0_[2]\,
      I3 => \initA_reg_n_0_[1]\,
      I4 => \initA_reg_n_0_[4]\,
      O => \initA[6]_i_4_n_0\
    );
\initA_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \initA[0]_i_1_n_0\,
      Q => \initA_reg_n_0_[0]\,
      R => SR(0)
    );
\initA_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(1),
      Q => \initA_reg_n_0_[1]\,
      R => SR(0)
    );
\initA_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(2),
      Q => \initA_reg_n_0_[2]\,
      R => SR(0)
    );
\initA_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(3),
      Q => \initA_reg_n_0_[3]\,
      R => SR(0)
    );
\initA_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(4),
      Q => \initA_reg_n_0_[4]\,
      R => SR(0)
    );
\initA_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(5),
      Q => \initA_reg_n_0_[5]\,
      R => SR(0)
    );
\initA_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_5,
      D => \p_1_in__0\(6),
      Q => \initA_reg_n_0_[6]\,
      R => SR(0)
    );
initEn_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => '1',
      D => twi_controller_n_6,
      Q => initEn,
      R => '0'
    );
\initWord[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[0]\,
      O => \initWord[0]_i_1_n_0\
    );
\initWord[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000C0A6010"
    )
        port map (
      I0 => \initA_reg_n_0_[1]\,
      I1 => \initA_reg_n_0_[0]\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[2]\,
      I4 => \initA_reg_n_0_[4]\,
      I5 => \initA_reg_n_0_[5]\,
      O => \initWord[10]_i_1_n_0\
    );
\initWord[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10101110"
    )
        port map (
      I0 => \initA_reg_n_0_[1]\,
      I1 => \initA_reg_n_0_[0]\,
      I2 => \initA_reg_n_0_[5]\,
      I3 => \initA_reg_n_0_[3]\,
      I4 => \initA_reg_n_0_[4]\,
      O => \initWord[11]_i_1_n_0\
    );
\initWord[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000006"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[0]\,
      I3 => \initA_reg_n_0_[1]\,
      I4 => \initA_reg_n_0_[5]\,
      I5 => \initA_reg_n_0_[4]\,
      O => \initWord[12]_i_1_n_0\
    );
\initWord[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C1C0C0C8C4C9C0C8"
    )
        port map (
      I0 => \initA_reg_n_0_[4]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[5]\,
      I3 => \initA_reg_n_0_[2]\,
      I4 => \initA_reg_n_0_[3]\,
      I5 => \initA_reg_n_0_[0]\,
      O => \initWord[13]_i_1_n_0\
    );
\initWord[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010180884"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[1]\,
      I3 => \initA_reg_n_0_[0]\,
      I4 => \initA_reg_n_0_[4]\,
      I5 => \initA_reg_n_0_[5]\,
      O => \initWord[14]_i_1_n_0\
    );
\initWord[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010000900180000"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[4]\,
      I3 => \initA_reg_n_0_[5]\,
      I4 => \initA_reg_n_0_[1]\,
      I5 => \initA_reg_n_0_[0]\,
      O => \initWord[15]_i_1_n_0\
    );
\initWord[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00006AB50000A415"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[4]\,
      I4 => \initA_reg_n_0_[5]\,
      I5 => \initA_reg_n_0_[2]\,
      O => \initWord[16]_i_1_n_0\
    );
\initWord[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1E1F1F0B54040450"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[4]\,
      I2 => \initA_reg_n_0_[1]\,
      I3 => \initA_reg_n_0_[3]\,
      I4 => \initA_reg_n_0_[2]\,
      I5 => \initA_reg_n_0_[0]\,
      O => \initWord[17]_i_1_n_0\
    );
\initWord[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB54EE00EE44EB11"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[4]\,
      I2 => \initA_reg_n_0_[0]\,
      I3 => \initA_reg_n_0_[1]\,
      I4 => \initA_reg_n_0_[3]\,
      I5 => \initA_reg_n_0_[2]\,
      O => \initWord[18]_i_1_n_0\
    );
\initWord[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCC7B16CCCC7A1A"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[4]\,
      I4 => \initA_reg_n_0_[5]\,
      I5 => \initA_reg_n_0_[0]\,
      O => \initWord[19]_i_1_n_0\
    );
\initWord[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0908080909090307"
    )
        port map (
      I0 => \initA_reg_n_0_[3]\,
      I1 => \initA_reg_n_0_[4]\,
      I2 => \initA_reg_n_0_[5]\,
      I3 => \initA_reg_n_0_[0]\,
      I4 => \initA_reg_n_0_[1]\,
      I5 => \initA_reg_n_0_[2]\,
      O => \initWord[20]_i_1_n_0\
    );
\initWord[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000C00EFEF8"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      I1 => \initA_reg_n_0_[1]\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[2]\,
      I4 => \initA_reg_n_0_[4]\,
      I5 => \initA_reg_n_0_[5]\,
      O => \initWord[21]_i_1_n_0\
    );
\initWord[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080003400800004"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[1]\,
      I3 => \initA_reg_n_0_[5]\,
      I4 => \initA_reg_n_0_[4]\,
      I5 => \initA_reg_n_0_[0]\,
      O => \initWord[23]_i_1_n_0\
    );
\initWord[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000055555557"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initWord[30]_i_3_n_0\,
      I2 => \initA_reg_n_0_[3]\,
      I3 => \initA_reg_n_0_[2]\,
      I4 => \initA_reg_n_0_[4]\,
      I5 => \initA_reg_n_0_[6]\,
      O => \initWord[30]_i_1_n_0\
    );
\initWord[30]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \initA_reg_n_0_[5]\,
      I1 => \initA_reg_n_0_[1]\,
      O => \initWord[30]_i_2_n_0\
    );
\initWord[30]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      I1 => \initA_reg_n_0_[1]\,
      O => \initWord[30]_i_3_n_0\
    );
\initWord[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \initA_reg_n_0_[1]\,
      I1 => \initA_reg_n_0_[0]\,
      I2 => \initA_reg_n_0_[5]\,
      O => \initWord[6]_i_1_n_0\
    );
\initWord[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF2EFFE60066FFFF"
    )
        port map (
      I0 => \initA_reg_n_0_[2]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[4]\,
      I3 => \initA_reg_n_0_[5]\,
      I4 => \initA_reg_n_0_[0]\,
      I5 => \initA_reg_n_0_[1]\,
      O => \initWord[8]_i_1_n_0\
    );
\initWord[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0003000C00D400BC"
    )
        port map (
      I0 => \initA_reg_n_0_[0]\,
      I1 => \initA_reg_n_0_[3]\,
      I2 => \initA_reg_n_0_[2]\,
      I3 => \initA_reg_n_0_[5]\,
      I4 => \initA_reg_n_0_[1]\,
      I5 => \initA_reg_n_0_[4]\,
      O => \initWord[9]_i_1_n_0\
    );
\initWord_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[0]_i_1_n_0\,
      Q => \initWord_reg_n_0_[0]\,
      R => '0'
    );
\initWord_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[10]_i_1_n_0\,
      Q => data2(2),
      R => '0'
    );
\initWord_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[11]_i_1_n_0\,
      Q => data2(3),
      R => '0'
    );
\initWord_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[12]_i_1_n_0\,
      Q => data2(4),
      R => '0'
    );
\initWord_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[13]_i_1_n_0\,
      Q => data2(5),
      R => '0'
    );
\initWord_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[14]_i_1_n_0\,
      Q => data2(6),
      R => '0'
    );
\initWord_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[15]_i_1_n_0\,
      Q => data2(7),
      R => '0'
    );
\initWord_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[16]_i_1_n_0\,
      Q => data1(0),
      R => '0'
    );
\initWord_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[17]_i_1_n_0\,
      Q => data1(1),
      R => '0'
    );
\initWord_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[18]_i_1_n_0\,
      Q => data1(2),
      R => '0'
    );
\initWord_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[19]_i_1_n_0\,
      Q => data1(3),
      R => '0'
    );
\initWord_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[20]_i_1_n_0\,
      Q => data1(4),
      R => '0'
    );
\initWord_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[21]_i_1_n_0\,
      Q => data1(5),
      R => '0'
    );
\initWord_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[23]_i_1_n_0\,
      Q => data1(7),
      R => '0'
    );
\initWord_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[30]_i_2_n_0\,
      Q => data0(6),
      R => '0'
    );
\initWord_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[6]_i_1_n_0\,
      Q => \initWord_reg_n_0_[6]\,
      R => '0'
    );
\initWord_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[8]_i_1_n_0\,
      Q => data2(0),
      R => '0'
    );
\initWord_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => \initWord[30]_i_1_n_0\,
      D => \initWord[9]_i_1_n_0\,
      Q => data2(1),
      R => '0'
    );
msg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \state_reg_n_0_[3]\,
      I1 => \state_reg_n_0_[2]\,
      I2 => \state_reg_n_0_[0]\,
      I3 => \state_reg_n_0_[1]\,
      O => msg0
    );
msg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => reset_n,
      D => msg0,
      Q => msg,
      R => '0'
    );
\state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200000000000"
    )
        port map (
      I0 => \state[1]_i_4_n_0\,
      I1 => \initA_reg_n_0_[6]\,
      I2 => \initA_reg_n_0_[5]\,
      I3 => \state[1]_i_5_n_0\,
      I4 => \initA_reg_n_0_[4]\,
      I5 => \state[1]_i_6_n_0\,
      O => \state[1]_i_3_n_0\
    );
\state[1]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => data1(2),
      I1 => data1(7),
      I2 => data1(0),
      I3 => data1(1),
      I4 => \state[3]_i_9_n_0\,
      O => \state[1]_i_4_n_0\
    );
\state[1]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \initA_reg_n_0_[1]\,
      I1 => \initA_reg_n_0_[0]\,
      O => \state[1]_i_5_n_0\
    );
\state[1]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \initA_reg_n_0_[3]\,
      I1 => \initA_reg_n_0_[2]\,
      O => \state[1]_i_6_n_0\
    );
\state[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40404044"
    )
        port map (
      I0 => \state_reg_n_0_[3]\,
      I1 => \state_reg_n_0_[1]\,
      I2 => \state_reg_n_0_[0]\,
      I3 => \initWord_reg_n_0_[0]\,
      I4 => \initWord_reg_n_0_[6]\,
      O => \state[2]_i_2_n_0\
    );
\state[3]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => delaycnt(10),
      I1 => delaycnt(11),
      I2 => delaycnt(9),
      I3 => delaycnt(8),
      O => \state[3]_i_10_n_0\
    );
\state[3]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => delaycnt(3),
      I1 => delaycnt(2),
      I2 => delaycnt(1),
      I3 => delaycnt(0),
      O => \state[3]_i_11_n_0\
    );
\state[3]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => delaycnt(19),
      I1 => delaycnt(18),
      I2 => delaycnt(16),
      I3 => delaycnt(17),
      O => \state[3]_i_12_n_0\
    );
\state[3]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => delaycnt(31),
      I1 => delaycnt(30),
      I2 => delaycnt(28),
      I3 => delaycnt(29),
      O => \state[3]_i_13_n_0\
    );
\state[3]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \state[3]_i_5_n_0\,
      I1 => \state[3]_i_6_n_0\,
      I2 => \state[3]_i_7_n_0\,
      I3 => \state[3]_i_8_n_0\,
      O => \state[3]_i_3_n_0\
    );
\state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => \state_reg_n_0_[1]\,
      I1 => \state[3]_i_9_n_0\,
      I2 => data1(1),
      I3 => data1(0),
      I4 => data1(7),
      I5 => data1(2),
      O => \state[3]_i_4_n_0\
    );
\state[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => delaycnt(12),
      I1 => delaycnt(13),
      I2 => delaycnt(14),
      I3 => delaycnt(15),
      I4 => \state[3]_i_10_n_0\,
      O => \state[3]_i_5_n_0\
    );
\state[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => delaycnt(4),
      I1 => delaycnt(7),
      I2 => delaycnt(5),
      I3 => delaycnt(6),
      I4 => \state[3]_i_11_n_0\,
      O => \state[3]_i_6_n_0\
    );
\state[3]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => delaycnt(23),
      I1 => delaycnt(22),
      I2 => delaycnt(20),
      I3 => delaycnt(21),
      I4 => \state[3]_i_12_n_0\,
      O => \state[3]_i_7_n_0\
    );
\state[3]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => delaycnt(24),
      I1 => delaycnt(26),
      I2 => delaycnt(27),
      I3 => delaycnt(25),
      I4 => \state[3]_i_13_n_0\,
      O => \state[3]_i_8_n_0\
    );
\state[3]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \state_reg_n_0_[0]\,
      I1 => data1(3),
      I2 => data1(4),
      I3 => data1(5),
      O => \state[3]_i_9_n_0\
    );
\state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_4,
      D => twi_controller_n_3,
      Q => \state_reg_n_0_[0]\,
      R => SR(0)
    );
\state_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_4,
      D => twi_controller_n_2,
      Q => \state_reg_n_0_[1]\,
      S => SR(0)
    );
\state_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_4,
      D => twi_controller_n_1,
      Q => \state_reg_n_0_[2]\,
      S => SR(0)
    );
\state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => twi_controller_n_4,
      D => twi_controller_n_0,
      Q => \state_reg_n_0_[3]\,
      R => SR(0)
    );
stb_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8055"
    )
        port map (
      I0 => \state_reg_n_0_[3]\,
      I1 => \state_reg_n_0_[1]\,
      I2 => \state_reg_n_0_[0]\,
      I3 => \state_reg_n_0_[2]\,
      O => stb_i_1_n_0
    );
stb_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_out2,
      CE => reset_n,
      D => stb_i_1_n_0,
      Q => stb,
      R => '0'
    );
twi_controller: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl
     port map (
      D(3) => twi_controller_n_0,
      D(2) => twi_controller_n_1,
      D(1) => twi_controller_n_2,
      D(0) => twi_controller_n_3,
      E(0) => twi_controller_n_4,
      Q(3) => \state_reg_n_0_[3]\,
      Q(2) => \state_reg_n_0_[2]\,
      Q(1) => \state_reg_n_0_[1]\,
      Q(0) => \state_reg_n_0_[0]\,
      clk_out2 => clk_out2,
      data_i(7 downto 0) => data_i(7 downto 0),
      \delaycnt_reg[12]\ => \state[3]_i_3_n_0\,
      \initA_reg[6]\(0) => twi_controller_n_5,
      \initA_reg[6]_0\ => \state[1]_i_3_n_0\,
      initEn => initEn,
      initEn_reg => twi_controller_n_6,
      \initWord_reg[6]\(1) => \initWord_reg_n_0_[6]\,
      \initWord_reg[6]\(0) => \initWord_reg_n_0_[0]\,
      msg => msg,
      reset_n => reset_n,
      scl => scl,
      sda => sda,
      \state_reg[1]\ => \state[3]_i_4_n_0\,
      \state_reg[3]\ => \state[2]_i_2_n_0\,
      \state_reg[3]_0\ => \initA[6]_i_3_n_0\,
      stb => stb
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    lopt : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz
     port map (
      clk_in1 => clk_in1,
      clk_out1 => clk_out1,
      clk_out2 => clk_out2,
      clk_out3 => clk_out3,
      lopt => lopt,
      resetn => resetn
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1 is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    lopt : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1 is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz
     port map (
      clk_in1 => clk_in1,
      clk_out1 => clk_out1,
      clk_out2 => clk_out2,
      lopt => lopt,
      resetn => resetn
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid is
  port (
    red_s : out STD_LOGIC;
    green_s : out STD_LOGIC;
    blue_s : out STD_LOGIC;
    clock_s : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[0]\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_0\ : out STD_LOGIC;
    \encoded_reg[0]_0\ : out STD_LOGIC;
    \encoded_reg[0]_1\ : out STD_LOGIC;
    \encoded_reg[0]_2\ : out STD_LOGIC;
    \encoded_reg[0]_3\ : out STD_LOGIC;
    \encoded_reg[0]_4\ : out STD_LOGIC;
    \encoded_reg[0]_5\ : out STD_LOGIC;
    \encoded_reg[0]_6\ : out STD_LOGIC;
    \dc_bias_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[8]\ : out STD_LOGIC;
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \state_reg[0]\ : out STD_LOGIC;
    \state_reg[0]_0\ : out STD_LOGIC;
    \encoded_reg[8]_1\ : out STD_LOGIC;
    \state_reg[0]_1\ : out STD_LOGIC;
    \state_reg[0]_2\ : out STD_LOGIC;
    \encoded_reg[8]_2\ : out STD_LOGIC;
    \encoded_reg[8]_3\ : out STD_LOGIC;
    \encoded_reg[8]_4\ : out STD_LOGIC;
    \state_reg[0]_3\ : out STD_LOGIC;
    \encoded_reg[8]_5\ : out STD_LOGIC;
    \encoded_reg[8]_6\ : out STD_LOGIC;
    \encoded_reg[8]_7\ : out STD_LOGIC;
    \encoded_reg[8]_8\ : out STD_LOGIC;
    \encoded_reg[8]_9\ : out STD_LOGIC;
    \encoded_reg[8]_10\ : out STD_LOGIC;
    \encoded_reg[8]_11\ : out STD_LOGIC;
    \encoded_reg[8]_12\ : out STD_LOGIC;
    \encoded_reg[8]_13\ : out STD_LOGIC;
    \encoded_reg[8]_14\ : out STD_LOGIC;
    \encoded_reg[8]_15\ : out STD_LOGIC;
    \encoded_reg[8]_16\ : out STD_LOGIC;
    \encoded_reg[8]_17\ : out STD_LOGIC;
    \encoded_reg[8]_18\ : out STD_LOGIC;
    \encoded_reg[8]_19\ : out STD_LOGIC;
    \encoded_reg[8]_20\ : out STD_LOGIC;
    \state_reg[0]_4\ : out STD_LOGIC;
    \encoded_reg[8]_21\ : out STD_LOGIC;
    \encoded_reg[9]\ : out STD_LOGIC;
    clk_out2 : in STD_LOGIC;
    clk_out3 : in STD_LOGIC;
    \processQ_reg[5]\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \dc_bias_reg[3]\ : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC;
    \dc_bias_reg[3]_1\ : in STD_LOGIC;
    \processQ_reg[6]\ : in STD_LOGIC;
    \dc_bias_reg[3]_2\ : in STD_LOGIC;
    \dc_bias_reg[3]_3\ : in STD_LOGIC;
    \dc_bias_reg[3]_4\ : in STD_LOGIC;
    \dc_bias_reg[3]_5\ : in STD_LOGIC;
    \dc_bias_reg[3]_6\ : in STD_LOGIC;
    \processQ_reg[9]\ : in STD_LOGIC;
    \processQ_reg[5]_0\ : in STD_LOGIC;
    \processQ_reg[5]_1\ : in STD_LOGIC;
    \slv_reg10_reg[7]\ : in STD_LOGIC;
    \slv_reg10_reg[2]\ : in STD_LOGIC;
    \slv_reg10_reg[4]\ : in STD_LOGIC;
    \slv_reg10_reg[6]\ : in STD_LOGIC;
    \slv_reg10_reg[1]\ : in STD_LOGIC;
    \slv_reg10_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg5_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \int_trigger_time_s_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \processQ_reg[8]\ : in STD_LOGIC;
    \processQ_reg[8]_0\ : in STD_LOGIC;
    \processQ_reg[9]_0\ : in STD_LOGIC;
    \slv_reg11_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \int_trigger_volt_s_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid is
  signal D0 : STD_LOGIC;
  signal D1 : STD_LOGIC;
  signal TDMS_encoder_blue_n_0 : STD_LOGIC;
  signal TDMS_encoder_blue_n_1 : STD_LOGIC;
  signal TDMS_encoder_blue_n_2 : STD_LOGIC;
  signal TDMS_encoder_blue_n_3 : STD_LOGIC;
  signal TDMS_encoder_blue_n_4 : STD_LOGIC;
  signal TDMS_encoder_blue_n_5 : STD_LOGIC;
  signal TDMS_encoder_green_n_0 : STD_LOGIC;
  signal TDMS_encoder_green_n_1 : STD_LOGIC;
  signal TDMS_encoder_green_n_2 : STD_LOGIC;
  signal TDMS_encoder_green_n_3 : STD_LOGIC;
  signal TDMS_encoder_red_n_0 : STD_LOGIC;
  signal TDMS_encoder_red_n_1 : STD_LOGIC;
  signal TDMS_encoder_red_n_2 : STD_LOGIC;
  signal TDMS_encoder_red_n_3 : STD_LOGIC;
  signal TDMS_encoder_red_n_4 : STD_LOGIC;
  signal data1 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \^encoded_reg[8]_0\ : STD_LOGIC;
  signal \^encoded_reg[8]_3\ : STD_LOGIC;
  signal \^encoded_reg[8]_5\ : STD_LOGIC;
  signal \^encoded_reg[8]_6\ : STD_LOGIC;
  signal latched_blue : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal latched_green : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal latched_red : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal shift_blue : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \shift_blue[1]_i_1_n_0\ : STD_LOGIC;
  signal \shift_blue[3]_i_1_n_0\ : STD_LOGIC;
  signal \shift_blue[5]_i_1_n_0\ : STD_LOGIC;
  signal \shift_blue[7]_i_1_n_0\ : STD_LOGIC;
  signal \shift_blue[7]_i_2_n_0\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[0]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[1]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[2]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[3]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[4]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[5]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[6]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[7]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[8]\ : STD_LOGIC;
  signal \shift_blue_reg_n_0_[9]\ : STD_LOGIC;
  signal shift_clock : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \shift_clock__0\ : STD_LOGIC_VECTOR ( 9 downto 2 );
  signal shift_green : STD_LOGIC_VECTOR ( 6 downto 2 );
  signal \shift_green[0]_i_1_n_0\ : STD_LOGIC;
  signal \shift_green[1]_i_1_n_0\ : STD_LOGIC;
  signal \shift_green[3]_i_1_n_0\ : STD_LOGIC;
  signal \shift_green[5]_i_1_n_0\ : STD_LOGIC;
  signal \shift_green[7]_i_1_n_0\ : STD_LOGIC;
  signal \shift_green[7]_i_2_n_0\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[0]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[1]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[2]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[3]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[4]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[5]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[6]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[7]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[8]\ : STD_LOGIC;
  signal \shift_green_reg_n_0_[9]\ : STD_LOGIC;
  signal shift_red : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \shift_red[9]_i_1_n_0\ : STD_LOGIC;
  signal \shift_red[9]_i_2_n_0\ : STD_LOGIC;
  signal \^state_reg[0]\ : STD_LOGIC;
  signal \^state_reg[0]_0\ : STD_LOGIC;
  signal \^state_reg[0]_1\ : STD_LOGIC;
  signal \^state_reg[0]_2\ : STD_LOGIC;
  signal \^state_reg[0]_3\ : STD_LOGIC;
  signal NLW_ODDR2_blue_R_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_blue_S_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_clock_R_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_clock_S_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_green_R_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_green_S_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_red_R_UNCONNECTED : STD_LOGIC;
  signal NLW_ODDR2_red_S_UNCONNECTED : STD_LOGIC;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of ODDR2_blue : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ODDR2_blue : label is "ODDR2";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of ODDR2_blue : label is "D0:D1 D1:D2 C0:C";
  attribute \__SRVAL\ : string;
  attribute \__SRVAL\ of ODDR2_blue : label is "TRUE";
  attribute BOX_TYPE of ODDR2_clock : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of ODDR2_clock : label is "ODDR2";
  attribute XILINX_TRANSFORM_PINMAP of ODDR2_clock : label is "D0:D1 D1:D2 C0:C";
  attribute \__SRVAL\ of ODDR2_clock : label is "TRUE";
  attribute BOX_TYPE of ODDR2_green : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of ODDR2_green : label is "ODDR2";
  attribute XILINX_TRANSFORM_PINMAP of ODDR2_green : label is "D0:D1 D1:D2 C0:C";
  attribute \__SRVAL\ of ODDR2_green : label is "TRUE";
  attribute BOX_TYPE of ODDR2_red : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of ODDR2_red : label is "ODDR2";
  attribute XILINX_TRANSFORM_PINMAP of ODDR2_red : label is "D0:D1 D1:D2 C0:C";
  attribute \__SRVAL\ of ODDR2_red : label is "TRUE";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \shift_blue[0]_i_1\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \shift_blue[1]_i_1\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \shift_blue[2]_i_1\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \shift_blue[3]_i_1\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \shift_blue[4]_i_1\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \shift_blue[5]_i_1\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \shift_blue[6]_i_1\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \shift_blue[7]_i_2\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \shift_green[0]_i_1\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \shift_green[1]_i_1\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \shift_green[2]_i_1\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \shift_green[3]_i_1\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \shift_green[4]_i_1\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \shift_green[6]_i_1\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \shift_green[7]_i_2\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \shift_red[0]_i_1\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \shift_red[1]_i_1\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \shift_red[2]_i_1\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \shift_red[3]_i_1\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \shift_red[4]_i_1\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \shift_red[6]_i_1\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \shift_red[7]_i_1\ : label is "soft_lutpair85";
begin
  \encoded_reg[8]_0\ <= \^encoded_reg[8]_0\;
  \encoded_reg[8]_3\ <= \^encoded_reg[8]_3\;
  \encoded_reg[8]_5\ <= \^encoded_reg[8]_5\;
  \encoded_reg[8]_6\ <= \^encoded_reg[8]_6\;
  \state_reg[0]\ <= \^state_reg[0]\;
  \state_reg[0]_0\ <= \^state_reg[0]_0\;
  \state_reg[0]_1\ <= \^state_reg[0]_1\;
  \state_reg[0]_2\ <= \^state_reg[0]_2\;
  \state_reg[0]_3\ <= \^state_reg[0]_3\;
ODDR2_blue: unisim.vcomponents.ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE",
      INIT => '0',
      SRTYPE => "ASYNC"
    )
        port map (
      C => clk_out2,
      CE => '1',
      D1 => \shift_blue_reg_n_0_[0]\,
      D2 => \shift_blue_reg_n_0_[1]\,
      Q => blue_s,
      R => NLW_ODDR2_blue_R_UNCONNECTED,
      S => NLW_ODDR2_blue_S_UNCONNECTED
    );
ODDR2_clock: unisim.vcomponents.ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE",
      INIT => '0',
      SRTYPE => "ASYNC"
    )
        port map (
      C => clk_out2,
      CE => '1',
      D1 => shift_clock(0),
      D2 => shift_clock(1),
      Q => clock_s,
      R => NLW_ODDR2_clock_R_UNCONNECTED,
      S => NLW_ODDR2_clock_S_UNCONNECTED
    );
ODDR2_green: unisim.vcomponents.ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE",
      INIT => '0',
      SRTYPE => "ASYNC"
    )
        port map (
      C => clk_out2,
      CE => '1',
      D1 => \shift_green_reg_n_0_[0]\,
      D2 => \shift_green_reg_n_0_[1]\,
      Q => green_s,
      R => NLW_ODDR2_green_R_UNCONNECTED,
      S => NLW_ODDR2_green_S_UNCONNECTED
    );
ODDR2_red: unisim.vcomponents.ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE",
      INIT => '0',
      SRTYPE => "ASYNC"
    )
        port map (
      C => clk_out2,
      CE => '1',
      D1 => D0,
      D2 => D1,
      Q => red_s,
      R => NLW_ODDR2_red_R_UNCONNECTED,
      S => NLW_ODDR2_red_S_UNCONNECTED
    );
TDMS_encoder_blue: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder
     port map (
      CLK => CLK,
      D(5) => TDMS_encoder_blue_n_0,
      D(4) => TDMS_encoder_blue_n_1,
      D(3) => TDMS_encoder_blue_n_2,
      D(2) => TDMS_encoder_blue_n_3,
      D(1) => TDMS_encoder_blue_n_4,
      D(0) => TDMS_encoder_blue_n_5,
      Q(0) => \dc_bias_reg[0]\(0),
      SR(0) => SR(0),
      \dc_bias_reg[3]_0\ => \dc_bias_reg[3]_2\,
      \dc_bias_reg[3]_1\ => \dc_bias_reg[3]_3\,
      \dc_bias_reg[3]_2\ => \dc_bias_reg[3]_4\,
      \dc_bias_reg[3]_3\ => \dc_bias_reg[3]_5\,
      \dc_bias_reg[3]_4\ => \dc_bias_reg[3]_6\,
      \encoded_reg[8]_0\ => \encoded_reg[8]\,
      \encoded_reg[8]_1\ => \encoded_reg[8]_1\,
      \encoded_reg[8]_2\ => \encoded_reg[8]_4\,
      \encoded_reg[8]_3\ => \^encoded_reg[8]_5\,
      \encoded_reg[8]_4\ => \encoded_reg[8]_11\,
      \encoded_reg[8]_5\ => \encoded_reg[8]_12\,
      \encoded_reg[8]_6\ => \encoded_reg[8]_16\,
      \encoded_reg[9]_0\ => \encoded_reg[9]\,
      \int_trigger_volt_s_reg[1]\(1 downto 0) => \int_trigger_volt_s_reg[9]\(1 downto 0),
      \int_trigger_volt_s_reg[3]\ => \^encoded_reg[8]_6\,
      \processQ_reg[5]\ => \processQ_reg[5]_1\,
      \processQ_reg[5]_0\ => \processQ_reg[5]_0\,
      \processQ_reg[6]\ => \processQ_reg[6]\,
      \processQ_reg[8]\ => \processQ_reg[8]\,
      \processQ_reg[8]_0\ => \processQ_reg[8]_0\,
      \slv_reg11_reg[1]\ => \^state_reg[0]_3\,
      \slv_reg11_reg[1]_0\(1 downto 0) => \slv_reg11_reg[9]\(1 downto 0),
      \slv_reg11_reg[2]\ => \^encoded_reg[8]_3\,
      \slv_reg11_reg[3]\ => \^state_reg[0]_0\,
      \slv_reg11_reg[4]\ => \^state_reg[0]_1\,
      \slv_reg11_reg[5]\ => \^state_reg[0]\,
      \slv_reg11_reg[6]\ => \^state_reg[0]_2\,
      \slv_reg11_reg[7]\ => \^encoded_reg[8]_0\,
      \slv_reg5_reg[0]\(0) => \slv_reg5_reg[0]\(0)
    );
TDMS_encoder_green: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1
     port map (
      CLK => CLK,
      D(3) => TDMS_encoder_green_n_0,
      D(2) => TDMS_encoder_green_n_1,
      D(1) => TDMS_encoder_green_n_2,
      D(0) => TDMS_encoder_green_n_3,
      Q(0) => Q(0),
      SR(0) => SR(0),
      \dc_bias_reg[3]_0\ => \dc_bias_reg[3]_0\,
      \dc_bias_reg[3]_1\ => \dc_bias_reg[3]_1\,
      \processQ_reg[5]\ => \processQ_reg[5]_0\,
      \processQ_reg[9]\ => \processQ_reg[9]\
    );
TDMS_encoder_red: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2
     port map (
      CLK => CLK,
      D(4) => TDMS_encoder_red_n_0,
      D(3) => TDMS_encoder_red_n_1,
      D(2) => TDMS_encoder_red_n_2,
      D(1) => TDMS_encoder_red_n_3,
      D(0) => TDMS_encoder_red_n_4,
      Q(0) => \encoded_reg[3]\(0),
      SR(0) => SR(0),
      \dc_bias_reg[3]_0\ => \dc_bias_reg[3]\,
      \encoded_reg[0]_0\ => \encoded_reg[0]\,
      \encoded_reg[0]_1\ => \encoded_reg[0]_0\,
      \encoded_reg[0]_2\ => \encoded_reg[0]_1\,
      \encoded_reg[0]_3\ => \encoded_reg[0]_2\,
      \encoded_reg[0]_4\ => \encoded_reg[0]_3\,
      \encoded_reg[0]_5\ => \encoded_reg[0]_4\,
      \encoded_reg[0]_6\ => \encoded_reg[0]_5\,
      \encoded_reg[0]_7\ => \encoded_reg[0]_6\,
      \encoded_reg[8]_0\ => \encoded_reg[8]_2\,
      \encoded_reg[8]_1\ => \^encoded_reg[8]_0\,
      \encoded_reg[8]_10\ => \encoded_reg[8]_15\,
      \encoded_reg[8]_11\ => \encoded_reg[8]_17\,
      \encoded_reg[8]_12\ => \encoded_reg[8]_18\,
      \encoded_reg[8]_13\ => \encoded_reg[8]_19\,
      \encoded_reg[8]_14\ => \encoded_reg[8]_20\,
      \encoded_reg[8]_15\ => \encoded_reg[8]_21\,
      \encoded_reg[8]_2\ => \^encoded_reg[8]_3\,
      \encoded_reg[8]_3\ => \encoded_reg[8]_7\,
      \encoded_reg[8]_4\ => \encoded_reg[8]_8\,
      \encoded_reg[8]_5\ => \^encoded_reg[8]_6\,
      \encoded_reg[8]_6\ => \encoded_reg[8]_9\,
      \encoded_reg[8]_7\ => \encoded_reg[8]_10\,
      \encoded_reg[8]_8\ => \encoded_reg[8]_13\,
      \encoded_reg[8]_9\ => \encoded_reg[8]_14\,
      \int_trigger_time_s_reg[0]\ => \int_trigger_time_s_reg[0]\,
      \int_trigger_time_s_reg[0]_0\ => \int_trigger_time_s_reg[0]_0\,
      \int_trigger_time_s_reg[5]\(3 downto 0) => \int_trigger_time_s_reg[5]\(3 downto 0),
      \int_trigger_volt_s_reg[9]\(9 downto 0) => \int_trigger_volt_s_reg[9]\(9 downto 0),
      \processQ_reg[5]\ => \processQ_reg[5]\,
      \processQ_reg[5]_0\ => \processQ_reg[5]_1\,
      \processQ_reg[5]_1\ => \processQ_reg[5]_0\,
      \processQ_reg[9]\ => \processQ_reg[9]_0\,
      \slv_reg10_reg[1]\ => \slv_reg10_reg[1]\,
      \slv_reg10_reg[2]\ => \slv_reg10_reg[2]\,
      \slv_reg10_reg[4]\ => \slv_reg10_reg[4]\,
      \slv_reg10_reg[5]\(3 downto 0) => \slv_reg10_reg[5]\(3 downto 0),
      \slv_reg10_reg[6]\ => \slv_reg10_reg[6]\,
      \slv_reg10_reg[7]\ => \slv_reg10_reg[7]\,
      \slv_reg11_reg[0]\ => \^encoded_reg[8]_5\,
      \slv_reg11_reg[9]\(9 downto 0) => \slv_reg11_reg[9]\(9 downto 0),
      \slv_reg5_reg[0]\(0) => \slv_reg5_reg[0]\(0),
      \state_reg[0]\ => \^state_reg[0]\,
      \state_reg[0]_0\ => \^state_reg[0]_0\,
      \state_reg[0]_1\ => \^state_reg[0]_1\,
      \state_reg[0]_2\ => \^state_reg[0]_2\,
      \state_reg[0]_3\ => \^state_reg[0]_3\,
      \state_reg[0]_4\ => \state_reg[0]_4\
    );
\latched_blue_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_blue_n_5,
      Q => latched_blue(0),
      R => '0'
    );
\latched_blue_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_blue_n_4,
      Q => latched_blue(1),
      R => '0'
    );
\latched_blue_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_blue_n_3,
      Q => latched_blue(2),
      R => '0'
    );
\latched_blue_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_blue_n_2,
      Q => latched_blue(4),
      R => '0'
    );
\latched_blue_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_blue_n_1,
      Q => latched_blue(8),
      R => '0'
    );
\latched_blue_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_blue_n_0,
      Q => latched_blue(9),
      R => '0'
    );
\latched_green_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_green_n_3,
      Q => latched_green(0),
      R => '0'
    );
\latched_green_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_green_n_2,
      Q => latched_green(2),
      R => '0'
    );
\latched_green_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_green_n_1,
      Q => latched_green(8),
      R => '0'
    );
\latched_green_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_green_n_0,
      Q => latched_green(9),
      R => '0'
    );
\latched_red_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_red_n_4,
      Q => latched_red(0),
      R => '0'
    );
\latched_red_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_red_n_3,
      Q => latched_red(2),
      R => '0'
    );
\latched_red_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_red_n_2,
      Q => latched_red(3),
      R => '0'
    );
\latched_red_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_red_n_1,
      Q => latched_red(8),
      R => '0'
    );
\latched_red_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => TDMS_encoder_red_n_0,
      Q => latched_red(9),
      R => '0'
    );
\shift_blue[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_blue_reg_n_0_[2]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_blue(0),
      O => shift_blue(0)
    );
\shift_blue[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_blue_reg_n_0_[3]\,
      O => \shift_blue[1]_i_1_n_0\
    );
\shift_blue[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_blue_reg_n_0_[4]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_blue(2),
      O => shift_blue(2)
    );
\shift_blue[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_blue_reg_n_0_[5]\,
      O => \shift_blue[3]_i_1_n_0\
    );
\shift_blue[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_blue_reg_n_0_[6]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_blue(4),
      O => shift_blue(4)
    );
\shift_blue[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_blue_reg_n_0_[7]\,
      O => \shift_blue[5]_i_1_n_0\
    );
\shift_blue[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_blue_reg_n_0_[8]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_blue(2),
      O => shift_blue(6)
    );
\shift_blue[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => latched_blue(1),
      I1 => \shift_red[9]_i_1_n_0\,
      O => \shift_blue[7]_i_1_n_0\
    );
\shift_blue[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_blue_reg_n_0_[9]\,
      O => \shift_blue[7]_i_2_n_0\
    );
\shift_blue_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_blue(0),
      Q => \shift_blue_reg_n_0_[0]\,
      R => '0'
    );
\shift_blue_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_blue[1]_i_1_n_0\,
      Q => \shift_blue_reg_n_0_[1]\,
      S => \shift_blue[7]_i_1_n_0\
    );
\shift_blue_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_blue(2),
      Q => \shift_blue_reg_n_0_[2]\,
      R => '0'
    );
\shift_blue_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_blue[3]_i_1_n_0\,
      Q => \shift_blue_reg_n_0_[3]\,
      S => \shift_blue[7]_i_1_n_0\
    );
\shift_blue_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_blue(4),
      Q => \shift_blue_reg_n_0_[4]\,
      R => '0'
    );
\shift_blue_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_blue[5]_i_1_n_0\,
      Q => \shift_blue_reg_n_0_[5]\,
      S => \shift_blue[7]_i_1_n_0\
    );
\shift_blue_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_blue(6),
      Q => \shift_blue_reg_n_0_[6]\,
      R => '0'
    );
\shift_blue_reg[7]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_blue[7]_i_2_n_0\,
      Q => \shift_blue_reg_n_0_[7]\,
      S => \shift_blue[7]_i_1_n_0\
    );
\shift_blue_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_blue(8),
      Q => \shift_blue_reg_n_0_[8]\,
      R => \shift_red[9]_i_1_n_0\
    );
\shift_blue_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_blue(9),
      Q => \shift_blue_reg_n_0_[9]\,
      R => \shift_red[9]_i_1_n_0\
    );
\shift_clock_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(2),
      Q => shift_clock(0),
      R => '0'
    );
\shift_clock_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(3),
      Q => shift_clock(1),
      R => '0'
    );
\shift_clock_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(4),
      Q => \shift_clock__0\(2),
      R => '0'
    );
\shift_clock_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(5),
      Q => \shift_clock__0\(3),
      R => '0'
    );
\shift_clock_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(6),
      Q => \shift_clock__0\(4),
      R => '0'
    );
\shift_clock_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(7),
      Q => \shift_clock__0\(5),
      R => '0'
    );
\shift_clock_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(8),
      Q => \shift_clock__0\(6),
      R => '0'
    );
\shift_clock_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_clock__0\(9),
      Q => \shift_clock__0\(7),
      R => '0'
    );
\shift_clock_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_clock(0),
      Q => \shift_clock__0\(8),
      R => '0'
    );
\shift_clock_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_clock(1),
      Q => \shift_clock__0\(9),
      R => '0'
    );
\shift_green[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_green_reg_n_0_[2]\,
      O => \shift_green[0]_i_1_n_0\
    );
\shift_green[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_green_reg_n_0_[3]\,
      O => \shift_green[1]_i_1_n_0\
    );
\shift_green[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_green_reg_n_0_[4]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_green(2),
      O => shift_green(2)
    );
\shift_green[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_green_reg_n_0_[5]\,
      O => \shift_green[3]_i_1_n_0\
    );
\shift_green[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_green_reg_n_0_[6]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_green(2),
      O => shift_green(4)
    );
\shift_green[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_green_reg_n_0_[7]\,
      O => \shift_green[5]_i_1_n_0\
    );
\shift_green[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \shift_green_reg_n_0_[8]\,
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_green(2),
      O => shift_green(6)
    );
\shift_green[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => latched_green(0),
      I1 => \shift_red[9]_i_1_n_0\,
      O => \shift_green[7]_i_1_n_0\
    );
\shift_green[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \shift_red[9]_i_1_n_0\,
      I1 => \shift_green_reg_n_0_[9]\,
      O => \shift_green[7]_i_2_n_0\
    );
\shift_green_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_green[0]_i_1_n_0\,
      Q => \shift_green_reg_n_0_[0]\,
      S => \shift_green[7]_i_1_n_0\
    );
\shift_green_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_green[1]_i_1_n_0\,
      Q => \shift_green_reg_n_0_[1]\,
      S => \shift_green[7]_i_1_n_0\
    );
\shift_green_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_green(2),
      Q => \shift_green_reg_n_0_[2]\,
      R => '0'
    );
\shift_green_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_green[3]_i_1_n_0\,
      Q => \shift_green_reg_n_0_[3]\,
      S => \shift_green[7]_i_1_n_0\
    );
\shift_green_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_green(4),
      Q => \shift_green_reg_n_0_[4]\,
      R => '0'
    );
\shift_green_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_green[5]_i_1_n_0\,
      Q => \shift_green_reg_n_0_[5]\,
      S => \shift_green[7]_i_1_n_0\
    );
\shift_green_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_green(6),
      Q => \shift_green_reg_n_0_[6]\,
      R => '0'
    );
\shift_green_reg[7]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => \shift_green[7]_i_2_n_0\,
      Q => \shift_green_reg_n_0_[7]\,
      S => \shift_green[7]_i_1_n_0\
    );
\shift_green_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_green(8),
      Q => \shift_green_reg_n_0_[8]\,
      R => \shift_red[9]_i_1_n_0\
    );
\shift_green_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_green(9),
      Q => \shift_green_reg_n_0_[9]\,
      R => \shift_red[9]_i_1_n_0\
    );
\shift_red[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(0),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(0),
      O => shift_red(0)
    );
\shift_red[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(1),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(0),
      O => shift_red(1)
    );
\shift_red[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(2),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(2),
      O => shift_red(2)
    );
\shift_red[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(3),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(3),
      O => shift_red(3)
    );
\shift_red[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(4),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(2),
      O => shift_red(4)
    );
\shift_red[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(5),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(0),
      O => shift_red(5)
    );
\shift_red[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(6),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(2),
      O => shift_red(6)
    );
\shift_red[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(7),
      I1 => \shift_red[9]_i_1_n_0\,
      I2 => latched_red(3),
      O => shift_red(7)
    );
\shift_red[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFFFFFF"
    )
        port map (
      I0 => \shift_red[9]_i_2_n_0\,
      I1 => \shift_clock__0\(5),
      I2 => \shift_clock__0\(4),
      I3 => \shift_clock__0\(2),
      I4 => \shift_clock__0\(3),
      O => \shift_red[9]_i_1_n_0\
    );
\shift_red[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFFFFFFFFFF"
    )
        port map (
      I0 => \shift_clock__0\(8),
      I1 => \shift_clock__0\(9),
      I2 => \shift_clock__0\(6),
      I3 => \shift_clock__0\(7),
      I4 => shift_clock(1),
      I5 => shift_clock(0),
      O => \shift_red[9]_i_2_n_0\
    );
\shift_red_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(0),
      Q => D0,
      R => '0'
    );
\shift_red_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(1),
      Q => D1,
      R => '0'
    );
\shift_red_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(2),
      Q => data1(0),
      R => '0'
    );
\shift_red_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(3),
      Q => data1(1),
      R => '0'
    );
\shift_red_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(4),
      Q => data1(2),
      R => '0'
    );
\shift_red_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(5),
      Q => data1(3),
      R => '0'
    );
\shift_red_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(6),
      Q => data1(4),
      R => '0'
    );
\shift_red_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => shift_red(7),
      Q => data1(5),
      R => '0'
    );
\shift_red_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_red(8),
      Q => data1(6),
      R => \shift_red[9]_i_1_n_0\
    );
\shift_red_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_out2,
      CE => '1',
      D => latched_red(9),
      Q => data1(7),
      R => \shift_red[9]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga is
  port (
    \int_trigger_time_s_reg[0]\ : out STD_LOGIC;
    \sdp_bl.ramb18_dp_bl.ram18_bl\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \encoded_reg[8]\ : out STD_LOGIC;
    \encoded_reg[9]\ : out STD_LOGIC;
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \encoded_reg[0]\ : out STD_LOGIC;
    \processQ_reg[9]\ : out STD_LOGIC_VECTOR ( 9 downto 0 );
    ADDRARDADDR : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \encoded_reg[8]_1\ : out STD_LOGIC;
    \encoded_reg[8]_2\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_0\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_1\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_2\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_3\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_4\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_5\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_6\ : out STD_LOGIC;
    \encoded_reg[8]_3\ : out STD_LOGIC;
    \dc_bias_reg[3]\ : out STD_LOGIC;
    \encoded_reg[8]_4\ : out STD_LOGIC;
    \encoded_reg[2]\ : out STD_LOGIC;
    \encoded_reg[1]\ : out STD_LOGIC;
    \encoded_reg[4]\ : out STD_LOGIC;
    \encoded_reg[0]_0\ : out STD_LOGIC;
    \encoded_reg[0]_1\ : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 0 to 0 );
    \encoded_reg[9]_0\ : out STD_LOGIC;
    \encoded_reg[9]_1\ : out STD_LOGIC;
    \encoded_reg[9]_2\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg5_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg10_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \dc_bias_reg[1]\ : in STD_LOGIC;
    \dc_bias_reg[3]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_n : in STD_LOGIC;
    \dc_bias_reg[2]\ : in STD_LOGIC;
    \slv_reg10_reg[5]\ : in STD_LOGIC;
    \slv_reg10_reg[3]\ : in STD_LOGIC;
    \slv_reg10_reg[6]\ : in STD_LOGIC;
    \int_trigger_time_s_reg[1]\ : in STD_LOGIC;
    \slv_reg10_reg[6]_0\ : in STD_LOGIC;
    \int_trigger_time_s_reg[1]_0\ : in STD_LOGIC;
    \slv_reg10_reg[6]_1\ : in STD_LOGIC;
    \int_trigger_time_s_reg[1]_1\ : in STD_LOGIC;
    \slv_reg10_reg[2]\ : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    switch : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \processQ_reg[9]_0\ : in STD_LOGIC;
    \processQ_reg[8]\ : in STD_LOGIC;
    \dc_bias_reg[3]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg11_reg[1]\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[9]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \slv_reg11_reg[9]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    DOADO : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \slv_reg11_reg[9]_0\ : in STD_LOGIC;
    \slv_reg11_reg[8]\ : in STD_LOGIC;
    \slv_reg11_reg[7]\ : in STD_LOGIC;
    \slv_reg11_reg[6]\ : in STD_LOGIC;
    \slv_reg11_reg[5]\ : in STD_LOGIC;
    \slv_reg11_reg[7]_0\ : in STD_LOGIC;
    \slv_reg11_reg[6]_0\ : in STD_LOGIC;
    \slv_reg11_reg[5]_0\ : in STD_LOGIC;
    \slv_reg11_reg[6]_1\ : in STD_LOGIC;
    \slv_reg11_reg[6]_2\ : in STD_LOGIC;
    \slv_reg11_reg[5]_1\ : in STD_LOGIC;
    \slv_reg11_reg[6]_3\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[2]\ : in STD_LOGIC;
    \slv_reg11_reg[5]_2\ : in STD_LOGIC;
    \slv_reg11_reg[6]_4\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[2]_0\ : in STD_LOGIC;
    \slv_reg11_reg[3]\ : in STD_LOGIC;
    \slv_reg11_reg[2]\ : in STD_LOGIC;
    \slv_reg11_reg[4]\ : in STD_LOGIC;
    \slv_reg11_reg[0]\ : in STD_LOGIC;
    \slv_reg11_reg[0]_0\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[3]\ : in STD_LOGIC;
    \slv_reg11_reg[0]_1\ : in STD_LOGIC;
    \slv_reg11_reg[0]_2\ : in STD_LOGIC;
    \slv_reg11_reg[1]_0\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[2]_1\ : in STD_LOGIC;
    \processQ_reg[9]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg11_reg[7]_1\ : in STD_LOGIC;
    \slv_reg10_reg[7]\ : in STD_LOGIC;
    \dc_bias_reg[3]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga is
  signal \^encoded_reg[8]_0\ : STD_LOGIC;
  signal \^encoded_reg[8]_2\ : STD_LOGIC;
  signal \^encoded_reg[9]\ : STD_LOGIC;
  signal horiz_counter_n_0 : STD_LOGIC;
  signal horiz_counter_n_1 : STD_LOGIC;
  signal horiz_counter_n_2 : STD_LOGIC;
  signal horiz_counter_n_20 : STD_LOGIC;
  signal horiz_counter_n_21 : STD_LOGIC;
  signal horiz_counter_n_22 : STD_LOGIC;
  signal horiz_counter_n_23 : STD_LOGIC;
  signal horiz_counter_n_24 : STD_LOGIC;
  signal horiz_counter_n_25 : STD_LOGIC;
  signal horiz_counter_n_26 : STD_LOGIC;
  signal horiz_counter_n_27 : STD_LOGIC;
  signal horiz_counter_n_28 : STD_LOGIC;
  signal horiz_counter_n_29 : STD_LOGIC;
  signal horiz_counter_n_3 : STD_LOGIC;
  signal horiz_counter_n_30 : STD_LOGIC;
  signal horiz_counter_n_31 : STD_LOGIC;
  signal horiz_counter_n_32 : STD_LOGIC;
  signal horiz_counter_n_33 : STD_LOGIC;
  signal horiz_counter_n_34 : STD_LOGIC;
  signal horiz_counter_n_35 : STD_LOGIC;
  signal horiz_counter_n_36 : STD_LOGIC;
  signal horiz_counter_n_37 : STD_LOGIC;
  signal horiz_counter_n_38 : STD_LOGIC;
  signal horiz_counter_n_39 : STD_LOGIC;
  signal horiz_counter_n_40 : STD_LOGIC;
  signal horiz_counter_n_41 : STD_LOGIC;
  signal horiz_counter_n_42 : STD_LOGIC;
  signal horiz_counter_n_43 : STD_LOGIC;
  signal horiz_counter_n_44 : STD_LOGIC;
  signal horiz_counter_n_45 : STD_LOGIC;
  signal horiz_counter_n_46 : STD_LOGIC;
  signal horiz_counter_n_47 : STD_LOGIC;
  signal horiz_counter_n_48 : STD_LOGIC;
  signal horiz_counter_n_49 : STD_LOGIC;
  signal horiz_counter_n_50 : STD_LOGIC;
  signal horiz_counter_n_51 : STD_LOGIC;
  signal horiz_counter_n_52 : STD_LOGIC;
  signal horiz_counter_n_53 : STD_LOGIC;
  signal horiz_counter_n_54 : STD_LOGIC;
  signal horiz_counter_n_55 : STD_LOGIC;
  signal horiz_counter_n_56 : STD_LOGIC;
  signal horiz_counter_n_57 : STD_LOGIC;
  signal horiz_counter_n_58 : STD_LOGIC;
  signal horiz_counter_n_59 : STD_LOGIC;
  signal horiz_counter_n_60 : STD_LOGIC;
  signal horiz_counter_n_61 : STD_LOGIC;
  signal horiz_counter_n_62 : STD_LOGIC;
  signal horiz_counter_n_63 : STD_LOGIC;
  signal horiz_counter_n_64 : STD_LOGIC;
  signal horiz_counter_n_65 : STD_LOGIC;
  signal horiz_counter_n_66 : STD_LOGIC;
  signal horiz_counter_n_67 : STD_LOGIC;
  signal horiz_counter_n_68 : STD_LOGIC;
  signal horiz_counter_n_69 : STD_LOGIC;
  signal horiz_counter_n_7 : STD_LOGIC;
  signal horiz_counter_n_70 : STD_LOGIC;
  signal horiz_counter_n_71 : STD_LOGIC;
  signal horiz_counter_n_72 : STD_LOGIC;
  signal horiz_counter_n_73 : STD_LOGIC;
  signal horiz_counter_n_74 : STD_LOGIC;
  signal horiz_counter_n_75 : STD_LOGIC;
  signal horiz_counter_n_76 : STD_LOGIC;
  signal horiz_counter_n_77 : STD_LOGIC;
  signal horiz_counter_n_78 : STD_LOGIC;
  signal horiz_counter_n_79 : STD_LOGIC;
  signal horiz_counter_n_88 : STD_LOGIC;
  signal horiz_counter_n_89 : STD_LOGIC;
  signal horiz_counter_n_9 : STD_LOGIC;
  signal horiz_counter_n_90 : STD_LOGIC;
  signal horiz_counter_n_91 : STD_LOGIC;
  signal horiz_counter_n_92 : STD_LOGIC;
  signal horiz_counter_n_93 : STD_LOGIC;
  signal horiz_counter_n_94 : STD_LOGIC;
  signal horiz_counter_n_95 : STD_LOGIC;
  signal horiz_counter_n_96 : STD_LOGIC;
  signal horiz_counter_n_97 : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_0\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_1\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_2\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_3\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_4\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_5\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_6\ : STD_LOGIC;
  signal pixel_color3 : STD_LOGIC;
  signal pixel_color31_out : STD_LOGIC;
  signal pixel_color49_in : STD_LOGIC;
  signal pixel_color513_in : STD_LOGIC;
  signal pixel_color517_in : STD_LOGIC;
  signal pixel_color528_in : STD_LOGIC;
  signal pixel_color58_in : STD_LOGIC;
  signal pixel_color612_in : STD_LOGIC;
  signal pixel_color616_in : STD_LOGIC;
  signal pixel_color627_in : STD_LOGIC;
  signal \^processq_reg[9]\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal sF_n_10 : STD_LOGIC;
  signal sF_n_12 : STD_LOGIC;
  signal sF_n_15 : STD_LOGIC;
  signal sF_n_16 : STD_LOGIC;
  signal sF_n_18 : STD_LOGIC;
  signal sF_n_19 : STD_LOGIC;
  signal sF_n_20 : STD_LOGIC;
  signal sF_n_21 : STD_LOGIC;
  signal sF_n_22 : STD_LOGIC;
  signal sF_n_25 : STD_LOGIC;
  signal sF_n_26 : STD_LOGIC;
  signal sF_n_27 : STD_LOGIC;
  signal sF_n_28 : STD_LOGIC;
  signal sF_n_31 : STD_LOGIC;
  signal sF_n_32 : STD_LOGIC;
  signal sF_n_33 : STD_LOGIC;
  signal sF_n_34 : STD_LOGIC;
  signal \^sdp_bl.ramb18_dp_bl.ram18_bl\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal vert_counter_n_12 : STD_LOGIC;
  signal vert_counter_n_14 : STD_LOGIC;
  signal vert_counter_n_15 : STD_LOGIC;
  signal vert_counter_n_16 : STD_LOGIC;
  signal vert_counter_n_17 : STD_LOGIC;
  signal vert_counter_n_18 : STD_LOGIC;
  signal vert_counter_n_19 : STD_LOGIC;
  signal vert_counter_n_20 : STD_LOGIC;
  signal vert_counter_n_21 : STD_LOGIC;
  signal vert_counter_n_23 : STD_LOGIC;
  signal vert_counter_n_24 : STD_LOGIC;
  signal vert_counter_n_25 : STD_LOGIC;
  signal vert_counter_n_26 : STD_LOGIC;
  signal vert_counter_n_27 : STD_LOGIC;
  signal vert_counter_n_28 : STD_LOGIC;
  signal vert_counter_n_29 : STD_LOGIC;
  signal vert_counter_n_30 : STD_LOGIC;
  signal vert_counter_n_31 : STD_LOGIC;
  signal vert_counter_n_32 : STD_LOGIC;
  signal vert_counter_n_33 : STD_LOGIC;
  signal vert_counter_n_34 : STD_LOGIC;
  signal vert_counter_n_35 : STD_LOGIC;
  signal vert_counter_n_36 : STD_LOGIC;
  signal vert_counter_n_37 : STD_LOGIC;
  signal vert_counter_n_38 : STD_LOGIC;
  signal vert_counter_n_39 : STD_LOGIC;
  signal vert_counter_n_40 : STD_LOGIC;
  signal vert_counter_n_41 : STD_LOGIC;
  signal vert_counter_n_42 : STD_LOGIC;
  signal vert_counter_n_43 : STD_LOGIC;
  signal vert_counter_n_44 : STD_LOGIC;
  signal vert_counter_n_45 : STD_LOGIC;
  signal vert_counter_n_46 : STD_LOGIC;
  signal vert_counter_n_47 : STD_LOGIC;
  signal vert_counter_n_48 : STD_LOGIC;
  signal vert_counter_n_49 : STD_LOGIC;
  signal vert_counter_n_50 : STD_LOGIC;
  signal vert_counter_n_51 : STD_LOGIC;
  signal vert_counter_n_52 : STD_LOGIC;
  signal vert_counter_n_53 : STD_LOGIC;
  signal vert_counter_n_54 : STD_LOGIC;
  signal vert_counter_n_55 : STD_LOGIC;
  signal vert_counter_n_56 : STD_LOGIC;
  signal vert_counter_n_57 : STD_LOGIC;
  signal vert_counter_n_58 : STD_LOGIC;
  signal vert_counter_n_59 : STD_LOGIC;
  signal vert_counter_n_60 : STD_LOGIC;
  signal vert_counter_n_61 : STD_LOGIC;
  signal vert_counter_n_62 : STD_LOGIC;
  signal vert_counter_n_63 : STD_LOGIC;
  signal vert_counter_n_64 : STD_LOGIC;
  signal vert_counter_n_65 : STD_LOGIC;
  signal vert_counter_n_66 : STD_LOGIC;
  signal vert_counter_n_67 : STD_LOGIC;
  signal vert_counter_n_68 : STD_LOGIC;
  signal vert_counter_n_69 : STD_LOGIC;
  signal vert_counter_n_70 : STD_LOGIC;
  signal vert_counter_n_71 : STD_LOGIC;
  signal vert_counter_n_72 : STD_LOGIC;
  signal vert_counter_n_73 : STD_LOGIC;
  signal vert_counter_n_74 : STD_LOGIC;
  signal vert_counter_n_75 : STD_LOGIC;
  signal vert_counter_n_76 : STD_LOGIC;
  signal vert_counter_n_77 : STD_LOGIC;
  signal vert_counter_n_78 : STD_LOGIC;
  signal vert_counter_n_79 : STD_LOGIC;
  signal vert_counter_n_80 : STD_LOGIC;
  signal vert_counter_n_81 : STD_LOGIC;
  signal vert_counter_n_82 : STD_LOGIC;
  signal vert_counter_n_83 : STD_LOGIC;
  signal vert_counter_n_84 : STD_LOGIC;
  signal vert_counter_n_85 : STD_LOGIC;
  signal vert_counter_n_86 : STD_LOGIC;
  signal vert_counter_n_87 : STD_LOGIC;
begin
  \encoded_reg[8]_0\ <= \^encoded_reg[8]_0\;
  \encoded_reg[8]_2\ <= \^encoded_reg[8]_2\;
  \encoded_reg[9]\ <= \^encoded_reg[9]\;
  \int_trigger_time_s_reg[0]\ <= \^int_trigger_time_s_reg[0]\;
  \int_trigger_time_s_reg[0]_0\ <= \^int_trigger_time_s_reg[0]_0\;
  \int_trigger_time_s_reg[0]_1\ <= \^int_trigger_time_s_reg[0]_1\;
  \int_trigger_time_s_reg[0]_2\ <= \^int_trigger_time_s_reg[0]_2\;
  \int_trigger_time_s_reg[0]_3\ <= \^int_trigger_time_s_reg[0]_3\;
  \int_trigger_time_s_reg[0]_4\ <= \^int_trigger_time_s_reg[0]_4\;
  \int_trigger_time_s_reg[0]_5\ <= \^int_trigger_time_s_reg[0]_5\;
  \int_trigger_time_s_reg[0]_6\ <= \^int_trigger_time_s_reg[0]_6\;
  \processQ_reg[9]\(9 downto 0) <= \^processq_reg[9]\(9 downto 0);
  \sdp_bl.ramb18_dp_bl.ram18_bl\(1 downto 0) <= \^sdp_bl.ramb18_dp_bl.ram18_bl\(1 downto 0);
horiz_counter: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_hcounter
     port map (
      ADDRARDADDR(7 downto 0) => ADDRARDADDR(7 downto 0),
      CLK => CLK,
      CO(0) => CO(0),
      DI(3) => horiz_counter_n_0,
      DI(2) => horiz_counter_n_1,
      DI(1) => horiz_counter_n_2,
      DI(0) => horiz_counter_n_3,
      Q(3) => Q(9),
      Q(2 downto 1) => Q(3 downto 2),
      Q(0) => Q(0),
      S(3) => horiz_counter_n_20,
      S(2) => horiz_counter_n_21,
      S(1) => horiz_counter_n_22,
      S(0) => horiz_counter_n_23,
      \dc_bias_reg[1]\ => \dc_bias_reg[1]\,
      \dc_bias_reg[2]\ => \dc_bias_reg[2]\,
      \dc_bias_reg[3]\ => \dc_bias_reg[3]\,
      \dc_bias_reg[3]_0\(0) => \dc_bias_reg[3]_1\(0),
      \dc_bias_reg[3]_1\(0) => \dc_bias_reg[3]_2\(0),
      \encoded_reg[0]\(3) => horiz_counter_n_40,
      \encoded_reg[0]\(2) => horiz_counter_n_41,
      \encoded_reg[0]\(1) => horiz_counter_n_42,
      \encoded_reg[0]\(0) => horiz_counter_n_43,
      \encoded_reg[0]_0\(3) => horiz_counter_n_60,
      \encoded_reg[0]_0\(2) => horiz_counter_n_61,
      \encoded_reg[0]_0\(1) => horiz_counter_n_62,
      \encoded_reg[0]_0\(0) => horiz_counter_n_63,
      \encoded_reg[0]_1\(3) => horiz_counter_n_64,
      \encoded_reg[0]_1\(2) => horiz_counter_n_65,
      \encoded_reg[0]_1\(1) => horiz_counter_n_66,
      \encoded_reg[0]_1\(0) => horiz_counter_n_67,
      \encoded_reg[0]_10\ => horiz_counter_n_89,
      \encoded_reg[0]_11\ => horiz_counter_n_90,
      \encoded_reg[0]_2\(0) => horiz_counter_n_68,
      \encoded_reg[0]_3\(0) => horiz_counter_n_69,
      \encoded_reg[0]_4\(3) => horiz_counter_n_70,
      \encoded_reg[0]_4\(2) => horiz_counter_n_71,
      \encoded_reg[0]_4\(1) => horiz_counter_n_72,
      \encoded_reg[0]_4\(0) => horiz_counter_n_73,
      \encoded_reg[0]_5\(3) => horiz_counter_n_74,
      \encoded_reg[0]_5\(2) => horiz_counter_n_75,
      \encoded_reg[0]_5\(1) => horiz_counter_n_76,
      \encoded_reg[0]_5\(0) => horiz_counter_n_77,
      \encoded_reg[0]_6\(0) => horiz_counter_n_78,
      \encoded_reg[0]_7\(0) => horiz_counter_n_79,
      \encoded_reg[0]_8\ => \encoded_reg[0]_0\,
      \encoded_reg[0]_9\ => \encoded_reg[0]_1\,
      \encoded_reg[1]\ => \encoded_reg[1]\,
      \encoded_reg[2]\ => \encoded_reg[2]\,
      \encoded_reg[4]\ => \encoded_reg[4]\,
      \encoded_reg[8]\ => \encoded_reg[8]\,
      \encoded_reg[8]_0\ => horiz_counter_n_7,
      \encoded_reg[8]_1\ => \^encoded_reg[8]_0\,
      \encoded_reg[8]_10\(0) => horiz_counter_n_39,
      \encoded_reg[8]_11\(3) => horiz_counter_n_44,
      \encoded_reg[8]_11\(2) => horiz_counter_n_45,
      \encoded_reg[8]_11\(1) => horiz_counter_n_46,
      \encoded_reg[8]_11\(0) => horiz_counter_n_47,
      \encoded_reg[8]_12\(3) => horiz_counter_n_48,
      \encoded_reg[8]_12\(2) => horiz_counter_n_49,
      \encoded_reg[8]_12\(1) => horiz_counter_n_50,
      \encoded_reg[8]_12\(0) => horiz_counter_n_51,
      \encoded_reg[8]_13\(0) => horiz_counter_n_52,
      \encoded_reg[8]_14\(0) => horiz_counter_n_53,
      \encoded_reg[8]_15\(3) => horiz_counter_n_54,
      \encoded_reg[8]_15\(2) => horiz_counter_n_55,
      \encoded_reg[8]_15\(1) => horiz_counter_n_56,
      \encoded_reg[8]_15\(0) => horiz_counter_n_57,
      \encoded_reg[8]_16\(0) => horiz_counter_n_58,
      \encoded_reg[8]_17\(0) => horiz_counter_n_59,
      \encoded_reg[8]_18\ => \encoded_reg[8]_3\,
      \encoded_reg[8]_19\ => \encoded_reg[8]_4\,
      \encoded_reg[8]_2\ => \encoded_reg[8]_1\,
      \encoded_reg[8]_20\ => horiz_counter_n_88,
      \encoded_reg[8]_21\ => horiz_counter_n_91,
      \encoded_reg[8]_22\ => horiz_counter_n_92,
      \encoded_reg[8]_23\ => horiz_counter_n_93,
      \encoded_reg[8]_24\ => horiz_counter_n_94,
      \encoded_reg[8]_25\ => horiz_counter_n_96,
      \encoded_reg[8]_26\ => horiz_counter_n_97,
      \encoded_reg[8]_3\ => \^encoded_reg[8]_2\,
      \encoded_reg[8]_4\(3) => horiz_counter_n_24,
      \encoded_reg[8]_4\(2) => horiz_counter_n_25,
      \encoded_reg[8]_4\(1) => horiz_counter_n_26,
      \encoded_reg[8]_4\(0) => horiz_counter_n_27,
      \encoded_reg[8]_5\(0) => horiz_counter_n_28,
      \encoded_reg[8]_6\(0) => horiz_counter_n_29,
      \encoded_reg[8]_7\(3) => horiz_counter_n_30,
      \encoded_reg[8]_7\(2) => horiz_counter_n_31,
      \encoded_reg[8]_7\(1) => horiz_counter_n_32,
      \encoded_reg[8]_7\(0) => horiz_counter_n_33,
      \encoded_reg[8]_8\(3) => horiz_counter_n_34,
      \encoded_reg[8]_8\(2) => horiz_counter_n_35,
      \encoded_reg[8]_8\(1) => horiz_counter_n_36,
      \encoded_reg[8]_8\(0) => horiz_counter_n_37,
      \encoded_reg[8]_9\(0) => horiz_counter_n_38,
      \encoded_reg[9]\ => horiz_counter_n_95,
      \encoded_reg[9]_0\ => \encoded_reg[9]_2\,
      \int_trigger_time_s_reg[1]\ => \int_trigger_time_s_reg[1]\,
      \int_trigger_time_s_reg[1]_0\ => \int_trigger_time_s_reg[1]_0\,
      \int_trigger_time_s_reg[1]_1\ => sF_n_16,
      \int_trigger_time_s_reg[1]_2\ => sF_n_28,
      \int_trigger_time_s_reg[1]_3\ => \int_trigger_time_s_reg[1]_1\,
      \int_trigger_time_s_reg[2]\ => sF_n_27,
      \int_trigger_time_s_reg[3]\ => sF_n_26,
      \int_trigger_time_s_reg[5]\ => sF_n_21,
      \int_trigger_time_s_reg[7]\ => sF_n_19,
      \processQ_reg[0]_0\ => horiz_counter_n_9,
      \processQ_reg[0]_1\ => vert_counter_n_84,
      \processQ_reg[0]_2\ => sF_n_34,
      \processQ_reg[2]_0\ => vert_counter_n_15,
      \processQ_reg[2]_1\ => vert_counter_n_14,
      \processQ_reg[4]_0\ => vert_counter_n_83,
      \processQ_reg[4]_1\ => vert_counter_n_16,
      \processQ_reg[5]_0\ => \^encoded_reg[9]\,
      \processQ_reg[8]_0\ => \processQ_reg[8]\,
      \processQ_reg[8]_1\ => vert_counter_n_12,
      \processQ_reg[8]_2\ => vert_counter_n_85,
      \processQ_reg[8]_3\ => vert_counter_n_87,
      \processQ_reg[9]_0\ => \processQ_reg[9]_0\,
      \processQ_reg[9]_1\ => vert_counter_n_86,
      \processQ_reg[9]_10\(0) => pixel_color517_in,
      \processQ_reg[9]_2\ => vert_counter_n_21,
      \processQ_reg[9]_3\(0) => pixel_color49_in,
      \processQ_reg[9]_4\(0) => pixel_color58_in,
      \processQ_reg[9]_5\(0) => \processQ_reg[9]_1\(0),
      \processQ_reg[9]_6\(2) => \^processq_reg[9]\(9),
      \processQ_reg[9]_6\(1 downto 0) => \^processq_reg[9]\(1 downto 0),
      \processQ_reg[9]_7\(0) => pixel_color528_in,
      \processQ_reg[9]_8\(0) => pixel_color627_in,
      \processQ_reg[9]_9\(0) => pixel_color616_in,
      reset_n => reset_n,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(1 downto 0) => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1 downto 0),
      \slv_reg10_reg[0]\ => sF_n_12,
      \slv_reg10_reg[0]_0\ => \^int_trigger_time_s_reg[0]_6\,
      \slv_reg10_reg[1]\ => \^int_trigger_time_s_reg[0]\,
      \slv_reg10_reg[2]\ => \^int_trigger_time_s_reg[0]_5\,
      \slv_reg10_reg[2]_0\ => \slv_reg10_reg[2]\,
      \slv_reg10_reg[3]\ => \slv_reg10_reg[3]\,
      \slv_reg10_reg[4]\ => \^int_trigger_time_s_reg[0]_4\,
      \slv_reg10_reg[5]\ => sF_n_22,
      \slv_reg10_reg[5]_0\ => \slv_reg10_reg[5]\,
      \slv_reg10_reg[5]_1\ => sF_n_25,
      \slv_reg10_reg[5]_2\ => sF_n_20,
      \slv_reg10_reg[6]\ => \^int_trigger_time_s_reg[0]_1\,
      \slv_reg10_reg[6]_0\ => sF_n_15,
      \slv_reg10_reg[6]_1\ => \slv_reg10_reg[6]\,
      \slv_reg10_reg[6]_2\ => sF_n_18,
      \slv_reg10_reg[6]_3\ => \slv_reg10_reg[6]_0\,
      \slv_reg10_reg[6]_4\ => \slv_reg10_reg[6]_1\,
      \slv_reg10_reg[7]\ => \^int_trigger_time_s_reg[0]_0\,
      \slv_reg10_reg[7]_0\ => sF_n_10,
      \slv_reg10_reg[7]_1\ => \slv_reg10_reg[7]\,
      \slv_reg10_reg[8]\ => \^int_trigger_time_s_reg[0]_3\,
      \slv_reg10_reg[9]\(3) => \slv_reg10_reg[9]\(9),
      \slv_reg10_reg[9]\(2 downto 1) => \slv_reg10_reg[9]\(3 downto 2),
      \slv_reg10_reg[9]\(0) => \slv_reg10_reg[9]\(0),
      \slv_reg10_reg[9]_0\ => \^int_trigger_time_s_reg[0]_2\,
      \slv_reg10_reg[9]_1\(0) => pixel_color31_out,
      \slv_reg5_reg[0]\(0) => \slv_reg5_reg[0]\(0),
      switch(1 downto 0) => switch(1 downto 0)
    );
sF: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_scopeFace
     port map (
      CO(0) => pixel_color612_in,
      DI(3) => vert_counter_n_75,
      DI(2) => vert_counter_n_76,
      DI(1) => vert_counter_n_77,
      DI(0) => vert_counter_n_78,
      Q(9 downto 0) => Q(9 downto 0),
      S(3) => vert_counter_n_79,
      S(2) => vert_counter_n_80,
      S(1) => vert_counter_n_81,
      S(0) => vert_counter_n_82,
      \encoded_reg[0]\(0) => pixel_color627_in,
      \encoded_reg[0]_0\(0) => pixel_color528_in,
      \encoded_reg[0]_1\(0) => pixel_color31_out,
      \encoded_reg[0]_2\ => sF_n_27,
      \encoded_reg[8]\(0) => pixel_color513_in,
      \encoded_reg[8]_0\(0) => pixel_color58_in,
      \encoded_reg[8]_1\(0) => pixel_color49_in,
      \encoded_reg[8]_10\ => sF_n_19,
      \encoded_reg[8]_11\ => sF_n_20,
      \encoded_reg[8]_12\ => sF_n_21,
      \encoded_reg[8]_13\ => sF_n_22,
      \encoded_reg[8]_14\ => sF_n_25,
      \encoded_reg[8]_15\ => sF_n_26,
      \encoded_reg[8]_16\ => sF_n_28,
      \encoded_reg[8]_17\ => sF_n_31,
      \encoded_reg[8]_18\ => sF_n_32,
      \encoded_reg[8]_19\ => sF_n_33,
      \encoded_reg[8]_2\(0) => pixel_color3,
      \encoded_reg[8]_20\ => sF_n_34,
      \encoded_reg[8]_3\(0) => pixel_color616_in,
      \encoded_reg[8]_4\(0) => pixel_color517_in,
      \encoded_reg[8]_5\ => sF_n_10,
      \encoded_reg[8]_6\ => sF_n_12,
      \encoded_reg[8]_7\ => sF_n_15,
      \encoded_reg[8]_8\ => sF_n_16,
      \encoded_reg[8]_9\ => sF_n_18,
      \int_trigger_time_s_reg[0]\ => \^int_trigger_time_s_reg[0]_0\,
      \int_trigger_time_s_reg[0]_0\ => \^int_trigger_time_s_reg[0]_4\,
      \int_trigger_time_s_reg[0]_1\ => \^int_trigger_time_s_reg[0]_1\,
      \int_trigger_time_s_reg[0]_2\ => \^int_trigger_time_s_reg[0]_5\,
      \int_trigger_time_s_reg[0]_3\ => \^int_trigger_time_s_reg[0]_6\,
      \int_trigger_time_s_reg[0]_4\ => \^int_trigger_time_s_reg[0]\,
      \int_trigger_time_s_reg[0]_5\ => \^int_trigger_time_s_reg[0]_2\,
      \int_trigger_time_s_reg[0]_6\ => \^int_trigger_time_s_reg[0]_3\,
      \int_trigger_volt_s_reg[7]\(3 downto 2) => \int_trigger_volt_s_reg[9]\(4 downto 3),
      \int_trigger_volt_s_reg[7]\(1 downto 0) => \int_trigger_volt_s_reg[9]\(1 downto 0),
      \processQ_reg[1]\(1 downto 0) => \^processq_reg[9]\(1 downto 0),
      \processQ_reg[7]\(3) => vert_counter_n_65,
      \processQ_reg[7]\(2) => vert_counter_n_66,
      \processQ_reg[7]\(1) => vert_counter_n_67,
      \processQ_reg[7]\(0) => vert_counter_n_68,
      \processQ_reg[7]_0\(3) => vert_counter_n_69,
      \processQ_reg[7]_0\(2) => vert_counter_n_70,
      \processQ_reg[7]_0\(1) => vert_counter_n_71,
      \processQ_reg[7]_0\(0) => vert_counter_n_72,
      \processQ_reg[7]_1\(3) => vert_counter_n_55,
      \processQ_reg[7]_1\(2) => vert_counter_n_56,
      \processQ_reg[7]_1\(1) => vert_counter_n_57,
      \processQ_reg[7]_1\(0) => vert_counter_n_58,
      \processQ_reg[7]_10\(3) => horiz_counter_n_70,
      \processQ_reg[7]_10\(2) => horiz_counter_n_71,
      \processQ_reg[7]_10\(1) => horiz_counter_n_72,
      \processQ_reg[7]_10\(0) => horiz_counter_n_73,
      \processQ_reg[7]_11\(3) => horiz_counter_n_64,
      \processQ_reg[7]_11\(2) => horiz_counter_n_65,
      \processQ_reg[7]_11\(1) => horiz_counter_n_66,
      \processQ_reg[7]_11\(0) => horiz_counter_n_67,
      \processQ_reg[7]_12\(3) => horiz_counter_n_60,
      \processQ_reg[7]_12\(2) => horiz_counter_n_61,
      \processQ_reg[7]_12\(1) => horiz_counter_n_62,
      \processQ_reg[7]_12\(0) => horiz_counter_n_63,
      \processQ_reg[7]_13\(3) => horiz_counter_n_0,
      \processQ_reg[7]_13\(2) => horiz_counter_n_1,
      \processQ_reg[7]_13\(1) => horiz_counter_n_2,
      \processQ_reg[7]_13\(0) => horiz_counter_n_3,
      \processQ_reg[7]_14\(3) => horiz_counter_n_54,
      \processQ_reg[7]_14\(2) => horiz_counter_n_55,
      \processQ_reg[7]_14\(1) => horiz_counter_n_56,
      \processQ_reg[7]_14\(0) => horiz_counter_n_57,
      \processQ_reg[7]_15\(3) => horiz_counter_n_48,
      \processQ_reg[7]_15\(2) => horiz_counter_n_49,
      \processQ_reg[7]_15\(1) => horiz_counter_n_50,
      \processQ_reg[7]_15\(0) => horiz_counter_n_51,
      \processQ_reg[7]_16\(3) => horiz_counter_n_44,
      \processQ_reg[7]_16\(2) => horiz_counter_n_45,
      \processQ_reg[7]_16\(1) => horiz_counter_n_46,
      \processQ_reg[7]_16\(0) => horiz_counter_n_47,
      \processQ_reg[7]_17\(3) => horiz_counter_n_34,
      \processQ_reg[7]_17\(2) => horiz_counter_n_35,
      \processQ_reg[7]_17\(1) => horiz_counter_n_36,
      \processQ_reg[7]_17\(0) => horiz_counter_n_37,
      \processQ_reg[7]_18\(3) => horiz_counter_n_30,
      \processQ_reg[7]_18\(2) => horiz_counter_n_31,
      \processQ_reg[7]_18\(1) => horiz_counter_n_32,
      \processQ_reg[7]_18\(0) => horiz_counter_n_33,
      \processQ_reg[7]_19\(3) => horiz_counter_n_24,
      \processQ_reg[7]_19\(2) => horiz_counter_n_25,
      \processQ_reg[7]_19\(1) => horiz_counter_n_26,
      \processQ_reg[7]_19\(0) => horiz_counter_n_27,
      \processQ_reg[7]_2\(3) => vert_counter_n_59,
      \processQ_reg[7]_2\(2) => vert_counter_n_60,
      \processQ_reg[7]_2\(1) => vert_counter_n_61,
      \processQ_reg[7]_2\(0) => vert_counter_n_62,
      \processQ_reg[7]_20\(3) => horiz_counter_n_20,
      \processQ_reg[7]_20\(2) => horiz_counter_n_21,
      \processQ_reg[7]_20\(1) => horiz_counter_n_22,
      \processQ_reg[7]_20\(0) => horiz_counter_n_23,
      \processQ_reg[7]_3\(3) => vert_counter_n_17,
      \processQ_reg[7]_3\(2) => vert_counter_n_18,
      \processQ_reg[7]_3\(1) => vert_counter_n_19,
      \processQ_reg[7]_3\(0) => vert_counter_n_20,
      \processQ_reg[7]_4\(3) => vert_counter_n_49,
      \processQ_reg[7]_4\(2) => vert_counter_n_50,
      \processQ_reg[7]_4\(1) => vert_counter_n_51,
      \processQ_reg[7]_4\(0) => vert_counter_n_52,
      \processQ_reg[7]_5\(3) => vert_counter_n_35,
      \processQ_reg[7]_5\(2) => vert_counter_n_36,
      \processQ_reg[7]_5\(1) => vert_counter_n_37,
      \processQ_reg[7]_5\(0) => vert_counter_n_38,
      \processQ_reg[7]_6\(3) => vert_counter_n_39,
      \processQ_reg[7]_6\(2) => vert_counter_n_40,
      \processQ_reg[7]_6\(1) => vert_counter_n_41,
      \processQ_reg[7]_6\(0) => vert_counter_n_42,
      \processQ_reg[7]_7\(3) => vert_counter_n_25,
      \processQ_reg[7]_7\(2) => vert_counter_n_26,
      \processQ_reg[7]_7\(1) => vert_counter_n_27,
      \processQ_reg[7]_7\(0) => vert_counter_n_28,
      \processQ_reg[7]_8\(3) => vert_counter_n_29,
      \processQ_reg[7]_8\(2) => vert_counter_n_30,
      \processQ_reg[7]_8\(1) => vert_counter_n_31,
      \processQ_reg[7]_8\(0) => vert_counter_n_32,
      \processQ_reg[7]_9\(3) => horiz_counter_n_74,
      \processQ_reg[7]_9\(2) => horiz_counter_n_75,
      \processQ_reg[7]_9\(1) => horiz_counter_n_76,
      \processQ_reg[7]_9\(0) => horiz_counter_n_77,
      \processQ_reg[9]\(0) => vert_counter_n_74,
      \processQ_reg[9]_0\(0) => vert_counter_n_73,
      \processQ_reg[9]_1\(0) => vert_counter_n_64,
      \processQ_reg[9]_10\(0) => vert_counter_n_23,
      \processQ_reg[9]_11\(0) => horiz_counter_n_79,
      \processQ_reg[9]_12\(0) => horiz_counter_n_78,
      \processQ_reg[9]_13\(0) => horiz_counter_n_69,
      \processQ_reg[9]_14\(0) => horiz_counter_n_68,
      \processQ_reg[9]_15\(0) => horiz_counter_n_59,
      \processQ_reg[9]_16\(0) => horiz_counter_n_58,
      \processQ_reg[9]_17\(0) => horiz_counter_n_53,
      \processQ_reg[9]_18\(0) => horiz_counter_n_52,
      \processQ_reg[9]_19\(0) => horiz_counter_n_39,
      \processQ_reg[9]_2\(0) => vert_counter_n_63,
      \processQ_reg[9]_20\(0) => horiz_counter_n_38,
      \processQ_reg[9]_21\(0) => horiz_counter_n_29,
      \processQ_reg[9]_22\(0) => horiz_counter_n_28,
      \processQ_reg[9]_3\(0) => vert_counter_n_54,
      \processQ_reg[9]_4\(0) => vert_counter_n_53,
      \processQ_reg[9]_5\(0) => vert_counter_n_48,
      \processQ_reg[9]_6\(0) => vert_counter_n_47,
      \processQ_reg[9]_7\(0) => vert_counter_n_34,
      \processQ_reg[9]_8\(0) => vert_counter_n_33,
      \processQ_reg[9]_9\(0) => vert_counter_n_24,
      \slv_reg10_reg[3]\ => \slv_reg10_reg[3]\,
      \slv_reg10_reg[5]\ => \slv_reg10_reg[5]\,
      \slv_reg10_reg[9]\(3) => horiz_counter_n_40,
      \slv_reg10_reg[9]\(2) => horiz_counter_n_41,
      \slv_reg10_reg[9]\(1) => horiz_counter_n_42,
      \slv_reg10_reg[9]\(0) => horiz_counter_n_43,
      \slv_reg10_reg[9]_0\(9 downto 0) => \slv_reg10_reg[9]\(9 downto 0),
      \slv_reg11_reg[0]\ => \slv_reg11_reg[0]_1\,
      \slv_reg11_reg[1]\ => \slv_reg11_reg[1]\,
      \slv_reg11_reg[2]\ => \slv_reg11_reg[2]\,
      \slv_reg11_reg[3]\ => \slv_reg11_reg[3]\,
      \slv_reg11_reg[4]\ => \slv_reg11_reg[4]\,
      \slv_reg11_reg[5]\ => \slv_reg11_reg[5]_2\,
      \slv_reg11_reg[7]\(3 downto 2) => \slv_reg11_reg[9]\(4 downto 3),
      \slv_reg11_reg[7]\(1 downto 0) => \slv_reg11_reg[9]\(1 downto 0),
      \slv_reg11_reg[9]\(3) => vert_counter_n_43,
      \slv_reg11_reg[9]\(2) => vert_counter_n_44,
      \slv_reg11_reg[9]\(1) => vert_counter_n_45,
      \slv_reg11_reg[9]\(0) => vert_counter_n_46,
      \slv_reg5_reg[0]\(0) => \slv_reg5_reg[0]\(0)
    );
vert_counter: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vcounter
     port map (
      CLK => CLK,
      CO(0) => pixel_color612_in,
      DI(3) => vert_counter_n_75,
      DI(2) => vert_counter_n_76,
      DI(1) => vert_counter_n_77,
      DI(0) => vert_counter_n_78,
      DOADO(2 downto 0) => DOADO(2 downto 0),
      Q(9 downto 0) => \^processq_reg[9]\(9 downto 0),
      S(0) => S(0),
      SR(0) => SR(0),
      \dc_bias_reg[1]\ => \dc_bias_reg[1]\,
      \dc_bias_reg[3]\(0) => \dc_bias_reg[3]_0\(0),
      \dc_bias_reg[3]_0\(0) => \dc_bias_reg[3]_1\(0),
      \encoded_reg[0]\ => \encoded_reg[0]\,
      \encoded_reg[0]_0\ => vert_counter_n_16,
      \encoded_reg[8]\ => vert_counter_n_12,
      \encoded_reg[8]_0\ => vert_counter_n_14,
      \encoded_reg[8]_1\ => vert_counter_n_15,
      \encoded_reg[8]_10\(3) => vert_counter_n_35,
      \encoded_reg[8]_10\(2) => vert_counter_n_36,
      \encoded_reg[8]_10\(1) => vert_counter_n_37,
      \encoded_reg[8]_10\(0) => vert_counter_n_38,
      \encoded_reg[8]_11\(3) => vert_counter_n_39,
      \encoded_reg[8]_11\(2) => vert_counter_n_40,
      \encoded_reg[8]_11\(1) => vert_counter_n_41,
      \encoded_reg[8]_11\(0) => vert_counter_n_42,
      \encoded_reg[8]_12\(3) => vert_counter_n_43,
      \encoded_reg[8]_12\(2) => vert_counter_n_44,
      \encoded_reg[8]_12\(1) => vert_counter_n_45,
      \encoded_reg[8]_12\(0) => vert_counter_n_46,
      \encoded_reg[8]_13\(0) => vert_counter_n_47,
      \encoded_reg[8]_14\(0) => vert_counter_n_48,
      \encoded_reg[8]_15\(3) => vert_counter_n_49,
      \encoded_reg[8]_15\(2) => vert_counter_n_50,
      \encoded_reg[8]_15\(1) => vert_counter_n_51,
      \encoded_reg[8]_15\(0) => vert_counter_n_52,
      \encoded_reg[8]_16\(0) => vert_counter_n_53,
      \encoded_reg[8]_17\(0) => vert_counter_n_54,
      \encoded_reg[8]_18\(3) => vert_counter_n_55,
      \encoded_reg[8]_18\(2) => vert_counter_n_56,
      \encoded_reg[8]_18\(1) => vert_counter_n_57,
      \encoded_reg[8]_18\(0) => vert_counter_n_58,
      \encoded_reg[8]_19\(3) => vert_counter_n_59,
      \encoded_reg[8]_19\(2) => vert_counter_n_60,
      \encoded_reg[8]_19\(1) => vert_counter_n_61,
      \encoded_reg[8]_19\(0) => vert_counter_n_62,
      \encoded_reg[8]_2\(3) => vert_counter_n_17,
      \encoded_reg[8]_2\(2) => vert_counter_n_18,
      \encoded_reg[8]_2\(1) => vert_counter_n_19,
      \encoded_reg[8]_2\(0) => vert_counter_n_20,
      \encoded_reg[8]_20\(0) => vert_counter_n_63,
      \encoded_reg[8]_21\(0) => vert_counter_n_64,
      \encoded_reg[8]_22\(3) => vert_counter_n_65,
      \encoded_reg[8]_22\(2) => vert_counter_n_66,
      \encoded_reg[8]_22\(1) => vert_counter_n_67,
      \encoded_reg[8]_22\(0) => vert_counter_n_68,
      \encoded_reg[8]_23\(3) => vert_counter_n_69,
      \encoded_reg[8]_23\(2) => vert_counter_n_70,
      \encoded_reg[8]_23\(1) => vert_counter_n_71,
      \encoded_reg[8]_23\(0) => vert_counter_n_72,
      \encoded_reg[8]_24\(0) => vert_counter_n_73,
      \encoded_reg[8]_25\(0) => vert_counter_n_74,
      \encoded_reg[8]_26\(3) => vert_counter_n_79,
      \encoded_reg[8]_26\(2) => vert_counter_n_80,
      \encoded_reg[8]_26\(1) => vert_counter_n_81,
      \encoded_reg[8]_26\(0) => vert_counter_n_82,
      \encoded_reg[8]_27\ => vert_counter_n_83,
      \encoded_reg[8]_28\ => vert_counter_n_84,
      \encoded_reg[8]_29\ => vert_counter_n_85,
      \encoded_reg[8]_3\ => vert_counter_n_21,
      \encoded_reg[8]_30\ => vert_counter_n_86,
      \encoded_reg[8]_31\ => vert_counter_n_87,
      \encoded_reg[8]_4\(0) => vert_counter_n_23,
      \encoded_reg[8]_5\(0) => vert_counter_n_24,
      \encoded_reg[8]_6\(3) => vert_counter_n_25,
      \encoded_reg[8]_6\(2) => vert_counter_n_26,
      \encoded_reg[8]_6\(1) => vert_counter_n_27,
      \encoded_reg[8]_6\(0) => vert_counter_n_28,
      \encoded_reg[8]_7\(3) => vert_counter_n_29,
      \encoded_reg[8]_7\(2) => vert_counter_n_30,
      \encoded_reg[8]_7\(1) => vert_counter_n_31,
      \encoded_reg[8]_7\(0) => vert_counter_n_32,
      \encoded_reg[8]_8\(0) => vert_counter_n_33,
      \encoded_reg[8]_9\(0) => vert_counter_n_34,
      \encoded_reg[9]\ => \^encoded_reg[9]\,
      \encoded_reg[9]_0\ => \encoded_reg[9]_0\,
      \encoded_reg[9]_1\ => \encoded_reg[9]_1\,
      \int_trigger_volt_s_reg[2]\ => \int_trigger_volt_s_reg[2]\,
      \int_trigger_volt_s_reg[2]_0\ => \int_trigger_volt_s_reg[2]_0\,
      \int_trigger_volt_s_reg[2]_1\ => sF_n_33,
      \int_trigger_volt_s_reg[2]_2\ => \int_trigger_volt_s_reg[2]_1\,
      \int_trigger_volt_s_reg[3]\ => \int_trigger_volt_s_reg[3]\,
      \int_trigger_volt_s_reg[7]\ => sF_n_31,
      \int_trigger_volt_s_reg[9]\(3) => \int_trigger_volt_s_reg[9]\(5),
      \int_trigger_volt_s_reg[9]\(2 downto 0) => \int_trigger_volt_s_reg[9]\(2 downto 0),
      \processQ_reg[0]_0\ => horiz_counter_n_89,
      \processQ_reg[0]_1\ => horiz_counter_n_97,
      \processQ_reg[0]_2\ => sF_n_34,
      \processQ_reg[1]_0\ => horiz_counter_n_90,
      \processQ_reg[1]_1\(1 downto 0) => \^sdp_bl.ramb18_dp_bl.ram18_bl\(1 downto 0),
      \processQ_reg[2]_0\ => horiz_counter_n_92,
      \processQ_reg[4]_0\ => horiz_counter_n_94,
      \processQ_reg[4]_1\ => horiz_counter_n_93,
      \processQ_reg[5]_0\ => horiz_counter_n_88,
      \processQ_reg[6]_0\ => horiz_counter_n_7,
      \processQ_reg[7]_0\ => horiz_counter_n_96,
      \processQ_reg[8]_0\ => horiz_counter_n_9,
      \processQ_reg[8]_1\ => horiz_counter_n_95,
      \processQ_reg[8]_2\ => \^encoded_reg[8]_0\,
      \processQ_reg[9]_0\ => horiz_counter_n_91,
      \processQ_reg[9]_1\(0) => pixel_color616_in,
      \processQ_reg[9]_2\(0) => pixel_color517_in,
      \processQ_reg[9]_3\(0) => pixel_color513_in,
      \processQ_reg[9]_4\ => \^encoded_reg[8]_2\,
      reset_n => reset_n,
      \slv_reg11_reg[0]\ => \slv_reg11_reg[0]\,
      \slv_reg11_reg[0]_0\ => \slv_reg11_reg[0]_0\,
      \slv_reg11_reg[0]_1\ => \slv_reg11_reg[0]_2\,
      \slv_reg11_reg[0]_2\ => \slv_reg11_reg[0]_1\,
      \slv_reg11_reg[1]\ => \slv_reg11_reg[1]\,
      \slv_reg11_reg[1]_0\ => \slv_reg11_reg[1]_0\,
      \slv_reg11_reg[2]\ => \slv_reg11_reg[2]\,
      \slv_reg11_reg[3]\ => \slv_reg11_reg[3]\,
      \slv_reg11_reg[4]\ => \slv_reg11_reg[4]\,
      \slv_reg11_reg[5]\ => \slv_reg11_reg[5]\,
      \slv_reg11_reg[5]_0\ => \slv_reg11_reg[5]_0\,
      \slv_reg11_reg[5]_1\ => sF_n_32,
      \slv_reg11_reg[5]_2\ => \slv_reg11_reg[5]_1\,
      \slv_reg11_reg[5]_3\ => \slv_reg11_reg[5]_2\,
      \slv_reg11_reg[6]\ => \slv_reg11_reg[6]\,
      \slv_reg11_reg[6]_0\ => \slv_reg11_reg[6]_0\,
      \slv_reg11_reg[6]_1\ => \slv_reg11_reg[6]_1\,
      \slv_reg11_reg[6]_2\ => \slv_reg11_reg[6]_2\,
      \slv_reg11_reg[6]_3\ => \slv_reg11_reg[6]_3\,
      \slv_reg11_reg[6]_4\ => \slv_reg11_reg[6]_4\,
      \slv_reg11_reg[7]\ => \slv_reg11_reg[7]\,
      \slv_reg11_reg[7]_0\ => \slv_reg11_reg[7]_0\,
      \slv_reg11_reg[7]_1\ => \slv_reg11_reg[7]_1\,
      \slv_reg11_reg[8]\ => \slv_reg11_reg[8]\,
      \slv_reg11_reg[9]\(3) => \slv_reg11_reg[9]\(5),
      \slv_reg11_reg[9]\(2 downto 0) => \slv_reg11_reg[9]\(2 downto 0),
      \slv_reg11_reg[9]_0\ => \slv_reg11_reg[9]_0\,
      \slv_reg11_reg[9]_1\(0) => pixel_color3,
      \slv_reg5_reg[0]\(0) => \slv_reg5_reg[0]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper is
  port (
    ac_mclk : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    BCLK_int_reg : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    CLK : out STD_LOGIC;
    \R_unsigned_data_prev_reg[9]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 17 downto 0 );
    \R_unsigned_data_prev_reg[8]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \L_unsigned_data_prev_reg[9]\ : out STD_LOGIC;
    \L_bus_in_s_reg[17]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    \L_unsigned_data_prev_reg[8]\ : out STD_LOGIC;
    \L_unsigned_data_prev_reg[7]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \state_reg[0]\ : out STD_LOGIC;
    \axi_rdata_reg[15]\ : out STD_LOGIC_VECTOR ( 14 downto 0 );
    \axi_rdata_reg[2]\ : out STD_LOGIC;
    DIBDI : out STD_LOGIC_VECTOR ( 9 downto 0 );
    \sdp_bl.ramb18_dp_bl.ram18_bl\ : out STD_LOGIC_VECTOR ( 9 downto 0 );
    ac_dac_sdata : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    \^clk\ : in STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    state : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \processQ_reg[9]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_araddr_reg[4]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_0\ : in STD_LOGIC;
    \axi_araddr_reg[6]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \slv_reg3_reg[0]\ : in STD_LOGIC;
    \slv_reg15_reg[0]\ : in STD_LOGIC;
    \axi_araddr_reg[3]_rep\ : in STD_LOGIC;
    \slv_reg5_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \axi_araddr_reg[2]_rep\ : in STD_LOGIC;
    \slv_reg4_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg11_reg[9]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \slv_reg10_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    flagQ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_araddr_reg[4]_1\ : in STD_LOGIC;
    \axi_araddr_reg[4]_2\ : in STD_LOGIC;
    \axi_araddr_reg[4]_3\ : in STD_LOGIC;
    \slv_reg3_reg[1]\ : in STD_LOGIC;
    \slv_reg3_reg[2]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_4\ : in STD_LOGIC;
    \axi_araddr_reg[4]_5\ : in STD_LOGIC;
    \axi_araddr_reg[4]_6\ : in STD_LOGIC;
    \slv_reg3_reg[3]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_7\ : in STD_LOGIC;
    \axi_araddr_reg[4]_8\ : in STD_LOGIC;
    \axi_araddr_reg[4]_9\ : in STD_LOGIC;
    \slv_reg3_reg[4]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_10\ : in STD_LOGIC;
    \axi_araddr_reg[4]_11\ : in STD_LOGIC;
    \axi_araddr_reg[4]_12\ : in STD_LOGIC;
    \slv_reg3_reg[5]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_13\ : in STD_LOGIC;
    \axi_araddr_reg[4]_14\ : in STD_LOGIC;
    \axi_araddr_reg[4]_15\ : in STD_LOGIC;
    \slv_reg3_reg[6]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_16\ : in STD_LOGIC;
    \axi_araddr_reg[4]_17\ : in STD_LOGIC;
    \axi_araddr_reg[4]_18\ : in STD_LOGIC;
    \slv_reg3_reg[7]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_19\ : in STD_LOGIC;
    \axi_araddr_reg[4]_20\ : in STD_LOGIC;
    \axi_araddr_reg[4]_21\ : in STD_LOGIC;
    \slv_reg3_reg[8]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_22\ : in STD_LOGIC;
    \axi_araddr_reg[4]_23\ : in STD_LOGIC;
    \axi_araddr_reg[4]_24\ : in STD_LOGIC;
    \slv_reg3_reg[9]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_25\ : in STD_LOGIC;
    \axi_araddr_reg[4]_26\ : in STD_LOGIC;
    \axi_araddr_reg[4]_27\ : in STD_LOGIC;
    \slv_reg3_reg[10]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_28\ : in STD_LOGIC;
    \axi_araddr_reg[4]_29\ : in STD_LOGIC;
    \axi_araddr_reg[4]_30\ : in STD_LOGIC;
    \slv_reg3_reg[11]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_31\ : in STD_LOGIC;
    \axi_araddr_reg[4]_32\ : in STD_LOGIC;
    \axi_araddr_reg[4]_33\ : in STD_LOGIC;
    \slv_reg3_reg[12]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_34\ : in STD_LOGIC;
    \axi_araddr_reg[4]_35\ : in STD_LOGIC;
    \axi_araddr_reg[4]_36\ : in STD_LOGIC;
    \slv_reg3_reg[13]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_37\ : in STD_LOGIC;
    \axi_araddr_reg[4]_38\ : in STD_LOGIC;
    \axi_araddr_reg[4]_39\ : in STD_LOGIC;
    \slv_reg3_reg[14]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_40\ : in STD_LOGIC;
    \axi_araddr_reg[4]_41\ : in STD_LOGIC;
    \axi_araddr_reg[4]_42\ : in STD_LOGIC;
    \slv_reg3_reg[15]\ : in STD_LOGIC;
    \L_bus_in_s_reg[17]_0\ : in STD_LOGIC_VECTOR ( 17 downto 0 );
    \R_bus_in_s_reg[17]\ : in STD_LOGIC_VECTOR ( 17 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    switch : in STD_LOGIC_VECTOR ( 0 to 0 );
    \L_unsigned_data_prev_reg[9]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg11_reg[7]\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[9]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \slv_reg11_reg[6]\ : in STD_LOGIC;
    \slv_reg11_reg[5]\ : in STD_LOGIC;
    \slv_reg11_reg[4]\ : in STD_LOGIC;
    \slv_reg11_reg[3]\ : in STD_LOGIC;
    \slv_reg11_reg[1]\ : in STD_LOGIC;
    \slv_reg11_reg[8]\ : in STD_LOGIC;
    \slv_reg2_reg[15]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg1_reg[15]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    lopt : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper is
  signal \^bclk_int_reg\ : STD_LOGIC;
  signal \^clk_1\ : STD_LOGIC;
  signal ac_lrclk_count0 : STD_LOGIC;
  signal \ac_lrclk_count_reg__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ac_lrclk_sig_prev_reg_n_0 : STD_LOGIC;
  signal audio_inout_n_3 : STD_LOGIC;
  signal audio_inout_n_72 : STD_LOGIC;
  signal audio_inout_n_73 : STD_LOGIC;
  signal \axi_rdata[0]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_4_n_0\ : STD_LOGIC;
  signal clk_50 : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ac_lrclk_count[0]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \ac_lrclk_count[1]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \ac_lrclk_count[2]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \ac_lrclk_count[3]_i_3\ : label is "soft_lutpair33";
begin
  BCLK_int_reg <= \^bclk_int_reg\;
  CLK <= \^clk_1\;
\ac_lrclk_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ac_lrclk_count_reg__0\(0),
      O => plusOp(0)
    );
\ac_lrclk_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ac_lrclk_count_reg__0\(1),
      I1 => \ac_lrclk_count_reg__0\(0),
      O => plusOp(1)
    );
\ac_lrclk_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \ac_lrclk_count_reg__0\(2),
      I1 => \ac_lrclk_count_reg__0\(0),
      I2 => \ac_lrclk_count_reg__0\(1),
      O => plusOp(2)
    );
\ac_lrclk_count[3]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \ac_lrclk_count_reg__0\(3),
      I1 => \ac_lrclk_count_reg__0\(1),
      I2 => \ac_lrclk_count_reg__0\(0),
      I3 => \ac_lrclk_count_reg__0\(2),
      O => plusOp(3)
    );
\ac_lrclk_count_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^clk\,
      CE => ac_lrclk_count0,
      D => plusOp(0),
      Q => \ac_lrclk_count_reg__0\(0),
      R => audio_inout_n_3
    );
\ac_lrclk_count_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^clk\,
      CE => ac_lrclk_count0,
      D => plusOp(1),
      Q => \ac_lrclk_count_reg__0\(1),
      R => audio_inout_n_3
    );
\ac_lrclk_count_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^clk\,
      CE => ac_lrclk_count0,
      D => plusOp(2),
      Q => \ac_lrclk_count_reg__0\(2),
      R => audio_inout_n_3
    );
\ac_lrclk_count_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^clk\,
      CE => ac_lrclk_count0,
      D => plusOp(3),
      Q => \ac_lrclk_count_reg__0\(3),
      R => audio_inout_n_3
    );
ac_lrclk_sig_prev_reg: unisim.vcomponents.FDRE
     port map (
      C => \^clk\,
      CE => '1',
      D => audio_inout_n_72,
      Q => ac_lrclk_sig_prev_reg_n_0,
      R => '0'
    );
audio_inout: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl
     port map (
      CO(0) => CO(0),
      D(4 downto 0) => D(4 downto 0),
      DIBDI(9 downto 0) => DIBDI(9 downto 0),
      E(0) => ac_lrclk_count0,
      \L_bus_in_s_reg[17]\(17 downto 0) => \L_bus_in_s_reg[17]\(17 downto 0),
      \L_bus_in_s_reg[17]_0\(17 downto 0) => \L_bus_in_s_reg[17]_0\(17 downto 0),
      \L_unsigned_data_prev_reg[7]\(4 downto 0) => \L_unsigned_data_prev_reg[7]\(4 downto 0),
      \L_unsigned_data_prev_reg[8]\ => \L_unsigned_data_prev_reg[8]\,
      \L_unsigned_data_prev_reg[9]\ => \L_unsigned_data_prev_reg[9]\,
      \L_unsigned_data_prev_reg[9]_0\(0) => \L_unsigned_data_prev_reg[9]_0\(0),
      Q(3 downto 0) => \ac_lrclk_count_reg__0\(3 downto 0),
      \R_bus_in_s_reg[17]\(17 downto 0) => Q(17 downto 0),
      \R_bus_in_s_reg[17]_0\(17 downto 0) => \R_bus_in_s_reg[17]\(17 downto 0),
      \R_unsigned_data_prev_reg[8]\ => \R_unsigned_data_prev_reg[8]\,
      \R_unsigned_data_prev_reg[9]\ => \R_unsigned_data_prev_reg[9]\,
      SR(0) => \^bclk_int_reg\,
      ac_adc_sdata => ac_adc_sdata,
      ac_bclk => ac_bclk,
      ac_dac_sdata => ac_dac_sdata,
      ac_lrclk => ac_lrclk,
      \ac_lrclk_count_reg[0]\(0) => audio_inout_n_3,
      ac_lrclk_sig_prev_reg => audio_inout_n_72,
      ac_lrclk_sig_prev_reg_0 => ac_lrclk_sig_prev_reg_n_0,
      \axi_araddr_reg[2]_rep\ => \axi_araddr_reg[2]_rep\,
      \axi_araddr_reg[3]_rep\ => \axi_araddr_reg[3]_rep\,
      \axi_araddr_reg[4]\ => \axi_araddr_reg[4]\,
      \axi_araddr_reg[4]_0\ => \axi_araddr_reg[4]_0\,
      \axi_araddr_reg[4]_1\ => \axi_rdata_reg[0]_i_4_n_0\,
      \axi_araddr_reg[4]_10\ => \axi_araddr_reg[4]_9\,
      \axi_araddr_reg[4]_11\ => \axi_araddr_reg[4]_10\,
      \axi_araddr_reg[4]_12\ => \axi_araddr_reg[4]_11\,
      \axi_araddr_reg[4]_13\ => \axi_araddr_reg[4]_12\,
      \axi_araddr_reg[4]_14\ => \axi_araddr_reg[4]_13\,
      \axi_araddr_reg[4]_15\ => \axi_araddr_reg[4]_14\,
      \axi_araddr_reg[4]_16\ => \axi_araddr_reg[4]_15\,
      \axi_araddr_reg[4]_17\ => \axi_araddr_reg[4]_16\,
      \axi_araddr_reg[4]_18\ => \axi_araddr_reg[4]_17\,
      \axi_araddr_reg[4]_19\ => \axi_araddr_reg[4]_18\,
      \axi_araddr_reg[4]_2\ => \axi_araddr_reg[4]_1\,
      \axi_araddr_reg[4]_20\ => \axi_araddr_reg[4]_19\,
      \axi_araddr_reg[4]_21\ => \axi_araddr_reg[4]_20\,
      \axi_araddr_reg[4]_22\ => \axi_araddr_reg[4]_21\,
      \axi_araddr_reg[4]_23\ => \axi_araddr_reg[4]_22\,
      \axi_araddr_reg[4]_24\ => \axi_araddr_reg[4]_23\,
      \axi_araddr_reg[4]_25\ => \axi_araddr_reg[4]_24\,
      \axi_araddr_reg[4]_26\ => \axi_araddr_reg[4]_25\,
      \axi_araddr_reg[4]_27\ => \axi_araddr_reg[4]_26\,
      \axi_araddr_reg[4]_28\ => \axi_araddr_reg[4]_27\,
      \axi_araddr_reg[4]_29\ => \axi_araddr_reg[4]_28\,
      \axi_araddr_reg[4]_3\ => \axi_araddr_reg[4]_2\,
      \axi_araddr_reg[4]_30\ => \axi_araddr_reg[4]_29\,
      \axi_araddr_reg[4]_31\ => \axi_araddr_reg[4]_30\,
      \axi_araddr_reg[4]_32\ => \axi_araddr_reg[4]_31\,
      \axi_araddr_reg[4]_33\ => \axi_araddr_reg[4]_32\,
      \axi_araddr_reg[4]_34\ => \axi_araddr_reg[4]_33\,
      \axi_araddr_reg[4]_35\ => \axi_araddr_reg[4]_34\,
      \axi_araddr_reg[4]_36\ => \axi_araddr_reg[4]_35\,
      \axi_araddr_reg[4]_37\ => \axi_araddr_reg[4]_36\,
      \axi_araddr_reg[4]_38\ => \axi_araddr_reg[4]_37\,
      \axi_araddr_reg[4]_39\ => \axi_araddr_reg[4]_38\,
      \axi_araddr_reg[4]_4\ => \axi_araddr_reg[4]_3\,
      \axi_araddr_reg[4]_40\ => \axi_araddr_reg[4]_39\,
      \axi_araddr_reg[4]_41\ => \axi_araddr_reg[4]_40\,
      \axi_araddr_reg[4]_42\ => \axi_araddr_reg[4]_41\,
      \axi_araddr_reg[4]_43\ => \axi_araddr_reg[4]_42\,
      \axi_araddr_reg[4]_5\ => \axi_araddr_reg[4]_4\,
      \axi_araddr_reg[4]_6\ => \axi_araddr_reg[4]_5\,
      \axi_araddr_reg[4]_7\ => \axi_araddr_reg[4]_6\,
      \axi_araddr_reg[4]_8\ => \axi_araddr_reg[4]_7\,
      \axi_araddr_reg[4]_9\ => \axi_araddr_reg[4]_8\,
      \axi_araddr_reg[6]\(2 downto 0) => \axi_araddr_reg[6]\(2 downto 0),
      \axi_rdata_reg[15]\(14 downto 0) => \axi_rdata_reg[15]\(14 downto 0),
      \axi_rdata_reg[2]\ => \axi_rdata_reg[2]\,
      clk => \^clk\,
      \int_trigger_volt_s_reg[9]\(4 downto 0) => \int_trigger_volt_s_reg[9]\(4 downto 0),
      \processQ_reg[9]\(0) => \processQ_reg[9]\(0),
      ready_sig_reg => audio_inout_n_73,
      ready_sig_reg_0 => \^clk_1\,
      reset_n => reset_n,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(9 downto 0) => \sdp_bl.ramb18_dp_bl.ram18_bl\(9 downto 0),
      \slv_reg11_reg[1]\ => \slv_reg11_reg[1]\,
      \slv_reg11_reg[3]\ => \slv_reg11_reg[3]\,
      \slv_reg11_reg[4]\ => \slv_reg11_reg[4]\,
      \slv_reg11_reg[5]\ => \slv_reg11_reg[5]\,
      \slv_reg11_reg[6]\ => \slv_reg11_reg[6]\,
      \slv_reg11_reg[7]\ => \slv_reg11_reg[7]\,
      \slv_reg11_reg[8]\ => \slv_reg11_reg[8]\,
      \slv_reg11_reg[9]\(4 downto 0) => \slv_reg11_reg[9]\(4 downto 0),
      \slv_reg1_reg[15]\(9 downto 0) => \slv_reg1_reg[15]\(9 downto 0),
      \slv_reg2_reg[15]\(9 downto 0) => \slv_reg2_reg[15]\(9 downto 0),
      \slv_reg3_reg[0]\ => \slv_reg3_reg[0]\,
      \slv_reg3_reg[10]\ => \slv_reg3_reg[10]\,
      \slv_reg3_reg[11]\ => \slv_reg3_reg[11]\,
      \slv_reg3_reg[12]\ => \slv_reg3_reg[12]\,
      \slv_reg3_reg[13]\ => \slv_reg3_reg[13]\,
      \slv_reg3_reg[14]\ => \slv_reg3_reg[14]\,
      \slv_reg3_reg[15]\ => \slv_reg3_reg[15]\,
      \slv_reg3_reg[1]\ => \slv_reg3_reg[1]\,
      \slv_reg3_reg[2]\ => \slv_reg3_reg[2]\,
      \slv_reg3_reg[3]\ => \slv_reg3_reg[3]\,
      \slv_reg3_reg[4]\ => \slv_reg3_reg[4]\,
      \slv_reg3_reg[5]\ => \slv_reg3_reg[5]\,
      \slv_reg3_reg[6]\ => \slv_reg3_reg[6]\,
      \slv_reg3_reg[7]\ => \slv_reg3_reg[7]\,
      \slv_reg3_reg[8]\ => \slv_reg3_reg[8]\,
      \slv_reg3_reg[9]\ => \slv_reg3_reg[9]\,
      \slv_reg4_reg[15]\(15 downto 0) => \slv_reg4_reg[15]\(15 downto 0),
      \slv_reg5_reg[15]\(15 downto 0) => \slv_reg5_reg[15]\(15 downto 0),
      state(1 downto 0) => state(1 downto 0),
      \state_reg[0]\ => \state_reg[0]\,
      switch(0) => switch(0)
    );
audiocodec_master_clock: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1
     port map (
      clk_in1 => \^clk\,
      clk_out1 => ac_mclk,
      clk_out2 => clk_50,
      lopt => lopt,
      resetn => reset_n
    );
\axi_rdata[0]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(0),
      I1 => \slv_reg10_reg[0]\(0),
      I2 => \axi_araddr_reg[3]_rep\,
      I3 => \^clk_1\,
      I4 => \axi_araddr_reg[2]_rep\,
      I5 => flagQ(0),
      O => \axi_rdata[0]_i_10_n_0\
    );
\axi_rdata_reg[0]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_10_n_0\,
      I1 => \slv_reg15_reg[0]\,
      O => \axi_rdata_reg[0]_i_4_n_0\,
      S => \axi_araddr_reg[6]\(0)
    );
initialize_audio: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init
     port map (
      SR(0) => \^bclk_int_reg\,
      clk_out2 => clk_50,
      reset_n => reset_n,
      scl => scl,
      sda => sda
    );
ready_sig_reg: unisim.vcomponents.FDRE
     port map (
      C => \^clk\,
      CE => '1',
      D => audio_inout_n_73,
      Q => \^clk_1\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video is
  port (
    tmds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tmdsb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \int_trigger_time_s_reg[0]\ : out STD_LOGIC;
    ADDRARDADDR : out STD_LOGIC_VECTOR ( 9 downto 0 );
    \processQ_reg[9]\ : out STD_LOGIC_VECTOR ( 9 downto 0 );
    \int_trigger_time_s_reg[0]_0\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_1\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_2\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_3\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_4\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_5\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_6\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_7\ : out STD_LOGIC;
    \int_trigger_time_s_reg[0]_8\ : out STD_LOGIC;
    \encoded_reg[8]\ : out STD_LOGIC;
    \state_reg[0]\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 0 to 0 );
    \state_reg[0]_0\ : out STD_LOGIC;
    \encoded_reg[8]_0\ : out STD_LOGIC;
    \state_reg[0]_1\ : out STD_LOGIC;
    \state_reg[0]_2\ : out STD_LOGIC;
    \state_reg[0]_3\ : out STD_LOGIC;
    \state_reg[0]_4\ : out STD_LOGIC;
    reset_n : in STD_LOGIC;
    clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg5_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg10_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    switch : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \processQ_reg[9]_0\ : in STD_LOGIC;
    \processQ_reg[8]\ : in STD_LOGIC;
    \int_trigger_volt_s_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg11_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    DOADO : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \processQ_reg[9]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    lopt : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video is
  signal Inst_vga_n_25 : STD_LOGIC;
  signal Inst_vga_n_26 : STD_LOGIC;
  signal Inst_vga_n_3 : STD_LOGIC;
  signal Inst_vga_n_35 : STD_LOGIC;
  signal Inst_vga_n_36 : STD_LOGIC;
  signal Inst_vga_n_37 : STD_LOGIC;
  signal Inst_vga_n_38 : STD_LOGIC;
  signal Inst_vga_n_39 : STD_LOGIC;
  signal Inst_vga_n_4 : STD_LOGIC;
  signal Inst_vga_n_40 : STD_LOGIC;
  signal Inst_vga_n_41 : STD_LOGIC;
  signal Inst_vga_n_44 : STD_LOGIC;
  signal Inst_vga_n_45 : STD_LOGIC;
  signal Inst_vga_n_46 : STD_LOGIC;
  signal Inst_vga_n_5 : STD_LOGIC;
  signal Inst_vga_n_6 : STD_LOGIC;
  signal \TDMS_encoder_blue/p_1_in\ : STD_LOGIC;
  signal \TDMS_encoder_green/p_1_in\ : STD_LOGIC;
  signal \TDMS_encoder_red/p_1_in\ : STD_LOGIC;
  signal blank : STD_LOGIC;
  signal blue_s : STD_LOGIC;
  signal clock_s : STD_LOGIC;
  signal \^encoded_reg[8]_0\ : STD_LOGIC;
  signal green_s : STD_LOGIC;
  signal inst_dvid_n_10 : STD_LOGIC;
  signal inst_dvid_n_11 : STD_LOGIC;
  signal inst_dvid_n_12 : STD_LOGIC;
  signal inst_dvid_n_13 : STD_LOGIC;
  signal inst_dvid_n_14 : STD_LOGIC;
  signal inst_dvid_n_15 : STD_LOGIC;
  signal inst_dvid_n_17 : STD_LOGIC;
  signal inst_dvid_n_21 : STD_LOGIC;
  signal inst_dvid_n_24 : STD_LOGIC;
  signal inst_dvid_n_25 : STD_LOGIC;
  signal inst_dvid_n_26 : STD_LOGIC;
  signal inst_dvid_n_28 : STD_LOGIC;
  signal inst_dvid_n_29 : STD_LOGIC;
  signal inst_dvid_n_30 : STD_LOGIC;
  signal inst_dvid_n_31 : STD_LOGIC;
  signal inst_dvid_n_32 : STD_LOGIC;
  signal inst_dvid_n_33 : STD_LOGIC;
  signal inst_dvid_n_34 : STD_LOGIC;
  signal inst_dvid_n_35 : STD_LOGIC;
  signal inst_dvid_n_36 : STD_LOGIC;
  signal inst_dvid_n_37 : STD_LOGIC;
  signal inst_dvid_n_38 : STD_LOGIC;
  signal inst_dvid_n_39 : STD_LOGIC;
  signal inst_dvid_n_40 : STD_LOGIC;
  signal inst_dvid_n_41 : STD_LOGIC;
  signal inst_dvid_n_42 : STD_LOGIC;
  signal inst_dvid_n_43 : STD_LOGIC;
  signal inst_dvid_n_45 : STD_LOGIC;
  signal inst_dvid_n_46 : STD_LOGIC;
  signal inst_dvid_n_6 : STD_LOGIC;
  signal inst_dvid_n_9 : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_0\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_1\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_4\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_5\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_6\ : STD_LOGIC;
  signal \^int_trigger_time_s_reg[0]_7\ : STD_LOGIC;
  signal pixel_clk : STD_LOGIC;
  signal red_s : STD_LOGIC;
  signal serialize_clk : STD_LOGIC;
  signal serialize_clk_n : STD_LOGIC;
  signal \^state_reg[0]\ : STD_LOGIC;
  signal \^state_reg[0]_0\ : STD_LOGIC;
  signal \^state_reg[0]_1\ : STD_LOGIC;
  signal \^state_reg[0]_2\ : STD_LOGIC;
  signal \^state_reg[0]_3\ : STD_LOGIC;
  signal \^state_reg[0]_4\ : STD_LOGIC;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of OBUFDS_blue : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of OBUFDS_blue : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of OBUFDS_blue : label is "OBUFDS";
  attribute BOX_TYPE of OBUFDS_clock : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_clock : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of OBUFDS_clock : label is "OBUFDS";
  attribute BOX_TYPE of OBUFDS_green : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_green : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of OBUFDS_green : label is "OBUFDS";
  attribute BOX_TYPE of OBUFDS_red : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_red : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM of OBUFDS_red : label is "OBUFDS";
begin
  \encoded_reg[8]_0\ <= \^encoded_reg[8]_0\;
  \int_trigger_time_s_reg[0]\ <= \^int_trigger_time_s_reg[0]\;
  \int_trigger_time_s_reg[0]_0\ <= \^int_trigger_time_s_reg[0]_0\;
  \int_trigger_time_s_reg[0]_1\ <= \^int_trigger_time_s_reg[0]_1\;
  \int_trigger_time_s_reg[0]_4\ <= \^int_trigger_time_s_reg[0]_4\;
  \int_trigger_time_s_reg[0]_5\ <= \^int_trigger_time_s_reg[0]_5\;
  \int_trigger_time_s_reg[0]_6\ <= \^int_trigger_time_s_reg[0]_6\;
  \int_trigger_time_s_reg[0]_7\ <= \^int_trigger_time_s_reg[0]_7\;
  \state_reg[0]\ <= \^state_reg[0]\;
  \state_reg[0]_0\ <= \^state_reg[0]_0\;
  \state_reg[0]_1\ <= \^state_reg[0]_1\;
  \state_reg[0]_2\ <= \^state_reg[0]_2\;
  \state_reg[0]_3\ <= \^state_reg[0]_3\;
  \state_reg[0]_4\ <= \^state_reg[0]_4\;
Inst_vga: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga
     port map (
      ADDRARDADDR(7 downto 0) => ADDRARDADDR(9 downto 2),
      CLK => pixel_clk,
      CO(0) => CO(0),
      DOADO(2 downto 0) => DOADO(2 downto 0),
      Q(9 downto 0) => Q(9 downto 0),
      S(0) => S(0),
      SR(0) => blank,
      \dc_bias_reg[1]\ => inst_dvid_n_46,
      \dc_bias_reg[2]\ => inst_dvid_n_45,
      \dc_bias_reg[3]\ => Inst_vga_n_35,
      \dc_bias_reg[3]_0\(0) => \TDMS_encoder_green/p_1_in\,
      \dc_bias_reg[3]_1\(0) => \TDMS_encoder_blue/p_1_in\,
      \dc_bias_reg[3]_2\(0) => \TDMS_encoder_red/p_1_in\,
      \encoded_reg[0]\ => Inst_vga_n_6,
      \encoded_reg[0]_0\ => Inst_vga_n_40,
      \encoded_reg[0]_1\ => Inst_vga_n_41,
      \encoded_reg[1]\ => Inst_vga_n_38,
      \encoded_reg[2]\ => Inst_vga_n_37,
      \encoded_reg[4]\ => Inst_vga_n_39,
      \encoded_reg[8]\ => Inst_vga_n_3,
      \encoded_reg[8]_0\ => Inst_vga_n_5,
      \encoded_reg[8]_1\ => Inst_vga_n_25,
      \encoded_reg[8]_2\ => Inst_vga_n_26,
      \encoded_reg[8]_3\ => \encoded_reg[8]\,
      \encoded_reg[8]_4\ => Inst_vga_n_36,
      \encoded_reg[9]\ => Inst_vga_n_4,
      \encoded_reg[9]_0\ => Inst_vga_n_44,
      \encoded_reg[9]_1\ => Inst_vga_n_45,
      \encoded_reg[9]_2\ => Inst_vga_n_46,
      \int_trigger_time_s_reg[0]\ => \^int_trigger_time_s_reg[0]\,
      \int_trigger_time_s_reg[0]_0\ => \^int_trigger_time_s_reg[0]_0\,
      \int_trigger_time_s_reg[0]_1\ => \^int_trigger_time_s_reg[0]_1\,
      \int_trigger_time_s_reg[0]_2\ => \int_trigger_time_s_reg[0]_2\,
      \int_trigger_time_s_reg[0]_3\ => \int_trigger_time_s_reg[0]_3\,
      \int_trigger_time_s_reg[0]_4\ => \^int_trigger_time_s_reg[0]_5\,
      \int_trigger_time_s_reg[0]_5\ => \^int_trigger_time_s_reg[0]_6\,
      \int_trigger_time_s_reg[0]_6\ => \int_trigger_time_s_reg[0]_8\,
      \int_trigger_time_s_reg[1]\ => inst_dvid_n_12,
      \int_trigger_time_s_reg[1]_0\ => inst_dvid_n_13,
      \int_trigger_time_s_reg[1]_1\ => inst_dvid_n_15,
      \int_trigger_volt_s_reg[2]\ => inst_dvid_n_37,
      \int_trigger_volt_s_reg[2]_0\ => inst_dvid_n_38,
      \int_trigger_volt_s_reg[2]_1\ => inst_dvid_n_42,
      \int_trigger_volt_s_reg[3]\ => inst_dvid_n_29,
      \int_trigger_volt_s_reg[9]\(5) => \int_trigger_volt_s_reg[9]\(9),
      \int_trigger_volt_s_reg[9]\(4 downto 3) => \int_trigger_volt_s_reg[9]\(7 downto 6),
      \int_trigger_volt_s_reg[9]\(2 downto 1) => \int_trigger_volt_s_reg[9]\(3 downto 2),
      \int_trigger_volt_s_reg[9]\(0) => \int_trigger_volt_s_reg[9]\(0),
      \processQ_reg[8]\ => \processQ_reg[8]\,
      \processQ_reg[9]\(9 downto 0) => \processQ_reg[9]\(9 downto 0),
      \processQ_reg[9]_0\ => \processQ_reg[9]_0\,
      \processQ_reg[9]_1\(0) => \processQ_reg[9]_1\(0),
      reset_n => reset_n,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(1 downto 0) => ADDRARDADDR(1 downto 0),
      \slv_reg10_reg[2]\ => inst_dvid_n_14,
      \slv_reg10_reg[3]\ => \^int_trigger_time_s_reg[0]_7\,
      \slv_reg10_reg[5]\ => \^int_trigger_time_s_reg[0]_4\,
      \slv_reg10_reg[6]\ => inst_dvid_n_9,
      \slv_reg10_reg[6]_0\ => inst_dvid_n_10,
      \slv_reg10_reg[6]_1\ => inst_dvid_n_11,
      \slv_reg10_reg[7]\ => inst_dvid_n_6,
      \slv_reg10_reg[9]\(9 downto 0) => \slv_reg10_reg[9]\(9 downto 0),
      \slv_reg11_reg[0]\ => inst_dvid_n_21,
      \slv_reg11_reg[0]_0\ => inst_dvid_n_39,
      \slv_reg11_reg[0]_1\ => inst_dvid_n_28,
      \slv_reg11_reg[0]_2\ => inst_dvid_n_40,
      \slv_reg11_reg[1]\ => \^state_reg[0]\,
      \slv_reg11_reg[1]_0\ => inst_dvid_n_41,
      \slv_reg11_reg[2]\ => inst_dvid_n_25,
      \slv_reg11_reg[3]\ => \^state_reg[0]_2\,
      \slv_reg11_reg[4]\ => \^state_reg[0]_3\,
      \slv_reg11_reg[5]\ => inst_dvid_n_34,
      \slv_reg11_reg[5]_0\ => inst_dvid_n_35,
      \slv_reg11_reg[5]_1\ => inst_dvid_n_36,
      \slv_reg11_reg[5]_2\ => \^state_reg[0]_1\,
      \slv_reg11_reg[6]\ => \^state_reg[0]_4\,
      \slv_reg11_reg[6]_0\ => inst_dvid_n_26,
      \slv_reg11_reg[6]_1\ => inst_dvid_n_30,
      \slv_reg11_reg[6]_2\ => inst_dvid_n_31,
      \slv_reg11_reg[6]_3\ => inst_dvid_n_32,
      \slv_reg11_reg[6]_4\ => inst_dvid_n_33,
      \slv_reg11_reg[7]\ => inst_dvid_n_17,
      \slv_reg11_reg[7]_0\ => \^encoded_reg[8]_0\,
      \slv_reg11_reg[7]_1\ => inst_dvid_n_24,
      \slv_reg11_reg[8]\ => \^state_reg[0]_0\,
      \slv_reg11_reg[9]\(5) => \slv_reg11_reg[9]\(9),
      \slv_reg11_reg[9]\(4 downto 3) => \slv_reg11_reg[9]\(7 downto 6),
      \slv_reg11_reg[9]\(2 downto 1) => \slv_reg11_reg[9]\(3 downto 2),
      \slv_reg11_reg[9]\(0) => \slv_reg11_reg[9]\(0),
      \slv_reg11_reg[9]_0\ => inst_dvid_n_43,
      \slv_reg5_reg[0]\(0) => \slv_reg5_reg[0]\(0),
      switch(1 downto 0) => switch(1 downto 0)
    );
OBUFDS_blue: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => blue_s,
      O => tmds(0),
      OB => tmdsb(0)
    );
OBUFDS_clock: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clock_s,
      O => tmds(3),
      OB => tmdsb(3)
    );
OBUFDS_green: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => red_s,
      O => tmds(2),
      OB => tmdsb(2)
    );
OBUFDS_red: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => green_s,
      O => tmds(1),
      OB => tmdsb(1)
    );
inst_dvid: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid
     port map (
      CLK => pixel_clk,
      Q(0) => \TDMS_encoder_green/p_1_in\,
      SR(0) => blank,
      blue_s => blue_s,
      clk_out2 => serialize_clk,
      clk_out3 => serialize_clk_n,
      clock_s => clock_s,
      \dc_bias_reg[0]\(0) => \TDMS_encoder_blue/p_1_in\,
      \dc_bias_reg[3]\ => Inst_vga_n_46,
      \dc_bias_reg[3]_0\ => Inst_vga_n_6,
      \dc_bias_reg[3]_1\ => Inst_vga_n_45,
      \dc_bias_reg[3]_2\ => Inst_vga_n_39,
      \dc_bias_reg[3]_3\ => Inst_vga_n_37,
      \dc_bias_reg[3]_4\ => Inst_vga_n_38,
      \dc_bias_reg[3]_5\ => Inst_vga_n_41,
      \dc_bias_reg[3]_6\ => Inst_vga_n_44,
      \encoded_reg[0]\ => inst_dvid_n_6,
      \encoded_reg[0]_0\ => inst_dvid_n_9,
      \encoded_reg[0]_1\ => inst_dvid_n_10,
      \encoded_reg[0]_2\ => inst_dvid_n_11,
      \encoded_reg[0]_3\ => inst_dvid_n_12,
      \encoded_reg[0]_4\ => inst_dvid_n_13,
      \encoded_reg[0]_5\ => inst_dvid_n_14,
      \encoded_reg[0]_6\ => inst_dvid_n_15,
      \encoded_reg[3]\(0) => \TDMS_encoder_red/p_1_in\,
      \encoded_reg[8]\ => inst_dvid_n_17,
      \encoded_reg[8]_0\ => \^encoded_reg[8]_0\,
      \encoded_reg[8]_1\ => inst_dvid_n_21,
      \encoded_reg[8]_10\ => inst_dvid_n_33,
      \encoded_reg[8]_11\ => inst_dvid_n_34,
      \encoded_reg[8]_12\ => inst_dvid_n_35,
      \encoded_reg[8]_13\ => inst_dvid_n_36,
      \encoded_reg[8]_14\ => inst_dvid_n_37,
      \encoded_reg[8]_15\ => inst_dvid_n_38,
      \encoded_reg[8]_16\ => inst_dvid_n_39,
      \encoded_reg[8]_17\ => inst_dvid_n_40,
      \encoded_reg[8]_18\ => inst_dvid_n_41,
      \encoded_reg[8]_19\ => inst_dvid_n_42,
      \encoded_reg[8]_2\ => inst_dvid_n_24,
      \encoded_reg[8]_20\ => inst_dvid_n_43,
      \encoded_reg[8]_21\ => inst_dvid_n_45,
      \encoded_reg[8]_3\ => inst_dvid_n_25,
      \encoded_reg[8]_4\ => inst_dvid_n_26,
      \encoded_reg[8]_5\ => inst_dvid_n_28,
      \encoded_reg[8]_6\ => inst_dvid_n_29,
      \encoded_reg[8]_7\ => inst_dvid_n_30,
      \encoded_reg[8]_8\ => inst_dvid_n_31,
      \encoded_reg[8]_9\ => inst_dvid_n_32,
      \encoded_reg[9]\ => inst_dvid_n_46,
      green_s => green_s,
      \int_trigger_time_s_reg[0]\ => \^int_trigger_time_s_reg[0]_4\,
      \int_trigger_time_s_reg[0]_0\ => \^int_trigger_time_s_reg[0]_7\,
      \int_trigger_time_s_reg[5]\(3) => Q(5),
      \int_trigger_time_s_reg[5]\(2 downto 0) => Q(3 downto 1),
      \int_trigger_volt_s_reg[9]\(9 downto 0) => \int_trigger_volt_s_reg[9]\(9 downto 0),
      \processQ_reg[5]\ => Inst_vga_n_25,
      \processQ_reg[5]_0\ => Inst_vga_n_4,
      \processQ_reg[5]_1\ => Inst_vga_n_40,
      \processQ_reg[6]\ => Inst_vga_n_3,
      \processQ_reg[8]\ => Inst_vga_n_35,
      \processQ_reg[8]_0\ => Inst_vga_n_5,
      \processQ_reg[9]\ => Inst_vga_n_26,
      \processQ_reg[9]_0\ => Inst_vga_n_36,
      red_s => red_s,
      \slv_reg10_reg[1]\ => \^int_trigger_time_s_reg[0]\,
      \slv_reg10_reg[2]\ => \^int_trigger_time_s_reg[0]_6\,
      \slv_reg10_reg[4]\ => \^int_trigger_time_s_reg[0]_5\,
      \slv_reg10_reg[5]\(3) => \slv_reg10_reg[9]\(5),
      \slv_reg10_reg[5]\(2 downto 0) => \slv_reg10_reg[9]\(3 downto 1),
      \slv_reg10_reg[6]\ => \^int_trigger_time_s_reg[0]_1\,
      \slv_reg10_reg[7]\ => \^int_trigger_time_s_reg[0]_0\,
      \slv_reg11_reg[9]\(9 downto 0) => \slv_reg11_reg[9]\(9 downto 0),
      \slv_reg5_reg[0]\(0) => \slv_reg5_reg[0]\(0),
      \state_reg[0]\ => \^state_reg[0]_1\,
      \state_reg[0]_0\ => \^state_reg[0]_2\,
      \state_reg[0]_1\ => \^state_reg[0]_3\,
      \state_reg[0]_2\ => \^state_reg[0]_4\,
      \state_reg[0]_3\ => \^state_reg[0]\,
      \state_reg[0]_4\ => \^state_reg[0]_0\
    );
mmcm_adv_inst_display_clocks: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
     port map (
      clk_in1 => clk,
      clk_out1 => pixel_clk,
      clk_out2 => serialize_clk,
      clk_out3 => serialize_clk_n,
      lopt => lopt,
      resetn => reset_n
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath is
  port (
    tmds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tmdsb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ac_mclk : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    ac_bclk : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    ready : out STD_LOGIC;
    \state_reg[0]\ : out STD_LOGIC;
    \Q_reg[2]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ac_dac_sdata : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    clk : in STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    WREN : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg10_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    btn : in STD_LOGIC_VECTOR ( 4 downto 0 );
    state : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_araddr_reg[4]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_0\ : in STD_LOGIC;
    \axi_araddr_reg[6]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \slv_reg3_reg[0]\ : in STD_LOGIC;
    \slv_reg15_reg[0]\ : in STD_LOGIC;
    \axi_araddr_reg[3]_rep\ : in STD_LOGIC;
    \axi_araddr_reg[2]_rep\ : in STD_LOGIC;
    \slv_reg4_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg11_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \axi_araddr_reg[4]_1\ : in STD_LOGIC;
    \axi_araddr_reg[4]_2\ : in STD_LOGIC;
    \axi_araddr_reg[4]_3\ : in STD_LOGIC;
    \slv_reg3_reg[1]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_4\ : in STD_LOGIC;
    \axi_araddr_reg[4]_5\ : in STD_LOGIC;
    \slv_reg3_reg[2]\ : in STD_LOGIC;
    \slv_reg15_reg[2]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_6\ : in STD_LOGIC;
    \axi_araddr_reg[4]_7\ : in STD_LOGIC;
    \axi_araddr_reg[4]_8\ : in STD_LOGIC;
    \slv_reg3_reg[3]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_9\ : in STD_LOGIC;
    \axi_araddr_reg[4]_10\ : in STD_LOGIC;
    \axi_araddr_reg[4]_11\ : in STD_LOGIC;
    \slv_reg3_reg[4]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_12\ : in STD_LOGIC;
    \axi_araddr_reg[4]_13\ : in STD_LOGIC;
    \axi_araddr_reg[4]_14\ : in STD_LOGIC;
    \slv_reg3_reg[5]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_15\ : in STD_LOGIC;
    \axi_araddr_reg[4]_16\ : in STD_LOGIC;
    \axi_araddr_reg[4]_17\ : in STD_LOGIC;
    \slv_reg3_reg[6]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_18\ : in STD_LOGIC;
    \axi_araddr_reg[4]_19\ : in STD_LOGIC;
    \axi_araddr_reg[4]_20\ : in STD_LOGIC;
    \slv_reg3_reg[7]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_21\ : in STD_LOGIC;
    \axi_araddr_reg[4]_22\ : in STD_LOGIC;
    \axi_araddr_reg[4]_23\ : in STD_LOGIC;
    \slv_reg3_reg[8]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_24\ : in STD_LOGIC;
    \axi_araddr_reg[4]_25\ : in STD_LOGIC;
    \axi_araddr_reg[4]_26\ : in STD_LOGIC;
    \slv_reg3_reg[9]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_27\ : in STD_LOGIC;
    \axi_araddr_reg[4]_28\ : in STD_LOGIC;
    \axi_araddr_reg[4]_29\ : in STD_LOGIC;
    \slv_reg3_reg[10]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_30\ : in STD_LOGIC;
    \axi_araddr_reg[4]_31\ : in STD_LOGIC;
    \axi_araddr_reg[4]_32\ : in STD_LOGIC;
    \slv_reg3_reg[11]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_33\ : in STD_LOGIC;
    \axi_araddr_reg[4]_34\ : in STD_LOGIC;
    \axi_araddr_reg[4]_35\ : in STD_LOGIC;
    \slv_reg3_reg[12]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_36\ : in STD_LOGIC;
    \axi_araddr_reg[4]_37\ : in STD_LOGIC;
    \axi_araddr_reg[4]_38\ : in STD_LOGIC;
    \slv_reg3_reg[13]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_39\ : in STD_LOGIC;
    \axi_araddr_reg[4]_40\ : in STD_LOGIC;
    \axi_araddr_reg[4]_41\ : in STD_LOGIC;
    \slv_reg3_reg[14]\ : in STD_LOGIC;
    \axi_araddr_reg[4]_42\ : in STD_LOGIC;
    \axi_araddr_reg[4]_43\ : in STD_LOGIC;
    \axi_araddr_reg[4]_44\ : in STD_LOGIC;
    \slv_reg3_reg[15]\ : in STD_LOGIC;
    switch : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg2_reg[15]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg1_reg[15]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg0_reg[9]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \slv_reg3_reg[2]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \state_reg[1]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath is
  signal DI : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal L_bus_in : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal L_bus_l : STD_LOGIC;
  signal L_bus_out_s : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal L_out_bram : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal L_unsigned_data_prev : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal Lbus_out : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^q_reg[2]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal R_bus_in : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal R_bus_l : STD_LOGIC;
  signal R_bus_out_s : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal R_unsigned_data_prev : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal Rbus_out : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal WRADDR : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal audio_codec_n_24 : STD_LOGIC;
  signal audio_codec_n_25 : STD_LOGIC;
  signal audio_codec_n_26 : STD_LOGIC;
  signal audio_codec_n_27 : STD_LOGIC;
  signal audio_codec_n_28 : STD_LOGIC;
  signal audio_codec_n_29 : STD_LOGIC;
  signal audio_codec_n_30 : STD_LOGIC;
  signal audio_codec_n_49 : STD_LOGIC;
  signal audio_codec_n_5 : STD_LOGIC;
  signal audio_codec_n_50 : STD_LOGIC;
  signal audio_codec_n_51 : STD_LOGIC;
  signal audio_codec_n_52 : STD_LOGIC;
  signal audio_codec_n_53 : STD_LOGIC;
  signal audio_codec_n_54 : STD_LOGIC;
  signal audio_codec_n_71 : STD_LOGIC;
  signal audio_codec_n_72 : STD_LOGIC;
  signal audio_codec_n_73 : STD_LOGIC;
  signal audio_codec_n_74 : STD_LOGIC;
  signal audio_codec_n_75 : STD_LOGIC;
  signal audio_codec_n_76 : STD_LOGIC;
  signal audio_codec_n_77 : STD_LOGIC;
  signal audio_codec_n_78 : STD_LOGIC;
  signal audio_codec_n_79 : STD_LOGIC;
  signal audio_codec_n_80 : STD_LOGIC;
  signal audio_codec_n_81 : STD_LOGIC;
  signal ch1 : STD_LOGIC;
  signal ch2 : STD_LOGIC;
  signal \clock_divider[0]_i_2_n_0\ : STD_LOGIC;
  signal clock_divider_reg : STD_LOGIC_VECTOR ( 22 downto 21 );
  signal \clock_divider_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \clock_divider_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \clock_divider_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \clock_divider_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \clock_divider_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \clock_divider_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \clock_divider_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \clock_divider_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \clock_divider_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \clock_divider_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \clock_divider_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \clock_divider_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \clock_divider_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \clock_divider_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \clock_divider_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \clock_divider_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \clock_divider_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \clock_divider_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \clock_divider_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \clock_divider_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \clock_divider_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \clock_divider_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \clock_divider_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \clock_divider_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \clock_divider_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \clock_divider_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \clock_divider_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \clock_divider_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \clock_divider_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \clock_divider_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \clock_divider_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \clock_divider_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \clock_divider_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \clock_divider_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \clock_divider_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \clock_divider_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \clock_divider_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \clock_divider_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \clock_divider_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \clock_divider_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \clock_divider_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \clock_divider_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \clock_divider_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \clock_divider_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \clock_divider_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[0]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[10]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[11]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[12]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[13]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[14]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[15]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[16]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[17]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[18]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[19]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[1]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[20]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[2]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[3]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[4]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[5]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[6]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[7]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[8]\ : STD_LOGIC;
  signal \clock_divider_reg_n_0_[9]\ : STD_LOGIC;
  signal column : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal flagQ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \int_trigger_time_s[0]_i_1_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[4]_i_2_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[4]_i_3_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[4]_i_4_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[4]_i_5_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[4]_i_6_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[8]_i_2_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[8]_i_3_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[8]_i_4_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[8]_i_5_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[9]_i_1_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[9]_i_2_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[9]_i_5_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[9]_i_6_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[9]_i_7_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s[9]_i_8_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \int_trigger_time_s_reg[9]_i_3_n_7\ : STD_LOGIC;
  signal \int_trigger_time_s_reg__0\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \int_trigger_volt_s[0]_i_1_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[4]_i_2_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[4]_i_3_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[4]_i_4_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[4]_i_5_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[4]_i_6_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[8]_i_2_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[8]_i_3_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[8]_i_4_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[8]_i_5_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[9]_i_1_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[9]_i_2_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[9]_i_4_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[9]_i_5_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[9]_i_6_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s[9]_i_7_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg[9]_i_3_n_7\ : STD_LOGIC;
  signal \int_trigger_volt_s_reg__0\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal lBRAM_n_3 : STD_LOGIC;
  signal lopt : STD_LOGIC;
  signal rBRAM_n_0 : STD_LOGIC;
  signal \^ready\ : STD_LOGIC;
  signal row : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \state[0]_i_11_n_0\ : STD_LOGIC;
  signal \state[0]_i_12_n_0\ : STD_LOGIC;
  signal \state[0]_i_17_n_0\ : STD_LOGIC;
  signal \state[0]_i_18_n_0\ : STD_LOGIC;
  signal \state[0]_i_27_n_0\ : STD_LOGIC;
  signal \state[0]_i_28_n_0\ : STD_LOGIC;
  signal \state[0]_i_29_n_0\ : STD_LOGIC;
  signal \state[0]_i_30_n_0\ : STD_LOGIC;
  signal \state[0]_i_31_n_0\ : STD_LOGIC;
  signal \state[0]_i_32_n_0\ : STD_LOGIC;
  signal \state[0]_i_33_n_0\ : STD_LOGIC;
  signal \state[0]_i_34_n_0\ : STD_LOGIC;
  signal \state[0]_i_43_n_0\ : STD_LOGIC;
  signal \state[0]_i_44_n_0\ : STD_LOGIC;
  signal \state[0]_i_45_n_0\ : STD_LOGIC;
  signal \state[0]_i_46_n_0\ : STD_LOGIC;
  signal \state[0]_i_47_n_0\ : STD_LOGIC;
  signal \state[0]_i_48_n_0\ : STD_LOGIC;
  signal \state[0]_i_49_n_0\ : STD_LOGIC;
  signal \state[0]_i_50_n_0\ : STD_LOGIC;
  signal \state_reg[0]_i_10_n_0\ : STD_LOGIC;
  signal \state_reg[0]_i_10_n_1\ : STD_LOGIC;
  signal \state_reg[0]_i_10_n_2\ : STD_LOGIC;
  signal \state_reg[0]_i_10_n_3\ : STD_LOGIC;
  signal \state_reg[0]_i_16_n_0\ : STD_LOGIC;
  signal \state_reg[0]_i_16_n_1\ : STD_LOGIC;
  signal \state_reg[0]_i_16_n_2\ : STD_LOGIC;
  signal \state_reg[0]_i_16_n_3\ : STD_LOGIC;
  signal trigger_clock : STD_LOGIC;
  signal video_inst_n_10 : STD_LOGIC;
  signal video_inst_n_11 : STD_LOGIC;
  signal video_inst_n_12 : STD_LOGIC;
  signal video_inst_n_13 : STD_LOGIC;
  signal video_inst_n_14 : STD_LOGIC;
  signal video_inst_n_15 : STD_LOGIC;
  signal video_inst_n_16 : STD_LOGIC;
  signal video_inst_n_29 : STD_LOGIC;
  signal video_inst_n_30 : STD_LOGIC;
  signal video_inst_n_31 : STD_LOGIC;
  signal video_inst_n_32 : STD_LOGIC;
  signal video_inst_n_33 : STD_LOGIC;
  signal video_inst_n_34 : STD_LOGIC;
  signal video_inst_n_35 : STD_LOGIC;
  signal video_inst_n_36 : STD_LOGIC;
  signal video_inst_n_37 : STD_LOGIC;
  signal video_inst_n_38 : STD_LOGIC;
  signal video_inst_n_39 : STD_LOGIC;
  signal video_inst_n_40 : STD_LOGIC;
  signal video_inst_n_41 : STD_LOGIC;
  signal video_inst_n_42 : STD_LOGIC;
  signal video_inst_n_43 : STD_LOGIC;
  signal video_inst_n_44 : STD_LOGIC;
  signal video_inst_n_45 : STD_LOGIC;
  signal video_inst_n_46 : STD_LOGIC;
  signal video_inst_n_8 : STD_LOGIC;
  signal video_inst_n_9 : STD_LOGIC;
  signal \NLW_clock_divider_reg[20]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_clock_divider_reg[20]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_int_trigger_time_s_reg[9]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_int_trigger_time_s_reg[9]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_int_trigger_volt_s_reg[9]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_int_trigger_volt_s_reg[9]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_state_reg[0]_i_10_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_state_reg[0]_i_16_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_state_reg[0]_i_4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_state_reg[0]_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_state_reg[0]_i_6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_state_reg[0]_i_6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of \int_trigger_time_s_reg[4]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \int_trigger_time_s_reg[8]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \int_trigger_time_s_reg[9]_i_3\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \int_trigger_volt_s[0]_i_1\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \int_trigger_volt_s[9]_i_7\ : label is "soft_lutpair95";
  attribute METHODOLOGY_DRC_VIOS of \int_trigger_volt_s_reg[4]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \int_trigger_volt_s_reg[8]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \int_trigger_volt_s_reg[9]_i_3\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \state_reg[0]_i_10\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \state_reg[0]_i_16\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \state_reg[0]_i_4\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \state_reg[0]_i_6\ : label is "{SYNTH-8 {cell *THIS*}}";
begin
  \Q_reg[2]\(0) <= \^q_reg[2]\(0);
  SR(0) <= \^sr\(0);
  ready <= \^ready\;
\L_bus_in_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => L_bus_out_s(0),
      Q => L_bus_in(0),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(8),
      Q => L_bus_in(10),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(9),
      Q => L_bus_in(11),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(10),
      Q => L_bus_in(12),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(11),
      Q => L_bus_in(13),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(12),
      Q => L_bus_in(14),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(13),
      Q => L_bus_in(15),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(14),
      Q => L_bus_in(16),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(15),
      Q => L_bus_in(17),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => L_bus_out_s(1),
      Q => L_bus_in(1),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(0),
      Q => L_bus_in(2),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(1),
      Q => L_bus_in(3),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(2),
      Q => L_bus_in(4),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(3),
      Q => L_bus_in(5),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(4),
      Q => L_bus_in(6),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(5),
      Q => L_bus_in(7),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(6),
      Q => L_bus_in(8),
      R => \^sr\(0)
    );
\L_bus_in_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Lbus_out(7),
      Q => L_bus_in(9),
      R => \^sr\(0)
    );
\L_unsigned_data_prev_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => Lbus_out(7),
      Q => L_unsigned_data_prev(0),
      R => '0'
    );
\L_unsigned_data_prev_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => Lbus_out(8),
      Q => L_unsigned_data_prev(1),
      R => '0'
    );
\L_unsigned_data_prev_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => Lbus_out(9),
      Q => L_unsigned_data_prev(2),
      R => '0'
    );
\L_unsigned_data_prev_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_54,
      Q => L_unsigned_data_prev(3),
      R => '0'
    );
\L_unsigned_data_prev_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_53,
      Q => L_unsigned_data_prev(4),
      R => '0'
    );
\L_unsigned_data_prev_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_52,
      Q => L_unsigned_data_prev(5),
      R => '0'
    );
\L_unsigned_data_prev_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_51,
      Q => L_unsigned_data_prev(6),
      R => '0'
    );
\L_unsigned_data_prev_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_50,
      Q => L_unsigned_data_prev(7),
      R => '0'
    );
\L_unsigned_data_prev_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_49,
      Q => L_unsigned_data_prev(8),
      R => '0'
    );
\L_unsigned_data_prev_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_30,
      Q => L_unsigned_data_prev(9),
      R => '0'
    );
\R_bus_in_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => R_bus_out_s(0),
      Q => R_bus_in(0),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(8),
      Q => R_bus_in(10),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(9),
      Q => R_bus_in(11),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(10),
      Q => R_bus_in(12),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(11),
      Q => R_bus_in(13),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(12),
      Q => R_bus_in(14),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(13),
      Q => R_bus_in(15),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(14),
      Q => R_bus_in(16),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(15),
      Q => R_bus_in(17),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => R_bus_out_s(1),
      Q => R_bus_in(1),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(0),
      Q => R_bus_in(2),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(1),
      Q => R_bus_in(3),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(2),
      Q => R_bus_in(4),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(3),
      Q => R_bus_in(5),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(4),
      Q => R_bus_in(6),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(5),
      Q => R_bus_in(7),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(6),
      Q => R_bus_in(8),
      R => \^sr\(0)
    );
\R_bus_in_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^ready\,
      D => Rbus_out(7),
      Q => R_bus_in(9),
      R => \^sr\(0)
    );
\R_unsigned_data_prev_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => Rbus_out(7),
      Q => R_unsigned_data_prev(0),
      R => '0'
    );
\R_unsigned_data_prev_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => Rbus_out(8),
      Q => R_unsigned_data_prev(1),
      R => '0'
    );
\R_unsigned_data_prev_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => Rbus_out(9),
      Q => R_unsigned_data_prev(2),
      R => '0'
    );
\R_unsigned_data_prev_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_29,
      Q => R_unsigned_data_prev(3),
      R => '0'
    );
\R_unsigned_data_prev_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_28,
      Q => R_unsigned_data_prev(4),
      R => '0'
    );
\R_unsigned_data_prev_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_27,
      Q => R_unsigned_data_prev(5),
      R => '0'
    );
\R_unsigned_data_prev_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_26,
      Q => R_unsigned_data_prev(6),
      R => '0'
    );
\R_unsigned_data_prev_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_25,
      Q => R_unsigned_data_prev(7),
      R => '0'
    );
\R_unsigned_data_prev_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_24,
      Q => R_unsigned_data_prev(8),
      R => '0'
    );
\R_unsigned_data_prev_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => \^ready\,
      CE => '1',
      D => audio_codec_n_5,
      Q => R_unsigned_data_prev(9),
      R => '0'
    );
audio_codec: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper
     port map (
      BCLK_int_reg => \^sr\(0),
      CLK => \^ready\,
      CO(0) => R_bus_l,
      D(4) => audio_codec_n_25,
      D(3) => audio_codec_n_26,
      D(2) => audio_codec_n_27,
      D(1) => audio_codec_n_28,
      D(0) => audio_codec_n_29,
      DIBDI(9) => audio_codec_n_72,
      DIBDI(8) => audio_codec_n_73,
      DIBDI(7) => audio_codec_n_74,
      DIBDI(6) => audio_codec_n_75,
      DIBDI(5) => audio_codec_n_76,
      DIBDI(4) => audio_codec_n_77,
      DIBDI(3) => audio_codec_n_78,
      DIBDI(2) => audio_codec_n_79,
      DIBDI(1) => audio_codec_n_80,
      DIBDI(0) => audio_codec_n_81,
      \L_bus_in_s_reg[17]\(17 downto 2) => Lbus_out(15 downto 0),
      \L_bus_in_s_reg[17]\(1 downto 0) => L_bus_out_s(1 downto 0),
      \L_bus_in_s_reg[17]_0\(17 downto 0) => L_bus_in(17 downto 0),
      \L_unsigned_data_prev_reg[7]\(4) => audio_codec_n_50,
      \L_unsigned_data_prev_reg[7]\(3) => audio_codec_n_51,
      \L_unsigned_data_prev_reg[7]\(2) => audio_codec_n_52,
      \L_unsigned_data_prev_reg[7]\(1) => audio_codec_n_53,
      \L_unsigned_data_prev_reg[7]\(0) => audio_codec_n_54,
      \L_unsigned_data_prev_reg[8]\ => audio_codec_n_49,
      \L_unsigned_data_prev_reg[9]\ => audio_codec_n_30,
      \L_unsigned_data_prev_reg[9]_0\(0) => L_bus_l,
      Q(17 downto 2) => Rbus_out(15 downto 0),
      Q(1 downto 0) => R_bus_out_s(1 downto 0),
      \R_bus_in_s_reg[17]\(17 downto 0) => R_bus_in(17 downto 0),
      \R_unsigned_data_prev_reg[8]\ => audio_codec_n_24,
      \R_unsigned_data_prev_reg[9]\ => audio_codec_n_5,
      ac_adc_sdata => ac_adc_sdata,
      ac_bclk => ac_bclk,
      ac_dac_sdata => ac_dac_sdata,
      ac_lrclk => ac_lrclk,
      ac_mclk => ac_mclk,
      \axi_araddr_reg[2]_rep\ => \axi_araddr_reg[2]_rep\,
      \axi_araddr_reg[3]_rep\ => \axi_araddr_reg[3]_rep\,
      \axi_araddr_reg[4]\ => \axi_araddr_reg[4]\,
      \axi_araddr_reg[4]_0\ => \axi_araddr_reg[4]_0\,
      \axi_araddr_reg[4]_1\ => \axi_araddr_reg[4]_1\,
      \axi_araddr_reg[4]_10\ => \axi_araddr_reg[4]_12\,
      \axi_araddr_reg[4]_11\ => \axi_araddr_reg[4]_13\,
      \axi_araddr_reg[4]_12\ => \axi_araddr_reg[4]_14\,
      \axi_araddr_reg[4]_13\ => \axi_araddr_reg[4]_15\,
      \axi_araddr_reg[4]_14\ => \axi_araddr_reg[4]_16\,
      \axi_araddr_reg[4]_15\ => \axi_araddr_reg[4]_17\,
      \axi_araddr_reg[4]_16\ => \axi_araddr_reg[4]_18\,
      \axi_araddr_reg[4]_17\ => \axi_araddr_reg[4]_19\,
      \axi_araddr_reg[4]_18\ => \axi_araddr_reg[4]_20\,
      \axi_araddr_reg[4]_19\ => \axi_araddr_reg[4]_21\,
      \axi_araddr_reg[4]_2\ => \axi_araddr_reg[4]_2\,
      \axi_araddr_reg[4]_20\ => \axi_araddr_reg[4]_22\,
      \axi_araddr_reg[4]_21\ => \axi_araddr_reg[4]_23\,
      \axi_araddr_reg[4]_22\ => \axi_araddr_reg[4]_24\,
      \axi_araddr_reg[4]_23\ => \axi_araddr_reg[4]_25\,
      \axi_araddr_reg[4]_24\ => \axi_araddr_reg[4]_26\,
      \axi_araddr_reg[4]_25\ => \axi_araddr_reg[4]_27\,
      \axi_araddr_reg[4]_26\ => \axi_araddr_reg[4]_28\,
      \axi_araddr_reg[4]_27\ => \axi_araddr_reg[4]_29\,
      \axi_araddr_reg[4]_28\ => \axi_araddr_reg[4]_30\,
      \axi_araddr_reg[4]_29\ => \axi_araddr_reg[4]_31\,
      \axi_araddr_reg[4]_3\ => \axi_araddr_reg[4]_3\,
      \axi_araddr_reg[4]_30\ => \axi_araddr_reg[4]_32\,
      \axi_araddr_reg[4]_31\ => \axi_araddr_reg[4]_33\,
      \axi_araddr_reg[4]_32\ => \axi_araddr_reg[4]_34\,
      \axi_araddr_reg[4]_33\ => \axi_araddr_reg[4]_35\,
      \axi_araddr_reg[4]_34\ => \axi_araddr_reg[4]_36\,
      \axi_araddr_reg[4]_35\ => \axi_araddr_reg[4]_37\,
      \axi_araddr_reg[4]_36\ => \axi_araddr_reg[4]_38\,
      \axi_araddr_reg[4]_37\ => \axi_araddr_reg[4]_39\,
      \axi_araddr_reg[4]_38\ => \axi_araddr_reg[4]_40\,
      \axi_araddr_reg[4]_39\ => \axi_araddr_reg[4]_41\,
      \axi_araddr_reg[4]_4\ => \axi_araddr_reg[4]_6\,
      \axi_araddr_reg[4]_40\ => \axi_araddr_reg[4]_42\,
      \axi_araddr_reg[4]_41\ => \axi_araddr_reg[4]_43\,
      \axi_araddr_reg[4]_42\ => \axi_araddr_reg[4]_44\,
      \axi_araddr_reg[4]_5\ => \axi_araddr_reg[4]_7\,
      \axi_araddr_reg[4]_6\ => \axi_araddr_reg[4]_8\,
      \axi_araddr_reg[4]_7\ => \axi_araddr_reg[4]_9\,
      \axi_araddr_reg[4]_8\ => \axi_araddr_reg[4]_10\,
      \axi_araddr_reg[4]_9\ => \axi_araddr_reg[4]_11\,
      \axi_araddr_reg[6]\(2 downto 0) => \axi_araddr_reg[6]\(2 downto 0),
      \axi_rdata_reg[15]\(14 downto 2) => D(15 downto 3),
      \axi_rdata_reg[15]\(1 downto 0) => D(1 downto 0),
      \axi_rdata_reg[2]\ => audio_codec_n_71,
      \^clk\ => clk,
      flagQ(0) => flagQ(0),
      \int_trigger_volt_s_reg[9]\(4) => \int_trigger_volt_s_reg__0\(9),
      \int_trigger_volt_s_reg[9]\(3 downto 2) => \int_trigger_volt_s_reg__0\(7 downto 6),
      \int_trigger_volt_s_reg[9]\(1) => \int_trigger_volt_s_reg__0\(2),
      \int_trigger_volt_s_reg[9]\(0) => \int_trigger_volt_s_reg__0\(0),
      lopt => lopt,
      \processQ_reg[9]\(0) => \^q_reg[2]\(0),
      reset_n => reset_n,
      scl => scl,
      sda => sda,
      \sdp_bl.ramb18_dp_bl.ram18_bl\(9 downto 0) => DI(9 downto 0),
      \slv_reg10_reg[0]\(0) => \slv_reg10_reg[9]\(0),
      \slv_reg11_reg[1]\ => video_inst_n_39,
      \slv_reg11_reg[3]\ => video_inst_n_44,
      \slv_reg11_reg[4]\ => video_inst_n_45,
      \slv_reg11_reg[5]\ => video_inst_n_43,
      \slv_reg11_reg[6]\ => video_inst_n_46,
      \slv_reg11_reg[7]\ => video_inst_n_42,
      \slv_reg11_reg[8]\ => video_inst_n_41,
      \slv_reg11_reg[9]\(4) => \slv_reg11_reg[9]\(9),
      \slv_reg11_reg[9]\(3 downto 2) => \slv_reg11_reg[9]\(7 downto 6),
      \slv_reg11_reg[9]\(1) => \slv_reg11_reg[9]\(2),
      \slv_reg11_reg[9]\(0) => \slv_reg11_reg[9]\(0),
      \slv_reg15_reg[0]\ => \slv_reg15_reg[0]\,
      \slv_reg1_reg[15]\(9 downto 0) => \slv_reg1_reg[15]\(9 downto 0),
      \slv_reg2_reg[15]\(9 downto 0) => \slv_reg2_reg[15]\(9 downto 0),
      \slv_reg3_reg[0]\ => \slv_reg3_reg[0]\,
      \slv_reg3_reg[10]\ => \slv_reg3_reg[10]\,
      \slv_reg3_reg[11]\ => \slv_reg3_reg[11]\,
      \slv_reg3_reg[12]\ => \slv_reg3_reg[12]\,
      \slv_reg3_reg[13]\ => \slv_reg3_reg[13]\,
      \slv_reg3_reg[14]\ => \slv_reg3_reg[14]\,
      \slv_reg3_reg[15]\ => \slv_reg3_reg[15]\,
      \slv_reg3_reg[1]\ => \slv_reg3_reg[1]\,
      \slv_reg3_reg[2]\ => \slv_reg3_reg[2]\,
      \slv_reg3_reg[3]\ => \slv_reg3_reg[3]\,
      \slv_reg3_reg[4]\ => \slv_reg3_reg[4]\,
      \slv_reg3_reg[5]\ => \slv_reg3_reg[5]\,
      \slv_reg3_reg[6]\ => \slv_reg3_reg[6]\,
      \slv_reg3_reg[7]\ => \slv_reg3_reg[7]\,
      \slv_reg3_reg[8]\ => \slv_reg3_reg[8]\,
      \slv_reg3_reg[9]\ => \slv_reg3_reg[9]\,
      \slv_reg4_reg[15]\(15 downto 0) => \slv_reg4_reg[15]\(15 downto 0),
      \slv_reg5_reg[15]\(15 downto 0) => Q(15 downto 0),
      state(1 downto 0) => state(1 downto 0),
      \state_reg[0]\ => \state_reg[0]\,
      switch(0) => switch(3)
    );
\clock_divider[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \clock_divider_reg_n_0_[0]\,
      O => \clock_divider[0]_i_2_n_0\
    );
\clock_divider_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[0]_i_1_n_7\,
      Q => \clock_divider_reg_n_0_[0]\,
      R => '0'
    );
\clock_divider_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \clock_divider_reg[0]_i_1_n_0\,
      CO(2) => \clock_divider_reg[0]_i_1_n_1\,
      CO(1) => \clock_divider_reg[0]_i_1_n_2\,
      CO(0) => \clock_divider_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \clock_divider_reg[0]_i_1_n_4\,
      O(2) => \clock_divider_reg[0]_i_1_n_5\,
      O(1) => \clock_divider_reg[0]_i_1_n_6\,
      O(0) => \clock_divider_reg[0]_i_1_n_7\,
      S(3) => \clock_divider_reg_n_0_[3]\,
      S(2) => \clock_divider_reg_n_0_[2]\,
      S(1) => \clock_divider_reg_n_0_[1]\,
      S(0) => \clock_divider[0]_i_2_n_0\
    );
\clock_divider_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[8]_i_1_n_5\,
      Q => \clock_divider_reg_n_0_[10]\,
      R => '0'
    );
\clock_divider_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[8]_i_1_n_4\,
      Q => \clock_divider_reg_n_0_[11]\,
      R => '0'
    );
\clock_divider_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[12]_i_1_n_7\,
      Q => \clock_divider_reg_n_0_[12]\,
      R => '0'
    );
\clock_divider_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clock_divider_reg[8]_i_1_n_0\,
      CO(3) => \clock_divider_reg[12]_i_1_n_0\,
      CO(2) => \clock_divider_reg[12]_i_1_n_1\,
      CO(1) => \clock_divider_reg[12]_i_1_n_2\,
      CO(0) => \clock_divider_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clock_divider_reg[12]_i_1_n_4\,
      O(2) => \clock_divider_reg[12]_i_1_n_5\,
      O(1) => \clock_divider_reg[12]_i_1_n_6\,
      O(0) => \clock_divider_reg[12]_i_1_n_7\,
      S(3) => \clock_divider_reg_n_0_[15]\,
      S(2) => \clock_divider_reg_n_0_[14]\,
      S(1) => \clock_divider_reg_n_0_[13]\,
      S(0) => \clock_divider_reg_n_0_[12]\
    );
\clock_divider_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[12]_i_1_n_6\,
      Q => \clock_divider_reg_n_0_[13]\,
      R => '0'
    );
\clock_divider_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[12]_i_1_n_5\,
      Q => \clock_divider_reg_n_0_[14]\,
      R => '0'
    );
\clock_divider_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[12]_i_1_n_4\,
      Q => \clock_divider_reg_n_0_[15]\,
      R => '0'
    );
\clock_divider_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[16]_i_1_n_7\,
      Q => \clock_divider_reg_n_0_[16]\,
      R => '0'
    );
\clock_divider_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clock_divider_reg[12]_i_1_n_0\,
      CO(3) => \clock_divider_reg[16]_i_1_n_0\,
      CO(2) => \clock_divider_reg[16]_i_1_n_1\,
      CO(1) => \clock_divider_reg[16]_i_1_n_2\,
      CO(0) => \clock_divider_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clock_divider_reg[16]_i_1_n_4\,
      O(2) => \clock_divider_reg[16]_i_1_n_5\,
      O(1) => \clock_divider_reg[16]_i_1_n_6\,
      O(0) => \clock_divider_reg[16]_i_1_n_7\,
      S(3) => \clock_divider_reg_n_0_[19]\,
      S(2) => \clock_divider_reg_n_0_[18]\,
      S(1) => \clock_divider_reg_n_0_[17]\,
      S(0) => \clock_divider_reg_n_0_[16]\
    );
\clock_divider_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[16]_i_1_n_6\,
      Q => \clock_divider_reg_n_0_[17]\,
      R => '0'
    );
\clock_divider_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[16]_i_1_n_5\,
      Q => \clock_divider_reg_n_0_[18]\,
      R => '0'
    );
\clock_divider_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[16]_i_1_n_4\,
      Q => \clock_divider_reg_n_0_[19]\,
      R => '0'
    );
\clock_divider_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[0]_i_1_n_6\,
      Q => \clock_divider_reg_n_0_[1]\,
      R => '0'
    );
\clock_divider_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[20]_i_1_n_7\,
      Q => \clock_divider_reg_n_0_[20]\,
      R => '0'
    );
\clock_divider_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clock_divider_reg[16]_i_1_n_0\,
      CO(3 downto 2) => \NLW_clock_divider_reg[20]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \clock_divider_reg[20]_i_1_n_2\,
      CO(0) => \clock_divider_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_clock_divider_reg[20]_i_1_O_UNCONNECTED\(3),
      O(2) => \clock_divider_reg[20]_i_1_n_5\,
      O(1) => \clock_divider_reg[20]_i_1_n_6\,
      O(0) => \clock_divider_reg[20]_i_1_n_7\,
      S(3) => '0',
      S(2 downto 1) => clock_divider_reg(22 downto 21),
      S(0) => \clock_divider_reg_n_0_[20]\
    );
\clock_divider_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[20]_i_1_n_6\,
      Q => clock_divider_reg(21),
      R => '0'
    );
\clock_divider_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[20]_i_1_n_5\,
      Q => clock_divider_reg(22),
      R => '0'
    );
\clock_divider_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[0]_i_1_n_5\,
      Q => \clock_divider_reg_n_0_[2]\,
      R => '0'
    );
\clock_divider_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[0]_i_1_n_4\,
      Q => \clock_divider_reg_n_0_[3]\,
      R => '0'
    );
\clock_divider_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[4]_i_1_n_7\,
      Q => \clock_divider_reg_n_0_[4]\,
      R => '0'
    );
\clock_divider_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clock_divider_reg[0]_i_1_n_0\,
      CO(3) => \clock_divider_reg[4]_i_1_n_0\,
      CO(2) => \clock_divider_reg[4]_i_1_n_1\,
      CO(1) => \clock_divider_reg[4]_i_1_n_2\,
      CO(0) => \clock_divider_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clock_divider_reg[4]_i_1_n_4\,
      O(2) => \clock_divider_reg[4]_i_1_n_5\,
      O(1) => \clock_divider_reg[4]_i_1_n_6\,
      O(0) => \clock_divider_reg[4]_i_1_n_7\,
      S(3) => \clock_divider_reg_n_0_[7]\,
      S(2) => \clock_divider_reg_n_0_[6]\,
      S(1) => \clock_divider_reg_n_0_[5]\,
      S(0) => \clock_divider_reg_n_0_[4]\
    );
\clock_divider_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[4]_i_1_n_6\,
      Q => \clock_divider_reg_n_0_[5]\,
      R => '0'
    );
\clock_divider_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[4]_i_1_n_5\,
      Q => \clock_divider_reg_n_0_[6]\,
      R => '0'
    );
\clock_divider_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[4]_i_1_n_4\,
      Q => \clock_divider_reg_n_0_[7]\,
      R => '0'
    );
\clock_divider_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[8]_i_1_n_7\,
      Q => \clock_divider_reg_n_0_[8]\,
      R => '0'
    );
\clock_divider_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clock_divider_reg[4]_i_1_n_0\,
      CO(3) => \clock_divider_reg[8]_i_1_n_0\,
      CO(2) => \clock_divider_reg[8]_i_1_n_1\,
      CO(1) => \clock_divider_reg[8]_i_1_n_2\,
      CO(0) => \clock_divider_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clock_divider_reg[8]_i_1_n_4\,
      O(2) => \clock_divider_reg[8]_i_1_n_5\,
      O(1) => \clock_divider_reg[8]_i_1_n_6\,
      O(0) => \clock_divider_reg[8]_i_1_n_7\,
      S(3) => \clock_divider_reg_n_0_[11]\,
      S(2) => \clock_divider_reg_n_0_[10]\,
      S(1) => \clock_divider_reg_n_0_[9]\,
      S(0) => \clock_divider_reg_n_0_[8]\
    );
\clock_divider_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \clock_divider_reg[8]_i_1_n_6\,
      Q => \clock_divider_reg_n_0_[9]\,
      R => '0'
    );
flagReg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_flagRegister
     port map (
      CLK => \^ready\,
      D(0) => D(2),
      \Q_reg[0]_0\(0) => flagQ(0),
      \axi_araddr_reg[2]_rep\ => \axi_araddr_reg[2]_rep\,
      \axi_araddr_reg[3]_rep\ => \axi_araddr_reg[3]_rep\,
      \axi_araddr_reg[4]\ => \axi_araddr_reg[4]_4\,
      \axi_araddr_reg[4]_0\ => \axi_araddr_reg[4]_5\,
      \axi_araddr_reg[4]_1\ => audio_codec_n_71,
      \axi_araddr_reg[6]\(2 downto 0) => \axi_araddr_reg[6]\(2 downto 0),
      \^clk\ => clk,
      \processQ_reg[9]\(0) => \^q_reg[2]\(0),
      reset_n => \^sr\(0),
      \slv_reg10_reg[2]\(0) => \slv_reg10_reg[9]\(2),
      \slv_reg11_reg[2]\(0) => \slv_reg11_reg[9]\(2),
      \slv_reg15_reg[2]\ => \slv_reg15_reg[2]\,
      \slv_reg3_reg[2]\(1 downto 0) => \slv_reg3_reg[2]_0\(1 downto 0)
    );
\int_trigger_time_s[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(0),
      O => \int_trigger_time_s[0]_i_1_n_0\
    );
\int_trigger_time_s[4]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(1),
      O => \int_trigger_time_s[4]_i_2_n_0\
    );
\int_trigger_time_s[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(3),
      I1 => \int_trigger_time_s_reg__0\(4),
      O => \int_trigger_time_s[4]_i_3_n_0\
    );
\int_trigger_time_s[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(2),
      I1 => \int_trigger_time_s_reg__0\(3),
      O => \int_trigger_time_s[4]_i_4_n_0\
    );
\int_trigger_time_s[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(1),
      I1 => \int_trigger_time_s_reg__0\(2),
      O => \int_trigger_time_s[4]_i_5_n_0\
    );
\int_trigger_time_s[4]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(1),
      I1 => \int_trigger_time_s[9]_i_5_n_0\,
      O => \int_trigger_time_s[4]_i_6_n_0\
    );
\int_trigger_time_s[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(7),
      I1 => \int_trigger_time_s_reg__0\(8),
      O => \int_trigger_time_s[8]_i_2_n_0\
    );
\int_trigger_time_s[8]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(6),
      I1 => \int_trigger_time_s_reg__0\(7),
      O => \int_trigger_time_s[8]_i_3_n_0\
    );
\int_trigger_time_s[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(5),
      I1 => \int_trigger_time_s_reg__0\(6),
      O => \int_trigger_time_s[8]_i_4_n_0\
    );
\int_trigger_time_s[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(4),
      I1 => \int_trigger_time_s_reg__0\(5),
      O => \int_trigger_time_s[8]_i_5_n_0\
    );
\int_trigger_time_s[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"04550000FFFFFFFF"
    )
        port map (
      I0 => \int_trigger_time_s[9]_i_5_n_0\,
      I1 => \int_trigger_time_s_reg__0\(9),
      I2 => \int_trigger_time_s[9]_i_6_n_0\,
      I3 => btn(3),
      I4 => btn(4),
      I5 => reset_n,
      O => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s[9]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBAA"
    )
        port map (
      I0 => \int_trigger_time_s[9]_i_5_n_0\,
      I1 => \int_trigger_time_s_reg__0\(9),
      I2 => \int_trigger_time_s[9]_i_6_n_0\,
      I3 => btn(3),
      O => \int_trigger_time_s[9]_i_2_n_0\
    );
\int_trigger_time_s[9]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => clock_divider_reg(21),
      I1 => switch(2),
      I2 => clock_divider_reg(22),
      O => trigger_clock
    );
\int_trigger_time_s[9]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA8"
    )
        port map (
      I0 => btn(1),
      I1 => \int_trigger_time_s[9]_i_8_n_0\,
      I2 => video_inst_n_32,
      I3 => video_inst_n_29,
      I4 => video_inst_n_31,
      I5 => video_inst_n_34,
      O => \int_trigger_time_s[9]_i_5_n_0\
    );
\int_trigger_time_s[9]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000005777"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(6),
      I1 => \int_trigger_time_s_reg__0\(5),
      I2 => \int_trigger_time_s_reg__0\(4),
      I3 => \int_trigger_time_s_reg__0\(3),
      I4 => \int_trigger_time_s_reg__0\(8),
      I5 => \int_trigger_time_s_reg__0\(7),
      O => \int_trigger_time_s[9]_i_6_n_0\
    );
\int_trigger_time_s[9]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_time_s_reg__0\(8),
      I1 => \int_trigger_time_s_reg__0\(9),
      O => \int_trigger_time_s[9]_i_7_n_0\
    );
\int_trigger_time_s[9]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => video_inst_n_35,
      I1 => video_inst_n_36,
      I2 => video_inst_n_8,
      I3 => video_inst_n_37,
      I4 => video_inst_n_30,
      I5 => video_inst_n_33,
      O => \int_trigger_time_s[9]_i_8_n_0\
    );
\int_trigger_time_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_time_s[9]_i_2_n_0\,
      D => \int_trigger_time_s[0]_i_1_n_0\,
      Q => \int_trigger_time_s_reg__0\(0),
      R => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_time_s[9]_i_2_n_0\,
      D => \int_trigger_time_s_reg[4]_i_1_n_7\,
      Q => \int_trigger_time_s_reg__0\(1),
      R => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => trigger_clock,
      CE => \int_trigger_time_s[9]_i_2_n_0\,
      D => \int_trigger_time_s_reg[4]_i_1_n_6\,
      Q => \int_trigger_time_s_reg__0\(2),
      S => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => trigger_clock,
      CE => \int_trigger_time_s[9]_i_2_n_0\,
      D => \int_trigger_time_s_reg[4]_i_1_n_5\,
      Q => \int_trigger_time_s_reg__0\(3),
      S => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_time_s[9]_i_2_n_0\,
      D => \int_trigger_time_s_reg[4]_i_1_n_4\,
      Q => \int_trigger_time_s_reg__0\(4),
      R => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \int_trigger_time_s_reg[4]_i_1_n_0\,
      CO(2) => \int_trigger_time_s_reg[4]_i_1_n_1\,
      CO(1) => \int_trigger_time_s_reg[4]_i_1_n_2\,
      CO(0) => \int_trigger_time_s_reg[4]_i_1_n_3\,
      CYINIT => \int_trigger_time_s_reg__0\(0),
      DI(3 downto 1) => \int_trigger_time_s_reg__0\(3 downto 1),
      DI(0) => \int_trigger_time_s[4]_i_2_n_0\,
      O(3) => \int_trigger_time_s_reg[4]_i_1_n_4\,
      O(2) => \int_trigger_time_s_reg[4]_i_1_n_5\,
      O(1) => \int_trigger_time_s_reg[4]_i_1_n_6\,
      O(0) => \int_trigger_time_s_reg[4]_i_1_n_7\,
      S(3) => \int_trigger_time_s[4]_i_3_n_0\,
      S(2) => \int_trigger_time_s[4]_i_4_n_0\,
      S(1) => \int_trigger_time_s[4]_i_5_n_0\,
      S(0) => \int_trigger_time_s[4]_i_6_n_0\
    );
\int_trigger_time_s_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => trigger_clock,
      CE => \int_trigger_time_s[9]_i_2_n_0\,
      D => \int_trigger_time_s_reg[8]_i_1_n_7\,
      Q => \int_trigger_time_s_reg__0\(5),
      S => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_time_s[9]_i_2_n_0\,
      D => \int_trigger_time_s_reg[8]_i_1_n_6\,
      Q => \int_trigger_time_s_reg__0\(6),
      R => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_time_s[9]_i_2_n_0\,
      D => \int_trigger_time_s_reg[8]_i_1_n_5\,
      Q => \int_trigger_time_s_reg__0\(7),
      R => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => trigger_clock,
      CE => \int_trigger_time_s[9]_i_2_n_0\,
      D => \int_trigger_time_s_reg[8]_i_1_n_4\,
      Q => \int_trigger_time_s_reg__0\(8),
      S => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_trigger_time_s_reg[4]_i_1_n_0\,
      CO(3) => \int_trigger_time_s_reg[8]_i_1_n_0\,
      CO(2) => \int_trigger_time_s_reg[8]_i_1_n_1\,
      CO(1) => \int_trigger_time_s_reg[8]_i_1_n_2\,
      CO(0) => \int_trigger_time_s_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \int_trigger_time_s_reg__0\(7 downto 4),
      O(3) => \int_trigger_time_s_reg[8]_i_1_n_4\,
      O(2) => \int_trigger_time_s_reg[8]_i_1_n_5\,
      O(1) => \int_trigger_time_s_reg[8]_i_1_n_6\,
      O(0) => \int_trigger_time_s_reg[8]_i_1_n_7\,
      S(3) => \int_trigger_time_s[8]_i_2_n_0\,
      S(2) => \int_trigger_time_s[8]_i_3_n_0\,
      S(1) => \int_trigger_time_s[8]_i_4_n_0\,
      S(0) => \int_trigger_time_s[8]_i_5_n_0\
    );
\int_trigger_time_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_time_s[9]_i_2_n_0\,
      D => \int_trigger_time_s_reg[9]_i_3_n_7\,
      Q => \int_trigger_time_s_reg__0\(9),
      R => \int_trigger_time_s[9]_i_1_n_0\
    );
\int_trigger_time_s_reg[9]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_trigger_time_s_reg[8]_i_1_n_0\,
      CO(3 downto 0) => \NLW_int_trigger_time_s_reg[9]_i_3_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_int_trigger_time_s_reg[9]_i_3_O_UNCONNECTED\(3 downto 1),
      O(0) => \int_trigger_time_s_reg[9]_i_3_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \int_trigger_time_s[9]_i_7_n_0\
    );
\int_trigger_volt_s[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(0),
      O => \int_trigger_volt_s[0]_i_1_n_0\
    );
\int_trigger_volt_s[4]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(1),
      O => \int_trigger_volt_s[4]_i_2_n_0\
    );
\int_trigger_volt_s[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(3),
      I1 => \int_trigger_volt_s_reg__0\(4),
      O => \int_trigger_volt_s[4]_i_3_n_0\
    );
\int_trigger_volt_s[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(2),
      I1 => \int_trigger_volt_s_reg__0\(3),
      O => \int_trigger_volt_s[4]_i_4_n_0\
    );
\int_trigger_volt_s[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(1),
      I1 => \int_trigger_volt_s_reg__0\(2),
      O => \int_trigger_volt_s[4]_i_5_n_0\
    );
\int_trigger_volt_s[4]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(1),
      I1 => \int_trigger_volt_s[9]_i_4_n_0\,
      O => \int_trigger_volt_s[4]_i_6_n_0\
    );
\int_trigger_volt_s[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(7),
      I1 => \int_trigger_volt_s_reg__0\(8),
      O => \int_trigger_volt_s[8]_i_2_n_0\
    );
\int_trigger_volt_s[8]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(6),
      I1 => \int_trigger_volt_s_reg__0\(7),
      O => \int_trigger_volt_s[8]_i_3_n_0\
    );
\int_trigger_volt_s[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(5),
      I1 => \int_trigger_volt_s_reg__0\(6),
      O => \int_trigger_volt_s[8]_i_4_n_0\
    );
\int_trigger_volt_s[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(4),
      I1 => \int_trigger_volt_s_reg__0\(5),
      O => \int_trigger_volt_s[8]_i_5_n_0\
    );
\int_trigger_volt_s[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \int_trigger_volt_s[9]_i_2_n_0\,
      I1 => btn(4),
      I2 => reset_n,
      O => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAABABABABABABA"
    )
        port map (
      I0 => \int_trigger_volt_s[9]_i_4_n_0\,
      I1 => \int_trigger_volt_s_reg__0\(9),
      I2 => btn(2),
      I3 => \int_trigger_volt_s[9]_i_5_n_0\,
      I4 => \int_trigger_volt_s_reg__0\(8),
      I5 => \int_trigger_volt_s_reg__0\(7),
      O => \int_trigger_volt_s[9]_i_2_n_0\
    );
\int_trigger_volt_s[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA8AAAAAAAA"
    )
        port map (
      I0 => btn(0),
      I1 => \int_trigger_volt_s[9]_i_7_n_0\,
      I2 => \int_trigger_volt_s_reg__0\(1),
      I3 => \int_trigger_volt_s_reg__0\(3),
      I4 => \int_trigger_volt_s_reg__0\(2),
      I5 => \int_trigger_volt_s[9]_i_5_n_0\,
      O => \int_trigger_volt_s[9]_i_4_n_0\
    );
\int_trigger_volt_s[9]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(6),
      I1 => \int_trigger_volt_s_reg__0\(5),
      I2 => \int_trigger_volt_s_reg__0\(4),
      O => \int_trigger_volt_s[9]_i_5_n_0\
    );
\int_trigger_volt_s[9]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(8),
      I1 => \int_trigger_volt_s_reg__0\(9),
      O => \int_trigger_volt_s[9]_i_6_n_0\
    );
\int_trigger_volt_s[9]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \int_trigger_volt_s_reg__0\(9),
      I1 => \int_trigger_volt_s_reg__0\(8),
      I2 => \int_trigger_volt_s_reg__0\(0),
      I3 => \int_trigger_volt_s_reg__0\(7),
      O => \int_trigger_volt_s[9]_i_7_n_0\
    );
\int_trigger_volt_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_volt_s[9]_i_2_n_0\,
      D => \int_trigger_volt_s[0]_i_1_n_0\,
      Q => \int_trigger_volt_s_reg__0\(0),
      R => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_volt_s[9]_i_2_n_0\,
      D => \int_trigger_volt_s_reg[4]_i_1_n_7\,
      Q => \int_trigger_volt_s_reg__0\(1),
      R => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_volt_s[9]_i_2_n_0\,
      D => \int_trigger_volt_s_reg[4]_i_1_n_6\,
      Q => \int_trigger_volt_s_reg__0\(2),
      R => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => trigger_clock,
      CE => \int_trigger_volt_s[9]_i_2_n_0\,
      D => \int_trigger_volt_s_reg[4]_i_1_n_5\,
      Q => \int_trigger_volt_s_reg__0\(3),
      S => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_volt_s[9]_i_2_n_0\,
      D => \int_trigger_volt_s_reg[4]_i_1_n_4\,
      Q => \int_trigger_volt_s_reg__0\(4),
      R => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \int_trigger_volt_s_reg[4]_i_1_n_0\,
      CO(2) => \int_trigger_volt_s_reg[4]_i_1_n_1\,
      CO(1) => \int_trigger_volt_s_reg[4]_i_1_n_2\,
      CO(0) => \int_trigger_volt_s_reg[4]_i_1_n_3\,
      CYINIT => \int_trigger_volt_s_reg__0\(0),
      DI(3 downto 1) => \int_trigger_volt_s_reg__0\(3 downto 1),
      DI(0) => \int_trigger_volt_s[4]_i_2_n_0\,
      O(3) => \int_trigger_volt_s_reg[4]_i_1_n_4\,
      O(2) => \int_trigger_volt_s_reg[4]_i_1_n_5\,
      O(1) => \int_trigger_volt_s_reg[4]_i_1_n_6\,
      O(0) => \int_trigger_volt_s_reg[4]_i_1_n_7\,
      S(3) => \int_trigger_volt_s[4]_i_3_n_0\,
      S(2) => \int_trigger_volt_s[4]_i_4_n_0\,
      S(1) => \int_trigger_volt_s[4]_i_5_n_0\,
      S(0) => \int_trigger_volt_s[4]_i_6_n_0\
    );
\int_trigger_volt_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_volt_s[9]_i_2_n_0\,
      D => \int_trigger_volt_s_reg[8]_i_1_n_7\,
      Q => \int_trigger_volt_s_reg__0\(5),
      R => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => trigger_clock,
      CE => \int_trigger_volt_s[9]_i_2_n_0\,
      D => \int_trigger_volt_s_reg[8]_i_1_n_6\,
      Q => \int_trigger_volt_s_reg__0\(6),
      S => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => trigger_clock,
      CE => \int_trigger_volt_s[9]_i_2_n_0\,
      D => \int_trigger_volt_s_reg[8]_i_1_n_5\,
      Q => \int_trigger_volt_s_reg__0\(7),
      S => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_volt_s[9]_i_2_n_0\,
      D => \int_trigger_volt_s_reg[8]_i_1_n_4\,
      Q => \int_trigger_volt_s_reg__0\(8),
      R => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_trigger_volt_s_reg[4]_i_1_n_0\,
      CO(3) => \int_trigger_volt_s_reg[8]_i_1_n_0\,
      CO(2) => \int_trigger_volt_s_reg[8]_i_1_n_1\,
      CO(1) => \int_trigger_volt_s_reg[8]_i_1_n_2\,
      CO(0) => \int_trigger_volt_s_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \int_trigger_volt_s_reg__0\(7 downto 4),
      O(3) => \int_trigger_volt_s_reg[8]_i_1_n_4\,
      O(2) => \int_trigger_volt_s_reg[8]_i_1_n_5\,
      O(1) => \int_trigger_volt_s_reg[8]_i_1_n_6\,
      O(0) => \int_trigger_volt_s_reg[8]_i_1_n_7\,
      S(3) => \int_trigger_volt_s[8]_i_2_n_0\,
      S(2) => \int_trigger_volt_s[8]_i_3_n_0\,
      S(1) => \int_trigger_volt_s[8]_i_4_n_0\,
      S(0) => \int_trigger_volt_s[8]_i_5_n_0\
    );
\int_trigger_volt_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => trigger_clock,
      CE => \int_trigger_volt_s[9]_i_2_n_0\,
      D => \int_trigger_volt_s_reg[9]_i_3_n_7\,
      Q => \int_trigger_volt_s_reg__0\(9),
      R => \int_trigger_volt_s[9]_i_1_n_0\
    );
\int_trigger_volt_s_reg[9]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_trigger_volt_s_reg[8]_i_1_n_0\,
      CO(3 downto 0) => \NLW_int_trigger_volt_s_reg[9]_i_3_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_int_trigger_volt_s_reg[9]_i_3_O_UNCONNECTED\(3 downto 1),
      O(0) => \int_trigger_volt_s_reg[9]_i_3_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \int_trigger_volt_s[9]_i_6_n_0\
    );
lBRAM: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO
     port map (
      ADDRARDADDR(9) => video_inst_n_9,
      ADDRARDADDR(8) => video_inst_n_10,
      ADDRARDADDR(7) => video_inst_n_11,
      ADDRARDADDR(6) => video_inst_n_12,
      ADDRARDADDR(5) => video_inst_n_13,
      ADDRARDADDR(4) => video_inst_n_14,
      ADDRARDADDR(3) => video_inst_n_15,
      ADDRARDADDR(2) => video_inst_n_16,
      ADDRARDADDR(1 downto 0) => column(1 downto 0),
      ADDRBWRADDR(9 downto 0) => WRADDR(9 downto 0),
      CO(0) => ch1,
      DOADO(2 downto 0) => L_out_bram(2 downto 0),
      Q(6 downto 0) => row(9 downto 3),
      S(0) => video_inst_n_40,
      WREN => WREN,
      clk => clk,
      \encoded_reg[8]\ => lBRAM_n_3,
      \processQ_reg[8]\ => video_inst_n_38,
      reset_n => \^sr\(0),
      \slv_reg1_reg[15]\(9 downto 0) => DI(9 downto 0),
      switch(0) => switch(0)
    );
rBRAM: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0
     port map (
      ADDRARDADDR(9) => video_inst_n_9,
      ADDRARDADDR(8) => video_inst_n_10,
      ADDRARDADDR(7) => video_inst_n_11,
      ADDRARDADDR(6) => video_inst_n_12,
      ADDRARDADDR(5) => video_inst_n_13,
      ADDRARDADDR(4) => video_inst_n_14,
      ADDRARDADDR(3) => video_inst_n_15,
      ADDRARDADDR(2) => video_inst_n_16,
      ADDRARDADDR(1 downto 0) => column(1 downto 0),
      ADDRBWRADDR(9 downto 0) => WRADDR(9 downto 0),
      CO(0) => ch2,
      DIBDI(9) => audio_codec_n_72,
      DIBDI(8) => audio_codec_n_73,
      DIBDI(7) => audio_codec_n_74,
      DIBDI(6) => audio_codec_n_75,
      DIBDI(5) => audio_codec_n_76,
      DIBDI(4) => audio_codec_n_77,
      DIBDI(3) => audio_codec_n_78,
      DIBDI(2) => audio_codec_n_79,
      DIBDI(1) => audio_codec_n_80,
      DIBDI(0) => audio_codec_n_81,
      Q(9 downto 0) => row(9 downto 0),
      WREN => WREN,
      clk => clk,
      \encoded_reg[8]\ => rBRAM_n_0,
      reset_n => \^sr\(0),
      switch(0) => switch(1)
    );
sampCounter: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample_counter
     port map (
      ADDRBWRADDR(9 downto 0) => WRADDR(9 downto 0),
      E(0) => E(0),
      Q(0) => Q(0),
      \Q_reg[2]\(0) => \^q_reg[2]\(0),
      clk => clk,
      \slv_reg0_reg[9]\(9 downto 0) => \slv_reg0_reg[9]\(9 downto 0),
      \state_reg[1]\(0) => \state_reg[1]\(0)
    );
\state[0]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54045404FD5D5404"
    )
        port map (
      I0 => R_unsigned_data_prev(9),
      I1 => \int_trigger_volt_s_reg__0\(9),
      I2 => Q(0),
      I3 => \slv_reg11_reg[9]\(9),
      I4 => video_inst_n_41,
      I5 => R_unsigned_data_prev(8),
      O => \state[0]_i_11_n_0\
    );
\state[0]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B84700000000B847"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(9),
      I1 => Q(0),
      I2 => \int_trigger_volt_s_reg__0\(9),
      I3 => R_unsigned_data_prev(9),
      I4 => video_inst_n_41,
      I5 => R_unsigned_data_prev(8),
      O => \state[0]_i_12_n_0\
    );
\state[0]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54045404FD5D5404"
    )
        port map (
      I0 => L_unsigned_data_prev(9),
      I1 => \int_trigger_volt_s_reg__0\(9),
      I2 => Q(0),
      I3 => \slv_reg11_reg[9]\(9),
      I4 => video_inst_n_41,
      I5 => L_unsigned_data_prev(8),
      O => \state[0]_i_17_n_0\
    );
\state[0]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B84700000000B847"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(9),
      I1 => Q(0),
      I2 => \int_trigger_volt_s_reg__0\(9),
      I3 => L_unsigned_data_prev(9),
      I4 => video_inst_n_41,
      I5 => L_unsigned_data_prev(8),
      O => \state[0]_i_18_n_0\
    );
\state[0]_i_27\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44444444DDD444D4"
    )
        port map (
      I0 => R_unsigned_data_prev(7),
      I1 => video_inst_n_42,
      I2 => \int_trigger_volt_s_reg__0\(6),
      I3 => Q(0),
      I4 => \slv_reg11_reg[9]\(6),
      I5 => R_unsigned_data_prev(6),
      O => \state[0]_i_27_n_0\
    );
\state[0]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44444444DDD444D4"
    )
        port map (
      I0 => R_unsigned_data_prev(5),
      I1 => video_inst_n_43,
      I2 => \int_trigger_volt_s_reg__0\(4),
      I3 => Q(0),
      I4 => \slv_reg11_reg[9]\(4),
      I5 => R_unsigned_data_prev(4),
      O => \state[0]_i_28_n_0\
    );
\state[0]_i_29\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44444444DDD444D4"
    )
        port map (
      I0 => R_unsigned_data_prev(3),
      I1 => video_inst_n_44,
      I2 => \int_trigger_volt_s_reg__0\(2),
      I3 => Q(0),
      I4 => \slv_reg11_reg[9]\(2),
      I5 => R_unsigned_data_prev(2),
      O => \state[0]_i_29_n_0\
    );
\state[0]_i_30\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44444444DDD444D4"
    )
        port map (
      I0 => R_unsigned_data_prev(1),
      I1 => video_inst_n_39,
      I2 => \int_trigger_volt_s_reg__0\(0),
      I3 => Q(0),
      I4 => \slv_reg11_reg[9]\(0),
      I5 => R_unsigned_data_prev(0),
      O => \state[0]_i_30_n_0\
    );
\state[0]_i_31\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B84700000000B847"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(7),
      I1 => Q(0),
      I2 => \int_trigger_volt_s_reg__0\(7),
      I3 => R_unsigned_data_prev(7),
      I4 => video_inst_n_46,
      I5 => R_unsigned_data_prev(6),
      O => \state[0]_i_31_n_0\
    );
\state[0]_i_32\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B84700000000B847"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(5),
      I1 => Q(0),
      I2 => \int_trigger_volt_s_reg__0\(5),
      I3 => R_unsigned_data_prev(5),
      I4 => video_inst_n_45,
      I5 => R_unsigned_data_prev(4),
      O => \state[0]_i_32_n_0\
    );
\state[0]_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9099900009000999"
    )
        port map (
      I0 => video_inst_n_44,
      I1 => R_unsigned_data_prev(3),
      I2 => \slv_reg11_reg[9]\(2),
      I3 => Q(0),
      I4 => \int_trigger_volt_s_reg__0\(2),
      I5 => R_unsigned_data_prev(2),
      O => \state[0]_i_33_n_0\
    );
\state[0]_i_34\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9099900009000999"
    )
        port map (
      I0 => video_inst_n_39,
      I1 => R_unsigned_data_prev(1),
      I2 => \slv_reg11_reg[9]\(0),
      I3 => Q(0),
      I4 => \int_trigger_volt_s_reg__0\(0),
      I5 => R_unsigned_data_prev(0),
      O => \state[0]_i_34_n_0\
    );
\state[0]_i_43\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44444444DDD444D4"
    )
        port map (
      I0 => L_unsigned_data_prev(7),
      I1 => video_inst_n_42,
      I2 => \int_trigger_volt_s_reg__0\(6),
      I3 => Q(0),
      I4 => \slv_reg11_reg[9]\(6),
      I5 => L_unsigned_data_prev(6),
      O => \state[0]_i_43_n_0\
    );
\state[0]_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44444444DDD444D4"
    )
        port map (
      I0 => L_unsigned_data_prev(5),
      I1 => video_inst_n_43,
      I2 => \int_trigger_volt_s_reg__0\(4),
      I3 => Q(0),
      I4 => \slv_reg11_reg[9]\(4),
      I5 => L_unsigned_data_prev(4),
      O => \state[0]_i_44_n_0\
    );
\state[0]_i_45\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44444444DDD444D4"
    )
        port map (
      I0 => L_unsigned_data_prev(3),
      I1 => video_inst_n_44,
      I2 => \int_trigger_volt_s_reg__0\(2),
      I3 => Q(0),
      I4 => \slv_reg11_reg[9]\(2),
      I5 => L_unsigned_data_prev(2),
      O => \state[0]_i_45_n_0\
    );
\state[0]_i_46\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44444444DDD444D4"
    )
        port map (
      I0 => L_unsigned_data_prev(1),
      I1 => video_inst_n_39,
      I2 => \int_trigger_volt_s_reg__0\(0),
      I3 => Q(0),
      I4 => \slv_reg11_reg[9]\(0),
      I5 => L_unsigned_data_prev(0),
      O => \state[0]_i_46_n_0\
    );
\state[0]_i_47\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B84700000000B847"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(7),
      I1 => Q(0),
      I2 => \int_trigger_volt_s_reg__0\(7),
      I3 => L_unsigned_data_prev(7),
      I4 => video_inst_n_46,
      I5 => L_unsigned_data_prev(6),
      O => \state[0]_i_47_n_0\
    );
\state[0]_i_48\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B84700000000B847"
    )
        port map (
      I0 => \slv_reg11_reg[9]\(5),
      I1 => Q(0),
      I2 => \int_trigger_volt_s_reg__0\(5),
      I3 => L_unsigned_data_prev(5),
      I4 => video_inst_n_45,
      I5 => L_unsigned_data_prev(4),
      O => \state[0]_i_48_n_0\
    );
\state[0]_i_49\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9099900009000999"
    )
        port map (
      I0 => video_inst_n_44,
      I1 => L_unsigned_data_prev(3),
      I2 => \slv_reg11_reg[9]\(2),
      I3 => Q(0),
      I4 => \int_trigger_volt_s_reg__0\(2),
      I5 => L_unsigned_data_prev(2),
      O => \state[0]_i_49_n_0\
    );
\state[0]_i_50\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9099900009000999"
    )
        port map (
      I0 => video_inst_n_39,
      I1 => L_unsigned_data_prev(1),
      I2 => \slv_reg11_reg[9]\(0),
      I3 => Q(0),
      I4 => \int_trigger_volt_s_reg__0\(0),
      I5 => L_unsigned_data_prev(0),
      O => \state[0]_i_50_n_0\
    );
\state_reg[0]_i_10\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \state_reg[0]_i_10_n_0\,
      CO(2) => \state_reg[0]_i_10_n_1\,
      CO(1) => \state_reg[0]_i_10_n_2\,
      CO(0) => \state_reg[0]_i_10_n_3\,
      CYINIT => '1',
      DI(3) => \state[0]_i_27_n_0\,
      DI(2) => \state[0]_i_28_n_0\,
      DI(1) => \state[0]_i_29_n_0\,
      DI(0) => \state[0]_i_30_n_0\,
      O(3 downto 0) => \NLW_state_reg[0]_i_10_O_UNCONNECTED\(3 downto 0),
      S(3) => \state[0]_i_31_n_0\,
      S(2) => \state[0]_i_32_n_0\,
      S(1) => \state[0]_i_33_n_0\,
      S(0) => \state[0]_i_34_n_0\
    );
\state_reg[0]_i_16\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \state_reg[0]_i_16_n_0\,
      CO(2) => \state_reg[0]_i_16_n_1\,
      CO(1) => \state_reg[0]_i_16_n_2\,
      CO(0) => \state_reg[0]_i_16_n_3\,
      CYINIT => '1',
      DI(3) => \state[0]_i_43_n_0\,
      DI(2) => \state[0]_i_44_n_0\,
      DI(1) => \state[0]_i_45_n_0\,
      DI(0) => \state[0]_i_46_n_0\,
      O(3 downto 0) => \NLW_state_reg[0]_i_16_O_UNCONNECTED\(3 downto 0),
      S(3) => \state[0]_i_47_n_0\,
      S(2) => \state[0]_i_48_n_0\,
      S(1) => \state[0]_i_49_n_0\,
      S(0) => \state[0]_i_50_n_0\
    );
\state_reg[0]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => \state_reg[0]_i_10_n_0\,
      CO(3 downto 1) => \NLW_state_reg[0]_i_4_CO_UNCONNECTED\(3 downto 1),
      CO(0) => R_bus_l,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \state[0]_i_11_n_0\,
      O(3 downto 0) => \NLW_state_reg[0]_i_4_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \state[0]_i_12_n_0\
    );
\state_reg[0]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => \state_reg[0]_i_16_n_0\,
      CO(3 downto 1) => \NLW_state_reg[0]_i_6_CO_UNCONNECTED\(3 downto 1),
      CO(0) => L_bus_l,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \state[0]_i_17_n_0\,
      O(3 downto 0) => \NLW_state_reg[0]_i_6_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \state[0]_i_18_n_0\
    );
video_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video
     port map (
      ADDRARDADDR(9) => video_inst_n_9,
      ADDRARDADDR(8) => video_inst_n_10,
      ADDRARDADDR(7) => video_inst_n_11,
      ADDRARDADDR(6) => video_inst_n_12,
      ADDRARDADDR(5) => video_inst_n_13,
      ADDRARDADDR(4) => video_inst_n_14,
      ADDRARDADDR(3) => video_inst_n_15,
      ADDRARDADDR(2) => video_inst_n_16,
      ADDRARDADDR(1 downto 0) => column(1 downto 0),
      CO(0) => ch1,
      DOADO(2 downto 0) => L_out_bram(2 downto 0),
      Q(9 downto 0) => \int_trigger_time_s_reg__0\(9 downto 0),
      S(0) => video_inst_n_40,
      clk => clk,
      \encoded_reg[8]\ => video_inst_n_38,
      \encoded_reg[8]_0\ => video_inst_n_42,
      \int_trigger_time_s_reg[0]\ => video_inst_n_8,
      \int_trigger_time_s_reg[0]_0\ => video_inst_n_29,
      \int_trigger_time_s_reg[0]_1\ => video_inst_n_30,
      \int_trigger_time_s_reg[0]_2\ => video_inst_n_31,
      \int_trigger_time_s_reg[0]_3\ => video_inst_n_32,
      \int_trigger_time_s_reg[0]_4\ => video_inst_n_33,
      \int_trigger_time_s_reg[0]_5\ => video_inst_n_34,
      \int_trigger_time_s_reg[0]_6\ => video_inst_n_35,
      \int_trigger_time_s_reg[0]_7\ => video_inst_n_36,
      \int_trigger_time_s_reg[0]_8\ => video_inst_n_37,
      \int_trigger_volt_s_reg[9]\(9 downto 0) => \int_trigger_volt_s_reg__0\(9 downto 0),
      lopt => lopt,
      \processQ_reg[8]\ => lBRAM_n_3,
      \processQ_reg[9]\(9 downto 0) => row(9 downto 0),
      \processQ_reg[9]_0\ => rBRAM_n_0,
      \processQ_reg[9]_1\(0) => ch2,
      reset_n => reset_n,
      \slv_reg10_reg[9]\(9 downto 0) => \slv_reg10_reg[9]\(9 downto 0),
      \slv_reg11_reg[9]\(9 downto 0) => \slv_reg11_reg[9]\(9 downto 0),
      \slv_reg5_reg[0]\(0) => Q(0),
      \state_reg[0]\ => video_inst_n_39,
      \state_reg[0]_0\ => video_inst_n_41,
      \state_reg[0]_1\ => video_inst_n_43,
      \state_reg[0]_2\ => video_inst_n_44,
      \state_reg[0]_3\ => video_inst_n_45,
      \state_reg[0]_4\ => video_inst_n_46,
      switch(1 downto 0) => switch(1 downto 0),
      tmds(3 downto 0) => tmds(3 downto 0),
      tmdsb(3 downto 0) => tmdsb(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0_S00_AXI is
  port (
    s00_axi_wready : out STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    CLK : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    tmds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tmdsb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ac_mclk : out STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    \^clk\ : in STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aclk : in STD_LOGIC;
    axi_awready_reg_0 : in STD_LOGIC;
    axi_arready_reg_0 : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    btn : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0_S00_AXI;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0_S00_AXI is
  signal \^clk_1\ : STD_LOGIC;
  signal RST : STD_LOGIC;
  signal WREN : STD_LOGIC;
  signal \axi_araddr_reg[2]_rep_n_0\ : STD_LOGIC;
  signal \axi_araddr_reg[3]_rep_n_0\ : STD_LOGIC;
  signal axi_arready_i_1_n_0 : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal \axi_rdata[0]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_3_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal control_n_0 : STD_LOGIC;
  signal datapath_n_13 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_arready\ : STD_LOGIC;
  signal \^s00_axi_awready\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal \^s00_axi_wready\ : STD_LOGIC;
  signal \sampCounter/processQ0\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal slv_reg0 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \slv_reg0[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg0[31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg0__0\ : STD_LOGIC_VECTOR ( 31 downto 10 );
  signal slv_reg1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal slv_reg10 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \slv_reg10[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg10[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg10[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg10[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg10[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg10__0\ : STD_LOGIC_VECTOR ( 31 downto 10 );
  signal slv_reg11 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \slv_reg11[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg11[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg11[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg11[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg11[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg11__0\ : STD_LOGIC_VECTOR ( 31 downto 10 );
  signal slv_reg12 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg12[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg12[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg12[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg12[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg12[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg13 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg13[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg13[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg13[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg13[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg13[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg14 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg14[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg14[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg14[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg14[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg14[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg15 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg15[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg15[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg15[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg15[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg15[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg16 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg16[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg16[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg16[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg16[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg16[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg17 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg17[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg17[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg17[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg17[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg17[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg18 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg18[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg18[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg18[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg18[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg18[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg19 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg19[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg19[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg19[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg19[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg19[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1__0\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal slv_reg2 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal slv_reg20 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg20[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg20[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg20[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg20[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg20[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg21 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg21[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg21[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg21[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg21[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg21[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg22 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg22[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg22[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg22[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg22[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg22[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg23 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg23[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg23[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg23[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg23[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg23[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg24 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg24[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg24[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg24[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg24[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg24[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg25 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg25[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg25[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg25[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg25[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg25[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg26 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg26[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg26[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg26[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg26[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg26[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg27 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg27[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg27[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg27[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg27[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg27[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg28 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg28[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg28[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg28[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg28[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg28[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg29 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg29[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg29[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg29[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg29[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg29[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg2[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2__0\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal slv_reg3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal slv_reg30 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg30[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg30[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg30[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg30[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg30[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg31 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg31[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg31[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg31[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg31[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg31[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg3[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3__0\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal slv_reg4 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \slv_reg4[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg4[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4__0\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal slv_reg5 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \slv_reg5[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg5[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5__0\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal sw : STD_LOGIC_VECTOR ( 1 to 1 );
  attribute ORIG_CELL_NAME : string;
  attribute ORIG_CELL_NAME of \axi_araddr_reg[2]\ : label is "axi_araddr_reg[2]";
  attribute ORIG_CELL_NAME of \axi_araddr_reg[2]_rep\ : label is "axi_araddr_reg[2]";
  attribute ORIG_CELL_NAME of \axi_araddr_reg[3]\ : label is "axi_araddr_reg[3]";
  attribute ORIG_CELL_NAME of \axi_araddr_reg[3]_rep\ : label is "axi_araddr_reg[3]";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \slv_reg0[31]_i_3\ : label is "soft_lutpair96";
begin
  CLK <= \^clk_1\;
  s00_axi_arready <= \^s00_axi_arready\;
  s00_axi_awready <= \^s00_axi_awready\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
  s00_axi_wready <= \^s00_axi_wready\;
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready_i_1_n_0,
      D => s00_axi_araddr(0),
      Q => sel0(0),
      S => SR(0)
    );
\axi_araddr_reg[2]_rep\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready_i_1_n_0,
      D => s00_axi_araddr(0),
      Q => \axi_araddr_reg[2]_rep_n_0\,
      S => SR(0)
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready_i_1_n_0,
      D => s00_axi_araddr(1),
      Q => sel0(1),
      S => SR(0)
    );
\axi_araddr_reg[3]_rep\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready_i_1_n_0,
      D => s00_axi_araddr(1),
      Q => \axi_araddr_reg[3]_rep_n_0\,
      S => SR(0)
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready_i_1_n_0,
      D => s00_axi_araddr(2),
      Q => sel0(2),
      S => SR(0)
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready_i_1_n_0,
      D => s00_axi_araddr(3),
      Q => sel0(3),
      S => SR(0)
    );
\axi_araddr_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready_i_1_n_0,
      D => s00_axi_araddr(4),
      Q => sel0(4),
      S => SR(0)
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_arready\,
      O => axi_arready_i_1_n_0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready_i_1_n_0,
      Q => \^s00_axi_arready\,
      R => SR(0)
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(0),
      Q => p_0_in(0),
      R => SR(0)
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(1),
      Q => p_0_in(1),
      R => SR(0)
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(2),
      Q => p_0_in(2),
      R => SR(0)
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(3),
      Q => p_0_in(3),
      R => SR(0)
    );
\axi_awaddr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(4),
      Q => p_0_in(4),
      R => SR(0)
    );
axi_awready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s00_axi_awready\,
      R => SR(0)
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready_reg_0,
      Q => s00_axi_bvalid,
      R => SR(0)
    );
\axi_rdata[0]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(0),
      I1 => slv_reg14(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(0),
      O => \axi_rdata[0]_i_11_n_0\
    );
\axi_rdata[0]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(0),
      I1 => slv_reg2(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(0),
      O => \axi_rdata[0]_i_12_n_0\
    );
\axi_rdata[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(0),
      I1 => slv_reg26(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(0),
      O => \axi_rdata[0]_i_6_n_0\
    );
\axi_rdata[0]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(0),
      I1 => slv_reg30(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(0),
      O => \axi_rdata[0]_i_7_n_0\
    );
\axi_rdata[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(0),
      I1 => slv_reg18(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(0),
      O => \axi_rdata[0]_i_8_n_0\
    );
\axi_rdata[0]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(0),
      I1 => slv_reg22(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(0),
      O => \axi_rdata[0]_i_9_n_0\
    );
\axi_rdata[10]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(10),
      I1 => slv_reg14(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(10),
      O => \axi_rdata[10]_i_10_n_0\
    );
\axi_rdata[10]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(10),
      I1 => slv_reg2(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg0__0\(10),
      O => \axi_rdata[10]_i_11_n_0\
    );
\axi_rdata[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[10]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \slv_reg10__0\(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg11__0\(10),
      O => \axi_rdata[10]_i_4_n_0\
    );
\axi_rdata[10]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(10),
      I1 => slv_reg26(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(10),
      O => \axi_rdata[10]_i_6_n_0\
    );
\axi_rdata[10]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(10),
      I1 => slv_reg30(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(10),
      O => \axi_rdata[10]_i_7_n_0\
    );
\axi_rdata[10]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(10),
      I1 => slv_reg18(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(10),
      O => \axi_rdata[10]_i_8_n_0\
    );
\axi_rdata[10]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(10),
      I1 => slv_reg22(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(10),
      O => \axi_rdata[10]_i_9_n_0\
    );
\axi_rdata[11]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(11),
      I1 => slv_reg14(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(11),
      O => \axi_rdata[11]_i_10_n_0\
    );
\axi_rdata[11]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(11),
      I1 => slv_reg2(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg0__0\(11),
      O => \axi_rdata[11]_i_11_n_0\
    );
\axi_rdata[11]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[11]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \slv_reg10__0\(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg11__0\(11),
      O => \axi_rdata[11]_i_4_n_0\
    );
\axi_rdata[11]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(11),
      I1 => slv_reg26(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(11),
      O => \axi_rdata[11]_i_6_n_0\
    );
\axi_rdata[11]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(11),
      I1 => slv_reg30(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(11),
      O => \axi_rdata[11]_i_7_n_0\
    );
\axi_rdata[11]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(11),
      I1 => slv_reg18(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(11),
      O => \axi_rdata[11]_i_8_n_0\
    );
\axi_rdata[11]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(11),
      I1 => slv_reg22(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(11),
      O => \axi_rdata[11]_i_9_n_0\
    );
\axi_rdata[12]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(12),
      I1 => slv_reg14(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(12),
      O => \axi_rdata[12]_i_10_n_0\
    );
\axi_rdata[12]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(12),
      I1 => slv_reg2(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg0__0\(12),
      O => \axi_rdata[12]_i_11_n_0\
    );
\axi_rdata[12]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[12]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \slv_reg10__0\(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg11__0\(12),
      O => \axi_rdata[12]_i_4_n_0\
    );
\axi_rdata[12]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(12),
      I1 => slv_reg26(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(12),
      O => \axi_rdata[12]_i_6_n_0\
    );
\axi_rdata[12]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(12),
      I1 => slv_reg30(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(12),
      O => \axi_rdata[12]_i_7_n_0\
    );
\axi_rdata[12]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(12),
      I1 => slv_reg18(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(12),
      O => \axi_rdata[12]_i_8_n_0\
    );
\axi_rdata[12]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(12),
      I1 => slv_reg22(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(12),
      O => \axi_rdata[12]_i_9_n_0\
    );
\axi_rdata[13]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(13),
      I1 => slv_reg14(13),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(13),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(13),
      O => \axi_rdata[13]_i_10_n_0\
    );
\axi_rdata[13]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(13),
      I1 => slv_reg2(13),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(13),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg0__0\(13),
      O => \axi_rdata[13]_i_11_n_0\
    );
\axi_rdata[13]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[13]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \slv_reg10__0\(13),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg11__0\(13),
      O => \axi_rdata[13]_i_4_n_0\
    );
\axi_rdata[13]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(13),
      I1 => slv_reg26(13),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(13),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(13),
      O => \axi_rdata[13]_i_6_n_0\
    );
\axi_rdata[13]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(13),
      I1 => slv_reg30(13),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(13),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(13),
      O => \axi_rdata[13]_i_7_n_0\
    );
\axi_rdata[13]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(13),
      I1 => slv_reg18(13),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(13),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(13),
      O => \axi_rdata[13]_i_8_n_0\
    );
\axi_rdata[13]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(13),
      I1 => slv_reg22(13),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(13),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(13),
      O => \axi_rdata[13]_i_9_n_0\
    );
\axi_rdata[14]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(14),
      I1 => slv_reg14(14),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(14),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(14),
      O => \axi_rdata[14]_i_10_n_0\
    );
\axi_rdata[14]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(14),
      I1 => slv_reg2(14),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(14),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg0__0\(14),
      O => \axi_rdata[14]_i_11_n_0\
    );
\axi_rdata[14]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[14]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \slv_reg10__0\(14),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg11__0\(14),
      O => \axi_rdata[14]_i_4_n_0\
    );
\axi_rdata[14]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(14),
      I1 => slv_reg26(14),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(14),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(14),
      O => \axi_rdata[14]_i_6_n_0\
    );
\axi_rdata[14]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(14),
      I1 => slv_reg30(14),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(14),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(14),
      O => \axi_rdata[14]_i_7_n_0\
    );
\axi_rdata[14]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(14),
      I1 => slv_reg18(14),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(14),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(14),
      O => \axi_rdata[14]_i_8_n_0\
    );
\axi_rdata[14]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(14),
      I1 => slv_reg22(14),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(14),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(14),
      O => \axi_rdata[14]_i_9_n_0\
    );
\axi_rdata[15]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(15),
      I1 => slv_reg14(15),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(15),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(15),
      O => \axi_rdata[15]_i_10_n_0\
    );
\axi_rdata[15]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(15),
      I1 => slv_reg2(15),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(15),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg0__0\(15),
      O => \axi_rdata[15]_i_11_n_0\
    );
\axi_rdata[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[15]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \slv_reg10__0\(15),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => \slv_reg11__0\(15),
      O => \axi_rdata[15]_i_4_n_0\
    );
\axi_rdata[15]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(15),
      I1 => slv_reg26(15),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(15),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(15),
      O => \axi_rdata[15]_i_6_n_0\
    );
\axi_rdata[15]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(15),
      I1 => slv_reg30(15),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(15),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(15),
      O => \axi_rdata[15]_i_7_n_0\
    );
\axi_rdata[15]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(15),
      I1 => slv_reg18(15),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(15),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(15),
      O => \axi_rdata[15]_i_8_n_0\
    );
\axi_rdata[15]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(15),
      I1 => slv_reg22(15),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(15),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(15),
      O => \axi_rdata[15]_i_9_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[16]_i_2_n_0\,
      I1 => \axi_rdata_reg[16]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[16]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[16]_i_5_n_0\,
      O => reg_data_out(16)
    );
\axi_rdata[16]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(16),
      I1 => slv_reg14(16),
      I2 => sel0(1),
      I3 => slv_reg13(16),
      I4 => sel0(0),
      I5 => slv_reg12(16),
      O => \axi_rdata[16]_i_10_n_0\
    );
\axi_rdata[16]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(16),
      I1 => \slv_reg2__0\(16),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(16),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(16),
      O => \axi_rdata[16]_i_11_n_0\
    );
\axi_rdata[16]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[16]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(16),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(16),
      O => \axi_rdata[16]_i_4_n_0\
    );
\axi_rdata[16]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(16),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(16),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[16]_i_11_n_0\,
      O => \axi_rdata[16]_i_5_n_0\
    );
\axi_rdata[16]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(16),
      I1 => slv_reg26(16),
      I2 => sel0(1),
      I3 => slv_reg25(16),
      I4 => sel0(0),
      I5 => slv_reg24(16),
      O => \axi_rdata[16]_i_6_n_0\
    );
\axi_rdata[16]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(16),
      I1 => slv_reg30(16),
      I2 => sel0(1),
      I3 => slv_reg29(16),
      I4 => sel0(0),
      I5 => slv_reg28(16),
      O => \axi_rdata[16]_i_7_n_0\
    );
\axi_rdata[16]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(16),
      I1 => slv_reg18(16),
      I2 => sel0(1),
      I3 => slv_reg17(16),
      I4 => sel0(0),
      I5 => slv_reg16(16),
      O => \axi_rdata[16]_i_8_n_0\
    );
\axi_rdata[16]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(16),
      I1 => slv_reg22(16),
      I2 => sel0(1),
      I3 => slv_reg21(16),
      I4 => sel0(0),
      I5 => slv_reg20(16),
      O => \axi_rdata[16]_i_9_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[17]_i_2_n_0\,
      I1 => \axi_rdata_reg[17]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[17]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[17]_i_5_n_0\,
      O => reg_data_out(17)
    );
\axi_rdata[17]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(17),
      I1 => slv_reg14(17),
      I2 => sel0(1),
      I3 => slv_reg13(17),
      I4 => sel0(0),
      I5 => slv_reg12(17),
      O => \axi_rdata[17]_i_10_n_0\
    );
\axi_rdata[17]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(17),
      I1 => \slv_reg2__0\(17),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(17),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(17),
      O => \axi_rdata[17]_i_11_n_0\
    );
\axi_rdata[17]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[17]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(17),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(17),
      O => \axi_rdata[17]_i_4_n_0\
    );
\axi_rdata[17]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(17),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(17),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[17]_i_11_n_0\,
      O => \axi_rdata[17]_i_5_n_0\
    );
\axi_rdata[17]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(17),
      I1 => slv_reg26(17),
      I2 => sel0(1),
      I3 => slv_reg25(17),
      I4 => sel0(0),
      I5 => slv_reg24(17),
      O => \axi_rdata[17]_i_6_n_0\
    );
\axi_rdata[17]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(17),
      I1 => slv_reg30(17),
      I2 => sel0(1),
      I3 => slv_reg29(17),
      I4 => sel0(0),
      I5 => slv_reg28(17),
      O => \axi_rdata[17]_i_7_n_0\
    );
\axi_rdata[17]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(17),
      I1 => slv_reg18(17),
      I2 => sel0(1),
      I3 => slv_reg17(17),
      I4 => sel0(0),
      I5 => slv_reg16(17),
      O => \axi_rdata[17]_i_8_n_0\
    );
\axi_rdata[17]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(17),
      I1 => slv_reg22(17),
      I2 => sel0(1),
      I3 => slv_reg21(17),
      I4 => sel0(0),
      I5 => slv_reg20(17),
      O => \axi_rdata[17]_i_9_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[18]_i_2_n_0\,
      I1 => \axi_rdata_reg[18]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[18]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[18]_i_5_n_0\,
      O => reg_data_out(18)
    );
\axi_rdata[18]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(18),
      I1 => slv_reg14(18),
      I2 => sel0(1),
      I3 => slv_reg13(18),
      I4 => sel0(0),
      I5 => slv_reg12(18),
      O => \axi_rdata[18]_i_10_n_0\
    );
\axi_rdata[18]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(18),
      I1 => \slv_reg2__0\(18),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(18),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(18),
      O => \axi_rdata[18]_i_11_n_0\
    );
\axi_rdata[18]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[18]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(18),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(18),
      O => \axi_rdata[18]_i_4_n_0\
    );
\axi_rdata[18]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(18),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(18),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[18]_i_11_n_0\,
      O => \axi_rdata[18]_i_5_n_0\
    );
\axi_rdata[18]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(18),
      I1 => slv_reg26(18),
      I2 => sel0(1),
      I3 => slv_reg25(18),
      I4 => sel0(0),
      I5 => slv_reg24(18),
      O => \axi_rdata[18]_i_6_n_0\
    );
\axi_rdata[18]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(18),
      I1 => slv_reg30(18),
      I2 => sel0(1),
      I3 => slv_reg29(18),
      I4 => sel0(0),
      I5 => slv_reg28(18),
      O => \axi_rdata[18]_i_7_n_0\
    );
\axi_rdata[18]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(18),
      I1 => slv_reg18(18),
      I2 => sel0(1),
      I3 => slv_reg17(18),
      I4 => sel0(0),
      I5 => slv_reg16(18),
      O => \axi_rdata[18]_i_8_n_0\
    );
\axi_rdata[18]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(18),
      I1 => slv_reg22(18),
      I2 => sel0(1),
      I3 => slv_reg21(18),
      I4 => sel0(0),
      I5 => slv_reg20(18),
      O => \axi_rdata[18]_i_9_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[19]_i_2_n_0\,
      I1 => \axi_rdata_reg[19]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[19]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[19]_i_5_n_0\,
      O => reg_data_out(19)
    );
\axi_rdata[19]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(19),
      I1 => slv_reg14(19),
      I2 => sel0(1),
      I3 => slv_reg13(19),
      I4 => sel0(0),
      I5 => slv_reg12(19),
      O => \axi_rdata[19]_i_10_n_0\
    );
\axi_rdata[19]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(19),
      I1 => \slv_reg2__0\(19),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(19),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(19),
      O => \axi_rdata[19]_i_11_n_0\
    );
\axi_rdata[19]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[19]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(19),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(19),
      O => \axi_rdata[19]_i_4_n_0\
    );
\axi_rdata[19]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(19),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(19),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[19]_i_11_n_0\,
      O => \axi_rdata[19]_i_5_n_0\
    );
\axi_rdata[19]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(19),
      I1 => slv_reg26(19),
      I2 => sel0(1),
      I3 => slv_reg25(19),
      I4 => sel0(0),
      I5 => slv_reg24(19),
      O => \axi_rdata[19]_i_6_n_0\
    );
\axi_rdata[19]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(19),
      I1 => slv_reg30(19),
      I2 => sel0(1),
      I3 => slv_reg29(19),
      I4 => sel0(0),
      I5 => slv_reg28(19),
      O => \axi_rdata[19]_i_7_n_0\
    );
\axi_rdata[19]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(19),
      I1 => slv_reg18(19),
      I2 => sel0(1),
      I3 => slv_reg17(19),
      I4 => sel0(0),
      I5 => slv_reg16(19),
      O => \axi_rdata[19]_i_8_n_0\
    );
\axi_rdata[19]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(19),
      I1 => slv_reg22(19),
      I2 => sel0(1),
      I3 => slv_reg21(19),
      I4 => sel0(0),
      I5 => slv_reg20(19),
      O => \axi_rdata[19]_i_9_n_0\
    );
\axi_rdata[1]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(1),
      I1 => slv_reg14(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(1),
      O => \axi_rdata[1]_i_10_n_0\
    );
\axi_rdata[1]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(1),
      I1 => slv_reg2(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(1),
      O => \axi_rdata[1]_i_11_n_0\
    );
\axi_rdata[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[1]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg10(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg11(1),
      O => \axi_rdata[1]_i_4_n_0\
    );
\axi_rdata[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(1),
      I1 => slv_reg26(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(1),
      O => \axi_rdata[1]_i_6_n_0\
    );
\axi_rdata[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(1),
      I1 => slv_reg30(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(1),
      O => \axi_rdata[1]_i_7_n_0\
    );
\axi_rdata[1]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(1),
      I1 => slv_reg18(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(1),
      O => \axi_rdata[1]_i_8_n_0\
    );
\axi_rdata[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(1),
      I1 => slv_reg22(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(1),
      O => \axi_rdata[1]_i_9_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[20]_i_2_n_0\,
      I1 => \axi_rdata_reg[20]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[20]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[20]_i_5_n_0\,
      O => reg_data_out(20)
    );
\axi_rdata[20]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(20),
      I1 => slv_reg14(20),
      I2 => sel0(1),
      I3 => slv_reg13(20),
      I4 => sel0(0),
      I5 => slv_reg12(20),
      O => \axi_rdata[20]_i_10_n_0\
    );
\axi_rdata[20]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(20),
      I1 => \slv_reg2__0\(20),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(20),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(20),
      O => \axi_rdata[20]_i_11_n_0\
    );
\axi_rdata[20]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[20]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(20),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(20),
      O => \axi_rdata[20]_i_4_n_0\
    );
\axi_rdata[20]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(20),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(20),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[20]_i_11_n_0\,
      O => \axi_rdata[20]_i_5_n_0\
    );
\axi_rdata[20]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(20),
      I1 => slv_reg26(20),
      I2 => sel0(1),
      I3 => slv_reg25(20),
      I4 => sel0(0),
      I5 => slv_reg24(20),
      O => \axi_rdata[20]_i_6_n_0\
    );
\axi_rdata[20]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(20),
      I1 => slv_reg30(20),
      I2 => sel0(1),
      I3 => slv_reg29(20),
      I4 => sel0(0),
      I5 => slv_reg28(20),
      O => \axi_rdata[20]_i_7_n_0\
    );
\axi_rdata[20]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(20),
      I1 => slv_reg18(20),
      I2 => sel0(1),
      I3 => slv_reg17(20),
      I4 => sel0(0),
      I5 => slv_reg16(20),
      O => \axi_rdata[20]_i_8_n_0\
    );
\axi_rdata[20]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(20),
      I1 => slv_reg22(20),
      I2 => sel0(1),
      I3 => slv_reg21(20),
      I4 => sel0(0),
      I5 => slv_reg20(20),
      O => \axi_rdata[20]_i_9_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[21]_i_2_n_0\,
      I1 => \axi_rdata_reg[21]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[21]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[21]_i_5_n_0\,
      O => reg_data_out(21)
    );
\axi_rdata[21]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(21),
      I1 => slv_reg14(21),
      I2 => sel0(1),
      I3 => slv_reg13(21),
      I4 => sel0(0),
      I5 => slv_reg12(21),
      O => \axi_rdata[21]_i_10_n_0\
    );
\axi_rdata[21]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(21),
      I1 => \slv_reg2__0\(21),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(21),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(21),
      O => \axi_rdata[21]_i_11_n_0\
    );
\axi_rdata[21]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[21]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(21),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(21),
      O => \axi_rdata[21]_i_4_n_0\
    );
\axi_rdata[21]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(21),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(21),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[21]_i_11_n_0\,
      O => \axi_rdata[21]_i_5_n_0\
    );
\axi_rdata[21]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(21),
      I1 => slv_reg26(21),
      I2 => sel0(1),
      I3 => slv_reg25(21),
      I4 => sel0(0),
      I5 => slv_reg24(21),
      O => \axi_rdata[21]_i_6_n_0\
    );
\axi_rdata[21]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(21),
      I1 => slv_reg30(21),
      I2 => sel0(1),
      I3 => slv_reg29(21),
      I4 => sel0(0),
      I5 => slv_reg28(21),
      O => \axi_rdata[21]_i_7_n_0\
    );
\axi_rdata[21]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(21),
      I1 => slv_reg18(21),
      I2 => sel0(1),
      I3 => slv_reg17(21),
      I4 => sel0(0),
      I5 => slv_reg16(21),
      O => \axi_rdata[21]_i_8_n_0\
    );
\axi_rdata[21]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(21),
      I1 => slv_reg22(21),
      I2 => sel0(1),
      I3 => slv_reg21(21),
      I4 => sel0(0),
      I5 => slv_reg20(21),
      O => \axi_rdata[21]_i_9_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[22]_i_2_n_0\,
      I1 => \axi_rdata_reg[22]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[22]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[22]_i_5_n_0\,
      O => reg_data_out(22)
    );
\axi_rdata[22]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(22),
      I1 => slv_reg14(22),
      I2 => sel0(1),
      I3 => slv_reg13(22),
      I4 => sel0(0),
      I5 => slv_reg12(22),
      O => \axi_rdata[22]_i_10_n_0\
    );
\axi_rdata[22]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(22),
      I1 => \slv_reg2__0\(22),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(22),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(22),
      O => \axi_rdata[22]_i_11_n_0\
    );
\axi_rdata[22]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[22]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(22),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(22),
      O => \axi_rdata[22]_i_4_n_0\
    );
\axi_rdata[22]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(22),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(22),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[22]_i_11_n_0\,
      O => \axi_rdata[22]_i_5_n_0\
    );
\axi_rdata[22]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(22),
      I1 => slv_reg26(22),
      I2 => sel0(1),
      I3 => slv_reg25(22),
      I4 => sel0(0),
      I5 => slv_reg24(22),
      O => \axi_rdata[22]_i_6_n_0\
    );
\axi_rdata[22]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(22),
      I1 => slv_reg30(22),
      I2 => sel0(1),
      I3 => slv_reg29(22),
      I4 => sel0(0),
      I5 => slv_reg28(22),
      O => \axi_rdata[22]_i_7_n_0\
    );
\axi_rdata[22]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(22),
      I1 => slv_reg18(22),
      I2 => sel0(1),
      I3 => slv_reg17(22),
      I4 => sel0(0),
      I5 => slv_reg16(22),
      O => \axi_rdata[22]_i_8_n_0\
    );
\axi_rdata[22]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(22),
      I1 => slv_reg22(22),
      I2 => sel0(1),
      I3 => slv_reg21(22),
      I4 => sel0(0),
      I5 => slv_reg20(22),
      O => \axi_rdata[22]_i_9_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[23]_i_2_n_0\,
      I1 => \axi_rdata_reg[23]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[23]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[23]_i_5_n_0\,
      O => reg_data_out(23)
    );
\axi_rdata[23]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(23),
      I1 => slv_reg14(23),
      I2 => sel0(1),
      I3 => slv_reg13(23),
      I4 => sel0(0),
      I5 => slv_reg12(23),
      O => \axi_rdata[23]_i_10_n_0\
    );
\axi_rdata[23]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(23),
      I1 => \slv_reg2__0\(23),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(23),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(23),
      O => \axi_rdata[23]_i_11_n_0\
    );
\axi_rdata[23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[23]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(23),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(23),
      O => \axi_rdata[23]_i_4_n_0\
    );
\axi_rdata[23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(23),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(23),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[23]_i_11_n_0\,
      O => \axi_rdata[23]_i_5_n_0\
    );
\axi_rdata[23]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(23),
      I1 => slv_reg26(23),
      I2 => sel0(1),
      I3 => slv_reg25(23),
      I4 => sel0(0),
      I5 => slv_reg24(23),
      O => \axi_rdata[23]_i_6_n_0\
    );
\axi_rdata[23]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(23),
      I1 => slv_reg30(23),
      I2 => sel0(1),
      I3 => slv_reg29(23),
      I4 => sel0(0),
      I5 => slv_reg28(23),
      O => \axi_rdata[23]_i_7_n_0\
    );
\axi_rdata[23]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(23),
      I1 => slv_reg18(23),
      I2 => sel0(1),
      I3 => slv_reg17(23),
      I4 => sel0(0),
      I5 => slv_reg16(23),
      O => \axi_rdata[23]_i_8_n_0\
    );
\axi_rdata[23]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(23),
      I1 => slv_reg22(23),
      I2 => sel0(1),
      I3 => slv_reg21(23),
      I4 => sel0(0),
      I5 => slv_reg20(23),
      O => \axi_rdata[23]_i_9_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[24]_i_2_n_0\,
      I1 => \axi_rdata_reg[24]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[24]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[24]_i_5_n_0\,
      O => reg_data_out(24)
    );
\axi_rdata[24]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(24),
      I1 => slv_reg14(24),
      I2 => sel0(1),
      I3 => slv_reg13(24),
      I4 => sel0(0),
      I5 => slv_reg12(24),
      O => \axi_rdata[24]_i_10_n_0\
    );
\axi_rdata[24]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(24),
      I1 => \slv_reg2__0\(24),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(24),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(24),
      O => \axi_rdata[24]_i_11_n_0\
    );
\axi_rdata[24]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[24]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(24),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(24),
      O => \axi_rdata[24]_i_4_n_0\
    );
\axi_rdata[24]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(24),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(24),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[24]_i_11_n_0\,
      O => \axi_rdata[24]_i_5_n_0\
    );
\axi_rdata[24]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(24),
      I1 => slv_reg26(24),
      I2 => sel0(1),
      I3 => slv_reg25(24),
      I4 => sel0(0),
      I5 => slv_reg24(24),
      O => \axi_rdata[24]_i_6_n_0\
    );
\axi_rdata[24]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(24),
      I1 => slv_reg30(24),
      I2 => sel0(1),
      I3 => slv_reg29(24),
      I4 => sel0(0),
      I5 => slv_reg28(24),
      O => \axi_rdata[24]_i_7_n_0\
    );
\axi_rdata[24]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(24),
      I1 => slv_reg18(24),
      I2 => sel0(1),
      I3 => slv_reg17(24),
      I4 => sel0(0),
      I5 => slv_reg16(24),
      O => \axi_rdata[24]_i_8_n_0\
    );
\axi_rdata[24]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(24),
      I1 => slv_reg22(24),
      I2 => sel0(1),
      I3 => slv_reg21(24),
      I4 => sel0(0),
      I5 => slv_reg20(24),
      O => \axi_rdata[24]_i_9_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[25]_i_2_n_0\,
      I1 => \axi_rdata_reg[25]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[25]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[25]_i_5_n_0\,
      O => reg_data_out(25)
    );
\axi_rdata[25]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(25),
      I1 => slv_reg14(25),
      I2 => sel0(1),
      I3 => slv_reg13(25),
      I4 => sel0(0),
      I5 => slv_reg12(25),
      O => \axi_rdata[25]_i_10_n_0\
    );
\axi_rdata[25]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(25),
      I1 => \slv_reg2__0\(25),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(25),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(25),
      O => \axi_rdata[25]_i_11_n_0\
    );
\axi_rdata[25]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[25]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(25),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(25),
      O => \axi_rdata[25]_i_4_n_0\
    );
\axi_rdata[25]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(25),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(25),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[25]_i_11_n_0\,
      O => \axi_rdata[25]_i_5_n_0\
    );
\axi_rdata[25]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(25),
      I1 => slv_reg26(25),
      I2 => sel0(1),
      I3 => slv_reg25(25),
      I4 => sel0(0),
      I5 => slv_reg24(25),
      O => \axi_rdata[25]_i_6_n_0\
    );
\axi_rdata[25]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(25),
      I1 => slv_reg30(25),
      I2 => sel0(1),
      I3 => slv_reg29(25),
      I4 => sel0(0),
      I5 => slv_reg28(25),
      O => \axi_rdata[25]_i_7_n_0\
    );
\axi_rdata[25]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(25),
      I1 => slv_reg18(25),
      I2 => sel0(1),
      I3 => slv_reg17(25),
      I4 => sel0(0),
      I5 => slv_reg16(25),
      O => \axi_rdata[25]_i_8_n_0\
    );
\axi_rdata[25]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(25),
      I1 => slv_reg22(25),
      I2 => sel0(1),
      I3 => slv_reg21(25),
      I4 => sel0(0),
      I5 => slv_reg20(25),
      O => \axi_rdata[25]_i_9_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[26]_i_2_n_0\,
      I1 => \axi_rdata_reg[26]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[26]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[26]_i_5_n_0\,
      O => reg_data_out(26)
    );
\axi_rdata[26]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(26),
      I1 => slv_reg14(26),
      I2 => sel0(1),
      I3 => slv_reg13(26),
      I4 => sel0(0),
      I5 => slv_reg12(26),
      O => \axi_rdata[26]_i_10_n_0\
    );
\axi_rdata[26]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(26),
      I1 => \slv_reg2__0\(26),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(26),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(26),
      O => \axi_rdata[26]_i_11_n_0\
    );
\axi_rdata[26]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[26]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(26),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(26),
      O => \axi_rdata[26]_i_4_n_0\
    );
\axi_rdata[26]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(26),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(26),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[26]_i_11_n_0\,
      O => \axi_rdata[26]_i_5_n_0\
    );
\axi_rdata[26]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(26),
      I1 => slv_reg26(26),
      I2 => sel0(1),
      I3 => slv_reg25(26),
      I4 => sel0(0),
      I5 => slv_reg24(26),
      O => \axi_rdata[26]_i_6_n_0\
    );
\axi_rdata[26]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(26),
      I1 => slv_reg30(26),
      I2 => sel0(1),
      I3 => slv_reg29(26),
      I4 => sel0(0),
      I5 => slv_reg28(26),
      O => \axi_rdata[26]_i_7_n_0\
    );
\axi_rdata[26]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(26),
      I1 => slv_reg18(26),
      I2 => sel0(1),
      I3 => slv_reg17(26),
      I4 => sel0(0),
      I5 => slv_reg16(26),
      O => \axi_rdata[26]_i_8_n_0\
    );
\axi_rdata[26]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(26),
      I1 => slv_reg22(26),
      I2 => sel0(1),
      I3 => slv_reg21(26),
      I4 => sel0(0),
      I5 => slv_reg20(26),
      O => \axi_rdata[26]_i_9_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[27]_i_2_n_0\,
      I1 => \axi_rdata_reg[27]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[27]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[27]_i_5_n_0\,
      O => reg_data_out(27)
    );
\axi_rdata[27]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(27),
      I1 => slv_reg14(27),
      I2 => sel0(1),
      I3 => slv_reg13(27),
      I4 => sel0(0),
      I5 => slv_reg12(27),
      O => \axi_rdata[27]_i_10_n_0\
    );
\axi_rdata[27]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(27),
      I1 => \slv_reg2__0\(27),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(27),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(27),
      O => \axi_rdata[27]_i_11_n_0\
    );
\axi_rdata[27]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[27]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(27),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(27),
      O => \axi_rdata[27]_i_4_n_0\
    );
\axi_rdata[27]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(27),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(27),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[27]_i_11_n_0\,
      O => \axi_rdata[27]_i_5_n_0\
    );
\axi_rdata[27]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(27),
      I1 => slv_reg26(27),
      I2 => sel0(1),
      I3 => slv_reg25(27),
      I4 => sel0(0),
      I5 => slv_reg24(27),
      O => \axi_rdata[27]_i_6_n_0\
    );
\axi_rdata[27]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(27),
      I1 => slv_reg30(27),
      I2 => sel0(1),
      I3 => slv_reg29(27),
      I4 => sel0(0),
      I5 => slv_reg28(27),
      O => \axi_rdata[27]_i_7_n_0\
    );
\axi_rdata[27]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(27),
      I1 => slv_reg18(27),
      I2 => sel0(1),
      I3 => slv_reg17(27),
      I4 => sel0(0),
      I5 => slv_reg16(27),
      O => \axi_rdata[27]_i_8_n_0\
    );
\axi_rdata[27]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(27),
      I1 => slv_reg22(27),
      I2 => sel0(1),
      I3 => slv_reg21(27),
      I4 => sel0(0),
      I5 => slv_reg20(27),
      O => \axi_rdata[27]_i_9_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[28]_i_2_n_0\,
      I1 => \axi_rdata_reg[28]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[28]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[28]_i_5_n_0\,
      O => reg_data_out(28)
    );
\axi_rdata[28]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(28),
      I1 => slv_reg14(28),
      I2 => sel0(1),
      I3 => slv_reg13(28),
      I4 => sel0(0),
      I5 => slv_reg12(28),
      O => \axi_rdata[28]_i_10_n_0\
    );
\axi_rdata[28]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(28),
      I1 => \slv_reg2__0\(28),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(28),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(28),
      O => \axi_rdata[28]_i_11_n_0\
    );
\axi_rdata[28]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[28]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(28),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(28),
      O => \axi_rdata[28]_i_4_n_0\
    );
\axi_rdata[28]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(28),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(28),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[28]_i_11_n_0\,
      O => \axi_rdata[28]_i_5_n_0\
    );
\axi_rdata[28]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(28),
      I1 => slv_reg26(28),
      I2 => sel0(1),
      I3 => slv_reg25(28),
      I4 => sel0(0),
      I5 => slv_reg24(28),
      O => \axi_rdata[28]_i_6_n_0\
    );
\axi_rdata[28]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(28),
      I1 => slv_reg30(28),
      I2 => sel0(1),
      I3 => slv_reg29(28),
      I4 => sel0(0),
      I5 => slv_reg28(28),
      O => \axi_rdata[28]_i_7_n_0\
    );
\axi_rdata[28]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(28),
      I1 => slv_reg18(28),
      I2 => sel0(1),
      I3 => slv_reg17(28),
      I4 => sel0(0),
      I5 => slv_reg16(28),
      O => \axi_rdata[28]_i_8_n_0\
    );
\axi_rdata[28]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(28),
      I1 => slv_reg22(28),
      I2 => sel0(1),
      I3 => slv_reg21(28),
      I4 => sel0(0),
      I5 => slv_reg20(28),
      O => \axi_rdata[28]_i_9_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[29]_i_2_n_0\,
      I1 => \axi_rdata_reg[29]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[29]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[29]_i_5_n_0\,
      O => reg_data_out(29)
    );
\axi_rdata[29]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(29),
      I1 => slv_reg14(29),
      I2 => sel0(1),
      I3 => slv_reg13(29),
      I4 => sel0(0),
      I5 => slv_reg12(29),
      O => \axi_rdata[29]_i_10_n_0\
    );
\axi_rdata[29]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(29),
      I1 => \slv_reg2__0\(29),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(29),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(29),
      O => \axi_rdata[29]_i_11_n_0\
    );
\axi_rdata[29]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[29]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(29),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(29),
      O => \axi_rdata[29]_i_4_n_0\
    );
\axi_rdata[29]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(29),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(29),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[29]_i_11_n_0\,
      O => \axi_rdata[29]_i_5_n_0\
    );
\axi_rdata[29]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(29),
      I1 => slv_reg26(29),
      I2 => sel0(1),
      I3 => slv_reg25(29),
      I4 => sel0(0),
      I5 => slv_reg24(29),
      O => \axi_rdata[29]_i_6_n_0\
    );
\axi_rdata[29]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(29),
      I1 => slv_reg30(29),
      I2 => sel0(1),
      I3 => slv_reg29(29),
      I4 => sel0(0),
      I5 => slv_reg28(29),
      O => \axi_rdata[29]_i_7_n_0\
    );
\axi_rdata[29]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(29),
      I1 => slv_reg18(29),
      I2 => sel0(1),
      I3 => slv_reg17(29),
      I4 => sel0(0),
      I5 => slv_reg16(29),
      O => \axi_rdata[29]_i_8_n_0\
    );
\axi_rdata[29]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(29),
      I1 => slv_reg22(29),
      I2 => sel0(1),
      I3 => slv_reg21(29),
      I4 => sel0(0),
      I5 => slv_reg20(29),
      O => \axi_rdata[29]_i_9_n_0\
    );
\axi_rdata[2]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(2),
      I1 => slv_reg14(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(2),
      O => \axi_rdata[2]_i_11_n_0\
    );
\axi_rdata[2]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(2),
      I1 => slv_reg2(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(2),
      O => \axi_rdata[2]_i_12_n_0\
    );
\axi_rdata[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(2),
      I1 => slv_reg26(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(2),
      O => \axi_rdata[2]_i_6_n_0\
    );
\axi_rdata[2]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(2),
      I1 => slv_reg30(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(2),
      O => \axi_rdata[2]_i_7_n_0\
    );
\axi_rdata[2]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(2),
      I1 => slv_reg18(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(2),
      O => \axi_rdata[2]_i_8_n_0\
    );
\axi_rdata[2]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(2),
      I1 => slv_reg22(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(2),
      O => \axi_rdata[2]_i_9_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[30]_i_2_n_0\,
      I1 => \axi_rdata_reg[30]_i_3_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[30]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[30]_i_5_n_0\,
      O => reg_data_out(30)
    );
\axi_rdata[30]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(30),
      I1 => slv_reg14(30),
      I2 => sel0(1),
      I3 => slv_reg13(30),
      I4 => sel0(0),
      I5 => slv_reg12(30),
      O => \axi_rdata[30]_i_10_n_0\
    );
\axi_rdata[30]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(30),
      I1 => \slv_reg2__0\(30),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(30),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(30),
      O => \axi_rdata[30]_i_11_n_0\
    );
\axi_rdata[30]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[30]_i_10_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(30),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(30),
      O => \axi_rdata[30]_i_4_n_0\
    );
\axi_rdata[30]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(30),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(30),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[30]_i_11_n_0\,
      O => \axi_rdata[30]_i_5_n_0\
    );
\axi_rdata[30]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(30),
      I1 => slv_reg26(30),
      I2 => sel0(1),
      I3 => slv_reg25(30),
      I4 => sel0(0),
      I5 => slv_reg24(30),
      O => \axi_rdata[30]_i_6_n_0\
    );
\axi_rdata[30]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(30),
      I1 => slv_reg30(30),
      I2 => sel0(1),
      I3 => slv_reg29(30),
      I4 => sel0(0),
      I5 => slv_reg28(30),
      O => \axi_rdata[30]_i_7_n_0\
    );
\axi_rdata[30]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(30),
      I1 => slv_reg18(30),
      I2 => sel0(1),
      I3 => slv_reg17(30),
      I4 => sel0(0),
      I5 => slv_reg16(30),
      O => \axi_rdata[30]_i_8_n_0\
    );
\axi_rdata[30]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(30),
      I1 => slv_reg22(30),
      I2 => sel0(1),
      I3 => slv_reg21(30),
      I4 => sel0(0),
      I5 => slv_reg20(30),
      O => \axi_rdata[30]_i_9_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s00_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      O => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata[31]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(31),
      I1 => slv_reg22(31),
      I2 => sel0(1),
      I3 => slv_reg21(31),
      I4 => sel0(0),
      I5 => slv_reg20(31),
      O => \axi_rdata[31]_i_10_n_0\
    );
\axi_rdata[31]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(31),
      I1 => slv_reg14(31),
      I2 => sel0(1),
      I3 => slv_reg13(31),
      I4 => sel0(0),
      I5 => slv_reg12(31),
      O => \axi_rdata[31]_i_11_n_0\
    );
\axi_rdata[31]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(31),
      I1 => \slv_reg2__0\(31),
      I2 => sel0(1),
      I3 => \slv_reg1__0\(31),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(31),
      O => \axi_rdata[31]_i_12_n_0\
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[31]_i_3_n_0\,
      I1 => \axi_rdata_reg[31]_i_4_n_0\,
      I2 => sel0(4),
      I3 => \axi_rdata[31]_i_5_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata[31]_i_6_n_0\,
      O => reg_data_out(31)
    );
\axi_rdata[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[31]_i_11_n_0\,
      I1 => sel0(2),
      I2 => sel0(1),
      I3 => \slv_reg10__0\(31),
      I4 => sel0(0),
      I5 => \slv_reg11__0\(31),
      O => \axi_rdata[31]_i_5_n_0\
    );
\axi_rdata[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(31),
      I1 => sel0(0),
      I2 => \slv_reg5__0\(31),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[31]_i_12_n_0\,
      O => \axi_rdata[31]_i_6_n_0\
    );
\axi_rdata[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(31),
      I1 => slv_reg26(31),
      I2 => sel0(1),
      I3 => slv_reg25(31),
      I4 => sel0(0),
      I5 => slv_reg24(31),
      O => \axi_rdata[31]_i_7_n_0\
    );
\axi_rdata[31]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(31),
      I1 => slv_reg30(31),
      I2 => sel0(1),
      I3 => slv_reg29(31),
      I4 => sel0(0),
      I5 => slv_reg28(31),
      O => \axi_rdata[31]_i_8_n_0\
    );
\axi_rdata[31]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(31),
      I1 => slv_reg18(31),
      I2 => sel0(1),
      I3 => slv_reg17(31),
      I4 => sel0(0),
      I5 => slv_reg16(31),
      O => \axi_rdata[31]_i_9_n_0\
    );
\axi_rdata[3]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(3),
      I1 => slv_reg14(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(3),
      O => \axi_rdata[3]_i_10_n_0\
    );
\axi_rdata[3]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(3),
      I1 => slv_reg2(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(3),
      O => \axi_rdata[3]_i_11_n_0\
    );
\axi_rdata[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[3]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg10(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg11(3),
      O => \axi_rdata[3]_i_4_n_0\
    );
\axi_rdata[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(3),
      I1 => slv_reg26(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(3),
      O => \axi_rdata[3]_i_6_n_0\
    );
\axi_rdata[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(3),
      I1 => slv_reg30(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(3),
      O => \axi_rdata[3]_i_7_n_0\
    );
\axi_rdata[3]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(3),
      I1 => slv_reg18(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(3),
      O => \axi_rdata[3]_i_8_n_0\
    );
\axi_rdata[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(3),
      I1 => slv_reg22(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(3),
      O => \axi_rdata[3]_i_9_n_0\
    );
\axi_rdata[4]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(4),
      I1 => slv_reg14(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(4),
      O => \axi_rdata[4]_i_10_n_0\
    );
\axi_rdata[4]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(4),
      I1 => slv_reg2(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(4),
      O => \axi_rdata[4]_i_11_n_0\
    );
\axi_rdata[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[4]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg10(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg11(4),
      O => \axi_rdata[4]_i_4_n_0\
    );
\axi_rdata[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(4),
      I1 => slv_reg26(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(4),
      O => \axi_rdata[4]_i_6_n_0\
    );
\axi_rdata[4]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(4),
      I1 => slv_reg30(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(4),
      O => \axi_rdata[4]_i_7_n_0\
    );
\axi_rdata[4]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(4),
      I1 => slv_reg18(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(4),
      O => \axi_rdata[4]_i_8_n_0\
    );
\axi_rdata[4]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(4),
      I1 => slv_reg22(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(4),
      O => \axi_rdata[4]_i_9_n_0\
    );
\axi_rdata[5]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(5),
      I1 => slv_reg14(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(5),
      O => \axi_rdata[5]_i_10_n_0\
    );
\axi_rdata[5]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(5),
      I1 => slv_reg2(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(5),
      O => \axi_rdata[5]_i_11_n_0\
    );
\axi_rdata[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[5]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg10(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg11(5),
      O => \axi_rdata[5]_i_4_n_0\
    );
\axi_rdata[5]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(5),
      I1 => slv_reg26(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(5),
      O => \axi_rdata[5]_i_6_n_0\
    );
\axi_rdata[5]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(5),
      I1 => slv_reg30(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(5),
      O => \axi_rdata[5]_i_7_n_0\
    );
\axi_rdata[5]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(5),
      I1 => slv_reg18(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(5),
      O => \axi_rdata[5]_i_8_n_0\
    );
\axi_rdata[5]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(5),
      I1 => slv_reg22(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(5),
      O => \axi_rdata[5]_i_9_n_0\
    );
\axi_rdata[6]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(6),
      I1 => slv_reg14(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(6),
      O => \axi_rdata[6]_i_10_n_0\
    );
\axi_rdata[6]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(6),
      I1 => slv_reg2(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(6),
      O => \axi_rdata[6]_i_11_n_0\
    );
\axi_rdata[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[6]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg10(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg11(6),
      O => \axi_rdata[6]_i_4_n_0\
    );
\axi_rdata[6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(6),
      I1 => slv_reg26(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(6),
      O => \axi_rdata[6]_i_6_n_0\
    );
\axi_rdata[6]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(6),
      I1 => slv_reg30(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(6),
      O => \axi_rdata[6]_i_7_n_0\
    );
\axi_rdata[6]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(6),
      I1 => slv_reg18(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(6),
      O => \axi_rdata[6]_i_8_n_0\
    );
\axi_rdata[6]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(6),
      I1 => slv_reg22(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(6),
      O => \axi_rdata[6]_i_9_n_0\
    );
\axi_rdata[7]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(7),
      I1 => slv_reg14(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(7),
      O => \axi_rdata[7]_i_10_n_0\
    );
\axi_rdata[7]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(7),
      I1 => slv_reg2(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(7),
      O => \axi_rdata[7]_i_11_n_0\
    );
\axi_rdata[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[7]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg10(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg11(7),
      O => \axi_rdata[7]_i_4_n_0\
    );
\axi_rdata[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(7),
      I1 => slv_reg26(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(7),
      O => \axi_rdata[7]_i_6_n_0\
    );
\axi_rdata[7]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(7),
      I1 => slv_reg30(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(7),
      O => \axi_rdata[7]_i_7_n_0\
    );
\axi_rdata[7]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(7),
      I1 => slv_reg18(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(7),
      O => \axi_rdata[7]_i_8_n_0\
    );
\axi_rdata[7]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(7),
      I1 => slv_reg22(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(7),
      O => \axi_rdata[7]_i_9_n_0\
    );
\axi_rdata[8]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(8),
      I1 => slv_reg14(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(8),
      O => \axi_rdata[8]_i_10_n_0\
    );
\axi_rdata[8]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(8),
      I1 => slv_reg2(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(8),
      O => \axi_rdata[8]_i_11_n_0\
    );
\axi_rdata[8]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[8]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg10(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg11(8),
      O => \axi_rdata[8]_i_4_n_0\
    );
\axi_rdata[8]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(8),
      I1 => slv_reg26(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(8),
      O => \axi_rdata[8]_i_6_n_0\
    );
\axi_rdata[8]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(8),
      I1 => slv_reg30(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(8),
      O => \axi_rdata[8]_i_7_n_0\
    );
\axi_rdata[8]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(8),
      I1 => slv_reg18(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(8),
      O => \axi_rdata[8]_i_8_n_0\
    );
\axi_rdata[8]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(8),
      I1 => slv_reg22(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(8),
      O => \axi_rdata[8]_i_9_n_0\
    );
\axi_rdata[9]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg15(9),
      I1 => slv_reg14(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg13(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg12(9),
      O => \axi_rdata[9]_i_10_n_0\
    );
\axi_rdata[9]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_reg3__0\(9),
      I1 => slv_reg2(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg1(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg0(9),
      O => \axi_rdata[9]_i_11_n_0\
    );
\axi_rdata[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8888888B888"
    )
        port map (
      I0 => \axi_rdata[9]_i_10_n_0\,
      I1 => sel0(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg10(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg11(9),
      O => \axi_rdata[9]_i_4_n_0\
    );
\axi_rdata[9]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg27(9),
      I1 => slv_reg26(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg25(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg24(9),
      O => \axi_rdata[9]_i_6_n_0\
    );
\axi_rdata[9]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg31(9),
      I1 => slv_reg30(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg29(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg28(9),
      O => \axi_rdata[9]_i_7_n_0\
    );
\axi_rdata[9]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg19(9),
      I1 => slv_reg18(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg17(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg16(9),
      O => \axi_rdata[9]_i_8_n_0\
    );
\axi_rdata[9]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg23(9),
      I1 => slv_reg22(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => slv_reg21(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => slv_reg20(9),
      O => \axi_rdata[9]_i_9_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => SR(0)
    );
\axi_rdata_reg[0]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_6_n_0\,
      I1 => \axi_rdata[0]_i_7_n_0\,
      O => \axi_rdata_reg[0]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[0]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_8_n_0\,
      I1 => \axi_rdata[0]_i_9_n_0\,
      O => \axi_rdata_reg[0]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => SR(0)
    );
\axi_rdata_reg[10]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_6_n_0\,
      I1 => \axi_rdata[10]_i_7_n_0\,
      O => \axi_rdata_reg[10]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[10]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_8_n_0\,
      I1 => \axi_rdata[10]_i_9_n_0\,
      O => \axi_rdata_reg[10]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => SR(0)
    );
\axi_rdata_reg[11]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_6_n_0\,
      I1 => \axi_rdata[11]_i_7_n_0\,
      O => \axi_rdata_reg[11]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[11]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_8_n_0\,
      I1 => \axi_rdata[11]_i_9_n_0\,
      O => \axi_rdata_reg[11]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => SR(0)
    );
\axi_rdata_reg[12]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_6_n_0\,
      I1 => \axi_rdata[12]_i_7_n_0\,
      O => \axi_rdata_reg[12]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[12]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_8_n_0\,
      I1 => \axi_rdata[12]_i_9_n_0\,
      O => \axi_rdata_reg[12]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => SR(0)
    );
\axi_rdata_reg[13]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_6_n_0\,
      I1 => \axi_rdata[13]_i_7_n_0\,
      O => \axi_rdata_reg[13]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[13]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_8_n_0\,
      I1 => \axi_rdata[13]_i_9_n_0\,
      O => \axi_rdata_reg[13]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => SR(0)
    );
\axi_rdata_reg[14]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_6_n_0\,
      I1 => \axi_rdata[14]_i_7_n_0\,
      O => \axi_rdata_reg[14]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[14]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_8_n_0\,
      I1 => \axi_rdata[14]_i_9_n_0\,
      O => \axi_rdata_reg[14]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => SR(0)
    );
\axi_rdata_reg[15]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_6_n_0\,
      I1 => \axi_rdata[15]_i_7_n_0\,
      O => \axi_rdata_reg[15]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[15]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_8_n_0\,
      I1 => \axi_rdata[15]_i_9_n_0\,
      O => \axi_rdata_reg[15]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => SR(0)
    );
\axi_rdata_reg[16]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_6_n_0\,
      I1 => \axi_rdata[16]_i_7_n_0\,
      O => \axi_rdata_reg[16]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[16]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_8_n_0\,
      I1 => \axi_rdata[16]_i_9_n_0\,
      O => \axi_rdata_reg[16]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => SR(0)
    );
\axi_rdata_reg[17]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_6_n_0\,
      I1 => \axi_rdata[17]_i_7_n_0\,
      O => \axi_rdata_reg[17]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[17]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_8_n_0\,
      I1 => \axi_rdata[17]_i_9_n_0\,
      O => \axi_rdata_reg[17]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => SR(0)
    );
\axi_rdata_reg[18]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_6_n_0\,
      I1 => \axi_rdata[18]_i_7_n_0\,
      O => \axi_rdata_reg[18]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[18]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_8_n_0\,
      I1 => \axi_rdata[18]_i_9_n_0\,
      O => \axi_rdata_reg[18]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => SR(0)
    );
\axi_rdata_reg[19]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_6_n_0\,
      I1 => \axi_rdata[19]_i_7_n_0\,
      O => \axi_rdata_reg[19]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[19]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_8_n_0\,
      I1 => \axi_rdata[19]_i_9_n_0\,
      O => \axi_rdata_reg[19]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => SR(0)
    );
\axi_rdata_reg[1]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_6_n_0\,
      I1 => \axi_rdata[1]_i_7_n_0\,
      O => \axi_rdata_reg[1]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[1]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_8_n_0\,
      I1 => \axi_rdata[1]_i_9_n_0\,
      O => \axi_rdata_reg[1]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => SR(0)
    );
\axi_rdata_reg[20]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_6_n_0\,
      I1 => \axi_rdata[20]_i_7_n_0\,
      O => \axi_rdata_reg[20]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[20]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_8_n_0\,
      I1 => \axi_rdata[20]_i_9_n_0\,
      O => \axi_rdata_reg[20]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => SR(0)
    );
\axi_rdata_reg[21]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_6_n_0\,
      I1 => \axi_rdata[21]_i_7_n_0\,
      O => \axi_rdata_reg[21]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[21]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_8_n_0\,
      I1 => \axi_rdata[21]_i_9_n_0\,
      O => \axi_rdata_reg[21]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => SR(0)
    );
\axi_rdata_reg[22]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_6_n_0\,
      I1 => \axi_rdata[22]_i_7_n_0\,
      O => \axi_rdata_reg[22]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[22]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_8_n_0\,
      I1 => \axi_rdata[22]_i_9_n_0\,
      O => \axi_rdata_reg[22]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => SR(0)
    );
\axi_rdata_reg[23]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_6_n_0\,
      I1 => \axi_rdata[23]_i_7_n_0\,
      O => \axi_rdata_reg[23]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[23]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_8_n_0\,
      I1 => \axi_rdata[23]_i_9_n_0\,
      O => \axi_rdata_reg[23]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => SR(0)
    );
\axi_rdata_reg[24]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_6_n_0\,
      I1 => \axi_rdata[24]_i_7_n_0\,
      O => \axi_rdata_reg[24]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[24]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_8_n_0\,
      I1 => \axi_rdata[24]_i_9_n_0\,
      O => \axi_rdata_reg[24]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => SR(0)
    );
\axi_rdata_reg[25]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_6_n_0\,
      I1 => \axi_rdata[25]_i_7_n_0\,
      O => \axi_rdata_reg[25]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[25]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_8_n_0\,
      I1 => \axi_rdata[25]_i_9_n_0\,
      O => \axi_rdata_reg[25]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => SR(0)
    );
\axi_rdata_reg[26]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_6_n_0\,
      I1 => \axi_rdata[26]_i_7_n_0\,
      O => \axi_rdata_reg[26]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[26]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_8_n_0\,
      I1 => \axi_rdata[26]_i_9_n_0\,
      O => \axi_rdata_reg[26]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => SR(0)
    );
\axi_rdata_reg[27]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_6_n_0\,
      I1 => \axi_rdata[27]_i_7_n_0\,
      O => \axi_rdata_reg[27]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[27]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_8_n_0\,
      I1 => \axi_rdata[27]_i_9_n_0\,
      O => \axi_rdata_reg[27]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => SR(0)
    );
\axi_rdata_reg[28]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_6_n_0\,
      I1 => \axi_rdata[28]_i_7_n_0\,
      O => \axi_rdata_reg[28]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[28]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_8_n_0\,
      I1 => \axi_rdata[28]_i_9_n_0\,
      O => \axi_rdata_reg[28]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => SR(0)
    );
\axi_rdata_reg[29]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_6_n_0\,
      I1 => \axi_rdata[29]_i_7_n_0\,
      O => \axi_rdata_reg[29]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[29]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_8_n_0\,
      I1 => \axi_rdata[29]_i_9_n_0\,
      O => \axi_rdata_reg[29]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => SR(0)
    );
\axi_rdata_reg[2]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_6_n_0\,
      I1 => \axi_rdata[2]_i_7_n_0\,
      O => \axi_rdata_reg[2]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[2]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_8_n_0\,
      I1 => \axi_rdata[2]_i_9_n_0\,
      O => \axi_rdata_reg[2]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => SR(0)
    );
\axi_rdata_reg[30]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_6_n_0\,
      I1 => \axi_rdata[30]_i_7_n_0\,
      O => \axi_rdata_reg[30]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[30]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_8_n_0\,
      I1 => \axi_rdata[30]_i_9_n_0\,
      O => \axi_rdata_reg[30]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => SR(0)
    );
\axi_rdata_reg[31]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_7_n_0\,
      I1 => \axi_rdata[31]_i_8_n_0\,
      O => \axi_rdata_reg[31]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[31]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_9_n_0\,
      I1 => \axi_rdata[31]_i_10_n_0\,
      O => \axi_rdata_reg[31]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => SR(0)
    );
\axi_rdata_reg[3]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_6_n_0\,
      I1 => \axi_rdata[3]_i_7_n_0\,
      O => \axi_rdata_reg[3]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[3]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_8_n_0\,
      I1 => \axi_rdata[3]_i_9_n_0\,
      O => \axi_rdata_reg[3]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => SR(0)
    );
\axi_rdata_reg[4]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_6_n_0\,
      I1 => \axi_rdata[4]_i_7_n_0\,
      O => \axi_rdata_reg[4]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[4]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_8_n_0\,
      I1 => \axi_rdata[4]_i_9_n_0\,
      O => \axi_rdata_reg[4]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => SR(0)
    );
\axi_rdata_reg[5]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_6_n_0\,
      I1 => \axi_rdata[5]_i_7_n_0\,
      O => \axi_rdata_reg[5]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[5]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_8_n_0\,
      I1 => \axi_rdata[5]_i_9_n_0\,
      O => \axi_rdata_reg[5]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => SR(0)
    );
\axi_rdata_reg[6]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_6_n_0\,
      I1 => \axi_rdata[6]_i_7_n_0\,
      O => \axi_rdata_reg[6]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[6]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_8_n_0\,
      I1 => \axi_rdata[6]_i_9_n_0\,
      O => \axi_rdata_reg[6]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => SR(0)
    );
\axi_rdata_reg[7]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_6_n_0\,
      I1 => \axi_rdata[7]_i_7_n_0\,
      O => \axi_rdata_reg[7]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[7]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_8_n_0\,
      I1 => \axi_rdata[7]_i_9_n_0\,
      O => \axi_rdata_reg[7]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => SR(0)
    );
\axi_rdata_reg[8]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_6_n_0\,
      I1 => \axi_rdata[8]_i_7_n_0\,
      O => \axi_rdata_reg[8]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[8]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_8_n_0\,
      I1 => \axi_rdata[8]_i_9_n_0\,
      O => \axi_rdata_reg[8]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => SR(0)
    );
\axi_rdata_reg[9]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_6_n_0\,
      I1 => \axi_rdata[9]_i_7_n_0\,
      O => \axi_rdata_reg[9]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[9]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_8_n_0\,
      I1 => \axi_rdata[9]_i_9_n_0\,
      O => \axi_rdata_reg[9]_i_3_n_0\,
      S => sel0(2)
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready_reg_0,
      Q => \^s00_axi_rvalid\,
      R => SR(0)
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s00_axi_wready\,
      R => SR(0)
    );
control: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm
     port map (
      E(0) => \sampCounter/processQ0\,
      Q(0) => slv_reg4(0),
      SR(0) => control_n_0,
      WREN => WREN,
      clk => \^clk\,
      ready_sig_reg => \^clk_1\,
      reset_n => reset_n,
      reset_n_0(0) => RST,
      \slv_reg5_reg[0]\(0) => slv_reg5(0),
      state(1 downto 0) => state(1 downto 0),
      \state_reg[1]_0\ => datapath_n_13,
      sw(0) => sw(1)
    );
datapath: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath
     port map (
      D(15 downto 0) => reg_data_out(15 downto 0),
      E(0) => \sampCounter/processQ0\,
      Q(15 downto 1) => \slv_reg5__0\(15 downto 1),
      Q(0) => slv_reg5(0),
      \Q_reg[2]\(0) => sw(1),
      SR(0) => RST,
      WREN => WREN,
      ac_adc_sdata => ac_adc_sdata,
      ac_bclk => ac_bclk,
      ac_dac_sdata => ac_dac_sdata,
      ac_lrclk => ac_lrclk,
      ac_mclk => ac_mclk,
      \axi_araddr_reg[2]_rep\ => \axi_araddr_reg[2]_rep_n_0\,
      \axi_araddr_reg[3]_rep\ => \axi_araddr_reg[3]_rep_n_0\,
      \axi_araddr_reg[4]\ => \axi_rdata_reg[0]_i_2_n_0\,
      \axi_araddr_reg[4]_0\ => \axi_rdata_reg[0]_i_3_n_0\,
      \axi_araddr_reg[4]_1\ => \axi_rdata_reg[1]_i_2_n_0\,
      \axi_araddr_reg[4]_10\ => \axi_rdata_reg[4]_i_3_n_0\,
      \axi_araddr_reg[4]_11\ => \axi_rdata[4]_i_4_n_0\,
      \axi_araddr_reg[4]_12\ => \axi_rdata_reg[5]_i_2_n_0\,
      \axi_araddr_reg[4]_13\ => \axi_rdata_reg[5]_i_3_n_0\,
      \axi_araddr_reg[4]_14\ => \axi_rdata[5]_i_4_n_0\,
      \axi_araddr_reg[4]_15\ => \axi_rdata_reg[6]_i_2_n_0\,
      \axi_araddr_reg[4]_16\ => \axi_rdata_reg[6]_i_3_n_0\,
      \axi_araddr_reg[4]_17\ => \axi_rdata[6]_i_4_n_0\,
      \axi_araddr_reg[4]_18\ => \axi_rdata_reg[7]_i_2_n_0\,
      \axi_araddr_reg[4]_19\ => \axi_rdata_reg[7]_i_3_n_0\,
      \axi_araddr_reg[4]_2\ => \axi_rdata_reg[1]_i_3_n_0\,
      \axi_araddr_reg[4]_20\ => \axi_rdata[7]_i_4_n_0\,
      \axi_araddr_reg[4]_21\ => \axi_rdata_reg[8]_i_2_n_0\,
      \axi_araddr_reg[4]_22\ => \axi_rdata_reg[8]_i_3_n_0\,
      \axi_araddr_reg[4]_23\ => \axi_rdata[8]_i_4_n_0\,
      \axi_araddr_reg[4]_24\ => \axi_rdata_reg[9]_i_2_n_0\,
      \axi_araddr_reg[4]_25\ => \axi_rdata_reg[9]_i_3_n_0\,
      \axi_araddr_reg[4]_26\ => \axi_rdata[9]_i_4_n_0\,
      \axi_araddr_reg[4]_27\ => \axi_rdata_reg[10]_i_2_n_0\,
      \axi_araddr_reg[4]_28\ => \axi_rdata_reg[10]_i_3_n_0\,
      \axi_araddr_reg[4]_29\ => \axi_rdata[10]_i_4_n_0\,
      \axi_araddr_reg[4]_3\ => \axi_rdata[1]_i_4_n_0\,
      \axi_araddr_reg[4]_30\ => \axi_rdata_reg[11]_i_2_n_0\,
      \axi_araddr_reg[4]_31\ => \axi_rdata_reg[11]_i_3_n_0\,
      \axi_araddr_reg[4]_32\ => \axi_rdata[11]_i_4_n_0\,
      \axi_araddr_reg[4]_33\ => \axi_rdata_reg[12]_i_2_n_0\,
      \axi_araddr_reg[4]_34\ => \axi_rdata_reg[12]_i_3_n_0\,
      \axi_araddr_reg[4]_35\ => \axi_rdata[12]_i_4_n_0\,
      \axi_araddr_reg[4]_36\ => \axi_rdata_reg[13]_i_2_n_0\,
      \axi_araddr_reg[4]_37\ => \axi_rdata_reg[13]_i_3_n_0\,
      \axi_araddr_reg[4]_38\ => \axi_rdata[13]_i_4_n_0\,
      \axi_araddr_reg[4]_39\ => \axi_rdata_reg[14]_i_2_n_0\,
      \axi_araddr_reg[4]_4\ => \axi_rdata_reg[2]_i_2_n_0\,
      \axi_araddr_reg[4]_40\ => \axi_rdata_reg[14]_i_3_n_0\,
      \axi_araddr_reg[4]_41\ => \axi_rdata[14]_i_4_n_0\,
      \axi_araddr_reg[4]_42\ => \axi_rdata_reg[15]_i_2_n_0\,
      \axi_araddr_reg[4]_43\ => \axi_rdata_reg[15]_i_3_n_0\,
      \axi_araddr_reg[4]_44\ => \axi_rdata[15]_i_4_n_0\,
      \axi_araddr_reg[4]_5\ => \axi_rdata_reg[2]_i_3_n_0\,
      \axi_araddr_reg[4]_6\ => \axi_rdata_reg[3]_i_2_n_0\,
      \axi_araddr_reg[4]_7\ => \axi_rdata_reg[3]_i_3_n_0\,
      \axi_araddr_reg[4]_8\ => \axi_rdata[3]_i_4_n_0\,
      \axi_araddr_reg[4]_9\ => \axi_rdata_reg[4]_i_2_n_0\,
      \axi_araddr_reg[6]\(2 downto 0) => sel0(4 downto 2),
      btn(4 downto 0) => btn(4 downto 0),
      clk => \^clk\,
      ready => \^clk_1\,
      reset_n => reset_n,
      scl => scl,
      sda => sda,
      \slv_reg0_reg[9]\(9 downto 0) => slv_reg0(9 downto 0),
      \slv_reg10_reg[9]\(9 downto 0) => slv_reg10(9 downto 0),
      \slv_reg11_reg[9]\(9 downto 0) => slv_reg11(9 downto 0),
      \slv_reg15_reg[0]\ => \axi_rdata[0]_i_11_n_0\,
      \slv_reg15_reg[2]\ => \axi_rdata[2]_i_11_n_0\,
      \slv_reg1_reg[15]\(9 downto 0) => slv_reg1(15 downto 6),
      \slv_reg2_reg[15]\(9 downto 0) => slv_reg2(15 downto 6),
      \slv_reg3_reg[0]\ => \axi_rdata[0]_i_12_n_0\,
      \slv_reg3_reg[10]\ => \axi_rdata[10]_i_11_n_0\,
      \slv_reg3_reg[11]\ => \axi_rdata[11]_i_11_n_0\,
      \slv_reg3_reg[12]\ => \axi_rdata[12]_i_11_n_0\,
      \slv_reg3_reg[13]\ => \axi_rdata[13]_i_11_n_0\,
      \slv_reg3_reg[14]\ => \axi_rdata[14]_i_11_n_0\,
      \slv_reg3_reg[15]\ => \axi_rdata[15]_i_11_n_0\,
      \slv_reg3_reg[1]\ => \axi_rdata[1]_i_11_n_0\,
      \slv_reg3_reg[2]\ => \axi_rdata[2]_i_12_n_0\,
      \slv_reg3_reg[2]_0\(1) => slv_reg3(2),
      \slv_reg3_reg[2]_0\(0) => slv_reg3(0),
      \slv_reg3_reg[3]\ => \axi_rdata[3]_i_11_n_0\,
      \slv_reg3_reg[4]\ => \axi_rdata[4]_i_11_n_0\,
      \slv_reg3_reg[5]\ => \axi_rdata[5]_i_11_n_0\,
      \slv_reg3_reg[6]\ => \axi_rdata[6]_i_11_n_0\,
      \slv_reg3_reg[7]\ => \axi_rdata[7]_i_11_n_0\,
      \slv_reg3_reg[8]\ => \axi_rdata[8]_i_11_n_0\,
      \slv_reg3_reg[9]\ => \axi_rdata[9]_i_11_n_0\,
      \slv_reg4_reg[15]\(15 downto 1) => \slv_reg4__0\(15 downto 1),
      \slv_reg4_reg[15]\(0) => slv_reg4(0),
      state(1 downto 0) => state(1 downto 0),
      \state_reg[0]\ => datapath_n_13,
      \state_reg[1]\(0) => control_n_0,
      switch(3 downto 0) => switch(3 downto 0),
      tmds(3 downto 0) => tmds(3 downto 0),
      tmdsb(3 downto 0) => tmdsb(3 downto 0)
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg0[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => p_1_in(15)
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg0[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => p_1_in(23)
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg0[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => p_1_in(31)
    );
\slv_reg0[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg0[31]_i_2_n_0\
    );
\slv_reg0[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^s00_axi_wready\,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s00_axi_awready\,
      O => \slv_reg0[31]_i_3_n_0\
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg0[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => p_1_in(7)
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => slv_reg0(0),
      R => SR(0)
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => \slv_reg0__0\(10),
      R => SR(0)
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => \slv_reg0__0\(11),
      R => SR(0)
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => \slv_reg0__0\(12),
      R => SR(0)
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => \slv_reg0__0\(13),
      R => SR(0)
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => \slv_reg0__0\(14),
      R => SR(0)
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => \slv_reg0__0\(15),
      R => SR(0)
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => \slv_reg0__0\(16),
      R => SR(0)
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => \slv_reg0__0\(17),
      R => SR(0)
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => \slv_reg0__0\(18),
      R => SR(0)
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => \slv_reg0__0\(19),
      R => SR(0)
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => slv_reg0(1),
      R => SR(0)
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => \slv_reg0__0\(20),
      R => SR(0)
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => \slv_reg0__0\(21),
      R => SR(0)
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => \slv_reg0__0\(22),
      R => SR(0)
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => \slv_reg0__0\(23),
      R => SR(0)
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => \slv_reg0__0\(24),
      R => SR(0)
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => \slv_reg0__0\(25),
      R => SR(0)
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => \slv_reg0__0\(26),
      R => SR(0)
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => \slv_reg0__0\(27),
      R => SR(0)
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => \slv_reg0__0\(28),
      R => SR(0)
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => \slv_reg0__0\(29),
      R => SR(0)
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => slv_reg0(2),
      R => SR(0)
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => \slv_reg0__0\(30),
      R => SR(0)
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => \slv_reg0__0\(31),
      R => SR(0)
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => slv_reg0(3),
      R => SR(0)
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => slv_reg0(4),
      R => SR(0)
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => slv_reg0(5),
      R => SR(0)
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => slv_reg0(6),
      R => SR(0)
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => slv_reg0(7),
      R => SR(0)
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => slv_reg0(8),
      R => SR(0)
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => slv_reg0(9),
      R => SR(0)
    );
\slv_reg10[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg10[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg10[15]_i_1_n_0\
    );
\slv_reg10[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg10[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg10[23]_i_1_n_0\
    );
\slv_reg10[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg10[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg10[31]_i_1_n_0\
    );
\slv_reg10[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000400000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg10[31]_i_2_n_0\
    );
\slv_reg10[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg10[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg10[7]_i_1_n_0\
    );
\slv_reg10_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg10(0),
      R => SR(0)
    );
\slv_reg10_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg10__0\(10),
      R => SR(0)
    );
\slv_reg10_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg10__0\(11),
      R => SR(0)
    );
\slv_reg10_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg10__0\(12),
      R => SR(0)
    );
\slv_reg10_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg10__0\(13),
      R => SR(0)
    );
\slv_reg10_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg10__0\(14),
      R => SR(0)
    );
\slv_reg10_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg10__0\(15),
      R => SR(0)
    );
\slv_reg10_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg10__0\(16),
      R => SR(0)
    );
\slv_reg10_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg10__0\(17),
      R => SR(0)
    );
\slv_reg10_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg10__0\(18),
      R => SR(0)
    );
\slv_reg10_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg10__0\(19),
      R => SR(0)
    );
\slv_reg10_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg10(1),
      R => SR(0)
    );
\slv_reg10_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg10__0\(20),
      R => SR(0)
    );
\slv_reg10_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg10__0\(21),
      R => SR(0)
    );
\slv_reg10_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg10__0\(22),
      R => SR(0)
    );
\slv_reg10_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg10__0\(23),
      R => SR(0)
    );
\slv_reg10_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg10__0\(24),
      R => SR(0)
    );
\slv_reg10_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg10__0\(25),
      R => SR(0)
    );
\slv_reg10_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg10__0\(26),
      R => SR(0)
    );
\slv_reg10_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg10__0\(27),
      R => SR(0)
    );
\slv_reg10_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg10__0\(28),
      R => SR(0)
    );
\slv_reg10_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg10__0\(29),
      R => SR(0)
    );
\slv_reg10_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg10(2),
      R => SR(0)
    );
\slv_reg10_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg10__0\(30),
      R => SR(0)
    );
\slv_reg10_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg10__0\(31),
      R => SR(0)
    );
\slv_reg10_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg10(3),
      R => SR(0)
    );
\slv_reg10_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg10(4),
      R => SR(0)
    );
\slv_reg10_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg10(5),
      R => SR(0)
    );
\slv_reg10_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg10(6),
      R => SR(0)
    );
\slv_reg10_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg10(7),
      R => SR(0)
    );
\slv_reg10_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg10(8),
      R => SR(0)
    );
\slv_reg10_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg10[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg10(9),
      R => SR(0)
    );
\slv_reg11[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg11[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg11[15]_i_1_n_0\
    );
\slv_reg11[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg11[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg11[23]_i_1_n_0\
    );
\slv_reg11[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg11[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg11[31]_i_1_n_0\
    );
\slv_reg11[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg11[31]_i_2_n_0\
    );
\slv_reg11[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg11[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg11[7]_i_1_n_0\
    );
\slv_reg11_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg11(0),
      R => SR(0)
    );
\slv_reg11_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg11__0\(10),
      R => SR(0)
    );
\slv_reg11_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg11__0\(11),
      R => SR(0)
    );
\slv_reg11_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg11__0\(12),
      R => SR(0)
    );
\slv_reg11_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg11__0\(13),
      R => SR(0)
    );
\slv_reg11_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg11__0\(14),
      R => SR(0)
    );
\slv_reg11_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg11__0\(15),
      R => SR(0)
    );
\slv_reg11_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg11__0\(16),
      R => SR(0)
    );
\slv_reg11_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg11__0\(17),
      R => SR(0)
    );
\slv_reg11_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg11__0\(18),
      R => SR(0)
    );
\slv_reg11_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg11__0\(19),
      R => SR(0)
    );
\slv_reg11_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg11(1),
      R => SR(0)
    );
\slv_reg11_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg11__0\(20),
      R => SR(0)
    );
\slv_reg11_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg11__0\(21),
      R => SR(0)
    );
\slv_reg11_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg11__0\(22),
      R => SR(0)
    );
\slv_reg11_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg11__0\(23),
      R => SR(0)
    );
\slv_reg11_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg11__0\(24),
      R => SR(0)
    );
\slv_reg11_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg11__0\(25),
      R => SR(0)
    );
\slv_reg11_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg11__0\(26),
      R => SR(0)
    );
\slv_reg11_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg11__0\(27),
      R => SR(0)
    );
\slv_reg11_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg11__0\(28),
      R => SR(0)
    );
\slv_reg11_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg11__0\(29),
      R => SR(0)
    );
\slv_reg11_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg11(2),
      R => SR(0)
    );
\slv_reg11_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg11__0\(30),
      R => SR(0)
    );
\slv_reg11_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg11__0\(31),
      R => SR(0)
    );
\slv_reg11_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg11(3),
      R => SR(0)
    );
\slv_reg11_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg11(4),
      R => SR(0)
    );
\slv_reg11_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg11(5),
      R => SR(0)
    );
\slv_reg11_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg11(6),
      R => SR(0)
    );
\slv_reg11_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg11(7),
      R => SR(0)
    );
\slv_reg11_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg11(8),
      R => SR(0)
    );
\slv_reg11_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg11[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg11(9),
      R => SR(0)
    );
\slv_reg12[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg12[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg12[15]_i_1_n_0\
    );
\slv_reg12[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg12[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg12[23]_i_1_n_0\
    );
\slv_reg12[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg12[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg12[31]_i_1_n_0\
    );
\slv_reg12[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000004000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg12[31]_i_2_n_0\
    );
\slv_reg12[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg12[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg12[7]_i_1_n_0\
    );
\slv_reg12_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg12(0),
      R => SR(0)
    );
\slv_reg12_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg12(10),
      R => SR(0)
    );
\slv_reg12_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg12(11),
      R => SR(0)
    );
\slv_reg12_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg12(12),
      R => SR(0)
    );
\slv_reg12_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg12(13),
      R => SR(0)
    );
\slv_reg12_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg12(14),
      R => SR(0)
    );
\slv_reg12_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg12(15),
      R => SR(0)
    );
\slv_reg12_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg12(16),
      R => SR(0)
    );
\slv_reg12_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg12(17),
      R => SR(0)
    );
\slv_reg12_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg12(18),
      R => SR(0)
    );
\slv_reg12_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg12(19),
      R => SR(0)
    );
\slv_reg12_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg12(1),
      R => SR(0)
    );
\slv_reg12_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg12(20),
      R => SR(0)
    );
\slv_reg12_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg12(21),
      R => SR(0)
    );
\slv_reg12_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg12(22),
      R => SR(0)
    );
\slv_reg12_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg12(23),
      R => SR(0)
    );
\slv_reg12_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg12(24),
      R => SR(0)
    );
\slv_reg12_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg12(25),
      R => SR(0)
    );
\slv_reg12_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg12(26),
      R => SR(0)
    );
\slv_reg12_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg12(27),
      R => SR(0)
    );
\slv_reg12_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg12(28),
      R => SR(0)
    );
\slv_reg12_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg12(29),
      R => SR(0)
    );
\slv_reg12_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg12(2),
      R => SR(0)
    );
\slv_reg12_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg12(30),
      R => SR(0)
    );
\slv_reg12_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg12(31),
      R => SR(0)
    );
\slv_reg12_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg12(3),
      R => SR(0)
    );
\slv_reg12_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg12(4),
      R => SR(0)
    );
\slv_reg12_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg12(5),
      R => SR(0)
    );
\slv_reg12_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg12(6),
      R => SR(0)
    );
\slv_reg12_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg12(7),
      R => SR(0)
    );
\slv_reg12_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg12(8),
      R => SR(0)
    );
\slv_reg12_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg12[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg12(9),
      R => SR(0)
    );
\slv_reg13[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg13[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg13[15]_i_1_n_0\
    );
\slv_reg13[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg13[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg13[23]_i_1_n_0\
    );
\slv_reg13[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg13[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg13[31]_i_1_n_0\
    );
\slv_reg13[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000400000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg13[31]_i_2_n_0\
    );
\slv_reg13[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg13[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg13[7]_i_1_n_0\
    );
\slv_reg13_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg13(0),
      R => SR(0)
    );
\slv_reg13_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg13(10),
      R => SR(0)
    );
\slv_reg13_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg13(11),
      R => SR(0)
    );
\slv_reg13_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg13(12),
      R => SR(0)
    );
\slv_reg13_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg13(13),
      R => SR(0)
    );
\slv_reg13_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg13(14),
      R => SR(0)
    );
\slv_reg13_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg13(15),
      R => SR(0)
    );
\slv_reg13_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg13(16),
      R => SR(0)
    );
\slv_reg13_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg13(17),
      R => SR(0)
    );
\slv_reg13_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg13(18),
      R => SR(0)
    );
\slv_reg13_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg13(19),
      R => SR(0)
    );
\slv_reg13_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg13(1),
      R => SR(0)
    );
\slv_reg13_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg13(20),
      R => SR(0)
    );
\slv_reg13_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg13(21),
      R => SR(0)
    );
\slv_reg13_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg13(22),
      R => SR(0)
    );
\slv_reg13_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg13(23),
      R => SR(0)
    );
\slv_reg13_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg13(24),
      R => SR(0)
    );
\slv_reg13_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg13(25),
      R => SR(0)
    );
\slv_reg13_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg13(26),
      R => SR(0)
    );
\slv_reg13_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg13(27),
      R => SR(0)
    );
\slv_reg13_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg13(28),
      R => SR(0)
    );
\slv_reg13_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg13(29),
      R => SR(0)
    );
\slv_reg13_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg13(2),
      R => SR(0)
    );
\slv_reg13_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg13(30),
      R => SR(0)
    );
\slv_reg13_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg13(31),
      R => SR(0)
    );
\slv_reg13_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg13(3),
      R => SR(0)
    );
\slv_reg13_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg13(4),
      R => SR(0)
    );
\slv_reg13_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg13(5),
      R => SR(0)
    );
\slv_reg13_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg13(6),
      R => SR(0)
    );
\slv_reg13_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg13(7),
      R => SR(0)
    );
\slv_reg13_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg13(8),
      R => SR(0)
    );
\slv_reg13_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg13[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg13(9),
      R => SR(0)
    );
\slv_reg14[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg14[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg14[15]_i_1_n_0\
    );
\slv_reg14[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg14[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg14[23]_i_1_n_0\
    );
\slv_reg14[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg14[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg14[31]_i_1_n_0\
    );
\slv_reg14[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg14[31]_i_2_n_0\
    );
\slv_reg14[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg14[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg14[7]_i_1_n_0\
    );
\slv_reg14_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg14(0),
      R => SR(0)
    );
\slv_reg14_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg14(10),
      R => SR(0)
    );
\slv_reg14_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg14(11),
      R => SR(0)
    );
\slv_reg14_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg14(12),
      R => SR(0)
    );
\slv_reg14_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg14(13),
      R => SR(0)
    );
\slv_reg14_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg14(14),
      R => SR(0)
    );
\slv_reg14_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg14(15),
      R => SR(0)
    );
\slv_reg14_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg14(16),
      R => SR(0)
    );
\slv_reg14_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg14(17),
      R => SR(0)
    );
\slv_reg14_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg14(18),
      R => SR(0)
    );
\slv_reg14_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg14(19),
      R => SR(0)
    );
\slv_reg14_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg14(1),
      R => SR(0)
    );
\slv_reg14_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg14(20),
      R => SR(0)
    );
\slv_reg14_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg14(21),
      R => SR(0)
    );
\slv_reg14_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg14(22),
      R => SR(0)
    );
\slv_reg14_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg14(23),
      R => SR(0)
    );
\slv_reg14_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg14(24),
      R => SR(0)
    );
\slv_reg14_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg14(25),
      R => SR(0)
    );
\slv_reg14_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg14(26),
      R => SR(0)
    );
\slv_reg14_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg14(27),
      R => SR(0)
    );
\slv_reg14_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg14(28),
      R => SR(0)
    );
\slv_reg14_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg14(29),
      R => SR(0)
    );
\slv_reg14_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg14(2),
      R => SR(0)
    );
\slv_reg14_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg14(30),
      R => SR(0)
    );
\slv_reg14_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg14(31),
      R => SR(0)
    );
\slv_reg14_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg14(3),
      R => SR(0)
    );
\slv_reg14_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg14(4),
      R => SR(0)
    );
\slv_reg14_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg14(5),
      R => SR(0)
    );
\slv_reg14_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg14(6),
      R => SR(0)
    );
\slv_reg14_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg14(7),
      R => SR(0)
    );
\slv_reg14_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg14(8),
      R => SR(0)
    );
\slv_reg14_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg14[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg14(9),
      R => SR(0)
    );
\slv_reg15[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg15[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg15[15]_i_1_n_0\
    );
\slv_reg15[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg15[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg15[23]_i_1_n_0\
    );
\slv_reg15[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg15[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg15[31]_i_1_n_0\
    );
\slv_reg15[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg15[31]_i_2_n_0\
    );
\slv_reg15[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg15[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg15[7]_i_1_n_0\
    );
\slv_reg15_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg15(0),
      R => SR(0)
    );
\slv_reg15_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg15(10),
      R => SR(0)
    );
\slv_reg15_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg15(11),
      R => SR(0)
    );
\slv_reg15_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg15(12),
      R => SR(0)
    );
\slv_reg15_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg15(13),
      R => SR(0)
    );
\slv_reg15_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg15(14),
      R => SR(0)
    );
\slv_reg15_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg15(15),
      R => SR(0)
    );
\slv_reg15_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg15(16),
      R => SR(0)
    );
\slv_reg15_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg15(17),
      R => SR(0)
    );
\slv_reg15_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg15(18),
      R => SR(0)
    );
\slv_reg15_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg15(19),
      R => SR(0)
    );
\slv_reg15_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg15(1),
      R => SR(0)
    );
\slv_reg15_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg15(20),
      R => SR(0)
    );
\slv_reg15_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg15(21),
      R => SR(0)
    );
\slv_reg15_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg15(22),
      R => SR(0)
    );
\slv_reg15_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg15(23),
      R => SR(0)
    );
\slv_reg15_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg15(24),
      R => SR(0)
    );
\slv_reg15_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg15(25),
      R => SR(0)
    );
\slv_reg15_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg15(26),
      R => SR(0)
    );
\slv_reg15_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg15(27),
      R => SR(0)
    );
\slv_reg15_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg15(28),
      R => SR(0)
    );
\slv_reg15_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg15(29),
      R => SR(0)
    );
\slv_reg15_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg15(2),
      R => SR(0)
    );
\slv_reg15_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg15(30),
      R => SR(0)
    );
\slv_reg15_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg15(31),
      R => SR(0)
    );
\slv_reg15_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg15(3),
      R => SR(0)
    );
\slv_reg15_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg15(4),
      R => SR(0)
    );
\slv_reg15_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg15(5),
      R => SR(0)
    );
\slv_reg15_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg15(6),
      R => SR(0)
    );
\slv_reg15_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg15(7),
      R => SR(0)
    );
\slv_reg15_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg15(8),
      R => SR(0)
    );
\slv_reg15_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg15[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg15(9),
      R => SR(0)
    );
\slv_reg16[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg16[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg16[15]_i_1_n_0\
    );
\slv_reg16[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg16[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg16[23]_i_1_n_0\
    );
\slv_reg16[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg16[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg16[31]_i_1_n_0\
    );
\slv_reg16[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg16[31]_i_2_n_0\
    );
\slv_reg16[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg16[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg16[7]_i_1_n_0\
    );
\slv_reg16_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg16(0),
      R => SR(0)
    );
\slv_reg16_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg16(10),
      R => SR(0)
    );
\slv_reg16_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg16(11),
      R => SR(0)
    );
\slv_reg16_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg16(12),
      R => SR(0)
    );
\slv_reg16_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg16(13),
      R => SR(0)
    );
\slv_reg16_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg16(14),
      R => SR(0)
    );
\slv_reg16_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg16(15),
      R => SR(0)
    );
\slv_reg16_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg16(16),
      R => SR(0)
    );
\slv_reg16_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg16(17),
      R => SR(0)
    );
\slv_reg16_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg16(18),
      R => SR(0)
    );
\slv_reg16_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg16(19),
      R => SR(0)
    );
\slv_reg16_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg16(1),
      R => SR(0)
    );
\slv_reg16_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg16(20),
      R => SR(0)
    );
\slv_reg16_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg16(21),
      R => SR(0)
    );
\slv_reg16_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg16(22),
      R => SR(0)
    );
\slv_reg16_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg16(23),
      R => SR(0)
    );
\slv_reg16_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg16(24),
      R => SR(0)
    );
\slv_reg16_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg16(25),
      R => SR(0)
    );
\slv_reg16_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg16(26),
      R => SR(0)
    );
\slv_reg16_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg16(27),
      R => SR(0)
    );
\slv_reg16_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg16(28),
      R => SR(0)
    );
\slv_reg16_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg16(29),
      R => SR(0)
    );
\slv_reg16_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg16(2),
      R => SR(0)
    );
\slv_reg16_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg16(30),
      R => SR(0)
    );
\slv_reg16_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg16(31),
      R => SR(0)
    );
\slv_reg16_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg16(3),
      R => SR(0)
    );
\slv_reg16_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg16(4),
      R => SR(0)
    );
\slv_reg16_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg16(5),
      R => SR(0)
    );
\slv_reg16_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg16(6),
      R => SR(0)
    );
\slv_reg16_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg16(7),
      R => SR(0)
    );
\slv_reg16_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg16(8),
      R => SR(0)
    );
\slv_reg16_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg16[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg16(9),
      R => SR(0)
    );
\slv_reg17[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg17[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg17[15]_i_1_n_0\
    );
\slv_reg17[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg17[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg17[23]_i_1_n_0\
    );
\slv_reg17[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg17[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg17[31]_i_1_n_0\
    );
\slv_reg17[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg17[31]_i_2_n_0\
    );
\slv_reg17[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg17[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg17[7]_i_1_n_0\
    );
\slv_reg17_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg17(0),
      R => SR(0)
    );
\slv_reg17_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg17(10),
      R => SR(0)
    );
\slv_reg17_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg17(11),
      R => SR(0)
    );
\slv_reg17_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg17(12),
      R => SR(0)
    );
\slv_reg17_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg17(13),
      R => SR(0)
    );
\slv_reg17_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg17(14),
      R => SR(0)
    );
\slv_reg17_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg17(15),
      R => SR(0)
    );
\slv_reg17_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg17(16),
      R => SR(0)
    );
\slv_reg17_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg17(17),
      R => SR(0)
    );
\slv_reg17_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg17(18),
      R => SR(0)
    );
\slv_reg17_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg17(19),
      R => SR(0)
    );
\slv_reg17_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg17(1),
      R => SR(0)
    );
\slv_reg17_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg17(20),
      R => SR(0)
    );
\slv_reg17_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg17(21),
      R => SR(0)
    );
\slv_reg17_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg17(22),
      R => SR(0)
    );
\slv_reg17_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg17(23),
      R => SR(0)
    );
\slv_reg17_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg17(24),
      R => SR(0)
    );
\slv_reg17_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg17(25),
      R => SR(0)
    );
\slv_reg17_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg17(26),
      R => SR(0)
    );
\slv_reg17_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg17(27),
      R => SR(0)
    );
\slv_reg17_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg17(28),
      R => SR(0)
    );
\slv_reg17_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg17(29),
      R => SR(0)
    );
\slv_reg17_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg17(2),
      R => SR(0)
    );
\slv_reg17_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg17(30),
      R => SR(0)
    );
\slv_reg17_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg17(31),
      R => SR(0)
    );
\slv_reg17_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg17(3),
      R => SR(0)
    );
\slv_reg17_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg17(4),
      R => SR(0)
    );
\slv_reg17_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg17(5),
      R => SR(0)
    );
\slv_reg17_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg17(6),
      R => SR(0)
    );
\slv_reg17_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg17(7),
      R => SR(0)
    );
\slv_reg17_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg17(8),
      R => SR(0)
    );
\slv_reg17_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg17[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg17(9),
      R => SR(0)
    );
\slv_reg18[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg18[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg18[15]_i_1_n_0\
    );
\slv_reg18[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg18[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg18[23]_i_1_n_0\
    );
\slv_reg18[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg18[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg18[31]_i_1_n_0\
    );
\slv_reg18[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000200000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg18[31]_i_2_n_0\
    );
\slv_reg18[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg18[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg18[7]_i_1_n_0\
    );
\slv_reg18_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg18(0),
      R => SR(0)
    );
\slv_reg18_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg18(10),
      R => SR(0)
    );
\slv_reg18_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg18(11),
      R => SR(0)
    );
\slv_reg18_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg18(12),
      R => SR(0)
    );
\slv_reg18_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg18(13),
      R => SR(0)
    );
\slv_reg18_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg18(14),
      R => SR(0)
    );
\slv_reg18_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg18(15),
      R => SR(0)
    );
\slv_reg18_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg18(16),
      R => SR(0)
    );
\slv_reg18_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg18(17),
      R => SR(0)
    );
\slv_reg18_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg18(18),
      R => SR(0)
    );
\slv_reg18_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg18(19),
      R => SR(0)
    );
\slv_reg18_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg18(1),
      R => SR(0)
    );
\slv_reg18_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg18(20),
      R => SR(0)
    );
\slv_reg18_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg18(21),
      R => SR(0)
    );
\slv_reg18_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg18(22),
      R => SR(0)
    );
\slv_reg18_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg18(23),
      R => SR(0)
    );
\slv_reg18_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg18(24),
      R => SR(0)
    );
\slv_reg18_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg18(25),
      R => SR(0)
    );
\slv_reg18_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg18(26),
      R => SR(0)
    );
\slv_reg18_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg18(27),
      R => SR(0)
    );
\slv_reg18_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg18(28),
      R => SR(0)
    );
\slv_reg18_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg18(29),
      R => SR(0)
    );
\slv_reg18_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg18(2),
      R => SR(0)
    );
\slv_reg18_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg18(30),
      R => SR(0)
    );
\slv_reg18_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg18(31),
      R => SR(0)
    );
\slv_reg18_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg18(3),
      R => SR(0)
    );
\slv_reg18_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg18(4),
      R => SR(0)
    );
\slv_reg18_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg18(5),
      R => SR(0)
    );
\slv_reg18_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg18(6),
      R => SR(0)
    );
\slv_reg18_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg18(7),
      R => SR(0)
    );
\slv_reg18_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg18(8),
      R => SR(0)
    );
\slv_reg18_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg18[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg18(9),
      R => SR(0)
    );
\slv_reg19[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg19[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg19[15]_i_1_n_0\
    );
\slv_reg19[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg19[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg19[23]_i_1_n_0\
    );
\slv_reg19[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg19[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg19[31]_i_1_n_0\
    );
\slv_reg19[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg19[31]_i_2_n_0\
    );
\slv_reg19[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg19[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg19[7]_i_1_n_0\
    );
\slv_reg19_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg19(0),
      R => SR(0)
    );
\slv_reg19_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg19(10),
      R => SR(0)
    );
\slv_reg19_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg19(11),
      R => SR(0)
    );
\slv_reg19_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg19(12),
      R => SR(0)
    );
\slv_reg19_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg19(13),
      R => SR(0)
    );
\slv_reg19_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg19(14),
      R => SR(0)
    );
\slv_reg19_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg19(15),
      R => SR(0)
    );
\slv_reg19_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg19(16),
      R => SR(0)
    );
\slv_reg19_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg19(17),
      R => SR(0)
    );
\slv_reg19_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg19(18),
      R => SR(0)
    );
\slv_reg19_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg19(19),
      R => SR(0)
    );
\slv_reg19_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg19(1),
      R => SR(0)
    );
\slv_reg19_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg19(20),
      R => SR(0)
    );
\slv_reg19_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg19(21),
      R => SR(0)
    );
\slv_reg19_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg19(22),
      R => SR(0)
    );
\slv_reg19_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg19(23),
      R => SR(0)
    );
\slv_reg19_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg19(24),
      R => SR(0)
    );
\slv_reg19_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg19(25),
      R => SR(0)
    );
\slv_reg19_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg19(26),
      R => SR(0)
    );
\slv_reg19_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg19(27),
      R => SR(0)
    );
\slv_reg19_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg19(28),
      R => SR(0)
    );
\slv_reg19_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg19(29),
      R => SR(0)
    );
\slv_reg19_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg19(2),
      R => SR(0)
    );
\slv_reg19_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg19(30),
      R => SR(0)
    );
\slv_reg19_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg19(31),
      R => SR(0)
    );
\slv_reg19_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg19(3),
      R => SR(0)
    );
\slv_reg19_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg19(4),
      R => SR(0)
    );
\slv_reg19_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg19(5),
      R => SR(0)
    );
\slv_reg19_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg19(6),
      R => SR(0)
    );
\slv_reg19_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg19(7),
      R => SR(0)
    );
\slv_reg19_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg19(8),
      R => SR(0)
    );
\slv_reg19_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg19[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg19(9),
      R => SR(0)
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg1[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg1[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg1[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000001000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg1[31]_i_2_n_0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg1[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg1(0),
      R => SR(0)
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg1(10),
      R => SR(0)
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg1(11),
      R => SR(0)
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg1(12),
      R => SR(0)
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg1(13),
      R => SR(0)
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg1(14),
      R => SR(0)
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg1(15),
      R => SR(0)
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg1__0\(16),
      R => SR(0)
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg1__0\(17),
      R => SR(0)
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg1__0\(18),
      R => SR(0)
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg1__0\(19),
      R => SR(0)
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg1(1),
      R => SR(0)
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg1__0\(20),
      R => SR(0)
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg1__0\(21),
      R => SR(0)
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg1__0\(22),
      R => SR(0)
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg1__0\(23),
      R => SR(0)
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg1__0\(24),
      R => SR(0)
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg1__0\(25),
      R => SR(0)
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg1__0\(26),
      R => SR(0)
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg1__0\(27),
      R => SR(0)
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg1__0\(28),
      R => SR(0)
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg1__0\(29),
      R => SR(0)
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg1(2),
      R => SR(0)
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg1__0\(30),
      R => SR(0)
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg1__0\(31),
      R => SR(0)
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg1(3),
      R => SR(0)
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg1(4),
      R => SR(0)
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg1(5),
      R => SR(0)
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg1(6),
      R => SR(0)
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg1(7),
      R => SR(0)
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg1(8),
      R => SR(0)
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg1(9),
      R => SR(0)
    );
\slv_reg20[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg20[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg20[15]_i_1_n_0\
    );
\slv_reg20[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg20[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg20[23]_i_1_n_0\
    );
\slv_reg20[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg20[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg20[31]_i_1_n_0\
    );
\slv_reg20[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000002000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg20[31]_i_2_n_0\
    );
\slv_reg20[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg20[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg20[7]_i_1_n_0\
    );
\slv_reg20_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg20(0),
      R => SR(0)
    );
\slv_reg20_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg20(10),
      R => SR(0)
    );
\slv_reg20_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg20(11),
      R => SR(0)
    );
\slv_reg20_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg20(12),
      R => SR(0)
    );
\slv_reg20_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg20(13),
      R => SR(0)
    );
\slv_reg20_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg20(14),
      R => SR(0)
    );
\slv_reg20_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg20(15),
      R => SR(0)
    );
\slv_reg20_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg20(16),
      R => SR(0)
    );
\slv_reg20_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg20(17),
      R => SR(0)
    );
\slv_reg20_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg20(18),
      R => SR(0)
    );
\slv_reg20_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg20(19),
      R => SR(0)
    );
\slv_reg20_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg20(1),
      R => SR(0)
    );
\slv_reg20_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg20(20),
      R => SR(0)
    );
\slv_reg20_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg20(21),
      R => SR(0)
    );
\slv_reg20_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg20(22),
      R => SR(0)
    );
\slv_reg20_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg20(23),
      R => SR(0)
    );
\slv_reg20_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg20(24),
      R => SR(0)
    );
\slv_reg20_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg20(25),
      R => SR(0)
    );
\slv_reg20_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg20(26),
      R => SR(0)
    );
\slv_reg20_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg20(27),
      R => SR(0)
    );
\slv_reg20_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg20(28),
      R => SR(0)
    );
\slv_reg20_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg20(29),
      R => SR(0)
    );
\slv_reg20_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg20(2),
      R => SR(0)
    );
\slv_reg20_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg20(30),
      R => SR(0)
    );
\slv_reg20_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg20(31),
      R => SR(0)
    );
\slv_reg20_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg20(3),
      R => SR(0)
    );
\slv_reg20_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg20(4),
      R => SR(0)
    );
\slv_reg20_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg20(5),
      R => SR(0)
    );
\slv_reg20_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg20(6),
      R => SR(0)
    );
\slv_reg20_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg20(7),
      R => SR(0)
    );
\slv_reg20_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg20(8),
      R => SR(0)
    );
\slv_reg20_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg20[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg20(9),
      R => SR(0)
    );
\slv_reg21[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg21[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg21[15]_i_1_n_0\
    );
\slv_reg21[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg21[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg21[23]_i_1_n_0\
    );
\slv_reg21[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg21[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg21[31]_i_1_n_0\
    );
\slv_reg21[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg21[31]_i_2_n_0\
    );
\slv_reg21[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg21[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg21[7]_i_1_n_0\
    );
\slv_reg21_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg21(0),
      R => SR(0)
    );
\slv_reg21_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg21(10),
      R => SR(0)
    );
\slv_reg21_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg21(11),
      R => SR(0)
    );
\slv_reg21_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg21(12),
      R => SR(0)
    );
\slv_reg21_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg21(13),
      R => SR(0)
    );
\slv_reg21_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg21(14),
      R => SR(0)
    );
\slv_reg21_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg21(15),
      R => SR(0)
    );
\slv_reg21_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg21(16),
      R => SR(0)
    );
\slv_reg21_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg21(17),
      R => SR(0)
    );
\slv_reg21_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg21(18),
      R => SR(0)
    );
\slv_reg21_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg21(19),
      R => SR(0)
    );
\slv_reg21_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg21(1),
      R => SR(0)
    );
\slv_reg21_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg21(20),
      R => SR(0)
    );
\slv_reg21_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg21(21),
      R => SR(0)
    );
\slv_reg21_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg21(22),
      R => SR(0)
    );
\slv_reg21_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg21(23),
      R => SR(0)
    );
\slv_reg21_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg21(24),
      R => SR(0)
    );
\slv_reg21_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg21(25),
      R => SR(0)
    );
\slv_reg21_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg21(26),
      R => SR(0)
    );
\slv_reg21_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg21(27),
      R => SR(0)
    );
\slv_reg21_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg21(28),
      R => SR(0)
    );
\slv_reg21_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg21(29),
      R => SR(0)
    );
\slv_reg21_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg21(2),
      R => SR(0)
    );
\slv_reg21_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg21(30),
      R => SR(0)
    );
\slv_reg21_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg21(31),
      R => SR(0)
    );
\slv_reg21_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg21(3),
      R => SR(0)
    );
\slv_reg21_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg21(4),
      R => SR(0)
    );
\slv_reg21_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg21(5),
      R => SR(0)
    );
\slv_reg21_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg21(6),
      R => SR(0)
    );
\slv_reg21_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg21(7),
      R => SR(0)
    );
\slv_reg21_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg21(8),
      R => SR(0)
    );
\slv_reg21_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg21[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg21(9),
      R => SR(0)
    );
\slv_reg22[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg22[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg22[15]_i_1_n_0\
    );
\slv_reg22[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg22[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg22[23]_i_1_n_0\
    );
\slv_reg22[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg22[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg22[31]_i_1_n_0\
    );
\slv_reg22[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000020000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg22[31]_i_2_n_0\
    );
\slv_reg22[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg22[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg22[7]_i_1_n_0\
    );
\slv_reg22_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg22(0),
      R => SR(0)
    );
\slv_reg22_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg22(10),
      R => SR(0)
    );
\slv_reg22_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg22(11),
      R => SR(0)
    );
\slv_reg22_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg22(12),
      R => SR(0)
    );
\slv_reg22_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg22(13),
      R => SR(0)
    );
\slv_reg22_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg22(14),
      R => SR(0)
    );
\slv_reg22_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg22(15),
      R => SR(0)
    );
\slv_reg22_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg22(16),
      R => SR(0)
    );
\slv_reg22_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg22(17),
      R => SR(0)
    );
\slv_reg22_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg22(18),
      R => SR(0)
    );
\slv_reg22_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg22(19),
      R => SR(0)
    );
\slv_reg22_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg22(1),
      R => SR(0)
    );
\slv_reg22_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg22(20),
      R => SR(0)
    );
\slv_reg22_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg22(21),
      R => SR(0)
    );
\slv_reg22_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg22(22),
      R => SR(0)
    );
\slv_reg22_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg22(23),
      R => SR(0)
    );
\slv_reg22_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg22(24),
      R => SR(0)
    );
\slv_reg22_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg22(25),
      R => SR(0)
    );
\slv_reg22_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg22(26),
      R => SR(0)
    );
\slv_reg22_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg22(27),
      R => SR(0)
    );
\slv_reg22_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg22(28),
      R => SR(0)
    );
\slv_reg22_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg22(29),
      R => SR(0)
    );
\slv_reg22_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg22(2),
      R => SR(0)
    );
\slv_reg22_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg22(30),
      R => SR(0)
    );
\slv_reg22_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg22(31),
      R => SR(0)
    );
\slv_reg22_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg22(3),
      R => SR(0)
    );
\slv_reg22_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg22(4),
      R => SR(0)
    );
\slv_reg22_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg22(5),
      R => SR(0)
    );
\slv_reg22_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg22(6),
      R => SR(0)
    );
\slv_reg22_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg22(7),
      R => SR(0)
    );
\slv_reg22_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg22(8),
      R => SR(0)
    );
\slv_reg22_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg22[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg22(9),
      R => SR(0)
    );
\slv_reg23[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg23[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg23[15]_i_1_n_0\
    );
\slv_reg23[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg23[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg23[23]_i_1_n_0\
    );
\slv_reg23[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg23[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg23[31]_i_1_n_0\
    );
\slv_reg23[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg23[31]_i_2_n_0\
    );
\slv_reg23[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg23[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg23[7]_i_1_n_0\
    );
\slv_reg23_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg23(0),
      R => SR(0)
    );
\slv_reg23_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg23(10),
      R => SR(0)
    );
\slv_reg23_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg23(11),
      R => SR(0)
    );
\slv_reg23_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg23(12),
      R => SR(0)
    );
\slv_reg23_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg23(13),
      R => SR(0)
    );
\slv_reg23_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg23(14),
      R => SR(0)
    );
\slv_reg23_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg23(15),
      R => SR(0)
    );
\slv_reg23_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg23(16),
      R => SR(0)
    );
\slv_reg23_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg23(17),
      R => SR(0)
    );
\slv_reg23_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg23(18),
      R => SR(0)
    );
\slv_reg23_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg23(19),
      R => SR(0)
    );
\slv_reg23_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg23(1),
      R => SR(0)
    );
\slv_reg23_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg23(20),
      R => SR(0)
    );
\slv_reg23_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg23(21),
      R => SR(0)
    );
\slv_reg23_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg23(22),
      R => SR(0)
    );
\slv_reg23_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg23(23),
      R => SR(0)
    );
\slv_reg23_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg23(24),
      R => SR(0)
    );
\slv_reg23_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg23(25),
      R => SR(0)
    );
\slv_reg23_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg23(26),
      R => SR(0)
    );
\slv_reg23_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg23(27),
      R => SR(0)
    );
\slv_reg23_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg23(28),
      R => SR(0)
    );
\slv_reg23_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg23(29),
      R => SR(0)
    );
\slv_reg23_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg23(2),
      R => SR(0)
    );
\slv_reg23_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg23(30),
      R => SR(0)
    );
\slv_reg23_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg23(31),
      R => SR(0)
    );
\slv_reg23_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg23(3),
      R => SR(0)
    );
\slv_reg23_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg23(4),
      R => SR(0)
    );
\slv_reg23_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg23(5),
      R => SR(0)
    );
\slv_reg23_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg23(6),
      R => SR(0)
    );
\slv_reg23_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg23(7),
      R => SR(0)
    );
\slv_reg23_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg23(8),
      R => SR(0)
    );
\slv_reg23_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg23[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg23(9),
      R => SR(0)
    );
\slv_reg24[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg24[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg24[15]_i_1_n_0\
    );
\slv_reg24[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg24[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg24[23]_i_1_n_0\
    );
\slv_reg24[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg24[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg24[31]_i_1_n_0\
    );
\slv_reg24[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000080"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg24[31]_i_2_n_0\
    );
\slv_reg24[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg24[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg24[7]_i_1_n_0\
    );
\slv_reg24_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg24(0),
      R => SR(0)
    );
\slv_reg24_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg24(10),
      R => SR(0)
    );
\slv_reg24_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg24(11),
      R => SR(0)
    );
\slv_reg24_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg24(12),
      R => SR(0)
    );
\slv_reg24_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg24(13),
      R => SR(0)
    );
\slv_reg24_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg24(14),
      R => SR(0)
    );
\slv_reg24_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg24(15),
      R => SR(0)
    );
\slv_reg24_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg24(16),
      R => SR(0)
    );
\slv_reg24_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg24(17),
      R => SR(0)
    );
\slv_reg24_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg24(18),
      R => SR(0)
    );
\slv_reg24_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg24(19),
      R => SR(0)
    );
\slv_reg24_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg24(1),
      R => SR(0)
    );
\slv_reg24_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg24(20),
      R => SR(0)
    );
\slv_reg24_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg24(21),
      R => SR(0)
    );
\slv_reg24_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg24(22),
      R => SR(0)
    );
\slv_reg24_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg24(23),
      R => SR(0)
    );
\slv_reg24_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg24(24),
      R => SR(0)
    );
\slv_reg24_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg24(25),
      R => SR(0)
    );
\slv_reg24_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg24(26),
      R => SR(0)
    );
\slv_reg24_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg24(27),
      R => SR(0)
    );
\slv_reg24_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg24(28),
      R => SR(0)
    );
\slv_reg24_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg24(29),
      R => SR(0)
    );
\slv_reg24_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg24(2),
      R => SR(0)
    );
\slv_reg24_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg24(30),
      R => SR(0)
    );
\slv_reg24_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg24(31),
      R => SR(0)
    );
\slv_reg24_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg24(3),
      R => SR(0)
    );
\slv_reg24_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg24(4),
      R => SR(0)
    );
\slv_reg24_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg24(5),
      R => SR(0)
    );
\slv_reg24_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg24(6),
      R => SR(0)
    );
\slv_reg24_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg24(7),
      R => SR(0)
    );
\slv_reg24_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg24(8),
      R => SR(0)
    );
\slv_reg24_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg24[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg24(9),
      R => SR(0)
    );
\slv_reg25[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg25[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg25[15]_i_1_n_0\
    );
\slv_reg25[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg25[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg25[23]_i_1_n_0\
    );
\slv_reg25[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg25[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg25[31]_i_1_n_0\
    );
\slv_reg25[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg25[31]_i_2_n_0\
    );
\slv_reg25[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg25[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg25[7]_i_1_n_0\
    );
\slv_reg25_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg25(0),
      R => SR(0)
    );
\slv_reg25_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg25(10),
      R => SR(0)
    );
\slv_reg25_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg25(11),
      R => SR(0)
    );
\slv_reg25_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg25(12),
      R => SR(0)
    );
\slv_reg25_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg25(13),
      R => SR(0)
    );
\slv_reg25_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg25(14),
      R => SR(0)
    );
\slv_reg25_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg25(15),
      R => SR(0)
    );
\slv_reg25_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg25(16),
      R => SR(0)
    );
\slv_reg25_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg25(17),
      R => SR(0)
    );
\slv_reg25_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg25(18),
      R => SR(0)
    );
\slv_reg25_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg25(19),
      R => SR(0)
    );
\slv_reg25_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg25(1),
      R => SR(0)
    );
\slv_reg25_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg25(20),
      R => SR(0)
    );
\slv_reg25_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg25(21),
      R => SR(0)
    );
\slv_reg25_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg25(22),
      R => SR(0)
    );
\slv_reg25_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg25(23),
      R => SR(0)
    );
\slv_reg25_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg25(24),
      R => SR(0)
    );
\slv_reg25_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg25(25),
      R => SR(0)
    );
\slv_reg25_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg25(26),
      R => SR(0)
    );
\slv_reg25_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg25(27),
      R => SR(0)
    );
\slv_reg25_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg25(28),
      R => SR(0)
    );
\slv_reg25_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg25(29),
      R => SR(0)
    );
\slv_reg25_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg25(2),
      R => SR(0)
    );
\slv_reg25_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg25(30),
      R => SR(0)
    );
\slv_reg25_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg25(31),
      R => SR(0)
    );
\slv_reg25_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg25(3),
      R => SR(0)
    );
\slv_reg25_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg25(4),
      R => SR(0)
    );
\slv_reg25_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg25(5),
      R => SR(0)
    );
\slv_reg25_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg25(6),
      R => SR(0)
    );
\slv_reg25_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg25(7),
      R => SR(0)
    );
\slv_reg25_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg25(8),
      R => SR(0)
    );
\slv_reg25_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg25[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg25(9),
      R => SR(0)
    );
\slv_reg26[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg26[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg26[15]_i_1_n_0\
    );
\slv_reg26[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg26[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg26[23]_i_1_n_0\
    );
\slv_reg26[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg26[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg26[31]_i_1_n_0\
    );
\slv_reg26[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000800000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg26[31]_i_2_n_0\
    );
\slv_reg26[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg26[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg26[7]_i_1_n_0\
    );
\slv_reg26_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg26(0),
      R => SR(0)
    );
\slv_reg26_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg26(10),
      R => SR(0)
    );
\slv_reg26_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg26(11),
      R => SR(0)
    );
\slv_reg26_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg26(12),
      R => SR(0)
    );
\slv_reg26_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg26(13),
      R => SR(0)
    );
\slv_reg26_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg26(14),
      R => SR(0)
    );
\slv_reg26_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg26(15),
      R => SR(0)
    );
\slv_reg26_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg26(16),
      R => SR(0)
    );
\slv_reg26_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg26(17),
      R => SR(0)
    );
\slv_reg26_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg26(18),
      R => SR(0)
    );
\slv_reg26_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg26(19),
      R => SR(0)
    );
\slv_reg26_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg26(1),
      R => SR(0)
    );
\slv_reg26_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg26(20),
      R => SR(0)
    );
\slv_reg26_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg26(21),
      R => SR(0)
    );
\slv_reg26_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg26(22),
      R => SR(0)
    );
\slv_reg26_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg26(23),
      R => SR(0)
    );
\slv_reg26_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg26(24),
      R => SR(0)
    );
\slv_reg26_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg26(25),
      R => SR(0)
    );
\slv_reg26_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg26(26),
      R => SR(0)
    );
\slv_reg26_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg26(27),
      R => SR(0)
    );
\slv_reg26_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg26(28),
      R => SR(0)
    );
\slv_reg26_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg26(29),
      R => SR(0)
    );
\slv_reg26_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg26(2),
      R => SR(0)
    );
\slv_reg26_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg26(30),
      R => SR(0)
    );
\slv_reg26_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg26(31),
      R => SR(0)
    );
\slv_reg26_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg26(3),
      R => SR(0)
    );
\slv_reg26_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg26(4),
      R => SR(0)
    );
\slv_reg26_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg26(5),
      R => SR(0)
    );
\slv_reg26_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg26(6),
      R => SR(0)
    );
\slv_reg26_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg26(7),
      R => SR(0)
    );
\slv_reg26_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg26(8),
      R => SR(0)
    );
\slv_reg26_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg26[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg26(9),
      R => SR(0)
    );
\slv_reg27[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg27[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg27[15]_i_1_n_0\
    );
\slv_reg27[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg27[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg27[23]_i_1_n_0\
    );
\slv_reg27[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg27[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg27[31]_i_1_n_0\
    );
\slv_reg27[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg27[31]_i_2_n_0\
    );
\slv_reg27[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg27[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg27[7]_i_1_n_0\
    );
\slv_reg27_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg27(0),
      R => SR(0)
    );
\slv_reg27_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg27(10),
      R => SR(0)
    );
\slv_reg27_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg27(11),
      R => SR(0)
    );
\slv_reg27_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg27(12),
      R => SR(0)
    );
\slv_reg27_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg27(13),
      R => SR(0)
    );
\slv_reg27_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg27(14),
      R => SR(0)
    );
\slv_reg27_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg27(15),
      R => SR(0)
    );
\slv_reg27_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg27(16),
      R => SR(0)
    );
\slv_reg27_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg27(17),
      R => SR(0)
    );
\slv_reg27_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg27(18),
      R => SR(0)
    );
\slv_reg27_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg27(19),
      R => SR(0)
    );
\slv_reg27_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg27(1),
      R => SR(0)
    );
\slv_reg27_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg27(20),
      R => SR(0)
    );
\slv_reg27_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg27(21),
      R => SR(0)
    );
\slv_reg27_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg27(22),
      R => SR(0)
    );
\slv_reg27_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg27(23),
      R => SR(0)
    );
\slv_reg27_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg27(24),
      R => SR(0)
    );
\slv_reg27_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg27(25),
      R => SR(0)
    );
\slv_reg27_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg27(26),
      R => SR(0)
    );
\slv_reg27_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg27(27),
      R => SR(0)
    );
\slv_reg27_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg27(28),
      R => SR(0)
    );
\slv_reg27_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg27(29),
      R => SR(0)
    );
\slv_reg27_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg27(2),
      R => SR(0)
    );
\slv_reg27_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg27(30),
      R => SR(0)
    );
\slv_reg27_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg27(31),
      R => SR(0)
    );
\slv_reg27_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg27(3),
      R => SR(0)
    );
\slv_reg27_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg27(4),
      R => SR(0)
    );
\slv_reg27_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg27(5),
      R => SR(0)
    );
\slv_reg27_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg27(6),
      R => SR(0)
    );
\slv_reg27_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg27(7),
      R => SR(0)
    );
\slv_reg27_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg27(8),
      R => SR(0)
    );
\slv_reg27_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg27[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg27(9),
      R => SR(0)
    );
\slv_reg28[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg28[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg28[15]_i_1_n_0\
    );
\slv_reg28[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg28[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg28[23]_i_1_n_0\
    );
\slv_reg28[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg28[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg28[31]_i_1_n_0\
    );
\slv_reg28[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg28[31]_i_2_n_0\
    );
\slv_reg28[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg28[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg28[7]_i_1_n_0\
    );
\slv_reg28_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg28(0),
      R => SR(0)
    );
\slv_reg28_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg28(10),
      R => SR(0)
    );
\slv_reg28_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg28(11),
      R => SR(0)
    );
\slv_reg28_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg28(12),
      R => SR(0)
    );
\slv_reg28_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg28(13),
      R => SR(0)
    );
\slv_reg28_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg28(14),
      R => SR(0)
    );
\slv_reg28_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg28(15),
      R => SR(0)
    );
\slv_reg28_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg28(16),
      R => SR(0)
    );
\slv_reg28_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg28(17),
      R => SR(0)
    );
\slv_reg28_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg28(18),
      R => SR(0)
    );
\slv_reg28_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg28(19),
      R => SR(0)
    );
\slv_reg28_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg28(1),
      R => SR(0)
    );
\slv_reg28_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg28(20),
      R => SR(0)
    );
\slv_reg28_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg28(21),
      R => SR(0)
    );
\slv_reg28_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg28(22),
      R => SR(0)
    );
\slv_reg28_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg28(23),
      R => SR(0)
    );
\slv_reg28_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg28(24),
      R => SR(0)
    );
\slv_reg28_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg28(25),
      R => SR(0)
    );
\slv_reg28_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg28(26),
      R => SR(0)
    );
\slv_reg28_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg28(27),
      R => SR(0)
    );
\slv_reg28_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg28(28),
      R => SR(0)
    );
\slv_reg28_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg28(29),
      R => SR(0)
    );
\slv_reg28_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg28(2),
      R => SR(0)
    );
\slv_reg28_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg28(30),
      R => SR(0)
    );
\slv_reg28_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg28(31),
      R => SR(0)
    );
\slv_reg28_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg28(3),
      R => SR(0)
    );
\slv_reg28_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg28(4),
      R => SR(0)
    );
\slv_reg28_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg28(5),
      R => SR(0)
    );
\slv_reg28_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg28(6),
      R => SR(0)
    );
\slv_reg28_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg28(7),
      R => SR(0)
    );
\slv_reg28_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg28(8),
      R => SR(0)
    );
\slv_reg28_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg28[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg28(9),
      R => SR(0)
    );
\slv_reg29[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg29[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg29[15]_i_1_n_0\
    );
\slv_reg29[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg29[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg29[23]_i_1_n_0\
    );
\slv_reg29[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg29[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg29[31]_i_1_n_0\
    );
\slv_reg29[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg29[31]_i_2_n_0\
    );
\slv_reg29[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg29[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg29[7]_i_1_n_0\
    );
\slv_reg29_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg29(0),
      R => SR(0)
    );
\slv_reg29_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg29(10),
      R => SR(0)
    );
\slv_reg29_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg29(11),
      R => SR(0)
    );
\slv_reg29_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg29(12),
      R => SR(0)
    );
\slv_reg29_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg29(13),
      R => SR(0)
    );
\slv_reg29_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg29(14),
      R => SR(0)
    );
\slv_reg29_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg29(15),
      R => SR(0)
    );
\slv_reg29_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg29(16),
      R => SR(0)
    );
\slv_reg29_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg29(17),
      R => SR(0)
    );
\slv_reg29_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg29(18),
      R => SR(0)
    );
\slv_reg29_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg29(19),
      R => SR(0)
    );
\slv_reg29_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg29(1),
      R => SR(0)
    );
\slv_reg29_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg29(20),
      R => SR(0)
    );
\slv_reg29_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg29(21),
      R => SR(0)
    );
\slv_reg29_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg29(22),
      R => SR(0)
    );
\slv_reg29_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg29(23),
      R => SR(0)
    );
\slv_reg29_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg29(24),
      R => SR(0)
    );
\slv_reg29_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg29(25),
      R => SR(0)
    );
\slv_reg29_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg29(26),
      R => SR(0)
    );
\slv_reg29_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg29(27),
      R => SR(0)
    );
\slv_reg29_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg29(28),
      R => SR(0)
    );
\slv_reg29_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg29(29),
      R => SR(0)
    );
\slv_reg29_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg29(2),
      R => SR(0)
    );
\slv_reg29_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg29(30),
      R => SR(0)
    );
\slv_reg29_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg29(31),
      R => SR(0)
    );
\slv_reg29_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg29(3),
      R => SR(0)
    );
\slv_reg29_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg29(4),
      R => SR(0)
    );
\slv_reg29_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg29(5),
      R => SR(0)
    );
\slv_reg29_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg29(6),
      R => SR(0)
    );
\slv_reg29_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg29(7),
      R => SR(0)
    );
\slv_reg29_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg29(8),
      R => SR(0)
    );
\slv_reg29_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg29[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg29(9),
      R => SR(0)
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg2[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg2[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg2[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000100000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg2[31]_i_2_n_0\
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg2[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg2[7]_i_1_n_0\
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg2(0),
      R => SR(0)
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg2(10),
      R => SR(0)
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg2(11),
      R => SR(0)
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg2(12),
      R => SR(0)
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg2(13),
      R => SR(0)
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg2(14),
      R => SR(0)
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg2(15),
      R => SR(0)
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg2__0\(16),
      R => SR(0)
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg2__0\(17),
      R => SR(0)
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg2__0\(18),
      R => SR(0)
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg2__0\(19),
      R => SR(0)
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg2(1),
      R => SR(0)
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg2__0\(20),
      R => SR(0)
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg2__0\(21),
      R => SR(0)
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg2__0\(22),
      R => SR(0)
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg2__0\(23),
      R => SR(0)
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg2__0\(24),
      R => SR(0)
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg2__0\(25),
      R => SR(0)
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg2__0\(26),
      R => SR(0)
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg2__0\(27),
      R => SR(0)
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg2__0\(28),
      R => SR(0)
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg2__0\(29),
      R => SR(0)
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg2(2),
      R => SR(0)
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg2__0\(30),
      R => SR(0)
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg2__0\(31),
      R => SR(0)
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg2(3),
      R => SR(0)
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg2(4),
      R => SR(0)
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg2(5),
      R => SR(0)
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg2(6),
      R => SR(0)
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg2(7),
      R => SR(0)
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg2(8),
      R => SR(0)
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg2(9),
      R => SR(0)
    );
\slv_reg30[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg30[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg30[15]_i_1_n_0\
    );
\slv_reg30[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg30[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg30[23]_i_1_n_0\
    );
\slv_reg30[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg30[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg30[31]_i_1_n_0\
    );
\slv_reg30[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg30[31]_i_2_n_0\
    );
\slv_reg30[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg30[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg30[7]_i_1_n_0\
    );
\slv_reg30_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg30(0),
      R => SR(0)
    );
\slv_reg30_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg30(10),
      R => SR(0)
    );
\slv_reg30_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg30(11),
      R => SR(0)
    );
\slv_reg30_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg30(12),
      R => SR(0)
    );
\slv_reg30_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg30(13),
      R => SR(0)
    );
\slv_reg30_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg30(14),
      R => SR(0)
    );
\slv_reg30_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg30(15),
      R => SR(0)
    );
\slv_reg30_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg30(16),
      R => SR(0)
    );
\slv_reg30_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg30(17),
      R => SR(0)
    );
\slv_reg30_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg30(18),
      R => SR(0)
    );
\slv_reg30_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg30(19),
      R => SR(0)
    );
\slv_reg30_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg30(1),
      R => SR(0)
    );
\slv_reg30_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg30(20),
      R => SR(0)
    );
\slv_reg30_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg30(21),
      R => SR(0)
    );
\slv_reg30_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg30(22),
      R => SR(0)
    );
\slv_reg30_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg30(23),
      R => SR(0)
    );
\slv_reg30_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg30(24),
      R => SR(0)
    );
\slv_reg30_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg30(25),
      R => SR(0)
    );
\slv_reg30_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg30(26),
      R => SR(0)
    );
\slv_reg30_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg30(27),
      R => SR(0)
    );
\slv_reg30_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg30(28),
      R => SR(0)
    );
\slv_reg30_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg30(29),
      R => SR(0)
    );
\slv_reg30_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg30(2),
      R => SR(0)
    );
\slv_reg30_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg30(30),
      R => SR(0)
    );
\slv_reg30_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg30(31),
      R => SR(0)
    );
\slv_reg30_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg30(3),
      R => SR(0)
    );
\slv_reg30_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg30(4),
      R => SR(0)
    );
\slv_reg30_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg30(5),
      R => SR(0)
    );
\slv_reg30_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg30(6),
      R => SR(0)
    );
\slv_reg30_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg30(7),
      R => SR(0)
    );
\slv_reg30_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg30(8),
      R => SR(0)
    );
\slv_reg30_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg30[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg30(9),
      R => SR(0)
    );
\slv_reg31[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg31[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg31[15]_i_1_n_0\
    );
\slv_reg31[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg31[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg31[23]_i_1_n_0\
    );
\slv_reg31[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg31[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg31[31]_i_1_n_0\
    );
\slv_reg31[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg31[31]_i_2_n_0\
    );
\slv_reg31[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg31[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg31[7]_i_1_n_0\
    );
\slv_reg31_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg31(0),
      R => SR(0)
    );
\slv_reg31_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg31(10),
      R => SR(0)
    );
\slv_reg31_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg31(11),
      R => SR(0)
    );
\slv_reg31_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg31(12),
      R => SR(0)
    );
\slv_reg31_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg31(13),
      R => SR(0)
    );
\slv_reg31_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg31(14),
      R => SR(0)
    );
\slv_reg31_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg31(15),
      R => SR(0)
    );
\slv_reg31_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg31(16),
      R => SR(0)
    );
\slv_reg31_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg31(17),
      R => SR(0)
    );
\slv_reg31_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg31(18),
      R => SR(0)
    );
\slv_reg31_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg31(19),
      R => SR(0)
    );
\slv_reg31_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg31(1),
      R => SR(0)
    );
\slv_reg31_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg31(20),
      R => SR(0)
    );
\slv_reg31_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg31(21),
      R => SR(0)
    );
\slv_reg31_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg31(22),
      R => SR(0)
    );
\slv_reg31_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg31(23),
      R => SR(0)
    );
\slv_reg31_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg31(24),
      R => SR(0)
    );
\slv_reg31_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg31(25),
      R => SR(0)
    );
\slv_reg31_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg31(26),
      R => SR(0)
    );
\slv_reg31_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg31(27),
      R => SR(0)
    );
\slv_reg31_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg31(28),
      R => SR(0)
    );
\slv_reg31_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg31(29),
      R => SR(0)
    );
\slv_reg31_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg31(2),
      R => SR(0)
    );
\slv_reg31_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg31(30),
      R => SR(0)
    );
\slv_reg31_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg31(31),
      R => SR(0)
    );
\slv_reg31_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg31(3),
      R => SR(0)
    );
\slv_reg31_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg31(4),
      R => SR(0)
    );
\slv_reg31_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg31(5),
      R => SR(0)
    );
\slv_reg31_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg31(6),
      R => SR(0)
    );
\slv_reg31_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg31(7),
      R => SR(0)
    );
\slv_reg31_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg31(8),
      R => SR(0)
    );
\slv_reg31_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg31[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg31(9),
      R => SR(0)
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg3[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg3[15]_i_1_n_0\
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg3[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg3[23]_i_1_n_0\
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg3[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg3[31]_i_1_n_0\
    );
\slv_reg3[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010000000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg3[31]_i_2_n_0\
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg3[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg3[7]_i_1_n_0\
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg3(0),
      R => SR(0)
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg3__0\(10),
      R => SR(0)
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg3__0\(11),
      R => SR(0)
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg3__0\(12),
      R => SR(0)
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg3__0\(13),
      R => SR(0)
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg3__0\(14),
      R => SR(0)
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg3__0\(15),
      R => SR(0)
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg3__0\(16),
      R => SR(0)
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg3__0\(17),
      R => SR(0)
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg3__0\(18),
      R => SR(0)
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg3__0\(19),
      R => SR(0)
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg3(1),
      R => SR(0)
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg3__0\(20),
      R => SR(0)
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg3__0\(21),
      R => SR(0)
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg3__0\(22),
      R => SR(0)
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg3__0\(23),
      R => SR(0)
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg3__0\(24),
      R => SR(0)
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg3__0\(25),
      R => SR(0)
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg3__0\(26),
      R => SR(0)
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg3__0\(27),
      R => SR(0)
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg3__0\(28),
      R => SR(0)
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg3__0\(29),
      R => SR(0)
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg3(2),
      R => SR(0)
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg3__0\(30),
      R => SR(0)
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg3__0\(31),
      R => SR(0)
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg3(3),
      R => SR(0)
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg3(4),
      R => SR(0)
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg3(5),
      R => SR(0)
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg3(6),
      R => SR(0)
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg3(7),
      R => SR(0)
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \slv_reg3__0\(8),
      R => SR(0)
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \slv_reg3__0\(9),
      R => SR(0)
    );
\slv_reg4[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg4[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg4[15]_i_1_n_0\
    );
\slv_reg4[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg4[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg4[23]_i_1_n_0\
    );
\slv_reg4[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg4[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg4[31]_i_1_n_0\
    );
\slv_reg4[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg4[31]_i_2_n_0\
    );
\slv_reg4[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg4[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg4[7]_i_1_n_0\
    );
\slv_reg4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg4(0),
      R => SR(0)
    );
\slv_reg4_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg4__0\(10),
      R => SR(0)
    );
\slv_reg4_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg4__0\(11),
      R => SR(0)
    );
\slv_reg4_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg4__0\(12),
      R => SR(0)
    );
\slv_reg4_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg4__0\(13),
      R => SR(0)
    );
\slv_reg4_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg4__0\(14),
      R => SR(0)
    );
\slv_reg4_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg4__0\(15),
      R => SR(0)
    );
\slv_reg4_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg4__0\(16),
      R => SR(0)
    );
\slv_reg4_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg4__0\(17),
      R => SR(0)
    );
\slv_reg4_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg4__0\(18),
      R => SR(0)
    );
\slv_reg4_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg4__0\(19),
      R => SR(0)
    );
\slv_reg4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \slv_reg4__0\(1),
      R => SR(0)
    );
\slv_reg4_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg4__0\(20),
      R => SR(0)
    );
\slv_reg4_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg4__0\(21),
      R => SR(0)
    );
\slv_reg4_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg4__0\(22),
      R => SR(0)
    );
\slv_reg4_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg4__0\(23),
      R => SR(0)
    );
\slv_reg4_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg4__0\(24),
      R => SR(0)
    );
\slv_reg4_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg4__0\(25),
      R => SR(0)
    );
\slv_reg4_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg4__0\(26),
      R => SR(0)
    );
\slv_reg4_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg4__0\(27),
      R => SR(0)
    );
\slv_reg4_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg4__0\(28),
      R => SR(0)
    );
\slv_reg4_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg4__0\(29),
      R => SR(0)
    );
\slv_reg4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \slv_reg4__0\(2),
      R => SR(0)
    );
\slv_reg4_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg4__0\(30),
      R => SR(0)
    );
\slv_reg4_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg4__0\(31),
      R => SR(0)
    );
\slv_reg4_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \slv_reg4__0\(3),
      R => SR(0)
    );
\slv_reg4_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \slv_reg4__0\(4),
      R => SR(0)
    );
\slv_reg4_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \slv_reg4__0\(5),
      R => SR(0)
    );
\slv_reg4_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \slv_reg4__0\(6),
      R => SR(0)
    );
\slv_reg4_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \slv_reg4__0\(7),
      R => SR(0)
    );
\slv_reg4_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \slv_reg4__0\(8),
      R => SR(0)
    );
\slv_reg4_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \slv_reg4__0\(9),
      R => SR(0)
    );
\slv_reg5[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg5[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \slv_reg5[15]_i_1_n_0\
    );
\slv_reg5[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg5[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \slv_reg5[23]_i_1_n_0\
    );
\slv_reg5[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg5[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \slv_reg5[31]_i_1_n_0\
    );
\slv_reg5[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000100000000000"
    )
        port map (
      I0 => p_0_in(4),
      I1 => p_0_in(3),
      I2 => \slv_reg0[31]_i_3_n_0\,
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(0),
      O => \slv_reg5[31]_i_2_n_0\
    );
\slv_reg5[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg5[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \slv_reg5[7]_i_1_n_0\
    );
\slv_reg5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg5(0),
      R => SR(0)
    );
\slv_reg5_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg5__0\(10),
      R => SR(0)
    );
\slv_reg5_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg5__0\(11),
      R => SR(0)
    );
\slv_reg5_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg5__0\(12),
      R => SR(0)
    );
\slv_reg5_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg5__0\(13),
      R => SR(0)
    );
\slv_reg5_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg5__0\(14),
      R => SR(0)
    );
\slv_reg5_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg5__0\(15),
      R => SR(0)
    );
\slv_reg5_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg5__0\(16),
      R => SR(0)
    );
\slv_reg5_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg5__0\(17),
      R => SR(0)
    );
\slv_reg5_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg5__0\(18),
      R => SR(0)
    );
\slv_reg5_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg5__0\(19),
      R => SR(0)
    );
\slv_reg5_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \slv_reg5__0\(1),
      R => SR(0)
    );
\slv_reg5_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg5__0\(20),
      R => SR(0)
    );
\slv_reg5_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg5__0\(21),
      R => SR(0)
    );
\slv_reg5_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg5__0\(22),
      R => SR(0)
    );
\slv_reg5_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg5__0\(23),
      R => SR(0)
    );
\slv_reg5_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg5__0\(24),
      R => SR(0)
    );
\slv_reg5_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg5__0\(25),
      R => SR(0)
    );
\slv_reg5_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg5__0\(26),
      R => SR(0)
    );
\slv_reg5_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg5__0\(27),
      R => SR(0)
    );
\slv_reg5_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg5__0\(28),
      R => SR(0)
    );
\slv_reg5_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg5__0\(29),
      R => SR(0)
    );
\slv_reg5_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \slv_reg5__0\(2),
      R => SR(0)
    );
\slv_reg5_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg5__0\(30),
      R => SR(0)
    );
\slv_reg5_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg5__0\(31),
      R => SR(0)
    );
\slv_reg5_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \slv_reg5__0\(3),
      R => SR(0)
    );
\slv_reg5_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \slv_reg5__0\(4),
      R => SR(0)
    );
\slv_reg5_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \slv_reg5__0\(5),
      R => SR(0)
    );
\slv_reg5_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \slv_reg5__0\(6),
      R => SR(0)
    );
\slv_reg5_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \slv_reg5__0\(7),
      R => SR(0)
    );
\slv_reg5_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \slv_reg5__0\(8),
      R => SR(0)
    );
\slv_reg5_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \slv_reg5__0\(9),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0 is
  port (
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    ready : out STD_LOGIC;
    tmds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tmdsb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ac_mclk : out STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    clk : in STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    btn : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    switch : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0 is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal axi_awready_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => axi_awready_i_1_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555555C0000000"
    )
        port map (
      I0 => s00_axi_bready,
      I1 => \^s_axi_awready\,
      I2 => s00_axi_awvalid,
      I3 => s00_axi_wvalid,
      I4 => \^s_axi_wready\,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
my_oscope_ip_v1_0_S00_AXI_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0_S00_AXI
     port map (
      CLK => ready,
      SR(0) => axi_awready_i_1_n_0,
      ac_adc_sdata => ac_adc_sdata,
      ac_bclk => ac_bclk,
      ac_dac_sdata => ac_dac_sdata,
      ac_lrclk => ac_lrclk,
      ac_mclk => ac_mclk,
      axi_arready_reg_0 => axi_rvalid_i_1_n_0,
      axi_awready_reg_0 => axi_bvalid_i_1_n_0,
      btn(4 downto 0) => btn(4 downto 0),
      \^clk\ => clk,
      reset_n => reset_n,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(4 downto 0) => s00_axi_araddr(4 downto 0),
      s00_axi_arready => \^s_axi_arready\,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(4 downto 0) => s00_axi_awaddr(4 downto 0),
      s00_axi_awready => \^s_axi_awready\,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bvalid => \^s00_axi_bvalid\,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rvalid => \^s00_axi_rvalid\,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wready => \^s_axi_wready\,
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      scl => scl,
      sda => sda,
      switch(3 downto 0) => switch(3 downto 0),
      tmds(3 downto 0) => tmds(3 downto 0),
      tmdsb(3 downto 0) => tmdsb(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    ac_mclk : out STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
    tmds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tmdsb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    switch : in STD_LOGIC_VECTOR ( 3 downto 0 );
    btn : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ready : out STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_my_oscope_0_0,my_oscope_ip_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "my_oscope_ip_v1_0,Vivado 2017.3";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of clk : signal is "XIL_INTERFACENAME clk, ASSOCIATED_RESET reset_n, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1";
  attribute x_interface_info of reset_n : signal is "xilinx.com:signal:reset:1.0 reset_n RST";
  attribute x_interface_parameter of reset_n : signal is "XIL_INTERFACENAME reset_n, POLARITY ACTIVE_LOW";
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 32, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 7, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0
     port map (
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      ac_adc_sdata => ac_adc_sdata,
      ac_bclk => ac_bclk,
      ac_dac_sdata => ac_dac_sdata,
      ac_lrclk => ac_lrclk,
      ac_mclk => ac_mclk,
      btn(4 downto 0) => btn(4 downto 0),
      clk => clk,
      ready => ready,
      reset_n => reset_n,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(4 downto 0) => s00_axi_araddr(6 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(4 downto 0) => s00_axi_awaddr(6 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      scl => scl,
      sda => sda,
      switch(3 downto 0) => switch(3 downto 0),
      tmds(3 downto 0) => tmds(3 downto 0),
      tmdsb(3 downto 0) => tmdsb(3 downto 0)
    );
end STRUCTURE;
