// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.3 (win64) Build 2018833 Wed Oct  4 19:58:22 MDT 2017
// Date        : Tue Mar  6 15:40:06 2018
// Host        : C19JMSCHWARTZ running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_my_oscope_0_0_sim_netlist.v
// Design      : design_1_my_oscope_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tsbg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper
   (ac_mclk,
    ac_bclk,
    BCLK_int_reg,
    ac_lrclk,
    CLK,
    \R_unsigned_data_prev_reg[9] ,
    Q,
    \R_unsigned_data_prev_reg[8] ,
    D,
    \L_unsigned_data_prev_reg[9] ,
    \L_bus_in_s_reg[17] ,
    \L_unsigned_data_prev_reg[8] ,
    \L_unsigned_data_prev_reg[7] ,
    \state_reg[0] ,
    \axi_rdata_reg[15] ,
    \axi_rdata_reg[2] ,
    DIBDI,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    ac_dac_sdata,
    scl,
    sda,
    clk,
    ac_adc_sdata,
    reset_n,
    state,
    \processQ_reg[9] ,
    \axi_araddr_reg[4] ,
    \axi_araddr_reg[4]_0 ,
    \axi_araddr_reg[6] ,
    \slv_reg3_reg[0] ,
    \slv_reg15_reg[0] ,
    \axi_araddr_reg[3]_rep ,
    \slv_reg5_reg[15] ,
    \axi_araddr_reg[2]_rep ,
    \slv_reg4_reg[15] ,
    \slv_reg11_reg[9] ,
    \slv_reg10_reg[0] ,
    flagQ,
    \axi_araddr_reg[4]_1 ,
    \axi_araddr_reg[4]_2 ,
    \axi_araddr_reg[4]_3 ,
    \slv_reg3_reg[1] ,
    \slv_reg3_reg[2] ,
    \axi_araddr_reg[4]_4 ,
    \axi_araddr_reg[4]_5 ,
    \axi_araddr_reg[4]_6 ,
    \slv_reg3_reg[3] ,
    \axi_araddr_reg[4]_7 ,
    \axi_araddr_reg[4]_8 ,
    \axi_araddr_reg[4]_9 ,
    \slv_reg3_reg[4] ,
    \axi_araddr_reg[4]_10 ,
    \axi_araddr_reg[4]_11 ,
    \axi_araddr_reg[4]_12 ,
    \slv_reg3_reg[5] ,
    \axi_araddr_reg[4]_13 ,
    \axi_araddr_reg[4]_14 ,
    \axi_araddr_reg[4]_15 ,
    \slv_reg3_reg[6] ,
    \axi_araddr_reg[4]_16 ,
    \axi_araddr_reg[4]_17 ,
    \axi_araddr_reg[4]_18 ,
    \slv_reg3_reg[7] ,
    \axi_araddr_reg[4]_19 ,
    \axi_araddr_reg[4]_20 ,
    \axi_araddr_reg[4]_21 ,
    \slv_reg3_reg[8] ,
    \axi_araddr_reg[4]_22 ,
    \axi_araddr_reg[4]_23 ,
    \axi_araddr_reg[4]_24 ,
    \slv_reg3_reg[9] ,
    \axi_araddr_reg[4]_25 ,
    \axi_araddr_reg[4]_26 ,
    \axi_araddr_reg[4]_27 ,
    \slv_reg3_reg[10] ,
    \axi_araddr_reg[4]_28 ,
    \axi_araddr_reg[4]_29 ,
    \axi_araddr_reg[4]_30 ,
    \slv_reg3_reg[11] ,
    \axi_araddr_reg[4]_31 ,
    \axi_araddr_reg[4]_32 ,
    \axi_araddr_reg[4]_33 ,
    \slv_reg3_reg[12] ,
    \axi_araddr_reg[4]_34 ,
    \axi_araddr_reg[4]_35 ,
    \axi_araddr_reg[4]_36 ,
    \slv_reg3_reg[13] ,
    \axi_araddr_reg[4]_37 ,
    \axi_araddr_reg[4]_38 ,
    \axi_araddr_reg[4]_39 ,
    \slv_reg3_reg[14] ,
    \axi_araddr_reg[4]_40 ,
    \axi_araddr_reg[4]_41 ,
    \axi_araddr_reg[4]_42 ,
    \slv_reg3_reg[15] ,
    \L_bus_in_s_reg[17]_0 ,
    \R_bus_in_s_reg[17] ,
    CO,
    switch,
    \L_unsigned_data_prev_reg[9]_0 ,
    \slv_reg11_reg[7] ,
    \int_trigger_volt_s_reg[9] ,
    \slv_reg11_reg[6] ,
    \slv_reg11_reg[5] ,
    \slv_reg11_reg[4] ,
    \slv_reg11_reg[3] ,
    \slv_reg11_reg[1] ,
    \slv_reg11_reg[8] ,
    \slv_reg2_reg[9] ,
    \slv_reg1_reg[9] ,
    lopt);
  output ac_mclk;
  output ac_bclk;
  output BCLK_int_reg;
  output ac_lrclk;
  output CLK;
  output \R_unsigned_data_prev_reg[9] ;
  output [17:0]Q;
  output \R_unsigned_data_prev_reg[8] ;
  output [4:0]D;
  output \L_unsigned_data_prev_reg[9] ;
  output [17:0]\L_bus_in_s_reg[17] ;
  output \L_unsigned_data_prev_reg[8] ;
  output [4:0]\L_unsigned_data_prev_reg[7] ;
  output \state_reg[0] ;
  output [14:0]\axi_rdata_reg[15] ;
  output \axi_rdata_reg[2] ;
  output [9:0]DIBDI;
  output [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  output ac_dac_sdata;
  inout scl;
  inout sda;
  input clk;
  input ac_adc_sdata;
  input reset_n;
  input [1:0]state;
  input [0:0]\processQ_reg[9] ;
  input \axi_araddr_reg[4] ;
  input \axi_araddr_reg[4]_0 ;
  input [2:0]\axi_araddr_reg[6] ;
  input \slv_reg3_reg[0] ;
  input \slv_reg15_reg[0] ;
  input \axi_araddr_reg[3]_rep ;
  input [15:0]\slv_reg5_reg[15] ;
  input \axi_araddr_reg[2]_rep ;
  input [15:0]\slv_reg4_reg[15] ;
  input [4:0]\slv_reg11_reg[9] ;
  input [0:0]\slv_reg10_reg[0] ;
  input [0:0]flagQ;
  input \axi_araddr_reg[4]_1 ;
  input \axi_araddr_reg[4]_2 ;
  input \axi_araddr_reg[4]_3 ;
  input \slv_reg3_reg[1] ;
  input \slv_reg3_reg[2] ;
  input \axi_araddr_reg[4]_4 ;
  input \axi_araddr_reg[4]_5 ;
  input \axi_araddr_reg[4]_6 ;
  input \slv_reg3_reg[3] ;
  input \axi_araddr_reg[4]_7 ;
  input \axi_araddr_reg[4]_8 ;
  input \axi_araddr_reg[4]_9 ;
  input \slv_reg3_reg[4] ;
  input \axi_araddr_reg[4]_10 ;
  input \axi_araddr_reg[4]_11 ;
  input \axi_araddr_reg[4]_12 ;
  input \slv_reg3_reg[5] ;
  input \axi_araddr_reg[4]_13 ;
  input \axi_araddr_reg[4]_14 ;
  input \axi_araddr_reg[4]_15 ;
  input \slv_reg3_reg[6] ;
  input \axi_araddr_reg[4]_16 ;
  input \axi_araddr_reg[4]_17 ;
  input \axi_araddr_reg[4]_18 ;
  input \slv_reg3_reg[7] ;
  input \axi_araddr_reg[4]_19 ;
  input \axi_araddr_reg[4]_20 ;
  input \axi_araddr_reg[4]_21 ;
  input \slv_reg3_reg[8] ;
  input \axi_araddr_reg[4]_22 ;
  input \axi_araddr_reg[4]_23 ;
  input \axi_araddr_reg[4]_24 ;
  input \slv_reg3_reg[9] ;
  input \axi_araddr_reg[4]_25 ;
  input \axi_araddr_reg[4]_26 ;
  input \axi_araddr_reg[4]_27 ;
  input \slv_reg3_reg[10] ;
  input \axi_araddr_reg[4]_28 ;
  input \axi_araddr_reg[4]_29 ;
  input \axi_araddr_reg[4]_30 ;
  input \slv_reg3_reg[11] ;
  input \axi_araddr_reg[4]_31 ;
  input \axi_araddr_reg[4]_32 ;
  input \axi_araddr_reg[4]_33 ;
  input \slv_reg3_reg[12] ;
  input \axi_araddr_reg[4]_34 ;
  input \axi_araddr_reg[4]_35 ;
  input \axi_araddr_reg[4]_36 ;
  input \slv_reg3_reg[13] ;
  input \axi_araddr_reg[4]_37 ;
  input \axi_araddr_reg[4]_38 ;
  input \axi_araddr_reg[4]_39 ;
  input \slv_reg3_reg[14] ;
  input \axi_araddr_reg[4]_40 ;
  input \axi_araddr_reg[4]_41 ;
  input \axi_araddr_reg[4]_42 ;
  input \slv_reg3_reg[15] ;
  input [17:0]\L_bus_in_s_reg[17]_0 ;
  input [17:0]\R_bus_in_s_reg[17] ;
  input [0:0]CO;
  input [0:0]switch;
  input [0:0]\L_unsigned_data_prev_reg[9]_0 ;
  input \slv_reg11_reg[7] ;
  input [4:0]\int_trigger_volt_s_reg[9] ;
  input \slv_reg11_reg[6] ;
  input \slv_reg11_reg[5] ;
  input \slv_reg11_reg[4] ;
  input \slv_reg11_reg[3] ;
  input \slv_reg11_reg[1] ;
  input \slv_reg11_reg[8] ;
  input [9:0]\slv_reg2_reg[9] ;
  input [9:0]\slv_reg1_reg[9] ;
  output lopt;

  wire BCLK_int_reg;
  wire CLK;
  wire [0:0]CO;
  wire [4:0]D;
  wire [9:0]DIBDI;
  wire [17:0]\L_bus_in_s_reg[17] ;
  wire [17:0]\L_bus_in_s_reg[17]_0 ;
  wire [4:0]\L_unsigned_data_prev_reg[7] ;
  wire \L_unsigned_data_prev_reg[8] ;
  wire \L_unsigned_data_prev_reg[9] ;
  wire [0:0]\L_unsigned_data_prev_reg[9]_0 ;
  wire [17:0]Q;
  wire [17:0]\R_bus_in_s_reg[17] ;
  wire \R_unsigned_data_prev_reg[8] ;
  wire \R_unsigned_data_prev_reg[9] ;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_lrclk_count0;
  wire [3:0]ac_lrclk_count_reg__0;
  wire ac_lrclk_sig_prev_reg_n_0;
  wire ac_mclk;
  wire audio_inout_n_3;
  wire audio_inout_n_72;
  wire audio_inout_n_73;
  wire \axi_araddr_reg[2]_rep ;
  wire \axi_araddr_reg[3]_rep ;
  wire \axi_araddr_reg[4] ;
  wire \axi_araddr_reg[4]_0 ;
  wire \axi_araddr_reg[4]_1 ;
  wire \axi_araddr_reg[4]_10 ;
  wire \axi_araddr_reg[4]_11 ;
  wire \axi_araddr_reg[4]_12 ;
  wire \axi_araddr_reg[4]_13 ;
  wire \axi_araddr_reg[4]_14 ;
  wire \axi_araddr_reg[4]_15 ;
  wire \axi_araddr_reg[4]_16 ;
  wire \axi_araddr_reg[4]_17 ;
  wire \axi_araddr_reg[4]_18 ;
  wire \axi_araddr_reg[4]_19 ;
  wire \axi_araddr_reg[4]_2 ;
  wire \axi_araddr_reg[4]_20 ;
  wire \axi_araddr_reg[4]_21 ;
  wire \axi_araddr_reg[4]_22 ;
  wire \axi_araddr_reg[4]_23 ;
  wire \axi_araddr_reg[4]_24 ;
  wire \axi_araddr_reg[4]_25 ;
  wire \axi_araddr_reg[4]_26 ;
  wire \axi_araddr_reg[4]_27 ;
  wire \axi_araddr_reg[4]_28 ;
  wire \axi_araddr_reg[4]_29 ;
  wire \axi_araddr_reg[4]_3 ;
  wire \axi_araddr_reg[4]_30 ;
  wire \axi_araddr_reg[4]_31 ;
  wire \axi_araddr_reg[4]_32 ;
  wire \axi_araddr_reg[4]_33 ;
  wire \axi_araddr_reg[4]_34 ;
  wire \axi_araddr_reg[4]_35 ;
  wire \axi_araddr_reg[4]_36 ;
  wire \axi_araddr_reg[4]_37 ;
  wire \axi_araddr_reg[4]_38 ;
  wire \axi_araddr_reg[4]_39 ;
  wire \axi_araddr_reg[4]_4 ;
  wire \axi_araddr_reg[4]_40 ;
  wire \axi_araddr_reg[4]_41 ;
  wire \axi_araddr_reg[4]_42 ;
  wire \axi_araddr_reg[4]_5 ;
  wire \axi_araddr_reg[4]_6 ;
  wire \axi_araddr_reg[4]_7 ;
  wire \axi_araddr_reg[4]_8 ;
  wire \axi_araddr_reg[4]_9 ;
  wire [2:0]\axi_araddr_reg[6] ;
  wire \axi_rdata[0]_i_10_n_0 ;
  wire \axi_rdata_reg[0]_i_4_n_0 ;
  wire [14:0]\axi_rdata_reg[15] ;
  wire \axi_rdata_reg[2] ;
  wire clk;
  wire clk_50;
  wire [0:0]flagQ;
  wire [4:0]\int_trigger_volt_s_reg[9] ;
  wire lopt;
  wire [3:0]plusOp;
  wire [0:0]\processQ_reg[9] ;
  wire reset_n;
  wire scl;
  wire sda;
  wire [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire [0:0]\slv_reg10_reg[0] ;
  wire \slv_reg11_reg[1] ;
  wire \slv_reg11_reg[3] ;
  wire \slv_reg11_reg[4] ;
  wire \slv_reg11_reg[5] ;
  wire \slv_reg11_reg[6] ;
  wire \slv_reg11_reg[7] ;
  wire \slv_reg11_reg[8] ;
  wire [4:0]\slv_reg11_reg[9] ;
  wire \slv_reg15_reg[0] ;
  wire [9:0]\slv_reg1_reg[9] ;
  wire [9:0]\slv_reg2_reg[9] ;
  wire \slv_reg3_reg[0] ;
  wire \slv_reg3_reg[10] ;
  wire \slv_reg3_reg[11] ;
  wire \slv_reg3_reg[12] ;
  wire \slv_reg3_reg[13] ;
  wire \slv_reg3_reg[14] ;
  wire \slv_reg3_reg[15] ;
  wire \slv_reg3_reg[1] ;
  wire \slv_reg3_reg[2] ;
  wire \slv_reg3_reg[3] ;
  wire \slv_reg3_reg[4] ;
  wire \slv_reg3_reg[5] ;
  wire \slv_reg3_reg[6] ;
  wire \slv_reg3_reg[7] ;
  wire \slv_reg3_reg[8] ;
  wire \slv_reg3_reg[9] ;
  wire [15:0]\slv_reg4_reg[15] ;
  wire [15:0]\slv_reg5_reg[15] ;
  wire [1:0]state;
  wire \state_reg[0] ;
  wire [0:0]switch;

  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \ac_lrclk_count[0]_i_1 
       (.I0(ac_lrclk_count_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \ac_lrclk_count[1]_i_1 
       (.I0(ac_lrclk_count_reg__0[1]),
        .I1(ac_lrclk_count_reg__0[0]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \ac_lrclk_count[2]_i_1 
       (.I0(ac_lrclk_count_reg__0[2]),
        .I1(ac_lrclk_count_reg__0[0]),
        .I2(ac_lrclk_count_reg__0[1]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \ac_lrclk_count[3]_i_3 
       (.I0(ac_lrclk_count_reg__0[3]),
        .I1(ac_lrclk_count_reg__0[1]),
        .I2(ac_lrclk_count_reg__0[0]),
        .I3(ac_lrclk_count_reg__0[2]),
        .O(plusOp[3]));
  FDRE \ac_lrclk_count_reg[0] 
       (.C(clk),
        .CE(ac_lrclk_count0),
        .D(plusOp[0]),
        .Q(ac_lrclk_count_reg__0[0]),
        .R(audio_inout_n_3));
  FDRE \ac_lrclk_count_reg[1] 
       (.C(clk),
        .CE(ac_lrclk_count0),
        .D(plusOp[1]),
        .Q(ac_lrclk_count_reg__0[1]),
        .R(audio_inout_n_3));
  FDRE \ac_lrclk_count_reg[2] 
       (.C(clk),
        .CE(ac_lrclk_count0),
        .D(plusOp[2]),
        .Q(ac_lrclk_count_reg__0[2]),
        .R(audio_inout_n_3));
  FDRE \ac_lrclk_count_reg[3] 
       (.C(clk),
        .CE(ac_lrclk_count0),
        .D(plusOp[3]),
        .Q(ac_lrclk_count_reg__0[3]),
        .R(audio_inout_n_3));
  FDRE ac_lrclk_sig_prev_reg
       (.C(clk),
        .CE(1'b1),
        .D(audio_inout_n_72),
        .Q(ac_lrclk_sig_prev_reg_n_0),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl audio_inout
       (.CO(CO),
        .D(D),
        .DIBDI(DIBDI),
        .E(ac_lrclk_count0),
        .\L_bus_in_s_reg[17] (\L_bus_in_s_reg[17] ),
        .\L_bus_in_s_reg[17]_0 (\L_bus_in_s_reg[17]_0 ),
        .\L_unsigned_data_prev_reg[7] (\L_unsigned_data_prev_reg[7] ),
        .\L_unsigned_data_prev_reg[8] (\L_unsigned_data_prev_reg[8] ),
        .\L_unsigned_data_prev_reg[9] (\L_unsigned_data_prev_reg[9] ),
        .\L_unsigned_data_prev_reg[9]_0 (\L_unsigned_data_prev_reg[9]_0 ),
        .Q(ac_lrclk_count_reg__0),
        .\R_bus_in_s_reg[17] (Q),
        .\R_bus_in_s_reg[17]_0 (\R_bus_in_s_reg[17] ),
        .\R_unsigned_data_prev_reg[8] (\R_unsigned_data_prev_reg[8] ),
        .\R_unsigned_data_prev_reg[9] (\R_unsigned_data_prev_reg[9] ),
        .SR(BCLK_int_reg),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .\ac_lrclk_count_reg[0] (audio_inout_n_3),
        .ac_lrclk_sig_prev_reg(audio_inout_n_72),
        .ac_lrclk_sig_prev_reg_0(ac_lrclk_sig_prev_reg_n_0),
        .\axi_araddr_reg[2]_rep (\axi_araddr_reg[2]_rep ),
        .\axi_araddr_reg[3]_rep (\axi_araddr_reg[3]_rep ),
        .\axi_araddr_reg[4] (\axi_araddr_reg[4] ),
        .\axi_araddr_reg[4]_0 (\axi_araddr_reg[4]_0 ),
        .\axi_araddr_reg[4]_1 (\axi_rdata_reg[0]_i_4_n_0 ),
        .\axi_araddr_reg[4]_10 (\axi_araddr_reg[4]_9 ),
        .\axi_araddr_reg[4]_11 (\axi_araddr_reg[4]_10 ),
        .\axi_araddr_reg[4]_12 (\axi_araddr_reg[4]_11 ),
        .\axi_araddr_reg[4]_13 (\axi_araddr_reg[4]_12 ),
        .\axi_araddr_reg[4]_14 (\axi_araddr_reg[4]_13 ),
        .\axi_araddr_reg[4]_15 (\axi_araddr_reg[4]_14 ),
        .\axi_araddr_reg[4]_16 (\axi_araddr_reg[4]_15 ),
        .\axi_araddr_reg[4]_17 (\axi_araddr_reg[4]_16 ),
        .\axi_araddr_reg[4]_18 (\axi_araddr_reg[4]_17 ),
        .\axi_araddr_reg[4]_19 (\axi_araddr_reg[4]_18 ),
        .\axi_araddr_reg[4]_2 (\axi_araddr_reg[4]_1 ),
        .\axi_araddr_reg[4]_20 (\axi_araddr_reg[4]_19 ),
        .\axi_araddr_reg[4]_21 (\axi_araddr_reg[4]_20 ),
        .\axi_araddr_reg[4]_22 (\axi_araddr_reg[4]_21 ),
        .\axi_araddr_reg[4]_23 (\axi_araddr_reg[4]_22 ),
        .\axi_araddr_reg[4]_24 (\axi_araddr_reg[4]_23 ),
        .\axi_araddr_reg[4]_25 (\axi_araddr_reg[4]_24 ),
        .\axi_araddr_reg[4]_26 (\axi_araddr_reg[4]_25 ),
        .\axi_araddr_reg[4]_27 (\axi_araddr_reg[4]_26 ),
        .\axi_araddr_reg[4]_28 (\axi_araddr_reg[4]_27 ),
        .\axi_araddr_reg[4]_29 (\axi_araddr_reg[4]_28 ),
        .\axi_araddr_reg[4]_3 (\axi_araddr_reg[4]_2 ),
        .\axi_araddr_reg[4]_30 (\axi_araddr_reg[4]_29 ),
        .\axi_araddr_reg[4]_31 (\axi_araddr_reg[4]_30 ),
        .\axi_araddr_reg[4]_32 (\axi_araddr_reg[4]_31 ),
        .\axi_araddr_reg[4]_33 (\axi_araddr_reg[4]_32 ),
        .\axi_araddr_reg[4]_34 (\axi_araddr_reg[4]_33 ),
        .\axi_araddr_reg[4]_35 (\axi_araddr_reg[4]_34 ),
        .\axi_araddr_reg[4]_36 (\axi_araddr_reg[4]_35 ),
        .\axi_araddr_reg[4]_37 (\axi_araddr_reg[4]_36 ),
        .\axi_araddr_reg[4]_38 (\axi_araddr_reg[4]_37 ),
        .\axi_araddr_reg[4]_39 (\axi_araddr_reg[4]_38 ),
        .\axi_araddr_reg[4]_4 (\axi_araddr_reg[4]_3 ),
        .\axi_araddr_reg[4]_40 (\axi_araddr_reg[4]_39 ),
        .\axi_araddr_reg[4]_41 (\axi_araddr_reg[4]_40 ),
        .\axi_araddr_reg[4]_42 (\axi_araddr_reg[4]_41 ),
        .\axi_araddr_reg[4]_43 (\axi_araddr_reg[4]_42 ),
        .\axi_araddr_reg[4]_5 (\axi_araddr_reg[4]_4 ),
        .\axi_araddr_reg[4]_6 (\axi_araddr_reg[4]_5 ),
        .\axi_araddr_reg[4]_7 (\axi_araddr_reg[4]_6 ),
        .\axi_araddr_reg[4]_8 (\axi_araddr_reg[4]_7 ),
        .\axi_araddr_reg[4]_9 (\axi_araddr_reg[4]_8 ),
        .\axi_araddr_reg[6] (\axi_araddr_reg[6] ),
        .\axi_rdata_reg[15] (\axi_rdata_reg[15] ),
        .\axi_rdata_reg[2] (\axi_rdata_reg[2] ),
        .clk(clk),
        .\int_trigger_volt_s_reg[9] (\int_trigger_volt_s_reg[9] ),
        .\processQ_reg[9] (\processQ_reg[9] ),
        .ready_sig_reg(audio_inout_n_73),
        .ready_sig_reg_0(CLK),
        .reset_n(reset_n),
        .\sdp_bl.ramb18_dp_bl.ram18_bl (\sdp_bl.ramb18_dp_bl.ram18_bl ),
        .\slv_reg11_reg[1] (\slv_reg11_reg[1] ),
        .\slv_reg11_reg[3] (\slv_reg11_reg[3] ),
        .\slv_reg11_reg[4] (\slv_reg11_reg[4] ),
        .\slv_reg11_reg[5] (\slv_reg11_reg[5] ),
        .\slv_reg11_reg[6] (\slv_reg11_reg[6] ),
        .\slv_reg11_reg[7] (\slv_reg11_reg[7] ),
        .\slv_reg11_reg[8] (\slv_reg11_reg[8] ),
        .\slv_reg11_reg[9] (\slv_reg11_reg[9] ),
        .\slv_reg1_reg[9] (\slv_reg1_reg[9] ),
        .\slv_reg2_reg[9] (\slv_reg2_reg[9] ),
        .\slv_reg3_reg[0] (\slv_reg3_reg[0] ),
        .\slv_reg3_reg[10] (\slv_reg3_reg[10] ),
        .\slv_reg3_reg[11] (\slv_reg3_reg[11] ),
        .\slv_reg3_reg[12] (\slv_reg3_reg[12] ),
        .\slv_reg3_reg[13] (\slv_reg3_reg[13] ),
        .\slv_reg3_reg[14] (\slv_reg3_reg[14] ),
        .\slv_reg3_reg[15] (\slv_reg3_reg[15] ),
        .\slv_reg3_reg[1] (\slv_reg3_reg[1] ),
        .\slv_reg3_reg[2] (\slv_reg3_reg[2] ),
        .\slv_reg3_reg[3] (\slv_reg3_reg[3] ),
        .\slv_reg3_reg[4] (\slv_reg3_reg[4] ),
        .\slv_reg3_reg[5] (\slv_reg3_reg[5] ),
        .\slv_reg3_reg[6] (\slv_reg3_reg[6] ),
        .\slv_reg3_reg[7] (\slv_reg3_reg[7] ),
        .\slv_reg3_reg[8] (\slv_reg3_reg[8] ),
        .\slv_reg3_reg[9] (\slv_reg3_reg[9] ),
        .\slv_reg4_reg[15] (\slv_reg4_reg[15] ),
        .\slv_reg5_reg[15] (\slv_reg5_reg[15] ),
        .state(state),
        .\state_reg[0] (\state_reg[0] ),
        .switch(switch));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1 audiocodec_master_clock
       (.clk_in1(clk),
        .clk_out1(ac_mclk),
        .clk_out2(clk_50),
        .lopt(lopt),
        .resetn(reset_n));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_10 
       (.I0(\slv_reg11_reg[9] [0]),
        .I1(\slv_reg10_reg[0] ),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(CLK),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(flagQ),
        .O(\axi_rdata[0]_i_10_n_0 ));
  MUXF7 \axi_rdata_reg[0]_i_4 
       (.I0(\axi_rdata[0]_i_10_n_0 ),
        .I1(\slv_reg15_reg[0] ),
        .O(\axi_rdata_reg[0]_i_4_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init initialize_audio
       (.SR(BCLK_int_reg),
        .clk_out2(clk_50),
        .reset_n(reset_n),
        .scl(scl),
        .sda(sda));
  FDRE ready_sig_reg
       (.C(clk),
        .CE(1'b1),
        .D(audio_inout_n_73),
        .Q(CLK),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder
   (D,
    Q,
    \encoded_reg[8]_0 ,
    \encoded_reg[8]_1 ,
    \encoded_reg[8]_2 ,
    \encoded_reg[8]_3 ,
    \encoded_reg[8]_4 ,
    \encoded_reg[8]_5 ,
    \encoded_reg[8]_6 ,
    \encoded_reg[9]_0 ,
    \processQ_reg[6] ,
    CLK,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[3]_2 ,
    \dc_bias_reg[3]_3 ,
    \dc_bias_reg[3]_4 ,
    \processQ_reg[8] ,
    \processQ_reg[5] ,
    \processQ_reg[8]_0 ,
    \processQ_reg[5]_0 ,
    \slv_reg11_reg[7] ,
    \slv_reg11_reg[5] ,
    \slv_reg11_reg[3] ,
    \slv_reg11_reg[4] ,
    \slv_reg11_reg[6] ,
    \slv_reg11_reg[1] ,
    \int_trigger_volt_s_reg[3] ,
    \slv_reg11_reg[2] ,
    \slv_reg11_reg[1]_0 ,
    \slv_reg5_reg[0] ,
    \int_trigger_volt_s_reg[1] ,
    SR);
  output [5:0]D;
  output [0:0]Q;
  output \encoded_reg[8]_0 ;
  output \encoded_reg[8]_1 ;
  output \encoded_reg[8]_2 ;
  output \encoded_reg[8]_3 ;
  output \encoded_reg[8]_4 ;
  output \encoded_reg[8]_5 ;
  output \encoded_reg[8]_6 ;
  output \encoded_reg[9]_0 ;
  input \processQ_reg[6] ;
  input CLK;
  input \dc_bias_reg[3]_0 ;
  input \dc_bias_reg[3]_1 ;
  input \dc_bias_reg[3]_2 ;
  input \dc_bias_reg[3]_3 ;
  input \dc_bias_reg[3]_4 ;
  input \processQ_reg[8] ;
  input \processQ_reg[5] ;
  input \processQ_reg[8]_0 ;
  input \processQ_reg[5]_0 ;
  input \slv_reg11_reg[7] ;
  input \slv_reg11_reg[5] ;
  input \slv_reg11_reg[3] ;
  input \slv_reg11_reg[4] ;
  input \slv_reg11_reg[6] ;
  input \slv_reg11_reg[1] ;
  input \int_trigger_volt_s_reg[3] ;
  input \slv_reg11_reg[2] ;
  input [1:0]\slv_reg11_reg[1]_0 ;
  input [0:0]\slv_reg5_reg[0] ;
  input [1:0]\int_trigger_volt_s_reg[1] ;
  input [0:0]SR;

  wire CLK;
  wire [5:0]D;
  wire [0:0]Q;
  wire [0:0]SR;
  wire \dc_bias[0]_i_1__0_n_0 ;
  wire \dc_bias[1]_i_1__0_n_0 ;
  wire \dc_bias[2]_i_1_n_0 ;
  wire \dc_bias[3]_i_1__0_n_0 ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire \dc_bias_reg[3]_3 ;
  wire \dc_bias_reg[3]_4 ;
  wire \dc_bias_reg_n_0_[0] ;
  wire \dc_bias_reg_n_0_[1] ;
  wire \dc_bias_reg_n_0_[2] ;
  wire \encoded[8]_i_1_n_0 ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \encoded_reg[8]_2 ;
  wire \encoded_reg[8]_3 ;
  wire \encoded_reg[8]_4 ;
  wire \encoded_reg[8]_5 ;
  wire \encoded_reg[8]_6 ;
  wire \encoded_reg[9]_0 ;
  wire [1:0]\int_trigger_volt_s_reg[1] ;
  wire \int_trigger_volt_s_reg[3] ;
  wire \processQ_reg[5] ;
  wire \processQ_reg[5]_0 ;
  wire \processQ_reg[6] ;
  wire \processQ_reg[8] ;
  wire \processQ_reg[8]_0 ;
  wire \slv_reg11_reg[1] ;
  wire [1:0]\slv_reg11_reg[1]_0 ;
  wire \slv_reg11_reg[2] ;
  wire \slv_reg11_reg[3] ;
  wire \slv_reg11_reg[4] ;
  wire \slv_reg11_reg[5] ;
  wire \slv_reg11_reg[6] ;
  wire \slv_reg11_reg[7] ;
  wire [0:0]\slv_reg5_reg[0] ;

  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[0]_i_1__0 
       (.I0(Q),
        .I1(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT5 #(
    .INIT(32'h69966666)) 
    \dc_bias[1]_i_1__0 
       (.I0(\processQ_reg[5] ),
        .I1(\dc_bias_reg_n_0_[1] ),
        .I2(\dc_bias_reg_n_0_[0] ),
        .I3(\processQ_reg[8]_0 ),
        .I4(Q),
        .O(\dc_bias[1]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h18E710EF00FFA25D)) 
    \dc_bias[2]_i_1 
       (.I0(Q),
        .I1(\processQ_reg[8] ),
        .I2(\processQ_reg[5] ),
        .I3(\dc_bias_reg_n_0_[2] ),
        .I4(\dc_bias_reg_n_0_[0] ),
        .I5(\dc_bias_reg_n_0_[1] ),
        .O(\dc_bias[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000A2E7EFFFFF)) 
    \dc_bias[3]_i_1__0 
       (.I0(Q),
        .I1(\processQ_reg[8] ),
        .I2(\processQ_reg[5] ),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(\dc_bias_reg_n_0_[1] ),
        .I5(\dc_bias_reg_n_0_[2] ),
        .O(\dc_bias[3]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1__0_n_0 ),
        .Q(\dc_bias_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[1]_i_1__0_n_0 ),
        .Q(\dc_bias_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[2]_i_1_n_0 ),
        .Q(\dc_bias_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[3]_i_1__0_n_0 ),
        .Q(Q),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \encoded[8]_i_1 
       (.I0(\processQ_reg[8]_0 ),
        .I1(\dc_bias_reg_n_0_[1] ),
        .I2(\dc_bias_reg_n_0_[0] ),
        .I3(\dc_bias_reg_n_0_[2] ),
        .I4(Q),
        .I5(\processQ_reg[5]_0 ),
        .O(\encoded[8]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \encoded[8]_i_4 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(Q),
        .O(\encoded_reg[9]_0 ));
  FDRE \encoded_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_3 ),
        .Q(D[0]),
        .R(1'b0));
  FDRE \encoded_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_2 ),
        .Q(D[1]),
        .R(1'b0));
  FDRE \encoded_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_1 ),
        .Q(D[2]),
        .R(1'b0));
  FDRE \encoded_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_0 ),
        .Q(D[3]),
        .R(1'b0));
  FDSE \encoded_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\processQ_reg[6] ),
        .Q(D[4]),
        .S(\encoded[8]_i_1_n_0 ));
  FDRE \encoded_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_4 ),
        .Q(D[5]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h8888008000000000)) 
    i__carry__0_i_3__5
       (.I0(\slv_reg11_reg[7] ),
        .I1(\slv_reg11_reg[5] ),
        .I2(\slv_reg11_reg[3] ),
        .I3(\encoded_reg[8]_1 ),
        .I4(\slv_reg11_reg[4] ),
        .I5(\slv_reg11_reg[6] ),
        .O(\encoded_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hA888888800000000)) 
    i__carry__0_i_3__6
       (.I0(\slv_reg11_reg[6] ),
        .I1(\slv_reg11_reg[4] ),
        .I2(\slv_reg11_reg[1] ),
        .I3(\encoded_reg[8]_3 ),
        .I4(\int_trigger_volt_s_reg[3] ),
        .I5(\slv_reg11_reg[5] ),
        .O(\encoded_reg[8]_2 ));
  LUT6 #(
    .INIT(64'h8088800000000000)) 
    i__carry_i_10__6
       (.I0(\slv_reg11_reg[2] ),
        .I1(\slv_reg11_reg[3] ),
        .I2(\slv_reg11_reg[1]_0 [0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\int_trigger_volt_s_reg[1] [0]),
        .I5(\slv_reg11_reg[1] ),
        .O(\encoded_reg[8]_6 ));
  LUT6 #(
    .INIT(64'h0000000000440347)) 
    i__carry_i_10__8
       (.I0(\slv_reg11_reg[1]_0 [0]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[1] [0]),
        .I3(\slv_reg11_reg[1]_0 [1]),
        .I4(\int_trigger_volt_s_reg[1] [1]),
        .I5(\slv_reg11_reg[2] ),
        .O(\encoded_reg[8]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_11__2
       (.I0(\slv_reg11_reg[1]_0 [0]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[1] [0]),
        .O(\encoded_reg[8]_3 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA88888880)) 
    i__carry_i_9__6
       (.I0(\slv_reg11_reg[5] ),
        .I1(\slv_reg11_reg[3] ),
        .I2(\slv_reg11_reg[2] ),
        .I3(\slv_reg11_reg[1] ),
        .I4(\encoded_reg[8]_3 ),
        .I5(\slv_reg11_reg[4] ),
        .O(\encoded_reg[8]_4 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA80000000)) 
    i__carry_i_9__7
       (.I0(\slv_reg11_reg[5] ),
        .I1(\slv_reg11_reg[2] ),
        .I2(\slv_reg11_reg[3] ),
        .I3(\encoded_reg[8]_3 ),
        .I4(\slv_reg11_reg[1] ),
        .I5(\slv_reg11_reg[4] ),
        .O(\encoded_reg[8]_5 ));
endmodule

(* ORIG_REF_NAME = "TDMS_encoder" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1
   (D,
    Q,
    CLK,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \processQ_reg[9] ,
    \processQ_reg[5] ,
    SR);
  output [3:0]D;
  output [0:0]Q;
  input CLK;
  input \dc_bias_reg[3]_0 ;
  input \dc_bias_reg[3]_1 ;
  input \processQ_reg[9] ;
  input \processQ_reg[5] ;
  input [0:0]SR;

  wire CLK;
  wire [3:0]D;
  wire [0:0]Q;
  wire [0:0]SR;
  wire \dc_bias[0]_i_1_n_0 ;
  wire \dc_bias[1]_i_1_n_0 ;
  wire \dc_bias[2]_i_1__1_n_0 ;
  wire \dc_bias[3]_i_1_n_0 ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg_n_0_[0] ;
  wire \dc_bias_reg_n_0_[1] ;
  wire \dc_bias_reg_n_0_[2] ;
  wire \encoded[2]_i_1__1_n_0 ;
  wire \encoded[8]_i_1__1_n_0 ;
  wire \encoded[8]_i_2_n_0 ;
  wire \processQ_reg[5] ;
  wire \processQ_reg[9] ;

  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[0]_i_1 
       (.I0(Q),
        .I1(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT4 #(
    .INIT(16'h69AA)) 
    \dc_bias[1]_i_1 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\processQ_reg[9] ),
        .I3(Q),
        .O(\dc_bias[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT5 #(
    .INIT(32'h95565555)) 
    \dc_bias[2]_i_1__1 
       (.I0(\dc_bias_reg_n_0_[2] ),
        .I1(\dc_bias_reg_n_0_[1] ),
        .I2(\dc_bias_reg_n_0_[0] ),
        .I3(\processQ_reg[9] ),
        .I4(Q),
        .O(\dc_bias[2]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT5 #(
    .INIT(32'h00027FFF)) 
    \dc_bias[3]_i_1 
       (.I0(Q),
        .I1(\processQ_reg[9] ),
        .I2(\dc_bias_reg_n_0_[0] ),
        .I3(\dc_bias_reg_n_0_[1] ),
        .I4(\dc_bias_reg_n_0_[2] ),
        .O(\dc_bias[3]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1_n_0 ),
        .Q(\dc_bias_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[1]_i_1_n_0 ),
        .Q(\dc_bias_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[2]_i_1__1_n_0 ),
        .Q(\dc_bias_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[3]_i_1_n_0 ),
        .Q(Q),
        .R(SR));
  LUT2 #(
    .INIT(4'hB)) 
    \encoded[2]_i_1__1 
       (.I0(Q),
        .I1(\processQ_reg[5] ),
        .O(\encoded[2]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \encoded[8]_i_1__1 
       (.I0(\processQ_reg[9] ),
        .I1(\dc_bias_reg_n_0_[2] ),
        .I2(\dc_bias_reg_n_0_[0] ),
        .I3(\dc_bias_reg_n_0_[1] ),
        .I4(Q),
        .I5(\processQ_reg[5] ),
        .O(\encoded[8]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBBBBBBBBBB3)) 
    \encoded[8]_i_2 
       (.I0(\processQ_reg[9] ),
        .I1(\processQ_reg[5] ),
        .I2(Q),
        .I3(\dc_bias_reg_n_0_[1] ),
        .I4(\dc_bias_reg_n_0_[0] ),
        .I5(\dc_bias_reg_n_0_[2] ),
        .O(\encoded[8]_i_2_n_0 ));
  FDRE \encoded_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_0 ),
        .Q(D[0]),
        .R(1'b0));
  FDRE \encoded_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\encoded[2]_i_1__1_n_0 ),
        .Q(D[1]),
        .R(1'b0));
  FDSE \encoded_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\encoded[8]_i_2_n_0 ),
        .Q(D[2]),
        .S(\encoded[8]_i_1__1_n_0 ));
  FDRE \encoded_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_1 ),
        .Q(D[3]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "TDMS_encoder" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2
   (D,
    Q,
    \encoded_reg[0]_0 ,
    \int_trigger_time_s_reg[0] ,
    \int_trigger_time_s_reg[0]_0 ,
    \encoded_reg[0]_1 ,
    \encoded_reg[0]_2 ,
    \encoded_reg[0]_3 ,
    \encoded_reg[0]_4 ,
    \encoded_reg[0]_5 ,
    \encoded_reg[0]_6 ,
    \encoded_reg[0]_7 ,
    \encoded_reg[8]_0 ,
    \encoded_reg[8]_1 ,
    \state_reg[0] ,
    \state_reg[0]_0 ,
    \encoded_reg[8]_2 ,
    \state_reg[0]_1 ,
    \state_reg[0]_2 ,
    \encoded_reg[8]_3 ,
    \encoded_reg[8]_4 ,
    \state_reg[0]_3 ,
    \encoded_reg[8]_5 ,
    \encoded_reg[8]_6 ,
    \encoded_reg[8]_7 ,
    \encoded_reg[8]_8 ,
    \encoded_reg[8]_9 ,
    \encoded_reg[8]_10 ,
    \encoded_reg[8]_11 ,
    \encoded_reg[8]_12 ,
    \encoded_reg[8]_13 ,
    \encoded_reg[8]_14 ,
    \state_reg[0]_4 ,
    \encoded_reg[8]_15 ,
    \processQ_reg[5] ,
    CLK,
    \dc_bias_reg[3]_0 ,
    \processQ_reg[5]_0 ,
    \processQ_reg[5]_1 ,
    \slv_reg10_reg[7] ,
    \slv_reg10_reg[2] ,
    \slv_reg10_reg[4] ,
    \slv_reg10_reg[6] ,
    \slv_reg10_reg[1] ,
    \slv_reg10_reg[5] ,
    \slv_reg5_reg[0] ,
    \int_trigger_time_s_reg[5] ,
    \processQ_reg[9] ,
    \slv_reg11_reg[9] ,
    \int_trigger_volt_s_reg[9] ,
    \slv_reg11_reg[0] ,
    SR);
  output [4:0]D;
  output [0:0]Q;
  output \encoded_reg[0]_0 ;
  output \int_trigger_time_s_reg[0] ;
  output \int_trigger_time_s_reg[0]_0 ;
  output \encoded_reg[0]_1 ;
  output \encoded_reg[0]_2 ;
  output \encoded_reg[0]_3 ;
  output \encoded_reg[0]_4 ;
  output \encoded_reg[0]_5 ;
  output \encoded_reg[0]_6 ;
  output \encoded_reg[0]_7 ;
  output \encoded_reg[8]_0 ;
  output \encoded_reg[8]_1 ;
  output \state_reg[0] ;
  output \state_reg[0]_0 ;
  output \encoded_reg[8]_2 ;
  output \state_reg[0]_1 ;
  output \state_reg[0]_2 ;
  output \encoded_reg[8]_3 ;
  output \encoded_reg[8]_4 ;
  output \state_reg[0]_3 ;
  output \encoded_reg[8]_5 ;
  output \encoded_reg[8]_6 ;
  output \encoded_reg[8]_7 ;
  output \encoded_reg[8]_8 ;
  output \encoded_reg[8]_9 ;
  output \encoded_reg[8]_10 ;
  output \encoded_reg[8]_11 ;
  output \encoded_reg[8]_12 ;
  output \encoded_reg[8]_13 ;
  output \encoded_reg[8]_14 ;
  output \state_reg[0]_4 ;
  output \encoded_reg[8]_15 ;
  input \processQ_reg[5] ;
  input CLK;
  input \dc_bias_reg[3]_0 ;
  input \processQ_reg[5]_0 ;
  input \processQ_reg[5]_1 ;
  input \slv_reg10_reg[7] ;
  input \slv_reg10_reg[2] ;
  input \slv_reg10_reg[4] ;
  input \slv_reg10_reg[6] ;
  input \slv_reg10_reg[1] ;
  input [3:0]\slv_reg10_reg[5] ;
  input [0:0]\slv_reg5_reg[0] ;
  input [3:0]\int_trigger_time_s_reg[5] ;
  input \processQ_reg[9] ;
  input [9:0]\slv_reg11_reg[9] ;
  input [9:0]\int_trigger_volt_s_reg[9] ;
  input \slv_reg11_reg[0] ;
  input [0:0]SR;

  wire CLK;
  wire [4:0]D;
  wire [0:0]Q;
  wire [0:0]SR;
  wire \dc_bias[0]_i_1__1_n_0 ;
  wire \dc_bias[1]_i_1__1_n_0 ;
  wire \dc_bias[2]_i_1__0_n_0 ;
  wire \dc_bias[3]_i_2__0_n_0 ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg_n_0_[0] ;
  wire \dc_bias_reg_n_0_[1] ;
  wire \dc_bias_reg_n_0_[2] ;
  wire [8:8]encoded0_in;
  wire [3:0]encoded1_in;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[0]_1 ;
  wire \encoded_reg[0]_2 ;
  wire \encoded_reg[0]_3 ;
  wire \encoded_reg[0]_4 ;
  wire \encoded_reg[0]_5 ;
  wire \encoded_reg[0]_6 ;
  wire \encoded_reg[0]_7 ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \encoded_reg[8]_10 ;
  wire \encoded_reg[8]_11 ;
  wire \encoded_reg[8]_12 ;
  wire \encoded_reg[8]_13 ;
  wire \encoded_reg[8]_14 ;
  wire \encoded_reg[8]_15 ;
  wire \encoded_reg[8]_2 ;
  wire \encoded_reg[8]_3 ;
  wire \encoded_reg[8]_4 ;
  wire \encoded_reg[8]_5 ;
  wire \encoded_reg[8]_6 ;
  wire \encoded_reg[8]_7 ;
  wire \encoded_reg[8]_8 ;
  wire \encoded_reg[8]_9 ;
  wire \int_trigger_time_s_reg[0] ;
  wire \int_trigger_time_s_reg[0]_0 ;
  wire [3:0]\int_trigger_time_s_reg[5] ;
  wire [9:0]\int_trigger_volt_s_reg[9] ;
  wire \processQ_reg[5] ;
  wire \processQ_reg[5]_0 ;
  wire \processQ_reg[5]_1 ;
  wire \processQ_reg[9] ;
  wire \slv_reg10_reg[1] ;
  wire \slv_reg10_reg[2] ;
  wire \slv_reg10_reg[4] ;
  wire [3:0]\slv_reg10_reg[5] ;
  wire \slv_reg10_reg[6] ;
  wire \slv_reg10_reg[7] ;
  wire \slv_reg11_reg[0] ;
  wire [9:0]\slv_reg11_reg[9] ;
  wire [0:0]\slv_reg5_reg[0] ;
  wire \state_reg[0] ;
  wire \state_reg[0]_0 ;
  wire \state_reg[0]_1 ;
  wire \state_reg[0]_2 ;
  wire \state_reg[0]_3 ;
  wire \state_reg[0]_4 ;

  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT5 #(
    .INIT(32'h99996662)) 
    \dc_bias[0]_i_1__1 
       (.I0(Q),
        .I1(\processQ_reg[5]_0 ),
        .I2(\dc_bias_reg_n_0_[1] ),
        .I3(\dc_bias_reg_n_0_[2] ),
        .I4(\dc_bias_reg_n_0_[0] ),
        .O(\dc_bias[0]_i_1__1_n_0 ));
  LUT5 #(
    .INIT(32'h659A596A)) 
    \dc_bias[1]_i_1__1 
       (.I0(\dc_bias_reg_n_0_[1] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(Q),
        .I3(\processQ_reg[5]_0 ),
        .I4(\processQ_reg[9] ),
        .O(\dc_bias[1]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h63696363C3C39983)) 
    \dc_bias[2]_i_1__0 
       (.I0(Q),
        .I1(\dc_bias_reg_n_0_[2] ),
        .I2(\processQ_reg[5]_0 ),
        .I3(\processQ_reg[9] ),
        .I4(\dc_bias_reg_n_0_[0] ),
        .I5(\dc_bias_reg_n_0_[1] ),
        .O(\dc_bias[2]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h3F1F3F07000F000F)) 
    \dc_bias[3]_i_2__0 
       (.I0(\dc_bias_reg_n_0_[0] ),
        .I1(\dc_bias_reg_n_0_[1] ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\processQ_reg[5]_0 ),
        .I4(\processQ_reg[9] ),
        .I5(Q),
        .O(\dc_bias[3]_i_2__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1__1_n_0 ),
        .Q(\dc_bias_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[1]_i_1__1_n_0 ),
        .Q(\dc_bias_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[2]_i_1__0_n_0 ),
        .Q(\dc_bias_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias[3]_i_2__0_n_0 ),
        .Q(Q),
        .R(SR));
  LUT6 #(
    .INIT(64'h88888888888888A8)) 
    \encoded[0]_i_1__0 
       (.I0(\processQ_reg[5]_1 ),
        .I1(Q),
        .I2(\processQ_reg[5]_0 ),
        .I3(\dc_bias_reg_n_0_[2] ),
        .I4(\dc_bias_reg_n_0_[0] ),
        .I5(\dc_bias_reg_n_0_[1] ),
        .O(encoded1_in[0]));
  LUT6 #(
    .INIT(64'hDDDDDDDDDDDDDDFD)) 
    \encoded[2]_i_1__0 
       (.I0(\processQ_reg[5]_1 ),
        .I1(Q),
        .I2(\processQ_reg[5]_0 ),
        .I3(\dc_bias_reg_n_0_[2] ),
        .I4(\dc_bias_reg_n_0_[0] ),
        .I5(\dc_bias_reg_n_0_[1] ),
        .O(encoded1_in[2]));
  LUT6 #(
    .INIT(64'h6666666200000000)) 
    \encoded[3]_i_1 
       (.I0(Q),
        .I1(\processQ_reg[5]_0 ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(\dc_bias_reg_n_0_[1] ),
        .I5(\processQ_reg[5]_1 ),
        .O(encoded1_in[3]));
  LUT6 #(
    .INIT(64'h0000000000010000)) 
    \encoded[8]_i_1__0 
       (.I0(\dc_bias_reg_n_0_[2] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\dc_bias_reg_n_0_[1] ),
        .I3(Q),
        .I4(\processQ_reg[5]_1 ),
        .I5(\processQ_reg[9] ),
        .O(encoded0_in));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \encoded[8]_i_3__0 
       (.I0(\dc_bias_reg_n_0_[2] ),
        .I1(\dc_bias_reg_n_0_[0] ),
        .I2(\dc_bias_reg_n_0_[1] ),
        .I3(Q),
        .O(\encoded_reg[8]_15 ));
  FDRE \encoded_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(encoded1_in[0]),
        .Q(D[0]),
        .R(1'b0));
  FDRE \encoded_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(encoded1_in[2]),
        .Q(D[1]),
        .R(1'b0));
  FDRE \encoded_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(encoded1_in[3]),
        .Q(D[2]),
        .R(1'b0));
  FDSE \encoded_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\processQ_reg[5] ),
        .Q(D[3]),
        .S(encoded0_in));
  FDRE \encoded_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_0 ),
        .Q(D[4]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAA8888800000000)) 
    i__carry__0_i_3__3
       (.I0(\slv_reg10_reg[6] ),
        .I1(\slv_reg10_reg[4] ),
        .I2(\slv_reg10_reg[1] ),
        .I3(\slv_reg10_reg[2] ),
        .I4(\int_trigger_time_s_reg[0]_0 ),
        .I5(\int_trigger_time_s_reg[0] ),
        .O(\encoded_reg[0]_2 ));
  LUT6 #(
    .INIT(64'hAAA8888800000000)) 
    i__carry__0_i_3__7
       (.I0(\state_reg[0]_2 ),
        .I1(\state_reg[0]_1 ),
        .I2(\state_reg[0]_3 ),
        .I3(\slv_reg11_reg[0] ),
        .I4(\encoded_reg[8]_5 ),
        .I5(\state_reg[0] ),
        .O(\encoded_reg[8]_4 ));
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry__0_i_3__8
       (.I0(\slv_reg11_reg[9] [8]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[9] [8]),
        .O(\state_reg[0]_4 ));
  LUT6 #(
    .INIT(64'hA888888800000000)) 
    i__carry__0_i_4
       (.I0(\slv_reg10_reg[6] ),
        .I1(\slv_reg10_reg[4] ),
        .I2(\slv_reg10_reg[1] ),
        .I3(\slv_reg10_reg[2] ),
        .I4(\int_trigger_time_s_reg[0]_0 ),
        .I5(\int_trigger_time_s_reg[0] ),
        .O(\encoded_reg[0]_3 ));
  LUT6 #(
    .INIT(64'hA888888800000000)) 
    i__carry__0_i_4__0
       (.I0(\state_reg[0]_2 ),
        .I1(\state_reg[0]_1 ),
        .I2(\encoded_reg[8]_2 ),
        .I3(\state_reg[0]_3 ),
        .I4(\state_reg[0]_0 ),
        .I5(\state_reg[0] ),
        .O(\encoded_reg[8]_7 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry__0_i_5__0
       (.I0(\slv_reg11_reg[9] [9]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[9] [9]),
        .O(\encoded_reg[8]_14 ));
  LUT6 #(
    .INIT(64'h0151555555555555)) 
    i__carry_i_10__1
       (.I0(\slv_reg10_reg[4] ),
        .I1(\int_trigger_time_s_reg[5] [0]),
        .I2(\slv_reg5_reg[0] ),
        .I3(\slv_reg10_reg[5] [0]),
        .I4(\slv_reg10_reg[2] ),
        .I5(\int_trigger_time_s_reg[0]_0 ),
        .O(\encoded_reg[0]_5 ));
  LUT5 #(
    .INIT(32'h00053305)) 
    i__carry_i_10__2
       (.I0(\int_trigger_time_s_reg[5] [0]),
        .I1(\slv_reg10_reg[5] [0]),
        .I2(\int_trigger_time_s_reg[5] [1]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\slv_reg10_reg[5] [1]),
        .O(\encoded_reg[0]_7 ));
  LUT6 #(
    .INIT(64'h0151555555555555)) 
    i__carry_i_10__5
       (.I0(\state_reg[0]_1 ),
        .I1(\int_trigger_volt_s_reg[9] [2]),
        .I2(\slv_reg5_reg[0] ),
        .I3(\slv_reg11_reg[9] [2]),
        .I4(\state_reg[0]_3 ),
        .I5(\state_reg[0]_0 ),
        .O(\encoded_reg[8]_10 ));
  LUT6 #(
    .INIT(64'h8888888880888000)) 
    i__carry_i_10__7
       (.I0(\encoded_reg[8]_2 ),
        .I1(\state_reg[0]_0 ),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\int_trigger_volt_s_reg[9] [0]),
        .I5(\state_reg[0]_3 ),
        .O(\encoded_reg[8]_11 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_11
       (.I0(\slv_reg10_reg[5] [3]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_time_s_reg[5] [3]),
        .O(\int_trigger_time_s_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_11__1
       (.I0(\slv_reg11_reg[9] [5]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[9] [5]),
        .O(\state_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_12__1
       (.I0(\slv_reg11_reg[9] [7]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[9] [7]),
        .O(\encoded_reg[8]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_13__0
       (.I0(\slv_reg11_reg[9] [4]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[9] [4]),
        .O(\state_reg[0]_1 ));
  LUT6 #(
    .INIT(64'h8A800A0080800000)) 
    i__carry_i_14
       (.I0(\int_trigger_time_s_reg[0]_0 ),
        .I1(\slv_reg10_reg[5] [1]),
        .I2(\slv_reg5_reg[0] ),
        .I3(\int_trigger_time_s_reg[5] [1]),
        .I4(\slv_reg10_reg[5] [0]),
        .I5(\int_trigger_time_s_reg[5] [0]),
        .O(\encoded_reg[0]_6 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT5 #(
    .INIT(32'hCCA000A0)) 
    i__carry_i_14__0
       (.I0(\int_trigger_volt_s_reg[9] [2]),
        .I1(\slv_reg11_reg[9] [2]),
        .I2(\int_trigger_volt_s_reg[9] [1]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\slv_reg11_reg[9] [1]),
        .O(\encoded_reg[8]_13 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_15__0
       (.I0(\slv_reg11_reg[9] [3]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[9] [3]),
        .O(\state_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_16__0
       (.I0(\slv_reg11_reg[9] [2]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[9] [2]),
        .O(\encoded_reg[8]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_17
       (.I0(\slv_reg10_reg[5] [2]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_time_s_reg[5] [2]),
        .O(\int_trigger_time_s_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_17__0
       (.I0(\slv_reg11_reg[9] [1]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[9] [1]),
        .O(\state_reg[0]_3 ));
  LUT6 #(
    .INIT(64'h8888800000000000)) 
    i__carry_i_5__1
       (.I0(\slv_reg10_reg[7] ),
        .I1(\int_trigger_time_s_reg[0] ),
        .I2(\slv_reg10_reg[2] ),
        .I3(\int_trigger_time_s_reg[0]_0 ),
        .I4(\slv_reg10_reg[4] ),
        .I5(\slv_reg10_reg[6] ),
        .O(\encoded_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h8888800000000000)) 
    i__carry_i_5__8
       (.I0(\encoded_reg[8]_1 ),
        .I1(\state_reg[0] ),
        .I2(\state_reg[0]_0 ),
        .I3(\encoded_reg[8]_2 ),
        .I4(\state_reg[0]_1 ),
        .I5(\state_reg[0]_2 ),
        .O(\encoded_reg[8]_0 ));
  LUT5 #(
    .INIT(32'hA8880000)) 
    i__carry_i_6
       (.I0(\slv_reg10_reg[6] ),
        .I1(\slv_reg10_reg[4] ),
        .I2(\int_trigger_time_s_reg[0]_0 ),
        .I3(\slv_reg10_reg[2] ),
        .I4(\int_trigger_time_s_reg[0] ),
        .O(\encoded_reg[0]_1 ));
  LUT5 #(
    .INIT(32'hA8880000)) 
    i__carry_i_6__6
       (.I0(\state_reg[0]_2 ),
        .I1(\state_reg[0]_1 ),
        .I2(\encoded_reg[8]_2 ),
        .I3(\state_reg[0]_0 ),
        .I4(\state_reg[0] ),
        .O(\encoded_reg[8]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT5 #(
    .INIT(32'hCCA000A0)) 
    i__carry_i_8__3
       (.I0(\int_trigger_volt_s_reg[9] [3]),
        .I1(\slv_reg11_reg[9] [3]),
        .I2(\int_trigger_volt_s_reg[9] [2]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\slv_reg11_reg[9] [2]),
        .O(\encoded_reg[8]_5 ));
  LUT6 #(
    .INIT(64'h0000015155555555)) 
    i__carry_i_9__4
       (.I0(\slv_reg10_reg[4] ),
        .I1(\int_trigger_time_s_reg[5] [0]),
        .I2(\slv_reg5_reg[0] ),
        .I3(\slv_reg10_reg[5] [0]),
        .I4(\slv_reg10_reg[2] ),
        .I5(\int_trigger_time_s_reg[0]_0 ),
        .O(\encoded_reg[0]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_9__5
       (.I0(\slv_reg11_reg[9] [6]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\int_trigger_volt_s_reg[9] [6]),
        .O(\state_reg[0]_2 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA80808000)) 
    i__carry_i_9__8
       (.I0(\state_reg[0] ),
        .I1(\encoded_reg[8]_2 ),
        .I2(\state_reg[0]_0 ),
        .I3(\slv_reg11_reg[0] ),
        .I4(\state_reg[0]_3 ),
        .I5(\state_reg[0]_1 ),
        .O(\encoded_reg[8]_8 ));
  LUT6 #(
    .INIT(64'hAAA8888800000000)) 
    pixel_color5_carry__0_i_3
       (.I0(\state_reg[0]_2 ),
        .I1(\state_reg[0]_1 ),
        .I2(\encoded_reg[8]_2 ),
        .I3(\state_reg[0]_3 ),
        .I4(\state_reg[0]_0 ),
        .I5(\state_reg[0] ),
        .O(\encoded_reg[8]_6 ));
  LUT6 #(
    .INIT(64'hAAAA8A8AAAA08A80)) 
    pixel_color5_carry_i_10
       (.I0(\state_reg[0]_0 ),
        .I1(\slv_reg11_reg[9] [1]),
        .I2(\slv_reg5_reg[0] ),
        .I3(\int_trigger_volt_s_reg[9] [1]),
        .I4(\slv_reg11_reg[9] [2]),
        .I5(\int_trigger_volt_s_reg[9] [2]),
        .O(\encoded_reg[8]_12 ));
  LUT6 #(
    .INIT(64'h0000015155555555)) 
    pixel_color5_carry_i_9
       (.I0(\state_reg[0]_1 ),
        .I1(\int_trigger_volt_s_reg[9] [2]),
        .I2(\slv_reg5_reg[0] ),
        .I3(\slv_reg11_reg[9] [2]),
        .I4(\state_reg[0]_3 ),
        .I5(\state_reg[0]_0 ),
        .O(\encoded_reg[8]_9 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl
   (D,
    E,
    \initA_reg[6] ,
    initEn_reg,
    scl,
    sda,
    clk_out2,
    Q,
    data_i,
    reset_n,
    stb,
    \state_reg[3] ,
    \delaycnt_reg[12] ,
    \state_reg[3]_0 ,
    \state_reg[1] ,
    \initWord_reg[6] ,
    \initA_reg[6]_0 ,
    initEn,
    msg);
  output [3:0]D;
  output [0:0]E;
  output [0:0]\initA_reg[6] ;
  output initEn_reg;
  inout scl;
  inout sda;
  input clk_out2;
  input [3:0]Q;
  input [7:0]data_i;
  input reset_n;
  input stb;
  input \state_reg[3] ;
  input \delaycnt_reg[12] ;
  input \state_reg[3]_0 ;
  input \state_reg[1] ;
  input [1:0]\initWord_reg[6] ;
  input \initA_reg[6]_0 ;
  input initEn;
  input msg;

  wire [3:0]D;
  wire DONE_O_i_1_n_0;
  wire DONE_O_i_3_n_0;
  wire DONE_O_i_4_n_0;
  wire DONE_O_i_5_n_0;
  wire [0:0]E;
  wire ERR_O_i_1_n_0;
  wire ERR_O_i_2_n_0;
  wire \FSM_gray_state[0]_i_1_n_0 ;
  wire \FSM_gray_state[0]_i_2_n_0 ;
  wire \FSM_gray_state[1]_i_1_n_0 ;
  wire \FSM_gray_state[1]_i_2_n_0 ;
  wire \FSM_gray_state[1]_i_3_n_0 ;
  wire \FSM_gray_state[2]_i_1_n_0 ;
  wire \FSM_gray_state[2]_i_2_n_0 ;
  wire \FSM_gray_state[3]_i_10_n_0 ;
  wire \FSM_gray_state[3]_i_1_n_0 ;
  wire \FSM_gray_state[3]_i_2_n_0 ;
  wire \FSM_gray_state[3]_i_4_n_0 ;
  wire \FSM_gray_state[3]_i_6_n_0 ;
  wire \FSM_gray_state[3]_i_7_n_0 ;
  wire \FSM_gray_state[3]_i_8_n_0 ;
  wire \FSM_gray_state[3]_i_9_n_0 ;
  wire \FSM_gray_state_reg[3]_i_5_n_0 ;
  wire [3:0]Q;
  wire addrNData_i_1_n_0;
  wire addrNData_reg_n_0;
  wire arbLost;
  wire [2:0]bitCount;
  wire \bitCount[0]_i_1_n_0 ;
  wire \bitCount[1]_i_1_n_0 ;
  wire \bitCount[2]_i_1_n_0 ;
  wire [6:0]busFreeCnt0;
  wire busFreeCnt0_1;
  wire \busFreeCnt[1]_i_1_n_0 ;
  wire \busFreeCnt[6]_i_3_n_0 ;
  wire [6:0]busFreeCnt_reg__0;
  wire busState0;
  wire \busState[0]_i_1_n_0 ;
  wire \busState[1]_i_1_n_0 ;
  wire \busState_reg_n_0_[0] ;
  wire \busState_reg_n_0_[1] ;
  wire clk_out2;
  wire dScl;
  wire dataByte0;
  wire dataByte1;
  wire \dataByte[7]_i_1_n_0 ;
  wire \dataByte[7]_i_4_n_0 ;
  wire \dataByte[7]_i_5_n_0 ;
  wire \dataByte_reg_n_0_[0] ;
  wire \dataByte_reg_n_0_[1] ;
  wire \dataByte_reg_n_0_[2] ;
  wire \dataByte_reg_n_0_[3] ;
  wire \dataByte_reg_n_0_[4] ;
  wire \dataByte_reg_n_0_[5] ;
  wire \dataByte_reg_n_0_[6] ;
  wire \dataByte_reg_n_0_[7] ;
  wire [7:0]data_i;
  wire ddSda;
  wire \delaycnt_reg[12] ;
  wire done;
  wire error;
  wire [0:0]\initA_reg[6] ;
  wire \initA_reg[6]_0 ;
  wire initEn;
  wire initEn_i_2_n_0;
  wire initEn_reg;
  wire [1:0]\initWord_reg[6] ;
  wire int_Rst;
  wire int_Rst_i_1_n_0;
  wire msg;
  wire nstate122_out;
  wire [0:0]p_0_in;
  wire p_0_in7_in;
  wire p_19_in;
  wire [7:0]p_1_in;
  wire p_1_in4_in;
  wire rScl;
  wire rScl_i_1_n_0;
  wire rScl_i_2_n_0;
  wire rSda;
  wire rSda_i_1_n_0;
  wire rSda_i_2_n_0;
  wire rSda_i_3_n_0;
  wire rSda_i_4_n_0;
  wire rSda_i_5_n_0;
  wire rSda_i_6_n_0;
  wire rSda_i_7_n_0;
  wire rSda_i_8_n_0;
  wire reset_n;
  wire scl;
  wire [6:0]sclCnt0;
  wire sclCnt0_0;
  wire \sclCnt[1]_i_1_n_0 ;
  wire \sclCnt[6]_i_2_n_0 ;
  wire \sclCnt[6]_i_4_n_0 ;
  wire [6:0]sclCnt_reg__0;
  wire scl_INST_0_i_1_n_0;
  wire sda;
  wire sda_INST_0_i_1_n_0;
  (* RTL_KEEP = "yes" *) wire [3:0]state;
  wire \state[1]_i_2_n_0 ;
  wire \state_reg[1] ;
  wire \state_reg[3] ;
  wire \state_reg[3]_0 ;
  wire stb;
  wire \subState[0]_i_1_n_0 ;
  wire \subState[1]_i_1_n_0 ;
  wire \subState[1]_i_2_n_0 ;
  wire \subState[1]_i_3_n_0 ;
  wire \subState_reg_n_0_[0] ;
  wire \subState_reg_n_0_[1] ;

  LUT6 #(
    .INIT(64'h888888888F888888)) 
    DONE_O_i_1
       (.I0(arbLost),
        .I1(p_0_in7_in),
        .I2(DONE_O_i_3_n_0),
        .I3(\subState[1]_i_2_n_0 ),
        .I4(\subState_reg_n_0_[0] ),
        .I5(\subState_reg_n_0_[1] ),
        .O(DONE_O_i_1_n_0));
  LUT4 #(
    .INIT(16'h0008)) 
    DONE_O_i_2
       (.I0(state[1]),
        .I1(state[0]),
        .I2(state[2]),
        .I3(state[3]),
        .O(p_0_in7_in));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAFF0CFF)) 
    DONE_O_i_3
       (.I0(DONE_O_i_4_n_0),
        .I1(addrNData_reg_n_0),
        .I2(p_0_in),
        .I3(DONE_O_i_5_n_0),
        .I4(state[2]),
        .I5(state[3]),
        .O(DONE_O_i_3_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    DONE_O_i_4
       (.I0(bitCount[2]),
        .I1(bitCount[0]),
        .I2(bitCount[1]),
        .O(DONE_O_i_4_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    DONE_O_i_5
       (.I0(state[1]),
        .I1(state[0]),
        .O(DONE_O_i_5_n_0));
  FDRE DONE_O_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(DONE_O_i_1_n_0),
        .Q(done),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000F0008800)) 
    ERR_O_i_1
       (.I0(ERR_O_i_2_n_0),
        .I1(p_0_in),
        .I2(arbLost),
        .I3(state[1]),
        .I4(state[0]),
        .I5(\subState[1]_i_3_n_0 ),
        .O(ERR_O_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h08)) 
    ERR_O_i_2
       (.I0(\subState[1]_i_2_n_0 ),
        .I1(\subState_reg_n_0_[0] ),
        .I2(\subState_reg_n_0_[1] ),
        .O(ERR_O_i_2_n_0));
  FDRE ERR_O_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(ERR_O_i_1_n_0),
        .Q(error),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h4445444544555555)) 
    \FSM_gray_state[0]_i_1 
       (.I0(state[3]),
        .I1(\FSM_gray_state[0]_i_2_n_0 ),
        .I2(state[0]),
        .I3(state[2]),
        .I4(arbLost),
        .I5(state[1]),
        .O(\FSM_gray_state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0400)) 
    \FSM_gray_state[0]_i_2 
       (.I0(int_Rst),
        .I1(stb),
        .I2(state[0]),
        .I3(state[1]),
        .O(\FSM_gray_state[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA2AAAAAA)) 
    \FSM_gray_state[1]_i_1 
       (.I0(\FSM_gray_state[1]_i_2_n_0 ),
        .I1(msg),
        .I2(state[0]),
        .I3(state[1]),
        .I4(\FSM_gray_state[1]_i_3_n_0 ),
        .O(\FSM_gray_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000A000517A017A)) 
    \FSM_gray_state[1]_i_2 
       (.I0(state[0]),
        .I1(arbLost),
        .I2(state[1]),
        .I3(state[2]),
        .I4(nstate122_out),
        .I5(state[3]),
        .O(\FSM_gray_state[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAAAAABA)) 
    \FSM_gray_state[1]_i_3 
       (.I0(state[2]),
        .I1(addrNData_reg_n_0),
        .I2(stb),
        .I3(\dataByte_reg_n_0_[0] ),
        .I4(int_Rst),
        .O(\FSM_gray_state[1]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_gray_state[1]_i_4 
       (.I0(stb),
        .I1(int_Rst),
        .O(nstate122_out));
  LUT6 #(
    .INIT(64'hEE0000000000EE0E)) 
    \FSM_gray_state[2]_i_1 
       (.I0(\FSM_gray_state[2]_i_2_n_0 ),
        .I1(state[2]),
        .I2(arbLost),
        .I3(state[1]),
        .I4(state[0]),
        .I5(state[3]),
        .O(\FSM_gray_state[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4440444044404444)) 
    \FSM_gray_state[2]_i_2 
       (.I0(state[0]),
        .I1(state[1]),
        .I2(int_Rst),
        .I3(\dataByte_reg_n_0_[0] ),
        .I4(addrNData_reg_n_0),
        .I5(stb),
        .O(\FSM_gray_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_gray_state[2]_i_3 
       (.I0(rSda),
        .I1(dScl),
        .I2(p_0_in),
        .O(arbLost));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \FSM_gray_state[3]_i_1 
       (.I0(p_19_in),
        .I1(state[3]),
        .I2(\FSM_gray_state[3]_i_4_n_0 ),
        .I3(state[2]),
        .I4(\FSM_gray_state_reg[3]_i_5_n_0 ),
        .O(\FSM_gray_state[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \FSM_gray_state[3]_i_10 
       (.I0(bitCount[0]),
        .I1(bitCount[1]),
        .I2(bitCount[2]),
        .O(\FSM_gray_state[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \FSM_gray_state[3]_i_2 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(stb),
        .I3(int_Rst),
        .I4(\FSM_gray_state[3]_i_6_n_0 ),
        .I5(msg),
        .O(\FSM_gray_state[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \FSM_gray_state[3]_i_3 
       (.I0(\subState[1]_i_2_n_0 ),
        .I1(\subState_reg_n_0_[1] ),
        .I2(\subState_reg_n_0_[0] ),
        .O(p_19_in));
  LUT6 #(
    .INIT(64'hB8BBB8B8B8B8B8B8)) 
    \FSM_gray_state[3]_i_4 
       (.I0(\FSM_gray_state[3]_i_7_n_0 ),
        .I1(state[1]),
        .I2(p_19_in),
        .I3(p_0_in),
        .I4(dScl),
        .I5(rSda),
        .O(\FSM_gray_state[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_gray_state[3]_i_6 
       (.I0(state[3]),
        .I1(state[2]),
        .O(\FSM_gray_state[3]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h20006000)) 
    \FSM_gray_state[3]_i_7 
       (.I0(state[0]),
        .I1(\subState_reg_n_0_[0] ),
        .I2(\subState_reg_n_0_[1] ),
        .I3(\subState[1]_i_2_n_0 ),
        .I4(\FSM_gray_state[3]_i_10_n_0 ),
        .O(\FSM_gray_state[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h88888888B8888888)) 
    \FSM_gray_state[3]_i_8 
       (.I0(p_19_in),
        .I1(state[0]),
        .I2(\busState_reg_n_0_[0] ),
        .I3(reset_n),
        .I4(stb),
        .I5(\busState_reg_n_0_[1] ),
        .O(\FSM_gray_state[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAAABFFFFAAAA0000)) 
    \FSM_gray_state[3]_i_9 
       (.I0(arbLost),
        .I1(bitCount[2]),
        .I2(bitCount[0]),
        .I3(bitCount[1]),
        .I4(state[0]),
        .I5(p_19_in),
        .O(\FSM_gray_state[3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[0] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[0]_i_1_n_0 ),
        .Q(state[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[1] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[1]_i_1_n_0 ),
        .Q(state[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[2] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[2]_i_1_n_0 ),
        .Q(state[2]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "stidle:0000,ststart:0001,stwrite:0011,stsack:0010,ststop:0111,stread:0110,stmack:1111,stmnackstart:0101,stmnackstop:0100" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_gray_state_reg[3] 
       (.C(clk_out2),
        .CE(\FSM_gray_state[3]_i_1_n_0 ),
        .D(\FSM_gray_state[3]_i_2_n_0 ),
        .Q(state[3]),
        .R(1'b0));
  MUXF7 \FSM_gray_state_reg[3]_i_5 
       (.I0(\FSM_gray_state[3]_i_8_n_0 ),
        .I1(\FSM_gray_state[3]_i_9_n_0 ),
        .O(\FSM_gray_state_reg[3]_i_5_n_0 ),
        .S(state[1]));
  LUT6 #(
    .INIT(64'h0AEAEAEAEAEAEAEA)) 
    addrNData_i_1
       (.I0(addrNData_reg_n_0),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(\subState[1]_i_2_n_0 ),
        .I3(\subState_reg_n_0_[1] ),
        .I4(\subState_reg_n_0_[0] ),
        .I5(p_1_in4_in),
        .O(addrNData_i_1_n_0));
  LUT4 #(
    .INIT(16'h0004)) 
    addrNData_i_2
       (.I0(state[0]),
        .I1(state[1]),
        .I2(state[2]),
        .I3(state[3]),
        .O(p_1_in4_in));
  FDRE addrNData_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(addrNData_i_1_n_0),
        .Q(addrNData_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hF6)) 
    \bitCount[0]_i_1 
       (.I0(bitCount[0]),
        .I1(dataByte0),
        .I2(dataByte1),
        .O(\bitCount[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFFA6)) 
    \bitCount[1]_i_1 
       (.I0(bitCount[1]),
        .I1(dataByte0),
        .I2(bitCount[0]),
        .I3(dataByte1),
        .O(\bitCount[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hFFFFAAA6)) 
    \bitCount[2]_i_1 
       (.I0(bitCount[2]),
        .I1(dataByte0),
        .I2(bitCount[1]),
        .I3(bitCount[0]),
        .I4(dataByte1),
        .O(\bitCount[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0002030000020000)) 
    \bitCount[2]_i_2 
       (.I0(p_19_in),
        .I1(state[2]),
        .I2(state[3]),
        .I3(state[0]),
        .I4(state[1]),
        .I5(\subState[1]_i_2_n_0 ),
        .O(dataByte1));
  FDRE #(
    .INIT(1'b1)) 
    \bitCount_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\bitCount[0]_i_1_n_0 ),
        .Q(bitCount[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \bitCount_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\bitCount[1]_i_1_n_0 ),
        .Q(bitCount[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \bitCount_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\bitCount[2]_i_1_n_0 ),
        .Q(bitCount[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \busFreeCnt[0]_i_1 
       (.I0(busFreeCnt_reg__0[0]),
        .O(busFreeCnt0[0]));
  LUT2 #(
    .INIT(4'h9)) 
    \busFreeCnt[1]_i_1 
       (.I0(busFreeCnt_reg__0[1]),
        .I1(busFreeCnt_reg__0[0]),
        .O(\busFreeCnt[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hA9)) 
    \busFreeCnt[2]_i_1 
       (.I0(busFreeCnt_reg__0[2]),
        .I1(busFreeCnt_reg__0[0]),
        .I2(busFreeCnt_reg__0[1]),
        .O(busFreeCnt0[2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \busFreeCnt[3]_i_1 
       (.I0(busFreeCnt_reg__0[3]),
        .I1(busFreeCnt_reg__0[2]),
        .I2(busFreeCnt_reg__0[1]),
        .I3(busFreeCnt_reg__0[0]),
        .O(busFreeCnt0[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \busFreeCnt[4]_i_1 
       (.I0(busFreeCnt_reg__0[4]),
        .I1(busFreeCnt_reg__0[3]),
        .I2(busFreeCnt_reg__0[0]),
        .I3(busFreeCnt_reg__0[1]),
        .I4(busFreeCnt_reg__0[2]),
        .O(busFreeCnt0[4]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \busFreeCnt[5]_i_1 
       (.I0(busFreeCnt_reg__0[5]),
        .I1(busFreeCnt_reg__0[4]),
        .I2(busFreeCnt_reg__0[2]),
        .I3(busFreeCnt_reg__0[1]),
        .I4(busFreeCnt_reg__0[0]),
        .I5(busFreeCnt_reg__0[3]),
        .O(busFreeCnt0[5]));
  LUT3 #(
    .INIT(8'hBF)) 
    \busFreeCnt[6]_i_1 
       (.I0(int_Rst),
        .I1(p_0_in),
        .I2(dScl),
        .O(busFreeCnt0_1));
  LUT2 #(
    .INIT(4'h6)) 
    \busFreeCnt[6]_i_2 
       (.I0(busFreeCnt_reg__0[6]),
        .I1(\busFreeCnt[6]_i_3_n_0 ),
        .O(busFreeCnt0[6]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \busFreeCnt[6]_i_3 
       (.I0(busFreeCnt_reg__0[4]),
        .I1(busFreeCnt_reg__0[2]),
        .I2(busFreeCnt_reg__0[1]),
        .I3(busFreeCnt_reg__0[0]),
        .I4(busFreeCnt_reg__0[3]),
        .I5(busFreeCnt_reg__0[5]),
        .O(\busFreeCnt[6]_i_3_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[0]),
        .Q(busFreeCnt_reg__0[0]),
        .S(busFreeCnt0_1));
  FDRE #(
    .INIT(1'b0)) 
    \busFreeCnt_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\busFreeCnt[1]_i_1_n_0 ),
        .Q(busFreeCnt_reg__0[1]),
        .R(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[2]),
        .Q(busFreeCnt_reg__0[2]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[3]),
        .Q(busFreeCnt_reg__0[3]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[4]),
        .Q(busFreeCnt_reg__0[4]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[5]),
        .Q(busFreeCnt_reg__0[5]),
        .S(busFreeCnt0_1));
  FDSE #(
    .INIT(1'b1)) 
    \busFreeCnt_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(busFreeCnt0[6]),
        .Q(busFreeCnt_reg__0[6]),
        .S(busFreeCnt0_1));
  LUT6 #(
    .INIT(64'h4555FFFF45550000)) 
    \busState[0]_i_1 
       (.I0(int_Rst),
        .I1(p_0_in),
        .I2(dScl),
        .I3(ddSda),
        .I4(busState0),
        .I5(\busState_reg_n_0_[0] ),
        .O(\busState[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \busState[1]_i_1 
       (.I0(p_0_in),
        .I1(dScl),
        .I2(ddSda),
        .I3(int_Rst),
        .I4(busState0),
        .I5(\busState_reg_n_0_[1] ),
        .O(\busState[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF4444F444)) 
    \busState[1]_i_2 
       (.I0(busFreeCnt_reg__0[6]),
        .I1(\busFreeCnt[6]_i_3_n_0 ),
        .I2(ddSda),
        .I3(dScl),
        .I4(p_0_in),
        .I5(int_Rst),
        .O(busState0));
  FDRE #(
    .INIT(1'b0)) 
    \busState_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\busState[0]_i_1_n_0 ),
        .Q(\busState_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \busState_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\busState[1]_i_1_n_0 ),
        .Q(\busState_reg_n_0_[1] ),
        .R(1'b0));
  FDRE dScl_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(scl),
        .Q(dScl),
        .R(1'b0));
  FDRE dSda_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(sda),
        .Q(p_0_in),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h2F20)) 
    \dataByte[0]_i_1 
       (.I0(data_i[0]),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(\dataByte[7]_i_4_n_0 ),
        .I3(p_0_in),
        .O(p_1_in[0]));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \dataByte[1]_i_1 
       (.I0(\dataByte[7]_i_5_n_0 ),
        .I1(data_i[1]),
        .I2(\dataByte[7]_i_4_n_0 ),
        .I3(\dataByte_reg_n_0_[0] ),
        .O(p_1_in[1]));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \dataByte[2]_i_1 
       (.I0(\dataByte[7]_i_5_n_0 ),
        .I1(data_i[2]),
        .I2(\dataByte[7]_i_4_n_0 ),
        .I3(\dataByte_reg_n_0_[1] ),
        .O(p_1_in[2]));
  LUT4 #(
    .INIT(16'h2F20)) 
    \dataByte[3]_i_1 
       (.I0(data_i[3]),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(\dataByte[7]_i_4_n_0 ),
        .I3(\dataByte_reg_n_0_[2] ),
        .O(p_1_in[3]));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \dataByte[4]_i_1 
       (.I0(\dataByte[7]_i_5_n_0 ),
        .I1(data_i[4]),
        .I2(\dataByte[7]_i_4_n_0 ),
        .I3(\dataByte_reg_n_0_[3] ),
        .O(p_1_in[4]));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \dataByte[5]_i_1 
       (.I0(\dataByte[7]_i_5_n_0 ),
        .I1(data_i[5]),
        .I2(\dataByte[7]_i_4_n_0 ),
        .I3(\dataByte_reg_n_0_[4] ),
        .O(p_1_in[5]));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \dataByte[6]_i_1 
       (.I0(\dataByte[7]_i_5_n_0 ),
        .I1(data_i[6]),
        .I2(\dataByte[7]_i_4_n_0 ),
        .I3(\dataByte_reg_n_0_[5] ),
        .O(p_1_in[6]));
  LUT2 #(
    .INIT(4'hE)) 
    \dataByte[7]_i_1 
       (.I0(dataByte0),
        .I1(\dataByte[7]_i_4_n_0 ),
        .O(\dataByte[7]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2F20)) 
    \dataByte[7]_i_2 
       (.I0(data_i[7]),
        .I1(\dataByte[7]_i_5_n_0 ),
        .I2(\dataByte[7]_i_4_n_0 ),
        .I3(\dataByte_reg_n_0_[6] ),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'h0200330002000000)) 
    \dataByte[7]_i_3 
       (.I0(p_19_in),
        .I1(state[3]),
        .I2(state[2]),
        .I3(state[1]),
        .I4(state[0]),
        .I5(ERR_O_i_2_n_0),
        .O(dataByte0));
  LUT6 #(
    .INIT(64'h0220020002000200)) 
    \dataByte[7]_i_4 
       (.I0(\subState[1]_i_2_n_0 ),
        .I1(\subState[1]_i_3_n_0 ),
        .I2(state[1]),
        .I3(state[0]),
        .I4(\subState_reg_n_0_[1] ),
        .I5(\subState_reg_n_0_[0] ),
        .O(\dataByte[7]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \dataByte[7]_i_5 
       (.I0(state[2]),
        .I1(state[3]),
        .I2(state[0]),
        .I3(state[1]),
        .O(\dataByte[7]_i_5_n_0 ));
  FDRE \dataByte_reg[0] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[0]),
        .Q(\dataByte_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \dataByte_reg[1] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[1]),
        .Q(\dataByte_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \dataByte_reg[2] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[2]),
        .Q(\dataByte_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \dataByte_reg[3] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[3]),
        .Q(\dataByte_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \dataByte_reg[4] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[4]),
        .Q(\dataByte_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \dataByte_reg[5] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[5]),
        .Q(\dataByte_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \dataByte_reg[6] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[6]),
        .Q(\dataByte_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \dataByte_reg[7] 
       (.C(clk_out2),
        .CE(\dataByte[7]_i_1_n_0 ),
        .D(p_1_in[7]),
        .Q(\dataByte_reg_n_0_[7] ),
        .R(1'b0));
  FDRE ddSda_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(p_0_in),
        .Q(ddSda),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAABAAAAAAAAA)) 
    \initA[6]_i_1 
       (.I0(\state_reg[3]_0 ),
        .I1(Q[3]),
        .I2(done),
        .I3(Q[2]),
        .I4(error),
        .I5(\state_reg[1] ),
        .O(\initA_reg[6] ));
  LUT6 #(
    .INIT(64'h7F777F7F40444040)) 
    initEn_i_1
       (.I0(Q[2]),
        .I1(reset_n),
        .I2(\state_reg[3]_0 ),
        .I3(initEn_i_2_n_0),
        .I4(\state_reg[3] ),
        .I5(initEn),
        .O(initEn_reg));
  LUT3 #(
    .INIT(8'hEF)) 
    initEn_i_2
       (.I0(error),
        .I1(Q[2]),
        .I2(done),
        .O(initEn_i_2_n_0));
  LUT6 #(
    .INIT(64'hBBBBBBBBBBBBBBB3)) 
    int_Rst_i_1
       (.I0(int_Rst),
        .I1(reset_n),
        .I2(state[1]),
        .I3(state[0]),
        .I4(state[2]),
        .I5(state[3]),
        .O(int_Rst_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    int_Rst_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(int_Rst_i_1_n_0),
        .Q(int_Rst),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h7F70)) 
    rScl_i_1
       (.I0(\subState_reg_n_0_[1] ),
        .I1(\subState_reg_n_0_[0] ),
        .I2(rScl_i_2_n_0),
        .I3(rScl),
        .O(rScl_i_1_n_0));
  LUT6 #(
    .INIT(64'h9554D554D5540000)) 
    rScl_i_2
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[1]),
        .I3(state[0]),
        .I4(\subState_reg_n_0_[0] ),
        .I5(\subState_reg_n_0_[1] ),
        .O(rScl_i_2_n_0));
  FDRE #(
    .INIT(1'b1)) 
    rScl_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(rScl_i_1_n_0),
        .Q(rScl),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAABFFFFAAAB0000)) 
    rSda_i_1
       (.I0(rSda_i_2_n_0),
        .I1(rSda_i_3_n_0),
        .I2(\subState_reg_n_0_[0] ),
        .I3(\subState_reg_n_0_[1] ),
        .I4(rSda_i_4_n_0),
        .I5(rSda),
        .O(rSda_i_1_n_0));
  LUT6 #(
    .INIT(64'hFCFC303030F430F4)) 
    rSda_i_2
       (.I0(rSda_i_5_n_0),
        .I1(rSda_i_6_n_0),
        .I2(rSda_i_7_n_0),
        .I3(rSda_i_8_n_0),
        .I4(\dataByte_reg_n_0_[7] ),
        .I5(p_0_in7_in),
        .O(rSda_i_2_n_0));
  LUT3 #(
    .INIT(8'hFB)) 
    rSda_i_3
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[1]),
        .O(rSda_i_3_n_0));
  LUT6 #(
    .INIT(64'h010041004100D554)) 
    rSda_i_4
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[1]),
        .I3(state[0]),
        .I4(\subState_reg_n_0_[0] ),
        .I5(\subState_reg_n_0_[1] ),
        .O(rSda_i_4_n_0));
  LUT3 #(
    .INIT(8'hFB)) 
    rSda_i_5
       (.I0(state[3]),
        .I1(state[1]),
        .I2(state[0]),
        .O(rSda_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h1)) 
    rSda_i_6
       (.I0(\subState_reg_n_0_[0] ),
        .I1(\subState_reg_n_0_[1] ),
        .O(rSda_i_6_n_0));
  LUT6 #(
    .INIT(64'h00004000FFFF3FFF)) 
    rSda_i_7
       (.I0(\subState_reg_n_0_[0] ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(state[3]),
        .I5(\subState_reg_n_0_[1] ),
        .O(rSda_i_7_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    rSda_i_8
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .I3(state[3]),
        .O(rSda_i_8_n_0));
  FDRE #(
    .INIT(1'b1)) 
    rSda_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(rSda_i_1_n_0),
        .Q(rSda),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \sclCnt[0]_i_1 
       (.I0(sclCnt_reg__0[0]),
        .O(sclCnt0[0]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \sclCnt[1]_i_1 
       (.I0(sclCnt_reg__0[1]),
        .I1(sclCnt_reg__0[0]),
        .O(\sclCnt[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hA9)) 
    \sclCnt[2]_i_1 
       (.I0(sclCnt_reg__0[2]),
        .I1(sclCnt_reg__0[0]),
        .I2(sclCnt_reg__0[1]),
        .O(sclCnt0[2]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \sclCnt[3]_i_1 
       (.I0(sclCnt_reg__0[3]),
        .I1(sclCnt_reg__0[2]),
        .I2(sclCnt_reg__0[1]),
        .I3(sclCnt_reg__0[0]),
        .O(sclCnt0[3]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \sclCnt[4]_i_1 
       (.I0(sclCnt_reg__0[4]),
        .I1(sclCnt_reg__0[3]),
        .I2(sclCnt_reg__0[0]),
        .I3(sclCnt_reg__0[1]),
        .I4(sclCnt_reg__0[2]),
        .O(sclCnt0[4]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \sclCnt[5]_i_1 
       (.I0(sclCnt_reg__0[5]),
        .I1(sclCnt_reg__0[4]),
        .I2(sclCnt_reg__0[2]),
        .I3(sclCnt_reg__0[1]),
        .I4(sclCnt_reg__0[0]),
        .I5(sclCnt_reg__0[3]),
        .O(sclCnt0[5]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \sclCnt[6]_i_1 
       (.I0(state[3]),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(\subState[1]_i_2_n_0 ),
        .O(sclCnt0_0));
  LUT2 #(
    .INIT(4'hB)) 
    \sclCnt[6]_i_2 
       (.I0(dScl),
        .I1(rScl),
        .O(\sclCnt[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sclCnt[6]_i_3 
       (.I0(sclCnt_reg__0[6]),
        .I1(\sclCnt[6]_i_4_n_0 ),
        .O(sclCnt0[6]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \sclCnt[6]_i_4 
       (.I0(sclCnt_reg__0[4]),
        .I1(sclCnt_reg__0[2]),
        .I2(sclCnt_reg__0[1]),
        .I3(sclCnt_reg__0[0]),
        .I4(sclCnt_reg__0[3]),
        .I5(sclCnt_reg__0[5]),
        .O(\sclCnt[6]_i_4_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[0] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[0]),
        .Q(sclCnt_reg__0[0]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \sclCnt_reg[1] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(\sclCnt[1]_i_1_n_0 ),
        .Q(sclCnt_reg__0[1]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[2] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[2]),
        .Q(sclCnt_reg__0[2]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[3] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[3]),
        .Q(sclCnt_reg__0[3]),
        .S(sclCnt0_0));
  FDSE #(
    .INIT(1'b1)) 
    \sclCnt_reg[4] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[4]),
        .Q(sclCnt_reg__0[4]),
        .S(sclCnt0_0));
  FDRE #(
    .INIT(1'b1)) 
    \sclCnt_reg[5] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[5]),
        .Q(sclCnt_reg__0[5]),
        .R(sclCnt0_0));
  FDRE #(
    .INIT(1'b1)) 
    \sclCnt_reg[6] 
       (.C(clk_out2),
        .CE(\sclCnt[6]_i_2_n_0 ),
        .D(sclCnt0[6]),
        .Q(sclCnt_reg__0[6]),
        .R(sclCnt0_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    scl_INST_0
       (.I0(1'b0),
        .I1(scl_INST_0_i_1_n_0),
        .I2(1'b0),
        .I3(1'b0),
        .I4(1'b0),
        .I5(1'b0),
        .O(scl));
  LUT1 #(
    .INIT(2'h1)) 
    scl_INST_0_i_1
       (.I0(rScl),
        .O(scl_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    sda_INST_0
       (.I0(1'b0),
        .I1(sda_INST_0_i_1_n_0),
        .I2(1'b0),
        .I3(1'b0),
        .I4(1'b0),
        .I5(1'b0),
        .O(sda));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT1 #(
    .INIT(2'h1)) 
    sda_INST_0_i_1
       (.I0(rSda),
        .O(sda_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h0000FF0D)) 
    \state[0]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(error),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h00A8AA00AAAAAA00)) 
    \state[1]_i_1 
       (.I0(\state[1]_i_2_n_0 ),
        .I1(\initWord_reg[6] [1]),
        .I2(\initWord_reg[6] [0]),
        .I3(Q[0]),
        .I4(Q[1]),
        .I5(\initA_reg[6]_0 ),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \state[1]_i_2 
       (.I0(Q[3]),
        .I1(error),
        .I2(Q[2]),
        .O(\state[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'hE0EE)) 
    \state[2]_i_1 
       (.I0(\state_reg[3] ),
        .I1(error),
        .I2(Q[3]),
        .I3(Q[2]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h80CF00AA800F00AA)) 
    \state[3]_i_1 
       (.I0(done),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(\delaycnt_reg[12] ),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    \state[3]_i_2 
       (.I0(Q[2]),
        .I1(error),
        .I2(Q[3]),
        .I3(\state_reg[1] ),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h6666666666666660)) 
    \subState[0]_i_1 
       (.I0(\subState_reg_n_0_[0] ),
        .I1(\subState[1]_i_2_n_0 ),
        .I2(state[3]),
        .I3(state[2]),
        .I4(state[0]),
        .I5(state[1]),
        .O(\subState[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6A6A6A6A6A6A6A00)) 
    \subState[1]_i_1 
       (.I0(\subState_reg_n_0_[1] ),
        .I1(\subState[1]_i_2_n_0 ),
        .I2(\subState_reg_n_0_[0] ),
        .I3(\subState[1]_i_3_n_0 ),
        .I4(state[0]),
        .I5(state[1]),
        .O(\subState[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \subState[1]_i_2 
       (.I0(\sclCnt[6]_i_4_n_0 ),
        .I1(sclCnt_reg__0[6]),
        .O(\subState[1]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \subState[1]_i_3 
       (.I0(state[3]),
        .I1(state[2]),
        .O(\subState[1]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \subState_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\subState[0]_i_1_n_0 ),
        .Q(\subState_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \subState_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\subState[1]_i_1_n_0 ),
        .Q(\subState_reg_n_0_[1] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_audio_init
   (scl,
    sda,
    clk_out2,
    reset_n,
    SR);
  inout scl;
  inout sda;
  input clk_out2;
  input reset_n;
  input [0:0]SR;

  wire [0:0]SR;
  wire clk_out2;
  wire [6:6]data0;
  wire [7:0]data1;
  wire [7:0]data2;
  wire [7:0]data_i;
  wire \data_i[0]_i_1_n_0 ;
  wire \data_i[1]_i_1_n_0 ;
  wire \data_i[2]_i_1_n_0 ;
  wire \data_i[3]_i_1_n_0 ;
  wire \data_i[4]_i_1_n_0 ;
  wire \data_i[4]_i_2_n_0 ;
  wire \data_i[5]_i_1_n_0 ;
  wire \data_i[5]_i_2_n_0 ;
  wire \data_i[6]_i_1_n_0 ;
  wire \data_i[6]_i_2_n_0 ;
  wire \data_i[7]_i_1_n_0 ;
  wire delayEn;
  wire delayEn_i_1_n_0;
  wire delayEn_i_2_n_0;
  wire [31:0]delaycnt;
  wire delaycnt0;
  wire delaycnt0_carry__0_i_1_n_0;
  wire delaycnt0_carry__0_i_2_n_0;
  wire delaycnt0_carry__0_i_3_n_0;
  wire delaycnt0_carry__0_i_4_n_0;
  wire delaycnt0_carry__0_n_0;
  wire delaycnt0_carry__0_n_1;
  wire delaycnt0_carry__0_n_2;
  wire delaycnt0_carry__0_n_3;
  wire delaycnt0_carry__0_n_4;
  wire delaycnt0_carry__0_n_5;
  wire delaycnt0_carry__0_n_6;
  wire delaycnt0_carry__0_n_7;
  wire delaycnt0_carry__1_i_1_n_0;
  wire delaycnt0_carry__1_i_2_n_0;
  wire delaycnt0_carry__1_i_3_n_0;
  wire delaycnt0_carry__1_i_4_n_0;
  wire delaycnt0_carry__1_n_0;
  wire delaycnt0_carry__1_n_1;
  wire delaycnt0_carry__1_n_2;
  wire delaycnt0_carry__1_n_3;
  wire delaycnt0_carry__1_n_4;
  wire delaycnt0_carry__1_n_5;
  wire delaycnt0_carry__1_n_6;
  wire delaycnt0_carry__1_n_7;
  wire delaycnt0_carry__2_i_1_n_0;
  wire delaycnt0_carry__2_i_2_n_0;
  wire delaycnt0_carry__2_i_3_n_0;
  wire delaycnt0_carry__2_i_4_n_0;
  wire delaycnt0_carry__2_n_0;
  wire delaycnt0_carry__2_n_1;
  wire delaycnt0_carry__2_n_2;
  wire delaycnt0_carry__2_n_3;
  wire delaycnt0_carry__2_n_4;
  wire delaycnt0_carry__2_n_5;
  wire delaycnt0_carry__2_n_6;
  wire delaycnt0_carry__2_n_7;
  wire delaycnt0_carry__3_i_1_n_0;
  wire delaycnt0_carry__3_i_2_n_0;
  wire delaycnt0_carry__3_i_3_n_0;
  wire delaycnt0_carry__3_i_4_n_0;
  wire delaycnt0_carry__3_n_0;
  wire delaycnt0_carry__3_n_1;
  wire delaycnt0_carry__3_n_2;
  wire delaycnt0_carry__3_n_3;
  wire delaycnt0_carry__3_n_4;
  wire delaycnt0_carry__3_n_5;
  wire delaycnt0_carry__3_n_6;
  wire delaycnt0_carry__3_n_7;
  wire delaycnt0_carry__4_i_1_n_0;
  wire delaycnt0_carry__4_i_2_n_0;
  wire delaycnt0_carry__4_i_3_n_0;
  wire delaycnt0_carry__4_i_4_n_0;
  wire delaycnt0_carry__4_n_0;
  wire delaycnt0_carry__4_n_1;
  wire delaycnt0_carry__4_n_2;
  wire delaycnt0_carry__4_n_3;
  wire delaycnt0_carry__4_n_4;
  wire delaycnt0_carry__4_n_5;
  wire delaycnt0_carry__4_n_6;
  wire delaycnt0_carry__4_n_7;
  wire delaycnt0_carry__5_i_1_n_0;
  wire delaycnt0_carry__5_i_2_n_0;
  wire delaycnt0_carry__5_i_3_n_0;
  wire delaycnt0_carry__5_i_4_n_0;
  wire delaycnt0_carry__5_n_0;
  wire delaycnt0_carry__5_n_1;
  wire delaycnt0_carry__5_n_2;
  wire delaycnt0_carry__5_n_3;
  wire delaycnt0_carry__5_n_4;
  wire delaycnt0_carry__5_n_5;
  wire delaycnt0_carry__5_n_6;
  wire delaycnt0_carry__5_n_7;
  wire delaycnt0_carry__6_i_1_n_0;
  wire delaycnt0_carry__6_i_2_n_0;
  wire delaycnt0_carry__6_i_3_n_0;
  wire delaycnt0_carry__6_n_2;
  wire delaycnt0_carry__6_n_3;
  wire delaycnt0_carry__6_n_5;
  wire delaycnt0_carry__6_n_6;
  wire delaycnt0_carry__6_n_7;
  wire delaycnt0_carry_i_1_n_0;
  wire delaycnt0_carry_i_2_n_0;
  wire delaycnt0_carry_i_3_n_0;
  wire delaycnt0_carry_i_4_n_0;
  wire delaycnt0_carry_n_0;
  wire delaycnt0_carry_n_1;
  wire delaycnt0_carry_n_2;
  wire delaycnt0_carry_n_3;
  wire delaycnt0_carry_n_4;
  wire delaycnt0_carry_n_5;
  wire delaycnt0_carry_n_6;
  wire delaycnt0_carry_n_7;
  wire \delaycnt[0]_i_1_n_0 ;
  wire \initA[0]_i_1_n_0 ;
  wire \initA[6]_i_3_n_0 ;
  wire \initA[6]_i_4_n_0 ;
  wire \initA_reg_n_0_[0] ;
  wire \initA_reg_n_0_[1] ;
  wire \initA_reg_n_0_[2] ;
  wire \initA_reg_n_0_[3] ;
  wire \initA_reg_n_0_[4] ;
  wire \initA_reg_n_0_[5] ;
  wire \initA_reg_n_0_[6] ;
  wire initEn;
  wire \initWord[0]_i_1_n_0 ;
  wire \initWord[10]_i_1_n_0 ;
  wire \initWord[11]_i_1_n_0 ;
  wire \initWord[12]_i_1_n_0 ;
  wire \initWord[13]_i_1_n_0 ;
  wire \initWord[14]_i_1_n_0 ;
  wire \initWord[15]_i_1_n_0 ;
  wire \initWord[16]_i_1_n_0 ;
  wire \initWord[17]_i_1_n_0 ;
  wire \initWord[18]_i_1_n_0 ;
  wire \initWord[19]_i_1_n_0 ;
  wire \initWord[20]_i_1_n_0 ;
  wire \initWord[21]_i_1_n_0 ;
  wire \initWord[23]_i_1_n_0 ;
  wire \initWord[30]_i_1_n_0 ;
  wire \initWord[30]_i_2_n_0 ;
  wire \initWord[30]_i_3_n_0 ;
  wire \initWord[6]_i_1_n_0 ;
  wire \initWord[8]_i_1_n_0 ;
  wire \initWord[9]_i_1_n_0 ;
  wire \initWord_reg_n_0_[0] ;
  wire \initWord_reg_n_0_[6] ;
  wire msg;
  wire msg0;
  wire [6:1]p_1_in__0;
  wire reset_n;
  wire scl;
  wire sda;
  wire \state[1]_i_3_n_0 ;
  wire \state[1]_i_4_n_0 ;
  wire \state[1]_i_5_n_0 ;
  wire \state[1]_i_6_n_0 ;
  wire \state[2]_i_2_n_0 ;
  wire \state[3]_i_10_n_0 ;
  wire \state[3]_i_11_n_0 ;
  wire \state[3]_i_12_n_0 ;
  wire \state[3]_i_13_n_0 ;
  wire \state[3]_i_3_n_0 ;
  wire \state[3]_i_4_n_0 ;
  wire \state[3]_i_5_n_0 ;
  wire \state[3]_i_6_n_0 ;
  wire \state[3]_i_7_n_0 ;
  wire \state[3]_i_8_n_0 ;
  wire \state[3]_i_9_n_0 ;
  wire \state_reg_n_0_[0] ;
  wire \state_reg_n_0_[1] ;
  wire \state_reg_n_0_[2] ;
  wire \state_reg_n_0_[3] ;
  wire stb;
  wire stb_i_1_n_0;
  wire twi_controller_n_0;
  wire twi_controller_n_1;
  wire twi_controller_n_2;
  wire twi_controller_n_3;
  wire twi_controller_n_4;
  wire twi_controller_n_5;
  wire twi_controller_n_6;
  wire [3:2]NLW_delaycnt0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_delaycnt0_carry__6_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hF0AACC00FFFFFFFF)) 
    \data_i[0]_i_1 
       (.I0(data2[0]),
        .I1(data1[0]),
        .I2(\initWord_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(\state_reg_n_0_[1] ),
        .I5(\data_i[4]_i_2_n_0 ),
        .O(\data_i[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0010110000100000)) 
    \data_i[1]_i_1 
       (.I0(\state_reg_n_0_[3] ),
        .I1(\state_reg_n_0_[2] ),
        .I2(data1[1]),
        .I3(\state_reg_n_0_[1] ),
        .I4(\state_reg_n_0_[0] ),
        .I5(data2[1]),
        .O(\data_i[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF0CCAA00FFFFFFFF)) 
    \data_i[2]_i_1 
       (.I0(data1[2]),
        .I1(data2[2]),
        .I2(\initWord_reg_n_0_[6] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(\state_reg_n_0_[1] ),
        .I5(\data_i[4]_i_2_n_0 ),
        .O(\data_i[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF0CCAA00FFFFFFFF)) 
    \data_i[3]_i_1 
       (.I0(data1[3]),
        .I1(data2[3]),
        .I2(\initWord_reg_n_0_[6] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(\state_reg_n_0_[1] ),
        .I5(\data_i[4]_i_2_n_0 ),
        .O(\data_i[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8AA080A08A008000)) 
    \data_i[4]_i_1 
       (.I0(\data_i[4]_i_2_n_0 ),
        .I1(\initWord_reg_n_0_[6] ),
        .I2(\state_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[1] ),
        .I4(data2[4]),
        .I5(data1[4]),
        .O(\data_i[4]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \data_i[4]_i_2 
       (.I0(\state_reg_n_0_[2] ),
        .I1(\state_reg_n_0_[3] ),
        .O(\data_i[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \data_i[5]_i_1 
       (.I0(\state_reg_n_0_[2] ),
        .I1(\state_reg_n_0_[0] ),
        .I2(\state_reg_n_0_[1] ),
        .I3(\state_reg_n_0_[3] ),
        .I4(reset_n),
        .O(\data_i[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF0AACC00FFFFFFFF)) 
    \data_i[5]_i_2 
       (.I0(data2[5]),
        .I1(data1[5]),
        .I2(\initWord_reg_n_0_[6] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(\state_reg_n_0_[1] ),
        .I5(\data_i[4]_i_2_n_0 ),
        .O(\data_i[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h800000FF80000010)) 
    \data_i[6]_i_1 
       (.I0(\state_reg_n_0_[1] ),
        .I1(\state_reg_n_0_[0] ),
        .I2(data0),
        .I3(\state_reg_n_0_[2] ),
        .I4(\state_reg_n_0_[3] ),
        .I5(\data_i[6]_i_2_n_0 ),
        .O(\data_i[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \data_i[6]_i_2 
       (.I0(\initWord_reg_n_0_[6] ),
        .I1(data2[6]),
        .I2(\state_reg_n_0_[1] ),
        .I3(\state_reg_n_0_[0] ),
        .I4(data1[7]),
        .O(\data_i[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000A000C0000)) 
    \data_i[7]_i_1 
       (.I0(data1[7]),
        .I1(data2[7]),
        .I2(\state_reg_n_0_[2] ),
        .I3(\state_reg_n_0_[3] ),
        .I4(\state_reg_n_0_[1] ),
        .I5(\state_reg_n_0_[0] ),
        .O(\data_i[7]_i_1_n_0 ));
  FDRE \data_i_reg[0] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[0]_i_1_n_0 ),
        .Q(data_i[0]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[1] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[1]_i_1_n_0 ),
        .Q(data_i[1]),
        .R(1'b0));
  FDRE \data_i_reg[2] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[2]_i_1_n_0 ),
        .Q(data_i[2]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[3] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[3]_i_1_n_0 ),
        .Q(data_i[3]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[4] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[4]_i_1_n_0 ),
        .Q(data_i[4]),
        .R(1'b0));
  FDRE \data_i_reg[5] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[5]_i_2_n_0 ),
        .Q(data_i[5]),
        .R(\data_i[5]_i_1_n_0 ));
  FDRE \data_i_reg[6] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[6]_i_1_n_0 ),
        .Q(data_i[6]),
        .R(1'b0));
  FDRE \data_i_reg[7] 
       (.C(clk_out2),
        .CE(reset_n),
        .D(\data_i[7]_i_1_n_0 ),
        .Q(data_i[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hA2AAAEAA00000000)) 
    delayEn_i_1
       (.I0(delayEn),
        .I1(\state_reg_n_0_[2] ),
        .I2(\state_reg_n_0_[3] ),
        .I3(delayEn_i_2_n_0),
        .I4(\state[3]_i_3_n_0 ),
        .I5(reset_n),
        .O(delayEn_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    delayEn_i_2
       (.I0(\state_reg_n_0_[1] ),
        .I1(\state_reg_n_0_[0] ),
        .O(delayEn_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    delayEn_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(delayEn_i_1_n_0),
        .Q(delayEn),
        .R(1'b0));
  CARRY4 delaycnt0_carry
       (.CI(1'b0),
        .CO({delaycnt0_carry_n_0,delaycnt0_carry_n_1,delaycnt0_carry_n_2,delaycnt0_carry_n_3}),
        .CYINIT(delaycnt[0]),
        .DI(delaycnt[4:1]),
        .O({delaycnt0_carry_n_4,delaycnt0_carry_n_5,delaycnt0_carry_n_6,delaycnt0_carry_n_7}),
        .S({delaycnt0_carry_i_1_n_0,delaycnt0_carry_i_2_n_0,delaycnt0_carry_i_3_n_0,delaycnt0_carry_i_4_n_0}));
  CARRY4 delaycnt0_carry__0
       (.CI(delaycnt0_carry_n_0),
        .CO({delaycnt0_carry__0_n_0,delaycnt0_carry__0_n_1,delaycnt0_carry__0_n_2,delaycnt0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[8:5]),
        .O({delaycnt0_carry__0_n_4,delaycnt0_carry__0_n_5,delaycnt0_carry__0_n_6,delaycnt0_carry__0_n_7}),
        .S({delaycnt0_carry__0_i_1_n_0,delaycnt0_carry__0_i_2_n_0,delaycnt0_carry__0_i_3_n_0,delaycnt0_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_1
       (.I0(delaycnt[8]),
        .O(delaycnt0_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_2
       (.I0(delaycnt[7]),
        .O(delaycnt0_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_3
       (.I0(delaycnt[6]),
        .O(delaycnt0_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__0_i_4
       (.I0(delaycnt[5]),
        .O(delaycnt0_carry__0_i_4_n_0));
  CARRY4 delaycnt0_carry__1
       (.CI(delaycnt0_carry__0_n_0),
        .CO({delaycnt0_carry__1_n_0,delaycnt0_carry__1_n_1,delaycnt0_carry__1_n_2,delaycnt0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[12:9]),
        .O({delaycnt0_carry__1_n_4,delaycnt0_carry__1_n_5,delaycnt0_carry__1_n_6,delaycnt0_carry__1_n_7}),
        .S({delaycnt0_carry__1_i_1_n_0,delaycnt0_carry__1_i_2_n_0,delaycnt0_carry__1_i_3_n_0,delaycnt0_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_1
       (.I0(delaycnt[12]),
        .O(delaycnt0_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_2
       (.I0(delaycnt[11]),
        .O(delaycnt0_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_3
       (.I0(delaycnt[10]),
        .O(delaycnt0_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__1_i_4
       (.I0(delaycnt[9]),
        .O(delaycnt0_carry__1_i_4_n_0));
  CARRY4 delaycnt0_carry__2
       (.CI(delaycnt0_carry__1_n_0),
        .CO({delaycnt0_carry__2_n_0,delaycnt0_carry__2_n_1,delaycnt0_carry__2_n_2,delaycnt0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[16:13]),
        .O({delaycnt0_carry__2_n_4,delaycnt0_carry__2_n_5,delaycnt0_carry__2_n_6,delaycnt0_carry__2_n_7}),
        .S({delaycnt0_carry__2_i_1_n_0,delaycnt0_carry__2_i_2_n_0,delaycnt0_carry__2_i_3_n_0,delaycnt0_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_1
       (.I0(delaycnt[16]),
        .O(delaycnt0_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_2
       (.I0(delaycnt[15]),
        .O(delaycnt0_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_3
       (.I0(delaycnt[14]),
        .O(delaycnt0_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__2_i_4
       (.I0(delaycnt[13]),
        .O(delaycnt0_carry__2_i_4_n_0));
  CARRY4 delaycnt0_carry__3
       (.CI(delaycnt0_carry__2_n_0),
        .CO({delaycnt0_carry__3_n_0,delaycnt0_carry__3_n_1,delaycnt0_carry__3_n_2,delaycnt0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[20:17]),
        .O({delaycnt0_carry__3_n_4,delaycnt0_carry__3_n_5,delaycnt0_carry__3_n_6,delaycnt0_carry__3_n_7}),
        .S({delaycnt0_carry__3_i_1_n_0,delaycnt0_carry__3_i_2_n_0,delaycnt0_carry__3_i_3_n_0,delaycnt0_carry__3_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_1
       (.I0(delaycnt[20]),
        .O(delaycnt0_carry__3_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_2
       (.I0(delaycnt[19]),
        .O(delaycnt0_carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_3
       (.I0(delaycnt[18]),
        .O(delaycnt0_carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__3_i_4
       (.I0(delaycnt[17]),
        .O(delaycnt0_carry__3_i_4_n_0));
  CARRY4 delaycnt0_carry__4
       (.CI(delaycnt0_carry__3_n_0),
        .CO({delaycnt0_carry__4_n_0,delaycnt0_carry__4_n_1,delaycnt0_carry__4_n_2,delaycnt0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[24:21]),
        .O({delaycnt0_carry__4_n_4,delaycnt0_carry__4_n_5,delaycnt0_carry__4_n_6,delaycnt0_carry__4_n_7}),
        .S({delaycnt0_carry__4_i_1_n_0,delaycnt0_carry__4_i_2_n_0,delaycnt0_carry__4_i_3_n_0,delaycnt0_carry__4_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_1
       (.I0(delaycnt[24]),
        .O(delaycnt0_carry__4_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_2
       (.I0(delaycnt[23]),
        .O(delaycnt0_carry__4_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_3
       (.I0(delaycnt[22]),
        .O(delaycnt0_carry__4_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__4_i_4
       (.I0(delaycnt[21]),
        .O(delaycnt0_carry__4_i_4_n_0));
  CARRY4 delaycnt0_carry__5
       (.CI(delaycnt0_carry__4_n_0),
        .CO({delaycnt0_carry__5_n_0,delaycnt0_carry__5_n_1,delaycnt0_carry__5_n_2,delaycnt0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(delaycnt[28:25]),
        .O({delaycnt0_carry__5_n_4,delaycnt0_carry__5_n_5,delaycnt0_carry__5_n_6,delaycnt0_carry__5_n_7}),
        .S({delaycnt0_carry__5_i_1_n_0,delaycnt0_carry__5_i_2_n_0,delaycnt0_carry__5_i_3_n_0,delaycnt0_carry__5_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_1
       (.I0(delaycnt[28]),
        .O(delaycnt0_carry__5_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_2
       (.I0(delaycnt[27]),
        .O(delaycnt0_carry__5_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_3
       (.I0(delaycnt[26]),
        .O(delaycnt0_carry__5_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__5_i_4
       (.I0(delaycnt[25]),
        .O(delaycnt0_carry__5_i_4_n_0));
  CARRY4 delaycnt0_carry__6
       (.CI(delaycnt0_carry__5_n_0),
        .CO({NLW_delaycnt0_carry__6_CO_UNCONNECTED[3:2],delaycnt0_carry__6_n_2,delaycnt0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,delaycnt[30:29]}),
        .O({NLW_delaycnt0_carry__6_O_UNCONNECTED[3],delaycnt0_carry__6_n_5,delaycnt0_carry__6_n_6,delaycnt0_carry__6_n_7}),
        .S({1'b0,delaycnt0_carry__6_i_1_n_0,delaycnt0_carry__6_i_2_n_0,delaycnt0_carry__6_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__6_i_1
       (.I0(delaycnt[31]),
        .O(delaycnt0_carry__6_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__6_i_2
       (.I0(delaycnt[30]),
        .O(delaycnt0_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry__6_i_3
       (.I0(delaycnt[29]),
        .O(delaycnt0_carry__6_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_1
       (.I0(delaycnt[4]),
        .O(delaycnt0_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_2
       (.I0(delaycnt[3]),
        .O(delaycnt0_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_3
       (.I0(delaycnt[2]),
        .O(delaycnt0_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    delaycnt0_carry_i_4
       (.I0(delaycnt[1]),
        .O(delaycnt0_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \delaycnt[0]_i_1 
       (.I0(delaycnt[0]),
        .O(\delaycnt[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \delaycnt[31]_i_1 
       (.I0(delayEn),
        .O(delaycnt0));
  FDRE \delaycnt_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\delaycnt[0]_i_1_n_0 ),
        .Q(delaycnt[0]),
        .R(delaycnt0));
  FDSE \delaycnt_reg[10] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_6),
        .Q(delaycnt[10]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[11] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_5),
        .Q(delaycnt[11]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[12] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_4),
        .Q(delaycnt[12]),
        .S(delaycnt0));
  FDRE \delaycnt_reg[13] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_7),
        .Q(delaycnt[13]),
        .R(delaycnt0));
  FDSE \delaycnt_reg[14] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_6),
        .Q(delaycnt[14]),
        .S(delaycnt0));
  FDRE \delaycnt_reg[15] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_5),
        .Q(delaycnt[15]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[16] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__2_n_4),
        .Q(delaycnt[16]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[17] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_7),
        .Q(delaycnt[17]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[18] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_6),
        .Q(delaycnt[18]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[19] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_5),
        .Q(delaycnt[19]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_7),
        .Q(delaycnt[1]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[20] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__3_n_4),
        .Q(delaycnt[20]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[21] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_7),
        .Q(delaycnt[21]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[22] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_6),
        .Q(delaycnt[22]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[23] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_5),
        .Q(delaycnt[23]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[24] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__4_n_4),
        .Q(delaycnt[24]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[25] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_7),
        .Q(delaycnt[25]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[26] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_6),
        .Q(delaycnt[26]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[27] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_5),
        .Q(delaycnt[27]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[28] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__5_n_4),
        .Q(delaycnt[28]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[29] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__6_n_7),
        .Q(delaycnt[29]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_6),
        .Q(delaycnt[2]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[30] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__6_n_6),
        .Q(delaycnt[30]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[31] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__6_n_5),
        .Q(delaycnt[31]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_5),
        .Q(delaycnt[3]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry_n_4),
        .Q(delaycnt[4]),
        .R(delaycnt0));
  FDRE \delaycnt_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_7),
        .Q(delaycnt[5]),
        .R(delaycnt0));
  FDSE \delaycnt_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_6),
        .Q(delaycnt[6]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_5),
        .Q(delaycnt[7]),
        .S(delaycnt0));
  FDSE \delaycnt_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__0_n_4),
        .Q(delaycnt[8]),
        .S(delaycnt0));
  FDRE \delaycnt_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(delaycnt0_carry__1_n_7),
        .Q(delaycnt[9]),
        .R(delaycnt0));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \initA[0]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .O(\initA[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \initA[1]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[1] ),
        .O(p_1_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \initA[2]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[0] ),
        .O(p_1_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \initA[3]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[3] ),
        .O(p_1_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \initA[4]_i_1 
       (.I0(\initA_reg_n_0_[4] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[0] ),
        .I3(\initA_reg_n_0_[2] ),
        .I4(\initA_reg_n_0_[1] ),
        .O(p_1_in__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \initA[5]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[4] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[2] ),
        .I4(\initA_reg_n_0_[0] ),
        .I5(\initA_reg_n_0_[3] ),
        .O(p_1_in__0[5]));
  LUT3 #(
    .INIT(8'h6A)) 
    \initA[6]_i_2 
       (.I0(\initA_reg_n_0_[6] ),
        .I1(\initA_reg_n_0_[5] ),
        .I2(\initA[6]_i_4_n_0 ),
        .O(p_1_in__0[6]));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \initA[6]_i_3 
       (.I0(\state[3]_i_3_n_0 ),
        .I1(\state_reg_n_0_[3] ),
        .I2(initEn),
        .I3(\state_reg_n_0_[2] ),
        .I4(\state_reg_n_0_[0] ),
        .I5(\state_reg_n_0_[1] ),
        .O(\initA[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \initA[6]_i_4 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[1] ),
        .I4(\initA_reg_n_0_[4] ),
        .O(\initA[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[0] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(\initA[0]_i_1_n_0 ),
        .Q(\initA_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[1] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[1]),
        .Q(\initA_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[2] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[2]),
        .Q(\initA_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[3] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[3]),
        .Q(\initA_reg_n_0_[3] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[4] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[4]),
        .Q(\initA_reg_n_0_[4] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[5] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[5]),
        .Q(\initA_reg_n_0_[5] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \initA_reg[6] 
       (.C(clk_out2),
        .CE(twi_controller_n_5),
        .D(p_1_in__0[6]),
        .Q(\initA_reg_n_0_[6] ),
        .R(SR));
  FDRE initEn_reg
       (.C(clk_out2),
        .CE(1'b1),
        .D(twi_controller_n_6),
        .Q(initEn),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hA8)) 
    \initWord[0]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[0] ),
        .O(\initWord[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000000C0A6010)) 
    \initWord[10]_i_1 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[2] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[5] ),
        .O(\initWord[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h10101110)) 
    \initWord[11]_i_1 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[5] ),
        .I3(\initA_reg_n_0_[3] ),
        .I4(\initA_reg_n_0_[4] ),
        .O(\initWord[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000006)) 
    \initWord[12]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[0] ),
        .I3(\initA_reg_n_0_[1] ),
        .I4(\initA_reg_n_0_[5] ),
        .I5(\initA_reg_n_0_[4] ),
        .O(\initWord[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hC1C0C0C8C4C9C0C8)) 
    \initWord[13]_i_1 
       (.I0(\initA_reg_n_0_[4] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[5] ),
        .I3(\initA_reg_n_0_[2] ),
        .I4(\initA_reg_n_0_[3] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010180884)) 
    \initWord[14]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[0] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[5] ),
        .O(\initWord[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0010000900180000)) 
    \initWord[15]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[4] ),
        .I3(\initA_reg_n_0_[5] ),
        .I4(\initA_reg_n_0_[1] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00006AB50000A415)) 
    \initWord[16]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[4] ),
        .I4(\initA_reg_n_0_[5] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(\initWord[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1E1F1F0B54040450)) 
    \initWord[17]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[4] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[3] ),
        .I4(\initA_reg_n_0_[2] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB54EE00EE44EB11)) 
    \initWord[18]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[4] ),
        .I2(\initA_reg_n_0_[0] ),
        .I3(\initA_reg_n_0_[1] ),
        .I4(\initA_reg_n_0_[3] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(\initWord[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCCCC7B16CCCC7A1A)) 
    \initWord[19]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[4] ),
        .I4(\initA_reg_n_0_[5] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0908080909090307)) 
    \initWord[20]_i_1 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[4] ),
        .I2(\initA_reg_n_0_[5] ),
        .I3(\initA_reg_n_0_[0] ),
        .I4(\initA_reg_n_0_[1] ),
        .I5(\initA_reg_n_0_[2] ),
        .O(\initWord[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000C00EFEF8)) 
    \initWord[21]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[1] ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[2] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[5] ),
        .O(\initWord[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0080003400800004)) 
    \initWord[23]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[1] ),
        .I3(\initA_reg_n_0_[5] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[0] ),
        .O(\initWord[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000055555557)) 
    \initWord[30]_i_1 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initWord[30]_i_3_n_0 ),
        .I2(\initA_reg_n_0_[3] ),
        .I3(\initA_reg_n_0_[2] ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\initA_reg_n_0_[6] ),
        .O(\initWord[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \initWord[30]_i_2 
       (.I0(\initA_reg_n_0_[5] ),
        .I1(\initA_reg_n_0_[1] ),
        .O(\initWord[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \initWord[30]_i_3 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[1] ),
        .O(\initWord[30]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \initWord[6]_i_1 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[0] ),
        .I2(\initA_reg_n_0_[5] ),
        .O(\initWord[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF2EFFE60066FFFF)) 
    \initWord[8]_i_1 
       (.I0(\initA_reg_n_0_[2] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[4] ),
        .I3(\initA_reg_n_0_[5] ),
        .I4(\initA_reg_n_0_[0] ),
        .I5(\initA_reg_n_0_[1] ),
        .O(\initWord[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0003000C00D400BC)) 
    \initWord[9]_i_1 
       (.I0(\initA_reg_n_0_[0] ),
        .I1(\initA_reg_n_0_[3] ),
        .I2(\initA_reg_n_0_[2] ),
        .I3(\initA_reg_n_0_[5] ),
        .I4(\initA_reg_n_0_[1] ),
        .I5(\initA_reg_n_0_[4] ),
        .O(\initWord[9]_i_1_n_0 ));
  FDRE \initWord_reg[0] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[0]_i_1_n_0 ),
        .Q(\initWord_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \initWord_reg[10] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[10]_i_1_n_0 ),
        .Q(data2[2]),
        .R(1'b0));
  FDRE \initWord_reg[11] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[11]_i_1_n_0 ),
        .Q(data2[3]),
        .R(1'b0));
  FDRE \initWord_reg[12] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[12]_i_1_n_0 ),
        .Q(data2[4]),
        .R(1'b0));
  FDRE \initWord_reg[13] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[13]_i_1_n_0 ),
        .Q(data2[5]),
        .R(1'b0));
  FDRE \initWord_reg[14] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[14]_i_1_n_0 ),
        .Q(data2[6]),
        .R(1'b0));
  FDRE \initWord_reg[15] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[15]_i_1_n_0 ),
        .Q(data2[7]),
        .R(1'b0));
  FDRE \initWord_reg[16] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[16]_i_1_n_0 ),
        .Q(data1[0]),
        .R(1'b0));
  FDRE \initWord_reg[17] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[17]_i_1_n_0 ),
        .Q(data1[1]),
        .R(1'b0));
  FDRE \initWord_reg[18] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[18]_i_1_n_0 ),
        .Q(data1[2]),
        .R(1'b0));
  FDRE \initWord_reg[19] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[19]_i_1_n_0 ),
        .Q(data1[3]),
        .R(1'b0));
  FDRE \initWord_reg[20] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[20]_i_1_n_0 ),
        .Q(data1[4]),
        .R(1'b0));
  FDRE \initWord_reg[21] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[21]_i_1_n_0 ),
        .Q(data1[5]),
        .R(1'b0));
  FDRE \initWord_reg[23] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[23]_i_1_n_0 ),
        .Q(data1[7]),
        .R(1'b0));
  FDRE \initWord_reg[30] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[30]_i_2_n_0 ),
        .Q(data0),
        .R(1'b0));
  FDRE \initWord_reg[6] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[6]_i_1_n_0 ),
        .Q(\initWord_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \initWord_reg[8] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[8]_i_1_n_0 ),
        .Q(data2[0]),
        .R(1'b0));
  FDRE \initWord_reg[9] 
       (.C(clk_out2),
        .CE(\initWord[30]_i_1_n_0 ),
        .D(\initWord[9]_i_1_n_0 ),
        .Q(data2[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    msg_i_1
       (.I0(\state_reg_n_0_[3] ),
        .I1(\state_reg_n_0_[2] ),
        .I2(\state_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[1] ),
        .O(msg0));
  FDRE msg_reg
       (.C(clk_out2),
        .CE(reset_n),
        .D(msg0),
        .Q(msg),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    \state[1]_i_3 
       (.I0(\state[1]_i_4_n_0 ),
        .I1(\initA_reg_n_0_[6] ),
        .I2(\initA_reg_n_0_[5] ),
        .I3(\state[1]_i_5_n_0 ),
        .I4(\initA_reg_n_0_[4] ),
        .I5(\state[1]_i_6_n_0 ),
        .O(\state[1]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \state[1]_i_4 
       (.I0(data1[2]),
        .I1(data1[7]),
        .I2(data1[0]),
        .I3(data1[1]),
        .I4(\state[3]_i_9_n_0 ),
        .O(\state[1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \state[1]_i_5 
       (.I0(\initA_reg_n_0_[1] ),
        .I1(\initA_reg_n_0_[0] ),
        .O(\state[1]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \state[1]_i_6 
       (.I0(\initA_reg_n_0_[3] ),
        .I1(\initA_reg_n_0_[2] ),
        .O(\state[1]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h40404044)) 
    \state[2]_i_2 
       (.I0(\state_reg_n_0_[3] ),
        .I1(\state_reg_n_0_[1] ),
        .I2(\state_reg_n_0_[0] ),
        .I3(\initWord_reg_n_0_[0] ),
        .I4(\initWord_reg_n_0_[6] ),
        .O(\state[2]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_10 
       (.I0(delaycnt[10]),
        .I1(delaycnt[11]),
        .I2(delaycnt[9]),
        .I3(delaycnt[8]),
        .O(\state[3]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_11 
       (.I0(delaycnt[3]),
        .I1(delaycnt[2]),
        .I2(delaycnt[1]),
        .I3(delaycnt[0]),
        .O(\state[3]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_12 
       (.I0(delaycnt[19]),
        .I1(delaycnt[18]),
        .I2(delaycnt[16]),
        .I3(delaycnt[17]),
        .O(\state[3]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \state[3]_i_13 
       (.I0(delaycnt[31]),
        .I1(delaycnt[30]),
        .I2(delaycnt[28]),
        .I3(delaycnt[29]),
        .O(\state[3]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \state[3]_i_3 
       (.I0(\state[3]_i_5_n_0 ),
        .I1(\state[3]_i_6_n_0 ),
        .I2(\state[3]_i_7_n_0 ),
        .I3(\state[3]_i_8_n_0 ),
        .O(\state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \state[3]_i_4 
       (.I0(\state_reg_n_0_[1] ),
        .I1(\state[3]_i_9_n_0 ),
        .I2(data1[1]),
        .I3(data1[0]),
        .I4(data1[7]),
        .I5(data1[2]),
        .O(\state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \state[3]_i_5 
       (.I0(delaycnt[12]),
        .I1(delaycnt[13]),
        .I2(delaycnt[14]),
        .I3(delaycnt[15]),
        .I4(\state[3]_i_10_n_0 ),
        .O(\state[3]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \state[3]_i_6 
       (.I0(delaycnt[4]),
        .I1(delaycnt[7]),
        .I2(delaycnt[5]),
        .I3(delaycnt[6]),
        .I4(\state[3]_i_11_n_0 ),
        .O(\state[3]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \state[3]_i_7 
       (.I0(delaycnt[23]),
        .I1(delaycnt[22]),
        .I2(delaycnt[20]),
        .I3(delaycnt[21]),
        .I4(\state[3]_i_12_n_0 ),
        .O(\state[3]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \state[3]_i_8 
       (.I0(delaycnt[24]),
        .I1(delaycnt[26]),
        .I2(delaycnt[27]),
        .I3(delaycnt[25]),
        .I4(\state[3]_i_13_n_0 ),
        .O(\state[3]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \state[3]_i_9 
       (.I0(\state_reg_n_0_[0] ),
        .I1(data1[3]),
        .I2(data1[4]),
        .I3(data1[5]),
        .O(\state[3]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[0] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(twi_controller_n_3),
        .Q(\state_reg_n_0_[0] ),
        .R(SR));
  FDSE #(
    .INIT(1'b1)) 
    \state_reg[1] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(twi_controller_n_2),
        .Q(\state_reg_n_0_[1] ),
        .S(SR));
  FDSE #(
    .INIT(1'b1)) 
    \state_reg[2] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(twi_controller_n_1),
        .Q(\state_reg_n_0_[2] ),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[3] 
       (.C(clk_out2),
        .CE(twi_controller_n_4),
        .D(twi_controller_n_0),
        .Q(\state_reg_n_0_[3] ),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h8055)) 
    stb_i_1
       (.I0(\state_reg_n_0_[3] ),
        .I1(\state_reg_n_0_[1] ),
        .I2(\state_reg_n_0_[0] ),
        .I3(\state_reg_n_0_[2] ),
        .O(stb_i_1_n_0));
  FDRE stb_reg
       (.C(clk_out2),
        .CE(reset_n),
        .D(stb_i_1_n_0),
        .Q(stb),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TWICtl twi_controller
       (.D({twi_controller_n_0,twi_controller_n_1,twi_controller_n_2,twi_controller_n_3}),
        .E(twi_controller_n_4),
        .Q({\state_reg_n_0_[3] ,\state_reg_n_0_[2] ,\state_reg_n_0_[1] ,\state_reg_n_0_[0] }),
        .clk_out2(clk_out2),
        .data_i(data_i),
        .\delaycnt_reg[12] (\state[3]_i_3_n_0 ),
        .\initA_reg[6] (twi_controller_n_5),
        .\initA_reg[6]_0 (\state[1]_i_3_n_0 ),
        .initEn(initEn),
        .initEn_reg(twi_controller_n_6),
        .\initWord_reg[6] ({\initWord_reg_n_0_[6] ,\initWord_reg_n_0_[0] }),
        .msg(msg),
        .reset_n(reset_n),
        .scl(scl),
        .sda(sda),
        .\state_reg[1] (\state[3]_i_4_n_0 ),
        .\state_reg[3] (\state[2]_i_2_n_0 ),
        .\state_reg[3]_0 (\initA[6]_i_3_n_0 ),
        .stb(stb));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
   (clk_out1,
    clk_out2,
    clk_out3,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  input resetn;
  input clk_in1;
  input lopt;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire clk_out3;
  wire lopt;
  wire resetn;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .clk_out3(clk_out3),
        .lopt(lopt),
        .resetn(resetn));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz
   (clk_out1,
    clk_out2,
    clk_out3,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  input resetn;
  input clk_in1;
  input lopt;

  wire clk_in1;
  wire clk_out1;
  wire clk_out1_clk_wiz_0;
  wire clk_out2;
  wire clk_out2_clk_wiz_0;
  wire clk_out3;
  wire clk_out3_clk_wiz_0;
  wire clkfbout_buf_clk_wiz_0;
  wire clkfbout_clk_wiz_0;
  wire lopt;
  wire reset_high;
  wire resetn;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_LOCKED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_0),
        .O(clkfbout_buf_clk_wiz_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_0),
        .O(clk_out1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_0),
        .O(clk_out2));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout3_buf
       (.I(clk_out3_clk_wiz_0),
        .O(clk_out3));
  (* BOX_TYPE = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(40.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(8),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_0),
        .CLKFBOUT(clkfbout_clk_wiz_0),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(lopt),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_0),
        .CLKOUT1B(clk_out3_clk_wiz_0),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(NLW_mmcm_adv_inst_LOCKED_UNCONNECTED),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(reset_high));
  LUT1 #(
    .INIT(2'h1)) 
    mmcm_adv_inst_i_1
       (.I0(resetn),
        .O(reset_high));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1
   (clk_out1,
    clk_out2,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  input resetn;
  input clk_in1;
  output lopt;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire lopt;
  wire resetn;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .lopt(lopt),
        .resetn(resetn));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz
   (clk_out1,
    clk_out2,
    resetn,
    clk_in1,
    lopt);
  output clk_out1;
  output clk_out2;
  input resetn;
  input clk_in1;
  output lopt;

  wire clk_in1;
  wire clk_in1_clk_wiz_1;
  wire clk_out1;
  wire clk_out1_clk_wiz_1;
  wire clk_out2;
  wire clk_out2_clk_wiz_1;
  wire clkfbout_buf_clk_wiz_1;
  wire clkfbout_clk_wiz_1;
  wire reset_high;
  wire resetn;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_LOCKED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  assign lopt = clk_in1_clk_wiz_1;
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_1),
        .O(clkfbout_buf_clk_wiz_1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibufg
       (.I(clk_in1),
        .O(clk_in1_clk_wiz_1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_1),
        .O(clk_out1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_1),
        .O(clk_out2));
  (* BOX_TYPE = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(81.375000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(20),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_1),
        .CLKFBOUT(clkfbout_clk_wiz_1),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1_clk_wiz_1),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_1),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(NLW_mmcm_adv_inst_LOCKED_UNCONNECTED),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(reset_high));
  LUT1 #(
    .INIT(2'h1)) 
    mmcm_adv_inst_i_1
       (.I0(resetn),
        .O(reset_high));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_my_oscope_0_0,my_oscope_ip_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "my_oscope_ip_v1_0,Vivado 2017.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    reset_n,
    ac_mclk,
    ac_adc_sdata,
    ac_dac_sdata,
    ac_bclk,
    ac_lrclk,
    scl,
    sda,
    tmds,
    tmdsb,
    switch,
    btn,
    ready,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_RESET reset_n, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1" *) input clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 reset_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME reset_n, POLARITY ACTIVE_LOW" *) input reset_n;
  output ac_mclk;
  input ac_adc_sdata;
  output ac_dac_sdata;
  output ac_bclk;
  output ac_lrclk;
  inout scl;
  inout sda;
  output [3:0]tmds;
  output [3:0]tmdsb;
  input [3:0]switch;
  input [4:0]btn;
  output ready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 32, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 7, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [6:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [6:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire [4:0]btn;
  (* IBUF_LOW_PWR *) wire clk;
  wire ready;
  wire reset_n;
  wire s00_axi_aclk;
  wire [6:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [6:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire scl;
  wire sda;
  wire [3:0]switch;
  (* SLEW = "SLOW" *) wire [3:0]tmds;
  (* SLEW = "SLOW" *) wire [3:0]tmdsb;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .btn(btn),
        .clk(clk),
        .ready(ready),
        .reset_n(reset_n),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[6:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[6:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .scl(scl),
        .sda(sda),
        .switch(switch),
        .tmds(tmds),
        .tmdsb(tmdsb));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid
   (red_s,
    green_s,
    blue_s,
    clock_s,
    Q,
    \encoded_reg[3] ,
    \encoded_reg[0] ,
    \int_trigger_time_s_reg[0] ,
    \int_trigger_time_s_reg[0]_0 ,
    \encoded_reg[0]_0 ,
    \encoded_reg[0]_1 ,
    \encoded_reg[0]_2 ,
    \encoded_reg[0]_3 ,
    \encoded_reg[0]_4 ,
    \encoded_reg[0]_5 ,
    \encoded_reg[0]_6 ,
    \dc_bias_reg[0] ,
    \encoded_reg[8] ,
    \encoded_reg[8]_0 ,
    \state_reg[0] ,
    \state_reg[0]_0 ,
    \encoded_reg[8]_1 ,
    \state_reg[0]_1 ,
    \state_reg[0]_2 ,
    \encoded_reg[8]_2 ,
    \encoded_reg[8]_3 ,
    \encoded_reg[8]_4 ,
    \state_reg[0]_3 ,
    \encoded_reg[8]_5 ,
    \encoded_reg[8]_6 ,
    \encoded_reg[8]_7 ,
    \encoded_reg[8]_8 ,
    \encoded_reg[8]_9 ,
    \encoded_reg[8]_10 ,
    \encoded_reg[8]_11 ,
    \encoded_reg[8]_12 ,
    \encoded_reg[8]_13 ,
    \encoded_reg[8]_14 ,
    \encoded_reg[8]_15 ,
    \encoded_reg[8]_16 ,
    \encoded_reg[8]_17 ,
    \encoded_reg[8]_18 ,
    \encoded_reg[8]_19 ,
    \encoded_reg[8]_20 ,
    \state_reg[0]_4 ,
    \encoded_reg[8]_21 ,
    \encoded_reg[9] ,
    clk_out2,
    clk_out3,
    \processQ_reg[5] ,
    CLK,
    \dc_bias_reg[3] ,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[3]_1 ,
    \processQ_reg[6] ,
    \dc_bias_reg[3]_2 ,
    \dc_bias_reg[3]_3 ,
    \dc_bias_reg[3]_4 ,
    \dc_bias_reg[3]_5 ,
    \dc_bias_reg[3]_6 ,
    \processQ_reg[9] ,
    \processQ_reg[5]_0 ,
    \processQ_reg[5]_1 ,
    \slv_reg10_reg[7] ,
    \slv_reg10_reg[2] ,
    \slv_reg10_reg[4] ,
    \slv_reg10_reg[6] ,
    \slv_reg10_reg[1] ,
    \slv_reg10_reg[5] ,
    \slv_reg5_reg[0] ,
    \int_trigger_time_s_reg[5] ,
    \processQ_reg[8] ,
    \processQ_reg[8]_0 ,
    \processQ_reg[9]_0 ,
    \slv_reg11_reg[9] ,
    \int_trigger_volt_s_reg[9] ,
    SR);
  output red_s;
  output green_s;
  output blue_s;
  output clock_s;
  output [0:0]Q;
  output [0:0]\encoded_reg[3] ;
  output \encoded_reg[0] ;
  output \int_trigger_time_s_reg[0] ;
  output \int_trigger_time_s_reg[0]_0 ;
  output \encoded_reg[0]_0 ;
  output \encoded_reg[0]_1 ;
  output \encoded_reg[0]_2 ;
  output \encoded_reg[0]_3 ;
  output \encoded_reg[0]_4 ;
  output \encoded_reg[0]_5 ;
  output \encoded_reg[0]_6 ;
  output [0:0]\dc_bias_reg[0] ;
  output \encoded_reg[8] ;
  output \encoded_reg[8]_0 ;
  output \state_reg[0] ;
  output \state_reg[0]_0 ;
  output \encoded_reg[8]_1 ;
  output \state_reg[0]_1 ;
  output \state_reg[0]_2 ;
  output \encoded_reg[8]_2 ;
  output \encoded_reg[8]_3 ;
  output \encoded_reg[8]_4 ;
  output \state_reg[0]_3 ;
  output \encoded_reg[8]_5 ;
  output \encoded_reg[8]_6 ;
  output \encoded_reg[8]_7 ;
  output \encoded_reg[8]_8 ;
  output \encoded_reg[8]_9 ;
  output \encoded_reg[8]_10 ;
  output \encoded_reg[8]_11 ;
  output \encoded_reg[8]_12 ;
  output \encoded_reg[8]_13 ;
  output \encoded_reg[8]_14 ;
  output \encoded_reg[8]_15 ;
  output \encoded_reg[8]_16 ;
  output \encoded_reg[8]_17 ;
  output \encoded_reg[8]_18 ;
  output \encoded_reg[8]_19 ;
  output \encoded_reg[8]_20 ;
  output \state_reg[0]_4 ;
  output \encoded_reg[8]_21 ;
  output \encoded_reg[9] ;
  input clk_out2;
  input clk_out3;
  input \processQ_reg[5] ;
  input CLK;
  input \dc_bias_reg[3] ;
  input \dc_bias_reg[3]_0 ;
  input \dc_bias_reg[3]_1 ;
  input \processQ_reg[6] ;
  input \dc_bias_reg[3]_2 ;
  input \dc_bias_reg[3]_3 ;
  input \dc_bias_reg[3]_4 ;
  input \dc_bias_reg[3]_5 ;
  input \dc_bias_reg[3]_6 ;
  input \processQ_reg[9] ;
  input \processQ_reg[5]_0 ;
  input \processQ_reg[5]_1 ;
  input \slv_reg10_reg[7] ;
  input \slv_reg10_reg[2] ;
  input \slv_reg10_reg[4] ;
  input \slv_reg10_reg[6] ;
  input \slv_reg10_reg[1] ;
  input [3:0]\slv_reg10_reg[5] ;
  input [0:0]\slv_reg5_reg[0] ;
  input [3:0]\int_trigger_time_s_reg[5] ;
  input \processQ_reg[8] ;
  input \processQ_reg[8]_0 ;
  input \processQ_reg[9]_0 ;
  input [9:0]\slv_reg11_reg[9] ;
  input [9:0]\int_trigger_volt_s_reg[9] ;
  input [0:0]SR;

  wire CLK;
  wire D0;
  wire D1;
  wire [0:0]Q;
  wire [0:0]SR;
  wire TDMS_encoder_blue_n_0;
  wire TDMS_encoder_blue_n_1;
  wire TDMS_encoder_blue_n_2;
  wire TDMS_encoder_blue_n_3;
  wire TDMS_encoder_blue_n_4;
  wire TDMS_encoder_blue_n_5;
  wire TDMS_encoder_green_n_0;
  wire TDMS_encoder_green_n_1;
  wire TDMS_encoder_green_n_2;
  wire TDMS_encoder_green_n_3;
  wire TDMS_encoder_red_n_0;
  wire TDMS_encoder_red_n_1;
  wire TDMS_encoder_red_n_2;
  wire TDMS_encoder_red_n_3;
  wire TDMS_encoder_red_n_4;
  wire blue_s;
  wire clk_out2;
  wire clk_out3;
  wire clock_s;
  wire [7:0]data1;
  wire [0:0]\dc_bias_reg[0] ;
  wire \dc_bias_reg[3] ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire \dc_bias_reg[3]_3 ;
  wire \dc_bias_reg[3]_4 ;
  wire \dc_bias_reg[3]_5 ;
  wire \dc_bias_reg[3]_6 ;
  wire \encoded_reg[0] ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[0]_1 ;
  wire \encoded_reg[0]_2 ;
  wire \encoded_reg[0]_3 ;
  wire \encoded_reg[0]_4 ;
  wire \encoded_reg[0]_5 ;
  wire \encoded_reg[0]_6 ;
  wire [0:0]\encoded_reg[3] ;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \encoded_reg[8]_10 ;
  wire \encoded_reg[8]_11 ;
  wire \encoded_reg[8]_12 ;
  wire \encoded_reg[8]_13 ;
  wire \encoded_reg[8]_14 ;
  wire \encoded_reg[8]_15 ;
  wire \encoded_reg[8]_16 ;
  wire \encoded_reg[8]_17 ;
  wire \encoded_reg[8]_18 ;
  wire \encoded_reg[8]_19 ;
  wire \encoded_reg[8]_2 ;
  wire \encoded_reg[8]_20 ;
  wire \encoded_reg[8]_21 ;
  wire \encoded_reg[8]_3 ;
  wire \encoded_reg[8]_4 ;
  wire \encoded_reg[8]_5 ;
  wire \encoded_reg[8]_6 ;
  wire \encoded_reg[8]_7 ;
  wire \encoded_reg[8]_8 ;
  wire \encoded_reg[8]_9 ;
  wire \encoded_reg[9] ;
  wire green_s;
  wire \int_trigger_time_s_reg[0] ;
  wire \int_trigger_time_s_reg[0]_0 ;
  wire [3:0]\int_trigger_time_s_reg[5] ;
  wire [9:0]\int_trigger_volt_s_reg[9] ;
  wire [9:0]latched_blue;
  wire [9:0]latched_green;
  wire [9:0]latched_red;
  wire \processQ_reg[5] ;
  wire \processQ_reg[5]_0 ;
  wire \processQ_reg[5]_1 ;
  wire \processQ_reg[6] ;
  wire \processQ_reg[8] ;
  wire \processQ_reg[8]_0 ;
  wire \processQ_reg[9] ;
  wire \processQ_reg[9]_0 ;
  wire red_s;
  wire [6:0]shift_blue;
  wire \shift_blue[1]_i_1_n_0 ;
  wire \shift_blue[3]_i_1_n_0 ;
  wire \shift_blue[5]_i_1_n_0 ;
  wire \shift_blue[7]_i_1_n_0 ;
  wire \shift_blue[7]_i_2_n_0 ;
  wire \shift_blue_reg_n_0_[0] ;
  wire \shift_blue_reg_n_0_[1] ;
  wire \shift_blue_reg_n_0_[2] ;
  wire \shift_blue_reg_n_0_[3] ;
  wire \shift_blue_reg_n_0_[4] ;
  wire \shift_blue_reg_n_0_[5] ;
  wire \shift_blue_reg_n_0_[6] ;
  wire \shift_blue_reg_n_0_[7] ;
  wire \shift_blue_reg_n_0_[8] ;
  wire \shift_blue_reg_n_0_[9] ;
  wire [1:0]shift_clock;
  wire [9:2]shift_clock__0;
  wire [6:2]shift_green;
  wire \shift_green[0]_i_1_n_0 ;
  wire \shift_green[1]_i_1_n_0 ;
  wire \shift_green[3]_i_1_n_0 ;
  wire \shift_green[5]_i_1_n_0 ;
  wire \shift_green[7]_i_1_n_0 ;
  wire \shift_green[7]_i_2_n_0 ;
  wire \shift_green_reg_n_0_[0] ;
  wire \shift_green_reg_n_0_[1] ;
  wire \shift_green_reg_n_0_[2] ;
  wire \shift_green_reg_n_0_[3] ;
  wire \shift_green_reg_n_0_[4] ;
  wire \shift_green_reg_n_0_[5] ;
  wire \shift_green_reg_n_0_[6] ;
  wire \shift_green_reg_n_0_[7] ;
  wire \shift_green_reg_n_0_[8] ;
  wire \shift_green_reg_n_0_[9] ;
  wire [7:0]shift_red;
  wire \shift_red[9]_i_1_n_0 ;
  wire \shift_red[9]_i_2_n_0 ;
  wire \slv_reg10_reg[1] ;
  wire \slv_reg10_reg[2] ;
  wire \slv_reg10_reg[4] ;
  wire [3:0]\slv_reg10_reg[5] ;
  wire \slv_reg10_reg[6] ;
  wire \slv_reg10_reg[7] ;
  wire [9:0]\slv_reg11_reg[9] ;
  wire [0:0]\slv_reg5_reg[0] ;
  wire \state_reg[0] ;
  wire \state_reg[0]_0 ;
  wire \state_reg[0]_1 ;
  wire \state_reg[0]_2 ;
  wire \state_reg[0]_3 ;
  wire \state_reg[0]_4 ;
  wire NLW_ODDR2_blue_R_UNCONNECTED;
  wire NLW_ODDR2_blue_S_UNCONNECTED;
  wire NLW_ODDR2_clock_R_UNCONNECTED;
  wire NLW_ODDR2_clock_S_UNCONNECTED;
  wire NLW_ODDR2_green_R_UNCONNECTED;
  wire NLW_ODDR2_green_S_UNCONNECTED;
  wire NLW_ODDR2_red_R_UNCONNECTED;
  wire NLW_ODDR2_red_S_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_blue
       (.C(clk_out2),
        .CE(1'b1),
        .D1(\shift_blue_reg_n_0_[0] ),
        .D2(\shift_blue_reg_n_0_[1] ),
        .Q(blue_s),
        .R(NLW_ODDR2_blue_R_UNCONNECTED),
        .S(NLW_ODDR2_blue_S_UNCONNECTED));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_clock
       (.C(clk_out2),
        .CE(1'b1),
        .D1(shift_clock[0]),
        .D2(shift_clock[1]),
        .Q(clock_s),
        .R(NLW_ODDR2_clock_R_UNCONNECTED),
        .S(NLW_ODDR2_clock_S_UNCONNECTED));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_green
       (.C(clk_out2),
        .CE(1'b1),
        .D1(\shift_green_reg_n_0_[0] ),
        .D2(\shift_green_reg_n_0_[1] ),
        .Q(green_s),
        .R(NLW_ODDR2_green_R_UNCONNECTED),
        .S(NLW_ODDR2_green_S_UNCONNECTED));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_red
       (.C(clk_out2),
        .CE(1'b1),
        .D1(D0),
        .D2(D1),
        .Q(red_s),
        .R(NLW_ODDR2_red_R_UNCONNECTED),
        .S(NLW_ODDR2_red_S_UNCONNECTED));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder TDMS_encoder_blue
       (.CLK(CLK),
        .D({TDMS_encoder_blue_n_0,TDMS_encoder_blue_n_1,TDMS_encoder_blue_n_2,TDMS_encoder_blue_n_3,TDMS_encoder_blue_n_4,TDMS_encoder_blue_n_5}),
        .Q(\dc_bias_reg[0] ),
        .SR(SR),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_2 ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_3 ),
        .\dc_bias_reg[3]_2 (\dc_bias_reg[3]_4 ),
        .\dc_bias_reg[3]_3 (\dc_bias_reg[3]_5 ),
        .\dc_bias_reg[3]_4 (\dc_bias_reg[3]_6 ),
        .\encoded_reg[8]_0 (\encoded_reg[8] ),
        .\encoded_reg[8]_1 (\encoded_reg[8]_1 ),
        .\encoded_reg[8]_2 (\encoded_reg[8]_4 ),
        .\encoded_reg[8]_3 (\encoded_reg[8]_5 ),
        .\encoded_reg[8]_4 (\encoded_reg[8]_11 ),
        .\encoded_reg[8]_5 (\encoded_reg[8]_12 ),
        .\encoded_reg[8]_6 (\encoded_reg[8]_16 ),
        .\encoded_reg[9]_0 (\encoded_reg[9] ),
        .\int_trigger_volt_s_reg[1] (\int_trigger_volt_s_reg[9] [1:0]),
        .\int_trigger_volt_s_reg[3] (\encoded_reg[8]_6 ),
        .\processQ_reg[5] (\processQ_reg[5]_1 ),
        .\processQ_reg[5]_0 (\processQ_reg[5]_0 ),
        .\processQ_reg[6] (\processQ_reg[6] ),
        .\processQ_reg[8] (\processQ_reg[8] ),
        .\processQ_reg[8]_0 (\processQ_reg[8]_0 ),
        .\slv_reg11_reg[1] (\state_reg[0]_3 ),
        .\slv_reg11_reg[1]_0 (\slv_reg11_reg[9] [1:0]),
        .\slv_reg11_reg[2] (\encoded_reg[8]_3 ),
        .\slv_reg11_reg[3] (\state_reg[0]_0 ),
        .\slv_reg11_reg[4] (\state_reg[0]_1 ),
        .\slv_reg11_reg[5] (\state_reg[0] ),
        .\slv_reg11_reg[6] (\state_reg[0]_2 ),
        .\slv_reg11_reg[7] (\encoded_reg[8]_0 ),
        .\slv_reg5_reg[0] (\slv_reg5_reg[0] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1 TDMS_encoder_green
       (.CLK(CLK),
        .D({TDMS_encoder_green_n_0,TDMS_encoder_green_n_1,TDMS_encoder_green_n_2,TDMS_encoder_green_n_3}),
        .Q(Q),
        .SR(SR),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_0 ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_1 ),
        .\processQ_reg[5] (\processQ_reg[5]_0 ),
        .\processQ_reg[9] (\processQ_reg[9] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2 TDMS_encoder_red
       (.CLK(CLK),
        .D({TDMS_encoder_red_n_0,TDMS_encoder_red_n_1,TDMS_encoder_red_n_2,TDMS_encoder_red_n_3,TDMS_encoder_red_n_4}),
        .Q(\encoded_reg[3] ),
        .SR(SR),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3] ),
        .\encoded_reg[0]_0 (\encoded_reg[0] ),
        .\encoded_reg[0]_1 (\encoded_reg[0]_0 ),
        .\encoded_reg[0]_2 (\encoded_reg[0]_1 ),
        .\encoded_reg[0]_3 (\encoded_reg[0]_2 ),
        .\encoded_reg[0]_4 (\encoded_reg[0]_3 ),
        .\encoded_reg[0]_5 (\encoded_reg[0]_4 ),
        .\encoded_reg[0]_6 (\encoded_reg[0]_5 ),
        .\encoded_reg[0]_7 (\encoded_reg[0]_6 ),
        .\encoded_reg[8]_0 (\encoded_reg[8]_2 ),
        .\encoded_reg[8]_1 (\encoded_reg[8]_0 ),
        .\encoded_reg[8]_10 (\encoded_reg[8]_15 ),
        .\encoded_reg[8]_11 (\encoded_reg[8]_17 ),
        .\encoded_reg[8]_12 (\encoded_reg[8]_18 ),
        .\encoded_reg[8]_13 (\encoded_reg[8]_19 ),
        .\encoded_reg[8]_14 (\encoded_reg[8]_20 ),
        .\encoded_reg[8]_15 (\encoded_reg[8]_21 ),
        .\encoded_reg[8]_2 (\encoded_reg[8]_3 ),
        .\encoded_reg[8]_3 (\encoded_reg[8]_7 ),
        .\encoded_reg[8]_4 (\encoded_reg[8]_8 ),
        .\encoded_reg[8]_5 (\encoded_reg[8]_6 ),
        .\encoded_reg[8]_6 (\encoded_reg[8]_9 ),
        .\encoded_reg[8]_7 (\encoded_reg[8]_10 ),
        .\encoded_reg[8]_8 (\encoded_reg[8]_13 ),
        .\encoded_reg[8]_9 (\encoded_reg[8]_14 ),
        .\int_trigger_time_s_reg[0] (\int_trigger_time_s_reg[0] ),
        .\int_trigger_time_s_reg[0]_0 (\int_trigger_time_s_reg[0]_0 ),
        .\int_trigger_time_s_reg[5] (\int_trigger_time_s_reg[5] ),
        .\int_trigger_volt_s_reg[9] (\int_trigger_volt_s_reg[9] ),
        .\processQ_reg[5] (\processQ_reg[5] ),
        .\processQ_reg[5]_0 (\processQ_reg[5]_1 ),
        .\processQ_reg[5]_1 (\processQ_reg[5]_0 ),
        .\processQ_reg[9] (\processQ_reg[9]_0 ),
        .\slv_reg10_reg[1] (\slv_reg10_reg[1] ),
        .\slv_reg10_reg[2] (\slv_reg10_reg[2] ),
        .\slv_reg10_reg[4] (\slv_reg10_reg[4] ),
        .\slv_reg10_reg[5] (\slv_reg10_reg[5] ),
        .\slv_reg10_reg[6] (\slv_reg10_reg[6] ),
        .\slv_reg10_reg[7] (\slv_reg10_reg[7] ),
        .\slv_reg11_reg[0] (\encoded_reg[8]_5 ),
        .\slv_reg11_reg[9] (\slv_reg11_reg[9] ),
        .\slv_reg5_reg[0] (\slv_reg5_reg[0] ),
        .\state_reg[0] (\state_reg[0] ),
        .\state_reg[0]_0 (\state_reg[0]_0 ),
        .\state_reg[0]_1 (\state_reg[0]_1 ),
        .\state_reg[0]_2 (\state_reg[0]_2 ),
        .\state_reg[0]_3 (\state_reg[0]_3 ),
        .\state_reg[0]_4 (\state_reg[0]_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_5),
        .Q(latched_blue[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_4),
        .Q(latched_blue[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_3),
        .Q(latched_blue[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_2),
        .Q(latched_blue[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_1),
        .Q(latched_blue[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_0),
        .Q(latched_blue[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_3),
        .Q(latched_green[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_2),
        .Q(latched_green[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_1),
        .Q(latched_green[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_0),
        .Q(latched_green[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_4),
        .Q(latched_red[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_3),
        .Q(latched_red[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_2),
        .Q(latched_red[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_1),
        .Q(latched_red[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_0),
        .Q(latched_red[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[0]_i_1 
       (.I0(\shift_blue_reg_n_0_[2] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[0]),
        .O(shift_blue[0]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[1]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[3] ),
        .O(\shift_blue[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[2]_i_1 
       (.I0(\shift_blue_reg_n_0_[4] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[2]),
        .O(shift_blue[2]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[3]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[5] ),
        .O(\shift_blue[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[4]_i_1 
       (.I0(\shift_blue_reg_n_0_[6] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[4]),
        .O(shift_blue[4]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[5]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[7] ),
        .O(\shift_blue[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[6]_i_1 
       (.I0(\shift_blue_reg_n_0_[8] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_blue[2]),
        .O(shift_blue[6]));
  LUT2 #(
    .INIT(4'h2)) 
    \shift_blue[7]_i_1 
       (.I0(latched_blue[1]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .O(\shift_blue[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_blue[7]_i_2 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_blue_reg_n_0_[9] ),
        .O(\shift_blue[7]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[0]),
        .Q(\shift_blue_reg_n_0_[0] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[1]_i_1_n_0 ),
        .Q(\shift_blue_reg_n_0_[1] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[2]),
        .Q(\shift_blue_reg_n_0_[2] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[3]_i_1_n_0 ),
        .Q(\shift_blue_reg_n_0_[3] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[4]),
        .Q(\shift_blue_reg_n_0_[4] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[5]_i_1_n_0 ),
        .Q(\shift_blue_reg_n_0_[5] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_blue[6]),
        .Q(\shift_blue_reg_n_0_[6] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_blue_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_blue[7]_i_2_n_0 ),
        .Q(\shift_blue_reg_n_0_[7] ),
        .S(\shift_blue[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_blue[8]),
        .Q(\shift_blue_reg_n_0_[8] ),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_blue[9]),
        .Q(\shift_blue_reg_n_0_[9] ),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[2]),
        .Q(shift_clock[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[3]),
        .Q(shift_clock[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[4]),
        .Q(shift_clock__0[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[5]),
        .Q(shift_clock__0[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[6]),
        .Q(shift_clock__0[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[7]),
        .Q(shift_clock__0[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[8]),
        .Q(shift_clock__0[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock__0[9]),
        .Q(shift_clock__0[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock[0]),
        .Q(shift_clock__0[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_clock[1]),
        .Q(shift_clock__0[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[0]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[2] ),
        .O(\shift_green[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[1]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[3] ),
        .O(\shift_green[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[2]_i_1 
       (.I0(\shift_green_reg_n_0_[4] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_green[2]),
        .O(shift_green[2]));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[3]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[5] ),
        .O(\shift_green[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[4]_i_1 
       (.I0(\shift_green_reg_n_0_[6] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_green[2]),
        .O(shift_green[4]));
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[5]_i_1 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[7] ),
        .O(\shift_green[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[6]_i_1 
       (.I0(\shift_green_reg_n_0_[8] ),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_green[2]),
        .O(shift_green[6]));
  LUT2 #(
    .INIT(4'h2)) 
    \shift_green[7]_i_1 
       (.I0(latched_green[0]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .O(\shift_green[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \shift_green[7]_i_2 
       (.I0(\shift_red[9]_i_1_n_0 ),
        .I1(\shift_green_reg_n_0_[9] ),
        .O(\shift_green[7]_i_2_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[0]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[0] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[1]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[1] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_green[2]),
        .Q(\shift_green_reg_n_0_[2] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[3]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[3] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_green[4]),
        .Q(\shift_green_reg_n_0_[4] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[5]_i_1_n_0 ),
        .Q(\shift_green_reg_n_0_[5] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_green[6]),
        .Q(\shift_green_reg_n_0_[6] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \shift_green_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(\shift_green[7]_i_2_n_0 ),
        .Q(\shift_green_reg_n_0_[7] ),
        .S(\shift_green[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_green[8]),
        .Q(\shift_green_reg_n_0_[8] ),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_green[9]),
        .Q(\shift_green_reg_n_0_[9] ),
        .R(\shift_red[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[0]_i_1 
       (.I0(data1[0]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[0]),
        .O(shift_red[0]));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[1]_i_1 
       (.I0(data1[1]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[0]),
        .O(shift_red[1]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[2]_i_1 
       (.I0(data1[2]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[2]),
        .O(shift_red[2]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[3]_i_1 
       (.I0(data1[3]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[3]),
        .O(shift_red[3]));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[4]_i_1 
       (.I0(data1[4]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[2]),
        .O(shift_red[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[5]_i_1 
       (.I0(data1[5]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[0]),
        .O(shift_red[5]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[6]_i_1 
       (.I0(data1[6]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[2]),
        .O(shift_red[6]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[7]_i_1 
       (.I0(data1[7]),
        .I1(\shift_red[9]_i_1_n_0 ),
        .I2(latched_red[3]),
        .O(shift_red[7]));
  LUT5 #(
    .INIT(32'hEFFFFFFF)) 
    \shift_red[9]_i_1 
       (.I0(\shift_red[9]_i_2_n_0 ),
        .I1(shift_clock__0[5]),
        .I2(shift_clock__0[4]),
        .I3(shift_clock__0[2]),
        .I4(shift_clock__0[3]),
        .O(\shift_red[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFFFFFF)) 
    \shift_red[9]_i_2 
       (.I0(shift_clock__0[8]),
        .I1(shift_clock__0[9]),
        .I2(shift_clock__0[6]),
        .I3(shift_clock__0[7]),
        .I4(shift_clock[1]),
        .I5(shift_clock[0]),
        .O(\shift_red[9]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[0] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[0]),
        .Q(D0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[1] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[1]),
        .Q(D1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[2] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[2]),
        .Q(data1[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[3] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[3]),
        .Q(data1[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[4] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[4]),
        .Q(data1[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[5] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[5]),
        .Q(data1[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[6] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[6]),
        .Q(data1[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[7] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(shift_red[7]),
        .Q(data1[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[8] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_red[8]),
        .Q(data1[6]),
        .R(\shift_red[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[9] 
       (.C(clk_out2),
        .CE(1'b1),
        .D(latched_red[9]),
        .Q(data1[7]),
        .R(\shift_red[9]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_flagRegister
   (D,
    \Q_reg[0]_0 ,
    \axi_araddr_reg[4] ,
    \axi_araddr_reg[4]_0 ,
    \axi_araddr_reg[6] ,
    \axi_araddr_reg[4]_1 ,
    \slv_reg15_reg[2] ,
    \slv_reg11_reg[2] ,
    \slv_reg10_reg[2] ,
    \axi_araddr_reg[3]_rep ,
    \axi_araddr_reg[2]_rep ,
    \slv_reg3_reg[2] ,
    \processQ_reg[9] ,
    CLK,
    reset_n,
    clk);
  output [0:0]D;
  output [0:0]\Q_reg[0]_0 ;
  input \axi_araddr_reg[4] ;
  input \axi_araddr_reg[4]_0 ;
  input [2:0]\axi_araddr_reg[6] ;
  input \axi_araddr_reg[4]_1 ;
  input \slv_reg15_reg[2] ;
  input [0:0]\slv_reg11_reg[2] ;
  input [0:0]\slv_reg10_reg[2] ;
  input \axi_araddr_reg[3]_rep ;
  input \axi_araddr_reg[2]_rep ;
  input [1:0]\slv_reg3_reg[2] ;
  input [0:0]\processQ_reg[9] ;
  input CLK;
  input reset_n;
  input clk;

  wire CLK;
  wire [0:0]D;
  wire \Q[0]_i_1_n_0 ;
  wire \Q[2]_i_1_n_0 ;
  wire [0:0]\Q_reg[0]_0 ;
  wire \axi_araddr_reg[2]_rep ;
  wire \axi_araddr_reg[3]_rep ;
  wire \axi_araddr_reg[4] ;
  wire \axi_araddr_reg[4]_0 ;
  wire \axi_araddr_reg[4]_1 ;
  wire [2:0]\axi_araddr_reg[6] ;
  wire \axi_rdata[2]_i_10_n_0 ;
  wire \axi_rdata_reg[2]_i_4_n_0 ;
  wire clk;
  wire [2:2]flagQ;
  wire [0:0]\processQ_reg[9] ;
  wire reset_n;
  wire [0:0]\slv_reg10_reg[2] ;
  wire [0:0]\slv_reg11_reg[2] ;
  wire \slv_reg15_reg[2] ;
  wire [1:0]\slv_reg3_reg[2] ;

  LUT3 #(
    .INIT(8'hD4)) 
    \Q[0]_i_1 
       (.I0(\slv_reg3_reg[2] [0]),
        .I1(CLK),
        .I2(\Q_reg[0]_0 ),
        .O(\Q[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Q[2]_i_1 
       (.I0(\slv_reg3_reg[2] [1]),
        .I1(\processQ_reg[9] ),
        .I2(flagQ),
        .O(\Q[2]_i_1_n_0 ));
  FDRE \Q_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\Q[0]_i_1_n_0 ),
        .Q(\Q_reg[0]_0 ),
        .R(reset_n));
  FDRE \Q_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\Q[2]_i_1_n_0 ),
        .Q(flagQ),
        .R(reset_n));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_araddr_reg[4] ),
        .I1(\axi_araddr_reg[4]_0 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_rdata_reg[2]_i_4_n_0 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_araddr_reg[4]_1 ),
        .O(D));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[2]_i_10 
       (.I0(\slv_reg11_reg[2] ),
        .I1(\slv_reg10_reg[2] ),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(flagQ),
        .I4(\axi_araddr_reg[2]_rep ),
        .O(\axi_rdata[2]_i_10_n_0 ));
  MUXF7 \axi_rdata_reg[2]_i_4 
       (.I0(\axi_rdata[2]_i_10_n_0 ),
        .I1(\slv_reg15_reg[2] ),
        .O(\axi_rdata_reg[2]_i_4_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_hcounter
   (DI,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    \encoded_reg[8] ,
    \encoded_reg[8]_0 ,
    \encoded_reg[8]_1 ,
    \processQ_reg[0]_0 ,
    ADDRARDADDR,
    \encoded_reg[8]_2 ,
    \encoded_reg[8]_3 ,
    S,
    \encoded_reg[8]_4 ,
    \encoded_reg[8]_5 ,
    \encoded_reg[8]_6 ,
    \encoded_reg[8]_7 ,
    \encoded_reg[8]_8 ,
    \encoded_reg[8]_9 ,
    \encoded_reg[8]_10 ,
    \encoded_reg[0] ,
    \encoded_reg[8]_11 ,
    \encoded_reg[8]_12 ,
    \encoded_reg[8]_13 ,
    \encoded_reg[8]_14 ,
    \encoded_reg[8]_15 ,
    \encoded_reg[8]_16 ,
    \encoded_reg[8]_17 ,
    \encoded_reg[0]_0 ,
    \encoded_reg[0]_1 ,
    \encoded_reg[0]_2 ,
    \encoded_reg[0]_3 ,
    \encoded_reg[0]_4 ,
    \encoded_reg[0]_5 ,
    \encoded_reg[0]_6 ,
    \encoded_reg[0]_7 ,
    \encoded_reg[8]_18 ,
    \dc_bias_reg[3] ,
    \encoded_reg[8]_19 ,
    \encoded_reg[2] ,
    \encoded_reg[1] ,
    \encoded_reg[4] ,
    \encoded_reg[0]_8 ,
    \encoded_reg[0]_9 ,
    \encoded_reg[8]_20 ,
    \encoded_reg[0]_10 ,
    \encoded_reg[0]_11 ,
    \encoded_reg[8]_21 ,
    \encoded_reg[8]_22 ,
    \encoded_reg[8]_23 ,
    \encoded_reg[8]_24 ,
    \encoded_reg[9] ,
    \encoded_reg[8]_25 ,
    \encoded_reg[8]_26 ,
    \encoded_reg[9]_0 ,
    Q,
    \slv_reg5_reg[0] ,
    \slv_reg10_reg[9] ,
    \slv_reg10_reg[1] ,
    \processQ_reg[5]_0 ,
    \dc_bias_reg[1] ,
    reset_n,
    \dc_bias_reg[2] ,
    \slv_reg10_reg[7] ,
    \slv_reg10_reg[6] ,
    \int_trigger_time_s_reg[5] ,
    \slv_reg10_reg[9]_0 ,
    \slv_reg10_reg[8] ,
    \slv_reg10_reg[7]_0 ,
    \slv_reg10_reg[5] ,
    \slv_reg10_reg[6]_0 ,
    \slv_reg10_reg[6]_1 ,
    \slv_reg10_reg[5]_0 ,
    \slv_reg10_reg[2] ,
    \slv_reg10_reg[3] ,
    \slv_reg10_reg[4] ,
    \slv_reg10_reg[5]_1 ,
    \slv_reg10_reg[6]_2 ,
    \slv_reg10_reg[5]_2 ,
    \int_trigger_time_s_reg[7] ,
    \int_trigger_time_s_reg[1] ,
    \slv_reg10_reg[6]_3 ,
    \int_trigger_time_s_reg[1]_0 ,
    \slv_reg10_reg[6]_4 ,
    \slv_reg10_reg[0] ,
    \int_trigger_time_s_reg[1]_1 ,
    \int_trigger_time_s_reg[2] ,
    \int_trigger_time_s_reg[3] ,
    \int_trigger_time_s_reg[1]_2 ,
    \int_trigger_time_s_reg[1]_3 ,
    \slv_reg10_reg[2]_0 ,
    \slv_reg10_reg[0]_0 ,
    \processQ_reg[4]_0 ,
    CO,
    switch,
    \processQ_reg[9]_0 ,
    \processQ_reg[8]_0 ,
    \processQ_reg[9]_1 ,
    \processQ_reg[2]_0 ,
    \processQ_reg[8]_1 ,
    \dc_bias_reg[3]_0 ,
    \processQ_reg[9]_2 ,
    \processQ_reg[2]_1 ,
    \processQ_reg[4]_1 ,
    \processQ_reg[9]_3 ,
    \processQ_reg[9]_4 ,
    \processQ_reg[0]_1 ,
    \processQ_reg[9]_5 ,
    \processQ_reg[8]_2 ,
    \processQ_reg[8]_3 ,
    \processQ_reg[0]_2 ,
    \slv_reg10_reg[9]_1 ,
    \processQ_reg[9]_6 ,
    \processQ_reg[9]_7 ,
    \processQ_reg[9]_8 ,
    \processQ_reg[9]_9 ,
    \processQ_reg[9]_10 ,
    \slv_reg10_reg[7]_1 ,
    \dc_bias_reg[3]_1 ,
    CLK);
  output [3:0]DI;
  output [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  output \encoded_reg[8] ;
  output \encoded_reg[8]_0 ;
  output \encoded_reg[8]_1 ;
  output \processQ_reg[0]_0 ;
  output [7:0]ADDRARDADDR;
  output \encoded_reg[8]_2 ;
  output \encoded_reg[8]_3 ;
  output [3:0]S;
  output [3:0]\encoded_reg[8]_4 ;
  output [0:0]\encoded_reg[8]_5 ;
  output [0:0]\encoded_reg[8]_6 ;
  output [3:0]\encoded_reg[8]_7 ;
  output [3:0]\encoded_reg[8]_8 ;
  output [0:0]\encoded_reg[8]_9 ;
  output [0:0]\encoded_reg[8]_10 ;
  output [3:0]\encoded_reg[0] ;
  output [3:0]\encoded_reg[8]_11 ;
  output [3:0]\encoded_reg[8]_12 ;
  output [0:0]\encoded_reg[8]_13 ;
  output [0:0]\encoded_reg[8]_14 ;
  output [3:0]\encoded_reg[8]_15 ;
  output [0:0]\encoded_reg[8]_16 ;
  output [0:0]\encoded_reg[8]_17 ;
  output [3:0]\encoded_reg[0]_0 ;
  output [3:0]\encoded_reg[0]_1 ;
  output [0:0]\encoded_reg[0]_2 ;
  output [0:0]\encoded_reg[0]_3 ;
  output [3:0]\encoded_reg[0]_4 ;
  output [3:0]\encoded_reg[0]_5 ;
  output [0:0]\encoded_reg[0]_6 ;
  output [0:0]\encoded_reg[0]_7 ;
  output \encoded_reg[8]_18 ;
  output \dc_bias_reg[3] ;
  output \encoded_reg[8]_19 ;
  output \encoded_reg[2] ;
  output \encoded_reg[1] ;
  output \encoded_reg[4] ;
  output \encoded_reg[0]_8 ;
  output \encoded_reg[0]_9 ;
  output \encoded_reg[8]_20 ;
  output \encoded_reg[0]_10 ;
  output \encoded_reg[0]_11 ;
  output \encoded_reg[8]_21 ;
  output \encoded_reg[8]_22 ;
  output \encoded_reg[8]_23 ;
  output \encoded_reg[8]_24 ;
  output \encoded_reg[9] ;
  output \encoded_reg[8]_25 ;
  output \encoded_reg[8]_26 ;
  output \encoded_reg[9]_0 ;
  input [3:0]Q;
  input [0:0]\slv_reg5_reg[0] ;
  input [3:0]\slv_reg10_reg[9] ;
  input \slv_reg10_reg[1] ;
  input \processQ_reg[5]_0 ;
  input \dc_bias_reg[1] ;
  input reset_n;
  input \dc_bias_reg[2] ;
  input \slv_reg10_reg[7] ;
  input \slv_reg10_reg[6] ;
  input \int_trigger_time_s_reg[5] ;
  input \slv_reg10_reg[9]_0 ;
  input \slv_reg10_reg[8] ;
  input \slv_reg10_reg[7]_0 ;
  input \slv_reg10_reg[5] ;
  input \slv_reg10_reg[6]_0 ;
  input \slv_reg10_reg[6]_1 ;
  input \slv_reg10_reg[5]_0 ;
  input \slv_reg10_reg[2] ;
  input \slv_reg10_reg[3] ;
  input \slv_reg10_reg[4] ;
  input \slv_reg10_reg[5]_1 ;
  input \slv_reg10_reg[6]_2 ;
  input \slv_reg10_reg[5]_2 ;
  input \int_trigger_time_s_reg[7] ;
  input \int_trigger_time_s_reg[1] ;
  input \slv_reg10_reg[6]_3 ;
  input \int_trigger_time_s_reg[1]_0 ;
  input \slv_reg10_reg[6]_4 ;
  input \slv_reg10_reg[0] ;
  input \int_trigger_time_s_reg[1]_1 ;
  input \int_trigger_time_s_reg[2] ;
  input \int_trigger_time_s_reg[3] ;
  input \int_trigger_time_s_reg[1]_2 ;
  input \int_trigger_time_s_reg[1]_3 ;
  input \slv_reg10_reg[2]_0 ;
  input \slv_reg10_reg[0]_0 ;
  input \processQ_reg[4]_0 ;
  input [0:0]CO;
  input [1:0]switch;
  input \processQ_reg[9]_0 ;
  input \processQ_reg[8]_0 ;
  input \processQ_reg[9]_1 ;
  input \processQ_reg[2]_0 ;
  input \processQ_reg[8]_1 ;
  input [0:0]\dc_bias_reg[3]_0 ;
  input \processQ_reg[9]_2 ;
  input \processQ_reg[2]_1 ;
  input \processQ_reg[4]_1 ;
  input [0:0]\processQ_reg[9]_3 ;
  input [0:0]\processQ_reg[9]_4 ;
  input \processQ_reg[0]_1 ;
  input [0:0]\processQ_reg[9]_5 ;
  input \processQ_reg[8]_2 ;
  input \processQ_reg[8]_3 ;
  input \processQ_reg[0]_2 ;
  input [0:0]\slv_reg10_reg[9]_1 ;
  input [2:0]\processQ_reg[9]_6 ;
  input [0:0]\processQ_reg[9]_7 ;
  input [0:0]\processQ_reg[9]_8 ;
  input [0:0]\processQ_reg[9]_9 ;
  input [0:0]\processQ_reg[9]_10 ;
  input \slv_reg10_reg[7]_1 ;
  input [0:0]\dc_bias_reg[3]_1 ;
  input CLK;

  wire [7:0]ADDRARDADDR;
  wire CLK;
  wire [0:0]CO;
  wire [3:0]DI;
  wire [3:0]Q;
  wire [3:0]S;
  wire [9:2]column__0;
  wire \dc_bias[3]_i_11__0_n_0 ;
  wire \dc_bias[3]_i_12_n_0 ;
  wire \dc_bias[3]_i_14__1_n_0 ;
  wire \dc_bias[3]_i_15__0_n_0 ;
  wire \dc_bias[3]_i_16__0_n_0 ;
  wire \dc_bias[3]_i_16__1_n_0 ;
  wire \dc_bias[3]_i_17__0_n_0 ;
  wire \dc_bias[3]_i_19_n_0 ;
  wire \dc_bias[3]_i_23__0_n_0 ;
  wire \dc_bias[3]_i_23_n_0 ;
  wire \dc_bias[3]_i_24__0_n_0 ;
  wire \dc_bias[3]_i_24_n_0 ;
  wire \dc_bias[3]_i_25__0_n_0 ;
  wire \dc_bias[3]_i_25_n_0 ;
  wire \dc_bias[3]_i_26__0_n_0 ;
  wire \dc_bias[3]_i_27_n_0 ;
  wire \dc_bias[3]_i_33_n_0 ;
  wire \dc_bias[3]_i_34_n_0 ;
  wire \dc_bias[3]_i_35_n_0 ;
  wire \dc_bias[3]_i_36_n_0 ;
  wire \dc_bias[3]_i_37_n_0 ;
  wire \dc_bias[3]_i_38_n_0 ;
  wire \dc_bias[3]_i_39_n_0 ;
  wire \dc_bias[3]_i_3__0_n_0 ;
  wire \dc_bias[3]_i_40_n_0 ;
  wire \dc_bias[3]_i_41_n_0 ;
  wire \dc_bias[3]_i_42_n_0 ;
  wire \dc_bias[3]_i_50_n_0 ;
  wire \dc_bias[3]_i_51_n_0 ;
  wire \dc_bias[3]_i_56_n_0 ;
  wire \dc_bias[3]_i_57_n_0 ;
  wire \dc_bias[3]_i_58_n_0 ;
  wire \dc_bias[3]_i_5__1_n_0 ;
  wire \dc_bias[3]_i_6_n_0 ;
  wire \dc_bias[3]_i_7_n_0 ;
  wire \dc_bias[3]_i_8__1_n_0 ;
  wire \dc_bias[3]_i_9__0_n_0 ;
  wire \dc_bias_reg[1] ;
  wire \dc_bias_reg[2] ;
  wire \dc_bias_reg[3] ;
  wire [0:0]\dc_bias_reg[3]_0 ;
  wire [0:0]\dc_bias_reg[3]_1 ;
  wire \encoded[8]_i_4__0_n_0 ;
  wire \encoded[9]_i_2__0_n_0 ;
  wire [3:0]\encoded_reg[0] ;
  wire [3:0]\encoded_reg[0]_0 ;
  wire [3:0]\encoded_reg[0]_1 ;
  wire \encoded_reg[0]_10 ;
  wire \encoded_reg[0]_11 ;
  wire [0:0]\encoded_reg[0]_2 ;
  wire [0:0]\encoded_reg[0]_3 ;
  wire [3:0]\encoded_reg[0]_4 ;
  wire [3:0]\encoded_reg[0]_5 ;
  wire [0:0]\encoded_reg[0]_6 ;
  wire [0:0]\encoded_reg[0]_7 ;
  wire \encoded_reg[0]_8 ;
  wire \encoded_reg[0]_9 ;
  wire \encoded_reg[1] ;
  wire \encoded_reg[2] ;
  wire \encoded_reg[4] ;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire [0:0]\encoded_reg[8]_10 ;
  wire [3:0]\encoded_reg[8]_11 ;
  wire [3:0]\encoded_reg[8]_12 ;
  wire [0:0]\encoded_reg[8]_13 ;
  wire [0:0]\encoded_reg[8]_14 ;
  wire [3:0]\encoded_reg[8]_15 ;
  wire [0:0]\encoded_reg[8]_16 ;
  wire [0:0]\encoded_reg[8]_17 ;
  wire \encoded_reg[8]_18 ;
  wire \encoded_reg[8]_19 ;
  wire \encoded_reg[8]_2 ;
  wire \encoded_reg[8]_20 ;
  wire \encoded_reg[8]_21 ;
  wire \encoded_reg[8]_22 ;
  wire \encoded_reg[8]_23 ;
  wire \encoded_reg[8]_24 ;
  wire \encoded_reg[8]_25 ;
  wire \encoded_reg[8]_26 ;
  wire \encoded_reg[8]_3 ;
  wire [3:0]\encoded_reg[8]_4 ;
  wire [0:0]\encoded_reg[8]_5 ;
  wire [0:0]\encoded_reg[8]_6 ;
  wire [3:0]\encoded_reg[8]_7 ;
  wire [3:0]\encoded_reg[8]_8 ;
  wire [0:0]\encoded_reg[8]_9 ;
  wire \encoded_reg[9] ;
  wire \encoded_reg[9]_0 ;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8_n_0;
  wire \int_trigger_time_s_reg[1] ;
  wire \int_trigger_time_s_reg[1]_0 ;
  wire \int_trigger_time_s_reg[1]_1 ;
  wire \int_trigger_time_s_reg[1]_2 ;
  wire \int_trigger_time_s_reg[1]_3 ;
  wire \int_trigger_time_s_reg[2] ;
  wire \int_trigger_time_s_reg[3] ;
  wire \int_trigger_time_s_reg[5] ;
  wire \int_trigger_time_s_reg[7] ;
  wire [9:1]plusOp__1;
  wire processQ0;
  wire \processQ[0]_i_1__1_n_0 ;
  wire \processQ[2]_i_1__1_n_0 ;
  wire \processQ[6]_i_2__0_n_0 ;
  wire \processQ[9]_i_1__1_n_0 ;
  wire \processQ[9]_i_5__0_n_0 ;
  wire \processQ[9]_i_6__0_n_0 ;
  wire \processQ_reg[0]_0 ;
  wire \processQ_reg[0]_1 ;
  wire \processQ_reg[0]_2 ;
  wire \processQ_reg[2]_0 ;
  wire \processQ_reg[2]_1 ;
  wire \processQ_reg[4]_0 ;
  wire \processQ_reg[4]_1 ;
  wire \processQ_reg[5]_0 ;
  wire \processQ_reg[8]_0 ;
  wire \processQ_reg[8]_1 ;
  wire \processQ_reg[8]_2 ;
  wire \processQ_reg[8]_3 ;
  wire \processQ_reg[9]_0 ;
  wire \processQ_reg[9]_1 ;
  wire [0:0]\processQ_reg[9]_10 ;
  wire \processQ_reg[9]_2 ;
  wire [0:0]\processQ_reg[9]_3 ;
  wire [0:0]\processQ_reg[9]_4 ;
  wire [0:0]\processQ_reg[9]_5 ;
  wire [2:0]\processQ_reg[9]_6 ;
  wire [0:0]\processQ_reg[9]_7 ;
  wire [0:0]\processQ_reg[9]_8 ;
  wire [0:0]\processQ_reg[9]_9 ;
  wire reset_n;
  wire [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_i_30_n_0 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_i_31_n_0 ;
  wire \slv_reg10_reg[0] ;
  wire \slv_reg10_reg[0]_0 ;
  wire \slv_reg10_reg[1] ;
  wire \slv_reg10_reg[2] ;
  wire \slv_reg10_reg[2]_0 ;
  wire \slv_reg10_reg[3] ;
  wire \slv_reg10_reg[4] ;
  wire \slv_reg10_reg[5] ;
  wire \slv_reg10_reg[5]_0 ;
  wire \slv_reg10_reg[5]_1 ;
  wire \slv_reg10_reg[5]_2 ;
  wire \slv_reg10_reg[6] ;
  wire \slv_reg10_reg[6]_0 ;
  wire \slv_reg10_reg[6]_1 ;
  wire \slv_reg10_reg[6]_2 ;
  wire \slv_reg10_reg[6]_3 ;
  wire \slv_reg10_reg[6]_4 ;
  wire \slv_reg10_reg[7] ;
  wire \slv_reg10_reg[7]_0 ;
  wire \slv_reg10_reg[7]_1 ;
  wire \slv_reg10_reg[8] ;
  wire [3:0]\slv_reg10_reg[9] ;
  wire \slv_reg10_reg[9]_0 ;
  wire [0:0]\slv_reg10_reg[9]_1 ;
  wire [0:0]\slv_reg5_reg[0] ;
  wire [1:0]switch;

  LUT6 #(
    .INIT(64'h0003FFFF6665FFFF)) 
    \TDMS_encoder_red/encoded[9]_i_1 
       (.I0(\dc_bias_reg[3]_1 ),
        .I1(\encoded_reg[0]_8 ),
        .I2(\encoded[9]_i_2__0_n_0 ),
        .I3(\encoded_reg[8]_3 ),
        .I4(\processQ_reg[5]_0 ),
        .I5(\dc_bias_reg[2] ),
        .O(\encoded_reg[9]_0 ));
  LUT6 #(
    .INIT(64'h00000000EFFFEEEE)) 
    \dc_bias[1]_i_2 
       (.I0(\processQ_reg[4]_0 ),
        .I1(\encoded_reg[8]_18 ),
        .I2(CO),
        .I3(switch[0]),
        .I4(\processQ_reg[9]_0 ),
        .I5(\encoded_reg[8]_3 ),
        .O(\encoded_reg[8]_1 ));
  LUT6 #(
    .INIT(64'h00000000BAAAAAAA)) 
    \dc_bias[3]_i_10__0 
       (.I0(\processQ_reg[0]_1 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(\processQ_reg[9]_4 ),
        .I4(\processQ_reg[9]_3 ),
        .I5(\dc_bias[3]_i_16__1_n_0 ),
        .O(\encoded_reg[0]_10 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \dc_bias[3]_i_10__1 
       (.I0(\dc_bias[3]_i_23_n_0 ),
        .I1(\dc_bias[3]_i_25__0_n_0 ),
        .I2(\dc_bias[3]_i_24_n_0 ),
        .I3(\dc_bias[3]_i_19_n_0 ),
        .I4(column__0[9]),
        .I5(column__0[6]),
        .O(\encoded_reg[8]_21 ));
  LUT6 #(
    .INIT(64'hF80000000000001F)) 
    \dc_bias[3]_i_11 
       (.I0(column__0[2]),
        .I1(column__0[3]),
        .I2(column__0[4]),
        .I3(column__0[6]),
        .I4(column__0[9]),
        .I5(column__0[5]),
        .O(\encoded_reg[8]_22 ));
  LUT6 #(
    .INIT(64'hA2AAAAAAAAAAAA2A)) 
    \dc_bias[3]_i_11__0 
       (.I0(\dc_bias[3]_i_23__0_n_0 ),
        .I1(column__0[7]),
        .I2(column__0[6]),
        .I3(column__0[8]),
        .I4(column__0[5]),
        .I5(column__0[4]),
        .O(\dc_bias[3]_i_11__0_n_0 ));
  LUT6 #(
    .INIT(64'hF4F4F0F0FFF0F0F0)) 
    \dc_bias[3]_i_12 
       (.I0(\dc_bias[3]_i_24__0_n_0 ),
        .I1(\dc_bias[3]_i_25_n_0 ),
        .I2(\dc_bias[3]_i_26__0_n_0 ),
        .I3(\dc_bias[3]_i_27_n_0 ),
        .I4(column__0[2]),
        .I5(column__0[3]),
        .O(\dc_bias[3]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hEFEAEAEAAAAAAAAA)) 
    \dc_bias[3]_i_12__1 
       (.I0(\processQ_reg[0]_2 ),
        .I1(\slv_reg10_reg[9]_1 ),
        .I2(\processQ_reg[9]_6 [1]),
        .I3(\processQ_reg[9]_7 ),
        .I4(\processQ_reg[9]_8 ),
        .I5(\processQ_reg[9]_6 [0]),
        .O(\encoded_reg[0]_11 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFBFF)) 
    \dc_bias[3]_i_14__0 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl_i_30_n_0 ),
        .I1(column__0[4]),
        .I2(column__0[8]),
        .I3(\dc_bias[3]_i_23_n_0 ),
        .I4(column__0[9]),
        .I5(\dc_bias[3]_i_25__0_n_0 ),
        .O(\encoded_reg[8]_23 ));
  LUT6 #(
    .INIT(64'hFFFFFBFFFFFFFFFF)) 
    \dc_bias[3]_i_14__1 
       (.I0(column__0[3]),
        .I1(column__0[2]),
        .I2(column__0[9]),
        .I3(\dc_bias[3]_i_23_n_0 ),
        .I4(column__0[8]),
        .I5(column__0[4]),
        .O(\dc_bias[3]_i_14__1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAEAEAEFFAEAEAE)) 
    \dc_bias[3]_i_15__0 
       (.I0(\dc_bias[3]_i_33_n_0 ),
        .I1(\dc_bias[3]_i_34_n_0 ),
        .I2(\dc_bias[3]_i_35_n_0 ),
        .I3(\dc_bias[3]_i_36_n_0 ),
        .I4(\dc_bias[3]_i_37_n_0 ),
        .I5(column__0[5]),
        .O(\dc_bias[3]_i_15__0_n_0 ));
  LUT6 #(
    .INIT(64'h4440444044404444)) 
    \dc_bias[3]_i_16__0 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\dc_bias[3]_i_38_n_0 ),
        .I3(\dc_bias[3]_i_39_n_0 ),
        .I4(\dc_bias[3]_i_40_n_0 ),
        .I5(\dc_bias[3]_i_41_n_0 ),
        .O(\dc_bias[3]_i_16__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEFF)) 
    \dc_bias[3]_i_16__1 
       (.I0(column__0[6]),
        .I1(column__0[9]),
        .I2(column__0[5]),
        .I3(column__0[4]),
        .I4(\dc_bias[3]_i_24_n_0 ),
        .I5(\dc_bias[3]_i_25__0_n_0 ),
        .O(\dc_bias[3]_i_16__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'h40404044)) 
    \dc_bias[3]_i_17__0 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\dc_bias[3]_i_42_n_0 ),
        .I3(\dc_bias[3]_i_35_n_0 ),
        .I4(\dc_bias[3]_i_40_n_0 ),
        .O(\dc_bias[3]_i_17__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \dc_bias[3]_i_19 
       (.I0(column__0[5]),
        .I1(column__0[4]),
        .O(\dc_bias[3]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAEBBB)) 
    \dc_bias[3]_i_2 
       (.I0(\encoded_reg[8]_3 ),
        .I1(\processQ_reg[9]_0 ),
        .I2(switch[0]),
        .I3(CO),
        .I4(\encoded_reg[8]_18 ),
        .I5(\processQ_reg[4]_0 ),
        .O(\dc_bias_reg[3] ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \dc_bias[3]_i_23 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .O(\dc_bias[3]_i_23_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \dc_bias[3]_i_23__0 
       (.I0(column__0[3]),
        .I1(column__0[2]),
        .O(\dc_bias[3]_i_23__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \dc_bias[3]_i_24 
       (.I0(column__0[8]),
        .I1(column__0[7]),
        .O(\dc_bias[3]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'h00000000F8000000)) 
    \dc_bias[3]_i_24__0 
       (.I0(column__0[8]),
        .I1(column__0[4]),
        .I2(column__0[9]),
        .I3(column__0[5]),
        .I4(column__0[6]),
        .I5(column__0[7]),
        .O(\dc_bias[3]_i_24__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \dc_bias[3]_i_25 
       (.I0(column__0[7]),
        .I1(column__0[6]),
        .I2(column__0[8]),
        .I3(column__0[4]),
        .I4(column__0[5]),
        .O(\dc_bias[3]_i_25_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \dc_bias[3]_i_25__0 
       (.I0(column__0[3]),
        .I1(column__0[2]),
        .O(\dc_bias[3]_i_25__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000F5F5F4F5)) 
    \dc_bias[3]_i_26__0 
       (.I0(\dc_bias[3]_i_50_n_0 ),
        .I1(column__0[8]),
        .I2(column__0[7]),
        .I3(column__0[4]),
        .I4(\dc_bias[3]_i_51_n_0 ),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl_i_31_n_0 ),
        .O(\dc_bias[3]_i_26__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFD)) 
    \dc_bias[3]_i_27 
       (.I0(column__0[8]),
        .I1(column__0[7]),
        .I2(column__0[6]),
        .I3(column__0[4]),
        .I4(column__0[5]),
        .O(\dc_bias[3]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    \dc_bias[3]_i_2__1 
       (.I0(\processQ_reg[8]_0 ),
        .I1(\dc_bias[3]_i_3__0_n_0 ),
        .I2(\processQ_reg[9]_1 ),
        .I3(\dc_bias[3]_i_5__1_n_0 ),
        .I4(\dc_bias[3]_i_6_n_0 ),
        .I5(\dc_bias[3]_i_7_n_0 ),
        .O(\encoded_reg[8]_3 ));
  LUT6 #(
    .INIT(64'h0004FFFF00040004)) 
    \dc_bias[3]_i_30 
       (.I0(column__0[4]),
        .I1(column__0[5]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I4(column__0[9]),
        .I5(\dc_bias[3]_i_24_n_0 ),
        .O(\encoded_reg[8]_24 ));
  LUT6 #(
    .INIT(64'h0000000800140000)) 
    \dc_bias[3]_i_33 
       (.I0(column__0[5]),
        .I1(column__0[4]),
        .I2(column__0[8]),
        .I3(\dc_bias[3]_i_56_n_0 ),
        .I4(column__0[6]),
        .I5(column__0[9]),
        .O(\dc_bias[3]_i_33_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'h0412)) 
    \dc_bias[3]_i_34 
       (.I0(column__0[5]),
        .I1(column__0[4]),
        .I2(column__0[9]),
        .I3(column__0[8]),
        .O(\dc_bias[3]_i_34_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \dc_bias[3]_i_35 
       (.I0(column__0[2]),
        .I1(column__0[3]),
        .I2(column__0[7]),
        .I3(column__0[6]),
        .O(\dc_bias[3]_i_35_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[3]_i_36 
       (.I0(column__0[8]),
        .I1(column__0[4]),
        .O(\dc_bias[3]_i_36_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'h4200)) 
    \dc_bias[3]_i_37 
       (.I0(column__0[3]),
        .I1(column__0[2]),
        .I2(column__0[6]),
        .I3(column__0[7]),
        .O(\dc_bias[3]_i_37_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000018)) 
    \dc_bias[3]_i_38 
       (.I0(column__0[5]),
        .I1(column__0[9]),
        .I2(column__0[6]),
        .I3(\dc_bias[3]_i_24_n_0 ),
        .I4(column__0[4]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl_i_31_n_0 ),
        .O(\dc_bias[3]_i_38_n_0 ));
  LUT6 #(
    .INIT(64'h0A00A0003A00A000)) 
    \dc_bias[3]_i_39 
       (.I0(\dc_bias[3]_i_37_n_0 ),
        .I1(\dc_bias[3]_i_35_n_0 ),
        .I2(column__0[4]),
        .I3(column__0[5]),
        .I4(column__0[8]),
        .I5(column__0[9]),
        .O(\dc_bias[3]_i_39_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF55554000)) 
    \dc_bias[3]_i_3__0 
       (.I0(\dc_bias[3]_i_16__1_n_0 ),
        .I1(\processQ_reg[9]_3 ),
        .I2(\processQ_reg[9]_4 ),
        .I3(\dc_bias[3]_i_8__1_n_0 ),
        .I4(\processQ_reg[0]_1 ),
        .I5(\dc_bias[3]_i_9__0_n_0 ),
        .O(\dc_bias[3]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h0004000000040004)) 
    \dc_bias[3]_i_4 
       (.I0(\processQ_reg[9]_0 ),
        .I1(\processQ_reg[8]_0 ),
        .I2(\dc_bias[3]_i_9__0_n_0 ),
        .I3(\encoded_reg[0]_10 ),
        .I4(\processQ_reg[4]_1 ),
        .I5(\encoded_reg[0]_11 ),
        .O(\encoded_reg[0]_8 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'hFBD7)) 
    \dc_bias[3]_i_40 
       (.I0(column__0[4]),
        .I1(column__0[5]),
        .I2(column__0[9]),
        .I3(column__0[8]),
        .O(\dc_bias[3]_i_40_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'hF7FF)) 
    \dc_bias[3]_i_41 
       (.I0(column__0[3]),
        .I1(column__0[2]),
        .I2(column__0[7]),
        .I3(column__0[6]),
        .O(\dc_bias[3]_i_41_n_0 ));
  LUT6 #(
    .INIT(64'h0A000B0000A300B0)) 
    \dc_bias[3]_i_42 
       (.I0(\dc_bias[3]_i_37_n_0 ),
        .I1(\dc_bias[3]_i_41_n_0 ),
        .I2(column__0[5]),
        .I3(column__0[4]),
        .I4(column__0[9]),
        .I5(column__0[8]),
        .O(\dc_bias[3]_i_42_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFDFFB)) 
    \dc_bias[3]_i_45 
       (.I0(column__0[5]),
        .I1(column__0[6]),
        .I2(column__0[3]),
        .I3(column__0[4]),
        .I4(\dc_bias[3]_i_57_n_0 ),
        .I5(\dc_bias[3]_i_58_n_0 ),
        .O(\encoded_reg[8]_20 ));
  LUT6 #(
    .INIT(64'hFFFFABAAFFFFFFFF)) 
    \dc_bias[3]_i_4__0 
       (.I0(\encoded_reg[8]_21 ),
        .I1(column__0[8]),
        .I2(column__0[7]),
        .I3(\encoded_reg[8]_22 ),
        .I4(\processQ_reg[8]_2 ),
        .I5(\processQ_reg[8]_3 ),
        .O(\encoded_reg[8]_18 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \dc_bias[3]_i_5 
       (.I0(\encoded[8]_i_4__0_n_0 ),
        .I1(\encoded_reg[8]_3 ),
        .O(\encoded_reg[8]_19 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \dc_bias[3]_i_50 
       (.I0(column__0[6]),
        .I1(column__0[8]),
        .I2(column__0[4]),
        .I3(column__0[5]),
        .O(\dc_bias[3]_i_50_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[3]_i_51 
       (.I0(column__0[5]),
        .I1(column__0[9]),
        .O(\dc_bias[3]_i_51_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dc_bias[3]_i_55 
       (.I0(column__0[7]),
        .I1(column__0[9]),
        .O(\encoded_reg[8]_25 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \dc_bias[3]_i_56 
       (.I0(column__0[7]),
        .I1(column__0[2]),
        .I2(column__0[3]),
        .O(\dc_bias[3]_i_56_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \dc_bias[3]_i_57 
       (.I0(column__0[8]),
        .I1(column__0[9]),
        .I2(column__0[7]),
        .I3(\processQ_reg[9]_6 [2]),
        .I4(\processQ_reg[9]_6 [0]),
        .O(\dc_bias[3]_i_57_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'h76EE)) 
    \dc_bias[3]_i_58 
       (.I0(column__0[3]),
        .I1(column__0[2]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .O(\dc_bias[3]_i_58_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \dc_bias[3]_i_5__1 
       (.I0(\encoded_reg[8]_18 ),
        .I1(switch[1]),
        .I2(\processQ_reg[9]_5 ),
        .O(\dc_bias[3]_i_5__1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF55555554)) 
    \dc_bias[3]_i_6 
       (.I0(\processQ_reg[2]_0 ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(\dc_bias[3]_i_11__0_n_0 ),
        .I4(\dc_bias[3]_i_12_n_0 ),
        .I5(\processQ_reg[8]_1 ),
        .O(\dc_bias[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'hFFE0)) 
    \dc_bias[3]_i_6__0 
       (.I0(column__0[8]),
        .I1(column__0[7]),
        .I2(column__0[9]),
        .I3(\processQ_reg[9]_6 [2]),
        .O(\encoded_reg[9] ));
  LUT6 #(
    .INIT(64'hFFFFFFFF55555510)) 
    \dc_bias[3]_i_7 
       (.I0(\processQ_reg[9]_2 ),
        .I1(\processQ[6]_i_2__0_n_0 ),
        .I2(\dc_bias[3]_i_15__0_n_0 ),
        .I3(\dc_bias[3]_i_16__0_n_0 ),
        .I4(\dc_bias[3]_i_17__0_n_0 ),
        .I5(\processQ_reg[2]_1 ),
        .O(\dc_bias[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \dc_bias[3]_i_8__1 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .O(\dc_bias[3]_i_8__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \dc_bias[3]_i_9__0 
       (.I0(\processQ_reg[9]_9 ),
        .I1(\processQ_reg[9]_10 ),
        .I2(\dc_bias[3]_i_14__1_n_0 ),
        .I3(column__0[5]),
        .I4(column__0[7]),
        .I5(column__0[6]),
        .O(\dc_bias[3]_i_9__0_n_0 ));
  LUT5 #(
    .INIT(32'h557FFF7F)) 
    \dc_bias[3]_i_9__1 
       (.I0(\processQ_reg[9]_6 [0]),
        .I1(\processQ_reg[9]_8 ),
        .I2(\processQ_reg[9]_7 ),
        .I3(\processQ_reg[9]_6 [1]),
        .I4(\slv_reg10_reg[9]_1 ),
        .O(\encoded_reg[8]_26 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h660F)) 
    \encoded[0]_i_1 
       (.I0(\dc_bias_reg[3]_0 ),
        .I1(\encoded_reg[0]_8 ),
        .I2(\encoded_reg[8]_0 ),
        .I3(\processQ_reg[5]_0 ),
        .O(\encoded_reg[0]_9 ));
  LUT3 #(
    .INIT(8'hA3)) 
    \encoded[1]_i_1 
       (.I0(\dc_bias_reg[3]_0 ),
        .I1(\encoded_reg[8]_0 ),
        .I2(\processQ_reg[5]_0 ),
        .O(\encoded_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \encoded[2]_i_1 
       (.I0(\encoded_reg[8]_0 ),
        .I1(\dc_bias_reg[3]_0 ),
        .I2(\processQ_reg[5]_0 ),
        .O(\encoded_reg[2] ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'hAEEA)) 
    \encoded[4]_i_1 
       (.I0(\encoded_reg[8]_0 ),
        .I1(\processQ_reg[5]_0 ),
        .I2(\dc_bias_reg[3]_0 ),
        .I3(\encoded_reg[0]_8 ),
        .O(\encoded_reg[4] ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h2AAE)) 
    \encoded[8]_i_2__0 
       (.I0(\encoded_reg[8]_0 ),
        .I1(\processQ_reg[5]_0 ),
        .I2(\dc_bias_reg[1] ),
        .I3(\encoded_reg[8]_1 ),
        .O(\encoded_reg[8] ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'h3B3F)) 
    \encoded[8]_i_2__1 
       (.I0(\encoded_reg[8]_3 ),
        .I1(\processQ_reg[5]_0 ),
        .I2(\dc_bias_reg[2] ),
        .I3(\encoded[8]_i_4__0_n_0 ),
        .O(\encoded_reg[8]_2 ));
  LUT6 #(
    .INIT(64'h000000007E000000)) 
    \encoded[8]_i_3 
       (.I0(column__0[6]),
        .I1(column__0[4]),
        .I2(column__0[5]),
        .I3(column__0[7]),
        .I4(column__0[9]),
        .I5(column__0[8]),
        .O(\encoded_reg[8]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \encoded[8]_i_4__0 
       (.I0(\processQ_reg[9]_1 ),
        .I1(\dc_bias[3]_i_3__0_n_0 ),
        .I2(\encoded_reg[8]_18 ),
        .I3(\processQ_reg[9]_0 ),
        .O(\encoded[8]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000001000101)) 
    \encoded[9]_i_2__0 
       (.I0(\processQ_reg[8]_0 ),
        .I1(\dc_bias[3]_i_9__0_n_0 ),
        .I2(\encoded_reg[0]_10 ),
        .I3(\processQ_reg[4]_1 ),
        .I4(\encoded_reg[0]_11 ),
        .I5(\dc_bias[3]_i_5__1_n_0 ),
        .O(\encoded[9]_i_2__0_n_0 ));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i__carry__0_i_1
       (.I0(column__0[9]),
        .I1(\slv_reg10_reg[8] ),
        .I2(\slv_reg10_reg[7]_0 ),
        .I3(\slv_reg10_reg[9]_0 ),
        .I4(column__0[8]),
        .O(\encoded_reg[8]_6 ));
  LUT6 #(
    .INIT(64'hC222ABBB80002AAA)) 
    i__carry__0_i_1__0
       (.I0(column__0[9]),
        .I1(\slv_reg10_reg[8] ),
        .I2(\slv_reg10_reg[6]_0 ),
        .I3(\slv_reg10_reg[7] ),
        .I4(\slv_reg10_reg[9]_0 ),
        .I5(column__0[8]),
        .O(\encoded_reg[8]_10 ));
  LUT6 #(
    .INIT(64'h155540003DDD5444)) 
    i__carry__0_i_1__1
       (.I0(column__0[9]),
        .I1(\slv_reg10_reg[8] ),
        .I2(\slv_reg10_reg[6]_2 ),
        .I3(\slv_reg10_reg[7] ),
        .I4(\slv_reg10_reg[9]_0 ),
        .I5(column__0[8]),
        .O(\encoded_reg[8]_14 ));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry__0_i_1__2
       (.I0(column__0[9]),
        .I1(\slv_reg10_reg[8] ),
        .I2(\int_trigger_time_s_reg[7] ),
        .I3(\slv_reg10_reg[9]_0 ),
        .I4(column__0[8]),
        .O(\encoded_reg[8]_17 ));
  LUT6 #(
    .INIT(64'h155540003DDD5444)) 
    i__carry__0_i_1__3
       (.I0(column__0[9]),
        .I1(\slv_reg10_reg[8] ),
        .I2(\slv_reg10_reg[6]_3 ),
        .I3(\slv_reg10_reg[7] ),
        .I4(\slv_reg10_reg[9]_0 ),
        .I5(column__0[8]),
        .O(\encoded_reg[0]_3 ));
  LUT6 #(
    .INIT(64'hC222ABBB80002AAA)) 
    i__carry__0_i_1__4
       (.I0(column__0[9]),
        .I1(\slv_reg10_reg[8] ),
        .I2(\slv_reg10_reg[6]_4 ),
        .I3(\slv_reg10_reg[7] ),
        .I4(\slv_reg10_reg[9]_0 ),
        .I5(column__0[8]),
        .O(\encoded_reg[0]_7 ));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry__0_i_2
       (.I0(\slv_reg10_reg[9]_0 ),
        .I1(column__0[9]),
        .I2(\slv_reg10_reg[8] ),
        .I3(\slv_reg10_reg[7]_0 ),
        .I4(column__0[8]),
        .O(\encoded_reg[8]_5 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__0
       (.I0(\slv_reg10_reg[9]_0 ),
        .I1(column__0[9]),
        .I2(\slv_reg10_reg[8] ),
        .I3(\slv_reg10_reg[6]_0 ),
        .I4(\slv_reg10_reg[7] ),
        .I5(column__0[8]),
        .O(\encoded_reg[8]_9 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__1
       (.I0(\slv_reg10_reg[9]_0 ),
        .I1(column__0[9]),
        .I2(\slv_reg10_reg[8] ),
        .I3(\slv_reg10_reg[6]_2 ),
        .I4(\slv_reg10_reg[7] ),
        .I5(column__0[8]),
        .O(\encoded_reg[8]_13 ));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry__0_i_2__2
       (.I0(\slv_reg10_reg[9]_0 ),
        .I1(column__0[9]),
        .I2(\slv_reg10_reg[8] ),
        .I3(\int_trigger_time_s_reg[7] ),
        .I4(column__0[8]),
        .O(\encoded_reg[8]_16 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__3
       (.I0(\slv_reg10_reg[9]_0 ),
        .I1(column__0[9]),
        .I2(\slv_reg10_reg[8] ),
        .I3(\slv_reg10_reg[6]_3 ),
        .I4(\slv_reg10_reg[7] ),
        .I5(column__0[8]),
        .O(\encoded_reg[0]_2 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__4
       (.I0(\slv_reg10_reg[9]_0 ),
        .I1(column__0[9]),
        .I2(\slv_reg10_reg[8] ),
        .I3(\slv_reg10_reg[6]_4 ),
        .I4(\slv_reg10_reg[7] ),
        .I5(column__0[8]),
        .O(\encoded_reg[0]_6 ));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i__carry_i_1__0
       (.I0(column__0[7]),
        .I1(\slv_reg10_reg[6] ),
        .I2(\int_trigger_time_s_reg[5] ),
        .I3(\slv_reg10_reg[7] ),
        .I4(column__0[6]),
        .O(\encoded_reg[8]_4 [3]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_1__1
       (.I0(column__0[7]),
        .I1(\slv_reg10_reg[6] ),
        .I2(\slv_reg10_reg[5] ),
        .I3(\slv_reg10_reg[7] ),
        .I4(column__0[6]),
        .O(\encoded_reg[8]_8 [3]));
  LUT6 #(
    .INIT(64'h7877788887888777)) 
    i__carry_i_1__11
       (.I0(\slv_reg10_reg[8] ),
        .I1(\slv_reg10_reg[7]_1 ),
        .I2(\slv_reg10_reg[9] [3]),
        .I3(\slv_reg5_reg[0] ),
        .I4(Q[3]),
        .I5(column__0[9]),
        .O(\encoded_reg[0] [3]));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i__carry_i_1__2
       (.I0(column__0[7]),
        .I1(\slv_reg10_reg[6] ),
        .I2(\slv_reg10_reg[5]_1 ),
        .I3(\slv_reg10_reg[7] ),
        .I4(column__0[6]),
        .O(\encoded_reg[8]_12 [3]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_1__3
       (.I0(column__0[7]),
        .I1(\slv_reg10_reg[6] ),
        .I2(\slv_reg10_reg[5]_2 ),
        .I3(\slv_reg10_reg[7] ),
        .I4(column__0[6]),
        .O(DI[3]));
  LUT6 #(
    .INIT(64'h51550400D3DD4544)) 
    i__carry_i_1__4
       (.I0(column__0[7]),
        .I1(\slv_reg10_reg[6] ),
        .I2(\int_trigger_time_s_reg[1] ),
        .I3(\slv_reg10_reg[5]_0 ),
        .I4(\slv_reg10_reg[7] ),
        .I5(column__0[6]),
        .O(\encoded_reg[0]_1 [3]));
  LUT6 #(
    .INIT(64'h2C22BABB0800A2AA)) 
    i__carry_i_1__5
       (.I0(column__0[7]),
        .I1(\slv_reg10_reg[6] ),
        .I2(\int_trigger_time_s_reg[1]_0 ),
        .I3(\slv_reg10_reg[5]_0 ),
        .I4(\slv_reg10_reg[7] ),
        .I5(column__0[6]),
        .O(\encoded_reg[0]_5 [3]));
  LUT6 #(
    .INIT(64'h0000000006909009)) 
    i__carry_i_2
       (.I0(\slv_reg10_reg[8] ),
        .I1(column__0[8]),
        .I2(column__0[7]),
        .I3(\slv_reg10_reg[6]_1 ),
        .I4(\slv_reg10_reg[7] ),
        .I5(i__carry_i_7_n_0),
        .O(\encoded_reg[0] [2]));
  LUT5 #(
    .INIT(32'h015443D5)) 
    i__carry_i_2__0
       (.I0(column__0[5]),
        .I1(\slv_reg10_reg[4] ),
        .I2(\slv_reg10_reg[0] ),
        .I3(\slv_reg10_reg[5]_0 ),
        .I4(column__0[4]),
        .O(\encoded_reg[8]_4 [2]));
  LUT6 #(
    .INIT(64'hBCCC2AAAA8880222)) 
    i__carry_i_2__1
       (.I0(column__0[5]),
        .I1(\slv_reg10_reg[4] ),
        .I2(\int_trigger_time_s_reg[1]_1 ),
        .I3(\int_trigger_time_s_reg[2] ),
        .I4(\slv_reg10_reg[5]_0 ),
        .I5(column__0[4]),
        .O(\encoded_reg[8]_8 [2]));
  LUT5 #(
    .INIT(32'h015443D5)) 
    i__carry_i_2__2
       (.I0(column__0[5]),
        .I1(\slv_reg10_reg[4] ),
        .I2(\int_trigger_time_s_reg[3] ),
        .I3(\slv_reg10_reg[5]_0 ),
        .I4(column__0[4]),
        .O(\encoded_reg[8]_12 [2]));
  LUT6 #(
    .INIT(64'hCBCCA2AA8A882022)) 
    i__carry_i_2__3
       (.I0(column__0[5]),
        .I1(\slv_reg10_reg[4] ),
        .I2(\int_trigger_time_s_reg[1]_2 ),
        .I3(\int_trigger_time_s_reg[2] ),
        .I4(\slv_reg10_reg[5]_0 ),
        .I5(column__0[4]),
        .O(DI[2]));
  LUT6 #(
    .INIT(64'h1011454434335D55)) 
    i__carry_i_2__4
       (.I0(column__0[5]),
        .I1(\slv_reg10_reg[4] ),
        .I2(\int_trigger_time_s_reg[1]_3 ),
        .I3(\slv_reg10_reg[3] ),
        .I4(\slv_reg10_reg[5]_0 ),
        .I5(column__0[4]),
        .O(\encoded_reg[0]_1 [2]));
  LUT5 #(
    .INIT(32'hBC2AA802)) 
    i__carry_i_2__5
       (.I0(column__0[5]),
        .I1(\slv_reg10_reg[4] ),
        .I2(\slv_reg10_reg[2]_0 ),
        .I3(\slv_reg10_reg[5]_0 ),
        .I4(column__0[4]),
        .O(\encoded_reg[0]_5 [2]));
  LUT6 #(
    .INIT(64'h0000000060060690)) 
    i__carry_i_3
       (.I0(\slv_reg10_reg[5]_0 ),
        .I1(column__0[5]),
        .I2(column__0[4]),
        .I3(\int_trigger_time_s_reg[2] ),
        .I4(\slv_reg10_reg[4] ),
        .I5(i__carry_i_8_n_0),
        .O(\encoded_reg[0] [1]));
  LUT6 #(
    .INIT(64'h111111147771111D)) 
    i__carry_i_3__0
       (.I0(column__0[3]),
        .I1(\slv_reg10_reg[3] ),
        .I2(\slv_reg10_reg[0]_0 ),
        .I3(\slv_reg10_reg[1] ),
        .I4(\slv_reg10_reg[2] ),
        .I5(column__0[2]),
        .O(\encoded_reg[8]_4 [1]));
  LUT6 #(
    .INIT(64'hC222ABBB80002AAA)) 
    i__carry_i_3__1
       (.I0(column__0[3]),
        .I1(\slv_reg10_reg[2] ),
        .I2(\slv_reg10_reg[0]_0 ),
        .I3(\slv_reg10_reg[1] ),
        .I4(\slv_reg10_reg[3] ),
        .I5(column__0[2]),
        .O(\encoded_reg[8]_8 [1]));
  LUT6 #(
    .INIT(64'h011154444333D555)) 
    i__carry_i_3__2
       (.I0(column__0[3]),
        .I1(\slv_reg10_reg[2] ),
        .I2(\slv_reg10_reg[0]_0 ),
        .I3(\slv_reg10_reg[1] ),
        .I4(\slv_reg10_reg[3] ),
        .I5(column__0[2]),
        .O(\encoded_reg[8]_12 [1]));
  LUT6 #(
    .INIT(64'hCCC2AAAB8880222A)) 
    i__carry_i_3__3
       (.I0(column__0[3]),
        .I1(\slv_reg10_reg[2] ),
        .I2(\slv_reg10_reg[0]_0 ),
        .I3(\slv_reg10_reg[1] ),
        .I4(\slv_reg10_reg[3] ),
        .I5(column__0[2]),
        .O(DI[1]));
  LUT5 #(
    .INIT(32'h015443D5)) 
    i__carry_i_3__4
       (.I0(column__0[3]),
        .I1(\slv_reg10_reg[1] ),
        .I2(\slv_reg10_reg[2] ),
        .I3(\slv_reg10_reg[3] ),
        .I4(column__0[2]),
        .O(\encoded_reg[0]_1 [1]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_3__5
       (.I0(column__0[3]),
        .I1(\slv_reg10_reg[1] ),
        .I2(\slv_reg10_reg[2] ),
        .I3(\slv_reg10_reg[3] ),
        .I4(column__0[2]),
        .O(\encoded_reg[0]_5 [1]));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    i__carry_i_4
       (.I0(\slv_reg10_reg[2] ),
        .I1(column__0[2]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I3(\slv_reg10_reg[0]_0 ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I5(\slv_reg10_reg[1] ),
        .O(\encoded_reg[0] [0]));
  LUT6 #(
    .INIT(64'h5017505050171717)) 
    i__carry_i_4__0
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\slv_reg10_reg[1] ),
        .I3(\slv_reg10_reg[9] [0]),
        .I4(\slv_reg5_reg[0] ),
        .I5(Q[0]),
        .O(\encoded_reg[8]_4 [0]));
  LUT6 #(
    .INIT(64'hFFFF1D001D000000)) 
    i__carry_i_4__1
       (.I0(Q[0]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\slv_reg10_reg[9] [0]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I4(\slv_reg10_reg[1] ),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .O(\encoded_reg[0]_5 [0]));
  LUT6 #(
    .INIT(64'hFFE200E21DE20000)) 
    i__carry_i_4__10
       (.I0(Q[0]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\slv_reg10_reg[9] [0]),
        .I3(\slv_reg10_reg[1] ),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .O(DI[0]));
  LUT6 #(
    .INIT(64'h000056A602A257F7)) 
    i__carry_i_4__2
       (.I0(\slv_reg10_reg[1] ),
        .I1(Q[0]),
        .I2(\slv_reg5_reg[0] ),
        .I3(\slv_reg10_reg[9] [0]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .O(\encoded_reg[8]_12 [0]));
  LUT6 #(
    .INIT(64'hE0EEE0008A888AAA)) 
    i__carry_i_4__3
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\slv_reg10_reg[9] [0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(Q[0]),
        .I5(\slv_reg10_reg[1] ),
        .O(\encoded_reg[8]_8 [0]));
  LUT6 #(
    .INIT(64'h000000E200E2FFFF)) 
    i__carry_i_4__4
       (.I0(Q[0]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\slv_reg10_reg[9] [0]),
        .I3(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I5(\slv_reg10_reg[1] ),
        .O(\encoded_reg[0]_1 [0]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5
       (.I0(\slv_reg10_reg[7] ),
        .I1(column__0[7]),
        .I2(\slv_reg10_reg[6] ),
        .I3(\int_trigger_time_s_reg[5] ),
        .I4(column__0[6]),
        .O(S[3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__0
       (.I0(\slv_reg10_reg[7] ),
        .I1(column__0[7]),
        .I2(\slv_reg10_reg[6] ),
        .I3(\slv_reg10_reg[5] ),
        .I4(column__0[6]),
        .O(\encoded_reg[8]_7 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__2
       (.I0(\slv_reg10_reg[7] ),
        .I1(column__0[7]),
        .I2(\slv_reg10_reg[6] ),
        .I3(\slv_reg10_reg[5]_1 ),
        .I4(column__0[6]),
        .O(\encoded_reg[8]_11 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__3
       (.I0(\slv_reg10_reg[7] ),
        .I1(column__0[7]),
        .I2(\slv_reg10_reg[6] ),
        .I3(\slv_reg10_reg[5]_2 ),
        .I4(column__0[6]),
        .O(\encoded_reg[8]_15 [3]));
  LUT6 #(
    .INIT(64'h9009909009600909)) 
    i__carry_i_5__4
       (.I0(\slv_reg10_reg[7] ),
        .I1(column__0[7]),
        .I2(\slv_reg10_reg[6] ),
        .I3(\int_trigger_time_s_reg[1] ),
        .I4(\slv_reg10_reg[5]_0 ),
        .I5(column__0[6]),
        .O(\encoded_reg[0]_0 [3]));
  LUT6 #(
    .INIT(64'h9009909009600909)) 
    i__carry_i_5__5
       (.I0(\slv_reg10_reg[7] ),
        .I1(column__0[7]),
        .I2(\slv_reg10_reg[6] ),
        .I3(\int_trigger_time_s_reg[1]_0 ),
        .I4(\slv_reg10_reg[5]_0 ),
        .I5(column__0[6]),
        .O(\encoded_reg[0]_4 [3]));
  LUT5 #(
    .INIT(32'h60090660)) 
    i__carry_i_6__0
       (.I0(\slv_reg10_reg[5]_0 ),
        .I1(column__0[5]),
        .I2(\slv_reg10_reg[4] ),
        .I3(\slv_reg10_reg[0] ),
        .I4(column__0[4]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h6009090906606060)) 
    i__carry_i_6__1
       (.I0(\slv_reg10_reg[5]_0 ),
        .I1(column__0[5]),
        .I2(\slv_reg10_reg[4] ),
        .I3(\int_trigger_time_s_reg[1]_1 ),
        .I4(\int_trigger_time_s_reg[2] ),
        .I5(column__0[4]),
        .O(\encoded_reg[8]_7 [2]));
  LUT5 #(
    .INIT(32'h60090660)) 
    i__carry_i_6__2
       (.I0(\slv_reg10_reg[5]_0 ),
        .I1(column__0[5]),
        .I2(\slv_reg10_reg[4] ),
        .I3(\int_trigger_time_s_reg[3] ),
        .I4(column__0[4]),
        .O(\encoded_reg[8]_11 [2]));
  LUT6 #(
    .INIT(64'h0960090960066060)) 
    i__carry_i_6__3
       (.I0(\slv_reg10_reg[5]_0 ),
        .I1(column__0[5]),
        .I2(\slv_reg10_reg[4] ),
        .I3(\int_trigger_time_s_reg[1]_2 ),
        .I4(\int_trigger_time_s_reg[2] ),
        .I5(column__0[4]),
        .O(\encoded_reg[8]_15 [2]));
  LUT6 #(
    .INIT(64'h0960090960066060)) 
    i__carry_i_6__4
       (.I0(\slv_reg10_reg[5]_0 ),
        .I1(column__0[5]),
        .I2(\slv_reg10_reg[4] ),
        .I3(\int_trigger_time_s_reg[1]_3 ),
        .I4(\slv_reg10_reg[3] ),
        .I5(column__0[4]),
        .O(\encoded_reg[0]_0 [2]));
  LUT5 #(
    .INIT(32'h60090660)) 
    i__carry_i_6__5
       (.I0(\slv_reg10_reg[5]_0 ),
        .I1(column__0[5]),
        .I2(\slv_reg10_reg[4] ),
        .I3(\slv_reg10_reg[2]_0 ),
        .I4(column__0[4]),
        .O(\encoded_reg[0]_4 [2]));
  LUT6 #(
    .INIT(64'h9999955566666AAA)) 
    i__carry_i_7
       (.I0(column__0[6]),
        .I1(\slv_reg10_reg[5]_0 ),
        .I2(\slv_reg10_reg[2] ),
        .I3(\slv_reg10_reg[3] ),
        .I4(\slv_reg10_reg[4] ),
        .I5(\slv_reg10_reg[6] ),
        .O(i__carry_i_7_n_0));
  LUT6 #(
    .INIT(64'h0002999499940002)) 
    i__carry_i_7__0
       (.I0(column__0[2]),
        .I1(\slv_reg10_reg[2] ),
        .I2(\slv_reg10_reg[1] ),
        .I3(\slv_reg10_reg[0]_0 ),
        .I4(\slv_reg10_reg[3] ),
        .I5(column__0[3]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h1888844442222111)) 
    i__carry_i_7__1
       (.I0(column__0[2]),
        .I1(\slv_reg10_reg[3] ),
        .I2(\slv_reg10_reg[1] ),
        .I3(\slv_reg10_reg[0]_0 ),
        .I4(\slv_reg10_reg[2] ),
        .I5(column__0[3]),
        .O(\encoded_reg[8]_7 [1]));
  LUT6 #(
    .INIT(64'h6009090906606060)) 
    i__carry_i_7__2
       (.I0(\slv_reg10_reg[3] ),
        .I1(column__0[3]),
        .I2(\slv_reg10_reg[2] ),
        .I3(\slv_reg10_reg[0]_0 ),
        .I4(\slv_reg10_reg[1] ),
        .I5(column__0[2]),
        .O(\encoded_reg[8]_11 [1]));
  LUT6 #(
    .INIT(64'h0009999066600009)) 
    i__carry_i_7__3
       (.I0(\slv_reg10_reg[3] ),
        .I1(column__0[3]),
        .I2(\slv_reg10_reg[0]_0 ),
        .I3(\slv_reg10_reg[1] ),
        .I4(\slv_reg10_reg[2] ),
        .I5(column__0[2]),
        .O(\encoded_reg[8]_15 [1]));
  LUT5 #(
    .INIT(32'h21188442)) 
    i__carry_i_7__4
       (.I0(column__0[2]),
        .I1(\slv_reg10_reg[3] ),
        .I2(\slv_reg10_reg[2] ),
        .I3(\slv_reg10_reg[1] ),
        .I4(column__0[3]),
        .O(\encoded_reg[0]_0 [1]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_7__5
       (.I0(\slv_reg10_reg[3] ),
        .I1(column__0[3]),
        .I2(\slv_reg10_reg[1] ),
        .I3(\slv_reg10_reg[2] ),
        .I4(column__0[2]),
        .O(\encoded_reg[0]_4 [1]));
  LUT6 #(
    .INIT(64'h9A956A65959A656A)) 
    i__carry_i_8
       (.I0(column__0[3]),
        .I1(\slv_reg10_reg[9] [2]),
        .I2(\slv_reg5_reg[0] ),
        .I3(Q[2]),
        .I4(\slv_reg10_reg[9] [1]),
        .I5(Q[1]),
        .O(i__carry_i_8_n_0));
  LUT6 #(
    .INIT(64'h1482141414828282)) 
    i__carry_i_8__0
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\slv_reg10_reg[1] ),
        .I3(\slv_reg10_reg[9] [0]),
        .I4(\slv_reg5_reg[0] ),
        .I5(Q[0]),
        .O(\encoded_reg[8]_7 [0]));
  LUT6 #(
    .INIT(64'h4128414141282828)) 
    i__carry_i_8__1
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\slv_reg10_reg[1] ),
        .I3(\slv_reg10_reg[9] [0]),
        .I4(\slv_reg5_reg[0] ),
        .I5(Q[0]),
        .O(S[0]));
  LUT6 #(
    .INIT(64'h1482141414828282)) 
    i__carry_i_8__10
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\slv_reg10_reg[1] ),
        .I3(\slv_reg10_reg[9] [0]),
        .I4(\slv_reg5_reg[0] ),
        .I5(Q[0]),
        .O(\encoded_reg[8]_11 [0]));
  LUT6 #(
    .INIT(64'h6066600006000666)) 
    i__carry_i_8__2
       (.I0(\slv_reg10_reg[1] ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\slv_reg10_reg[9] [0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(Q[0]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .O(\encoded_reg[0]_0 [0]));
  LUT6 #(
    .INIT(64'h6066600006000666)) 
    i__carry_i_8__8
       (.I0(\slv_reg10_reg[1] ),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\slv_reg10_reg[9] [0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(Q[0]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .O(\encoded_reg[0]_4 [0]));
  LUT6 #(
    .INIT(64'h4128414141282828)) 
    i__carry_i_8__9
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\slv_reg10_reg[1] ),
        .I3(\slv_reg10_reg[9] [0]),
        .I4(\slv_reg5_reg[0] ),
        .I5(Q[0]),
        .O(\encoded_reg[8]_15 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    \processQ[0]_i_1__1 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .O(\processQ[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \processQ[1]_i_1__1 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .O(plusOp__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \processQ[2]_i_1__1 
       (.I0(column__0[2]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .O(\processQ[2]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processQ[3]_i_1__1 
       (.I0(column__0[3]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(column__0[2]),
        .O(plusOp__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[4]_i_1__1 
       (.I0(column__0[4]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I2(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I3(column__0[3]),
        .I4(column__0[2]),
        .O(plusOp__1[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processQ[5]_i_1__1 
       (.I0(column__0[5]),
        .I1(column__0[4]),
        .I2(column__0[2]),
        .I3(column__0[3]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .O(plusOp__1[5]));
  LUT6 #(
    .INIT(64'hAAAAAAAA6AAAAAAA)) 
    \processQ[6]_i_1__0 
       (.I0(column__0[6]),
        .I1(column__0[5]),
        .I2(column__0[4]),
        .I3(column__0[2]),
        .I4(column__0[3]),
        .I5(\processQ[6]_i_2__0_n_0 ),
        .O(plusOp__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \processQ[6]_i_2__0 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .O(\processQ[6]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \processQ[7]_i_1__1 
       (.I0(column__0[7]),
        .I1(column__0[6]),
        .I2(\processQ[9]_i_6__0_n_0 ),
        .O(plusOp__1[7]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processQ[8]_i_1__1 
       (.I0(column__0[8]),
        .I1(column__0[6]),
        .I2(column__0[7]),
        .I3(\processQ[9]_i_6__0_n_0 ),
        .O(plusOp__1[8]));
  LUT2 #(
    .INIT(4'hB)) 
    \processQ[9]_i_1__1 
       (.I0(\processQ_reg[0]_0 ),
        .I1(reset_n),
        .O(\processQ[9]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h777777777777777F)) 
    \processQ[9]_i_2__1 
       (.I0(column__0[9]),
        .I1(column__0[8]),
        .I2(column__0[5]),
        .I3(column__0[7]),
        .I4(column__0[6]),
        .I5(\processQ[9]_i_5__0_n_0 ),
        .O(processQ0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[9]_i_3__1 
       (.I0(column__0[9]),
        .I1(column__0[8]),
        .I2(\processQ[9]_i_6__0_n_0 ),
        .I3(column__0[7]),
        .I4(column__0[6]),
        .O(plusOp__1[9]));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    \processQ[9]_i_4__1 
       (.I0(\processQ[9]_i_5__0_n_0 ),
        .I1(column__0[8]),
        .I2(column__0[9]),
        .I3(column__0[5]),
        .I4(column__0[7]),
        .I5(column__0[6]),
        .O(\processQ_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \processQ[9]_i_5__0 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(column__0[3]),
        .I3(column__0[2]),
        .I4(column__0[4]),
        .O(\processQ[9]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \processQ[9]_i_6__0 
       (.I0(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .I1(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .I2(column__0[3]),
        .I3(column__0[2]),
        .I4(column__0[4]),
        .I5(column__0[5]),
        .O(\processQ[9]_i_6__0_n_0 ));
  FDRE \processQ_reg[0] 
       (.C(CLK),
        .CE(processQ0),
        .D(\processQ[0]_i_1__1_n_0 ),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [0]),
        .R(\processQ[9]_i_1__1_n_0 ));
  FDRE \processQ_reg[1] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__1[1]),
        .Q(\sdp_bl.ramb18_dp_bl.ram18_bl [1]),
        .R(\processQ[9]_i_1__1_n_0 ));
  FDRE \processQ_reg[2] 
       (.C(CLK),
        .CE(processQ0),
        .D(\processQ[2]_i_1__1_n_0 ),
        .Q(column__0[2]),
        .R(\processQ[9]_i_1__1_n_0 ));
  FDRE \processQ_reg[3] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__1[3]),
        .Q(column__0[3]),
        .R(\processQ[9]_i_1__1_n_0 ));
  FDRE \processQ_reg[4] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__1[4]),
        .Q(column__0[4]),
        .R(\processQ[9]_i_1__1_n_0 ));
  FDRE \processQ_reg[5] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__1[5]),
        .Q(column__0[5]),
        .R(\processQ[9]_i_1__1_n_0 ));
  FDRE \processQ_reg[6] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__1[6]),
        .Q(column__0[6]),
        .R(\processQ[9]_i_1__1_n_0 ));
  FDRE \processQ_reg[7] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__1[7]),
        .Q(column__0[7]),
        .R(\processQ[9]_i_1__1_n_0 ));
  FDRE \processQ_reg[8] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__1[8]),
        .Q(column__0[8]),
        .R(\processQ[9]_i_1__1_n_0 ));
  FDRE \processQ_reg[9] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__1[9]),
        .Q(column__0[9]),
        .R(\processQ[9]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAA9A9A999)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_2__0 
       (.I0(column__0[9]),
        .I1(column__0[8]),
        .I2(column__0[4]),
        .I3(column__0[3]),
        .I4(column__0[2]),
        .I5(\sdp_bl.ramb18_dp_bl.ram18_bl_i_30_n_0 ),
        .O(ADDRARDADDR[7]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_30 
       (.I0(column__0[5]),
        .I1(column__0[7]),
        .I2(column__0[6]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl_i_30_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_31 
       (.I0(column__0[3]),
        .I1(column__0[2]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl_i_31_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA9AAA9AAA9)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_3__0 
       (.I0(column__0[8]),
        .I1(column__0[5]),
        .I2(column__0[7]),
        .I3(column__0[6]),
        .I4(\sdp_bl.ramb18_dp_bl.ram18_bl_i_31_n_0 ),
        .I5(column__0[4]),
        .O(ADDRARDADDR[6]));
  LUT6 #(
    .INIT(64'hAAAAAAA9A9A9A9A9)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_4__0 
       (.I0(column__0[7]),
        .I1(column__0[5]),
        .I2(column__0[6]),
        .I3(column__0[2]),
        .I4(column__0[3]),
        .I5(column__0[4]),
        .O(ADDRARDADDR[5]));
  LUT5 #(
    .INIT(32'hAAAAA955)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_5__0 
       (.I0(column__0[6]),
        .I1(column__0[2]),
        .I2(column__0[3]),
        .I3(column__0[4]),
        .I4(column__0[5]),
        .O(ADDRARDADDR[4]));
  LUT4 #(
    .INIT(16'h9995)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_6__0 
       (.I0(column__0[5]),
        .I1(column__0[4]),
        .I2(column__0[3]),
        .I3(column__0[2]),
        .O(ADDRARDADDR[3]));
  LUT3 #(
    .INIT(8'h56)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_7__0 
       (.I0(column__0[4]),
        .I1(column__0[2]),
        .I2(column__0[3]),
        .O(ADDRARDADDR[2]));
  LUT2 #(
    .INIT(4'h9)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_8__0 
       (.I0(column__0[3]),
        .I1(column__0[2]),
        .O(ADDRARDADDR[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_9 
       (.I0(column__0[2]),
        .O(ADDRARDADDR[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl
   (ac_bclk,
    SR,
    ac_lrclk,
    \ac_lrclk_count_reg[0] ,
    E,
    \R_unsigned_data_prev_reg[9] ,
    \R_bus_in_s_reg[17] ,
    \R_unsigned_data_prev_reg[8] ,
    D,
    \L_unsigned_data_prev_reg[9] ,
    \L_bus_in_s_reg[17] ,
    \L_unsigned_data_prev_reg[8] ,
    \L_unsigned_data_prev_reg[7] ,
    \state_reg[0] ,
    \axi_rdata_reg[15] ,
    \axi_rdata_reg[2] ,
    ac_lrclk_sig_prev_reg,
    ready_sig_reg,
    DIBDI,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    ac_dac_sdata,
    clk,
    ac_adc_sdata,
    Q,
    reset_n,
    state,
    \processQ_reg[9] ,
    ready_sig_reg_0,
    \axi_araddr_reg[4] ,
    \axi_araddr_reg[4]_0 ,
    \axi_araddr_reg[6] ,
    \axi_araddr_reg[4]_1 ,
    \slv_reg3_reg[0] ,
    \axi_araddr_reg[3]_rep ,
    \slv_reg5_reg[15] ,
    \axi_araddr_reg[2]_rep ,
    \slv_reg4_reg[15] ,
    \axi_araddr_reg[4]_2 ,
    \axi_araddr_reg[4]_3 ,
    \axi_araddr_reg[4]_4 ,
    \slv_reg3_reg[1] ,
    \slv_reg3_reg[2] ,
    \axi_araddr_reg[4]_5 ,
    \axi_araddr_reg[4]_6 ,
    \axi_araddr_reg[4]_7 ,
    \slv_reg3_reg[3] ,
    \axi_araddr_reg[4]_8 ,
    \axi_araddr_reg[4]_9 ,
    \axi_araddr_reg[4]_10 ,
    \slv_reg3_reg[4] ,
    \axi_araddr_reg[4]_11 ,
    \axi_araddr_reg[4]_12 ,
    \axi_araddr_reg[4]_13 ,
    \slv_reg3_reg[5] ,
    \axi_araddr_reg[4]_14 ,
    \axi_araddr_reg[4]_15 ,
    \axi_araddr_reg[4]_16 ,
    \slv_reg3_reg[6] ,
    \axi_araddr_reg[4]_17 ,
    \axi_araddr_reg[4]_18 ,
    \axi_araddr_reg[4]_19 ,
    \slv_reg3_reg[7] ,
    \axi_araddr_reg[4]_20 ,
    \axi_araddr_reg[4]_21 ,
    \axi_araddr_reg[4]_22 ,
    \slv_reg3_reg[8] ,
    \axi_araddr_reg[4]_23 ,
    \axi_araddr_reg[4]_24 ,
    \axi_araddr_reg[4]_25 ,
    \slv_reg3_reg[9] ,
    \axi_araddr_reg[4]_26 ,
    \axi_araddr_reg[4]_27 ,
    \axi_araddr_reg[4]_28 ,
    \slv_reg3_reg[10] ,
    \axi_araddr_reg[4]_29 ,
    \axi_araddr_reg[4]_30 ,
    \axi_araddr_reg[4]_31 ,
    \slv_reg3_reg[11] ,
    \axi_araddr_reg[4]_32 ,
    \axi_araddr_reg[4]_33 ,
    \axi_araddr_reg[4]_34 ,
    \slv_reg3_reg[12] ,
    \axi_araddr_reg[4]_35 ,
    \axi_araddr_reg[4]_36 ,
    \axi_araddr_reg[4]_37 ,
    \slv_reg3_reg[13] ,
    \axi_araddr_reg[4]_38 ,
    \axi_araddr_reg[4]_39 ,
    \axi_araddr_reg[4]_40 ,
    \slv_reg3_reg[14] ,
    \axi_araddr_reg[4]_41 ,
    \axi_araddr_reg[4]_42 ,
    \axi_araddr_reg[4]_43 ,
    \slv_reg3_reg[15] ,
    ac_lrclk_sig_prev_reg_0,
    \L_bus_in_s_reg[17]_0 ,
    \R_bus_in_s_reg[17]_0 ,
    CO,
    switch,
    \L_unsigned_data_prev_reg[9]_0 ,
    \slv_reg11_reg[7] ,
    \int_trigger_volt_s_reg[9] ,
    \slv_reg11_reg[9] ,
    \slv_reg11_reg[6] ,
    \slv_reg11_reg[5] ,
    \slv_reg11_reg[4] ,
    \slv_reg11_reg[3] ,
    \slv_reg11_reg[1] ,
    \slv_reg11_reg[8] ,
    \slv_reg2_reg[9] ,
    \slv_reg1_reg[9] );
  output ac_bclk;
  output [0:0]SR;
  output ac_lrclk;
  output [0:0]\ac_lrclk_count_reg[0] ;
  output [0:0]E;
  output \R_unsigned_data_prev_reg[9] ;
  output [17:0]\R_bus_in_s_reg[17] ;
  output \R_unsigned_data_prev_reg[8] ;
  output [4:0]D;
  output \L_unsigned_data_prev_reg[9] ;
  output [17:0]\L_bus_in_s_reg[17] ;
  output \L_unsigned_data_prev_reg[8] ;
  output [4:0]\L_unsigned_data_prev_reg[7] ;
  output \state_reg[0] ;
  output [14:0]\axi_rdata_reg[15] ;
  output \axi_rdata_reg[2] ;
  output ac_lrclk_sig_prev_reg;
  output ready_sig_reg;
  output [9:0]DIBDI;
  output [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  output ac_dac_sdata;
  input clk;
  input ac_adc_sdata;
  input [3:0]Q;
  input reset_n;
  input [1:0]state;
  input [0:0]\processQ_reg[9] ;
  input ready_sig_reg_0;
  input \axi_araddr_reg[4] ;
  input \axi_araddr_reg[4]_0 ;
  input [2:0]\axi_araddr_reg[6] ;
  input \axi_araddr_reg[4]_1 ;
  input \slv_reg3_reg[0] ;
  input \axi_araddr_reg[3]_rep ;
  input [15:0]\slv_reg5_reg[15] ;
  input \axi_araddr_reg[2]_rep ;
  input [15:0]\slv_reg4_reg[15] ;
  input \axi_araddr_reg[4]_2 ;
  input \axi_araddr_reg[4]_3 ;
  input \axi_araddr_reg[4]_4 ;
  input \slv_reg3_reg[1] ;
  input \slv_reg3_reg[2] ;
  input \axi_araddr_reg[4]_5 ;
  input \axi_araddr_reg[4]_6 ;
  input \axi_araddr_reg[4]_7 ;
  input \slv_reg3_reg[3] ;
  input \axi_araddr_reg[4]_8 ;
  input \axi_araddr_reg[4]_9 ;
  input \axi_araddr_reg[4]_10 ;
  input \slv_reg3_reg[4] ;
  input \axi_araddr_reg[4]_11 ;
  input \axi_araddr_reg[4]_12 ;
  input \axi_araddr_reg[4]_13 ;
  input \slv_reg3_reg[5] ;
  input \axi_araddr_reg[4]_14 ;
  input \axi_araddr_reg[4]_15 ;
  input \axi_araddr_reg[4]_16 ;
  input \slv_reg3_reg[6] ;
  input \axi_araddr_reg[4]_17 ;
  input \axi_araddr_reg[4]_18 ;
  input \axi_araddr_reg[4]_19 ;
  input \slv_reg3_reg[7] ;
  input \axi_araddr_reg[4]_20 ;
  input \axi_araddr_reg[4]_21 ;
  input \axi_araddr_reg[4]_22 ;
  input \slv_reg3_reg[8] ;
  input \axi_araddr_reg[4]_23 ;
  input \axi_araddr_reg[4]_24 ;
  input \axi_araddr_reg[4]_25 ;
  input \slv_reg3_reg[9] ;
  input \axi_araddr_reg[4]_26 ;
  input \axi_araddr_reg[4]_27 ;
  input \axi_araddr_reg[4]_28 ;
  input \slv_reg3_reg[10] ;
  input \axi_araddr_reg[4]_29 ;
  input \axi_araddr_reg[4]_30 ;
  input \axi_araddr_reg[4]_31 ;
  input \slv_reg3_reg[11] ;
  input \axi_araddr_reg[4]_32 ;
  input \axi_araddr_reg[4]_33 ;
  input \axi_araddr_reg[4]_34 ;
  input \slv_reg3_reg[12] ;
  input \axi_araddr_reg[4]_35 ;
  input \axi_araddr_reg[4]_36 ;
  input \axi_araddr_reg[4]_37 ;
  input \slv_reg3_reg[13] ;
  input \axi_araddr_reg[4]_38 ;
  input \axi_araddr_reg[4]_39 ;
  input \axi_araddr_reg[4]_40 ;
  input \slv_reg3_reg[14] ;
  input \axi_araddr_reg[4]_41 ;
  input \axi_araddr_reg[4]_42 ;
  input \axi_araddr_reg[4]_43 ;
  input \slv_reg3_reg[15] ;
  input ac_lrclk_sig_prev_reg_0;
  input [17:0]\L_bus_in_s_reg[17]_0 ;
  input [17:0]\R_bus_in_s_reg[17]_0 ;
  input [0:0]CO;
  input [0:0]switch;
  input [0:0]\L_unsigned_data_prev_reg[9]_0 ;
  input \slv_reg11_reg[7] ;
  input [4:0]\int_trigger_volt_s_reg[9] ;
  input [4:0]\slv_reg11_reg[9] ;
  input \slv_reg11_reg[6] ;
  input \slv_reg11_reg[5] ;
  input \slv_reg11_reg[4] ;
  input \slv_reg11_reg[3] ;
  input \slv_reg11_reg[1] ;
  input \slv_reg11_reg[8] ;
  input [9:0]\slv_reg2_reg[9] ;
  input [9:0]\slv_reg1_reg[9] ;

  wire BCLK_Fall_int;
  wire BCLK_int_i_2_n_0;
  wire [0:0]CO;
  wire Cnt_Bclk0;
  wire \Cnt_Bclk0_inferred__0/i__carry_n_3 ;
  wire \Cnt_Bclk[4]_i_1_n_0 ;
  wire [4:0]Cnt_Bclk_reg__0;
  wire [4:0]Cnt_Lrclk;
  wire \Cnt_Lrclk[0]_i_1_n_0 ;
  wire \Cnt_Lrclk[1]_i_1_n_0 ;
  wire \Cnt_Lrclk[2]_i_1_n_0 ;
  wire \Cnt_Lrclk[3]_i_1_n_0 ;
  wire \Cnt_Lrclk[4]_i_2_n_0 ;
  wire [4:0]D;
  wire [9:0]DIBDI;
  wire D_L_O_int;
  wire \D_R_O_int[23]_i_1_n_0 ;
  wire [31:14]Data_In_int;
  wire \Data_In_int[31]_i_1_n_0 ;
  wire \Data_In_int[31]_i_3_n_0 ;
  wire \Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11_n_0 ;
  wire \Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12_n_0 ;
  wire Data_In_int_reg_gate_n_0;
  wire Data_In_int_reg_r_0_n_0;
  wire Data_In_int_reg_r_10_n_0;
  wire Data_In_int_reg_r_11_n_0;
  wire Data_In_int_reg_r_12_n_0;
  wire Data_In_int_reg_r_1_n_0;
  wire Data_In_int_reg_r_2_n_0;
  wire Data_In_int_reg_r_3_n_0;
  wire Data_In_int_reg_r_4_n_0;
  wire Data_In_int_reg_r_5_n_0;
  wire Data_In_int_reg_r_6_n_0;
  wire Data_In_int_reg_r_7_n_0;
  wire Data_In_int_reg_r_8_n_0;
  wire Data_In_int_reg_r_9_n_0;
  wire Data_In_int_reg_r_n_0;
  wire \Data_Out_int[13]_i_1_n_0 ;
  wire \Data_Out_int[14]_i_1_n_0 ;
  wire \Data_Out_int[15]_i_1_n_0 ;
  wire \Data_Out_int[16]_i_1_n_0 ;
  wire \Data_Out_int[17]_i_1_n_0 ;
  wire \Data_Out_int[18]_i_1_n_0 ;
  wire \Data_Out_int[19]_i_1_n_0 ;
  wire \Data_Out_int[20]_i_1_n_0 ;
  wire \Data_Out_int[21]_i_1_n_0 ;
  wire \Data_Out_int[22]_i_1_n_0 ;
  wire \Data_Out_int[23]_i_1_n_0 ;
  wire \Data_Out_int[24]_i_1_n_0 ;
  wire \Data_Out_int[25]_i_1_n_0 ;
  wire \Data_Out_int[26]_i_1_n_0 ;
  wire \Data_Out_int[27]_i_1_n_0 ;
  wire \Data_Out_int[28]_i_1_n_0 ;
  wire \Data_Out_int[29]_i_1_n_0 ;
  wire \Data_Out_int[30]_i_1_n_0 ;
  wire \Data_Out_int[31]_i_1_n_0 ;
  wire \Data_Out_int[31]_i_2_n_0 ;
  wire \Data_Out_int[31]_i_3_n_0 ;
  wire \Data_Out_int_reg_n_0_[13] ;
  wire \Data_Out_int_reg_n_0_[14] ;
  wire \Data_Out_int_reg_n_0_[15] ;
  wire \Data_Out_int_reg_n_0_[16] ;
  wire \Data_Out_int_reg_n_0_[17] ;
  wire \Data_Out_int_reg_n_0_[18] ;
  wire \Data_Out_int_reg_n_0_[19] ;
  wire \Data_Out_int_reg_n_0_[20] ;
  wire \Data_Out_int_reg_n_0_[21] ;
  wire \Data_Out_int_reg_n_0_[22] ;
  wire \Data_Out_int_reg_n_0_[23] ;
  wire \Data_Out_int_reg_n_0_[24] ;
  wire \Data_Out_int_reg_n_0_[25] ;
  wire \Data_Out_int_reg_n_0_[26] ;
  wire \Data_Out_int_reg_n_0_[27] ;
  wire \Data_Out_int_reg_n_0_[28] ;
  wire \Data_Out_int_reg_n_0_[29] ;
  wire \Data_Out_int_reg_n_0_[30] ;
  wire [0:0]E;
  wire LRCLK_i_1_n_0;
  wire LRCLK_i_2_n_0;
  wire L_bus_g;
  wire [17:0]\L_bus_in_s_reg[17] ;
  wire [17:0]\L_bus_in_s_reg[17]_0 ;
  wire [4:0]\L_unsigned_data_prev_reg[7] ;
  wire \L_unsigned_data_prev_reg[8] ;
  wire \L_unsigned_data_prev_reg[9] ;
  wire [0:0]\L_unsigned_data_prev_reg[9]_0 ;
  wire [3:0]Q;
  wire R_bus_g;
  wire [17:0]\R_bus_in_s_reg[17] ;
  wire [17:0]\R_bus_in_s_reg[17]_0 ;
  wire \R_unsigned_data_prev_reg[8] ;
  wire \R_unsigned_data_prev_reg[9] ;
  wire [0:0]SR;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire [0:0]\ac_lrclk_count_reg[0] ;
  wire ac_lrclk_sig_prev_reg;
  wire ac_lrclk_sig_prev_reg_0;
  wire \axi_araddr_reg[2]_rep ;
  wire \axi_araddr_reg[3]_rep ;
  wire \axi_araddr_reg[4] ;
  wire \axi_araddr_reg[4]_0 ;
  wire \axi_araddr_reg[4]_1 ;
  wire \axi_araddr_reg[4]_10 ;
  wire \axi_araddr_reg[4]_11 ;
  wire \axi_araddr_reg[4]_12 ;
  wire \axi_araddr_reg[4]_13 ;
  wire \axi_araddr_reg[4]_14 ;
  wire \axi_araddr_reg[4]_15 ;
  wire \axi_araddr_reg[4]_16 ;
  wire \axi_araddr_reg[4]_17 ;
  wire \axi_araddr_reg[4]_18 ;
  wire \axi_araddr_reg[4]_19 ;
  wire \axi_araddr_reg[4]_2 ;
  wire \axi_araddr_reg[4]_20 ;
  wire \axi_araddr_reg[4]_21 ;
  wire \axi_araddr_reg[4]_22 ;
  wire \axi_araddr_reg[4]_23 ;
  wire \axi_araddr_reg[4]_24 ;
  wire \axi_araddr_reg[4]_25 ;
  wire \axi_araddr_reg[4]_26 ;
  wire \axi_araddr_reg[4]_27 ;
  wire \axi_araddr_reg[4]_28 ;
  wire \axi_araddr_reg[4]_29 ;
  wire \axi_araddr_reg[4]_3 ;
  wire \axi_araddr_reg[4]_30 ;
  wire \axi_araddr_reg[4]_31 ;
  wire \axi_araddr_reg[4]_32 ;
  wire \axi_araddr_reg[4]_33 ;
  wire \axi_araddr_reg[4]_34 ;
  wire \axi_araddr_reg[4]_35 ;
  wire \axi_araddr_reg[4]_36 ;
  wire \axi_araddr_reg[4]_37 ;
  wire \axi_araddr_reg[4]_38 ;
  wire \axi_araddr_reg[4]_39 ;
  wire \axi_araddr_reg[4]_4 ;
  wire \axi_araddr_reg[4]_40 ;
  wire \axi_araddr_reg[4]_41 ;
  wire \axi_araddr_reg[4]_42 ;
  wire \axi_araddr_reg[4]_43 ;
  wire \axi_araddr_reg[4]_5 ;
  wire \axi_araddr_reg[4]_6 ;
  wire \axi_araddr_reg[4]_7 ;
  wire \axi_araddr_reg[4]_8 ;
  wire \axi_araddr_reg[4]_9 ;
  wire [2:0]\axi_araddr_reg[6] ;
  wire \axi_rdata[0]_i_13_n_0 ;
  wire \axi_rdata[10]_i_12_n_0 ;
  wire \axi_rdata[11]_i_12_n_0 ;
  wire \axi_rdata[12]_i_12_n_0 ;
  wire \axi_rdata[13]_i_12_n_0 ;
  wire \axi_rdata[14]_i_12_n_0 ;
  wire \axi_rdata[15]_i_12_n_0 ;
  wire \axi_rdata[1]_i_12_n_0 ;
  wire \axi_rdata[2]_i_13_n_0 ;
  wire \axi_rdata[3]_i_12_n_0 ;
  wire \axi_rdata[4]_i_12_n_0 ;
  wire \axi_rdata[5]_i_12_n_0 ;
  wire \axi_rdata[6]_i_12_n_0 ;
  wire \axi_rdata[7]_i_12_n_0 ;
  wire \axi_rdata[8]_i_12_n_0 ;
  wire \axi_rdata[9]_i_12_n_0 ;
  wire \axi_rdata_reg[0]_i_5_n_0 ;
  wire \axi_rdata_reg[10]_i_5_n_0 ;
  wire \axi_rdata_reg[11]_i_5_n_0 ;
  wire \axi_rdata_reg[12]_i_5_n_0 ;
  wire \axi_rdata_reg[13]_i_5_n_0 ;
  wire \axi_rdata_reg[14]_i_5_n_0 ;
  wire [14:0]\axi_rdata_reg[15] ;
  wire \axi_rdata_reg[15]_i_5_n_0 ;
  wire \axi_rdata_reg[1]_i_5_n_0 ;
  wire \axi_rdata_reg[2] ;
  wire \axi_rdata_reg[3]_i_5_n_0 ;
  wire \axi_rdata_reg[4]_i_5_n_0 ;
  wire \axi_rdata_reg[5]_i_5_n_0 ;
  wire \axi_rdata_reg[6]_i_5_n_0 ;
  wire \axi_rdata_reg[7]_i_5_n_0 ;
  wire \axi_rdata_reg[8]_i_5_n_0 ;
  wire \axi_rdata_reg[9]_i_5_n_0 ;
  wire clk;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2__11_n_0;
  wire [4:0]\int_trigger_volt_s_reg[9] ;
  wire [4:0]p_0_in;
  wire p_17_in;
  wire [0:0]\processQ_reg[9] ;
  wire ready_sig_i_2_n_0;
  wire ready_sig_reg;
  wire ready_sig_reg_0;
  wire reset_n;
  wire [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire \slv_reg11_reg[1] ;
  wire \slv_reg11_reg[3] ;
  wire \slv_reg11_reg[4] ;
  wire \slv_reg11_reg[5] ;
  wire \slv_reg11_reg[6] ;
  wire \slv_reg11_reg[7] ;
  wire \slv_reg11_reg[8] ;
  wire [4:0]\slv_reg11_reg[9] ;
  wire [9:0]\slv_reg1_reg[9] ;
  wire [9:0]\slv_reg2_reg[9] ;
  wire \slv_reg3_reg[0] ;
  wire \slv_reg3_reg[10] ;
  wire \slv_reg3_reg[11] ;
  wire \slv_reg3_reg[12] ;
  wire \slv_reg3_reg[13] ;
  wire \slv_reg3_reg[14] ;
  wire \slv_reg3_reg[15] ;
  wire \slv_reg3_reg[1] ;
  wire \slv_reg3_reg[2] ;
  wire \slv_reg3_reg[3] ;
  wire \slv_reg3_reg[4] ;
  wire \slv_reg3_reg[5] ;
  wire \slv_reg3_reg[6] ;
  wire \slv_reg3_reg[7] ;
  wire \slv_reg3_reg[8] ;
  wire \slv_reg3_reg[9] ;
  wire [15:0]\slv_reg4_reg[15] ;
  wire [15:0]\slv_reg5_reg[15] ;
  wire [1:0]state;
  wire \state[0]_i_14_n_0 ;
  wire \state[0]_i_15_n_0 ;
  wire \state[0]_i_19_n_0 ;
  wire \state[0]_i_20_n_0 ;
  wire \state[0]_i_21_n_0 ;
  wire \state[0]_i_22_n_0 ;
  wire \state[0]_i_23_n_0 ;
  wire \state[0]_i_24_n_0 ;
  wire \state[0]_i_25_n_0 ;
  wire \state[0]_i_26_n_0 ;
  wire \state[0]_i_35_n_0 ;
  wire \state[0]_i_36_n_0 ;
  wire \state[0]_i_37_n_0 ;
  wire \state[0]_i_38_n_0 ;
  wire \state[0]_i_39_n_0 ;
  wire \state[0]_i_40_n_0 ;
  wire \state[0]_i_41_n_0 ;
  wire \state[0]_i_42_n_0 ;
  wire \state[0]_i_8_n_0 ;
  wire \state[0]_i_9_n_0 ;
  wire \state_reg[0] ;
  wire \state_reg[0]_i_13_n_0 ;
  wire \state_reg[0]_i_13_n_1 ;
  wire \state_reg[0]_i_13_n_2 ;
  wire \state_reg[0]_i_13_n_3 ;
  wire \state_reg[0]_i_7_n_0 ;
  wire \state_reg[0]_i_7_n_1 ;
  wire \state_reg[0]_i_7_n_2 ;
  wire \state_reg[0]_i_7_n_3 ;
  wire [2:2]sw;
  wire [0:0]switch;
  wire [3:2]\NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED ;
  wire [3:0]\NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_state_reg[0]_i_13_O_UNCONNECTED ;
  wire [3:1]\NLW_state_reg[0]_i_3_CO_UNCONNECTED ;
  wire [3:0]\NLW_state_reg[0]_i_3_O_UNCONNECTED ;
  wire [3:1]\NLW_state_reg[0]_i_5_CO_UNCONNECTED ;
  wire [3:0]\NLW_state_reg[0]_i_5_O_UNCONNECTED ;
  wire [3:0]\NLW_state_reg[0]_i_7_O_UNCONNECTED ;

  LUT1 #(
    .INIT(2'h1)) 
    BCLK_int_i_1
       (.I0(reset_n),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    BCLK_int_i_2
       (.I0(Cnt_Bclk0),
        .I1(ac_bclk),
        .O(BCLK_int_i_2_n_0));
  FDRE BCLK_int_reg
       (.C(clk),
        .CE(1'b1),
        .D(BCLK_int_i_2_n_0),
        .Q(ac_bclk),
        .R(SR));
  CARRY4 \Cnt_Bclk0_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED [3:2],Cnt_Bclk0,\Cnt_Bclk0_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,i__carry_i_1_n_0,i__carry_i_2__11_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    \Cnt_Bclk[0]_i_1 
       (.I0(Cnt_Bclk_reg__0[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \Cnt_Bclk[1]_i_1 
       (.I0(Cnt_Bclk_reg__0[1]),
        .I1(Cnt_Bclk_reg__0[0]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \Cnt_Bclk[2]_i_1 
       (.I0(Cnt_Bclk_reg__0[2]),
        .I1(Cnt_Bclk_reg__0[0]),
        .I2(Cnt_Bclk_reg__0[1]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \Cnt_Bclk[3]_i_1 
       (.I0(Cnt_Bclk_reg__0[3]),
        .I1(Cnt_Bclk_reg__0[2]),
        .I2(Cnt_Bclk_reg__0[1]),
        .I3(Cnt_Bclk_reg__0[0]),
        .O(p_0_in[3]));
  LUT2 #(
    .INIT(4'hB)) 
    \Cnt_Bclk[4]_i_1 
       (.I0(Cnt_Bclk0),
        .I1(reset_n),
        .O(\Cnt_Bclk[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \Cnt_Bclk[4]_i_2 
       (.I0(Cnt_Bclk_reg__0[4]),
        .I1(Cnt_Bclk_reg__0[0]),
        .I2(Cnt_Bclk_reg__0[1]),
        .I3(Cnt_Bclk_reg__0[2]),
        .I4(Cnt_Bclk_reg__0[3]),
        .O(p_0_in[4]));
  FDRE \Cnt_Bclk_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(Cnt_Bclk_reg__0[0]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(Cnt_Bclk_reg__0[1]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(Cnt_Bclk_reg__0[2]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(Cnt_Bclk_reg__0[3]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(Cnt_Bclk_reg__0[4]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \Cnt_Lrclk[0]_i_1 
       (.I0(Cnt_Lrclk[0]),
        .O(\Cnt_Lrclk[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \Cnt_Lrclk[1]_i_1 
       (.I0(Cnt_Lrclk[1]),
        .I1(Cnt_Lrclk[0]),
        .O(\Cnt_Lrclk[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \Cnt_Lrclk[2]_i_1 
       (.I0(Cnt_Lrclk[2]),
        .I1(Cnt_Lrclk[0]),
        .I2(Cnt_Lrclk[1]),
        .O(\Cnt_Lrclk[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \Cnt_Lrclk[3]_i_1 
       (.I0(Cnt_Lrclk[3]),
        .I1(Cnt_Lrclk[1]),
        .I2(Cnt_Lrclk[0]),
        .I3(Cnt_Lrclk[2]),
        .O(\Cnt_Lrclk[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Cnt_Lrclk[4]_i_1 
       (.I0(Cnt_Bclk0),
        .I1(ac_bclk),
        .O(BCLK_Fall_int));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \Cnt_Lrclk[4]_i_2 
       (.I0(Cnt_Lrclk[4]),
        .I1(Cnt_Lrclk[2]),
        .I2(Cnt_Lrclk[0]),
        .I3(Cnt_Lrclk[1]),
        .I4(Cnt_Lrclk[3]),
        .O(\Cnt_Lrclk[4]_i_2_n_0 ));
  FDRE \Cnt_Lrclk_reg[0] 
       (.C(clk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[0]_i_1_n_0 ),
        .Q(Cnt_Lrclk[0]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[1] 
       (.C(clk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[1]_i_1_n_0 ),
        .Q(Cnt_Lrclk[1]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[2] 
       (.C(clk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[2]_i_1_n_0 ),
        .Q(Cnt_Lrclk[2]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[3] 
       (.C(clk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[3]_i_1_n_0 ),
        .Q(Cnt_Lrclk[3]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[4] 
       (.C(clk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[4]_i_2_n_0 ),
        .Q(Cnt_Lrclk[4]),
        .R(SR));
  LUT2 #(
    .INIT(4'h2)) 
    \D_L_O_int[23]_i_1 
       (.I0(ac_lrclk),
        .I1(\Data_In_int[31]_i_3_n_0 ),
        .O(D_L_O_int));
  FDRE \D_L_O_int_reg[10] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[18]),
        .Q(\L_bus_in_s_reg[17] [4]),
        .R(SR));
  FDRE \D_L_O_int_reg[11] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[19]),
        .Q(\L_bus_in_s_reg[17] [5]),
        .R(SR));
  FDRE \D_L_O_int_reg[12] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[20]),
        .Q(\L_bus_in_s_reg[17] [6]),
        .R(SR));
  FDRE \D_L_O_int_reg[13] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[21]),
        .Q(\L_bus_in_s_reg[17] [7]),
        .R(SR));
  FDRE \D_L_O_int_reg[14] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[22]),
        .Q(\L_bus_in_s_reg[17] [8]),
        .R(SR));
  FDRE \D_L_O_int_reg[15] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[23]),
        .Q(\L_bus_in_s_reg[17] [9]),
        .R(SR));
  FDRE \D_L_O_int_reg[16] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[24]),
        .Q(\L_bus_in_s_reg[17] [10]),
        .R(SR));
  FDRE \D_L_O_int_reg[17] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[25]),
        .Q(\L_bus_in_s_reg[17] [11]),
        .R(SR));
  FDRE \D_L_O_int_reg[18] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[26]),
        .Q(\L_bus_in_s_reg[17] [12]),
        .R(SR));
  FDRE \D_L_O_int_reg[19] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[27]),
        .Q(\L_bus_in_s_reg[17] [13]),
        .R(SR));
  FDRE \D_L_O_int_reg[20] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[28]),
        .Q(\L_bus_in_s_reg[17] [14]),
        .R(SR));
  FDRE \D_L_O_int_reg[21] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[29]),
        .Q(\L_bus_in_s_reg[17] [15]),
        .R(SR));
  FDRE \D_L_O_int_reg[22] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[30]),
        .Q(\L_bus_in_s_reg[17] [16]),
        .R(SR));
  FDRE \D_L_O_int_reg[23] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[31]),
        .Q(\L_bus_in_s_reg[17] [17]),
        .R(SR));
  FDRE \D_L_O_int_reg[6] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[14]),
        .Q(\L_bus_in_s_reg[17] [0]),
        .R(SR));
  FDRE \D_L_O_int_reg[7] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[15]),
        .Q(\L_bus_in_s_reg[17] [1]),
        .R(SR));
  FDRE \D_L_O_int_reg[8] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[16]),
        .Q(\L_bus_in_s_reg[17] [2]),
        .R(SR));
  FDRE \D_L_O_int_reg[9] 
       (.C(clk),
        .CE(D_L_O_int),
        .D(Data_In_int[17]),
        .Q(\L_bus_in_s_reg[17] [3]),
        .R(SR));
  LUT2 #(
    .INIT(4'h1)) 
    \D_R_O_int[23]_i_1 
       (.I0(ac_lrclk),
        .I1(\Data_In_int[31]_i_3_n_0 ),
        .O(\D_R_O_int[23]_i_1_n_0 ));
  FDRE \D_R_O_int_reg[10] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[18]),
        .Q(\R_bus_in_s_reg[17] [4]),
        .R(SR));
  FDRE \D_R_O_int_reg[11] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[19]),
        .Q(\R_bus_in_s_reg[17] [5]),
        .R(SR));
  FDRE \D_R_O_int_reg[12] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[20]),
        .Q(\R_bus_in_s_reg[17] [6]),
        .R(SR));
  FDRE \D_R_O_int_reg[13] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[21]),
        .Q(\R_bus_in_s_reg[17] [7]),
        .R(SR));
  FDRE \D_R_O_int_reg[14] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[22]),
        .Q(\R_bus_in_s_reg[17] [8]),
        .R(SR));
  FDRE \D_R_O_int_reg[15] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[23]),
        .Q(\R_bus_in_s_reg[17] [9]),
        .R(SR));
  FDRE \D_R_O_int_reg[16] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[24]),
        .Q(\R_bus_in_s_reg[17] [10]),
        .R(SR));
  FDRE \D_R_O_int_reg[17] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[25]),
        .Q(\R_bus_in_s_reg[17] [11]),
        .R(SR));
  FDRE \D_R_O_int_reg[18] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[26]),
        .Q(\R_bus_in_s_reg[17] [12]),
        .R(SR));
  FDRE \D_R_O_int_reg[19] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[27]),
        .Q(\R_bus_in_s_reg[17] [13]),
        .R(SR));
  FDRE \D_R_O_int_reg[20] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[28]),
        .Q(\R_bus_in_s_reg[17] [14]),
        .R(SR));
  FDRE \D_R_O_int_reg[21] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[29]),
        .Q(\R_bus_in_s_reg[17] [15]),
        .R(SR));
  FDRE \D_R_O_int_reg[22] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[30]),
        .Q(\R_bus_in_s_reg[17] [16]),
        .R(SR));
  FDRE \D_R_O_int_reg[23] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[31]),
        .Q(\R_bus_in_s_reg[17] [17]),
        .R(SR));
  FDRE \D_R_O_int_reg[6] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[14]),
        .Q(\R_bus_in_s_reg[17] [0]),
        .R(SR));
  FDRE \D_R_O_int_reg[7] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[15]),
        .Q(\R_bus_in_s_reg[17] [1]),
        .R(SR));
  FDRE \D_R_O_int_reg[8] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[16]),
        .Q(\R_bus_in_s_reg[17] [2]),
        .R(SR));
  FDRE \D_R_O_int_reg[9] 
       (.C(clk),
        .CE(\D_R_O_int[23]_i_1_n_0 ),
        .D(Data_In_int[17]),
        .Q(\R_bus_in_s_reg[17] [3]),
        .R(SR));
  LUT2 #(
    .INIT(4'h7)) 
    \Data_In_int[31]_i_1 
       (.I0(reset_n),
        .I1(\Data_In_int[31]_i_3_n_0 ),
        .O(\Data_In_int[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Data_In_int[31]_i_2 
       (.I0(Cnt_Bclk0),
        .I1(ac_bclk),
        .O(p_17_in));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \Data_In_int[31]_i_3 
       (.I0(Cnt_Lrclk[3]),
        .I1(Cnt_Lrclk[4]),
        .I2(Cnt_Lrclk[2]),
        .I3(Cnt_Lrclk[1]),
        .I4(Cnt_Lrclk[0]),
        .I5(BCLK_Fall_int),
        .O(\Data_In_int[31]_i_3_n_0 ));
  (* srl_bus_name = "\U0/my_oscope_ip_v1_0_S00_AXI_inst/datapath/audio_codec/audio_inout/Data_In_int_reg " *) 
  (* srl_name = "\U0/my_oscope_ip_v1_0_S00_AXI_inst/datapath/audio_codec/audio_inout/Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11 " *) 
  SRL16E \Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11 
       (.A0(1'b0),
        .A1(1'b0),
        .A2(1'b1),
        .A3(1'b1),
        .CE(p_17_in),
        .CLK(clk),
        .D(ac_adc_sdata),
        .Q(\Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11_n_0 ));
  FDRE \Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12 
       (.C(clk),
        .CE(p_17_in),
        .D(\Data_In_int_reg[12]_srl13___U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_11_n_0 ),
        .Q(\Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12_n_0 ),
        .R(1'b0));
  FDRE \Data_In_int_reg[14] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_gate_n_0),
        .Q(Data_In_int[14]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[15] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[14]),
        .Q(Data_In_int[15]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[16] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[15]),
        .Q(Data_In_int[16]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[17] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[16]),
        .Q(Data_In_int[17]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[18] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[17]),
        .Q(Data_In_int[18]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[19] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[18]),
        .Q(Data_In_int[19]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[20] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[19]),
        .Q(Data_In_int[20]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[21] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[20]),
        .Q(Data_In_int[21]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[22] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[21]),
        .Q(Data_In_int[22]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[23] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[22]),
        .Q(Data_In_int[23]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[24] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[23]),
        .Q(Data_In_int[24]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[25] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[24]),
        .Q(Data_In_int[25]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[26] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[25]),
        .Q(Data_In_int[26]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[27] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[26]),
        .Q(Data_In_int[27]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[28] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[27]),
        .Q(Data_In_int[28]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[29] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[28]),
        .Q(Data_In_int[29]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[30] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[29]),
        .Q(Data_In_int[30]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE \Data_In_int_reg[31] 
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int[30]),
        .Q(Data_In_int[31]),
        .R(\Data_In_int[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    Data_In_int_reg_gate
       (.I0(\Data_In_int_reg[13]_U0_my_oscope_ip_v1_0_S00_AXI_inst_datapath_audio_codec_audio_inout_Data_In_int_reg_r_12_n_0 ),
        .I1(Data_In_int_reg_r_12_n_0),
        .O(Data_In_int_reg_gate_n_0));
  FDRE Data_In_int_reg_r
       (.C(clk),
        .CE(p_17_in),
        .D(1'b1),
        .Q(Data_In_int_reg_r_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_0
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_n_0),
        .Q(Data_In_int_reg_r_0_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_1
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_0_n_0),
        .Q(Data_In_int_reg_r_1_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_10
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_9_n_0),
        .Q(Data_In_int_reg_r_10_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_11
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_10_n_0),
        .Q(Data_In_int_reg_r_11_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_12
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_11_n_0),
        .Q(Data_In_int_reg_r_12_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_2
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_1_n_0),
        .Q(Data_In_int_reg_r_2_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_3
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_2_n_0),
        .Q(Data_In_int_reg_r_3_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_4
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_3_n_0),
        .Q(Data_In_int_reg_r_4_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_5
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_4_n_0),
        .Q(Data_In_int_reg_r_5_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_6
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_5_n_0),
        .Q(Data_In_int_reg_r_6_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_7
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_6_n_0),
        .Q(Data_In_int_reg_r_7_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_8
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_7_n_0),
        .Q(Data_In_int_reg_r_8_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  FDRE Data_In_int_reg_r_9
       (.C(clk),
        .CE(p_17_in),
        .D(Data_In_int_reg_r_8_n_0),
        .Q(Data_In_int_reg_r_9_n_0),
        .R(\Data_In_int[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA8CC08CC)) 
    \Data_Out_int[13]_i_1 
       (.I0(\Data_Out_int[31]_i_3_n_0 ),
        .I1(\L_bus_in_s_reg[17]_0 [0]),
        .I2(ac_lrclk),
        .I3(reset_n),
        .I4(\R_bus_in_s_reg[17]_0 [0]),
        .O(\Data_Out_int[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[14]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [1]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[13] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [1]),
        .O(\Data_Out_int[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[15]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [2]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[14] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [2]),
        .O(\Data_Out_int[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[16]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [3]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[15] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [3]),
        .O(\Data_Out_int[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[17]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [4]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[16] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [4]),
        .O(\Data_Out_int[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[18]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [5]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[17] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [5]),
        .O(\Data_Out_int[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[19]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [6]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[18] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [6]),
        .O(\Data_Out_int[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[20]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [7]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[19] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [7]),
        .O(\Data_Out_int[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[21]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [8]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[20] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [8]),
        .O(\Data_Out_int[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[22]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [9]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[21] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [9]),
        .O(\Data_Out_int[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[23]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [10]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[22] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [10]),
        .O(\Data_Out_int[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[24]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [11]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[23] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [11]),
        .O(\Data_Out_int[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[25]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [12]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[24] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [12]),
        .O(\Data_Out_int[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[26]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [13]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[25] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [13]),
        .O(\Data_Out_int[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[27]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [14]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[26] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [14]),
        .O(\Data_Out_int[27]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[28]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [15]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[27] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [15]),
        .O(\Data_Out_int[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[29]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [16]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[28] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [16]),
        .O(\Data_Out_int[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFB0FFFF8F800000)) 
    \Data_Out_int[30]_i_1 
       (.I0(\R_bus_in_s_reg[17]_0 [17]),
        .I1(ac_lrclk),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .I3(\Data_Out_int_reg_n_0_[29] ),
        .I4(reset_n),
        .I5(\L_bus_in_s_reg[17]_0 [17]),
        .O(\Data_Out_int[30]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF8F)) 
    \Data_Out_int[31]_i_1 
       (.I0(ac_bclk),
        .I1(Cnt_Bclk0),
        .I2(reset_n),
        .I3(\Data_Out_int[31]_i_3_n_0 ),
        .O(\Data_Out_int[31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \Data_Out_int[31]_i_2 
       (.I0(\Data_Out_int_reg_n_0_[30] ),
        .I1(reset_n),
        .I2(\Data_Out_int[31]_i_3_n_0 ),
        .O(\Data_Out_int[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \Data_Out_int[31]_i_3 
       (.I0(p_17_in),
        .I1(Cnt_Lrclk[3]),
        .I2(Cnt_Lrclk[4]),
        .I3(Cnt_Lrclk[2]),
        .I4(Cnt_Lrclk[1]),
        .I5(Cnt_Lrclk[0]),
        .O(\Data_Out_int[31]_i_3_n_0 ));
  FDRE \Data_Out_int_reg[13] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[13]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[14] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[14]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[15] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[15]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[16] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[16]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[17] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[17]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[18] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[18]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[19] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[19]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[20] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[20]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[20] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[21] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[21]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[22] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[22]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[23] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[23]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[23] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[24] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[24]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[24] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[25] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[25]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[25] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[26] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[26]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[26] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[27] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[27]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[27] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[28] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[28]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[28] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[29] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[29]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[29] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[30] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[30]_i_1_n_0 ),
        .Q(\Data_Out_int_reg_n_0_[30] ),
        .R(1'b0));
  FDRE \Data_Out_int_reg[31] 
       (.C(clk),
        .CE(\Data_Out_int[31]_i_1_n_0 ),
        .D(\Data_Out_int[31]_i_2_n_0 ),
        .Q(ac_dac_sdata),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    LRCLK_i_1
       (.I0(Cnt_Lrclk[4]),
        .I1(LRCLK_i_2_n_0),
        .I2(Cnt_Bclk0),
        .I3(ac_bclk),
        .I4(ac_lrclk),
        .O(LRCLK_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    LRCLK_i_2
       (.I0(Cnt_Lrclk[3]),
        .I1(Cnt_Lrclk[1]),
        .I2(Cnt_Lrclk[0]),
        .I3(Cnt_Lrclk[2]),
        .O(LRCLK_i_2_n_0));
  FDRE LRCLK_reg
       (.C(clk),
        .CE(1'b1),
        .D(LRCLK_i_1_n_0),
        .Q(ac_lrclk),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \L_unsigned_data_prev[3]_i_1 
       (.I0(\L_bus_in_s_reg[17] [12]),
        .O(\L_unsigned_data_prev_reg[7] [0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \L_unsigned_data_prev[4]_i_1 
       (.I0(\L_bus_in_s_reg[17] [12]),
        .I1(\L_bus_in_s_reg[17] [13]),
        .O(\L_unsigned_data_prev_reg[7] [1]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \L_unsigned_data_prev[5]_i_1 
       (.I0(\L_bus_in_s_reg[17] [14]),
        .I1(\L_bus_in_s_reg[17] [13]),
        .I2(\L_bus_in_s_reg[17] [12]),
        .O(\L_unsigned_data_prev_reg[7] [2]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h9555)) 
    \L_unsigned_data_prev[6]_i_1 
       (.I0(\L_bus_in_s_reg[17] [15]),
        .I1(\L_bus_in_s_reg[17] [12]),
        .I2(\L_bus_in_s_reg[17] [13]),
        .I3(\L_bus_in_s_reg[17] [14]),
        .O(\L_unsigned_data_prev_reg[7] [3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hAAAA9555)) 
    \L_unsigned_data_prev[7]_i_1 
       (.I0(\L_bus_in_s_reg[17] [16]),
        .I1(\L_bus_in_s_reg[17] [14]),
        .I2(\L_bus_in_s_reg[17] [13]),
        .I3(\L_bus_in_s_reg[17] [12]),
        .I4(\L_bus_in_s_reg[17] [15]),
        .O(\L_unsigned_data_prev_reg[7] [4]));
  LUT6 #(
    .INIT(64'h5556565656565656)) 
    \L_unsigned_data_prev[8]_i_1 
       (.I0(\L_bus_in_s_reg[17] [17]),
        .I1(\L_bus_in_s_reg[17] [16]),
        .I2(\L_bus_in_s_reg[17] [15]),
        .I3(\L_bus_in_s_reg[17] [12]),
        .I4(\L_bus_in_s_reg[17] [13]),
        .I5(\L_bus_in_s_reg[17] [14]),
        .O(\L_unsigned_data_prev_reg[8] ));
  LUT6 #(
    .INIT(64'h0111111100000000)) 
    \L_unsigned_data_prev[9]_i_1 
       (.I0(\L_bus_in_s_reg[17] [16]),
        .I1(\L_bus_in_s_reg[17] [15]),
        .I2(\L_bus_in_s_reg[17] [12]),
        .I3(\L_bus_in_s_reg[17] [13]),
        .I4(\L_bus_in_s_reg[17] [14]),
        .I5(\L_bus_in_s_reg[17] [17]),
        .O(\L_unsigned_data_prev_reg[9] ));
  LUT1 #(
    .INIT(2'h1)) 
    \R_unsigned_data_prev[3]_i_1 
       (.I0(\R_bus_in_s_reg[17] [12]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \R_unsigned_data_prev[4]_i_1 
       (.I0(\R_bus_in_s_reg[17] [12]),
        .I1(\R_bus_in_s_reg[17] [13]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \R_unsigned_data_prev[5]_i_1 
       (.I0(\R_bus_in_s_reg[17] [14]),
        .I1(\R_bus_in_s_reg[17] [13]),
        .I2(\R_bus_in_s_reg[17] [12]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h9555)) 
    \R_unsigned_data_prev[6]_i_1 
       (.I0(\R_bus_in_s_reg[17] [15]),
        .I1(\R_bus_in_s_reg[17] [12]),
        .I2(\R_bus_in_s_reg[17] [13]),
        .I3(\R_bus_in_s_reg[17] [14]),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hAAAA9555)) 
    \R_unsigned_data_prev[7]_i_1 
       (.I0(\R_bus_in_s_reg[17] [16]),
        .I1(\R_bus_in_s_reg[17] [14]),
        .I2(\R_bus_in_s_reg[17] [13]),
        .I3(\R_bus_in_s_reg[17] [12]),
        .I4(\R_bus_in_s_reg[17] [15]),
        .O(D[4]));
  LUT6 #(
    .INIT(64'h5556565656565656)) 
    \R_unsigned_data_prev[8]_i_1 
       (.I0(\R_bus_in_s_reg[17] [17]),
        .I1(\R_bus_in_s_reg[17] [16]),
        .I2(\R_bus_in_s_reg[17] [15]),
        .I3(\R_bus_in_s_reg[17] [12]),
        .I4(\R_bus_in_s_reg[17] [13]),
        .I5(\R_bus_in_s_reg[17] [14]),
        .O(\R_unsigned_data_prev_reg[8] ));
  LUT6 #(
    .INIT(64'h0111111100000000)) 
    \R_unsigned_data_prev[9]_i_1 
       (.I0(\R_bus_in_s_reg[17] [16]),
        .I1(\R_bus_in_s_reg[17] [15]),
        .I2(\R_bus_in_s_reg[17] [12]),
        .I3(\R_bus_in_s_reg[17] [13]),
        .I4(\R_bus_in_s_reg[17] [14]),
        .I5(\R_bus_in_s_reg[17] [17]),
        .O(\R_unsigned_data_prev_reg[9] ));
  LUT6 #(
    .INIT(64'hEAAA0000FFFFFFFF)) 
    \ac_lrclk_count[3]_i_1 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(E),
        .I5(reset_n),
        .O(\ac_lrclk_count_reg[0] ));
  LUT2 #(
    .INIT(4'h2)) 
    \ac_lrclk_count[3]_i_2 
       (.I0(ac_lrclk),
        .I1(ac_lrclk_sig_prev_reg_0),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hD8)) 
    ac_lrclk_sig_prev_i_1
       (.I0(reset_n),
        .I1(ac_lrclk),
        .I2(ac_lrclk_sig_prev_reg_0),
        .O(ac_lrclk_sig_prev_reg));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_araddr_reg[4] ),
        .I1(\axi_araddr_reg[4]_0 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_1 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[0]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_13 
       (.I0(\R_bus_in_s_reg[17] [2]),
        .I1(\L_bus_in_s_reg[17] [2]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [0]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [0]),
        .O(\axi_rdata[0]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_araddr_reg[4]_26 ),
        .I1(\axi_araddr_reg[4]_27 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_28 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[10]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_12 
       (.I0(\R_bus_in_s_reg[17] [12]),
        .I1(\L_bus_in_s_reg[17] [12]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [10]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [10]),
        .O(\axi_rdata[10]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_araddr_reg[4]_29 ),
        .I1(\axi_araddr_reg[4]_30 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_31 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[11]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_12 
       (.I0(\R_bus_in_s_reg[17] [13]),
        .I1(\L_bus_in_s_reg[17] [13]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [11]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [11]),
        .O(\axi_rdata[11]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_araddr_reg[4]_32 ),
        .I1(\axi_araddr_reg[4]_33 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_34 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[12]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_12 
       (.I0(\R_bus_in_s_reg[17] [14]),
        .I1(\L_bus_in_s_reg[17] [14]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [12]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [12]),
        .O(\axi_rdata[12]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_araddr_reg[4]_35 ),
        .I1(\axi_araddr_reg[4]_36 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_37 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[13]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_12 
       (.I0(\R_bus_in_s_reg[17] [15]),
        .I1(\L_bus_in_s_reg[17] [15]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [13]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [13]),
        .O(\axi_rdata[13]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_araddr_reg[4]_38 ),
        .I1(\axi_araddr_reg[4]_39 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_40 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[14]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_12 
       (.I0(\R_bus_in_s_reg[17] [16]),
        .I1(\L_bus_in_s_reg[17] [16]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [14]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [14]),
        .O(\axi_rdata[14]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_araddr_reg[4]_41 ),
        .I1(\axi_araddr_reg[4]_42 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_43 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[15]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_12 
       (.I0(\R_bus_in_s_reg[17] [17]),
        .I1(\L_bus_in_s_reg[17] [17]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [15]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [15]),
        .O(\axi_rdata[15]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_araddr_reg[4]_2 ),
        .I1(\axi_araddr_reg[4]_3 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_4 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[1]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_12 
       (.I0(\R_bus_in_s_reg[17] [3]),
        .I1(\L_bus_in_s_reg[17] [3]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [1]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [1]),
        .O(\axi_rdata[1]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_13 
       (.I0(\R_bus_in_s_reg[17] [4]),
        .I1(\L_bus_in_s_reg[17] [4]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [2]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [2]),
        .O(\axi_rdata[2]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_araddr_reg[4]_5 ),
        .I1(\axi_araddr_reg[4]_6 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_7 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[3]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_12 
       (.I0(\R_bus_in_s_reg[17] [5]),
        .I1(\L_bus_in_s_reg[17] [5]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [3]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [3]),
        .O(\axi_rdata[3]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_araddr_reg[4]_8 ),
        .I1(\axi_araddr_reg[4]_9 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_10 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[4]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_12 
       (.I0(\R_bus_in_s_reg[17] [6]),
        .I1(\L_bus_in_s_reg[17] [6]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [4]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [4]),
        .O(\axi_rdata[4]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_araddr_reg[4]_11 ),
        .I1(\axi_araddr_reg[4]_12 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_13 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[5]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_12 
       (.I0(\R_bus_in_s_reg[17] [7]),
        .I1(\L_bus_in_s_reg[17] [7]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [5]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [5]),
        .O(\axi_rdata[5]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_araddr_reg[4]_14 ),
        .I1(\axi_araddr_reg[4]_15 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_16 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[6]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_12 
       (.I0(\R_bus_in_s_reg[17] [8]),
        .I1(\L_bus_in_s_reg[17] [8]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [6]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [6]),
        .O(\axi_rdata[6]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_araddr_reg[4]_17 ),
        .I1(\axi_araddr_reg[4]_18 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_19 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[7]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_12 
       (.I0(\R_bus_in_s_reg[17] [9]),
        .I1(\L_bus_in_s_reg[17] [9]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [7]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [7]),
        .O(\axi_rdata[7]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_araddr_reg[4]_20 ),
        .I1(\axi_araddr_reg[4]_21 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_22 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[8]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_12 
       (.I0(\R_bus_in_s_reg[17] [10]),
        .I1(\L_bus_in_s_reg[17] [10]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [8]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [8]),
        .O(\axi_rdata[8]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_araddr_reg[4]_23 ),
        .I1(\axi_araddr_reg[4]_24 ),
        .I2(\axi_araddr_reg[6] [2]),
        .I3(\axi_araddr_reg[4]_25 ),
        .I4(\axi_araddr_reg[6] [1]),
        .I5(\axi_rdata_reg[9]_i_5_n_0 ),
        .O(\axi_rdata_reg[15] [8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_12 
       (.I0(\R_bus_in_s_reg[17] [11]),
        .I1(\L_bus_in_s_reg[17] [11]),
        .I2(\axi_araddr_reg[3]_rep ),
        .I3(\slv_reg5_reg[15] [9]),
        .I4(\axi_araddr_reg[2]_rep ),
        .I5(\slv_reg4_reg[15] [9]),
        .O(\axi_rdata[9]_i_12_n_0 ));
  MUXF7 \axi_rdata_reg[0]_i_5 
       (.I0(\slv_reg3_reg[0] ),
        .I1(\axi_rdata[0]_i_13_n_0 ),
        .O(\axi_rdata_reg[0]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[10]_i_5 
       (.I0(\slv_reg3_reg[10] ),
        .I1(\axi_rdata[10]_i_12_n_0 ),
        .O(\axi_rdata_reg[10]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[11]_i_5 
       (.I0(\slv_reg3_reg[11] ),
        .I1(\axi_rdata[11]_i_12_n_0 ),
        .O(\axi_rdata_reg[11]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[12]_i_5 
       (.I0(\slv_reg3_reg[12] ),
        .I1(\axi_rdata[12]_i_12_n_0 ),
        .O(\axi_rdata_reg[12]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[13]_i_5 
       (.I0(\slv_reg3_reg[13] ),
        .I1(\axi_rdata[13]_i_12_n_0 ),
        .O(\axi_rdata_reg[13]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[14]_i_5 
       (.I0(\slv_reg3_reg[14] ),
        .I1(\axi_rdata[14]_i_12_n_0 ),
        .O(\axi_rdata_reg[14]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[15]_i_5 
       (.I0(\slv_reg3_reg[15] ),
        .I1(\axi_rdata[15]_i_12_n_0 ),
        .O(\axi_rdata_reg[15]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[1]_i_5 
       (.I0(\slv_reg3_reg[1] ),
        .I1(\axi_rdata[1]_i_12_n_0 ),
        .O(\axi_rdata_reg[1]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[2]_i_5 
       (.I0(\slv_reg3_reg[2] ),
        .I1(\axi_rdata[2]_i_13_n_0 ),
        .O(\axi_rdata_reg[2] ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[3]_i_5 
       (.I0(\slv_reg3_reg[3] ),
        .I1(\axi_rdata[3]_i_12_n_0 ),
        .O(\axi_rdata_reg[3]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[4]_i_5 
       (.I0(\slv_reg3_reg[4] ),
        .I1(\axi_rdata[4]_i_12_n_0 ),
        .O(\axi_rdata_reg[4]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[5]_i_5 
       (.I0(\slv_reg3_reg[5] ),
        .I1(\axi_rdata[5]_i_12_n_0 ),
        .O(\axi_rdata_reg[5]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[6]_i_5 
       (.I0(\slv_reg3_reg[6] ),
        .I1(\axi_rdata[6]_i_12_n_0 ),
        .O(\axi_rdata_reg[6]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[7]_i_5 
       (.I0(\slv_reg3_reg[7] ),
        .I1(\axi_rdata[7]_i_12_n_0 ),
        .O(\axi_rdata_reg[7]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[8]_i_5 
       (.I0(\slv_reg3_reg[8] ),
        .I1(\axi_rdata[8]_i_12_n_0 ),
        .O(\axi_rdata_reg[8]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  MUXF7 \axi_rdata_reg[9]_i_5 
       (.I0(\slv_reg3_reg[9] ),
        .I1(\axi_rdata[9]_i_12_n_0 ),
        .O(\axi_rdata_reg[9]_i_5_n_0 ),
        .S(\axi_araddr_reg[6] [0]));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_1
       (.I0(Cnt_Bclk_reg__0[3]),
        .I1(Cnt_Bclk_reg__0[4]),
        .O(i__carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    i__carry_i_2__11
       (.I0(Cnt_Bclk_reg__0[2]),
        .I1(Cnt_Bclk_reg__0[0]),
        .I2(Cnt_Bclk_reg__0[1]),
        .O(i__carry_i_2__11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFF040F0)) 
    ready_sig_i_1
       (.I0(ac_lrclk_sig_prev_reg_0),
        .I1(ac_lrclk),
        .I2(ready_sig_reg_0),
        .I3(reset_n),
        .I4(ready_sig_i_2_n_0),
        .O(ready_sig_reg));
  LUT6 #(
    .INIT(64'h4444444440000000)) 
    ready_sig_i_2
       (.I0(ac_lrclk_sig_prev_reg_0),
        .I1(ac_lrclk),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[1]),
        .I5(Q[3]),
        .O(ready_sig_i_2_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_10__0 
       (.I0(\slv_reg2_reg[9] [0]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\R_bus_in_s_reg[17] [9]),
        .O(DIBDI[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_1__0 
       (.I0(\slv_reg2_reg[9] [9]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\R_unsigned_data_prev_reg[9] ),
        .O(DIBDI[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_2 
       (.I0(\slv_reg2_reg[9] [8]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\R_unsigned_data_prev_reg[8] ),
        .O(DIBDI[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_20 
       (.I0(\slv_reg1_reg[9] [9]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\L_unsigned_data_prev_reg[9] ),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_21 
       (.I0(\slv_reg1_reg[9] [8]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\L_unsigned_data_prev_reg[8] ),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_22 
       (.I0(\slv_reg1_reg[9] [7]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\L_unsigned_data_prev_reg[7] [4]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [7]));
  LUT6 #(
    .INIT(64'hB88B8B8B8B8B8B8B)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_23 
       (.I0(\slv_reg1_reg[9] [6]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\L_bus_in_s_reg[17] [15]),
        .I3(\L_bus_in_s_reg[17] [12]),
        .I4(\L_bus_in_s_reg[17] [13]),
        .I5(\L_bus_in_s_reg[17] [14]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [6]));
  LUT5 #(
    .INIT(32'h8BB8B8B8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_24 
       (.I0(\slv_reg1_reg[9] [5]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\L_bus_in_s_reg[17] [14]),
        .I3(\L_bus_in_s_reg[17] [13]),
        .I4(\L_bus_in_s_reg[17] [12]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [5]));
  LUT4 #(
    .INIT(16'h8BB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_25 
       (.I0(\slv_reg1_reg[9] [4]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\L_bus_in_s_reg[17] [12]),
        .I3(\L_bus_in_s_reg[17] [13]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [4]));
  LUT3 #(
    .INIT(8'h8B)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_26 
       (.I0(\slv_reg1_reg[9] [3]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\L_bus_in_s_reg[17] [12]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_27 
       (.I0(\slv_reg1_reg[9] [2]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\L_bus_in_s_reg[17] [11]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_28 
       (.I0(\slv_reg1_reg[9] [1]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\L_bus_in_s_reg[17] [10]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_29 
       (.I0(\slv_reg1_reg[9] [0]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\L_bus_in_s_reg[17] [9]),
        .O(\sdp_bl.ramb18_dp_bl.ram18_bl [0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_3 
       (.I0(\slv_reg2_reg[9] [7]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(D[4]),
        .O(DIBDI[7]));
  LUT6 #(
    .INIT(64'hB88B8B8B8B8B8B8B)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_4 
       (.I0(\slv_reg2_reg[9] [6]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\R_bus_in_s_reg[17] [15]),
        .I3(\R_bus_in_s_reg[17] [12]),
        .I4(\R_bus_in_s_reg[17] [13]),
        .I5(\R_bus_in_s_reg[17] [14]),
        .O(DIBDI[6]));
  LUT5 #(
    .INIT(32'h8BB8B8B8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_5 
       (.I0(\slv_reg2_reg[9] [5]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\R_bus_in_s_reg[17] [14]),
        .I3(\R_bus_in_s_reg[17] [13]),
        .I4(\R_bus_in_s_reg[17] [12]),
        .O(DIBDI[5]));
  LUT4 #(
    .INIT(16'h8BB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_6 
       (.I0(\slv_reg2_reg[9] [4]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\R_bus_in_s_reg[17] [12]),
        .I3(\R_bus_in_s_reg[17] [13]),
        .O(DIBDI[4]));
  LUT3 #(
    .INIT(8'h8B)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_7 
       (.I0(\slv_reg2_reg[9] [3]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\R_bus_in_s_reg[17] [12]),
        .O(DIBDI[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_8 
       (.I0(\slv_reg2_reg[9] [2]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\R_bus_in_s_reg[17] [11]),
        .O(DIBDI[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_9__0 
       (.I0(\slv_reg2_reg[9] [1]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\R_bus_in_s_reg[17] [10]),
        .O(DIBDI[1]));
  LUT6 #(
    .INIT(64'h1D001D00FF1D1D00)) 
    \state[0]_i_14 
       (.I0(\int_trigger_volt_s_reg[9] [4]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\slv_reg11_reg[9] [4]),
        .I3(\L_unsigned_data_prev_reg[9] ),
        .I4(\L_unsigned_data_prev_reg[8] ),
        .I5(\slv_reg11_reg[8] ),
        .O(\state[0]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'h9A95000000009A95)) 
    \state[0]_i_15 
       (.I0(\L_unsigned_data_prev_reg[9] ),
        .I1(\slv_reg11_reg[9] [4]),
        .I2(\slv_reg5_reg[15] [0]),
        .I3(\int_trigger_volt_s_reg[9] [4]),
        .I4(\L_unsigned_data_prev_reg[8] ),
        .I5(\slv_reg11_reg[8] ),
        .O(\state[0]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h444444D4D4D444D4)) 
    \state[0]_i_19 
       (.I0(\slv_reg11_reg[7] ),
        .I1(D[4]),
        .I2(D[3]),
        .I3(\int_trigger_volt_s_reg[9] [2]),
        .I4(\slv_reg5_reg[15] [0]),
        .I5(\slv_reg11_reg[9] [2]),
        .O(\state[0]_i_19_n_0 ));
  LUT5 #(
    .INIT(32'h56F556A0)) 
    \state[0]_i_1__0 
       (.I0(state[1]),
        .I1(\processQ_reg[9] ),
        .I2(ready_sig_reg_0),
        .I3(state[0]),
        .I4(sw),
        .O(\state_reg[0] ));
  LUT5 #(
    .INIT(32'h8F808080)) 
    \state[0]_i_2 
       (.I0(R_bus_g),
        .I1(CO),
        .I2(switch),
        .I3(L_bus_g),
        .I4(\L_unsigned_data_prev_reg[9]_0 ),
        .O(sw));
  LUT5 #(
    .INIT(32'h15403D54)) 
    \state[0]_i_20 
       (.I0(\slv_reg11_reg[5] ),
        .I1(\R_bus_in_s_reg[17] [12]),
        .I2(\R_bus_in_s_reg[17] [13]),
        .I3(\R_bus_in_s_reg[17] [14]),
        .I4(\slv_reg11_reg[4] ),
        .O(\state[0]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'h1111117171711171)) 
    \state[0]_i_21 
       (.I0(\R_bus_in_s_reg[17] [12]),
        .I1(\slv_reg11_reg[3] ),
        .I2(\R_bus_in_s_reg[17] [11]),
        .I3(\int_trigger_volt_s_reg[9] [1]),
        .I4(\slv_reg5_reg[15] [0]),
        .I5(\slv_reg11_reg[9] [1]),
        .O(\state[0]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'h222222B2B2B222B2)) 
    \state[0]_i_22 
       (.I0(\R_bus_in_s_reg[17] [10]),
        .I1(\slv_reg11_reg[1] ),
        .I2(\R_bus_in_s_reg[17] [9]),
        .I3(\int_trigger_volt_s_reg[9] [0]),
        .I4(\slv_reg5_reg[15] [0]),
        .I5(\slv_reg11_reg[9] [0]),
        .O(\state[0]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'h9A95000000009A95)) 
    \state[0]_i_23 
       (.I0(D[4]),
        .I1(\slv_reg11_reg[9] [3]),
        .I2(\slv_reg5_reg[15] [0]),
        .I3(\int_trigger_volt_s_reg[9] [3]),
        .I4(D[3]),
        .I5(\slv_reg11_reg[6] ),
        .O(\state[0]_i_23_n_0 ));
  LUT5 #(
    .INIT(32'h09906009)) 
    \state[0]_i_24 
       (.I0(\R_bus_in_s_reg[17] [14]),
        .I1(\slv_reg11_reg[5] ),
        .I2(\R_bus_in_s_reg[17] [12]),
        .I3(\R_bus_in_s_reg[17] [13]),
        .I4(\slv_reg11_reg[4] ),
        .O(\state[0]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'h6066600006000666)) 
    \state[0]_i_25 
       (.I0(\slv_reg11_reg[3] ),
        .I1(\R_bus_in_s_reg[17] [12]),
        .I2(\slv_reg11_reg[9] [1]),
        .I3(\slv_reg5_reg[15] [0]),
        .I4(\int_trigger_volt_s_reg[9] [1]),
        .I5(\R_bus_in_s_reg[17] [11]),
        .O(\state[0]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'h9099900009000999)) 
    \state[0]_i_26 
       (.I0(\slv_reg11_reg[1] ),
        .I1(\R_bus_in_s_reg[17] [10]),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(\slv_reg5_reg[15] [0]),
        .I4(\int_trigger_volt_s_reg[9] [0]),
        .I5(\R_bus_in_s_reg[17] [9]),
        .O(\state[0]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'h444444D4D4D444D4)) 
    \state[0]_i_35 
       (.I0(\slv_reg11_reg[7] ),
        .I1(\L_unsigned_data_prev_reg[7] [4]),
        .I2(\L_unsigned_data_prev_reg[7] [3]),
        .I3(\int_trigger_volt_s_reg[9] [2]),
        .I4(\slv_reg5_reg[15] [0]),
        .I5(\slv_reg11_reg[9] [2]),
        .O(\state[0]_i_35_n_0 ));
  LUT5 #(
    .INIT(32'h15403D54)) 
    \state[0]_i_36 
       (.I0(\slv_reg11_reg[5] ),
        .I1(\L_bus_in_s_reg[17] [12]),
        .I2(\L_bus_in_s_reg[17] [13]),
        .I3(\L_bus_in_s_reg[17] [14]),
        .I4(\slv_reg11_reg[4] ),
        .O(\state[0]_i_36_n_0 ));
  LUT6 #(
    .INIT(64'h1111117171711171)) 
    \state[0]_i_37 
       (.I0(\L_bus_in_s_reg[17] [12]),
        .I1(\slv_reg11_reg[3] ),
        .I2(\L_bus_in_s_reg[17] [11]),
        .I3(\int_trigger_volt_s_reg[9] [1]),
        .I4(\slv_reg5_reg[15] [0]),
        .I5(\slv_reg11_reg[9] [1]),
        .O(\state[0]_i_37_n_0 ));
  LUT6 #(
    .INIT(64'h222222B2B2B222B2)) 
    \state[0]_i_38 
       (.I0(\L_bus_in_s_reg[17] [10]),
        .I1(\slv_reg11_reg[1] ),
        .I2(\L_bus_in_s_reg[17] [9]),
        .I3(\int_trigger_volt_s_reg[9] [0]),
        .I4(\slv_reg5_reg[15] [0]),
        .I5(\slv_reg11_reg[9] [0]),
        .O(\state[0]_i_38_n_0 ));
  LUT6 #(
    .INIT(64'h9A95000000009A95)) 
    \state[0]_i_39 
       (.I0(\L_unsigned_data_prev_reg[7] [4]),
        .I1(\slv_reg11_reg[9] [3]),
        .I2(\slv_reg5_reg[15] [0]),
        .I3(\int_trigger_volt_s_reg[9] [3]),
        .I4(\L_unsigned_data_prev_reg[7] [3]),
        .I5(\slv_reg11_reg[6] ),
        .O(\state[0]_i_39_n_0 ));
  LUT5 #(
    .INIT(32'h09906009)) 
    \state[0]_i_40 
       (.I0(\L_bus_in_s_reg[17] [14]),
        .I1(\slv_reg11_reg[5] ),
        .I2(\L_bus_in_s_reg[17] [12]),
        .I3(\L_bus_in_s_reg[17] [13]),
        .I4(\slv_reg11_reg[4] ),
        .O(\state[0]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'h6066600006000666)) 
    \state[0]_i_41 
       (.I0(\slv_reg11_reg[3] ),
        .I1(\L_bus_in_s_reg[17] [12]),
        .I2(\slv_reg11_reg[9] [1]),
        .I3(\slv_reg5_reg[15] [0]),
        .I4(\int_trigger_volt_s_reg[9] [1]),
        .I5(\L_bus_in_s_reg[17] [11]),
        .O(\state[0]_i_41_n_0 ));
  LUT6 #(
    .INIT(64'h9099900009000999)) 
    \state[0]_i_42 
       (.I0(\slv_reg11_reg[1] ),
        .I1(\L_bus_in_s_reg[17] [10]),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(\slv_reg5_reg[15] [0]),
        .I4(\int_trigger_volt_s_reg[9] [0]),
        .I5(\L_bus_in_s_reg[17] [9]),
        .O(\state[0]_i_42_n_0 ));
  LUT6 #(
    .INIT(64'h1D001D00FF1D1D00)) 
    \state[0]_i_8 
       (.I0(\int_trigger_volt_s_reg[9] [4]),
        .I1(\slv_reg5_reg[15] [0]),
        .I2(\slv_reg11_reg[9] [4]),
        .I3(\R_unsigned_data_prev_reg[9] ),
        .I4(\R_unsigned_data_prev_reg[8] ),
        .I5(\slv_reg11_reg[8] ),
        .O(\state[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h9A95000000009A95)) 
    \state[0]_i_9 
       (.I0(\R_unsigned_data_prev_reg[9] ),
        .I1(\slv_reg11_reg[9] [4]),
        .I2(\slv_reg5_reg[15] [0]),
        .I3(\int_trigger_volt_s_reg[9] [4]),
        .I4(\R_unsigned_data_prev_reg[8] ),
        .I5(\slv_reg11_reg[8] ),
        .O(\state[0]_i_9_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \state_reg[0]_i_13 
       (.CI(1'b0),
        .CO({\state_reg[0]_i_13_n_0 ,\state_reg[0]_i_13_n_1 ,\state_reg[0]_i_13_n_2 ,\state_reg[0]_i_13_n_3 }),
        .CYINIT(1'b1),
        .DI({\state[0]_i_35_n_0 ,\state[0]_i_36_n_0 ,\state[0]_i_37_n_0 ,\state[0]_i_38_n_0 }),
        .O(\NLW_state_reg[0]_i_13_O_UNCONNECTED [3:0]),
        .S({\state[0]_i_39_n_0 ,\state[0]_i_40_n_0 ,\state[0]_i_41_n_0 ,\state[0]_i_42_n_0 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \state_reg[0]_i_3 
       (.CI(\state_reg[0]_i_7_n_0 ),
        .CO({\NLW_state_reg[0]_i_3_CO_UNCONNECTED [3:1],R_bus_g}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\state[0]_i_8_n_0 }),
        .O(\NLW_state_reg[0]_i_3_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\state[0]_i_9_n_0 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \state_reg[0]_i_5 
       (.CI(\state_reg[0]_i_13_n_0 ),
        .CO({\NLW_state_reg[0]_i_5_CO_UNCONNECTED [3:1],L_bus_g}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\state[0]_i_14_n_0 }),
        .O(\NLW_state_reg[0]_i_5_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\state[0]_i_15_n_0 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \state_reg[0]_i_7 
       (.CI(1'b0),
        .CO({\state_reg[0]_i_7_n_0 ,\state_reg[0]_i_7_n_1 ,\state_reg[0]_i_7_n_2 ,\state_reg[0]_i_7_n_3 }),
        .CYINIT(1'b1),
        .DI({\state[0]_i_19_n_0 ,\state[0]_i_20_n_0 ,\state[0]_i_21_n_0 ,\state[0]_i_22_n_0 }),
        .O(\NLW_state_reg[0]_i_7_O_UNCONNECTED [3:0]),
        .S({\state[0]_i_23_n_0 ,\state[0]_i_24_n_0 ,\state[0]_i_25_n_0 ,\state[0]_i_26_n_0 }));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath
   (tmds,
    tmdsb,
    ac_mclk,
    SR,
    ac_bclk,
    ac_lrclk,
    ready,
    \state_reg[0] ,
    \Q_reg[2] ,
    D,
    ac_dac_sdata,
    scl,
    sda,
    clk,
    ac_adc_sdata,
    reset_n,
    WREN,
    Q,
    \slv_reg10_reg[9] ,
    btn,
    state,
    \axi_araddr_reg[4] ,
    \axi_araddr_reg[4]_0 ,
    \axi_araddr_reg[6] ,
    \slv_reg3_reg[0] ,
    \slv_reg15_reg[0] ,
    \axi_araddr_reg[3]_rep ,
    \axi_araddr_reg[2]_rep ,
    \slv_reg4_reg[15] ,
    \slv_reg11_reg[9] ,
    \axi_araddr_reg[4]_1 ,
    \axi_araddr_reg[4]_2 ,
    \axi_araddr_reg[4]_3 ,
    \slv_reg3_reg[1] ,
    \axi_araddr_reg[4]_4 ,
    \axi_araddr_reg[4]_5 ,
    \slv_reg3_reg[2] ,
    \slv_reg15_reg[2] ,
    \axi_araddr_reg[4]_6 ,
    \axi_araddr_reg[4]_7 ,
    \axi_araddr_reg[4]_8 ,
    \slv_reg3_reg[3] ,
    \axi_araddr_reg[4]_9 ,
    \axi_araddr_reg[4]_10 ,
    \axi_araddr_reg[4]_11 ,
    \slv_reg3_reg[4] ,
    \axi_araddr_reg[4]_12 ,
    \axi_araddr_reg[4]_13 ,
    \axi_araddr_reg[4]_14 ,
    \slv_reg3_reg[5] ,
    \axi_araddr_reg[4]_15 ,
    \axi_araddr_reg[4]_16 ,
    \axi_araddr_reg[4]_17 ,
    \slv_reg3_reg[6] ,
    \axi_araddr_reg[4]_18 ,
    \axi_araddr_reg[4]_19 ,
    \axi_araddr_reg[4]_20 ,
    \slv_reg3_reg[7] ,
    \axi_araddr_reg[4]_21 ,
    \axi_araddr_reg[4]_22 ,
    \axi_araddr_reg[4]_23 ,
    \slv_reg3_reg[8] ,
    \axi_araddr_reg[4]_24 ,
    \axi_araddr_reg[4]_25 ,
    \axi_araddr_reg[4]_26 ,
    \slv_reg3_reg[9] ,
    \axi_araddr_reg[4]_27 ,
    \axi_araddr_reg[4]_28 ,
    \axi_araddr_reg[4]_29 ,
    \slv_reg3_reg[10] ,
    \axi_araddr_reg[4]_30 ,
    \axi_araddr_reg[4]_31 ,
    \axi_araddr_reg[4]_32 ,
    \slv_reg3_reg[11] ,
    \axi_araddr_reg[4]_33 ,
    \axi_araddr_reg[4]_34 ,
    \axi_araddr_reg[4]_35 ,
    \slv_reg3_reg[12] ,
    \axi_araddr_reg[4]_36 ,
    \axi_araddr_reg[4]_37 ,
    \axi_araddr_reg[4]_38 ,
    \slv_reg3_reg[13] ,
    \axi_araddr_reg[4]_39 ,
    \axi_araddr_reg[4]_40 ,
    \axi_araddr_reg[4]_41 ,
    \slv_reg3_reg[14] ,
    \axi_araddr_reg[4]_42 ,
    \axi_araddr_reg[4]_43 ,
    \axi_araddr_reg[4]_44 ,
    \slv_reg3_reg[15] ,
    switch,
    \slv_reg2_reg[9] ,
    \slv_reg1_reg[9] ,
    \slv_reg0_reg[9] ,
    \slv_reg3_reg[2]_0 ,
    \state_reg[1] ,
    E);
  output [3:0]tmds;
  output [3:0]tmdsb;
  output ac_mclk;
  output [0:0]SR;
  output ac_bclk;
  output ac_lrclk;
  output ready;
  output \state_reg[0] ;
  output [0:0]\Q_reg[2] ;
  output [15:0]D;
  output ac_dac_sdata;
  inout scl;
  inout sda;
  input clk;
  input ac_adc_sdata;
  input reset_n;
  input WREN;
  input [15:0]Q;
  input [9:0]\slv_reg10_reg[9] ;
  input [4:0]btn;
  input [1:0]state;
  input \axi_araddr_reg[4] ;
  input \axi_araddr_reg[4]_0 ;
  input [2:0]\axi_araddr_reg[6] ;
  input \slv_reg3_reg[0] ;
  input \slv_reg15_reg[0] ;
  input \axi_araddr_reg[3]_rep ;
  input \axi_araddr_reg[2]_rep ;
  input [15:0]\slv_reg4_reg[15] ;
  input [9:0]\slv_reg11_reg[9] ;
  input \axi_araddr_reg[4]_1 ;
  input \axi_araddr_reg[4]_2 ;
  input \axi_araddr_reg[4]_3 ;
  input \slv_reg3_reg[1] ;
  input \axi_araddr_reg[4]_4 ;
  input \axi_araddr_reg[4]_5 ;
  input \slv_reg3_reg[2] ;
  input \slv_reg15_reg[2] ;
  input \axi_araddr_reg[4]_6 ;
  input \axi_araddr_reg[4]_7 ;
  input \axi_araddr_reg[4]_8 ;
  input \slv_reg3_reg[3] ;
  input \axi_araddr_reg[4]_9 ;
  input \axi_araddr_reg[4]_10 ;
  input \axi_araddr_reg[4]_11 ;
  input \slv_reg3_reg[4] ;
  input \axi_araddr_reg[4]_12 ;
  input \axi_araddr_reg[4]_13 ;
  input \axi_araddr_reg[4]_14 ;
  input \slv_reg3_reg[5] ;
  input \axi_araddr_reg[4]_15 ;
  input \axi_araddr_reg[4]_16 ;
  input \axi_araddr_reg[4]_17 ;
  input \slv_reg3_reg[6] ;
  input \axi_araddr_reg[4]_18 ;
  input \axi_araddr_reg[4]_19 ;
  input \axi_araddr_reg[4]_20 ;
  input \slv_reg3_reg[7] ;
  input \axi_araddr_reg[4]_21 ;
  input \axi_araddr_reg[4]_22 ;
  input \axi_araddr_reg[4]_23 ;
  input \slv_reg3_reg[8] ;
  input \axi_araddr_reg[4]_24 ;
  input \axi_araddr_reg[4]_25 ;
  input \axi_araddr_reg[4]_26 ;
  input \slv_reg3_reg[9] ;
  input \axi_araddr_reg[4]_27 ;
  input \axi_araddr_reg[4]_28 ;
  input \axi_araddr_reg[4]_29 ;
  input \slv_reg3_reg[10] ;
  input \axi_araddr_reg[4]_30 ;
  input \axi_araddr_reg[4]_31 ;
  input \axi_araddr_reg[4]_32 ;
  input \slv_reg3_reg[11] ;
  input \axi_araddr_reg[4]_33 ;
  input \axi_araddr_reg[4]_34 ;
  input \axi_araddr_reg[4]_35 ;
  input \slv_reg3_reg[12] ;
  input \axi_araddr_reg[4]_36 ;
  input \axi_araddr_reg[4]_37 ;
  input \axi_araddr_reg[4]_38 ;
  input \slv_reg3_reg[13] ;
  input \axi_araddr_reg[4]_39 ;
  input \axi_araddr_reg[4]_40 ;
  input \axi_araddr_reg[4]_41 ;
  input \slv_reg3_reg[14] ;
  input \axi_araddr_reg[4]_42 ;
  input \axi_araddr_reg[4]_43 ;
  input \axi_araddr_reg[4]_44 ;
  input \slv_reg3_reg[15] ;
  input [3:0]switch;
  input [9:0]\slv_reg2_reg[9] ;
  input [9:0]\slv_reg1_reg[9] ;
  input [9:0]\slv_reg0_reg[9] ;
  input [1:0]\slv_reg3_reg[2]_0 ;
  input [0:0]\state_reg[1] ;
  input [0:0]E;

  wire [15:0]D;
  wire [9:0]DI;
  wire [0:0]E;
  wire [17:0]L_bus_in;
  wire L_bus_l;
  wire [1:0]L_bus_out_s;
  wire [2:0]L_out_bram;
  wire [9:0]L_unsigned_data_prev;
  wire [15:0]Lbus_out;
  wire [15:0]Q;
  wire [0:0]\Q_reg[2] ;
  wire [17:0]R_bus_in;
  wire R_bus_l;
  wire [1:0]R_bus_out_s;
  wire [9:0]R_unsigned_data_prev;
  wire [15:0]Rbus_out;
  wire [0:0]SR;
  wire [9:0]WRADDR;
  wire WREN;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire audio_codec_n_24;
  wire audio_codec_n_25;
  wire audio_codec_n_26;
  wire audio_codec_n_27;
  wire audio_codec_n_28;
  wire audio_codec_n_29;
  wire audio_codec_n_30;
  wire audio_codec_n_49;
  wire audio_codec_n_5;
  wire audio_codec_n_50;
  wire audio_codec_n_51;
  wire audio_codec_n_52;
  wire audio_codec_n_53;
  wire audio_codec_n_54;
  wire audio_codec_n_71;
  wire audio_codec_n_72;
  wire audio_codec_n_73;
  wire audio_codec_n_74;
  wire audio_codec_n_75;
  wire audio_codec_n_76;
  wire audio_codec_n_77;
  wire audio_codec_n_78;
  wire audio_codec_n_79;
  wire audio_codec_n_80;
  wire audio_codec_n_81;
  wire \axi_araddr_reg[2]_rep ;
  wire \axi_araddr_reg[3]_rep ;
  wire \axi_araddr_reg[4] ;
  wire \axi_araddr_reg[4]_0 ;
  wire \axi_araddr_reg[4]_1 ;
  wire \axi_araddr_reg[4]_10 ;
  wire \axi_araddr_reg[4]_11 ;
  wire \axi_araddr_reg[4]_12 ;
  wire \axi_araddr_reg[4]_13 ;
  wire \axi_araddr_reg[4]_14 ;
  wire \axi_araddr_reg[4]_15 ;
  wire \axi_araddr_reg[4]_16 ;
  wire \axi_araddr_reg[4]_17 ;
  wire \axi_araddr_reg[4]_18 ;
  wire \axi_araddr_reg[4]_19 ;
  wire \axi_araddr_reg[4]_2 ;
  wire \axi_araddr_reg[4]_20 ;
  wire \axi_araddr_reg[4]_21 ;
  wire \axi_araddr_reg[4]_22 ;
  wire \axi_araddr_reg[4]_23 ;
  wire \axi_araddr_reg[4]_24 ;
  wire \axi_araddr_reg[4]_25 ;
  wire \axi_araddr_reg[4]_26 ;
  wire \axi_araddr_reg[4]_27 ;
  wire \axi_araddr_reg[4]_28 ;
  wire \axi_araddr_reg[4]_29 ;
  wire \axi_araddr_reg[4]_3 ;
  wire \axi_araddr_reg[4]_30 ;
  wire \axi_araddr_reg[4]_31 ;
  wire \axi_araddr_reg[4]_32 ;
  wire \axi_araddr_reg[4]_33 ;
  wire \axi_araddr_reg[4]_34 ;
  wire \axi_araddr_reg[4]_35 ;
  wire \axi_araddr_reg[4]_36 ;
  wire \axi_araddr_reg[4]_37 ;
  wire \axi_araddr_reg[4]_38 ;
  wire \axi_araddr_reg[4]_39 ;
  wire \axi_araddr_reg[4]_4 ;
  wire \axi_araddr_reg[4]_40 ;
  wire \axi_araddr_reg[4]_41 ;
  wire \axi_araddr_reg[4]_42 ;
  wire \axi_araddr_reg[4]_43 ;
  wire \axi_araddr_reg[4]_44 ;
  wire \axi_araddr_reg[4]_5 ;
  wire \axi_araddr_reg[4]_6 ;
  wire \axi_araddr_reg[4]_7 ;
  wire \axi_araddr_reg[4]_8 ;
  wire \axi_araddr_reg[4]_9 ;
  wire [2:0]\axi_araddr_reg[6] ;
  wire [4:0]btn;
  wire ch1;
  wire ch2;
  wire clk;
  wire \clock_divider[0]_i_2_n_0 ;
  wire [22:21]clock_divider_reg;
  wire \clock_divider_reg[0]_i_1_n_0 ;
  wire \clock_divider_reg[0]_i_1_n_1 ;
  wire \clock_divider_reg[0]_i_1_n_2 ;
  wire \clock_divider_reg[0]_i_1_n_3 ;
  wire \clock_divider_reg[0]_i_1_n_4 ;
  wire \clock_divider_reg[0]_i_1_n_5 ;
  wire \clock_divider_reg[0]_i_1_n_6 ;
  wire \clock_divider_reg[0]_i_1_n_7 ;
  wire \clock_divider_reg[12]_i_1_n_0 ;
  wire \clock_divider_reg[12]_i_1_n_1 ;
  wire \clock_divider_reg[12]_i_1_n_2 ;
  wire \clock_divider_reg[12]_i_1_n_3 ;
  wire \clock_divider_reg[12]_i_1_n_4 ;
  wire \clock_divider_reg[12]_i_1_n_5 ;
  wire \clock_divider_reg[12]_i_1_n_6 ;
  wire \clock_divider_reg[12]_i_1_n_7 ;
  wire \clock_divider_reg[16]_i_1_n_0 ;
  wire \clock_divider_reg[16]_i_1_n_1 ;
  wire \clock_divider_reg[16]_i_1_n_2 ;
  wire \clock_divider_reg[16]_i_1_n_3 ;
  wire \clock_divider_reg[16]_i_1_n_4 ;
  wire \clock_divider_reg[16]_i_1_n_5 ;
  wire \clock_divider_reg[16]_i_1_n_6 ;
  wire \clock_divider_reg[16]_i_1_n_7 ;
  wire \clock_divider_reg[20]_i_1_n_2 ;
  wire \clock_divider_reg[20]_i_1_n_3 ;
  wire \clock_divider_reg[20]_i_1_n_5 ;
  wire \clock_divider_reg[20]_i_1_n_6 ;
  wire \clock_divider_reg[20]_i_1_n_7 ;
  wire \clock_divider_reg[4]_i_1_n_0 ;
  wire \clock_divider_reg[4]_i_1_n_1 ;
  wire \clock_divider_reg[4]_i_1_n_2 ;
  wire \clock_divider_reg[4]_i_1_n_3 ;
  wire \clock_divider_reg[4]_i_1_n_4 ;
  wire \clock_divider_reg[4]_i_1_n_5 ;
  wire \clock_divider_reg[4]_i_1_n_6 ;
  wire \clock_divider_reg[4]_i_1_n_7 ;
  wire \clock_divider_reg[8]_i_1_n_0 ;
  wire \clock_divider_reg[8]_i_1_n_1 ;
  wire \clock_divider_reg[8]_i_1_n_2 ;
  wire \clock_divider_reg[8]_i_1_n_3 ;
  wire \clock_divider_reg[8]_i_1_n_4 ;
  wire \clock_divider_reg[8]_i_1_n_5 ;
  wire \clock_divider_reg[8]_i_1_n_6 ;
  wire \clock_divider_reg[8]_i_1_n_7 ;
  wire \clock_divider_reg_n_0_[0] ;
  wire \clock_divider_reg_n_0_[10] ;
  wire \clock_divider_reg_n_0_[11] ;
  wire \clock_divider_reg_n_0_[12] ;
  wire \clock_divider_reg_n_0_[13] ;
  wire \clock_divider_reg_n_0_[14] ;
  wire \clock_divider_reg_n_0_[15] ;
  wire \clock_divider_reg_n_0_[16] ;
  wire \clock_divider_reg_n_0_[17] ;
  wire \clock_divider_reg_n_0_[18] ;
  wire \clock_divider_reg_n_0_[19] ;
  wire \clock_divider_reg_n_0_[1] ;
  wire \clock_divider_reg_n_0_[20] ;
  wire \clock_divider_reg_n_0_[2] ;
  wire \clock_divider_reg_n_0_[3] ;
  wire \clock_divider_reg_n_0_[4] ;
  wire \clock_divider_reg_n_0_[5] ;
  wire \clock_divider_reg_n_0_[6] ;
  wire \clock_divider_reg_n_0_[7] ;
  wire \clock_divider_reg_n_0_[8] ;
  wire \clock_divider_reg_n_0_[9] ;
  wire [1:0]column;
  wire [0:0]flagQ;
  wire \int_trigger_time_s[0]_i_1_n_0 ;
  wire \int_trigger_time_s[4]_i_2_n_0 ;
  wire \int_trigger_time_s[4]_i_3_n_0 ;
  wire \int_trigger_time_s[4]_i_4_n_0 ;
  wire \int_trigger_time_s[4]_i_5_n_0 ;
  wire \int_trigger_time_s[4]_i_6_n_0 ;
  wire \int_trigger_time_s[8]_i_2_n_0 ;
  wire \int_trigger_time_s[8]_i_3_n_0 ;
  wire \int_trigger_time_s[8]_i_4_n_0 ;
  wire \int_trigger_time_s[8]_i_5_n_0 ;
  wire \int_trigger_time_s[9]_i_1_n_0 ;
  wire \int_trigger_time_s[9]_i_2_n_0 ;
  wire \int_trigger_time_s[9]_i_5_n_0 ;
  wire \int_trigger_time_s[9]_i_6_n_0 ;
  wire \int_trigger_time_s[9]_i_7_n_0 ;
  wire \int_trigger_time_s[9]_i_8_n_0 ;
  wire \int_trigger_time_s_reg[4]_i_1_n_0 ;
  wire \int_trigger_time_s_reg[4]_i_1_n_1 ;
  wire \int_trigger_time_s_reg[4]_i_1_n_2 ;
  wire \int_trigger_time_s_reg[4]_i_1_n_3 ;
  wire \int_trigger_time_s_reg[4]_i_1_n_4 ;
  wire \int_trigger_time_s_reg[4]_i_1_n_5 ;
  wire \int_trigger_time_s_reg[4]_i_1_n_6 ;
  wire \int_trigger_time_s_reg[4]_i_1_n_7 ;
  wire \int_trigger_time_s_reg[8]_i_1_n_0 ;
  wire \int_trigger_time_s_reg[8]_i_1_n_1 ;
  wire \int_trigger_time_s_reg[8]_i_1_n_2 ;
  wire \int_trigger_time_s_reg[8]_i_1_n_3 ;
  wire \int_trigger_time_s_reg[8]_i_1_n_4 ;
  wire \int_trigger_time_s_reg[8]_i_1_n_5 ;
  wire \int_trigger_time_s_reg[8]_i_1_n_6 ;
  wire \int_trigger_time_s_reg[8]_i_1_n_7 ;
  wire \int_trigger_time_s_reg[9]_i_3_n_7 ;
  wire [9:0]int_trigger_time_s_reg__0;
  wire \int_trigger_volt_s[0]_i_1_n_0 ;
  wire \int_trigger_volt_s[4]_i_2_n_0 ;
  wire \int_trigger_volt_s[4]_i_3_n_0 ;
  wire \int_trigger_volt_s[4]_i_4_n_0 ;
  wire \int_trigger_volt_s[4]_i_5_n_0 ;
  wire \int_trigger_volt_s[4]_i_6_n_0 ;
  wire \int_trigger_volt_s[8]_i_2_n_0 ;
  wire \int_trigger_volt_s[8]_i_3_n_0 ;
  wire \int_trigger_volt_s[8]_i_4_n_0 ;
  wire \int_trigger_volt_s[8]_i_5_n_0 ;
  wire \int_trigger_volt_s[9]_i_1_n_0 ;
  wire \int_trigger_volt_s[9]_i_2_n_0 ;
  wire \int_trigger_volt_s[9]_i_4_n_0 ;
  wire \int_trigger_volt_s[9]_i_5_n_0 ;
  wire \int_trigger_volt_s[9]_i_6_n_0 ;
  wire \int_trigger_volt_s[9]_i_7_n_0 ;
  wire \int_trigger_volt_s_reg[4]_i_1_n_0 ;
  wire \int_trigger_volt_s_reg[4]_i_1_n_1 ;
  wire \int_trigger_volt_s_reg[4]_i_1_n_2 ;
  wire \int_trigger_volt_s_reg[4]_i_1_n_3 ;
  wire \int_trigger_volt_s_reg[4]_i_1_n_4 ;
  wire \int_trigger_volt_s_reg[4]_i_1_n_5 ;
  wire \int_trigger_volt_s_reg[4]_i_1_n_6 ;
  wire \int_trigger_volt_s_reg[4]_i_1_n_7 ;
  wire \int_trigger_volt_s_reg[8]_i_1_n_0 ;
  wire \int_trigger_volt_s_reg[8]_i_1_n_1 ;
  wire \int_trigger_volt_s_reg[8]_i_1_n_2 ;
  wire \int_trigger_volt_s_reg[8]_i_1_n_3 ;
  wire \int_trigger_volt_s_reg[8]_i_1_n_4 ;
  wire \int_trigger_volt_s_reg[8]_i_1_n_5 ;
  wire \int_trigger_volt_s_reg[8]_i_1_n_6 ;
  wire \int_trigger_volt_s_reg[8]_i_1_n_7 ;
  wire \int_trigger_volt_s_reg[9]_i_3_n_7 ;
  wire [9:0]int_trigger_volt_s_reg__0;
  wire lBRAM_n_3;
  wire lopt;
  wire rBRAM_n_0;
  wire ready;
  wire reset_n;
  wire [9:0]row;
  wire scl;
  wire sda;
  wire [9:0]\slv_reg0_reg[9] ;
  wire [9:0]\slv_reg10_reg[9] ;
  wire [9:0]\slv_reg11_reg[9] ;
  wire \slv_reg15_reg[0] ;
  wire \slv_reg15_reg[2] ;
  wire [9:0]\slv_reg1_reg[9] ;
  wire [9:0]\slv_reg2_reg[9] ;
  wire \slv_reg3_reg[0] ;
  wire \slv_reg3_reg[10] ;
  wire \slv_reg3_reg[11] ;
  wire \slv_reg3_reg[12] ;
  wire \slv_reg3_reg[13] ;
  wire \slv_reg3_reg[14] ;
  wire \slv_reg3_reg[15] ;
  wire \slv_reg3_reg[1] ;
  wire \slv_reg3_reg[2] ;
  wire [1:0]\slv_reg3_reg[2]_0 ;
  wire \slv_reg3_reg[3] ;
  wire \slv_reg3_reg[4] ;
  wire \slv_reg3_reg[5] ;
  wire \slv_reg3_reg[6] ;
  wire \slv_reg3_reg[7] ;
  wire \slv_reg3_reg[8] ;
  wire \slv_reg3_reg[9] ;
  wire [15:0]\slv_reg4_reg[15] ;
  wire [1:0]state;
  wire \state[0]_i_11_n_0 ;
  wire \state[0]_i_12_n_0 ;
  wire \state[0]_i_17_n_0 ;
  wire \state[0]_i_18_n_0 ;
  wire \state[0]_i_27_n_0 ;
  wire \state[0]_i_28_n_0 ;
  wire \state[0]_i_29_n_0 ;
  wire \state[0]_i_30_n_0 ;
  wire \state[0]_i_31_n_0 ;
  wire \state[0]_i_32_n_0 ;
  wire \state[0]_i_33_n_0 ;
  wire \state[0]_i_34_n_0 ;
  wire \state[0]_i_43_n_0 ;
  wire \state[0]_i_44_n_0 ;
  wire \state[0]_i_45_n_0 ;
  wire \state[0]_i_46_n_0 ;
  wire \state[0]_i_47_n_0 ;
  wire \state[0]_i_48_n_0 ;
  wire \state[0]_i_49_n_0 ;
  wire \state[0]_i_50_n_0 ;
  wire \state_reg[0] ;
  wire \state_reg[0]_i_10_n_0 ;
  wire \state_reg[0]_i_10_n_1 ;
  wire \state_reg[0]_i_10_n_2 ;
  wire \state_reg[0]_i_10_n_3 ;
  wire \state_reg[0]_i_16_n_0 ;
  wire \state_reg[0]_i_16_n_1 ;
  wire \state_reg[0]_i_16_n_2 ;
  wire \state_reg[0]_i_16_n_3 ;
  wire [0:0]\state_reg[1] ;
  wire [3:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;
  wire trigger_clock;
  wire video_inst_n_10;
  wire video_inst_n_11;
  wire video_inst_n_12;
  wire video_inst_n_13;
  wire video_inst_n_14;
  wire video_inst_n_15;
  wire video_inst_n_16;
  wire video_inst_n_29;
  wire video_inst_n_30;
  wire video_inst_n_31;
  wire video_inst_n_32;
  wire video_inst_n_33;
  wire video_inst_n_34;
  wire video_inst_n_35;
  wire video_inst_n_36;
  wire video_inst_n_37;
  wire video_inst_n_38;
  wire video_inst_n_39;
  wire video_inst_n_40;
  wire video_inst_n_41;
  wire video_inst_n_42;
  wire video_inst_n_43;
  wire video_inst_n_44;
  wire video_inst_n_45;
  wire video_inst_n_46;
  wire video_inst_n_8;
  wire video_inst_n_9;
  wire [3:2]\NLW_clock_divider_reg[20]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_clock_divider_reg[20]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_int_trigger_time_s_reg[9]_i_3_CO_UNCONNECTED ;
  wire [3:1]\NLW_int_trigger_time_s_reg[9]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_int_trigger_volt_s_reg[9]_i_3_CO_UNCONNECTED ;
  wire [3:1]\NLW_int_trigger_volt_s_reg[9]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_state_reg[0]_i_10_O_UNCONNECTED ;
  wire [3:0]\NLW_state_reg[0]_i_16_O_UNCONNECTED ;
  wire [3:1]\NLW_state_reg[0]_i_4_CO_UNCONNECTED ;
  wire [3:0]\NLW_state_reg[0]_i_4_O_UNCONNECTED ;
  wire [3:1]\NLW_state_reg[0]_i_6_CO_UNCONNECTED ;
  wire [3:0]\NLW_state_reg[0]_i_6_O_UNCONNECTED ;

  FDRE \L_bus_in_s_reg[0] 
       (.C(clk),
        .CE(ready),
        .D(L_bus_out_s[0]),
        .Q(L_bus_in[0]),
        .R(SR));
  FDRE \L_bus_in_s_reg[10] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[8]),
        .Q(L_bus_in[10]),
        .R(SR));
  FDRE \L_bus_in_s_reg[11] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[9]),
        .Q(L_bus_in[11]),
        .R(SR));
  FDRE \L_bus_in_s_reg[12] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[10]),
        .Q(L_bus_in[12]),
        .R(SR));
  FDRE \L_bus_in_s_reg[13] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[11]),
        .Q(L_bus_in[13]),
        .R(SR));
  FDRE \L_bus_in_s_reg[14] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[12]),
        .Q(L_bus_in[14]),
        .R(SR));
  FDRE \L_bus_in_s_reg[15] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[13]),
        .Q(L_bus_in[15]),
        .R(SR));
  FDRE \L_bus_in_s_reg[16] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[14]),
        .Q(L_bus_in[16]),
        .R(SR));
  FDRE \L_bus_in_s_reg[17] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[15]),
        .Q(L_bus_in[17]),
        .R(SR));
  FDRE \L_bus_in_s_reg[1] 
       (.C(clk),
        .CE(ready),
        .D(L_bus_out_s[1]),
        .Q(L_bus_in[1]),
        .R(SR));
  FDRE \L_bus_in_s_reg[2] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[0]),
        .Q(L_bus_in[2]),
        .R(SR));
  FDRE \L_bus_in_s_reg[3] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[1]),
        .Q(L_bus_in[3]),
        .R(SR));
  FDRE \L_bus_in_s_reg[4] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[2]),
        .Q(L_bus_in[4]),
        .R(SR));
  FDRE \L_bus_in_s_reg[5] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[3]),
        .Q(L_bus_in[5]),
        .R(SR));
  FDRE \L_bus_in_s_reg[6] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[4]),
        .Q(L_bus_in[6]),
        .R(SR));
  FDRE \L_bus_in_s_reg[7] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[5]),
        .Q(L_bus_in[7]),
        .R(SR));
  FDRE \L_bus_in_s_reg[8] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[6]),
        .Q(L_bus_in[8]),
        .R(SR));
  FDRE \L_bus_in_s_reg[9] 
       (.C(clk),
        .CE(ready),
        .D(Lbus_out[7]),
        .Q(L_bus_in[9]),
        .R(SR));
  FDRE \L_unsigned_data_prev_reg[0] 
       (.C(ready),
        .CE(1'b1),
        .D(Lbus_out[7]),
        .Q(L_unsigned_data_prev[0]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[1] 
       (.C(ready),
        .CE(1'b1),
        .D(Lbus_out[8]),
        .Q(L_unsigned_data_prev[1]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[2] 
       (.C(ready),
        .CE(1'b1),
        .D(Lbus_out[9]),
        .Q(L_unsigned_data_prev[2]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[3] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_54),
        .Q(L_unsigned_data_prev[3]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[4] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_53),
        .Q(L_unsigned_data_prev[4]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[5] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_52),
        .Q(L_unsigned_data_prev[5]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[6] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_51),
        .Q(L_unsigned_data_prev[6]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[7] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_50),
        .Q(L_unsigned_data_prev[7]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[8] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_49),
        .Q(L_unsigned_data_prev[8]),
        .R(1'b0));
  FDRE \L_unsigned_data_prev_reg[9] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_30),
        .Q(L_unsigned_data_prev[9]),
        .R(1'b0));
  FDRE \R_bus_in_s_reg[0] 
       (.C(clk),
        .CE(ready),
        .D(R_bus_out_s[0]),
        .Q(R_bus_in[0]),
        .R(SR));
  FDRE \R_bus_in_s_reg[10] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[8]),
        .Q(R_bus_in[10]),
        .R(SR));
  FDRE \R_bus_in_s_reg[11] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[9]),
        .Q(R_bus_in[11]),
        .R(SR));
  FDRE \R_bus_in_s_reg[12] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[10]),
        .Q(R_bus_in[12]),
        .R(SR));
  FDRE \R_bus_in_s_reg[13] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[11]),
        .Q(R_bus_in[13]),
        .R(SR));
  FDRE \R_bus_in_s_reg[14] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[12]),
        .Q(R_bus_in[14]),
        .R(SR));
  FDRE \R_bus_in_s_reg[15] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[13]),
        .Q(R_bus_in[15]),
        .R(SR));
  FDRE \R_bus_in_s_reg[16] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[14]),
        .Q(R_bus_in[16]),
        .R(SR));
  FDRE \R_bus_in_s_reg[17] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[15]),
        .Q(R_bus_in[17]),
        .R(SR));
  FDRE \R_bus_in_s_reg[1] 
       (.C(clk),
        .CE(ready),
        .D(R_bus_out_s[1]),
        .Q(R_bus_in[1]),
        .R(SR));
  FDRE \R_bus_in_s_reg[2] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[0]),
        .Q(R_bus_in[2]),
        .R(SR));
  FDRE \R_bus_in_s_reg[3] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[1]),
        .Q(R_bus_in[3]),
        .R(SR));
  FDRE \R_bus_in_s_reg[4] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[2]),
        .Q(R_bus_in[4]),
        .R(SR));
  FDRE \R_bus_in_s_reg[5] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[3]),
        .Q(R_bus_in[5]),
        .R(SR));
  FDRE \R_bus_in_s_reg[6] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[4]),
        .Q(R_bus_in[6]),
        .R(SR));
  FDRE \R_bus_in_s_reg[7] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[5]),
        .Q(R_bus_in[7]),
        .R(SR));
  FDRE \R_bus_in_s_reg[8] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[6]),
        .Q(R_bus_in[8]),
        .R(SR));
  FDRE \R_bus_in_s_reg[9] 
       (.C(clk),
        .CE(ready),
        .D(Rbus_out[7]),
        .Q(R_bus_in[9]),
        .R(SR));
  FDRE \R_unsigned_data_prev_reg[0] 
       (.C(ready),
        .CE(1'b1),
        .D(Rbus_out[7]),
        .Q(R_unsigned_data_prev[0]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[1] 
       (.C(ready),
        .CE(1'b1),
        .D(Rbus_out[8]),
        .Q(R_unsigned_data_prev[1]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[2] 
       (.C(ready),
        .CE(1'b1),
        .D(Rbus_out[9]),
        .Q(R_unsigned_data_prev[2]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[3] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_29),
        .Q(R_unsigned_data_prev[3]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[4] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_28),
        .Q(R_unsigned_data_prev[4]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[5] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_27),
        .Q(R_unsigned_data_prev[5]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[6] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_26),
        .Q(R_unsigned_data_prev[6]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[7] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_25),
        .Q(R_unsigned_data_prev[7]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[8] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_24),
        .Q(R_unsigned_data_prev[8]),
        .R(1'b0));
  FDRE \R_unsigned_data_prev_reg[9] 
       (.C(ready),
        .CE(1'b1),
        .D(audio_codec_n_5),
        .Q(R_unsigned_data_prev[9]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper audio_codec
       (.BCLK_int_reg(SR),
        .CLK(ready),
        .CO(R_bus_l),
        .D({audio_codec_n_25,audio_codec_n_26,audio_codec_n_27,audio_codec_n_28,audio_codec_n_29}),
        .DIBDI({audio_codec_n_72,audio_codec_n_73,audio_codec_n_74,audio_codec_n_75,audio_codec_n_76,audio_codec_n_77,audio_codec_n_78,audio_codec_n_79,audio_codec_n_80,audio_codec_n_81}),
        .\L_bus_in_s_reg[17] ({Lbus_out,L_bus_out_s}),
        .\L_bus_in_s_reg[17]_0 (L_bus_in),
        .\L_unsigned_data_prev_reg[7] ({audio_codec_n_50,audio_codec_n_51,audio_codec_n_52,audio_codec_n_53,audio_codec_n_54}),
        .\L_unsigned_data_prev_reg[8] (audio_codec_n_49),
        .\L_unsigned_data_prev_reg[9] (audio_codec_n_30),
        .\L_unsigned_data_prev_reg[9]_0 (L_bus_l),
        .Q({Rbus_out,R_bus_out_s}),
        .\R_bus_in_s_reg[17] (R_bus_in),
        .\R_unsigned_data_prev_reg[8] (audio_codec_n_24),
        .\R_unsigned_data_prev_reg[9] (audio_codec_n_5),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .\axi_araddr_reg[2]_rep (\axi_araddr_reg[2]_rep ),
        .\axi_araddr_reg[3]_rep (\axi_araddr_reg[3]_rep ),
        .\axi_araddr_reg[4] (\axi_araddr_reg[4] ),
        .\axi_araddr_reg[4]_0 (\axi_araddr_reg[4]_0 ),
        .\axi_araddr_reg[4]_1 (\axi_araddr_reg[4]_1 ),
        .\axi_araddr_reg[4]_10 (\axi_araddr_reg[4]_12 ),
        .\axi_araddr_reg[4]_11 (\axi_araddr_reg[4]_13 ),
        .\axi_araddr_reg[4]_12 (\axi_araddr_reg[4]_14 ),
        .\axi_araddr_reg[4]_13 (\axi_araddr_reg[4]_15 ),
        .\axi_araddr_reg[4]_14 (\axi_araddr_reg[4]_16 ),
        .\axi_araddr_reg[4]_15 (\axi_araddr_reg[4]_17 ),
        .\axi_araddr_reg[4]_16 (\axi_araddr_reg[4]_18 ),
        .\axi_araddr_reg[4]_17 (\axi_araddr_reg[4]_19 ),
        .\axi_araddr_reg[4]_18 (\axi_araddr_reg[4]_20 ),
        .\axi_araddr_reg[4]_19 (\axi_araddr_reg[4]_21 ),
        .\axi_araddr_reg[4]_2 (\axi_araddr_reg[4]_2 ),
        .\axi_araddr_reg[4]_20 (\axi_araddr_reg[4]_22 ),
        .\axi_araddr_reg[4]_21 (\axi_araddr_reg[4]_23 ),
        .\axi_araddr_reg[4]_22 (\axi_araddr_reg[4]_24 ),
        .\axi_araddr_reg[4]_23 (\axi_araddr_reg[4]_25 ),
        .\axi_araddr_reg[4]_24 (\axi_araddr_reg[4]_26 ),
        .\axi_araddr_reg[4]_25 (\axi_araddr_reg[4]_27 ),
        .\axi_araddr_reg[4]_26 (\axi_araddr_reg[4]_28 ),
        .\axi_araddr_reg[4]_27 (\axi_araddr_reg[4]_29 ),
        .\axi_araddr_reg[4]_28 (\axi_araddr_reg[4]_30 ),
        .\axi_araddr_reg[4]_29 (\axi_araddr_reg[4]_31 ),
        .\axi_araddr_reg[4]_3 (\axi_araddr_reg[4]_3 ),
        .\axi_araddr_reg[4]_30 (\axi_araddr_reg[4]_32 ),
        .\axi_araddr_reg[4]_31 (\axi_araddr_reg[4]_33 ),
        .\axi_araddr_reg[4]_32 (\axi_araddr_reg[4]_34 ),
        .\axi_araddr_reg[4]_33 (\axi_araddr_reg[4]_35 ),
        .\axi_araddr_reg[4]_34 (\axi_araddr_reg[4]_36 ),
        .\axi_araddr_reg[4]_35 (\axi_araddr_reg[4]_37 ),
        .\axi_araddr_reg[4]_36 (\axi_araddr_reg[4]_38 ),
        .\axi_araddr_reg[4]_37 (\axi_araddr_reg[4]_39 ),
        .\axi_araddr_reg[4]_38 (\axi_araddr_reg[4]_40 ),
        .\axi_araddr_reg[4]_39 (\axi_araddr_reg[4]_41 ),
        .\axi_araddr_reg[4]_4 (\axi_araddr_reg[4]_6 ),
        .\axi_araddr_reg[4]_40 (\axi_araddr_reg[4]_42 ),
        .\axi_araddr_reg[4]_41 (\axi_araddr_reg[4]_43 ),
        .\axi_araddr_reg[4]_42 (\axi_araddr_reg[4]_44 ),
        .\axi_araddr_reg[4]_5 (\axi_araddr_reg[4]_7 ),
        .\axi_araddr_reg[4]_6 (\axi_araddr_reg[4]_8 ),
        .\axi_araddr_reg[4]_7 (\axi_araddr_reg[4]_9 ),
        .\axi_araddr_reg[4]_8 (\axi_araddr_reg[4]_10 ),
        .\axi_araddr_reg[4]_9 (\axi_araddr_reg[4]_11 ),
        .\axi_araddr_reg[6] (\axi_araddr_reg[6] ),
        .\axi_rdata_reg[15] ({D[15:3],D[1:0]}),
        .\axi_rdata_reg[2] (audio_codec_n_71),
        .clk(clk),
        .flagQ(flagQ),
        .\int_trigger_volt_s_reg[9] ({int_trigger_volt_s_reg__0[9],int_trigger_volt_s_reg__0[7:6],int_trigger_volt_s_reg__0[2],int_trigger_volt_s_reg__0[0]}),
        .lopt(lopt),
        .\processQ_reg[9] (\Q_reg[2] ),
        .reset_n(reset_n),
        .scl(scl),
        .sda(sda),
        .\sdp_bl.ramb18_dp_bl.ram18_bl (DI),
        .\slv_reg10_reg[0] (\slv_reg10_reg[9] [0]),
        .\slv_reg11_reg[1] (video_inst_n_39),
        .\slv_reg11_reg[3] (video_inst_n_44),
        .\slv_reg11_reg[4] (video_inst_n_45),
        .\slv_reg11_reg[5] (video_inst_n_43),
        .\slv_reg11_reg[6] (video_inst_n_46),
        .\slv_reg11_reg[7] (video_inst_n_42),
        .\slv_reg11_reg[8] (video_inst_n_41),
        .\slv_reg11_reg[9] ({\slv_reg11_reg[9] [9],\slv_reg11_reg[9] [7:6],\slv_reg11_reg[9] [2],\slv_reg11_reg[9] [0]}),
        .\slv_reg15_reg[0] (\slv_reg15_reg[0] ),
        .\slv_reg1_reg[9] (\slv_reg1_reg[9] ),
        .\slv_reg2_reg[9] (\slv_reg2_reg[9] ),
        .\slv_reg3_reg[0] (\slv_reg3_reg[0] ),
        .\slv_reg3_reg[10] (\slv_reg3_reg[10] ),
        .\slv_reg3_reg[11] (\slv_reg3_reg[11] ),
        .\slv_reg3_reg[12] (\slv_reg3_reg[12] ),
        .\slv_reg3_reg[13] (\slv_reg3_reg[13] ),
        .\slv_reg3_reg[14] (\slv_reg3_reg[14] ),
        .\slv_reg3_reg[15] (\slv_reg3_reg[15] ),
        .\slv_reg3_reg[1] (\slv_reg3_reg[1] ),
        .\slv_reg3_reg[2] (\slv_reg3_reg[2] ),
        .\slv_reg3_reg[3] (\slv_reg3_reg[3] ),
        .\slv_reg3_reg[4] (\slv_reg3_reg[4] ),
        .\slv_reg3_reg[5] (\slv_reg3_reg[5] ),
        .\slv_reg3_reg[6] (\slv_reg3_reg[6] ),
        .\slv_reg3_reg[7] (\slv_reg3_reg[7] ),
        .\slv_reg3_reg[8] (\slv_reg3_reg[8] ),
        .\slv_reg3_reg[9] (\slv_reg3_reg[9] ),
        .\slv_reg4_reg[15] (\slv_reg4_reg[15] ),
        .\slv_reg5_reg[15] (Q),
        .state(state),
        .\state_reg[0] (\state_reg[0] ),
        .switch(switch[3]));
  LUT1 #(
    .INIT(2'h1)) 
    \clock_divider[0]_i_2 
       (.I0(\clock_divider_reg_n_0_[0] ),
        .O(\clock_divider[0]_i_2_n_0 ));
  FDRE \clock_divider_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[0]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[0] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\clock_divider_reg[0]_i_1_n_0 ,\clock_divider_reg[0]_i_1_n_1 ,\clock_divider_reg[0]_i_1_n_2 ,\clock_divider_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\clock_divider_reg[0]_i_1_n_4 ,\clock_divider_reg[0]_i_1_n_5 ,\clock_divider_reg[0]_i_1_n_6 ,\clock_divider_reg[0]_i_1_n_7 }),
        .S({\clock_divider_reg_n_0_[3] ,\clock_divider_reg_n_0_[2] ,\clock_divider_reg_n_0_[1] ,\clock_divider[0]_i_2_n_0 }));
  FDRE \clock_divider_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[8]_i_1_n_5 ),
        .Q(\clock_divider_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \clock_divider_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[8]_i_1_n_4 ),
        .Q(\clock_divider_reg_n_0_[11] ),
        .R(1'b0));
  FDRE \clock_divider_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[12]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[12] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[12]_i_1 
       (.CI(\clock_divider_reg[8]_i_1_n_0 ),
        .CO({\clock_divider_reg[12]_i_1_n_0 ,\clock_divider_reg[12]_i_1_n_1 ,\clock_divider_reg[12]_i_1_n_2 ,\clock_divider_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clock_divider_reg[12]_i_1_n_4 ,\clock_divider_reg[12]_i_1_n_5 ,\clock_divider_reg[12]_i_1_n_6 ,\clock_divider_reg[12]_i_1_n_7 }),
        .S({\clock_divider_reg_n_0_[15] ,\clock_divider_reg_n_0_[14] ,\clock_divider_reg_n_0_[13] ,\clock_divider_reg_n_0_[12] }));
  FDRE \clock_divider_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[12]_i_1_n_6 ),
        .Q(\clock_divider_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \clock_divider_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[12]_i_1_n_5 ),
        .Q(\clock_divider_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \clock_divider_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[12]_i_1_n_4 ),
        .Q(\clock_divider_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \clock_divider_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[16]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[16] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[16]_i_1 
       (.CI(\clock_divider_reg[12]_i_1_n_0 ),
        .CO({\clock_divider_reg[16]_i_1_n_0 ,\clock_divider_reg[16]_i_1_n_1 ,\clock_divider_reg[16]_i_1_n_2 ,\clock_divider_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clock_divider_reg[16]_i_1_n_4 ,\clock_divider_reg[16]_i_1_n_5 ,\clock_divider_reg[16]_i_1_n_6 ,\clock_divider_reg[16]_i_1_n_7 }),
        .S({\clock_divider_reg_n_0_[19] ,\clock_divider_reg_n_0_[18] ,\clock_divider_reg_n_0_[17] ,\clock_divider_reg_n_0_[16] }));
  FDRE \clock_divider_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[16]_i_1_n_6 ),
        .Q(\clock_divider_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \clock_divider_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[16]_i_1_n_5 ),
        .Q(\clock_divider_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \clock_divider_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[16]_i_1_n_4 ),
        .Q(\clock_divider_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \clock_divider_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[0]_i_1_n_6 ),
        .Q(\clock_divider_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \clock_divider_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[20]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[20] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[20]_i_1 
       (.CI(\clock_divider_reg[16]_i_1_n_0 ),
        .CO({\NLW_clock_divider_reg[20]_i_1_CO_UNCONNECTED [3:2],\clock_divider_reg[20]_i_1_n_2 ,\clock_divider_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_clock_divider_reg[20]_i_1_O_UNCONNECTED [3],\clock_divider_reg[20]_i_1_n_5 ,\clock_divider_reg[20]_i_1_n_6 ,\clock_divider_reg[20]_i_1_n_7 }),
        .S({1'b0,clock_divider_reg,\clock_divider_reg_n_0_[20] }));
  FDRE \clock_divider_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[20]_i_1_n_6 ),
        .Q(clock_divider_reg[21]),
        .R(1'b0));
  FDRE \clock_divider_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[20]_i_1_n_5 ),
        .Q(clock_divider_reg[22]),
        .R(1'b0));
  FDRE \clock_divider_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[0]_i_1_n_5 ),
        .Q(\clock_divider_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \clock_divider_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[0]_i_1_n_4 ),
        .Q(\clock_divider_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \clock_divider_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[4]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[4] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[4]_i_1 
       (.CI(\clock_divider_reg[0]_i_1_n_0 ),
        .CO({\clock_divider_reg[4]_i_1_n_0 ,\clock_divider_reg[4]_i_1_n_1 ,\clock_divider_reg[4]_i_1_n_2 ,\clock_divider_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clock_divider_reg[4]_i_1_n_4 ,\clock_divider_reg[4]_i_1_n_5 ,\clock_divider_reg[4]_i_1_n_6 ,\clock_divider_reg[4]_i_1_n_7 }),
        .S({\clock_divider_reg_n_0_[7] ,\clock_divider_reg_n_0_[6] ,\clock_divider_reg_n_0_[5] ,\clock_divider_reg_n_0_[4] }));
  FDRE \clock_divider_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[4]_i_1_n_6 ),
        .Q(\clock_divider_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \clock_divider_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[4]_i_1_n_5 ),
        .Q(\clock_divider_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \clock_divider_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[4]_i_1_n_4 ),
        .Q(\clock_divider_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \clock_divider_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[8]_i_1_n_7 ),
        .Q(\clock_divider_reg_n_0_[8] ),
        .R(1'b0));
  CARRY4 \clock_divider_reg[8]_i_1 
       (.CI(\clock_divider_reg[4]_i_1_n_0 ),
        .CO({\clock_divider_reg[8]_i_1_n_0 ,\clock_divider_reg[8]_i_1_n_1 ,\clock_divider_reg[8]_i_1_n_2 ,\clock_divider_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clock_divider_reg[8]_i_1_n_4 ,\clock_divider_reg[8]_i_1_n_5 ,\clock_divider_reg[8]_i_1_n_6 ,\clock_divider_reg[8]_i_1_n_7 }),
        .S({\clock_divider_reg_n_0_[11] ,\clock_divider_reg_n_0_[10] ,\clock_divider_reg_n_0_[9] ,\clock_divider_reg_n_0_[8] }));
  FDRE \clock_divider_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\clock_divider_reg[8]_i_1_n_6 ),
        .Q(\clock_divider_reg_n_0_[9] ),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_flagRegister flagReg
       (.CLK(ready),
        .D(D[2]),
        .\Q_reg[0]_0 (flagQ),
        .\axi_araddr_reg[2]_rep (\axi_araddr_reg[2]_rep ),
        .\axi_araddr_reg[3]_rep (\axi_araddr_reg[3]_rep ),
        .\axi_araddr_reg[4] (\axi_araddr_reg[4]_4 ),
        .\axi_araddr_reg[4]_0 (\axi_araddr_reg[4]_5 ),
        .\axi_araddr_reg[4]_1 (audio_codec_n_71),
        .\axi_araddr_reg[6] (\axi_araddr_reg[6] ),
        .clk(clk),
        .\processQ_reg[9] (\Q_reg[2] ),
        .reset_n(SR),
        .\slv_reg10_reg[2] (\slv_reg10_reg[9] [2]),
        .\slv_reg11_reg[2] (\slv_reg11_reg[9] [2]),
        .\slv_reg15_reg[2] (\slv_reg15_reg[2] ),
        .\slv_reg3_reg[2] (\slv_reg3_reg[2]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \int_trigger_time_s[0]_i_1 
       (.I0(int_trigger_time_s_reg__0[0]),
        .O(\int_trigger_time_s[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \int_trigger_time_s[4]_i_2 
       (.I0(int_trigger_time_s_reg__0[1]),
        .O(\int_trigger_time_s[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_time_s[4]_i_3 
       (.I0(int_trigger_time_s_reg__0[3]),
        .I1(int_trigger_time_s_reg__0[4]),
        .O(\int_trigger_time_s[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_time_s[4]_i_4 
       (.I0(int_trigger_time_s_reg__0[2]),
        .I1(int_trigger_time_s_reg__0[3]),
        .O(\int_trigger_time_s[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_time_s[4]_i_5 
       (.I0(int_trigger_time_s_reg__0[1]),
        .I1(int_trigger_time_s_reg__0[2]),
        .O(\int_trigger_time_s[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \int_trigger_time_s[4]_i_6 
       (.I0(int_trigger_time_s_reg__0[1]),
        .I1(\int_trigger_time_s[9]_i_5_n_0 ),
        .O(\int_trigger_time_s[4]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_time_s[8]_i_2 
       (.I0(int_trigger_time_s_reg__0[7]),
        .I1(int_trigger_time_s_reg__0[8]),
        .O(\int_trigger_time_s[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_time_s[8]_i_3 
       (.I0(int_trigger_time_s_reg__0[6]),
        .I1(int_trigger_time_s_reg__0[7]),
        .O(\int_trigger_time_s[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_time_s[8]_i_4 
       (.I0(int_trigger_time_s_reg__0[5]),
        .I1(int_trigger_time_s_reg__0[6]),
        .O(\int_trigger_time_s[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_time_s[8]_i_5 
       (.I0(int_trigger_time_s_reg__0[4]),
        .I1(int_trigger_time_s_reg__0[5]),
        .O(\int_trigger_time_s[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h04550000FFFFFFFF)) 
    \int_trigger_time_s[9]_i_1 
       (.I0(\int_trigger_time_s[9]_i_5_n_0 ),
        .I1(int_trigger_time_s_reg__0[9]),
        .I2(\int_trigger_time_s[9]_i_6_n_0 ),
        .I3(btn[3]),
        .I4(btn[4]),
        .I5(reset_n),
        .O(\int_trigger_time_s[9]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFBAA)) 
    \int_trigger_time_s[9]_i_2 
       (.I0(\int_trigger_time_s[9]_i_5_n_0 ),
        .I1(int_trigger_time_s_reg__0[9]),
        .I2(\int_trigger_time_s[9]_i_6_n_0 ),
        .I3(btn[3]),
        .O(\int_trigger_time_s[9]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \int_trigger_time_s[9]_i_4 
       (.I0(clock_divider_reg[21]),
        .I1(switch[2]),
        .I2(clock_divider_reg[22]),
        .O(trigger_clock));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    \int_trigger_time_s[9]_i_5 
       (.I0(btn[1]),
        .I1(\int_trigger_time_s[9]_i_8_n_0 ),
        .I2(video_inst_n_32),
        .I3(video_inst_n_29),
        .I4(video_inst_n_31),
        .I5(video_inst_n_34),
        .O(\int_trigger_time_s[9]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000005777)) 
    \int_trigger_time_s[9]_i_6 
       (.I0(int_trigger_time_s_reg__0[6]),
        .I1(int_trigger_time_s_reg__0[5]),
        .I2(int_trigger_time_s_reg__0[4]),
        .I3(int_trigger_time_s_reg__0[3]),
        .I4(int_trigger_time_s_reg__0[8]),
        .I5(int_trigger_time_s_reg__0[7]),
        .O(\int_trigger_time_s[9]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_time_s[9]_i_7 
       (.I0(int_trigger_time_s_reg__0[8]),
        .I1(int_trigger_time_s_reg__0[9]),
        .O(\int_trigger_time_s[9]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \int_trigger_time_s[9]_i_8 
       (.I0(video_inst_n_35),
        .I1(video_inst_n_36),
        .I2(video_inst_n_8),
        .I3(video_inst_n_37),
        .I4(video_inst_n_30),
        .I5(video_inst_n_33),
        .O(\int_trigger_time_s[9]_i_8_n_0 ));
  FDRE \int_trigger_time_s_reg[0] 
       (.C(trigger_clock),
        .CE(\int_trigger_time_s[9]_i_2_n_0 ),
        .D(\int_trigger_time_s[0]_i_1_n_0 ),
        .Q(int_trigger_time_s_reg__0[0]),
        .R(\int_trigger_time_s[9]_i_1_n_0 ));
  FDRE \int_trigger_time_s_reg[1] 
       (.C(trigger_clock),
        .CE(\int_trigger_time_s[9]_i_2_n_0 ),
        .D(\int_trigger_time_s_reg[4]_i_1_n_7 ),
        .Q(int_trigger_time_s_reg__0[1]),
        .R(\int_trigger_time_s[9]_i_1_n_0 ));
  FDSE \int_trigger_time_s_reg[2] 
       (.C(trigger_clock),
        .CE(\int_trigger_time_s[9]_i_2_n_0 ),
        .D(\int_trigger_time_s_reg[4]_i_1_n_6 ),
        .Q(int_trigger_time_s_reg__0[2]),
        .S(\int_trigger_time_s[9]_i_1_n_0 ));
  FDSE \int_trigger_time_s_reg[3] 
       (.C(trigger_clock),
        .CE(\int_trigger_time_s[9]_i_2_n_0 ),
        .D(\int_trigger_time_s_reg[4]_i_1_n_5 ),
        .Q(int_trigger_time_s_reg__0[3]),
        .S(\int_trigger_time_s[9]_i_1_n_0 ));
  FDRE \int_trigger_time_s_reg[4] 
       (.C(trigger_clock),
        .CE(\int_trigger_time_s[9]_i_2_n_0 ),
        .D(\int_trigger_time_s_reg[4]_i_1_n_4 ),
        .Q(int_trigger_time_s_reg__0[4]),
        .R(\int_trigger_time_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \int_trigger_time_s_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\int_trigger_time_s_reg[4]_i_1_n_0 ,\int_trigger_time_s_reg[4]_i_1_n_1 ,\int_trigger_time_s_reg[4]_i_1_n_2 ,\int_trigger_time_s_reg[4]_i_1_n_3 }),
        .CYINIT(int_trigger_time_s_reg__0[0]),
        .DI({int_trigger_time_s_reg__0[3:1],\int_trigger_time_s[4]_i_2_n_0 }),
        .O({\int_trigger_time_s_reg[4]_i_1_n_4 ,\int_trigger_time_s_reg[4]_i_1_n_5 ,\int_trigger_time_s_reg[4]_i_1_n_6 ,\int_trigger_time_s_reg[4]_i_1_n_7 }),
        .S({\int_trigger_time_s[4]_i_3_n_0 ,\int_trigger_time_s[4]_i_4_n_0 ,\int_trigger_time_s[4]_i_5_n_0 ,\int_trigger_time_s[4]_i_6_n_0 }));
  FDSE \int_trigger_time_s_reg[5] 
       (.C(trigger_clock),
        .CE(\int_trigger_time_s[9]_i_2_n_0 ),
        .D(\int_trigger_time_s_reg[8]_i_1_n_7 ),
        .Q(int_trigger_time_s_reg__0[5]),
        .S(\int_trigger_time_s[9]_i_1_n_0 ));
  FDRE \int_trigger_time_s_reg[6] 
       (.C(trigger_clock),
        .CE(\int_trigger_time_s[9]_i_2_n_0 ),
        .D(\int_trigger_time_s_reg[8]_i_1_n_6 ),
        .Q(int_trigger_time_s_reg__0[6]),
        .R(\int_trigger_time_s[9]_i_1_n_0 ));
  FDRE \int_trigger_time_s_reg[7] 
       (.C(trigger_clock),
        .CE(\int_trigger_time_s[9]_i_2_n_0 ),
        .D(\int_trigger_time_s_reg[8]_i_1_n_5 ),
        .Q(int_trigger_time_s_reg__0[7]),
        .R(\int_trigger_time_s[9]_i_1_n_0 ));
  FDSE \int_trigger_time_s_reg[8] 
       (.C(trigger_clock),
        .CE(\int_trigger_time_s[9]_i_2_n_0 ),
        .D(\int_trigger_time_s_reg[8]_i_1_n_4 ),
        .Q(int_trigger_time_s_reg__0[8]),
        .S(\int_trigger_time_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \int_trigger_time_s_reg[8]_i_1 
       (.CI(\int_trigger_time_s_reg[4]_i_1_n_0 ),
        .CO({\int_trigger_time_s_reg[8]_i_1_n_0 ,\int_trigger_time_s_reg[8]_i_1_n_1 ,\int_trigger_time_s_reg[8]_i_1_n_2 ,\int_trigger_time_s_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(int_trigger_time_s_reg__0[7:4]),
        .O({\int_trigger_time_s_reg[8]_i_1_n_4 ,\int_trigger_time_s_reg[8]_i_1_n_5 ,\int_trigger_time_s_reg[8]_i_1_n_6 ,\int_trigger_time_s_reg[8]_i_1_n_7 }),
        .S({\int_trigger_time_s[8]_i_2_n_0 ,\int_trigger_time_s[8]_i_3_n_0 ,\int_trigger_time_s[8]_i_4_n_0 ,\int_trigger_time_s[8]_i_5_n_0 }));
  FDRE \int_trigger_time_s_reg[9] 
       (.C(trigger_clock),
        .CE(\int_trigger_time_s[9]_i_2_n_0 ),
        .D(\int_trigger_time_s_reg[9]_i_3_n_7 ),
        .Q(int_trigger_time_s_reg__0[9]),
        .R(\int_trigger_time_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \int_trigger_time_s_reg[9]_i_3 
       (.CI(\int_trigger_time_s_reg[8]_i_1_n_0 ),
        .CO(\NLW_int_trigger_time_s_reg[9]_i_3_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_int_trigger_time_s_reg[9]_i_3_O_UNCONNECTED [3:1],\int_trigger_time_s_reg[9]_i_3_n_7 }),
        .S({1'b0,1'b0,1'b0,\int_trigger_time_s[9]_i_7_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \int_trigger_volt_s[0]_i_1 
       (.I0(int_trigger_volt_s_reg__0[0]),
        .O(\int_trigger_volt_s[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \int_trigger_volt_s[4]_i_2 
       (.I0(int_trigger_volt_s_reg__0[1]),
        .O(\int_trigger_volt_s[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_volt_s[4]_i_3 
       (.I0(int_trigger_volt_s_reg__0[3]),
        .I1(int_trigger_volt_s_reg__0[4]),
        .O(\int_trigger_volt_s[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_volt_s[4]_i_4 
       (.I0(int_trigger_volt_s_reg__0[2]),
        .I1(int_trigger_volt_s_reg__0[3]),
        .O(\int_trigger_volt_s[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_volt_s[4]_i_5 
       (.I0(int_trigger_volt_s_reg__0[1]),
        .I1(int_trigger_volt_s_reg__0[2]),
        .O(\int_trigger_volt_s[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \int_trigger_volt_s[4]_i_6 
       (.I0(int_trigger_volt_s_reg__0[1]),
        .I1(\int_trigger_volt_s[9]_i_4_n_0 ),
        .O(\int_trigger_volt_s[4]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_volt_s[8]_i_2 
       (.I0(int_trigger_volt_s_reg__0[7]),
        .I1(int_trigger_volt_s_reg__0[8]),
        .O(\int_trigger_volt_s[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_volt_s[8]_i_3 
       (.I0(int_trigger_volt_s_reg__0[6]),
        .I1(int_trigger_volt_s_reg__0[7]),
        .O(\int_trigger_volt_s[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_volt_s[8]_i_4 
       (.I0(int_trigger_volt_s_reg__0[5]),
        .I1(int_trigger_volt_s_reg__0[6]),
        .O(\int_trigger_volt_s[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_volt_s[8]_i_5 
       (.I0(int_trigger_volt_s_reg__0[4]),
        .I1(int_trigger_volt_s_reg__0[5]),
        .O(\int_trigger_volt_s[8]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h4F)) 
    \int_trigger_volt_s[9]_i_1 
       (.I0(\int_trigger_volt_s[9]_i_2_n_0 ),
        .I1(btn[4]),
        .I2(reset_n),
        .O(\int_trigger_volt_s[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBAAABABABABABABA)) 
    \int_trigger_volt_s[9]_i_2 
       (.I0(\int_trigger_volt_s[9]_i_4_n_0 ),
        .I1(int_trigger_volt_s_reg__0[9]),
        .I2(btn[2]),
        .I3(\int_trigger_volt_s[9]_i_5_n_0 ),
        .I4(int_trigger_volt_s_reg__0[8]),
        .I5(int_trigger_volt_s_reg__0[7]),
        .O(\int_trigger_volt_s[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA8AAAAAAAA)) 
    \int_trigger_volt_s[9]_i_4 
       (.I0(btn[0]),
        .I1(\int_trigger_volt_s[9]_i_7_n_0 ),
        .I2(int_trigger_volt_s_reg__0[1]),
        .I3(int_trigger_volt_s_reg__0[3]),
        .I4(int_trigger_volt_s_reg__0[2]),
        .I5(\int_trigger_volt_s[9]_i_5_n_0 ),
        .O(\int_trigger_volt_s[9]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \int_trigger_volt_s[9]_i_5 
       (.I0(int_trigger_volt_s_reg__0[6]),
        .I1(int_trigger_volt_s_reg__0[5]),
        .I2(int_trigger_volt_s_reg__0[4]),
        .O(\int_trigger_volt_s[9]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \int_trigger_volt_s[9]_i_6 
       (.I0(int_trigger_volt_s_reg__0[8]),
        .I1(int_trigger_volt_s_reg__0[9]),
        .O(\int_trigger_volt_s[9]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \int_trigger_volt_s[9]_i_7 
       (.I0(int_trigger_volt_s_reg__0[9]),
        .I1(int_trigger_volt_s_reg__0[8]),
        .I2(int_trigger_volt_s_reg__0[0]),
        .I3(int_trigger_volt_s_reg__0[7]),
        .O(\int_trigger_volt_s[9]_i_7_n_0 ));
  FDRE \int_trigger_volt_s_reg[0] 
       (.C(trigger_clock),
        .CE(\int_trigger_volt_s[9]_i_2_n_0 ),
        .D(\int_trigger_volt_s[0]_i_1_n_0 ),
        .Q(int_trigger_volt_s_reg__0[0]),
        .R(\int_trigger_volt_s[9]_i_1_n_0 ));
  FDRE \int_trigger_volt_s_reg[1] 
       (.C(trigger_clock),
        .CE(\int_trigger_volt_s[9]_i_2_n_0 ),
        .D(\int_trigger_volt_s_reg[4]_i_1_n_7 ),
        .Q(int_trigger_volt_s_reg__0[1]),
        .R(\int_trigger_volt_s[9]_i_1_n_0 ));
  FDRE \int_trigger_volt_s_reg[2] 
       (.C(trigger_clock),
        .CE(\int_trigger_volt_s[9]_i_2_n_0 ),
        .D(\int_trigger_volt_s_reg[4]_i_1_n_6 ),
        .Q(int_trigger_volt_s_reg__0[2]),
        .R(\int_trigger_volt_s[9]_i_1_n_0 ));
  FDSE \int_trigger_volt_s_reg[3] 
       (.C(trigger_clock),
        .CE(\int_trigger_volt_s[9]_i_2_n_0 ),
        .D(\int_trigger_volt_s_reg[4]_i_1_n_5 ),
        .Q(int_trigger_volt_s_reg__0[3]),
        .S(\int_trigger_volt_s[9]_i_1_n_0 ));
  FDRE \int_trigger_volt_s_reg[4] 
       (.C(trigger_clock),
        .CE(\int_trigger_volt_s[9]_i_2_n_0 ),
        .D(\int_trigger_volt_s_reg[4]_i_1_n_4 ),
        .Q(int_trigger_volt_s_reg__0[4]),
        .R(\int_trigger_volt_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \int_trigger_volt_s_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\int_trigger_volt_s_reg[4]_i_1_n_0 ,\int_trigger_volt_s_reg[4]_i_1_n_1 ,\int_trigger_volt_s_reg[4]_i_1_n_2 ,\int_trigger_volt_s_reg[4]_i_1_n_3 }),
        .CYINIT(int_trigger_volt_s_reg__0[0]),
        .DI({int_trigger_volt_s_reg__0[3:1],\int_trigger_volt_s[4]_i_2_n_0 }),
        .O({\int_trigger_volt_s_reg[4]_i_1_n_4 ,\int_trigger_volt_s_reg[4]_i_1_n_5 ,\int_trigger_volt_s_reg[4]_i_1_n_6 ,\int_trigger_volt_s_reg[4]_i_1_n_7 }),
        .S({\int_trigger_volt_s[4]_i_3_n_0 ,\int_trigger_volt_s[4]_i_4_n_0 ,\int_trigger_volt_s[4]_i_5_n_0 ,\int_trigger_volt_s[4]_i_6_n_0 }));
  FDRE \int_trigger_volt_s_reg[5] 
       (.C(trigger_clock),
        .CE(\int_trigger_volt_s[9]_i_2_n_0 ),
        .D(\int_trigger_volt_s_reg[8]_i_1_n_7 ),
        .Q(int_trigger_volt_s_reg__0[5]),
        .R(\int_trigger_volt_s[9]_i_1_n_0 ));
  FDSE \int_trigger_volt_s_reg[6] 
       (.C(trigger_clock),
        .CE(\int_trigger_volt_s[9]_i_2_n_0 ),
        .D(\int_trigger_volt_s_reg[8]_i_1_n_6 ),
        .Q(int_trigger_volt_s_reg__0[6]),
        .S(\int_trigger_volt_s[9]_i_1_n_0 ));
  FDSE \int_trigger_volt_s_reg[7] 
       (.C(trigger_clock),
        .CE(\int_trigger_volt_s[9]_i_2_n_0 ),
        .D(\int_trigger_volt_s_reg[8]_i_1_n_5 ),
        .Q(int_trigger_volt_s_reg__0[7]),
        .S(\int_trigger_volt_s[9]_i_1_n_0 ));
  FDRE \int_trigger_volt_s_reg[8] 
       (.C(trigger_clock),
        .CE(\int_trigger_volt_s[9]_i_2_n_0 ),
        .D(\int_trigger_volt_s_reg[8]_i_1_n_4 ),
        .Q(int_trigger_volt_s_reg__0[8]),
        .R(\int_trigger_volt_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \int_trigger_volt_s_reg[8]_i_1 
       (.CI(\int_trigger_volt_s_reg[4]_i_1_n_0 ),
        .CO({\int_trigger_volt_s_reg[8]_i_1_n_0 ,\int_trigger_volt_s_reg[8]_i_1_n_1 ,\int_trigger_volt_s_reg[8]_i_1_n_2 ,\int_trigger_volt_s_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(int_trigger_volt_s_reg__0[7:4]),
        .O({\int_trigger_volt_s_reg[8]_i_1_n_4 ,\int_trigger_volt_s_reg[8]_i_1_n_5 ,\int_trigger_volt_s_reg[8]_i_1_n_6 ,\int_trigger_volt_s_reg[8]_i_1_n_7 }),
        .S({\int_trigger_volt_s[8]_i_2_n_0 ,\int_trigger_volt_s[8]_i_3_n_0 ,\int_trigger_volt_s[8]_i_4_n_0 ,\int_trigger_volt_s[8]_i_5_n_0 }));
  FDRE \int_trigger_volt_s_reg[9] 
       (.C(trigger_clock),
        .CE(\int_trigger_volt_s[9]_i_2_n_0 ),
        .D(\int_trigger_volt_s_reg[9]_i_3_n_7 ),
        .Q(int_trigger_volt_s_reg__0[9]),
        .R(\int_trigger_volt_s[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \int_trigger_volt_s_reg[9]_i_3 
       (.CI(\int_trigger_volt_s_reg[8]_i_1_n_0 ),
        .CO(\NLW_int_trigger_volt_s_reg[9]_i_3_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_int_trigger_volt_s_reg[9]_i_3_O_UNCONNECTED [3:1],\int_trigger_volt_s_reg[9]_i_3_n_7 }),
        .S({1'b0,1'b0,1'b0,\int_trigger_volt_s[9]_i_6_n_0 }));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO lBRAM
       (.ADDRARDADDR({video_inst_n_9,video_inst_n_10,video_inst_n_11,video_inst_n_12,video_inst_n_13,video_inst_n_14,video_inst_n_15,video_inst_n_16,column}),
        .ADDRBWRADDR(WRADDR),
        .CO(ch1),
        .DOADO(L_out_bram),
        .Q(row[9:3]),
        .S(video_inst_n_40),
        .WREN(WREN),
        .clk(clk),
        .\encoded_reg[8] (lBRAM_n_3),
        .\processQ_reg[8] (video_inst_n_38),
        .reset_n(SR),
        .\slv_reg1_reg[9] (DI),
        .switch(switch[0]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0 rBRAM
       (.ADDRARDADDR({video_inst_n_9,video_inst_n_10,video_inst_n_11,video_inst_n_12,video_inst_n_13,video_inst_n_14,video_inst_n_15,video_inst_n_16,column}),
        .ADDRBWRADDR(WRADDR),
        .CO(ch2),
        .DIBDI({audio_codec_n_72,audio_codec_n_73,audio_codec_n_74,audio_codec_n_75,audio_codec_n_76,audio_codec_n_77,audio_codec_n_78,audio_codec_n_79,audio_codec_n_80,audio_codec_n_81}),
        .Q(row),
        .WREN(WREN),
        .clk(clk),
        .\encoded_reg[8] (rBRAM_n_0),
        .reset_n(SR),
        .switch(switch[1]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample_counter sampCounter
       (.ADDRBWRADDR(WRADDR),
        .E(E),
        .Q(Q[0]),
        .\Q_reg[2] (\Q_reg[2] ),
        .clk(clk),
        .\slv_reg0_reg[9] (\slv_reg0_reg[9] ),
        .\state_reg[1] (\state_reg[1] ));
  LUT6 #(
    .INIT(64'h54045404FD5D5404)) 
    \state[0]_i_11 
       (.I0(R_unsigned_data_prev[9]),
        .I1(int_trigger_volt_s_reg__0[9]),
        .I2(Q[0]),
        .I3(\slv_reg11_reg[9] [9]),
        .I4(video_inst_n_41),
        .I5(R_unsigned_data_prev[8]),
        .O(\state[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB84700000000B847)) 
    \state[0]_i_12 
       (.I0(\slv_reg11_reg[9] [9]),
        .I1(Q[0]),
        .I2(int_trigger_volt_s_reg__0[9]),
        .I3(R_unsigned_data_prev[9]),
        .I4(video_inst_n_41),
        .I5(R_unsigned_data_prev[8]),
        .O(\state[0]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h54045404FD5D5404)) 
    \state[0]_i_17 
       (.I0(L_unsigned_data_prev[9]),
        .I1(int_trigger_volt_s_reg__0[9]),
        .I2(Q[0]),
        .I3(\slv_reg11_reg[9] [9]),
        .I4(video_inst_n_41),
        .I5(L_unsigned_data_prev[8]),
        .O(\state[0]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hB84700000000B847)) 
    \state[0]_i_18 
       (.I0(\slv_reg11_reg[9] [9]),
        .I1(Q[0]),
        .I2(int_trigger_volt_s_reg__0[9]),
        .I3(L_unsigned_data_prev[9]),
        .I4(video_inst_n_41),
        .I5(L_unsigned_data_prev[8]),
        .O(\state[0]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'h44444444DDD444D4)) 
    \state[0]_i_27 
       (.I0(R_unsigned_data_prev[7]),
        .I1(video_inst_n_42),
        .I2(int_trigger_volt_s_reg__0[6]),
        .I3(Q[0]),
        .I4(\slv_reg11_reg[9] [6]),
        .I5(R_unsigned_data_prev[6]),
        .O(\state[0]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'h44444444DDD444D4)) 
    \state[0]_i_28 
       (.I0(R_unsigned_data_prev[5]),
        .I1(video_inst_n_43),
        .I2(int_trigger_volt_s_reg__0[4]),
        .I3(Q[0]),
        .I4(\slv_reg11_reg[9] [4]),
        .I5(R_unsigned_data_prev[4]),
        .O(\state[0]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'h44444444DDD444D4)) 
    \state[0]_i_29 
       (.I0(R_unsigned_data_prev[3]),
        .I1(video_inst_n_44),
        .I2(int_trigger_volt_s_reg__0[2]),
        .I3(Q[0]),
        .I4(\slv_reg11_reg[9] [2]),
        .I5(R_unsigned_data_prev[2]),
        .O(\state[0]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'h44444444DDD444D4)) 
    \state[0]_i_30 
       (.I0(R_unsigned_data_prev[1]),
        .I1(video_inst_n_39),
        .I2(int_trigger_volt_s_reg__0[0]),
        .I3(Q[0]),
        .I4(\slv_reg11_reg[9] [0]),
        .I5(R_unsigned_data_prev[0]),
        .O(\state[0]_i_30_n_0 ));
  LUT6 #(
    .INIT(64'hB84700000000B847)) 
    \state[0]_i_31 
       (.I0(\slv_reg11_reg[9] [7]),
        .I1(Q[0]),
        .I2(int_trigger_volt_s_reg__0[7]),
        .I3(R_unsigned_data_prev[7]),
        .I4(video_inst_n_46),
        .I5(R_unsigned_data_prev[6]),
        .O(\state[0]_i_31_n_0 ));
  LUT6 #(
    .INIT(64'hB84700000000B847)) 
    \state[0]_i_32 
       (.I0(\slv_reg11_reg[9] [5]),
        .I1(Q[0]),
        .I2(int_trigger_volt_s_reg__0[5]),
        .I3(R_unsigned_data_prev[5]),
        .I4(video_inst_n_45),
        .I5(R_unsigned_data_prev[4]),
        .O(\state[0]_i_32_n_0 ));
  LUT6 #(
    .INIT(64'h9099900009000999)) 
    \state[0]_i_33 
       (.I0(video_inst_n_44),
        .I1(R_unsigned_data_prev[3]),
        .I2(\slv_reg11_reg[9] [2]),
        .I3(Q[0]),
        .I4(int_trigger_volt_s_reg__0[2]),
        .I5(R_unsigned_data_prev[2]),
        .O(\state[0]_i_33_n_0 ));
  LUT6 #(
    .INIT(64'h9099900009000999)) 
    \state[0]_i_34 
       (.I0(video_inst_n_39),
        .I1(R_unsigned_data_prev[1]),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(Q[0]),
        .I4(int_trigger_volt_s_reg__0[0]),
        .I5(R_unsigned_data_prev[0]),
        .O(\state[0]_i_34_n_0 ));
  LUT6 #(
    .INIT(64'h44444444DDD444D4)) 
    \state[0]_i_43 
       (.I0(L_unsigned_data_prev[7]),
        .I1(video_inst_n_42),
        .I2(int_trigger_volt_s_reg__0[6]),
        .I3(Q[0]),
        .I4(\slv_reg11_reg[9] [6]),
        .I5(L_unsigned_data_prev[6]),
        .O(\state[0]_i_43_n_0 ));
  LUT6 #(
    .INIT(64'h44444444DDD444D4)) 
    \state[0]_i_44 
       (.I0(L_unsigned_data_prev[5]),
        .I1(video_inst_n_43),
        .I2(int_trigger_volt_s_reg__0[4]),
        .I3(Q[0]),
        .I4(\slv_reg11_reg[9] [4]),
        .I5(L_unsigned_data_prev[4]),
        .O(\state[0]_i_44_n_0 ));
  LUT6 #(
    .INIT(64'h44444444DDD444D4)) 
    \state[0]_i_45 
       (.I0(L_unsigned_data_prev[3]),
        .I1(video_inst_n_44),
        .I2(int_trigger_volt_s_reg__0[2]),
        .I3(Q[0]),
        .I4(\slv_reg11_reg[9] [2]),
        .I5(L_unsigned_data_prev[2]),
        .O(\state[0]_i_45_n_0 ));
  LUT6 #(
    .INIT(64'h44444444DDD444D4)) 
    \state[0]_i_46 
       (.I0(L_unsigned_data_prev[1]),
        .I1(video_inst_n_39),
        .I2(int_trigger_volt_s_reg__0[0]),
        .I3(Q[0]),
        .I4(\slv_reg11_reg[9] [0]),
        .I5(L_unsigned_data_prev[0]),
        .O(\state[0]_i_46_n_0 ));
  LUT6 #(
    .INIT(64'hB84700000000B847)) 
    \state[0]_i_47 
       (.I0(\slv_reg11_reg[9] [7]),
        .I1(Q[0]),
        .I2(int_trigger_volt_s_reg__0[7]),
        .I3(L_unsigned_data_prev[7]),
        .I4(video_inst_n_46),
        .I5(L_unsigned_data_prev[6]),
        .O(\state[0]_i_47_n_0 ));
  LUT6 #(
    .INIT(64'hB84700000000B847)) 
    \state[0]_i_48 
       (.I0(\slv_reg11_reg[9] [5]),
        .I1(Q[0]),
        .I2(int_trigger_volt_s_reg__0[5]),
        .I3(L_unsigned_data_prev[5]),
        .I4(video_inst_n_45),
        .I5(L_unsigned_data_prev[4]),
        .O(\state[0]_i_48_n_0 ));
  LUT6 #(
    .INIT(64'h9099900009000999)) 
    \state[0]_i_49 
       (.I0(video_inst_n_44),
        .I1(L_unsigned_data_prev[3]),
        .I2(\slv_reg11_reg[9] [2]),
        .I3(Q[0]),
        .I4(int_trigger_volt_s_reg__0[2]),
        .I5(L_unsigned_data_prev[2]),
        .O(\state[0]_i_49_n_0 ));
  LUT6 #(
    .INIT(64'h9099900009000999)) 
    \state[0]_i_50 
       (.I0(video_inst_n_39),
        .I1(L_unsigned_data_prev[1]),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(Q[0]),
        .I4(int_trigger_volt_s_reg__0[0]),
        .I5(L_unsigned_data_prev[0]),
        .O(\state[0]_i_50_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \state_reg[0]_i_10 
       (.CI(1'b0),
        .CO({\state_reg[0]_i_10_n_0 ,\state_reg[0]_i_10_n_1 ,\state_reg[0]_i_10_n_2 ,\state_reg[0]_i_10_n_3 }),
        .CYINIT(1'b1),
        .DI({\state[0]_i_27_n_0 ,\state[0]_i_28_n_0 ,\state[0]_i_29_n_0 ,\state[0]_i_30_n_0 }),
        .O(\NLW_state_reg[0]_i_10_O_UNCONNECTED [3:0]),
        .S({\state[0]_i_31_n_0 ,\state[0]_i_32_n_0 ,\state[0]_i_33_n_0 ,\state[0]_i_34_n_0 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \state_reg[0]_i_16 
       (.CI(1'b0),
        .CO({\state_reg[0]_i_16_n_0 ,\state_reg[0]_i_16_n_1 ,\state_reg[0]_i_16_n_2 ,\state_reg[0]_i_16_n_3 }),
        .CYINIT(1'b1),
        .DI({\state[0]_i_43_n_0 ,\state[0]_i_44_n_0 ,\state[0]_i_45_n_0 ,\state[0]_i_46_n_0 }),
        .O(\NLW_state_reg[0]_i_16_O_UNCONNECTED [3:0]),
        .S({\state[0]_i_47_n_0 ,\state[0]_i_48_n_0 ,\state[0]_i_49_n_0 ,\state[0]_i_50_n_0 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \state_reg[0]_i_4 
       (.CI(\state_reg[0]_i_10_n_0 ),
        .CO({\NLW_state_reg[0]_i_4_CO_UNCONNECTED [3:1],R_bus_l}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\state[0]_i_11_n_0 }),
        .O(\NLW_state_reg[0]_i_4_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\state[0]_i_12_n_0 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \state_reg[0]_i_6 
       (.CI(\state_reg[0]_i_16_n_0 ),
        .CO({\NLW_state_reg[0]_i_6_CO_UNCONNECTED [3:1],L_bus_l}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\state[0]_i_17_n_0 }),
        .O(\NLW_state_reg[0]_i_6_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\state[0]_i_18_n_0 }));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video video_inst
       (.ADDRARDADDR({video_inst_n_9,video_inst_n_10,video_inst_n_11,video_inst_n_12,video_inst_n_13,video_inst_n_14,video_inst_n_15,video_inst_n_16,column}),
        .CO(ch1),
        .DOADO(L_out_bram),
        .Q(int_trigger_time_s_reg__0),
        .S(video_inst_n_40),
        .clk(clk),
        .\encoded_reg[8] (video_inst_n_38),
        .\encoded_reg[8]_0 (video_inst_n_42),
        .\int_trigger_time_s_reg[0] (video_inst_n_8),
        .\int_trigger_time_s_reg[0]_0 (video_inst_n_29),
        .\int_trigger_time_s_reg[0]_1 (video_inst_n_30),
        .\int_trigger_time_s_reg[0]_2 (video_inst_n_31),
        .\int_trigger_time_s_reg[0]_3 (video_inst_n_32),
        .\int_trigger_time_s_reg[0]_4 (video_inst_n_33),
        .\int_trigger_time_s_reg[0]_5 (video_inst_n_34),
        .\int_trigger_time_s_reg[0]_6 (video_inst_n_35),
        .\int_trigger_time_s_reg[0]_7 (video_inst_n_36),
        .\int_trigger_time_s_reg[0]_8 (video_inst_n_37),
        .\int_trigger_volt_s_reg[9] (int_trigger_volt_s_reg__0),
        .lopt(lopt),
        .\processQ_reg[8] (lBRAM_n_3),
        .\processQ_reg[9] (row),
        .\processQ_reg[9]_0 (rBRAM_n_0),
        .\processQ_reg[9]_1 (ch2),
        .reset_n(reset_n),
        .\slv_reg10_reg[9] (\slv_reg10_reg[9] ),
        .\slv_reg11_reg[9] (\slv_reg11_reg[9] ),
        .\slv_reg5_reg[0] (Q[0]),
        .\state_reg[0] (video_inst_n_39),
        .\state_reg[0]_0 (video_inst_n_41),
        .\state_reg[0]_1 (video_inst_n_43),
        .\state_reg[0]_2 (video_inst_n_44),
        .\state_reg[0]_3 (video_inst_n_45),
        .\state_reg[0]_4 (video_inst_n_46),
        .switch(switch[1:0]),
        .tmds(tmds),
        .tmdsb(tmdsb));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm
   (SR,
    state,
    E,
    WREN,
    sw,
    reset_n,
    ready_sig_reg,
    Q,
    \slv_reg5_reg[0] ,
    reset_n_0,
    clk,
    \state_reg[1]_0 );
  output [0:0]SR;
  output [1:0]state;
  output [0:0]E;
  output WREN;
  input [0:0]sw;
  input reset_n;
  input ready_sig_reg;
  input [0:0]Q;
  input [0:0]\slv_reg5_reg[0] ;
  input [0:0]reset_n_0;
  input clk;
  input \state_reg[1]_0 ;

  wire [0:0]E;
  wire [0:0]Q;
  wire [0:0]SR;
  wire WREN;
  wire clk;
  wire ready_sig_reg;
  wire reset_n;
  wire [0:0]reset_n_0;
  wire [0:0]\slv_reg5_reg[0] ;
  wire [1:0]state;
  wire \state[1]_i_1__0_n_0 ;
  wire \state_reg[1]_0 ;
  wire [0:0]sw;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h51FF)) 
    \processQ[9]_i_1 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(sw),
        .I3(reset_n),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \processQ[9]_i_2 
       (.I0(sw),
        .I1(state[0]),
        .I2(state[1]),
        .O(E));
  LUT4 #(
    .INIT(16'h8B88)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_1 
       (.I0(Q),
        .I1(\slv_reg5_reg[0] ),
        .I2(state[1]),
        .I3(state[0]),
        .O(WREN));
  LUT4 #(
    .INIT(16'hAB02)) 
    \state[1]_i_1__0 
       (.I0(state[1]),
        .I1(sw),
        .I2(ready_sig_reg),
        .I3(state[0]),
        .O(\state[1]_i_1__0_n_0 ));
  FDRE \state_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\state_reg[1]_0 ),
        .Q(state[0]),
        .R(reset_n_0));
  FDRE \state_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\state[1]_i_1__0_n_0 ),
        .Q(state[1]),
        .R(reset_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0
   (S_AXI_ARREADY,
    s00_axi_rvalid,
    ready,
    tmds,
    tmdsb,
    ac_mclk,
    ac_dac_sdata,
    ac_lrclk,
    ac_bclk,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    s00_axi_rdata,
    s00_axi_bvalid,
    scl,
    sda,
    clk,
    ac_adc_sdata,
    reset_n,
    btn,
    s00_axi_arvalid,
    switch,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_ARREADY;
  output s00_axi_rvalid;
  output ready;
  output [3:0]tmds;
  output [3:0]tmdsb;
  output ac_mclk;
  output ac_dac_sdata;
  output ac_lrclk;
  output ac_bclk;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_bvalid;
  inout scl;
  inout sda;
  input clk;
  input ac_adc_sdata;
  input reset_n;
  input [4:0]btn;
  input s00_axi_arvalid;
  input [3:0]switch;
  input s00_axi_aclk;
  input [4:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [4:0]s00_axi_araddr;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire [4:0]btn;
  wire clk;
  wire ready;
  wire reset_n;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire scl;
  wire sda;
  wire [3:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;

  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h55555555C0000000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_bready),
        .I1(S_AXI_AWREADY),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .I4(S_AXI_WREADY),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0_S00_AXI my_oscope_ip_v1_0_S00_AXI_inst
       (.CLK(ready),
        .SR(axi_awready_i_1_n_0),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .axi_arready_reg_0(axi_rvalid_i_1_n_0),
        .axi_awready_reg_0(axi_bvalid_i_1_n_0),
        .btn(btn),
        .clk(clk),
        .reset_n(reset_n),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_arready(S_AXI_ARREADY),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awready(S_AXI_AWREADY),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(S_AXI_WREADY),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .scl(scl),
        .sda(sda),
        .switch(switch),
        .tmds(tmds),
        .tmdsb(tmdsb));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope_ip_v1_0_S00_AXI
   (s00_axi_wready,
    s00_axi_awready,
    s00_axi_arready,
    s00_axi_bvalid,
    s00_axi_rvalid,
    CLK,
    s00_axi_rdata,
    tmds,
    tmdsb,
    ac_mclk,
    ac_dac_sdata,
    ac_lrclk,
    ac_bclk,
    scl,
    sda,
    clk,
    ac_adc_sdata,
    SR,
    s00_axi_aclk,
    axi_awready_reg_0,
    axi_arready_reg_0,
    reset_n,
    btn,
    s00_axi_arvalid,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_wstrb,
    switch);
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_arready;
  output s00_axi_bvalid;
  output s00_axi_rvalid;
  output CLK;
  output [31:0]s00_axi_rdata;
  output [3:0]tmds;
  output [3:0]tmdsb;
  output ac_mclk;
  output ac_dac_sdata;
  output ac_lrclk;
  output ac_bclk;
  inout scl;
  inout sda;
  input clk;
  input ac_adc_sdata;
  input [0:0]SR;
  input s00_axi_aclk;
  input axi_awready_reg_0;
  input axi_arready_reg_0;
  input reset_n;
  input [4:0]btn;
  input s00_axi_arvalid;
  input [4:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [4:0]s00_axi_araddr;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [3:0]s00_axi_wstrb;
  input [3:0]switch;

  wire CLK;
  wire RST;
  wire [0:0]SR;
  wire WREN;
  wire ac_adc_sdata;
  wire ac_bclk;
  wire ac_dac_sdata;
  wire ac_lrclk;
  wire ac_mclk;
  wire \axi_araddr_reg[2]_rep_n_0 ;
  wire \axi_araddr_reg[3]_rep_n_0 ;
  wire axi_arready_i_1_n_0;
  wire axi_arready_reg_0;
  wire axi_awready0;
  wire axi_awready_reg_0;
  wire \axi_rdata[0]_i_11_n_0 ;
  wire \axi_rdata[0]_i_12_n_0 ;
  wire \axi_rdata[0]_i_6_n_0 ;
  wire \axi_rdata[0]_i_7_n_0 ;
  wire \axi_rdata[0]_i_8_n_0 ;
  wire \axi_rdata[0]_i_9_n_0 ;
  wire \axi_rdata[10]_i_10_n_0 ;
  wire \axi_rdata[10]_i_11_n_0 ;
  wire \axi_rdata[10]_i_4_n_0 ;
  wire \axi_rdata[10]_i_6_n_0 ;
  wire \axi_rdata[10]_i_7_n_0 ;
  wire \axi_rdata[10]_i_8_n_0 ;
  wire \axi_rdata[10]_i_9_n_0 ;
  wire \axi_rdata[11]_i_10_n_0 ;
  wire \axi_rdata[11]_i_11_n_0 ;
  wire \axi_rdata[11]_i_4_n_0 ;
  wire \axi_rdata[11]_i_6_n_0 ;
  wire \axi_rdata[11]_i_7_n_0 ;
  wire \axi_rdata[11]_i_8_n_0 ;
  wire \axi_rdata[11]_i_9_n_0 ;
  wire \axi_rdata[12]_i_10_n_0 ;
  wire \axi_rdata[12]_i_11_n_0 ;
  wire \axi_rdata[12]_i_4_n_0 ;
  wire \axi_rdata[12]_i_6_n_0 ;
  wire \axi_rdata[12]_i_7_n_0 ;
  wire \axi_rdata[12]_i_8_n_0 ;
  wire \axi_rdata[12]_i_9_n_0 ;
  wire \axi_rdata[13]_i_10_n_0 ;
  wire \axi_rdata[13]_i_11_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[13]_i_6_n_0 ;
  wire \axi_rdata[13]_i_7_n_0 ;
  wire \axi_rdata[13]_i_8_n_0 ;
  wire \axi_rdata[13]_i_9_n_0 ;
  wire \axi_rdata[14]_i_10_n_0 ;
  wire \axi_rdata[14]_i_11_n_0 ;
  wire \axi_rdata[14]_i_4_n_0 ;
  wire \axi_rdata[14]_i_6_n_0 ;
  wire \axi_rdata[14]_i_7_n_0 ;
  wire \axi_rdata[14]_i_8_n_0 ;
  wire \axi_rdata[14]_i_9_n_0 ;
  wire \axi_rdata[15]_i_10_n_0 ;
  wire \axi_rdata[15]_i_11_n_0 ;
  wire \axi_rdata[15]_i_4_n_0 ;
  wire \axi_rdata[15]_i_6_n_0 ;
  wire \axi_rdata[15]_i_7_n_0 ;
  wire \axi_rdata[15]_i_8_n_0 ;
  wire \axi_rdata[15]_i_9_n_0 ;
  wire \axi_rdata[16]_i_10_n_0 ;
  wire \axi_rdata[16]_i_11_n_0 ;
  wire \axi_rdata[16]_i_4_n_0 ;
  wire \axi_rdata[16]_i_5_n_0 ;
  wire \axi_rdata[16]_i_6_n_0 ;
  wire \axi_rdata[16]_i_7_n_0 ;
  wire \axi_rdata[16]_i_8_n_0 ;
  wire \axi_rdata[16]_i_9_n_0 ;
  wire \axi_rdata[17]_i_10_n_0 ;
  wire \axi_rdata[17]_i_11_n_0 ;
  wire \axi_rdata[17]_i_4_n_0 ;
  wire \axi_rdata[17]_i_5_n_0 ;
  wire \axi_rdata[17]_i_6_n_0 ;
  wire \axi_rdata[17]_i_7_n_0 ;
  wire \axi_rdata[17]_i_8_n_0 ;
  wire \axi_rdata[17]_i_9_n_0 ;
  wire \axi_rdata[18]_i_10_n_0 ;
  wire \axi_rdata[18]_i_11_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[18]_i_5_n_0 ;
  wire \axi_rdata[18]_i_6_n_0 ;
  wire \axi_rdata[18]_i_7_n_0 ;
  wire \axi_rdata[18]_i_8_n_0 ;
  wire \axi_rdata[18]_i_9_n_0 ;
  wire \axi_rdata[19]_i_10_n_0 ;
  wire \axi_rdata[19]_i_11_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[19]_i_5_n_0 ;
  wire \axi_rdata[19]_i_6_n_0 ;
  wire \axi_rdata[19]_i_7_n_0 ;
  wire \axi_rdata[19]_i_8_n_0 ;
  wire \axi_rdata[19]_i_9_n_0 ;
  wire \axi_rdata[1]_i_10_n_0 ;
  wire \axi_rdata[1]_i_11_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[1]_i_6_n_0 ;
  wire \axi_rdata[1]_i_7_n_0 ;
  wire \axi_rdata[1]_i_8_n_0 ;
  wire \axi_rdata[1]_i_9_n_0 ;
  wire \axi_rdata[20]_i_10_n_0 ;
  wire \axi_rdata[20]_i_11_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[20]_i_5_n_0 ;
  wire \axi_rdata[20]_i_6_n_0 ;
  wire \axi_rdata[20]_i_7_n_0 ;
  wire \axi_rdata[20]_i_8_n_0 ;
  wire \axi_rdata[20]_i_9_n_0 ;
  wire \axi_rdata[21]_i_10_n_0 ;
  wire \axi_rdata[21]_i_11_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[21]_i_5_n_0 ;
  wire \axi_rdata[21]_i_6_n_0 ;
  wire \axi_rdata[21]_i_7_n_0 ;
  wire \axi_rdata[21]_i_8_n_0 ;
  wire \axi_rdata[21]_i_9_n_0 ;
  wire \axi_rdata[22]_i_10_n_0 ;
  wire \axi_rdata[22]_i_11_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[22]_i_5_n_0 ;
  wire \axi_rdata[22]_i_6_n_0 ;
  wire \axi_rdata[22]_i_7_n_0 ;
  wire \axi_rdata[22]_i_8_n_0 ;
  wire \axi_rdata[22]_i_9_n_0 ;
  wire \axi_rdata[23]_i_10_n_0 ;
  wire \axi_rdata[23]_i_11_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[23]_i_5_n_0 ;
  wire \axi_rdata[23]_i_6_n_0 ;
  wire \axi_rdata[23]_i_7_n_0 ;
  wire \axi_rdata[23]_i_8_n_0 ;
  wire \axi_rdata[23]_i_9_n_0 ;
  wire \axi_rdata[24]_i_10_n_0 ;
  wire \axi_rdata[24]_i_11_n_0 ;
  wire \axi_rdata[24]_i_4_n_0 ;
  wire \axi_rdata[24]_i_5_n_0 ;
  wire \axi_rdata[24]_i_6_n_0 ;
  wire \axi_rdata[24]_i_7_n_0 ;
  wire \axi_rdata[24]_i_8_n_0 ;
  wire \axi_rdata[24]_i_9_n_0 ;
  wire \axi_rdata[25]_i_10_n_0 ;
  wire \axi_rdata[25]_i_11_n_0 ;
  wire \axi_rdata[25]_i_4_n_0 ;
  wire \axi_rdata[25]_i_5_n_0 ;
  wire \axi_rdata[25]_i_6_n_0 ;
  wire \axi_rdata[25]_i_7_n_0 ;
  wire \axi_rdata[25]_i_8_n_0 ;
  wire \axi_rdata[25]_i_9_n_0 ;
  wire \axi_rdata[26]_i_10_n_0 ;
  wire \axi_rdata[26]_i_11_n_0 ;
  wire \axi_rdata[26]_i_4_n_0 ;
  wire \axi_rdata[26]_i_5_n_0 ;
  wire \axi_rdata[26]_i_6_n_0 ;
  wire \axi_rdata[26]_i_7_n_0 ;
  wire \axi_rdata[26]_i_8_n_0 ;
  wire \axi_rdata[26]_i_9_n_0 ;
  wire \axi_rdata[27]_i_10_n_0 ;
  wire \axi_rdata[27]_i_11_n_0 ;
  wire \axi_rdata[27]_i_4_n_0 ;
  wire \axi_rdata[27]_i_5_n_0 ;
  wire \axi_rdata[27]_i_6_n_0 ;
  wire \axi_rdata[27]_i_7_n_0 ;
  wire \axi_rdata[27]_i_8_n_0 ;
  wire \axi_rdata[27]_i_9_n_0 ;
  wire \axi_rdata[28]_i_10_n_0 ;
  wire \axi_rdata[28]_i_11_n_0 ;
  wire \axi_rdata[28]_i_4_n_0 ;
  wire \axi_rdata[28]_i_5_n_0 ;
  wire \axi_rdata[28]_i_6_n_0 ;
  wire \axi_rdata[28]_i_7_n_0 ;
  wire \axi_rdata[28]_i_8_n_0 ;
  wire \axi_rdata[28]_i_9_n_0 ;
  wire \axi_rdata[29]_i_10_n_0 ;
  wire \axi_rdata[29]_i_11_n_0 ;
  wire \axi_rdata[29]_i_4_n_0 ;
  wire \axi_rdata[29]_i_5_n_0 ;
  wire \axi_rdata[29]_i_6_n_0 ;
  wire \axi_rdata[29]_i_7_n_0 ;
  wire \axi_rdata[29]_i_8_n_0 ;
  wire \axi_rdata[29]_i_9_n_0 ;
  wire \axi_rdata[2]_i_11_n_0 ;
  wire \axi_rdata[2]_i_12_n_0 ;
  wire \axi_rdata[2]_i_6_n_0 ;
  wire \axi_rdata[2]_i_7_n_0 ;
  wire \axi_rdata[2]_i_8_n_0 ;
  wire \axi_rdata[2]_i_9_n_0 ;
  wire \axi_rdata[30]_i_10_n_0 ;
  wire \axi_rdata[30]_i_11_n_0 ;
  wire \axi_rdata[30]_i_4_n_0 ;
  wire \axi_rdata[30]_i_5_n_0 ;
  wire \axi_rdata[30]_i_6_n_0 ;
  wire \axi_rdata[30]_i_7_n_0 ;
  wire \axi_rdata[30]_i_8_n_0 ;
  wire \axi_rdata[30]_i_9_n_0 ;
  wire \axi_rdata[31]_i_10_n_0 ;
  wire \axi_rdata[31]_i_11_n_0 ;
  wire \axi_rdata[31]_i_12_n_0 ;
  wire \axi_rdata[31]_i_1_n_0 ;
  wire \axi_rdata[31]_i_5_n_0 ;
  wire \axi_rdata[31]_i_6_n_0 ;
  wire \axi_rdata[31]_i_7_n_0 ;
  wire \axi_rdata[31]_i_8_n_0 ;
  wire \axi_rdata[31]_i_9_n_0 ;
  wire \axi_rdata[3]_i_10_n_0 ;
  wire \axi_rdata[3]_i_11_n_0 ;
  wire \axi_rdata[3]_i_4_n_0 ;
  wire \axi_rdata[3]_i_6_n_0 ;
  wire \axi_rdata[3]_i_7_n_0 ;
  wire \axi_rdata[3]_i_8_n_0 ;
  wire \axi_rdata[3]_i_9_n_0 ;
  wire \axi_rdata[4]_i_10_n_0 ;
  wire \axi_rdata[4]_i_11_n_0 ;
  wire \axi_rdata[4]_i_4_n_0 ;
  wire \axi_rdata[4]_i_6_n_0 ;
  wire \axi_rdata[4]_i_7_n_0 ;
  wire \axi_rdata[4]_i_8_n_0 ;
  wire \axi_rdata[4]_i_9_n_0 ;
  wire \axi_rdata[5]_i_10_n_0 ;
  wire \axi_rdata[5]_i_11_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[5]_i_6_n_0 ;
  wire \axi_rdata[5]_i_7_n_0 ;
  wire \axi_rdata[5]_i_8_n_0 ;
  wire \axi_rdata[5]_i_9_n_0 ;
  wire \axi_rdata[6]_i_10_n_0 ;
  wire \axi_rdata[6]_i_11_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[6]_i_6_n_0 ;
  wire \axi_rdata[6]_i_7_n_0 ;
  wire \axi_rdata[6]_i_8_n_0 ;
  wire \axi_rdata[6]_i_9_n_0 ;
  wire \axi_rdata[7]_i_10_n_0 ;
  wire \axi_rdata[7]_i_11_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[7]_i_6_n_0 ;
  wire \axi_rdata[7]_i_7_n_0 ;
  wire \axi_rdata[7]_i_8_n_0 ;
  wire \axi_rdata[7]_i_9_n_0 ;
  wire \axi_rdata[8]_i_10_n_0 ;
  wire \axi_rdata[8]_i_11_n_0 ;
  wire \axi_rdata[8]_i_4_n_0 ;
  wire \axi_rdata[8]_i_6_n_0 ;
  wire \axi_rdata[8]_i_7_n_0 ;
  wire \axi_rdata[8]_i_8_n_0 ;
  wire \axi_rdata[8]_i_9_n_0 ;
  wire \axi_rdata[9]_i_10_n_0 ;
  wire \axi_rdata[9]_i_11_n_0 ;
  wire \axi_rdata[9]_i_4_n_0 ;
  wire \axi_rdata[9]_i_6_n_0 ;
  wire \axi_rdata[9]_i_7_n_0 ;
  wire \axi_rdata[9]_i_8_n_0 ;
  wire \axi_rdata[9]_i_9_n_0 ;
  wire \axi_rdata_reg[0]_i_2_n_0 ;
  wire \axi_rdata_reg[0]_i_3_n_0 ;
  wire \axi_rdata_reg[10]_i_2_n_0 ;
  wire \axi_rdata_reg[10]_i_3_n_0 ;
  wire \axi_rdata_reg[11]_i_2_n_0 ;
  wire \axi_rdata_reg[11]_i_3_n_0 ;
  wire \axi_rdata_reg[12]_i_2_n_0 ;
  wire \axi_rdata_reg[12]_i_3_n_0 ;
  wire \axi_rdata_reg[13]_i_2_n_0 ;
  wire \axi_rdata_reg[13]_i_3_n_0 ;
  wire \axi_rdata_reg[14]_i_2_n_0 ;
  wire \axi_rdata_reg[14]_i_3_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_3_n_0 ;
  wire \axi_rdata_reg[16]_i_2_n_0 ;
  wire \axi_rdata_reg[16]_i_3_n_0 ;
  wire \axi_rdata_reg[17]_i_2_n_0 ;
  wire \axi_rdata_reg[17]_i_3_n_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[18]_i_3_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_3_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_3_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_3_n_0 ;
  wire \axi_rdata_reg[21]_i_2_n_0 ;
  wire \axi_rdata_reg[21]_i_3_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_3_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_3_n_0 ;
  wire \axi_rdata_reg[24]_i_2_n_0 ;
  wire \axi_rdata_reg[24]_i_3_n_0 ;
  wire \axi_rdata_reg[25]_i_2_n_0 ;
  wire \axi_rdata_reg[25]_i_3_n_0 ;
  wire \axi_rdata_reg[26]_i_2_n_0 ;
  wire \axi_rdata_reg[26]_i_3_n_0 ;
  wire \axi_rdata_reg[27]_i_2_n_0 ;
  wire \axi_rdata_reg[27]_i_3_n_0 ;
  wire \axi_rdata_reg[28]_i_2_n_0 ;
  wire \axi_rdata_reg[28]_i_3_n_0 ;
  wire \axi_rdata_reg[29]_i_2_n_0 ;
  wire \axi_rdata_reg[29]_i_3_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_3_n_0 ;
  wire \axi_rdata_reg[30]_i_2_n_0 ;
  wire \axi_rdata_reg[30]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_4_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[3]_i_3_n_0 ;
  wire \axi_rdata_reg[4]_i_2_n_0 ;
  wire \axi_rdata_reg[4]_i_3_n_0 ;
  wire \axi_rdata_reg[5]_i_2_n_0 ;
  wire \axi_rdata_reg[5]_i_3_n_0 ;
  wire \axi_rdata_reg[6]_i_2_n_0 ;
  wire \axi_rdata_reg[6]_i_3_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_3_n_0 ;
  wire \axi_rdata_reg[8]_i_2_n_0 ;
  wire \axi_rdata_reg[8]_i_3_n_0 ;
  wire \axi_rdata_reg[9]_i_2_n_0 ;
  wire \axi_rdata_reg[9]_i_3_n_0 ;
  wire axi_wready0;
  wire [4:0]btn;
  wire clk;
  wire control_n_0;
  wire datapath_n_13;
  wire [4:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire reset_n;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire \sampCounter/processQ0 ;
  wire scl;
  wire sda;
  wire [4:0]sel0;
  wire [9:0]slv_reg0;
  wire \slv_reg0[31]_i_2_n_0 ;
  wire \slv_reg0[31]_i_3_n_0 ;
  wire [31:10]slv_reg0__0;
  wire [15:0]slv_reg1;
  wire [9:0]slv_reg10;
  wire \slv_reg10[15]_i_1_n_0 ;
  wire \slv_reg10[23]_i_1_n_0 ;
  wire \slv_reg10[31]_i_1_n_0 ;
  wire \slv_reg10[31]_i_2_n_0 ;
  wire \slv_reg10[7]_i_1_n_0 ;
  wire [31:10]slv_reg10__0;
  wire [9:0]slv_reg11;
  wire \slv_reg11[15]_i_1_n_0 ;
  wire \slv_reg11[23]_i_1_n_0 ;
  wire \slv_reg11[31]_i_1_n_0 ;
  wire \slv_reg11[31]_i_2_n_0 ;
  wire \slv_reg11[7]_i_1_n_0 ;
  wire [31:10]slv_reg11__0;
  wire [31:0]slv_reg12;
  wire \slv_reg12[15]_i_1_n_0 ;
  wire \slv_reg12[23]_i_1_n_0 ;
  wire \slv_reg12[31]_i_1_n_0 ;
  wire \slv_reg12[31]_i_2_n_0 ;
  wire \slv_reg12[7]_i_1_n_0 ;
  wire [31:0]slv_reg13;
  wire \slv_reg13[15]_i_1_n_0 ;
  wire \slv_reg13[23]_i_1_n_0 ;
  wire \slv_reg13[31]_i_1_n_0 ;
  wire \slv_reg13[31]_i_2_n_0 ;
  wire \slv_reg13[7]_i_1_n_0 ;
  wire [31:0]slv_reg14;
  wire \slv_reg14[15]_i_1_n_0 ;
  wire \slv_reg14[23]_i_1_n_0 ;
  wire \slv_reg14[31]_i_1_n_0 ;
  wire \slv_reg14[31]_i_2_n_0 ;
  wire \slv_reg14[7]_i_1_n_0 ;
  wire [31:0]slv_reg15;
  wire \slv_reg15[15]_i_1_n_0 ;
  wire \slv_reg15[23]_i_1_n_0 ;
  wire \slv_reg15[31]_i_1_n_0 ;
  wire \slv_reg15[31]_i_2_n_0 ;
  wire \slv_reg15[7]_i_1_n_0 ;
  wire [31:0]slv_reg16;
  wire \slv_reg16[15]_i_1_n_0 ;
  wire \slv_reg16[23]_i_1_n_0 ;
  wire \slv_reg16[31]_i_1_n_0 ;
  wire \slv_reg16[31]_i_2_n_0 ;
  wire \slv_reg16[7]_i_1_n_0 ;
  wire [31:0]slv_reg17;
  wire \slv_reg17[15]_i_1_n_0 ;
  wire \slv_reg17[23]_i_1_n_0 ;
  wire \slv_reg17[31]_i_1_n_0 ;
  wire \slv_reg17[31]_i_2_n_0 ;
  wire \slv_reg17[7]_i_1_n_0 ;
  wire [31:0]slv_reg18;
  wire \slv_reg18[15]_i_1_n_0 ;
  wire \slv_reg18[23]_i_1_n_0 ;
  wire \slv_reg18[31]_i_1_n_0 ;
  wire \slv_reg18[31]_i_2_n_0 ;
  wire \slv_reg18[7]_i_1_n_0 ;
  wire [31:0]slv_reg19;
  wire \slv_reg19[15]_i_1_n_0 ;
  wire \slv_reg19[23]_i_1_n_0 ;
  wire \slv_reg19[31]_i_1_n_0 ;
  wire \slv_reg19[31]_i_2_n_0 ;
  wire \slv_reg19[7]_i_1_n_0 ;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[31]_i_2_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:16]slv_reg1__0;
  wire [15:0]slv_reg2;
  wire [31:0]slv_reg20;
  wire \slv_reg20[15]_i_1_n_0 ;
  wire \slv_reg20[23]_i_1_n_0 ;
  wire \slv_reg20[31]_i_1_n_0 ;
  wire \slv_reg20[31]_i_2_n_0 ;
  wire \slv_reg20[7]_i_1_n_0 ;
  wire [31:0]slv_reg21;
  wire \slv_reg21[15]_i_1_n_0 ;
  wire \slv_reg21[23]_i_1_n_0 ;
  wire \slv_reg21[31]_i_1_n_0 ;
  wire \slv_reg21[31]_i_2_n_0 ;
  wire \slv_reg21[7]_i_1_n_0 ;
  wire [31:0]slv_reg22;
  wire \slv_reg22[15]_i_1_n_0 ;
  wire \slv_reg22[23]_i_1_n_0 ;
  wire \slv_reg22[31]_i_1_n_0 ;
  wire \slv_reg22[31]_i_2_n_0 ;
  wire \slv_reg22[7]_i_1_n_0 ;
  wire [31:0]slv_reg23;
  wire \slv_reg23[15]_i_1_n_0 ;
  wire \slv_reg23[23]_i_1_n_0 ;
  wire \slv_reg23[31]_i_1_n_0 ;
  wire \slv_reg23[31]_i_2_n_0 ;
  wire \slv_reg23[7]_i_1_n_0 ;
  wire [31:0]slv_reg24;
  wire \slv_reg24[15]_i_1_n_0 ;
  wire \slv_reg24[23]_i_1_n_0 ;
  wire \slv_reg24[31]_i_1_n_0 ;
  wire \slv_reg24[31]_i_2_n_0 ;
  wire \slv_reg24[7]_i_1_n_0 ;
  wire [31:0]slv_reg25;
  wire \slv_reg25[15]_i_1_n_0 ;
  wire \slv_reg25[23]_i_1_n_0 ;
  wire \slv_reg25[31]_i_1_n_0 ;
  wire \slv_reg25[31]_i_2_n_0 ;
  wire \slv_reg25[7]_i_1_n_0 ;
  wire [31:0]slv_reg26;
  wire \slv_reg26[15]_i_1_n_0 ;
  wire \slv_reg26[23]_i_1_n_0 ;
  wire \slv_reg26[31]_i_1_n_0 ;
  wire \slv_reg26[31]_i_2_n_0 ;
  wire \slv_reg26[7]_i_1_n_0 ;
  wire [31:0]slv_reg27;
  wire \slv_reg27[15]_i_1_n_0 ;
  wire \slv_reg27[23]_i_1_n_0 ;
  wire \slv_reg27[31]_i_1_n_0 ;
  wire \slv_reg27[31]_i_2_n_0 ;
  wire \slv_reg27[7]_i_1_n_0 ;
  wire [31:0]slv_reg28;
  wire \slv_reg28[15]_i_1_n_0 ;
  wire \slv_reg28[23]_i_1_n_0 ;
  wire \slv_reg28[31]_i_1_n_0 ;
  wire \slv_reg28[31]_i_2_n_0 ;
  wire \slv_reg28[7]_i_1_n_0 ;
  wire [31:0]slv_reg29;
  wire \slv_reg29[15]_i_1_n_0 ;
  wire \slv_reg29[23]_i_1_n_0 ;
  wire \slv_reg29[31]_i_1_n_0 ;
  wire \slv_reg29[31]_i_2_n_0 ;
  wire \slv_reg29[7]_i_1_n_0 ;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[31]_i_2_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:16]slv_reg2__0;
  wire [7:0]slv_reg3;
  wire [31:0]slv_reg30;
  wire \slv_reg30[15]_i_1_n_0 ;
  wire \slv_reg30[23]_i_1_n_0 ;
  wire \slv_reg30[31]_i_1_n_0 ;
  wire \slv_reg30[31]_i_2_n_0 ;
  wire \slv_reg30[7]_i_1_n_0 ;
  wire [31:0]slv_reg31;
  wire \slv_reg31[15]_i_1_n_0 ;
  wire \slv_reg31[23]_i_1_n_0 ;
  wire \slv_reg31[31]_i_1_n_0 ;
  wire \slv_reg31[31]_i_2_n_0 ;
  wire \slv_reg31[7]_i_1_n_0 ;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[31]_i_2_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire [31:8]slv_reg3__0;
  wire [0:0]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[31]_i_2_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [31:1]slv_reg4__0;
  wire [0:0]slv_reg5;
  wire \slv_reg5[15]_i_1_n_0 ;
  wire \slv_reg5[23]_i_1_n_0 ;
  wire \slv_reg5[31]_i_1_n_0 ;
  wire \slv_reg5[31]_i_2_n_0 ;
  wire \slv_reg5[7]_i_1_n_0 ;
  wire [31:1]slv_reg5__0;
  wire [1:0]state;
  wire [1:1]sw;
  wire [3:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;

  (* ORIG_CELL_NAME = "axi_araddr_reg[2]" *) 
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .S(SR));
  (* ORIG_CELL_NAME = "axi_araddr_reg[2]" *) 
  FDSE \axi_araddr_reg[2]_rep 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[0]),
        .Q(\axi_araddr_reg[2]_rep_n_0 ),
        .S(SR));
  (* ORIG_CELL_NAME = "axi_araddr_reg[3]" *) 
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .S(SR));
  (* ORIG_CELL_NAME = "axi_araddr_reg[3]" *) 
  FDSE \axi_araddr_reg[3]_rep 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[1]),
        .Q(\axi_araddr_reg[3]_rep_n_0 ),
        .S(SR));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .S(SR));
  FDSE \axi_araddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[3]),
        .Q(sel0[3]),
        .S(SR));
  FDSE \axi_araddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_arready_i_1_n_0),
        .D(s00_axi_araddr[4]),
        .Q(sel0[4]),
        .S(SR));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_arready),
        .O(axi_arready_i_1_n_0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready_i_1_n_0),
        .Q(s00_axi_arready),
        .R(SR));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(SR));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(SR));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(SR));
  FDRE \axi_awaddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(SR));
  FDRE \axi_awaddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[4]),
        .Q(p_0_in[4]),
        .R(SR));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awready),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(s00_axi_awready),
        .R(SR));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready_reg_0),
        .Q(s00_axi_bvalid),
        .R(SR));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_11 
       (.I0(slv_reg15[0]),
        .I1(slv_reg14[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[0]),
        .O(\axi_rdata[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_12 
       (.I0(slv_reg3[0]),
        .I1(slv_reg2[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[0]),
        .O(\axi_rdata[0]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_6 
       (.I0(slv_reg27[0]),
        .I1(slv_reg26[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[0]),
        .O(\axi_rdata[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_7 
       (.I0(slv_reg31[0]),
        .I1(slv_reg30[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[0]),
        .O(\axi_rdata[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_8 
       (.I0(slv_reg19[0]),
        .I1(slv_reg18[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[0]),
        .O(\axi_rdata[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_9 
       (.I0(slv_reg23[0]),
        .I1(slv_reg22[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[0]),
        .O(\axi_rdata[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_10 
       (.I0(slv_reg15[10]),
        .I1(slv_reg14[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[10]),
        .O(\axi_rdata[10]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_11 
       (.I0(slv_reg3__0[10]),
        .I1(slv_reg2[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0__0[10]),
        .O(\axi_rdata[10]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[10]_i_4 
       (.I0(\axi_rdata[10]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10__0[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11__0[10]),
        .O(\axi_rdata[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_6 
       (.I0(slv_reg27[10]),
        .I1(slv_reg26[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[10]),
        .O(\axi_rdata[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_7 
       (.I0(slv_reg31[10]),
        .I1(slv_reg30[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[10]),
        .O(\axi_rdata[10]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_8 
       (.I0(slv_reg19[10]),
        .I1(slv_reg18[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[10]),
        .O(\axi_rdata[10]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_9 
       (.I0(slv_reg23[10]),
        .I1(slv_reg22[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[10]),
        .O(\axi_rdata[10]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_10 
       (.I0(slv_reg15[11]),
        .I1(slv_reg14[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[11]),
        .O(\axi_rdata[11]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_11 
       (.I0(slv_reg3__0[11]),
        .I1(slv_reg2[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0__0[11]),
        .O(\axi_rdata[11]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[11]_i_4 
       (.I0(\axi_rdata[11]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10__0[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11__0[11]),
        .O(\axi_rdata[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_6 
       (.I0(slv_reg27[11]),
        .I1(slv_reg26[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[11]),
        .O(\axi_rdata[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_7 
       (.I0(slv_reg31[11]),
        .I1(slv_reg30[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[11]),
        .O(\axi_rdata[11]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_8 
       (.I0(slv_reg19[11]),
        .I1(slv_reg18[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[11]),
        .O(\axi_rdata[11]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_9 
       (.I0(slv_reg23[11]),
        .I1(slv_reg22[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[11]),
        .O(\axi_rdata[11]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_10 
       (.I0(slv_reg15[12]),
        .I1(slv_reg14[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[12]),
        .O(\axi_rdata[12]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_11 
       (.I0(slv_reg3__0[12]),
        .I1(slv_reg2[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0__0[12]),
        .O(\axi_rdata[12]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[12]_i_4 
       (.I0(\axi_rdata[12]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10__0[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11__0[12]),
        .O(\axi_rdata[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_6 
       (.I0(slv_reg27[12]),
        .I1(slv_reg26[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[12]),
        .O(\axi_rdata[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_7 
       (.I0(slv_reg31[12]),
        .I1(slv_reg30[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[12]),
        .O(\axi_rdata[12]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_8 
       (.I0(slv_reg19[12]),
        .I1(slv_reg18[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[12]),
        .O(\axi_rdata[12]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_9 
       (.I0(slv_reg23[12]),
        .I1(slv_reg22[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[12]),
        .O(\axi_rdata[12]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_10 
       (.I0(slv_reg15[13]),
        .I1(slv_reg14[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[13]),
        .O(\axi_rdata[13]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_11 
       (.I0(slv_reg3__0[13]),
        .I1(slv_reg2[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0__0[13]),
        .O(\axi_rdata[13]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[13]_i_4 
       (.I0(\axi_rdata[13]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10__0[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11__0[13]),
        .O(\axi_rdata[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_6 
       (.I0(slv_reg27[13]),
        .I1(slv_reg26[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[13]),
        .O(\axi_rdata[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_7 
       (.I0(slv_reg31[13]),
        .I1(slv_reg30[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[13]),
        .O(\axi_rdata[13]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_8 
       (.I0(slv_reg19[13]),
        .I1(slv_reg18[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[13]),
        .O(\axi_rdata[13]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_9 
       (.I0(slv_reg23[13]),
        .I1(slv_reg22[13]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[13]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[13]),
        .O(\axi_rdata[13]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_10 
       (.I0(slv_reg15[14]),
        .I1(slv_reg14[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[14]),
        .O(\axi_rdata[14]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_11 
       (.I0(slv_reg3__0[14]),
        .I1(slv_reg2[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0__0[14]),
        .O(\axi_rdata[14]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[14]_i_4 
       (.I0(\axi_rdata[14]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10__0[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11__0[14]),
        .O(\axi_rdata[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_6 
       (.I0(slv_reg27[14]),
        .I1(slv_reg26[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[14]),
        .O(\axi_rdata[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_7 
       (.I0(slv_reg31[14]),
        .I1(slv_reg30[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[14]),
        .O(\axi_rdata[14]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_8 
       (.I0(slv_reg19[14]),
        .I1(slv_reg18[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[14]),
        .O(\axi_rdata[14]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_9 
       (.I0(slv_reg23[14]),
        .I1(slv_reg22[14]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[14]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[14]),
        .O(\axi_rdata[14]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_10 
       (.I0(slv_reg15[15]),
        .I1(slv_reg14[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[15]),
        .O(\axi_rdata[15]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_11 
       (.I0(slv_reg3__0[15]),
        .I1(slv_reg2[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0__0[15]),
        .O(\axi_rdata[15]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[15]_i_4 
       (.I0(\axi_rdata[15]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10__0[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11__0[15]),
        .O(\axi_rdata[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_6 
       (.I0(slv_reg27[15]),
        .I1(slv_reg26[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[15]),
        .O(\axi_rdata[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_7 
       (.I0(slv_reg31[15]),
        .I1(slv_reg30[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[15]),
        .O(\axi_rdata[15]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_8 
       (.I0(slv_reg19[15]),
        .I1(slv_reg18[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[15]),
        .O(\axi_rdata[15]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_9 
       (.I0(slv_reg23[15]),
        .I1(slv_reg22[15]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[15]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[15]),
        .O(\axi_rdata[15]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata_reg[16]_i_2_n_0 ),
        .I1(\axi_rdata_reg[16]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[16]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[16]_i_5_n_0 ),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_10 
       (.I0(slv_reg15[16]),
        .I1(slv_reg14[16]),
        .I2(sel0[1]),
        .I3(slv_reg13[16]),
        .I4(sel0[0]),
        .I5(slv_reg12[16]),
        .O(\axi_rdata[16]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_11 
       (.I0(slv_reg3__0[16]),
        .I1(slv_reg2__0[16]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[16]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[16]),
        .O(\axi_rdata[16]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[16]_i_4 
       (.I0(\axi_rdata[16]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[16]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[16]),
        .O(\axi_rdata[16]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[16]_i_5 
       (.I0(slv_reg4__0[16]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[16]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[16]_i_11_n_0 ),
        .O(\axi_rdata[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_6 
       (.I0(slv_reg27[16]),
        .I1(slv_reg26[16]),
        .I2(sel0[1]),
        .I3(slv_reg25[16]),
        .I4(sel0[0]),
        .I5(slv_reg24[16]),
        .O(\axi_rdata[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_7 
       (.I0(slv_reg31[16]),
        .I1(slv_reg30[16]),
        .I2(sel0[1]),
        .I3(slv_reg29[16]),
        .I4(sel0[0]),
        .I5(slv_reg28[16]),
        .O(\axi_rdata[16]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_8 
       (.I0(slv_reg19[16]),
        .I1(slv_reg18[16]),
        .I2(sel0[1]),
        .I3(slv_reg17[16]),
        .I4(sel0[0]),
        .I5(slv_reg16[16]),
        .O(\axi_rdata[16]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_9 
       (.I0(slv_reg23[16]),
        .I1(slv_reg22[16]),
        .I2(sel0[1]),
        .I3(slv_reg21[16]),
        .I4(sel0[0]),
        .I5(slv_reg20[16]),
        .O(\axi_rdata[16]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata_reg[17]_i_2_n_0 ),
        .I1(\axi_rdata_reg[17]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[17]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[17]_i_5_n_0 ),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_10 
       (.I0(slv_reg15[17]),
        .I1(slv_reg14[17]),
        .I2(sel0[1]),
        .I3(slv_reg13[17]),
        .I4(sel0[0]),
        .I5(slv_reg12[17]),
        .O(\axi_rdata[17]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_11 
       (.I0(slv_reg3__0[17]),
        .I1(slv_reg2__0[17]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[17]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[17]),
        .O(\axi_rdata[17]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[17]_i_4 
       (.I0(\axi_rdata[17]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[17]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[17]),
        .O(\axi_rdata[17]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[17]_i_5 
       (.I0(slv_reg4__0[17]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[17]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[17]_i_11_n_0 ),
        .O(\axi_rdata[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_6 
       (.I0(slv_reg27[17]),
        .I1(slv_reg26[17]),
        .I2(sel0[1]),
        .I3(slv_reg25[17]),
        .I4(sel0[0]),
        .I5(slv_reg24[17]),
        .O(\axi_rdata[17]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_7 
       (.I0(slv_reg31[17]),
        .I1(slv_reg30[17]),
        .I2(sel0[1]),
        .I3(slv_reg29[17]),
        .I4(sel0[0]),
        .I5(slv_reg28[17]),
        .O(\axi_rdata[17]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_8 
       (.I0(slv_reg19[17]),
        .I1(slv_reg18[17]),
        .I2(sel0[1]),
        .I3(slv_reg17[17]),
        .I4(sel0[0]),
        .I5(slv_reg16[17]),
        .O(\axi_rdata[17]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_9 
       (.I0(slv_reg23[17]),
        .I1(slv_reg22[17]),
        .I2(sel0[1]),
        .I3(slv_reg21[17]),
        .I4(sel0[0]),
        .I5(slv_reg20[17]),
        .O(\axi_rdata[17]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata_reg[18]_i_2_n_0 ),
        .I1(\axi_rdata_reg[18]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[18]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[18]_i_5_n_0 ),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_10 
       (.I0(slv_reg15[18]),
        .I1(slv_reg14[18]),
        .I2(sel0[1]),
        .I3(slv_reg13[18]),
        .I4(sel0[0]),
        .I5(slv_reg12[18]),
        .O(\axi_rdata[18]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_11 
       (.I0(slv_reg3__0[18]),
        .I1(slv_reg2__0[18]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[18]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[18]),
        .O(\axi_rdata[18]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[18]_i_4 
       (.I0(\axi_rdata[18]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[18]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[18]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[18]_i_5 
       (.I0(slv_reg4__0[18]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[18]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[18]_i_11_n_0 ),
        .O(\axi_rdata[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_6 
       (.I0(slv_reg27[18]),
        .I1(slv_reg26[18]),
        .I2(sel0[1]),
        .I3(slv_reg25[18]),
        .I4(sel0[0]),
        .I5(slv_reg24[18]),
        .O(\axi_rdata[18]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_7 
       (.I0(slv_reg31[18]),
        .I1(slv_reg30[18]),
        .I2(sel0[1]),
        .I3(slv_reg29[18]),
        .I4(sel0[0]),
        .I5(slv_reg28[18]),
        .O(\axi_rdata[18]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_8 
       (.I0(slv_reg19[18]),
        .I1(slv_reg18[18]),
        .I2(sel0[1]),
        .I3(slv_reg17[18]),
        .I4(sel0[0]),
        .I5(slv_reg16[18]),
        .O(\axi_rdata[18]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_9 
       (.I0(slv_reg23[18]),
        .I1(slv_reg22[18]),
        .I2(sel0[1]),
        .I3(slv_reg21[18]),
        .I4(sel0[0]),
        .I5(slv_reg20[18]),
        .O(\axi_rdata[18]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata_reg[19]_i_2_n_0 ),
        .I1(\axi_rdata_reg[19]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[19]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[19]_i_5_n_0 ),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_10 
       (.I0(slv_reg15[19]),
        .I1(slv_reg14[19]),
        .I2(sel0[1]),
        .I3(slv_reg13[19]),
        .I4(sel0[0]),
        .I5(slv_reg12[19]),
        .O(\axi_rdata[19]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_11 
       (.I0(slv_reg3__0[19]),
        .I1(slv_reg2__0[19]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[19]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[19]),
        .O(\axi_rdata[19]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[19]_i_4 
       (.I0(\axi_rdata[19]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[19]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[19]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[19]_i_5 
       (.I0(slv_reg4__0[19]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[19]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[19]_i_11_n_0 ),
        .O(\axi_rdata[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_6 
       (.I0(slv_reg27[19]),
        .I1(slv_reg26[19]),
        .I2(sel0[1]),
        .I3(slv_reg25[19]),
        .I4(sel0[0]),
        .I5(slv_reg24[19]),
        .O(\axi_rdata[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_7 
       (.I0(slv_reg31[19]),
        .I1(slv_reg30[19]),
        .I2(sel0[1]),
        .I3(slv_reg29[19]),
        .I4(sel0[0]),
        .I5(slv_reg28[19]),
        .O(\axi_rdata[19]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_8 
       (.I0(slv_reg19[19]),
        .I1(slv_reg18[19]),
        .I2(sel0[1]),
        .I3(slv_reg17[19]),
        .I4(sel0[0]),
        .I5(slv_reg16[19]),
        .O(\axi_rdata[19]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_9 
       (.I0(slv_reg23[19]),
        .I1(slv_reg22[19]),
        .I2(sel0[1]),
        .I3(slv_reg21[19]),
        .I4(sel0[0]),
        .I5(slv_reg20[19]),
        .O(\axi_rdata[19]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_10 
       (.I0(slv_reg15[1]),
        .I1(slv_reg14[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[1]),
        .O(\axi_rdata[1]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_11 
       (.I0(slv_reg3[1]),
        .I1(slv_reg2[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[1]),
        .O(\axi_rdata[1]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[1]_i_4 
       (.I0(\axi_rdata[1]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11[1]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_6 
       (.I0(slv_reg27[1]),
        .I1(slv_reg26[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[1]),
        .O(\axi_rdata[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_7 
       (.I0(slv_reg31[1]),
        .I1(slv_reg30[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[1]),
        .O(\axi_rdata[1]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_8 
       (.I0(slv_reg19[1]),
        .I1(slv_reg18[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[1]),
        .O(\axi_rdata[1]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_9 
       (.I0(slv_reg23[1]),
        .I1(slv_reg22[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[1]),
        .O(\axi_rdata[1]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata_reg[20]_i_2_n_0 ),
        .I1(\axi_rdata_reg[20]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[20]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[20]_i_5_n_0 ),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_10 
       (.I0(slv_reg15[20]),
        .I1(slv_reg14[20]),
        .I2(sel0[1]),
        .I3(slv_reg13[20]),
        .I4(sel0[0]),
        .I5(slv_reg12[20]),
        .O(\axi_rdata[20]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_11 
       (.I0(slv_reg3__0[20]),
        .I1(slv_reg2__0[20]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[20]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[20]),
        .O(\axi_rdata[20]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[20]_i_4 
       (.I0(\axi_rdata[20]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[20]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[20]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[20]_i_5 
       (.I0(slv_reg4__0[20]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[20]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[20]_i_11_n_0 ),
        .O(\axi_rdata[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_6 
       (.I0(slv_reg27[20]),
        .I1(slv_reg26[20]),
        .I2(sel0[1]),
        .I3(slv_reg25[20]),
        .I4(sel0[0]),
        .I5(slv_reg24[20]),
        .O(\axi_rdata[20]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_7 
       (.I0(slv_reg31[20]),
        .I1(slv_reg30[20]),
        .I2(sel0[1]),
        .I3(slv_reg29[20]),
        .I4(sel0[0]),
        .I5(slv_reg28[20]),
        .O(\axi_rdata[20]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_8 
       (.I0(slv_reg19[20]),
        .I1(slv_reg18[20]),
        .I2(sel0[1]),
        .I3(slv_reg17[20]),
        .I4(sel0[0]),
        .I5(slv_reg16[20]),
        .O(\axi_rdata[20]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_9 
       (.I0(slv_reg23[20]),
        .I1(slv_reg22[20]),
        .I2(sel0[1]),
        .I3(slv_reg21[20]),
        .I4(sel0[0]),
        .I5(slv_reg20[20]),
        .O(\axi_rdata[20]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata_reg[21]_i_2_n_0 ),
        .I1(\axi_rdata_reg[21]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[21]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[21]_i_5_n_0 ),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_10 
       (.I0(slv_reg15[21]),
        .I1(slv_reg14[21]),
        .I2(sel0[1]),
        .I3(slv_reg13[21]),
        .I4(sel0[0]),
        .I5(slv_reg12[21]),
        .O(\axi_rdata[21]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_11 
       (.I0(slv_reg3__0[21]),
        .I1(slv_reg2__0[21]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[21]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[21]),
        .O(\axi_rdata[21]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[21]_i_4 
       (.I0(\axi_rdata[21]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[21]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[21]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[21]_i_5 
       (.I0(slv_reg4__0[21]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[21]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[21]_i_11_n_0 ),
        .O(\axi_rdata[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_6 
       (.I0(slv_reg27[21]),
        .I1(slv_reg26[21]),
        .I2(sel0[1]),
        .I3(slv_reg25[21]),
        .I4(sel0[0]),
        .I5(slv_reg24[21]),
        .O(\axi_rdata[21]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_7 
       (.I0(slv_reg31[21]),
        .I1(slv_reg30[21]),
        .I2(sel0[1]),
        .I3(slv_reg29[21]),
        .I4(sel0[0]),
        .I5(slv_reg28[21]),
        .O(\axi_rdata[21]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_8 
       (.I0(slv_reg19[21]),
        .I1(slv_reg18[21]),
        .I2(sel0[1]),
        .I3(slv_reg17[21]),
        .I4(sel0[0]),
        .I5(slv_reg16[21]),
        .O(\axi_rdata[21]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_9 
       (.I0(slv_reg23[21]),
        .I1(slv_reg22[21]),
        .I2(sel0[1]),
        .I3(slv_reg21[21]),
        .I4(sel0[0]),
        .I5(slv_reg20[21]),
        .O(\axi_rdata[21]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata_reg[22]_i_2_n_0 ),
        .I1(\axi_rdata_reg[22]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[22]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[22]_i_5_n_0 ),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_10 
       (.I0(slv_reg15[22]),
        .I1(slv_reg14[22]),
        .I2(sel0[1]),
        .I3(slv_reg13[22]),
        .I4(sel0[0]),
        .I5(slv_reg12[22]),
        .O(\axi_rdata[22]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_11 
       (.I0(slv_reg3__0[22]),
        .I1(slv_reg2__0[22]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[22]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[22]),
        .O(\axi_rdata[22]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[22]_i_4 
       (.I0(\axi_rdata[22]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[22]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[22]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[22]_i_5 
       (.I0(slv_reg4__0[22]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[22]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[22]_i_11_n_0 ),
        .O(\axi_rdata[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_6 
       (.I0(slv_reg27[22]),
        .I1(slv_reg26[22]),
        .I2(sel0[1]),
        .I3(slv_reg25[22]),
        .I4(sel0[0]),
        .I5(slv_reg24[22]),
        .O(\axi_rdata[22]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_7 
       (.I0(slv_reg31[22]),
        .I1(slv_reg30[22]),
        .I2(sel0[1]),
        .I3(slv_reg29[22]),
        .I4(sel0[0]),
        .I5(slv_reg28[22]),
        .O(\axi_rdata[22]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_8 
       (.I0(slv_reg19[22]),
        .I1(slv_reg18[22]),
        .I2(sel0[1]),
        .I3(slv_reg17[22]),
        .I4(sel0[0]),
        .I5(slv_reg16[22]),
        .O(\axi_rdata[22]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_9 
       (.I0(slv_reg23[22]),
        .I1(slv_reg22[22]),
        .I2(sel0[1]),
        .I3(slv_reg21[22]),
        .I4(sel0[0]),
        .I5(slv_reg20[22]),
        .O(\axi_rdata[22]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata_reg[23]_i_2_n_0 ),
        .I1(\axi_rdata_reg[23]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[23]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[23]_i_5_n_0 ),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_10 
       (.I0(slv_reg15[23]),
        .I1(slv_reg14[23]),
        .I2(sel0[1]),
        .I3(slv_reg13[23]),
        .I4(sel0[0]),
        .I5(slv_reg12[23]),
        .O(\axi_rdata[23]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_11 
       (.I0(slv_reg3__0[23]),
        .I1(slv_reg2__0[23]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[23]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[23]),
        .O(\axi_rdata[23]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[23]_i_4 
       (.I0(\axi_rdata[23]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[23]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[23]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[23]_i_5 
       (.I0(slv_reg4__0[23]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[23]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[23]_i_11_n_0 ),
        .O(\axi_rdata[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_6 
       (.I0(slv_reg27[23]),
        .I1(slv_reg26[23]),
        .I2(sel0[1]),
        .I3(slv_reg25[23]),
        .I4(sel0[0]),
        .I5(slv_reg24[23]),
        .O(\axi_rdata[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_7 
       (.I0(slv_reg31[23]),
        .I1(slv_reg30[23]),
        .I2(sel0[1]),
        .I3(slv_reg29[23]),
        .I4(sel0[0]),
        .I5(slv_reg28[23]),
        .O(\axi_rdata[23]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_8 
       (.I0(slv_reg19[23]),
        .I1(slv_reg18[23]),
        .I2(sel0[1]),
        .I3(slv_reg17[23]),
        .I4(sel0[0]),
        .I5(slv_reg16[23]),
        .O(\axi_rdata[23]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_9 
       (.I0(slv_reg23[23]),
        .I1(slv_reg22[23]),
        .I2(sel0[1]),
        .I3(slv_reg21[23]),
        .I4(sel0[0]),
        .I5(slv_reg20[23]),
        .O(\axi_rdata[23]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata_reg[24]_i_2_n_0 ),
        .I1(\axi_rdata_reg[24]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[24]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[24]_i_5_n_0 ),
        .O(reg_data_out[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_10 
       (.I0(slv_reg15[24]),
        .I1(slv_reg14[24]),
        .I2(sel0[1]),
        .I3(slv_reg13[24]),
        .I4(sel0[0]),
        .I5(slv_reg12[24]),
        .O(\axi_rdata[24]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_11 
       (.I0(slv_reg3__0[24]),
        .I1(slv_reg2__0[24]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[24]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[24]),
        .O(\axi_rdata[24]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[24]_i_4 
       (.I0(\axi_rdata[24]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[24]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[24]),
        .O(\axi_rdata[24]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[24]_i_5 
       (.I0(slv_reg4__0[24]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[24]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[24]_i_11_n_0 ),
        .O(\axi_rdata[24]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_6 
       (.I0(slv_reg27[24]),
        .I1(slv_reg26[24]),
        .I2(sel0[1]),
        .I3(slv_reg25[24]),
        .I4(sel0[0]),
        .I5(slv_reg24[24]),
        .O(\axi_rdata[24]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_7 
       (.I0(slv_reg31[24]),
        .I1(slv_reg30[24]),
        .I2(sel0[1]),
        .I3(slv_reg29[24]),
        .I4(sel0[0]),
        .I5(slv_reg28[24]),
        .O(\axi_rdata[24]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_8 
       (.I0(slv_reg19[24]),
        .I1(slv_reg18[24]),
        .I2(sel0[1]),
        .I3(slv_reg17[24]),
        .I4(sel0[0]),
        .I5(slv_reg16[24]),
        .O(\axi_rdata[24]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_9 
       (.I0(slv_reg23[24]),
        .I1(slv_reg22[24]),
        .I2(sel0[1]),
        .I3(slv_reg21[24]),
        .I4(sel0[0]),
        .I5(slv_reg20[24]),
        .O(\axi_rdata[24]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata_reg[25]_i_2_n_0 ),
        .I1(\axi_rdata_reg[25]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[25]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[25]_i_5_n_0 ),
        .O(reg_data_out[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_10 
       (.I0(slv_reg15[25]),
        .I1(slv_reg14[25]),
        .I2(sel0[1]),
        .I3(slv_reg13[25]),
        .I4(sel0[0]),
        .I5(slv_reg12[25]),
        .O(\axi_rdata[25]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_11 
       (.I0(slv_reg3__0[25]),
        .I1(slv_reg2__0[25]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[25]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[25]),
        .O(\axi_rdata[25]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[25]_i_4 
       (.I0(\axi_rdata[25]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[25]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[25]),
        .O(\axi_rdata[25]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[25]_i_5 
       (.I0(slv_reg4__0[25]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[25]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[25]_i_11_n_0 ),
        .O(\axi_rdata[25]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_6 
       (.I0(slv_reg27[25]),
        .I1(slv_reg26[25]),
        .I2(sel0[1]),
        .I3(slv_reg25[25]),
        .I4(sel0[0]),
        .I5(slv_reg24[25]),
        .O(\axi_rdata[25]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_7 
       (.I0(slv_reg31[25]),
        .I1(slv_reg30[25]),
        .I2(sel0[1]),
        .I3(slv_reg29[25]),
        .I4(sel0[0]),
        .I5(slv_reg28[25]),
        .O(\axi_rdata[25]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_8 
       (.I0(slv_reg19[25]),
        .I1(slv_reg18[25]),
        .I2(sel0[1]),
        .I3(slv_reg17[25]),
        .I4(sel0[0]),
        .I5(slv_reg16[25]),
        .O(\axi_rdata[25]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_9 
       (.I0(slv_reg23[25]),
        .I1(slv_reg22[25]),
        .I2(sel0[1]),
        .I3(slv_reg21[25]),
        .I4(sel0[0]),
        .I5(slv_reg20[25]),
        .O(\axi_rdata[25]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata_reg[26]_i_2_n_0 ),
        .I1(\axi_rdata_reg[26]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[26]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[26]_i_5_n_0 ),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_10 
       (.I0(slv_reg15[26]),
        .I1(slv_reg14[26]),
        .I2(sel0[1]),
        .I3(slv_reg13[26]),
        .I4(sel0[0]),
        .I5(slv_reg12[26]),
        .O(\axi_rdata[26]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_11 
       (.I0(slv_reg3__0[26]),
        .I1(slv_reg2__0[26]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[26]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[26]),
        .O(\axi_rdata[26]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[26]_i_4 
       (.I0(\axi_rdata[26]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[26]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[26]),
        .O(\axi_rdata[26]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[26]_i_5 
       (.I0(slv_reg4__0[26]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[26]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[26]_i_11_n_0 ),
        .O(\axi_rdata[26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_6 
       (.I0(slv_reg27[26]),
        .I1(slv_reg26[26]),
        .I2(sel0[1]),
        .I3(slv_reg25[26]),
        .I4(sel0[0]),
        .I5(slv_reg24[26]),
        .O(\axi_rdata[26]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_7 
       (.I0(slv_reg31[26]),
        .I1(slv_reg30[26]),
        .I2(sel0[1]),
        .I3(slv_reg29[26]),
        .I4(sel0[0]),
        .I5(slv_reg28[26]),
        .O(\axi_rdata[26]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_8 
       (.I0(slv_reg19[26]),
        .I1(slv_reg18[26]),
        .I2(sel0[1]),
        .I3(slv_reg17[26]),
        .I4(sel0[0]),
        .I5(slv_reg16[26]),
        .O(\axi_rdata[26]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_9 
       (.I0(slv_reg23[26]),
        .I1(slv_reg22[26]),
        .I2(sel0[1]),
        .I3(slv_reg21[26]),
        .I4(sel0[0]),
        .I5(slv_reg20[26]),
        .O(\axi_rdata[26]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata_reg[27]_i_2_n_0 ),
        .I1(\axi_rdata_reg[27]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[27]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[27]_i_5_n_0 ),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_10 
       (.I0(slv_reg15[27]),
        .I1(slv_reg14[27]),
        .I2(sel0[1]),
        .I3(slv_reg13[27]),
        .I4(sel0[0]),
        .I5(slv_reg12[27]),
        .O(\axi_rdata[27]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_11 
       (.I0(slv_reg3__0[27]),
        .I1(slv_reg2__0[27]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[27]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[27]),
        .O(\axi_rdata[27]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[27]_i_4 
       (.I0(\axi_rdata[27]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[27]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[27]),
        .O(\axi_rdata[27]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[27]_i_5 
       (.I0(slv_reg4__0[27]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[27]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[27]_i_11_n_0 ),
        .O(\axi_rdata[27]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_6 
       (.I0(slv_reg27[27]),
        .I1(slv_reg26[27]),
        .I2(sel0[1]),
        .I3(slv_reg25[27]),
        .I4(sel0[0]),
        .I5(slv_reg24[27]),
        .O(\axi_rdata[27]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_7 
       (.I0(slv_reg31[27]),
        .I1(slv_reg30[27]),
        .I2(sel0[1]),
        .I3(slv_reg29[27]),
        .I4(sel0[0]),
        .I5(slv_reg28[27]),
        .O(\axi_rdata[27]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_8 
       (.I0(slv_reg19[27]),
        .I1(slv_reg18[27]),
        .I2(sel0[1]),
        .I3(slv_reg17[27]),
        .I4(sel0[0]),
        .I5(slv_reg16[27]),
        .O(\axi_rdata[27]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_9 
       (.I0(slv_reg23[27]),
        .I1(slv_reg22[27]),
        .I2(sel0[1]),
        .I3(slv_reg21[27]),
        .I4(sel0[0]),
        .I5(slv_reg20[27]),
        .O(\axi_rdata[27]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata_reg[28]_i_2_n_0 ),
        .I1(\axi_rdata_reg[28]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[28]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[28]_i_5_n_0 ),
        .O(reg_data_out[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_10 
       (.I0(slv_reg15[28]),
        .I1(slv_reg14[28]),
        .I2(sel0[1]),
        .I3(slv_reg13[28]),
        .I4(sel0[0]),
        .I5(slv_reg12[28]),
        .O(\axi_rdata[28]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_11 
       (.I0(slv_reg3__0[28]),
        .I1(slv_reg2__0[28]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[28]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[28]),
        .O(\axi_rdata[28]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[28]_i_4 
       (.I0(\axi_rdata[28]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[28]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[28]),
        .O(\axi_rdata[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[28]_i_5 
       (.I0(slv_reg4__0[28]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[28]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[28]_i_11_n_0 ),
        .O(\axi_rdata[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_6 
       (.I0(slv_reg27[28]),
        .I1(slv_reg26[28]),
        .I2(sel0[1]),
        .I3(slv_reg25[28]),
        .I4(sel0[0]),
        .I5(slv_reg24[28]),
        .O(\axi_rdata[28]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_7 
       (.I0(slv_reg31[28]),
        .I1(slv_reg30[28]),
        .I2(sel0[1]),
        .I3(slv_reg29[28]),
        .I4(sel0[0]),
        .I5(slv_reg28[28]),
        .O(\axi_rdata[28]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_8 
       (.I0(slv_reg19[28]),
        .I1(slv_reg18[28]),
        .I2(sel0[1]),
        .I3(slv_reg17[28]),
        .I4(sel0[0]),
        .I5(slv_reg16[28]),
        .O(\axi_rdata[28]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_9 
       (.I0(slv_reg23[28]),
        .I1(slv_reg22[28]),
        .I2(sel0[1]),
        .I3(slv_reg21[28]),
        .I4(sel0[0]),
        .I5(slv_reg20[28]),
        .O(\axi_rdata[28]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata_reg[29]_i_2_n_0 ),
        .I1(\axi_rdata_reg[29]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[29]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[29]_i_5_n_0 ),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_10 
       (.I0(slv_reg15[29]),
        .I1(slv_reg14[29]),
        .I2(sel0[1]),
        .I3(slv_reg13[29]),
        .I4(sel0[0]),
        .I5(slv_reg12[29]),
        .O(\axi_rdata[29]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_11 
       (.I0(slv_reg3__0[29]),
        .I1(slv_reg2__0[29]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[29]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[29]),
        .O(\axi_rdata[29]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[29]_i_4 
       (.I0(\axi_rdata[29]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[29]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[29]),
        .O(\axi_rdata[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[29]_i_5 
       (.I0(slv_reg4__0[29]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[29]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[29]_i_11_n_0 ),
        .O(\axi_rdata[29]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_6 
       (.I0(slv_reg27[29]),
        .I1(slv_reg26[29]),
        .I2(sel0[1]),
        .I3(slv_reg25[29]),
        .I4(sel0[0]),
        .I5(slv_reg24[29]),
        .O(\axi_rdata[29]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_7 
       (.I0(slv_reg31[29]),
        .I1(slv_reg30[29]),
        .I2(sel0[1]),
        .I3(slv_reg29[29]),
        .I4(sel0[0]),
        .I5(slv_reg28[29]),
        .O(\axi_rdata[29]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_8 
       (.I0(slv_reg19[29]),
        .I1(slv_reg18[29]),
        .I2(sel0[1]),
        .I3(slv_reg17[29]),
        .I4(sel0[0]),
        .I5(slv_reg16[29]),
        .O(\axi_rdata[29]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_9 
       (.I0(slv_reg23[29]),
        .I1(slv_reg22[29]),
        .I2(sel0[1]),
        .I3(slv_reg21[29]),
        .I4(sel0[0]),
        .I5(slv_reg20[29]),
        .O(\axi_rdata[29]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_11 
       (.I0(slv_reg15[2]),
        .I1(slv_reg14[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[2]),
        .O(\axi_rdata[2]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_12 
       (.I0(slv_reg3[2]),
        .I1(slv_reg2[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[2]),
        .O(\axi_rdata[2]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_6 
       (.I0(slv_reg27[2]),
        .I1(slv_reg26[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[2]),
        .O(\axi_rdata[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_7 
       (.I0(slv_reg31[2]),
        .I1(slv_reg30[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[2]),
        .O(\axi_rdata[2]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_8 
       (.I0(slv_reg19[2]),
        .I1(slv_reg18[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[2]),
        .O(\axi_rdata[2]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_9 
       (.I0(slv_reg23[2]),
        .I1(slv_reg22[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[2]),
        .O(\axi_rdata[2]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata_reg[30]_i_2_n_0 ),
        .I1(\axi_rdata_reg[30]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[30]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[30]_i_5_n_0 ),
        .O(reg_data_out[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_10 
       (.I0(slv_reg15[30]),
        .I1(slv_reg14[30]),
        .I2(sel0[1]),
        .I3(slv_reg13[30]),
        .I4(sel0[0]),
        .I5(slv_reg12[30]),
        .O(\axi_rdata[30]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_11 
       (.I0(slv_reg3__0[30]),
        .I1(slv_reg2__0[30]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[30]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[30]),
        .O(\axi_rdata[30]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[30]_i_4 
       (.I0(\axi_rdata[30]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[30]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[30]),
        .O(\axi_rdata[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[30]_i_5 
       (.I0(slv_reg4__0[30]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[30]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[30]_i_11_n_0 ),
        .O(\axi_rdata[30]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_6 
       (.I0(slv_reg27[30]),
        .I1(slv_reg26[30]),
        .I2(sel0[1]),
        .I3(slv_reg25[30]),
        .I4(sel0[0]),
        .I5(slv_reg24[30]),
        .O(\axi_rdata[30]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_7 
       (.I0(slv_reg31[30]),
        .I1(slv_reg30[30]),
        .I2(sel0[1]),
        .I3(slv_reg29[30]),
        .I4(sel0[0]),
        .I5(slv_reg28[30]),
        .O(\axi_rdata[30]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_8 
       (.I0(slv_reg19[30]),
        .I1(slv_reg18[30]),
        .I2(sel0[1]),
        .I3(slv_reg17[30]),
        .I4(sel0[0]),
        .I5(slv_reg16[30]),
        .O(\axi_rdata[30]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_9 
       (.I0(slv_reg23[30]),
        .I1(slv_reg22[30]),
        .I2(sel0[1]),
        .I3(slv_reg21[30]),
        .I4(sel0[0]),
        .I5(slv_reg20[30]),
        .O(\axi_rdata[30]_i_9_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(s00_axi_arready),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(\axi_rdata[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_10 
       (.I0(slv_reg23[31]),
        .I1(slv_reg22[31]),
        .I2(sel0[1]),
        .I3(slv_reg21[31]),
        .I4(sel0[0]),
        .I5(slv_reg20[31]),
        .O(\axi_rdata[31]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_11 
       (.I0(slv_reg15[31]),
        .I1(slv_reg14[31]),
        .I2(sel0[1]),
        .I3(slv_reg13[31]),
        .I4(sel0[0]),
        .I5(slv_reg12[31]),
        .O(\axi_rdata[31]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_12 
       (.I0(slv_reg3__0[31]),
        .I1(slv_reg2__0[31]),
        .I2(sel0[1]),
        .I3(slv_reg1__0[31]),
        .I4(sel0[0]),
        .I5(slv_reg0__0[31]),
        .O(\axi_rdata[31]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_2 
       (.I0(\axi_rdata_reg[31]_i_3_n_0 ),
        .I1(\axi_rdata_reg[31]_i_4_n_0 ),
        .I2(sel0[4]),
        .I3(\axi_rdata[31]_i_5_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata[31]_i_6_n_0 ),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[31]_i_5 
       (.I0(\axi_rdata[31]_i_11_n_0 ),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(slv_reg10__0[31]),
        .I4(sel0[0]),
        .I5(slv_reg11__0[31]),
        .O(\axi_rdata[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[31]_i_6 
       (.I0(slv_reg4__0[31]),
        .I1(sel0[0]),
        .I2(slv_reg5__0[31]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[31]_i_12_n_0 ),
        .O(\axi_rdata[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_7 
       (.I0(slv_reg27[31]),
        .I1(slv_reg26[31]),
        .I2(sel0[1]),
        .I3(slv_reg25[31]),
        .I4(sel0[0]),
        .I5(slv_reg24[31]),
        .O(\axi_rdata[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_8 
       (.I0(slv_reg31[31]),
        .I1(slv_reg30[31]),
        .I2(sel0[1]),
        .I3(slv_reg29[31]),
        .I4(sel0[0]),
        .I5(slv_reg28[31]),
        .O(\axi_rdata[31]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_9 
       (.I0(slv_reg19[31]),
        .I1(slv_reg18[31]),
        .I2(sel0[1]),
        .I3(slv_reg17[31]),
        .I4(sel0[0]),
        .I5(slv_reg16[31]),
        .O(\axi_rdata[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_10 
       (.I0(slv_reg15[3]),
        .I1(slv_reg14[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[3]),
        .O(\axi_rdata[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_11 
       (.I0(slv_reg3[3]),
        .I1(slv_reg2[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[3]),
        .O(\axi_rdata[3]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[3]_i_4 
       (.I0(\axi_rdata[3]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11[3]),
        .O(\axi_rdata[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_6 
       (.I0(slv_reg27[3]),
        .I1(slv_reg26[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[3]),
        .O(\axi_rdata[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_7 
       (.I0(slv_reg31[3]),
        .I1(slv_reg30[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[3]),
        .O(\axi_rdata[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_8 
       (.I0(slv_reg19[3]),
        .I1(slv_reg18[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[3]),
        .O(\axi_rdata[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_9 
       (.I0(slv_reg23[3]),
        .I1(slv_reg22[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[3]),
        .O(\axi_rdata[3]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_10 
       (.I0(slv_reg15[4]),
        .I1(slv_reg14[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[4]),
        .O(\axi_rdata[4]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_11 
       (.I0(slv_reg3[4]),
        .I1(slv_reg2[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[4]),
        .O(\axi_rdata[4]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[4]_i_4 
       (.I0(\axi_rdata[4]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11[4]),
        .O(\axi_rdata[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_6 
       (.I0(slv_reg27[4]),
        .I1(slv_reg26[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[4]),
        .O(\axi_rdata[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_7 
       (.I0(slv_reg31[4]),
        .I1(slv_reg30[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[4]),
        .O(\axi_rdata[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_8 
       (.I0(slv_reg19[4]),
        .I1(slv_reg18[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[4]),
        .O(\axi_rdata[4]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_9 
       (.I0(slv_reg23[4]),
        .I1(slv_reg22[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[4]),
        .O(\axi_rdata[4]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_10 
       (.I0(slv_reg15[5]),
        .I1(slv_reg14[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[5]),
        .O(\axi_rdata[5]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_11 
       (.I0(slv_reg3[5]),
        .I1(slv_reg2[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[5]),
        .O(\axi_rdata[5]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[5]_i_4 
       (.I0(\axi_rdata[5]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11[5]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_6 
       (.I0(slv_reg27[5]),
        .I1(slv_reg26[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[5]),
        .O(\axi_rdata[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_7 
       (.I0(slv_reg31[5]),
        .I1(slv_reg30[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[5]),
        .O(\axi_rdata[5]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_8 
       (.I0(slv_reg19[5]),
        .I1(slv_reg18[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[5]),
        .O(\axi_rdata[5]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_9 
       (.I0(slv_reg23[5]),
        .I1(slv_reg22[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[5]),
        .O(\axi_rdata[5]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_10 
       (.I0(slv_reg15[6]),
        .I1(slv_reg14[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[6]),
        .O(\axi_rdata[6]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_11 
       (.I0(slv_reg3[6]),
        .I1(slv_reg2[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[6]),
        .O(\axi_rdata[6]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[6]_i_4 
       (.I0(\axi_rdata[6]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11[6]),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_6 
       (.I0(slv_reg27[6]),
        .I1(slv_reg26[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[6]),
        .O(\axi_rdata[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_7 
       (.I0(slv_reg31[6]),
        .I1(slv_reg30[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[6]),
        .O(\axi_rdata[6]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_8 
       (.I0(slv_reg19[6]),
        .I1(slv_reg18[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[6]),
        .O(\axi_rdata[6]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_9 
       (.I0(slv_reg23[6]),
        .I1(slv_reg22[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[6]),
        .O(\axi_rdata[6]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_10 
       (.I0(slv_reg15[7]),
        .I1(slv_reg14[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[7]),
        .O(\axi_rdata[7]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_11 
       (.I0(slv_reg3[7]),
        .I1(slv_reg2[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[7]),
        .O(\axi_rdata[7]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[7]_i_4 
       (.I0(\axi_rdata[7]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11[7]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_6 
       (.I0(slv_reg27[7]),
        .I1(slv_reg26[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[7]),
        .O(\axi_rdata[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_7 
       (.I0(slv_reg31[7]),
        .I1(slv_reg30[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[7]),
        .O(\axi_rdata[7]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_8 
       (.I0(slv_reg19[7]),
        .I1(slv_reg18[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[7]),
        .O(\axi_rdata[7]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_9 
       (.I0(slv_reg23[7]),
        .I1(slv_reg22[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[7]),
        .O(\axi_rdata[7]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_10 
       (.I0(slv_reg15[8]),
        .I1(slv_reg14[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[8]),
        .O(\axi_rdata[8]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_11 
       (.I0(slv_reg3__0[8]),
        .I1(slv_reg2[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[8]),
        .O(\axi_rdata[8]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[8]_i_4 
       (.I0(\axi_rdata[8]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11[8]),
        .O(\axi_rdata[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_6 
       (.I0(slv_reg27[8]),
        .I1(slv_reg26[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[8]),
        .O(\axi_rdata[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_7 
       (.I0(slv_reg31[8]),
        .I1(slv_reg30[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[8]),
        .O(\axi_rdata[8]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_8 
       (.I0(slv_reg19[8]),
        .I1(slv_reg18[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[8]),
        .O(\axi_rdata[8]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_9 
       (.I0(slv_reg23[8]),
        .I1(slv_reg22[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[8]),
        .O(\axi_rdata[8]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_10 
       (.I0(slv_reg15[9]),
        .I1(slv_reg14[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg13[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg12[9]),
        .O(\axi_rdata[9]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_11 
       (.I0(slv_reg3__0[9]),
        .I1(slv_reg2[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg1[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg0[9]),
        .O(\axi_rdata[9]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[9]_i_4 
       (.I0(\axi_rdata[9]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg10[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg11[9]),
        .O(\axi_rdata[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_6 
       (.I0(slv_reg27[9]),
        .I1(slv_reg26[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg25[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg24[9]),
        .O(\axi_rdata[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_7 
       (.I0(slv_reg31[9]),
        .I1(slv_reg30[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg29[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg28[9]),
        .O(\axi_rdata[9]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_8 
       (.I0(slv_reg19[9]),
        .I1(slv_reg18[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg17[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg16[9]),
        .O(\axi_rdata[9]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_9 
       (.I0(slv_reg23[9]),
        .I1(slv_reg22[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(slv_reg21[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(slv_reg20[9]),
        .O(\axi_rdata[9]_i_9_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(SR));
  MUXF7 \axi_rdata_reg[0]_i_2 
       (.I0(\axi_rdata[0]_i_6_n_0 ),
        .I1(\axi_rdata[0]_i_7_n_0 ),
        .O(\axi_rdata_reg[0]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_3 
       (.I0(\axi_rdata[0]_i_8_n_0 ),
        .I1(\axi_rdata[0]_i_9_n_0 ),
        .O(\axi_rdata_reg[0]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(SR));
  MUXF7 \axi_rdata_reg[10]_i_2 
       (.I0(\axi_rdata[10]_i_6_n_0 ),
        .I1(\axi_rdata[10]_i_7_n_0 ),
        .O(\axi_rdata_reg[10]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_3 
       (.I0(\axi_rdata[10]_i_8_n_0 ),
        .I1(\axi_rdata[10]_i_9_n_0 ),
        .O(\axi_rdata_reg[10]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(SR));
  MUXF7 \axi_rdata_reg[11]_i_2 
       (.I0(\axi_rdata[11]_i_6_n_0 ),
        .I1(\axi_rdata[11]_i_7_n_0 ),
        .O(\axi_rdata_reg[11]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_3 
       (.I0(\axi_rdata[11]_i_8_n_0 ),
        .I1(\axi_rdata[11]_i_9_n_0 ),
        .O(\axi_rdata_reg[11]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(SR));
  MUXF7 \axi_rdata_reg[12]_i_2 
       (.I0(\axi_rdata[12]_i_6_n_0 ),
        .I1(\axi_rdata[12]_i_7_n_0 ),
        .O(\axi_rdata_reg[12]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_3 
       (.I0(\axi_rdata[12]_i_8_n_0 ),
        .I1(\axi_rdata[12]_i_9_n_0 ),
        .O(\axi_rdata_reg[12]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(SR));
  MUXF7 \axi_rdata_reg[13]_i_2 
       (.I0(\axi_rdata[13]_i_6_n_0 ),
        .I1(\axi_rdata[13]_i_7_n_0 ),
        .O(\axi_rdata_reg[13]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_3 
       (.I0(\axi_rdata[13]_i_8_n_0 ),
        .I1(\axi_rdata[13]_i_9_n_0 ),
        .O(\axi_rdata_reg[13]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(SR));
  MUXF7 \axi_rdata_reg[14]_i_2 
       (.I0(\axi_rdata[14]_i_6_n_0 ),
        .I1(\axi_rdata[14]_i_7_n_0 ),
        .O(\axi_rdata_reg[14]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_3 
       (.I0(\axi_rdata[14]_i_8_n_0 ),
        .I1(\axi_rdata[14]_i_9_n_0 ),
        .O(\axi_rdata_reg[14]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(SR));
  MUXF7 \axi_rdata_reg[15]_i_2 
       (.I0(\axi_rdata[15]_i_6_n_0 ),
        .I1(\axi_rdata[15]_i_7_n_0 ),
        .O(\axi_rdata_reg[15]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_3 
       (.I0(\axi_rdata[15]_i_8_n_0 ),
        .I1(\axi_rdata[15]_i_9_n_0 ),
        .O(\axi_rdata_reg[15]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(SR));
  MUXF7 \axi_rdata_reg[16]_i_2 
       (.I0(\axi_rdata[16]_i_6_n_0 ),
        .I1(\axi_rdata[16]_i_7_n_0 ),
        .O(\axi_rdata_reg[16]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_3 
       (.I0(\axi_rdata[16]_i_8_n_0 ),
        .I1(\axi_rdata[16]_i_9_n_0 ),
        .O(\axi_rdata_reg[16]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(SR));
  MUXF7 \axi_rdata_reg[17]_i_2 
       (.I0(\axi_rdata[17]_i_6_n_0 ),
        .I1(\axi_rdata[17]_i_7_n_0 ),
        .O(\axi_rdata_reg[17]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_3 
       (.I0(\axi_rdata[17]_i_8_n_0 ),
        .I1(\axi_rdata[17]_i_9_n_0 ),
        .O(\axi_rdata_reg[17]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(SR));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_6_n_0 ),
        .I1(\axi_rdata[18]_i_7_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_3 
       (.I0(\axi_rdata[18]_i_8_n_0 ),
        .I1(\axi_rdata[18]_i_9_n_0 ),
        .O(\axi_rdata_reg[18]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(SR));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_6_n_0 ),
        .I1(\axi_rdata[19]_i_7_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_3 
       (.I0(\axi_rdata[19]_i_8_n_0 ),
        .I1(\axi_rdata[19]_i_9_n_0 ),
        .O(\axi_rdata_reg[19]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(SR));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_6_n_0 ),
        .I1(\axi_rdata[1]_i_7_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_3 
       (.I0(\axi_rdata[1]_i_8_n_0 ),
        .I1(\axi_rdata[1]_i_9_n_0 ),
        .O(\axi_rdata_reg[1]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(SR));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_6_n_0 ),
        .I1(\axi_rdata[20]_i_7_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_3 
       (.I0(\axi_rdata[20]_i_8_n_0 ),
        .I1(\axi_rdata[20]_i_9_n_0 ),
        .O(\axi_rdata_reg[20]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(SR));
  MUXF7 \axi_rdata_reg[21]_i_2 
       (.I0(\axi_rdata[21]_i_6_n_0 ),
        .I1(\axi_rdata[21]_i_7_n_0 ),
        .O(\axi_rdata_reg[21]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_3 
       (.I0(\axi_rdata[21]_i_8_n_0 ),
        .I1(\axi_rdata[21]_i_9_n_0 ),
        .O(\axi_rdata_reg[21]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(SR));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_6_n_0 ),
        .I1(\axi_rdata[22]_i_7_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_3 
       (.I0(\axi_rdata[22]_i_8_n_0 ),
        .I1(\axi_rdata[22]_i_9_n_0 ),
        .O(\axi_rdata_reg[22]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(SR));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_6_n_0 ),
        .I1(\axi_rdata[23]_i_7_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_3 
       (.I0(\axi_rdata[23]_i_8_n_0 ),
        .I1(\axi_rdata[23]_i_9_n_0 ),
        .O(\axi_rdata_reg[23]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(SR));
  MUXF7 \axi_rdata_reg[24]_i_2 
       (.I0(\axi_rdata[24]_i_6_n_0 ),
        .I1(\axi_rdata[24]_i_7_n_0 ),
        .O(\axi_rdata_reg[24]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_3 
       (.I0(\axi_rdata[24]_i_8_n_0 ),
        .I1(\axi_rdata[24]_i_9_n_0 ),
        .O(\axi_rdata_reg[24]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(SR));
  MUXF7 \axi_rdata_reg[25]_i_2 
       (.I0(\axi_rdata[25]_i_6_n_0 ),
        .I1(\axi_rdata[25]_i_7_n_0 ),
        .O(\axi_rdata_reg[25]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_3 
       (.I0(\axi_rdata[25]_i_8_n_0 ),
        .I1(\axi_rdata[25]_i_9_n_0 ),
        .O(\axi_rdata_reg[25]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(SR));
  MUXF7 \axi_rdata_reg[26]_i_2 
       (.I0(\axi_rdata[26]_i_6_n_0 ),
        .I1(\axi_rdata[26]_i_7_n_0 ),
        .O(\axi_rdata_reg[26]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_3 
       (.I0(\axi_rdata[26]_i_8_n_0 ),
        .I1(\axi_rdata[26]_i_9_n_0 ),
        .O(\axi_rdata_reg[26]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(SR));
  MUXF7 \axi_rdata_reg[27]_i_2 
       (.I0(\axi_rdata[27]_i_6_n_0 ),
        .I1(\axi_rdata[27]_i_7_n_0 ),
        .O(\axi_rdata_reg[27]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_3 
       (.I0(\axi_rdata[27]_i_8_n_0 ),
        .I1(\axi_rdata[27]_i_9_n_0 ),
        .O(\axi_rdata_reg[27]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(SR));
  MUXF7 \axi_rdata_reg[28]_i_2 
       (.I0(\axi_rdata[28]_i_6_n_0 ),
        .I1(\axi_rdata[28]_i_7_n_0 ),
        .O(\axi_rdata_reg[28]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_3 
       (.I0(\axi_rdata[28]_i_8_n_0 ),
        .I1(\axi_rdata[28]_i_9_n_0 ),
        .O(\axi_rdata_reg[28]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(SR));
  MUXF7 \axi_rdata_reg[29]_i_2 
       (.I0(\axi_rdata[29]_i_6_n_0 ),
        .I1(\axi_rdata[29]_i_7_n_0 ),
        .O(\axi_rdata_reg[29]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_3 
       (.I0(\axi_rdata[29]_i_8_n_0 ),
        .I1(\axi_rdata[29]_i_9_n_0 ),
        .O(\axi_rdata_reg[29]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(SR));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_6_n_0 ),
        .I1(\axi_rdata[2]_i_7_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_3 
       (.I0(\axi_rdata[2]_i_8_n_0 ),
        .I1(\axi_rdata[2]_i_9_n_0 ),
        .O(\axi_rdata_reg[2]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(SR));
  MUXF7 \axi_rdata_reg[30]_i_2 
       (.I0(\axi_rdata[30]_i_6_n_0 ),
        .I1(\axi_rdata[30]_i_7_n_0 ),
        .O(\axi_rdata_reg[30]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_3 
       (.I0(\axi_rdata[30]_i_8_n_0 ),
        .I1(\axi_rdata[30]_i_9_n_0 ),
        .O(\axi_rdata_reg[30]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(SR));
  MUXF7 \axi_rdata_reg[31]_i_3 
       (.I0(\axi_rdata[31]_i_7_n_0 ),
        .I1(\axi_rdata[31]_i_8_n_0 ),
        .O(\axi_rdata_reg[31]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_4 
       (.I0(\axi_rdata[31]_i_9_n_0 ),
        .I1(\axi_rdata[31]_i_10_n_0 ),
        .O(\axi_rdata_reg[31]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(SR));
  MUXF7 \axi_rdata_reg[3]_i_2 
       (.I0(\axi_rdata[3]_i_6_n_0 ),
        .I1(\axi_rdata[3]_i_7_n_0 ),
        .O(\axi_rdata_reg[3]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_3 
       (.I0(\axi_rdata[3]_i_8_n_0 ),
        .I1(\axi_rdata[3]_i_9_n_0 ),
        .O(\axi_rdata_reg[3]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(SR));
  MUXF7 \axi_rdata_reg[4]_i_2 
       (.I0(\axi_rdata[4]_i_6_n_0 ),
        .I1(\axi_rdata[4]_i_7_n_0 ),
        .O(\axi_rdata_reg[4]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_3 
       (.I0(\axi_rdata[4]_i_8_n_0 ),
        .I1(\axi_rdata[4]_i_9_n_0 ),
        .O(\axi_rdata_reg[4]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(SR));
  MUXF7 \axi_rdata_reg[5]_i_2 
       (.I0(\axi_rdata[5]_i_6_n_0 ),
        .I1(\axi_rdata[5]_i_7_n_0 ),
        .O(\axi_rdata_reg[5]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_3 
       (.I0(\axi_rdata[5]_i_8_n_0 ),
        .I1(\axi_rdata[5]_i_9_n_0 ),
        .O(\axi_rdata_reg[5]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(SR));
  MUXF7 \axi_rdata_reg[6]_i_2 
       (.I0(\axi_rdata[6]_i_6_n_0 ),
        .I1(\axi_rdata[6]_i_7_n_0 ),
        .O(\axi_rdata_reg[6]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_3 
       (.I0(\axi_rdata[6]_i_8_n_0 ),
        .I1(\axi_rdata[6]_i_9_n_0 ),
        .O(\axi_rdata_reg[6]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(SR));
  MUXF7 \axi_rdata_reg[7]_i_2 
       (.I0(\axi_rdata[7]_i_6_n_0 ),
        .I1(\axi_rdata[7]_i_7_n_0 ),
        .O(\axi_rdata_reg[7]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_3 
       (.I0(\axi_rdata[7]_i_8_n_0 ),
        .I1(\axi_rdata[7]_i_9_n_0 ),
        .O(\axi_rdata_reg[7]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(SR));
  MUXF7 \axi_rdata_reg[8]_i_2 
       (.I0(\axi_rdata[8]_i_6_n_0 ),
        .I1(\axi_rdata[8]_i_7_n_0 ),
        .O(\axi_rdata_reg[8]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_3 
       (.I0(\axi_rdata[8]_i_8_n_0 ),
        .I1(\axi_rdata[8]_i_9_n_0 ),
        .O(\axi_rdata_reg[8]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(SR));
  MUXF7 \axi_rdata_reg[9]_i_2 
       (.I0(\axi_rdata[9]_i_6_n_0 ),
        .I1(\axi_rdata[9]_i_7_n_0 ),
        .O(\axi_rdata_reg[9]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_3 
       (.I0(\axi_rdata[9]_i_8_n_0 ),
        .I1(\axi_rdata[9]_i_9_n_0 ),
        .O(\axi_rdata_reg[9]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready_reg_0),
        .Q(s00_axi_rvalid),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_wready),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(s00_axi_wready),
        .R(SR));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm control
       (.E(\sampCounter/processQ0 ),
        .Q(slv_reg4),
        .SR(control_n_0),
        .WREN(WREN),
        .clk(clk),
        .ready_sig_reg(CLK),
        .reset_n(reset_n),
        .reset_n_0(RST),
        .\slv_reg5_reg[0] (slv_reg5),
        .state(state),
        .\state_reg[1]_0 (datapath_n_13),
        .sw(sw));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath datapath
       (.D(reg_data_out[15:0]),
        .E(\sampCounter/processQ0 ),
        .Q({slv_reg5__0[15:1],slv_reg5}),
        .\Q_reg[2] (sw),
        .SR(RST),
        .WREN(WREN),
        .ac_adc_sdata(ac_adc_sdata),
        .ac_bclk(ac_bclk),
        .ac_dac_sdata(ac_dac_sdata),
        .ac_lrclk(ac_lrclk),
        .ac_mclk(ac_mclk),
        .\axi_araddr_reg[2]_rep (\axi_araddr_reg[2]_rep_n_0 ),
        .\axi_araddr_reg[3]_rep (\axi_araddr_reg[3]_rep_n_0 ),
        .\axi_araddr_reg[4] (\axi_rdata_reg[0]_i_2_n_0 ),
        .\axi_araddr_reg[4]_0 (\axi_rdata_reg[0]_i_3_n_0 ),
        .\axi_araddr_reg[4]_1 (\axi_rdata_reg[1]_i_2_n_0 ),
        .\axi_araddr_reg[4]_10 (\axi_rdata_reg[4]_i_3_n_0 ),
        .\axi_araddr_reg[4]_11 (\axi_rdata[4]_i_4_n_0 ),
        .\axi_araddr_reg[4]_12 (\axi_rdata_reg[5]_i_2_n_0 ),
        .\axi_araddr_reg[4]_13 (\axi_rdata_reg[5]_i_3_n_0 ),
        .\axi_araddr_reg[4]_14 (\axi_rdata[5]_i_4_n_0 ),
        .\axi_araddr_reg[4]_15 (\axi_rdata_reg[6]_i_2_n_0 ),
        .\axi_araddr_reg[4]_16 (\axi_rdata_reg[6]_i_3_n_0 ),
        .\axi_araddr_reg[4]_17 (\axi_rdata[6]_i_4_n_0 ),
        .\axi_araddr_reg[4]_18 (\axi_rdata_reg[7]_i_2_n_0 ),
        .\axi_araddr_reg[4]_19 (\axi_rdata_reg[7]_i_3_n_0 ),
        .\axi_araddr_reg[4]_2 (\axi_rdata_reg[1]_i_3_n_0 ),
        .\axi_araddr_reg[4]_20 (\axi_rdata[7]_i_4_n_0 ),
        .\axi_araddr_reg[4]_21 (\axi_rdata_reg[8]_i_2_n_0 ),
        .\axi_araddr_reg[4]_22 (\axi_rdata_reg[8]_i_3_n_0 ),
        .\axi_araddr_reg[4]_23 (\axi_rdata[8]_i_4_n_0 ),
        .\axi_araddr_reg[4]_24 (\axi_rdata_reg[9]_i_2_n_0 ),
        .\axi_araddr_reg[4]_25 (\axi_rdata_reg[9]_i_3_n_0 ),
        .\axi_araddr_reg[4]_26 (\axi_rdata[9]_i_4_n_0 ),
        .\axi_araddr_reg[4]_27 (\axi_rdata_reg[10]_i_2_n_0 ),
        .\axi_araddr_reg[4]_28 (\axi_rdata_reg[10]_i_3_n_0 ),
        .\axi_araddr_reg[4]_29 (\axi_rdata[10]_i_4_n_0 ),
        .\axi_araddr_reg[4]_3 (\axi_rdata[1]_i_4_n_0 ),
        .\axi_araddr_reg[4]_30 (\axi_rdata_reg[11]_i_2_n_0 ),
        .\axi_araddr_reg[4]_31 (\axi_rdata_reg[11]_i_3_n_0 ),
        .\axi_araddr_reg[4]_32 (\axi_rdata[11]_i_4_n_0 ),
        .\axi_araddr_reg[4]_33 (\axi_rdata_reg[12]_i_2_n_0 ),
        .\axi_araddr_reg[4]_34 (\axi_rdata_reg[12]_i_3_n_0 ),
        .\axi_araddr_reg[4]_35 (\axi_rdata[12]_i_4_n_0 ),
        .\axi_araddr_reg[4]_36 (\axi_rdata_reg[13]_i_2_n_0 ),
        .\axi_araddr_reg[4]_37 (\axi_rdata_reg[13]_i_3_n_0 ),
        .\axi_araddr_reg[4]_38 (\axi_rdata[13]_i_4_n_0 ),
        .\axi_araddr_reg[4]_39 (\axi_rdata_reg[14]_i_2_n_0 ),
        .\axi_araddr_reg[4]_4 (\axi_rdata_reg[2]_i_2_n_0 ),
        .\axi_araddr_reg[4]_40 (\axi_rdata_reg[14]_i_3_n_0 ),
        .\axi_araddr_reg[4]_41 (\axi_rdata[14]_i_4_n_0 ),
        .\axi_araddr_reg[4]_42 (\axi_rdata_reg[15]_i_2_n_0 ),
        .\axi_araddr_reg[4]_43 (\axi_rdata_reg[15]_i_3_n_0 ),
        .\axi_araddr_reg[4]_44 (\axi_rdata[15]_i_4_n_0 ),
        .\axi_araddr_reg[4]_5 (\axi_rdata_reg[2]_i_3_n_0 ),
        .\axi_araddr_reg[4]_6 (\axi_rdata_reg[3]_i_2_n_0 ),
        .\axi_araddr_reg[4]_7 (\axi_rdata_reg[3]_i_3_n_0 ),
        .\axi_araddr_reg[4]_8 (\axi_rdata[3]_i_4_n_0 ),
        .\axi_araddr_reg[4]_9 (\axi_rdata_reg[4]_i_2_n_0 ),
        .\axi_araddr_reg[6] (sel0[4:2]),
        .btn(btn),
        .clk(clk),
        .ready(CLK),
        .reset_n(reset_n),
        .scl(scl),
        .sda(sda),
        .\slv_reg0_reg[9] (slv_reg0),
        .\slv_reg10_reg[9] (slv_reg10),
        .\slv_reg11_reg[9] (slv_reg11),
        .\slv_reg15_reg[0] (\axi_rdata[0]_i_11_n_0 ),
        .\slv_reg15_reg[2] (\axi_rdata[2]_i_11_n_0 ),
        .\slv_reg1_reg[9] (slv_reg1[9:0]),
        .\slv_reg2_reg[9] (slv_reg2[9:0]),
        .\slv_reg3_reg[0] (\axi_rdata[0]_i_12_n_0 ),
        .\slv_reg3_reg[10] (\axi_rdata[10]_i_11_n_0 ),
        .\slv_reg3_reg[11] (\axi_rdata[11]_i_11_n_0 ),
        .\slv_reg3_reg[12] (\axi_rdata[12]_i_11_n_0 ),
        .\slv_reg3_reg[13] (\axi_rdata[13]_i_11_n_0 ),
        .\slv_reg3_reg[14] (\axi_rdata[14]_i_11_n_0 ),
        .\slv_reg3_reg[15] (\axi_rdata[15]_i_11_n_0 ),
        .\slv_reg3_reg[1] (\axi_rdata[1]_i_11_n_0 ),
        .\slv_reg3_reg[2] (\axi_rdata[2]_i_12_n_0 ),
        .\slv_reg3_reg[2]_0 ({slv_reg3[2],slv_reg3[0]}),
        .\slv_reg3_reg[3] (\axi_rdata[3]_i_11_n_0 ),
        .\slv_reg3_reg[4] (\axi_rdata[4]_i_11_n_0 ),
        .\slv_reg3_reg[5] (\axi_rdata[5]_i_11_n_0 ),
        .\slv_reg3_reg[6] (\axi_rdata[6]_i_11_n_0 ),
        .\slv_reg3_reg[7] (\axi_rdata[7]_i_11_n_0 ),
        .\slv_reg3_reg[8] (\axi_rdata[8]_i_11_n_0 ),
        .\slv_reg3_reg[9] (\axi_rdata[9]_i_11_n_0 ),
        .\slv_reg4_reg[15] ({slv_reg4__0[15:1],slv_reg4}),
        .state(state),
        .\state_reg[0] (datapath_n_13),
        .\state_reg[1] (control_n_0),
        .switch(switch),
        .tmds(tmds),
        .tmdsb(tmdsb));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg0[15]_i_1 
       (.I0(\slv_reg0[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg0[23]_i_1 
       (.I0(\slv_reg0[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg0[31]_i_1 
       (.I0(\slv_reg0[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(p_1_in[31]));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \slv_reg0[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg0[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg0[31]_i_3 
       (.I0(s00_axi_wready),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_awready),
        .O(\slv_reg0[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg0[7]_i_1 
       (.I0(\slv_reg0[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg0[0]),
        .R(SR));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg0__0[10]),
        .R(SR));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg0__0[11]),
        .R(SR));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg0__0[12]),
        .R(SR));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg0__0[13]),
        .R(SR));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg0__0[14]),
        .R(SR));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg0__0[15]),
        .R(SR));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg0__0[16]),
        .R(SR));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg0__0[17]),
        .R(SR));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg0__0[18]),
        .R(SR));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg0__0[19]),
        .R(SR));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg0[1]),
        .R(SR));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg0__0[20]),
        .R(SR));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg0__0[21]),
        .R(SR));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg0__0[22]),
        .R(SR));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg0__0[23]),
        .R(SR));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg0__0[24]),
        .R(SR));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg0__0[25]),
        .R(SR));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg0__0[26]),
        .R(SR));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg0__0[27]),
        .R(SR));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg0__0[28]),
        .R(SR));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg0__0[29]),
        .R(SR));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg0[2]),
        .R(SR));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg0__0[30]),
        .R(SR));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg0__0[31]),
        .R(SR));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg0[3]),
        .R(SR));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg0[4]),
        .R(SR));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg0[5]),
        .R(SR));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg0[6]),
        .R(SR));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg0[7]),
        .R(SR));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg0[8]),
        .R(SR));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg0[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg10[15]_i_1 
       (.I0(\slv_reg10[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg10[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg10[23]_i_1 
       (.I0(\slv_reg10[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg10[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg10[31]_i_1 
       (.I0(\slv_reg10[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg10[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000400000)) 
    \slv_reg10[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg10[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg10[7]_i_1 
       (.I0(\slv_reg10[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg10[7]_i_1_n_0 ));
  FDRE \slv_reg10_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg10[0]),
        .R(SR));
  FDRE \slv_reg10_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg10__0[10]),
        .R(SR));
  FDRE \slv_reg10_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg10__0[11]),
        .R(SR));
  FDRE \slv_reg10_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg10__0[12]),
        .R(SR));
  FDRE \slv_reg10_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg10__0[13]),
        .R(SR));
  FDRE \slv_reg10_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg10__0[14]),
        .R(SR));
  FDRE \slv_reg10_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg10__0[15]),
        .R(SR));
  FDRE \slv_reg10_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg10__0[16]),
        .R(SR));
  FDRE \slv_reg10_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg10__0[17]),
        .R(SR));
  FDRE \slv_reg10_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg10__0[18]),
        .R(SR));
  FDRE \slv_reg10_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg10__0[19]),
        .R(SR));
  FDRE \slv_reg10_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg10[1]),
        .R(SR));
  FDRE \slv_reg10_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg10__0[20]),
        .R(SR));
  FDRE \slv_reg10_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg10__0[21]),
        .R(SR));
  FDRE \slv_reg10_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg10__0[22]),
        .R(SR));
  FDRE \slv_reg10_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg10__0[23]),
        .R(SR));
  FDRE \slv_reg10_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg10__0[24]),
        .R(SR));
  FDRE \slv_reg10_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg10__0[25]),
        .R(SR));
  FDRE \slv_reg10_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg10__0[26]),
        .R(SR));
  FDRE \slv_reg10_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg10__0[27]),
        .R(SR));
  FDRE \slv_reg10_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg10__0[28]),
        .R(SR));
  FDRE \slv_reg10_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg10__0[29]),
        .R(SR));
  FDRE \slv_reg10_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg10[2]),
        .R(SR));
  FDRE \slv_reg10_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg10__0[30]),
        .R(SR));
  FDRE \slv_reg10_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg10__0[31]),
        .R(SR));
  FDRE \slv_reg10_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg10[3]),
        .R(SR));
  FDRE \slv_reg10_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg10[4]),
        .R(SR));
  FDRE \slv_reg10_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg10[5]),
        .R(SR));
  FDRE \slv_reg10_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg10[6]),
        .R(SR));
  FDRE \slv_reg10_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg10[7]),
        .R(SR));
  FDRE \slv_reg10_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg10[8]),
        .R(SR));
  FDRE \slv_reg10_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg10[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg10[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg11[15]_i_1 
       (.I0(\slv_reg11[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg11[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg11[23]_i_1 
       (.I0(\slv_reg11[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg11[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg11[31]_i_1 
       (.I0(\slv_reg11[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg11[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0040000000000000)) 
    \slv_reg11[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg11[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg11[7]_i_1 
       (.I0(\slv_reg11[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg11[7]_i_1_n_0 ));
  FDRE \slv_reg11_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg11[0]),
        .R(SR));
  FDRE \slv_reg11_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg11__0[10]),
        .R(SR));
  FDRE \slv_reg11_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg11__0[11]),
        .R(SR));
  FDRE \slv_reg11_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg11__0[12]),
        .R(SR));
  FDRE \slv_reg11_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg11__0[13]),
        .R(SR));
  FDRE \slv_reg11_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg11__0[14]),
        .R(SR));
  FDRE \slv_reg11_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg11__0[15]),
        .R(SR));
  FDRE \slv_reg11_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg11__0[16]),
        .R(SR));
  FDRE \slv_reg11_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg11__0[17]),
        .R(SR));
  FDRE \slv_reg11_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg11__0[18]),
        .R(SR));
  FDRE \slv_reg11_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg11__0[19]),
        .R(SR));
  FDRE \slv_reg11_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg11[1]),
        .R(SR));
  FDRE \slv_reg11_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg11__0[20]),
        .R(SR));
  FDRE \slv_reg11_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg11__0[21]),
        .R(SR));
  FDRE \slv_reg11_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg11__0[22]),
        .R(SR));
  FDRE \slv_reg11_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg11__0[23]),
        .R(SR));
  FDRE \slv_reg11_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg11__0[24]),
        .R(SR));
  FDRE \slv_reg11_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg11__0[25]),
        .R(SR));
  FDRE \slv_reg11_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg11__0[26]),
        .R(SR));
  FDRE \slv_reg11_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg11__0[27]),
        .R(SR));
  FDRE \slv_reg11_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg11__0[28]),
        .R(SR));
  FDRE \slv_reg11_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg11__0[29]),
        .R(SR));
  FDRE \slv_reg11_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg11[2]),
        .R(SR));
  FDRE \slv_reg11_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg11__0[30]),
        .R(SR));
  FDRE \slv_reg11_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg11__0[31]),
        .R(SR));
  FDRE \slv_reg11_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg11[3]),
        .R(SR));
  FDRE \slv_reg11_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg11[4]),
        .R(SR));
  FDRE \slv_reg11_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg11[5]),
        .R(SR));
  FDRE \slv_reg11_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg11[6]),
        .R(SR));
  FDRE \slv_reg11_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg11[7]),
        .R(SR));
  FDRE \slv_reg11_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg11[8]),
        .R(SR));
  FDRE \slv_reg11_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg11[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg11[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg12[15]_i_1 
       (.I0(\slv_reg12[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg12[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg12[23]_i_1 
       (.I0(\slv_reg12[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg12[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg12[31]_i_1 
       (.I0(\slv_reg12[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg12[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \slv_reg12[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg12[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg12[7]_i_1 
       (.I0(\slv_reg12[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg12[7]_i_1_n_0 ));
  FDRE \slv_reg12_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg12[0]),
        .R(SR));
  FDRE \slv_reg12_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg12[10]),
        .R(SR));
  FDRE \slv_reg12_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg12[11]),
        .R(SR));
  FDRE \slv_reg12_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg12[12]),
        .R(SR));
  FDRE \slv_reg12_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg12[13]),
        .R(SR));
  FDRE \slv_reg12_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg12[14]),
        .R(SR));
  FDRE \slv_reg12_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg12[15]),
        .R(SR));
  FDRE \slv_reg12_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg12[16]),
        .R(SR));
  FDRE \slv_reg12_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg12[17]),
        .R(SR));
  FDRE \slv_reg12_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg12[18]),
        .R(SR));
  FDRE \slv_reg12_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg12[19]),
        .R(SR));
  FDRE \slv_reg12_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg12[1]),
        .R(SR));
  FDRE \slv_reg12_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg12[20]),
        .R(SR));
  FDRE \slv_reg12_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg12[21]),
        .R(SR));
  FDRE \slv_reg12_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg12[22]),
        .R(SR));
  FDRE \slv_reg12_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg12[23]),
        .R(SR));
  FDRE \slv_reg12_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg12[24]),
        .R(SR));
  FDRE \slv_reg12_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg12[25]),
        .R(SR));
  FDRE \slv_reg12_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg12[26]),
        .R(SR));
  FDRE \slv_reg12_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg12[27]),
        .R(SR));
  FDRE \slv_reg12_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg12[28]),
        .R(SR));
  FDRE \slv_reg12_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg12[29]),
        .R(SR));
  FDRE \slv_reg12_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg12[2]),
        .R(SR));
  FDRE \slv_reg12_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg12[30]),
        .R(SR));
  FDRE \slv_reg12_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg12[31]),
        .R(SR));
  FDRE \slv_reg12_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg12[3]),
        .R(SR));
  FDRE \slv_reg12_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg12[4]),
        .R(SR));
  FDRE \slv_reg12_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg12[5]),
        .R(SR));
  FDRE \slv_reg12_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg12[6]),
        .R(SR));
  FDRE \slv_reg12_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg12[7]),
        .R(SR));
  FDRE \slv_reg12_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg12[8]),
        .R(SR));
  FDRE \slv_reg12_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg12[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg12[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg13[15]_i_1 
       (.I0(\slv_reg13[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg13[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg13[23]_i_1 
       (.I0(\slv_reg13[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg13[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg13[31]_i_1 
       (.I0(\slv_reg13[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg13[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000400000000000)) 
    \slv_reg13[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg13[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg13[7]_i_1 
       (.I0(\slv_reg13[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg13[7]_i_1_n_0 ));
  FDRE \slv_reg13_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg13[0]),
        .R(SR));
  FDRE \slv_reg13_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg13[10]),
        .R(SR));
  FDRE \slv_reg13_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg13[11]),
        .R(SR));
  FDRE \slv_reg13_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg13[12]),
        .R(SR));
  FDRE \slv_reg13_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg13[13]),
        .R(SR));
  FDRE \slv_reg13_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg13[14]),
        .R(SR));
  FDRE \slv_reg13_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg13[15]),
        .R(SR));
  FDRE \slv_reg13_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg13[16]),
        .R(SR));
  FDRE \slv_reg13_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg13[17]),
        .R(SR));
  FDRE \slv_reg13_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg13[18]),
        .R(SR));
  FDRE \slv_reg13_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg13[19]),
        .R(SR));
  FDRE \slv_reg13_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg13[1]),
        .R(SR));
  FDRE \slv_reg13_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg13[20]),
        .R(SR));
  FDRE \slv_reg13_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg13[21]),
        .R(SR));
  FDRE \slv_reg13_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg13[22]),
        .R(SR));
  FDRE \slv_reg13_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg13[23]),
        .R(SR));
  FDRE \slv_reg13_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg13[24]),
        .R(SR));
  FDRE \slv_reg13_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg13[25]),
        .R(SR));
  FDRE \slv_reg13_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg13[26]),
        .R(SR));
  FDRE \slv_reg13_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg13[27]),
        .R(SR));
  FDRE \slv_reg13_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg13[28]),
        .R(SR));
  FDRE \slv_reg13_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg13[29]),
        .R(SR));
  FDRE \slv_reg13_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg13[2]),
        .R(SR));
  FDRE \slv_reg13_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg13[30]),
        .R(SR));
  FDRE \slv_reg13_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg13[31]),
        .R(SR));
  FDRE \slv_reg13_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg13[3]),
        .R(SR));
  FDRE \slv_reg13_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg13[4]),
        .R(SR));
  FDRE \slv_reg13_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg13[5]),
        .R(SR));
  FDRE \slv_reg13_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg13[6]),
        .R(SR));
  FDRE \slv_reg13_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg13[7]),
        .R(SR));
  FDRE \slv_reg13_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg13[8]),
        .R(SR));
  FDRE \slv_reg13_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg13[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg14[15]_i_1 
       (.I0(\slv_reg14[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg14[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg14[23]_i_1 
       (.I0(\slv_reg14[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg14[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg14[31]_i_1 
       (.I0(\slv_reg14[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg14[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \slv_reg14[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg14[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg14[7]_i_1 
       (.I0(\slv_reg14[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg14[7]_i_1_n_0 ));
  FDRE \slv_reg14_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg14[0]),
        .R(SR));
  FDRE \slv_reg14_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg14[10]),
        .R(SR));
  FDRE \slv_reg14_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg14[11]),
        .R(SR));
  FDRE \slv_reg14_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg14[12]),
        .R(SR));
  FDRE \slv_reg14_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg14[13]),
        .R(SR));
  FDRE \slv_reg14_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg14[14]),
        .R(SR));
  FDRE \slv_reg14_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg14[15]),
        .R(SR));
  FDRE \slv_reg14_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg14[16]),
        .R(SR));
  FDRE \slv_reg14_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg14[17]),
        .R(SR));
  FDRE \slv_reg14_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg14[18]),
        .R(SR));
  FDRE \slv_reg14_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg14[19]),
        .R(SR));
  FDRE \slv_reg14_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg14[1]),
        .R(SR));
  FDRE \slv_reg14_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg14[20]),
        .R(SR));
  FDRE \slv_reg14_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg14[21]),
        .R(SR));
  FDRE \slv_reg14_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg14[22]),
        .R(SR));
  FDRE \slv_reg14_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg14[23]),
        .R(SR));
  FDRE \slv_reg14_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg14[24]),
        .R(SR));
  FDRE \slv_reg14_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg14[25]),
        .R(SR));
  FDRE \slv_reg14_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg14[26]),
        .R(SR));
  FDRE \slv_reg14_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg14[27]),
        .R(SR));
  FDRE \slv_reg14_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg14[28]),
        .R(SR));
  FDRE \slv_reg14_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg14[29]),
        .R(SR));
  FDRE \slv_reg14_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg14[2]),
        .R(SR));
  FDRE \slv_reg14_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg14[30]),
        .R(SR));
  FDRE \slv_reg14_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg14[31]),
        .R(SR));
  FDRE \slv_reg14_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg14[3]),
        .R(SR));
  FDRE \slv_reg14_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg14[4]),
        .R(SR));
  FDRE \slv_reg14_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg14[5]),
        .R(SR));
  FDRE \slv_reg14_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg14[6]),
        .R(SR));
  FDRE \slv_reg14_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg14[7]),
        .R(SR));
  FDRE \slv_reg14_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg14[8]),
        .R(SR));
  FDRE \slv_reg14_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg14[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg15[15]_i_1 
       (.I0(\slv_reg15[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg15[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg15[23]_i_1 
       (.I0(\slv_reg15[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg15[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg15[31]_i_1 
       (.I0(\slv_reg15[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg15[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg15[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg15[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg15[7]_i_1 
       (.I0(\slv_reg15[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg15[7]_i_1_n_0 ));
  FDRE \slv_reg15_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg15[0]),
        .R(SR));
  FDRE \slv_reg15_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg15[10]),
        .R(SR));
  FDRE \slv_reg15_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg15[11]),
        .R(SR));
  FDRE \slv_reg15_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg15[12]),
        .R(SR));
  FDRE \slv_reg15_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg15[13]),
        .R(SR));
  FDRE \slv_reg15_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg15[14]),
        .R(SR));
  FDRE \slv_reg15_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg15[15]),
        .R(SR));
  FDRE \slv_reg15_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg15[16]),
        .R(SR));
  FDRE \slv_reg15_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg15[17]),
        .R(SR));
  FDRE \slv_reg15_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg15[18]),
        .R(SR));
  FDRE \slv_reg15_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg15[19]),
        .R(SR));
  FDRE \slv_reg15_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg15[1]),
        .R(SR));
  FDRE \slv_reg15_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg15[20]),
        .R(SR));
  FDRE \slv_reg15_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg15[21]),
        .R(SR));
  FDRE \slv_reg15_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg15[22]),
        .R(SR));
  FDRE \slv_reg15_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg15[23]),
        .R(SR));
  FDRE \slv_reg15_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg15[24]),
        .R(SR));
  FDRE \slv_reg15_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg15[25]),
        .R(SR));
  FDRE \slv_reg15_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg15[26]),
        .R(SR));
  FDRE \slv_reg15_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg15[27]),
        .R(SR));
  FDRE \slv_reg15_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg15[28]),
        .R(SR));
  FDRE \slv_reg15_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg15[29]),
        .R(SR));
  FDRE \slv_reg15_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg15[2]),
        .R(SR));
  FDRE \slv_reg15_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg15[30]),
        .R(SR));
  FDRE \slv_reg15_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg15[31]),
        .R(SR));
  FDRE \slv_reg15_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg15[3]),
        .R(SR));
  FDRE \slv_reg15_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg15[4]),
        .R(SR));
  FDRE \slv_reg15_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg15[5]),
        .R(SR));
  FDRE \slv_reg15_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg15[6]),
        .R(SR));
  FDRE \slv_reg15_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg15[7]),
        .R(SR));
  FDRE \slv_reg15_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg15[8]),
        .R(SR));
  FDRE \slv_reg15_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg15[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg16[15]_i_1 
       (.I0(\slv_reg16[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg16[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg16[23]_i_1 
       (.I0(\slv_reg16[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg16[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg16[31]_i_1 
       (.I0(\slv_reg16[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg16[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \slv_reg16[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg16[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg16[7]_i_1 
       (.I0(\slv_reg16[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg16[7]_i_1_n_0 ));
  FDRE \slv_reg16_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg16[0]),
        .R(SR));
  FDRE \slv_reg16_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg16[10]),
        .R(SR));
  FDRE \slv_reg16_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg16[11]),
        .R(SR));
  FDRE \slv_reg16_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg16[12]),
        .R(SR));
  FDRE \slv_reg16_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg16[13]),
        .R(SR));
  FDRE \slv_reg16_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg16[14]),
        .R(SR));
  FDRE \slv_reg16_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg16[15]),
        .R(SR));
  FDRE \slv_reg16_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg16[16]),
        .R(SR));
  FDRE \slv_reg16_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg16[17]),
        .R(SR));
  FDRE \slv_reg16_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg16[18]),
        .R(SR));
  FDRE \slv_reg16_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg16[19]),
        .R(SR));
  FDRE \slv_reg16_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg16[1]),
        .R(SR));
  FDRE \slv_reg16_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg16[20]),
        .R(SR));
  FDRE \slv_reg16_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg16[21]),
        .R(SR));
  FDRE \slv_reg16_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg16[22]),
        .R(SR));
  FDRE \slv_reg16_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg16[23]),
        .R(SR));
  FDRE \slv_reg16_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg16[24]),
        .R(SR));
  FDRE \slv_reg16_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg16[25]),
        .R(SR));
  FDRE \slv_reg16_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg16[26]),
        .R(SR));
  FDRE \slv_reg16_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg16[27]),
        .R(SR));
  FDRE \slv_reg16_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg16[28]),
        .R(SR));
  FDRE \slv_reg16_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg16[29]),
        .R(SR));
  FDRE \slv_reg16_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg16[2]),
        .R(SR));
  FDRE \slv_reg16_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg16[30]),
        .R(SR));
  FDRE \slv_reg16_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg16[31]),
        .R(SR));
  FDRE \slv_reg16_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg16[3]),
        .R(SR));
  FDRE \slv_reg16_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg16[4]),
        .R(SR));
  FDRE \slv_reg16_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg16[5]),
        .R(SR));
  FDRE \slv_reg16_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg16[6]),
        .R(SR));
  FDRE \slv_reg16_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg16[7]),
        .R(SR));
  FDRE \slv_reg16_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg16[8]),
        .R(SR));
  FDRE \slv_reg16_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg16[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg17[15]_i_1 
       (.I0(\slv_reg17[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg17[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg17[23]_i_1 
       (.I0(\slv_reg17[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg17[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg17[31]_i_1 
       (.I0(\slv_reg17[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg17[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \slv_reg17[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg17[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg17[7]_i_1 
       (.I0(\slv_reg17[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg17[7]_i_1_n_0 ));
  FDRE \slv_reg17_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg17[0]),
        .R(SR));
  FDRE \slv_reg17_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg17[10]),
        .R(SR));
  FDRE \slv_reg17_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg17[11]),
        .R(SR));
  FDRE \slv_reg17_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg17[12]),
        .R(SR));
  FDRE \slv_reg17_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg17[13]),
        .R(SR));
  FDRE \slv_reg17_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg17[14]),
        .R(SR));
  FDRE \slv_reg17_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg17[15]),
        .R(SR));
  FDRE \slv_reg17_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg17[16]),
        .R(SR));
  FDRE \slv_reg17_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg17[17]),
        .R(SR));
  FDRE \slv_reg17_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg17[18]),
        .R(SR));
  FDRE \slv_reg17_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg17[19]),
        .R(SR));
  FDRE \slv_reg17_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg17[1]),
        .R(SR));
  FDRE \slv_reg17_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg17[20]),
        .R(SR));
  FDRE \slv_reg17_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg17[21]),
        .R(SR));
  FDRE \slv_reg17_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg17[22]),
        .R(SR));
  FDRE \slv_reg17_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg17[23]),
        .R(SR));
  FDRE \slv_reg17_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg17[24]),
        .R(SR));
  FDRE \slv_reg17_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg17[25]),
        .R(SR));
  FDRE \slv_reg17_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg17[26]),
        .R(SR));
  FDRE \slv_reg17_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg17[27]),
        .R(SR));
  FDRE \slv_reg17_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg17[28]),
        .R(SR));
  FDRE \slv_reg17_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg17[29]),
        .R(SR));
  FDRE \slv_reg17_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg17[2]),
        .R(SR));
  FDRE \slv_reg17_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg17[30]),
        .R(SR));
  FDRE \slv_reg17_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg17[31]),
        .R(SR));
  FDRE \slv_reg17_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg17[3]),
        .R(SR));
  FDRE \slv_reg17_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg17[4]),
        .R(SR));
  FDRE \slv_reg17_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg17[5]),
        .R(SR));
  FDRE \slv_reg17_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg17[6]),
        .R(SR));
  FDRE \slv_reg17_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg17[7]),
        .R(SR));
  FDRE \slv_reg17_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg17[8]),
        .R(SR));
  FDRE \slv_reg17_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg17[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg18[15]_i_1 
       (.I0(\slv_reg18[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg18[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg18[23]_i_1 
       (.I0(\slv_reg18[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg18[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg18[31]_i_1 
       (.I0(\slv_reg18[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg18[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000200000)) 
    \slv_reg18[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg18[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg18[7]_i_1 
       (.I0(\slv_reg18[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg18[7]_i_1_n_0 ));
  FDRE \slv_reg18_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg18[0]),
        .R(SR));
  FDRE \slv_reg18_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg18[10]),
        .R(SR));
  FDRE \slv_reg18_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg18[11]),
        .R(SR));
  FDRE \slv_reg18_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg18[12]),
        .R(SR));
  FDRE \slv_reg18_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg18[13]),
        .R(SR));
  FDRE \slv_reg18_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg18[14]),
        .R(SR));
  FDRE \slv_reg18_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg18[15]),
        .R(SR));
  FDRE \slv_reg18_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg18[16]),
        .R(SR));
  FDRE \slv_reg18_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg18[17]),
        .R(SR));
  FDRE \slv_reg18_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg18[18]),
        .R(SR));
  FDRE \slv_reg18_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg18[19]),
        .R(SR));
  FDRE \slv_reg18_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg18[1]),
        .R(SR));
  FDRE \slv_reg18_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg18[20]),
        .R(SR));
  FDRE \slv_reg18_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg18[21]),
        .R(SR));
  FDRE \slv_reg18_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg18[22]),
        .R(SR));
  FDRE \slv_reg18_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg18[23]),
        .R(SR));
  FDRE \slv_reg18_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg18[24]),
        .R(SR));
  FDRE \slv_reg18_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg18[25]),
        .R(SR));
  FDRE \slv_reg18_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg18[26]),
        .R(SR));
  FDRE \slv_reg18_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg18[27]),
        .R(SR));
  FDRE \slv_reg18_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg18[28]),
        .R(SR));
  FDRE \slv_reg18_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg18[29]),
        .R(SR));
  FDRE \slv_reg18_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg18[2]),
        .R(SR));
  FDRE \slv_reg18_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg18[30]),
        .R(SR));
  FDRE \slv_reg18_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg18[31]),
        .R(SR));
  FDRE \slv_reg18_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg18[3]),
        .R(SR));
  FDRE \slv_reg18_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg18[4]),
        .R(SR));
  FDRE \slv_reg18_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg18[5]),
        .R(SR));
  FDRE \slv_reg18_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg18[6]),
        .R(SR));
  FDRE \slv_reg18_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg18[7]),
        .R(SR));
  FDRE \slv_reg18_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg18[8]),
        .R(SR));
  FDRE \slv_reg18_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg18[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg19[15]_i_1 
       (.I0(\slv_reg19[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg19[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg19[23]_i_1 
       (.I0(\slv_reg19[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg19[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg19[31]_i_1 
       (.I0(\slv_reg19[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg19[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \slv_reg19[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg19[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg19[7]_i_1 
       (.I0(\slv_reg19[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg19[7]_i_1_n_0 ));
  FDRE \slv_reg19_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg19[0]),
        .R(SR));
  FDRE \slv_reg19_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg19[10]),
        .R(SR));
  FDRE \slv_reg19_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg19[11]),
        .R(SR));
  FDRE \slv_reg19_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg19[12]),
        .R(SR));
  FDRE \slv_reg19_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg19[13]),
        .R(SR));
  FDRE \slv_reg19_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg19[14]),
        .R(SR));
  FDRE \slv_reg19_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg19[15]),
        .R(SR));
  FDRE \slv_reg19_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg19[16]),
        .R(SR));
  FDRE \slv_reg19_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg19[17]),
        .R(SR));
  FDRE \slv_reg19_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg19[18]),
        .R(SR));
  FDRE \slv_reg19_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg19[19]),
        .R(SR));
  FDRE \slv_reg19_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg19[1]),
        .R(SR));
  FDRE \slv_reg19_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg19[20]),
        .R(SR));
  FDRE \slv_reg19_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg19[21]),
        .R(SR));
  FDRE \slv_reg19_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg19[22]),
        .R(SR));
  FDRE \slv_reg19_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg19[23]),
        .R(SR));
  FDRE \slv_reg19_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg19[24]),
        .R(SR));
  FDRE \slv_reg19_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg19[25]),
        .R(SR));
  FDRE \slv_reg19_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg19[26]),
        .R(SR));
  FDRE \slv_reg19_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg19[27]),
        .R(SR));
  FDRE \slv_reg19_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg19[28]),
        .R(SR));
  FDRE \slv_reg19_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg19[29]),
        .R(SR));
  FDRE \slv_reg19_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg19[2]),
        .R(SR));
  FDRE \slv_reg19_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg19[30]),
        .R(SR));
  FDRE \slv_reg19_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg19[31]),
        .R(SR));
  FDRE \slv_reg19_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg19[3]),
        .R(SR));
  FDRE \slv_reg19_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg19[4]),
        .R(SR));
  FDRE \slv_reg19_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg19[5]),
        .R(SR));
  FDRE \slv_reg19_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg19[6]),
        .R(SR));
  FDRE \slv_reg19_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg19[7]),
        .R(SR));
  FDRE \slv_reg19_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg19[8]),
        .R(SR));
  FDRE \slv_reg19_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg19[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg1[15]_i_1 
       (.I0(\slv_reg1[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg1[23]_i_1 
       (.I0(\slv_reg1[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg1[31]_i_1 
       (.I0(\slv_reg1[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    \slv_reg1[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg1[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg1[7]_i_1 
       (.I0(\slv_reg1[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(SR));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(SR));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(SR));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(SR));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(SR));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(SR));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(SR));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1__0[16]),
        .R(SR));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1__0[17]),
        .R(SR));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1__0[18]),
        .R(SR));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1__0[19]),
        .R(SR));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(SR));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1__0[20]),
        .R(SR));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1__0[21]),
        .R(SR));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1__0[22]),
        .R(SR));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1__0[23]),
        .R(SR));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1__0[24]),
        .R(SR));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1__0[25]),
        .R(SR));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1__0[26]),
        .R(SR));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1__0[27]),
        .R(SR));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1__0[28]),
        .R(SR));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1__0[29]),
        .R(SR));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(SR));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1__0[30]),
        .R(SR));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1__0[31]),
        .R(SR));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(SR));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(SR));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(SR));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(SR));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(SR));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(SR));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg20[15]_i_1 
       (.I0(\slv_reg20[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg20[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg20[23]_i_1 
       (.I0(\slv_reg20[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg20[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg20[31]_i_1 
       (.I0(\slv_reg20[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg20[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    \slv_reg20[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg20[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg20[7]_i_1 
       (.I0(\slv_reg20[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg20[7]_i_1_n_0 ));
  FDRE \slv_reg20_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg20[0]),
        .R(SR));
  FDRE \slv_reg20_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg20[10]),
        .R(SR));
  FDRE \slv_reg20_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg20[11]),
        .R(SR));
  FDRE \slv_reg20_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg20[12]),
        .R(SR));
  FDRE \slv_reg20_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg20[13]),
        .R(SR));
  FDRE \slv_reg20_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg20[14]),
        .R(SR));
  FDRE \slv_reg20_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg20[15]),
        .R(SR));
  FDRE \slv_reg20_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg20[16]),
        .R(SR));
  FDRE \slv_reg20_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg20[17]),
        .R(SR));
  FDRE \slv_reg20_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg20[18]),
        .R(SR));
  FDRE \slv_reg20_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg20[19]),
        .R(SR));
  FDRE \slv_reg20_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg20[1]),
        .R(SR));
  FDRE \slv_reg20_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg20[20]),
        .R(SR));
  FDRE \slv_reg20_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg20[21]),
        .R(SR));
  FDRE \slv_reg20_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg20[22]),
        .R(SR));
  FDRE \slv_reg20_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg20[23]),
        .R(SR));
  FDRE \slv_reg20_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg20[24]),
        .R(SR));
  FDRE \slv_reg20_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg20[25]),
        .R(SR));
  FDRE \slv_reg20_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg20[26]),
        .R(SR));
  FDRE \slv_reg20_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg20[27]),
        .R(SR));
  FDRE \slv_reg20_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg20[28]),
        .R(SR));
  FDRE \slv_reg20_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg20[29]),
        .R(SR));
  FDRE \slv_reg20_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg20[2]),
        .R(SR));
  FDRE \slv_reg20_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg20[30]),
        .R(SR));
  FDRE \slv_reg20_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg20[31]),
        .R(SR));
  FDRE \slv_reg20_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg20[3]),
        .R(SR));
  FDRE \slv_reg20_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg20[4]),
        .R(SR));
  FDRE \slv_reg20_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg20[5]),
        .R(SR));
  FDRE \slv_reg20_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg20[6]),
        .R(SR));
  FDRE \slv_reg20_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg20[7]),
        .R(SR));
  FDRE \slv_reg20_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg20[8]),
        .R(SR));
  FDRE \slv_reg20_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg20[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg21[15]_i_1 
       (.I0(\slv_reg21[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg21[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg21[23]_i_1 
       (.I0(\slv_reg21[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg21[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg21[31]_i_1 
       (.I0(\slv_reg21[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg21[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    \slv_reg21[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg21[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg21[7]_i_1 
       (.I0(\slv_reg21[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg21[7]_i_1_n_0 ));
  FDRE \slv_reg21_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg21[0]),
        .R(SR));
  FDRE \slv_reg21_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg21[10]),
        .R(SR));
  FDRE \slv_reg21_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg21[11]),
        .R(SR));
  FDRE \slv_reg21_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg21[12]),
        .R(SR));
  FDRE \slv_reg21_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg21[13]),
        .R(SR));
  FDRE \slv_reg21_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg21[14]),
        .R(SR));
  FDRE \slv_reg21_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg21[15]),
        .R(SR));
  FDRE \slv_reg21_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg21[16]),
        .R(SR));
  FDRE \slv_reg21_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg21[17]),
        .R(SR));
  FDRE \slv_reg21_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg21[18]),
        .R(SR));
  FDRE \slv_reg21_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg21[19]),
        .R(SR));
  FDRE \slv_reg21_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg21[1]),
        .R(SR));
  FDRE \slv_reg21_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg21[20]),
        .R(SR));
  FDRE \slv_reg21_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg21[21]),
        .R(SR));
  FDRE \slv_reg21_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg21[22]),
        .R(SR));
  FDRE \slv_reg21_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg21[23]),
        .R(SR));
  FDRE \slv_reg21_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg21[24]),
        .R(SR));
  FDRE \slv_reg21_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg21[25]),
        .R(SR));
  FDRE \slv_reg21_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg21[26]),
        .R(SR));
  FDRE \slv_reg21_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg21[27]),
        .R(SR));
  FDRE \slv_reg21_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg21[28]),
        .R(SR));
  FDRE \slv_reg21_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg21[29]),
        .R(SR));
  FDRE \slv_reg21_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg21[2]),
        .R(SR));
  FDRE \slv_reg21_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg21[30]),
        .R(SR));
  FDRE \slv_reg21_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg21[31]),
        .R(SR));
  FDRE \slv_reg21_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg21[3]),
        .R(SR));
  FDRE \slv_reg21_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg21[4]),
        .R(SR));
  FDRE \slv_reg21_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg21[5]),
        .R(SR));
  FDRE \slv_reg21_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg21[6]),
        .R(SR));
  FDRE \slv_reg21_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg21[7]),
        .R(SR));
  FDRE \slv_reg21_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg21[8]),
        .R(SR));
  FDRE \slv_reg21_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg21[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg22[15]_i_1 
       (.I0(\slv_reg22[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg22[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg22[23]_i_1 
       (.I0(\slv_reg22[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg22[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg22[31]_i_1 
       (.I0(\slv_reg22[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg22[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000020000000)) 
    \slv_reg22[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg22[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg22[7]_i_1 
       (.I0(\slv_reg22[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg22[7]_i_1_n_0 ));
  FDRE \slv_reg22_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg22[0]),
        .R(SR));
  FDRE \slv_reg22_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg22[10]),
        .R(SR));
  FDRE \slv_reg22_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg22[11]),
        .R(SR));
  FDRE \slv_reg22_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg22[12]),
        .R(SR));
  FDRE \slv_reg22_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg22[13]),
        .R(SR));
  FDRE \slv_reg22_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg22[14]),
        .R(SR));
  FDRE \slv_reg22_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg22[15]),
        .R(SR));
  FDRE \slv_reg22_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg22[16]),
        .R(SR));
  FDRE \slv_reg22_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg22[17]),
        .R(SR));
  FDRE \slv_reg22_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg22[18]),
        .R(SR));
  FDRE \slv_reg22_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg22[19]),
        .R(SR));
  FDRE \slv_reg22_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg22[1]),
        .R(SR));
  FDRE \slv_reg22_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg22[20]),
        .R(SR));
  FDRE \slv_reg22_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg22[21]),
        .R(SR));
  FDRE \slv_reg22_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg22[22]),
        .R(SR));
  FDRE \slv_reg22_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg22[23]),
        .R(SR));
  FDRE \slv_reg22_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg22[24]),
        .R(SR));
  FDRE \slv_reg22_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg22[25]),
        .R(SR));
  FDRE \slv_reg22_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg22[26]),
        .R(SR));
  FDRE \slv_reg22_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg22[27]),
        .R(SR));
  FDRE \slv_reg22_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg22[28]),
        .R(SR));
  FDRE \slv_reg22_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg22[29]),
        .R(SR));
  FDRE \slv_reg22_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg22[2]),
        .R(SR));
  FDRE \slv_reg22_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg22[30]),
        .R(SR));
  FDRE \slv_reg22_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg22[31]),
        .R(SR));
  FDRE \slv_reg22_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg22[3]),
        .R(SR));
  FDRE \slv_reg22_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg22[4]),
        .R(SR));
  FDRE \slv_reg22_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg22[5]),
        .R(SR));
  FDRE \slv_reg22_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg22[6]),
        .R(SR));
  FDRE \slv_reg22_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg22[7]),
        .R(SR));
  FDRE \slv_reg22_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg22[8]),
        .R(SR));
  FDRE \slv_reg22_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg22[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg23[15]_i_1 
       (.I0(\slv_reg23[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg23[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg23[23]_i_1 
       (.I0(\slv_reg23[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg23[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg23[31]_i_1 
       (.I0(\slv_reg23[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg23[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \slv_reg23[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg23[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg23[7]_i_1 
       (.I0(\slv_reg23[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg23[7]_i_1_n_0 ));
  FDRE \slv_reg23_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg23[0]),
        .R(SR));
  FDRE \slv_reg23_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg23[10]),
        .R(SR));
  FDRE \slv_reg23_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg23[11]),
        .R(SR));
  FDRE \slv_reg23_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg23[12]),
        .R(SR));
  FDRE \slv_reg23_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg23[13]),
        .R(SR));
  FDRE \slv_reg23_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg23[14]),
        .R(SR));
  FDRE \slv_reg23_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg23[15]),
        .R(SR));
  FDRE \slv_reg23_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg23[16]),
        .R(SR));
  FDRE \slv_reg23_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg23[17]),
        .R(SR));
  FDRE \slv_reg23_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg23[18]),
        .R(SR));
  FDRE \slv_reg23_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg23[19]),
        .R(SR));
  FDRE \slv_reg23_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg23[1]),
        .R(SR));
  FDRE \slv_reg23_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg23[20]),
        .R(SR));
  FDRE \slv_reg23_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg23[21]),
        .R(SR));
  FDRE \slv_reg23_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg23[22]),
        .R(SR));
  FDRE \slv_reg23_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg23[23]),
        .R(SR));
  FDRE \slv_reg23_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg23[24]),
        .R(SR));
  FDRE \slv_reg23_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg23[25]),
        .R(SR));
  FDRE \slv_reg23_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg23[26]),
        .R(SR));
  FDRE \slv_reg23_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg23[27]),
        .R(SR));
  FDRE \slv_reg23_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg23[28]),
        .R(SR));
  FDRE \slv_reg23_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg23[29]),
        .R(SR));
  FDRE \slv_reg23_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg23[2]),
        .R(SR));
  FDRE \slv_reg23_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg23[30]),
        .R(SR));
  FDRE \slv_reg23_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg23[31]),
        .R(SR));
  FDRE \slv_reg23_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg23[3]),
        .R(SR));
  FDRE \slv_reg23_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg23[4]),
        .R(SR));
  FDRE \slv_reg23_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg23[5]),
        .R(SR));
  FDRE \slv_reg23_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg23[6]),
        .R(SR));
  FDRE \slv_reg23_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg23[7]),
        .R(SR));
  FDRE \slv_reg23_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg23[8]),
        .R(SR));
  FDRE \slv_reg23_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg23[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg24[15]_i_1 
       (.I0(\slv_reg24[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg24[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg24[23]_i_1 
       (.I0(\slv_reg24[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg24[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg24[31]_i_1 
       (.I0(\slv_reg24[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg24[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    \slv_reg24[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg24[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg24[7]_i_1 
       (.I0(\slv_reg24[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg24[7]_i_1_n_0 ));
  FDRE \slv_reg24_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg24[0]),
        .R(SR));
  FDRE \slv_reg24_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg24[10]),
        .R(SR));
  FDRE \slv_reg24_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg24[11]),
        .R(SR));
  FDRE \slv_reg24_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg24[12]),
        .R(SR));
  FDRE \slv_reg24_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg24[13]),
        .R(SR));
  FDRE \slv_reg24_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg24[14]),
        .R(SR));
  FDRE \slv_reg24_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg24[15]),
        .R(SR));
  FDRE \slv_reg24_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg24[16]),
        .R(SR));
  FDRE \slv_reg24_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg24[17]),
        .R(SR));
  FDRE \slv_reg24_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg24[18]),
        .R(SR));
  FDRE \slv_reg24_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg24[19]),
        .R(SR));
  FDRE \slv_reg24_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg24[1]),
        .R(SR));
  FDRE \slv_reg24_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg24[20]),
        .R(SR));
  FDRE \slv_reg24_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg24[21]),
        .R(SR));
  FDRE \slv_reg24_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg24[22]),
        .R(SR));
  FDRE \slv_reg24_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg24[23]),
        .R(SR));
  FDRE \slv_reg24_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg24[24]),
        .R(SR));
  FDRE \slv_reg24_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg24[25]),
        .R(SR));
  FDRE \slv_reg24_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg24[26]),
        .R(SR));
  FDRE \slv_reg24_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg24[27]),
        .R(SR));
  FDRE \slv_reg24_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg24[28]),
        .R(SR));
  FDRE \slv_reg24_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg24[29]),
        .R(SR));
  FDRE \slv_reg24_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg24[2]),
        .R(SR));
  FDRE \slv_reg24_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg24[30]),
        .R(SR));
  FDRE \slv_reg24_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg24[31]),
        .R(SR));
  FDRE \slv_reg24_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg24[3]),
        .R(SR));
  FDRE \slv_reg24_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg24[4]),
        .R(SR));
  FDRE \slv_reg24_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg24[5]),
        .R(SR));
  FDRE \slv_reg24_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg24[6]),
        .R(SR));
  FDRE \slv_reg24_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg24[7]),
        .R(SR));
  FDRE \slv_reg24_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg24[8]),
        .R(SR));
  FDRE \slv_reg24_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg24[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg25[15]_i_1 
       (.I0(\slv_reg25[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg25[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg25[23]_i_1 
       (.I0(\slv_reg25[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg25[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg25[31]_i_1 
       (.I0(\slv_reg25[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg25[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_reg25[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg25[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg25[7]_i_1 
       (.I0(\slv_reg25[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg25[7]_i_1_n_0 ));
  FDRE \slv_reg25_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg25[0]),
        .R(SR));
  FDRE \slv_reg25_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg25[10]),
        .R(SR));
  FDRE \slv_reg25_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg25[11]),
        .R(SR));
  FDRE \slv_reg25_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg25[12]),
        .R(SR));
  FDRE \slv_reg25_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg25[13]),
        .R(SR));
  FDRE \slv_reg25_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg25[14]),
        .R(SR));
  FDRE \slv_reg25_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg25[15]),
        .R(SR));
  FDRE \slv_reg25_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg25[16]),
        .R(SR));
  FDRE \slv_reg25_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg25[17]),
        .R(SR));
  FDRE \slv_reg25_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg25[18]),
        .R(SR));
  FDRE \slv_reg25_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg25[19]),
        .R(SR));
  FDRE \slv_reg25_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg25[1]),
        .R(SR));
  FDRE \slv_reg25_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg25[20]),
        .R(SR));
  FDRE \slv_reg25_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg25[21]),
        .R(SR));
  FDRE \slv_reg25_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg25[22]),
        .R(SR));
  FDRE \slv_reg25_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg25[23]),
        .R(SR));
  FDRE \slv_reg25_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg25[24]),
        .R(SR));
  FDRE \slv_reg25_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg25[25]),
        .R(SR));
  FDRE \slv_reg25_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg25[26]),
        .R(SR));
  FDRE \slv_reg25_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg25[27]),
        .R(SR));
  FDRE \slv_reg25_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg25[28]),
        .R(SR));
  FDRE \slv_reg25_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg25[29]),
        .R(SR));
  FDRE \slv_reg25_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg25[2]),
        .R(SR));
  FDRE \slv_reg25_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg25[30]),
        .R(SR));
  FDRE \slv_reg25_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg25[31]),
        .R(SR));
  FDRE \slv_reg25_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg25[3]),
        .R(SR));
  FDRE \slv_reg25_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg25[4]),
        .R(SR));
  FDRE \slv_reg25_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg25[5]),
        .R(SR));
  FDRE \slv_reg25_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg25[6]),
        .R(SR));
  FDRE \slv_reg25_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg25[7]),
        .R(SR));
  FDRE \slv_reg25_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg25[8]),
        .R(SR));
  FDRE \slv_reg25_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg25[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg26[15]_i_1 
       (.I0(\slv_reg26[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg26[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg26[23]_i_1 
       (.I0(\slv_reg26[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg26[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg26[31]_i_1 
       (.I0(\slv_reg26[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg26[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000800000)) 
    \slv_reg26[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg26[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg26[7]_i_1 
       (.I0(\slv_reg26[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg26[7]_i_1_n_0 ));
  FDRE \slv_reg26_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg26[0]),
        .R(SR));
  FDRE \slv_reg26_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg26[10]),
        .R(SR));
  FDRE \slv_reg26_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg26[11]),
        .R(SR));
  FDRE \slv_reg26_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg26[12]),
        .R(SR));
  FDRE \slv_reg26_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg26[13]),
        .R(SR));
  FDRE \slv_reg26_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg26[14]),
        .R(SR));
  FDRE \slv_reg26_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg26[15]),
        .R(SR));
  FDRE \slv_reg26_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg26[16]),
        .R(SR));
  FDRE \slv_reg26_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg26[17]),
        .R(SR));
  FDRE \slv_reg26_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg26[18]),
        .R(SR));
  FDRE \slv_reg26_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg26[19]),
        .R(SR));
  FDRE \slv_reg26_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg26[1]),
        .R(SR));
  FDRE \slv_reg26_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg26[20]),
        .R(SR));
  FDRE \slv_reg26_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg26[21]),
        .R(SR));
  FDRE \slv_reg26_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg26[22]),
        .R(SR));
  FDRE \slv_reg26_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg26[23]),
        .R(SR));
  FDRE \slv_reg26_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg26[24]),
        .R(SR));
  FDRE \slv_reg26_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg26[25]),
        .R(SR));
  FDRE \slv_reg26_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg26[26]),
        .R(SR));
  FDRE \slv_reg26_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg26[27]),
        .R(SR));
  FDRE \slv_reg26_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg26[28]),
        .R(SR));
  FDRE \slv_reg26_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg26[29]),
        .R(SR));
  FDRE \slv_reg26_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg26[2]),
        .R(SR));
  FDRE \slv_reg26_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg26[30]),
        .R(SR));
  FDRE \slv_reg26_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg26[31]),
        .R(SR));
  FDRE \slv_reg26_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg26[3]),
        .R(SR));
  FDRE \slv_reg26_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg26[4]),
        .R(SR));
  FDRE \slv_reg26_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg26[5]),
        .R(SR));
  FDRE \slv_reg26_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg26[6]),
        .R(SR));
  FDRE \slv_reg26_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg26[7]),
        .R(SR));
  FDRE \slv_reg26_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg26[8]),
        .R(SR));
  FDRE \slv_reg26_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg26[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg27[15]_i_1 
       (.I0(\slv_reg27[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg27[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg27[23]_i_1 
       (.I0(\slv_reg27[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg27[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg27[31]_i_1 
       (.I0(\slv_reg27[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg27[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \slv_reg27[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg27[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg27[7]_i_1 
       (.I0(\slv_reg27[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg27[7]_i_1_n_0 ));
  FDRE \slv_reg27_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg27[0]),
        .R(SR));
  FDRE \slv_reg27_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg27[10]),
        .R(SR));
  FDRE \slv_reg27_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg27[11]),
        .R(SR));
  FDRE \slv_reg27_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg27[12]),
        .R(SR));
  FDRE \slv_reg27_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg27[13]),
        .R(SR));
  FDRE \slv_reg27_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg27[14]),
        .R(SR));
  FDRE \slv_reg27_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg27[15]),
        .R(SR));
  FDRE \slv_reg27_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg27[16]),
        .R(SR));
  FDRE \slv_reg27_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg27[17]),
        .R(SR));
  FDRE \slv_reg27_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg27[18]),
        .R(SR));
  FDRE \slv_reg27_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg27[19]),
        .R(SR));
  FDRE \slv_reg27_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg27[1]),
        .R(SR));
  FDRE \slv_reg27_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg27[20]),
        .R(SR));
  FDRE \slv_reg27_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg27[21]),
        .R(SR));
  FDRE \slv_reg27_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg27[22]),
        .R(SR));
  FDRE \slv_reg27_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg27[23]),
        .R(SR));
  FDRE \slv_reg27_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg27[24]),
        .R(SR));
  FDRE \slv_reg27_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg27[25]),
        .R(SR));
  FDRE \slv_reg27_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg27[26]),
        .R(SR));
  FDRE \slv_reg27_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg27[27]),
        .R(SR));
  FDRE \slv_reg27_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg27[28]),
        .R(SR));
  FDRE \slv_reg27_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg27[29]),
        .R(SR));
  FDRE \slv_reg27_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg27[2]),
        .R(SR));
  FDRE \slv_reg27_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg27[30]),
        .R(SR));
  FDRE \slv_reg27_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg27[31]),
        .R(SR));
  FDRE \slv_reg27_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg27[3]),
        .R(SR));
  FDRE \slv_reg27_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg27[4]),
        .R(SR));
  FDRE \slv_reg27_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg27[5]),
        .R(SR));
  FDRE \slv_reg27_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg27[6]),
        .R(SR));
  FDRE \slv_reg27_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg27[7]),
        .R(SR));
  FDRE \slv_reg27_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg27[8]),
        .R(SR));
  FDRE \slv_reg27_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg27[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg28[15]_i_1 
       (.I0(\slv_reg28[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg28[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg28[23]_i_1 
       (.I0(\slv_reg28[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg28[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg28[31]_i_1 
       (.I0(\slv_reg28[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg28[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg28[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg28[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg28[7]_i_1 
       (.I0(\slv_reg28[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg28[7]_i_1_n_0 ));
  FDRE \slv_reg28_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg28[0]),
        .R(SR));
  FDRE \slv_reg28_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg28[10]),
        .R(SR));
  FDRE \slv_reg28_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg28[11]),
        .R(SR));
  FDRE \slv_reg28_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg28[12]),
        .R(SR));
  FDRE \slv_reg28_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg28[13]),
        .R(SR));
  FDRE \slv_reg28_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg28[14]),
        .R(SR));
  FDRE \slv_reg28_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg28[15]),
        .R(SR));
  FDRE \slv_reg28_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg28[16]),
        .R(SR));
  FDRE \slv_reg28_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg28[17]),
        .R(SR));
  FDRE \slv_reg28_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg28[18]),
        .R(SR));
  FDRE \slv_reg28_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg28[19]),
        .R(SR));
  FDRE \slv_reg28_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg28[1]),
        .R(SR));
  FDRE \slv_reg28_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg28[20]),
        .R(SR));
  FDRE \slv_reg28_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg28[21]),
        .R(SR));
  FDRE \slv_reg28_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg28[22]),
        .R(SR));
  FDRE \slv_reg28_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg28[23]),
        .R(SR));
  FDRE \slv_reg28_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg28[24]),
        .R(SR));
  FDRE \slv_reg28_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg28[25]),
        .R(SR));
  FDRE \slv_reg28_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg28[26]),
        .R(SR));
  FDRE \slv_reg28_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg28[27]),
        .R(SR));
  FDRE \slv_reg28_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg28[28]),
        .R(SR));
  FDRE \slv_reg28_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg28[29]),
        .R(SR));
  FDRE \slv_reg28_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg28[2]),
        .R(SR));
  FDRE \slv_reg28_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg28[30]),
        .R(SR));
  FDRE \slv_reg28_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg28[31]),
        .R(SR));
  FDRE \slv_reg28_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg28[3]),
        .R(SR));
  FDRE \slv_reg28_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg28[4]),
        .R(SR));
  FDRE \slv_reg28_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg28[5]),
        .R(SR));
  FDRE \slv_reg28_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg28[6]),
        .R(SR));
  FDRE \slv_reg28_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg28[7]),
        .R(SR));
  FDRE \slv_reg28_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg28[8]),
        .R(SR));
  FDRE \slv_reg28_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg28[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg29[15]_i_1 
       (.I0(\slv_reg29[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg29[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg29[23]_i_1 
       (.I0(\slv_reg29[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg29[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg29[31]_i_1 
       (.I0(\slv_reg29[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg29[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_reg29[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg29[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg29[7]_i_1 
       (.I0(\slv_reg29[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg29[7]_i_1_n_0 ));
  FDRE \slv_reg29_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg29[0]),
        .R(SR));
  FDRE \slv_reg29_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg29[10]),
        .R(SR));
  FDRE \slv_reg29_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg29[11]),
        .R(SR));
  FDRE \slv_reg29_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg29[12]),
        .R(SR));
  FDRE \slv_reg29_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg29[13]),
        .R(SR));
  FDRE \slv_reg29_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg29[14]),
        .R(SR));
  FDRE \slv_reg29_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg29[15]),
        .R(SR));
  FDRE \slv_reg29_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg29[16]),
        .R(SR));
  FDRE \slv_reg29_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg29[17]),
        .R(SR));
  FDRE \slv_reg29_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg29[18]),
        .R(SR));
  FDRE \slv_reg29_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg29[19]),
        .R(SR));
  FDRE \slv_reg29_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg29[1]),
        .R(SR));
  FDRE \slv_reg29_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg29[20]),
        .R(SR));
  FDRE \slv_reg29_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg29[21]),
        .R(SR));
  FDRE \slv_reg29_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg29[22]),
        .R(SR));
  FDRE \slv_reg29_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg29[23]),
        .R(SR));
  FDRE \slv_reg29_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg29[24]),
        .R(SR));
  FDRE \slv_reg29_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg29[25]),
        .R(SR));
  FDRE \slv_reg29_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg29[26]),
        .R(SR));
  FDRE \slv_reg29_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg29[27]),
        .R(SR));
  FDRE \slv_reg29_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg29[28]),
        .R(SR));
  FDRE \slv_reg29_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg29[29]),
        .R(SR));
  FDRE \slv_reg29_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg29[2]),
        .R(SR));
  FDRE \slv_reg29_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg29[30]),
        .R(SR));
  FDRE \slv_reg29_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg29[31]),
        .R(SR));
  FDRE \slv_reg29_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg29[3]),
        .R(SR));
  FDRE \slv_reg29_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg29[4]),
        .R(SR));
  FDRE \slv_reg29_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg29[5]),
        .R(SR));
  FDRE \slv_reg29_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg29[6]),
        .R(SR));
  FDRE \slv_reg29_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg29[7]),
        .R(SR));
  FDRE \slv_reg29_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg29[8]),
        .R(SR));
  FDRE \slv_reg29_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg29[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg2[15]_i_1 
       (.I0(\slv_reg2[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg2[23]_i_1 
       (.I0(\slv_reg2[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg2[31]_i_1 
       (.I0(\slv_reg2[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000100000)) 
    \slv_reg2[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg2[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg2[7]_i_1 
       (.I0(\slv_reg2[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(SR));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(SR));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(SR));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(SR));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(SR));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(SR));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(SR));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2__0[16]),
        .R(SR));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2__0[17]),
        .R(SR));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2__0[18]),
        .R(SR));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2__0[19]),
        .R(SR));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(SR));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2__0[20]),
        .R(SR));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2__0[21]),
        .R(SR));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2__0[22]),
        .R(SR));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2__0[23]),
        .R(SR));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2__0[24]),
        .R(SR));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2__0[25]),
        .R(SR));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2__0[26]),
        .R(SR));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2__0[27]),
        .R(SR));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2__0[28]),
        .R(SR));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2__0[29]),
        .R(SR));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(SR));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2__0[30]),
        .R(SR));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2__0[31]),
        .R(SR));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(SR));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(SR));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(SR));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(SR));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(SR));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(SR));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg30[15]_i_1 
       (.I0(\slv_reg30[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg30[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg30[23]_i_1 
       (.I0(\slv_reg30[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg30[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg30[31]_i_1 
       (.I0(\slv_reg30[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg30[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg30[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg30[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg30[7]_i_1 
       (.I0(\slv_reg30[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg30[7]_i_1_n_0 ));
  FDRE \slv_reg30_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg30[0]),
        .R(SR));
  FDRE \slv_reg30_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg30[10]),
        .R(SR));
  FDRE \slv_reg30_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg30[11]),
        .R(SR));
  FDRE \slv_reg30_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg30[12]),
        .R(SR));
  FDRE \slv_reg30_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg30[13]),
        .R(SR));
  FDRE \slv_reg30_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg30[14]),
        .R(SR));
  FDRE \slv_reg30_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg30[15]),
        .R(SR));
  FDRE \slv_reg30_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg30[16]),
        .R(SR));
  FDRE \slv_reg30_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg30[17]),
        .R(SR));
  FDRE \slv_reg30_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg30[18]),
        .R(SR));
  FDRE \slv_reg30_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg30[19]),
        .R(SR));
  FDRE \slv_reg30_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg30[1]),
        .R(SR));
  FDRE \slv_reg30_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg30[20]),
        .R(SR));
  FDRE \slv_reg30_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg30[21]),
        .R(SR));
  FDRE \slv_reg30_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg30[22]),
        .R(SR));
  FDRE \slv_reg30_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg30[23]),
        .R(SR));
  FDRE \slv_reg30_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg30[24]),
        .R(SR));
  FDRE \slv_reg30_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg30[25]),
        .R(SR));
  FDRE \slv_reg30_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg30[26]),
        .R(SR));
  FDRE \slv_reg30_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg30[27]),
        .R(SR));
  FDRE \slv_reg30_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg30[28]),
        .R(SR));
  FDRE \slv_reg30_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg30[29]),
        .R(SR));
  FDRE \slv_reg30_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg30[2]),
        .R(SR));
  FDRE \slv_reg30_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg30[30]),
        .R(SR));
  FDRE \slv_reg30_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg30[31]),
        .R(SR));
  FDRE \slv_reg30_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg30[3]),
        .R(SR));
  FDRE \slv_reg30_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg30[4]),
        .R(SR));
  FDRE \slv_reg30_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg30[5]),
        .R(SR));
  FDRE \slv_reg30_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg30[6]),
        .R(SR));
  FDRE \slv_reg30_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg30[7]),
        .R(SR));
  FDRE \slv_reg30_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg30[8]),
        .R(SR));
  FDRE \slv_reg30_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg30[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg31[15]_i_1 
       (.I0(\slv_reg31[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg31[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg31[23]_i_1 
       (.I0(\slv_reg31[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg31[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg31[31]_i_1 
       (.I0(\slv_reg31[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg31[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg31[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg31[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg31[7]_i_1 
       (.I0(\slv_reg31[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg31[7]_i_1_n_0 ));
  FDRE \slv_reg31_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg31[0]),
        .R(SR));
  FDRE \slv_reg31_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg31[10]),
        .R(SR));
  FDRE \slv_reg31_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg31[11]),
        .R(SR));
  FDRE \slv_reg31_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg31[12]),
        .R(SR));
  FDRE \slv_reg31_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg31[13]),
        .R(SR));
  FDRE \slv_reg31_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg31[14]),
        .R(SR));
  FDRE \slv_reg31_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg31[15]),
        .R(SR));
  FDRE \slv_reg31_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg31[16]),
        .R(SR));
  FDRE \slv_reg31_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg31[17]),
        .R(SR));
  FDRE \slv_reg31_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg31[18]),
        .R(SR));
  FDRE \slv_reg31_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg31[19]),
        .R(SR));
  FDRE \slv_reg31_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg31[1]),
        .R(SR));
  FDRE \slv_reg31_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg31[20]),
        .R(SR));
  FDRE \slv_reg31_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg31[21]),
        .R(SR));
  FDRE \slv_reg31_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg31[22]),
        .R(SR));
  FDRE \slv_reg31_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg31[23]),
        .R(SR));
  FDRE \slv_reg31_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg31[24]),
        .R(SR));
  FDRE \slv_reg31_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg31[25]),
        .R(SR));
  FDRE \slv_reg31_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg31[26]),
        .R(SR));
  FDRE \slv_reg31_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg31[27]),
        .R(SR));
  FDRE \slv_reg31_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg31[28]),
        .R(SR));
  FDRE \slv_reg31_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg31[29]),
        .R(SR));
  FDRE \slv_reg31_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg31[2]),
        .R(SR));
  FDRE \slv_reg31_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg31[30]),
        .R(SR));
  FDRE \slv_reg31_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg31[31]),
        .R(SR));
  FDRE \slv_reg31_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg31[3]),
        .R(SR));
  FDRE \slv_reg31_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg31[4]),
        .R(SR));
  FDRE \slv_reg31_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg31[5]),
        .R(SR));
  FDRE \slv_reg31_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg31[6]),
        .R(SR));
  FDRE \slv_reg31_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg31[7]),
        .R(SR));
  FDRE \slv_reg31_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg31[8]),
        .R(SR));
  FDRE \slv_reg31_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg31[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg3[15]_i_1 
       (.I0(\slv_reg3[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg3[23]_i_1 
       (.I0(\slv_reg3[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg3[31]_i_1 
       (.I0(\slv_reg3[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    \slv_reg3[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg3[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg3[7]_i_1 
       (.I0(\slv_reg3[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(SR));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3__0[10]),
        .R(SR));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3__0[11]),
        .R(SR));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3__0[12]),
        .R(SR));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3__0[13]),
        .R(SR));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3__0[14]),
        .R(SR));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3__0[15]),
        .R(SR));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3__0[16]),
        .R(SR));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3__0[17]),
        .R(SR));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3__0[18]),
        .R(SR));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3__0[19]),
        .R(SR));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(SR));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3__0[20]),
        .R(SR));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3__0[21]),
        .R(SR));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3__0[22]),
        .R(SR));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3__0[23]),
        .R(SR));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3__0[24]),
        .R(SR));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3__0[25]),
        .R(SR));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3__0[26]),
        .R(SR));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3__0[27]),
        .R(SR));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3__0[28]),
        .R(SR));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3__0[29]),
        .R(SR));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(SR));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3__0[30]),
        .R(SR));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3__0[31]),
        .R(SR));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(SR));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(SR));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(SR));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(SR));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(SR));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3__0[8]),
        .R(SR));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3__0[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg4[15]_i_1 
       (.I0(\slv_reg4[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg4[23]_i_1 
       (.I0(\slv_reg4[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg4[31]_i_1 
       (.I0(\slv_reg4[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \slv_reg4[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg4[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg4[7]_i_1 
       (.I0(\slv_reg4[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg4),
        .R(SR));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4__0[10]),
        .R(SR));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4__0[11]),
        .R(SR));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4__0[12]),
        .R(SR));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4__0[13]),
        .R(SR));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4__0[14]),
        .R(SR));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4__0[15]),
        .R(SR));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4__0[16]),
        .R(SR));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4__0[17]),
        .R(SR));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4__0[18]),
        .R(SR));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4__0[19]),
        .R(SR));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg4__0[1]),
        .R(SR));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4__0[20]),
        .R(SR));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4__0[21]),
        .R(SR));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4__0[22]),
        .R(SR));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4__0[23]),
        .R(SR));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4__0[24]),
        .R(SR));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4__0[25]),
        .R(SR));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4__0[26]),
        .R(SR));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4__0[27]),
        .R(SR));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4__0[28]),
        .R(SR));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4__0[29]),
        .R(SR));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg4__0[2]),
        .R(SR));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4__0[30]),
        .R(SR));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4__0[31]),
        .R(SR));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg4__0[3]),
        .R(SR));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg4__0[4]),
        .R(SR));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg4__0[5]),
        .R(SR));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4__0[6]),
        .R(SR));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4__0[7]),
        .R(SR));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4__0[8]),
        .R(SR));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4__0[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg5[15]_i_1 
       (.I0(\slv_reg5[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\slv_reg5[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg5[23]_i_1 
       (.I0(\slv_reg5[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\slv_reg5[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg5[31]_i_1 
       (.I0(\slv_reg5[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\slv_reg5[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000100000000000)) 
    \slv_reg5[31]_i_2 
       (.I0(p_0_in[4]),
        .I1(p_0_in[3]),
        .I2(\slv_reg0[31]_i_3_n_0 ),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[0]),
        .O(\slv_reg5[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg5[7]_i_1 
       (.I0(\slv_reg5[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\slv_reg5[7]_i_1_n_0 ));
  FDRE \slv_reg5_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg5),
        .R(SR));
  FDRE \slv_reg5_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg5__0[10]),
        .R(SR));
  FDRE \slv_reg5_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg5__0[11]),
        .R(SR));
  FDRE \slv_reg5_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg5__0[12]),
        .R(SR));
  FDRE \slv_reg5_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg5__0[13]),
        .R(SR));
  FDRE \slv_reg5_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg5__0[14]),
        .R(SR));
  FDRE \slv_reg5_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg5__0[15]),
        .R(SR));
  FDRE \slv_reg5_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg5__0[16]),
        .R(SR));
  FDRE \slv_reg5_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg5__0[17]),
        .R(SR));
  FDRE \slv_reg5_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg5__0[18]),
        .R(SR));
  FDRE \slv_reg5_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg5__0[19]),
        .R(SR));
  FDRE \slv_reg5_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg5__0[1]),
        .R(SR));
  FDRE \slv_reg5_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg5__0[20]),
        .R(SR));
  FDRE \slv_reg5_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg5__0[21]),
        .R(SR));
  FDRE \slv_reg5_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg5__0[22]),
        .R(SR));
  FDRE \slv_reg5_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg5__0[23]),
        .R(SR));
  FDRE \slv_reg5_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg5__0[24]),
        .R(SR));
  FDRE \slv_reg5_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg5__0[25]),
        .R(SR));
  FDRE \slv_reg5_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg5__0[26]),
        .R(SR));
  FDRE \slv_reg5_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg5__0[27]),
        .R(SR));
  FDRE \slv_reg5_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg5__0[28]),
        .R(SR));
  FDRE \slv_reg5_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg5__0[29]),
        .R(SR));
  FDRE \slv_reg5_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg5__0[2]),
        .R(SR));
  FDRE \slv_reg5_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg5__0[30]),
        .R(SR));
  FDRE \slv_reg5_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg5__0[31]),
        .R(SR));
  FDRE \slv_reg5_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg5__0[3]),
        .R(SR));
  FDRE \slv_reg5_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg5__0[4]),
        .R(SR));
  FDRE \slv_reg5_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg5__0[5]),
        .R(SR));
  FDRE \slv_reg5_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg5__0[6]),
        .R(SR));
  FDRE \slv_reg5_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg5__0[7]),
        .R(SR));
  FDRE \slv_reg5_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg5__0[8]),
        .R(SR));
  FDRE \slv_reg5_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg5__0[9]),
        .R(SR));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample_counter
   (\Q_reg[2] ,
    ADDRBWRADDR,
    \slv_reg0_reg[9] ,
    Q,
    \state_reg[1] ,
    E,
    clk);
  output [0:0]\Q_reg[2] ;
  output [9:0]ADDRBWRADDR;
  input [9:0]\slv_reg0_reg[9] ;
  input [0:0]Q;
  input [0:0]\state_reg[1] ;
  input [0:0]E;
  input clk;

  wire [9:0]ADDRBWRADDR;
  wire [0:0]E;
  wire [0:0]Q;
  wire [0:0]\Q_reg[2] ;
  wire clk;
  wire [9:0]plusOp__0;
  wire \processQ[9]_i_4_n_0 ;
  wire [9:0]\slv_reg0_reg[9] ;
  wire [0:0]\state_reg[1] ;
  wire [9:0]write_cntr;

  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \Q[2]_i_2 
       (.I0(write_cntr[9]),
        .I1(write_cntr[7]),
        .I2(\processQ[9]_i_4_n_0 ),
        .I3(write_cntr[6]),
        .I4(write_cntr[8]),
        .O(\Q_reg[2] ));
  LUT1 #(
    .INIT(2'h1)) 
    \processQ[0]_i_1 
       (.I0(write_cntr[0]),
        .O(plusOp__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \processQ[1]_i_1 
       (.I0(write_cntr[1]),
        .I1(write_cntr[0]),
        .O(plusOp__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \processQ[2]_i_1 
       (.I0(write_cntr[2]),
        .I1(write_cntr[0]),
        .I2(write_cntr[1]),
        .O(plusOp__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processQ[3]_i_1 
       (.I0(write_cntr[3]),
        .I1(write_cntr[1]),
        .I2(write_cntr[0]),
        .I3(write_cntr[2]),
        .O(plusOp__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[4]_i_1 
       (.I0(write_cntr[4]),
        .I1(write_cntr[2]),
        .I2(write_cntr[0]),
        .I3(write_cntr[1]),
        .I4(write_cntr[3]),
        .O(plusOp__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processQ[5]_i_1 
       (.I0(write_cntr[5]),
        .I1(write_cntr[3]),
        .I2(write_cntr[1]),
        .I3(write_cntr[0]),
        .I4(write_cntr[2]),
        .I5(write_cntr[4]),
        .O(plusOp__0[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \processQ[6]_i_1 
       (.I0(write_cntr[6]),
        .I1(\processQ[9]_i_4_n_0 ),
        .O(plusOp__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \processQ[7]_i_1 
       (.I0(write_cntr[7]),
        .I1(\processQ[9]_i_4_n_0 ),
        .I2(write_cntr[6]),
        .O(plusOp__0[7]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processQ[8]_i_1 
       (.I0(write_cntr[8]),
        .I1(write_cntr[6]),
        .I2(\processQ[9]_i_4_n_0 ),
        .I3(write_cntr[7]),
        .O(plusOp__0[8]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[9]_i_3 
       (.I0(write_cntr[9]),
        .I1(write_cntr[7]),
        .I2(\processQ[9]_i_4_n_0 ),
        .I3(write_cntr[6]),
        .I4(write_cntr[8]),
        .O(plusOp__0[9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \processQ[9]_i_4 
       (.I0(write_cntr[5]),
        .I1(write_cntr[3]),
        .I2(write_cntr[1]),
        .I3(write_cntr[0]),
        .I4(write_cntr[2]),
        .I5(write_cntr[4]),
        .O(\processQ[9]_i_4_n_0 ));
  FDRE \processQ_reg[0] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[0]),
        .Q(write_cntr[0]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[1] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[1]),
        .Q(write_cntr[1]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[2] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[2]),
        .Q(write_cntr[2]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[3] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[3]),
        .Q(write_cntr[3]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[4] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[4]),
        .Q(write_cntr[4]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[5] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[5]),
        .Q(write_cntr[5]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[6] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[6]),
        .Q(write_cntr[6]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[7] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[7]),
        .Q(write_cntr[7]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[8] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[8]),
        .Q(write_cntr[8]),
        .R(\state_reg[1] ));
  FDRE \processQ_reg[9] 
       (.C(clk),
        .CE(E),
        .D(plusOp__0[9]),
        .Q(write_cntr[9]),
        .R(\state_reg[1] ));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_10 
       (.I0(\slv_reg0_reg[9] [9]),
        .I1(Q),
        .I2(write_cntr[9]),
        .O(ADDRBWRADDR[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_11 
       (.I0(\slv_reg0_reg[9] [8]),
        .I1(Q),
        .I2(write_cntr[8]),
        .O(ADDRBWRADDR[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_12 
       (.I0(\slv_reg0_reg[9] [7]),
        .I1(Q),
        .I2(write_cntr[7]),
        .O(ADDRBWRADDR[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_13 
       (.I0(\slv_reg0_reg[9] [6]),
        .I1(Q),
        .I2(write_cntr[6]),
        .O(ADDRBWRADDR[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_14 
       (.I0(\slv_reg0_reg[9] [5]),
        .I1(Q),
        .I2(write_cntr[5]),
        .O(ADDRBWRADDR[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_15 
       (.I0(\slv_reg0_reg[9] [4]),
        .I1(Q),
        .I2(write_cntr[4]),
        .O(ADDRBWRADDR[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_16 
       (.I0(\slv_reg0_reg[9] [3]),
        .I1(Q),
        .I2(write_cntr[3]),
        .O(ADDRBWRADDR[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_17 
       (.I0(\slv_reg0_reg[9] [2]),
        .I1(Q),
        .I2(write_cntr[2]),
        .O(ADDRBWRADDR[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_18 
       (.I0(\slv_reg0_reg[9] [1]),
        .I1(Q),
        .I2(write_cntr[1]),
        .O(ADDRBWRADDR[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_19 
       (.I0(\slv_reg0_reg[9] [0]),
        .I1(Q),
        .I2(write_cntr[0]),
        .O(ADDRBWRADDR[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_scopeFace
   (CO,
    \encoded_reg[8] ,
    \encoded_reg[8]_0 ,
    \encoded_reg[8]_1 ,
    \encoded_reg[8]_2 ,
    \encoded_reg[8]_3 ,
    \encoded_reg[8]_4 ,
    \encoded_reg[0] ,
    \encoded_reg[0]_0 ,
    \encoded_reg[0]_1 ,
    \encoded_reg[8]_5 ,
    \int_trigger_time_s_reg[0] ,
    \encoded_reg[8]_6 ,
    \int_trigger_time_s_reg[0]_0 ,
    \int_trigger_time_s_reg[0]_1 ,
    \encoded_reg[8]_7 ,
    \encoded_reg[8]_8 ,
    \int_trigger_time_s_reg[0]_2 ,
    \encoded_reg[8]_9 ,
    \encoded_reg[8]_10 ,
    \encoded_reg[8]_11 ,
    \encoded_reg[8]_12 ,
    \encoded_reg[8]_13 ,
    \int_trigger_time_s_reg[0]_3 ,
    \int_trigger_time_s_reg[0]_4 ,
    \encoded_reg[8]_14 ,
    \encoded_reg[8]_15 ,
    \encoded_reg[0]_2 ,
    \encoded_reg[8]_16 ,
    \int_trigger_time_s_reg[0]_5 ,
    \int_trigger_time_s_reg[0]_6 ,
    \encoded_reg[8]_17 ,
    \encoded_reg[8]_18 ,
    \encoded_reg[8]_19 ,
    \encoded_reg[8]_20 ,
    DI,
    S,
    \processQ_reg[9] ,
    \processQ_reg[9]_0 ,
    \processQ_reg[7] ,
    \processQ_reg[7]_0 ,
    \processQ_reg[9]_1 ,
    \processQ_reg[9]_2 ,
    \processQ_reg[7]_1 ,
    \processQ_reg[7]_2 ,
    \processQ_reg[9]_3 ,
    \processQ_reg[9]_4 ,
    \processQ_reg[7]_3 ,
    \processQ_reg[7]_4 ,
    \processQ_reg[9]_5 ,
    \processQ_reg[9]_6 ,
    \slv_reg11_reg[9] ,
    \processQ_reg[7]_5 ,
    \processQ_reg[7]_6 ,
    \processQ_reg[9]_7 ,
    \processQ_reg[9]_8 ,
    \processQ_reg[7]_7 ,
    \processQ_reg[7]_8 ,
    \processQ_reg[9]_9 ,
    \processQ_reg[9]_10 ,
    \processQ_reg[7]_9 ,
    \processQ_reg[7]_10 ,
    \processQ_reg[9]_11 ,
    \processQ_reg[9]_12 ,
    \processQ_reg[7]_11 ,
    \processQ_reg[7]_12 ,
    \processQ_reg[9]_13 ,
    \processQ_reg[9]_14 ,
    \processQ_reg[7]_13 ,
    \processQ_reg[7]_14 ,
    \processQ_reg[9]_15 ,
    \processQ_reg[9]_16 ,
    \processQ_reg[7]_15 ,
    \processQ_reg[7]_16 ,
    \processQ_reg[9]_17 ,
    \processQ_reg[9]_18 ,
    \slv_reg10_reg[9] ,
    \processQ_reg[7]_17 ,
    \processQ_reg[7]_18 ,
    \processQ_reg[9]_19 ,
    \processQ_reg[9]_20 ,
    \processQ_reg[7]_19 ,
    \processQ_reg[7]_20 ,
    \processQ_reg[9]_21 ,
    \processQ_reg[9]_22 ,
    \slv_reg10_reg[5] ,
    \slv_reg10_reg[3] ,
    Q,
    \slv_reg10_reg[9]_0 ,
    \slv_reg5_reg[0] ,
    \int_trigger_volt_s_reg[7] ,
    \slv_reg11_reg[7] ,
    \slv_reg11_reg[5] ,
    \slv_reg11_reg[3] ,
    \slv_reg11_reg[1] ,
    \slv_reg11_reg[0] ,
    \slv_reg11_reg[2] ,
    \slv_reg11_reg[4] ,
    \processQ_reg[1] );
  output [0:0]CO;
  output [0:0]\encoded_reg[8] ;
  output [0:0]\encoded_reg[8]_0 ;
  output [0:0]\encoded_reg[8]_1 ;
  output [0:0]\encoded_reg[8]_2 ;
  output [0:0]\encoded_reg[8]_3 ;
  output [0:0]\encoded_reg[8]_4 ;
  output [0:0]\encoded_reg[0] ;
  output [0:0]\encoded_reg[0]_0 ;
  output [0:0]\encoded_reg[0]_1 ;
  output \encoded_reg[8]_5 ;
  output \int_trigger_time_s_reg[0] ;
  output \encoded_reg[8]_6 ;
  output \int_trigger_time_s_reg[0]_0 ;
  output \int_trigger_time_s_reg[0]_1 ;
  output \encoded_reg[8]_7 ;
  output \encoded_reg[8]_8 ;
  output \int_trigger_time_s_reg[0]_2 ;
  output \encoded_reg[8]_9 ;
  output \encoded_reg[8]_10 ;
  output \encoded_reg[8]_11 ;
  output \encoded_reg[8]_12 ;
  output \encoded_reg[8]_13 ;
  output \int_trigger_time_s_reg[0]_3 ;
  output \int_trigger_time_s_reg[0]_4 ;
  output \encoded_reg[8]_14 ;
  output \encoded_reg[8]_15 ;
  output \encoded_reg[0]_2 ;
  output \encoded_reg[8]_16 ;
  output \int_trigger_time_s_reg[0]_5 ;
  output \int_trigger_time_s_reg[0]_6 ;
  output \encoded_reg[8]_17 ;
  output \encoded_reg[8]_18 ;
  output \encoded_reg[8]_19 ;
  output \encoded_reg[8]_20 ;
  input [3:0]DI;
  input [3:0]S;
  input [0:0]\processQ_reg[9] ;
  input [0:0]\processQ_reg[9]_0 ;
  input [3:0]\processQ_reg[7] ;
  input [3:0]\processQ_reg[7]_0 ;
  input [0:0]\processQ_reg[9]_1 ;
  input [0:0]\processQ_reg[9]_2 ;
  input [3:0]\processQ_reg[7]_1 ;
  input [3:0]\processQ_reg[7]_2 ;
  input [0:0]\processQ_reg[9]_3 ;
  input [0:0]\processQ_reg[9]_4 ;
  input [3:0]\processQ_reg[7]_3 ;
  input [3:0]\processQ_reg[7]_4 ;
  input [0:0]\processQ_reg[9]_5 ;
  input [0:0]\processQ_reg[9]_6 ;
  input [3:0]\slv_reg11_reg[9] ;
  input [3:0]\processQ_reg[7]_5 ;
  input [3:0]\processQ_reg[7]_6 ;
  input [0:0]\processQ_reg[9]_7 ;
  input [0:0]\processQ_reg[9]_8 ;
  input [3:0]\processQ_reg[7]_7 ;
  input [3:0]\processQ_reg[7]_8 ;
  input [0:0]\processQ_reg[9]_9 ;
  input [0:0]\processQ_reg[9]_10 ;
  input [3:0]\processQ_reg[7]_9 ;
  input [3:0]\processQ_reg[7]_10 ;
  input [0:0]\processQ_reg[9]_11 ;
  input [0:0]\processQ_reg[9]_12 ;
  input [3:0]\processQ_reg[7]_11 ;
  input [3:0]\processQ_reg[7]_12 ;
  input [0:0]\processQ_reg[9]_13 ;
  input [0:0]\processQ_reg[9]_14 ;
  input [3:0]\processQ_reg[7]_13 ;
  input [3:0]\processQ_reg[7]_14 ;
  input [0:0]\processQ_reg[9]_15 ;
  input [0:0]\processQ_reg[9]_16 ;
  input [3:0]\processQ_reg[7]_15 ;
  input [3:0]\processQ_reg[7]_16 ;
  input [0:0]\processQ_reg[9]_17 ;
  input [0:0]\processQ_reg[9]_18 ;
  input [3:0]\slv_reg10_reg[9] ;
  input [3:0]\processQ_reg[7]_17 ;
  input [3:0]\processQ_reg[7]_18 ;
  input [0:0]\processQ_reg[9]_19 ;
  input [0:0]\processQ_reg[9]_20 ;
  input [3:0]\processQ_reg[7]_19 ;
  input [3:0]\processQ_reg[7]_20 ;
  input [0:0]\processQ_reg[9]_21 ;
  input [0:0]\processQ_reg[9]_22 ;
  input \slv_reg10_reg[5] ;
  input \slv_reg10_reg[3] ;
  input [9:0]Q;
  input [9:0]\slv_reg10_reg[9]_0 ;
  input [0:0]\slv_reg5_reg[0] ;
  input [3:0]\int_trigger_volt_s_reg[7] ;
  input [3:0]\slv_reg11_reg[7] ;
  input \slv_reg11_reg[5] ;
  input \slv_reg11_reg[3] ;
  input \slv_reg11_reg[1] ;
  input \slv_reg11_reg[0] ;
  input \slv_reg11_reg[2] ;
  input \slv_reg11_reg[4] ;
  input [1:0]\processQ_reg[1] ;

  wire [0:0]CO;
  wire [3:0]DI;
  wire [9:0]Q;
  wire [3:0]S;
  wire [0:0]\encoded_reg[0] ;
  wire [0:0]\encoded_reg[0]_0 ;
  wire [0:0]\encoded_reg[0]_1 ;
  wire \encoded_reg[0]_2 ;
  wire [0:0]\encoded_reg[8] ;
  wire [0:0]\encoded_reg[8]_0 ;
  wire [0:0]\encoded_reg[8]_1 ;
  wire \encoded_reg[8]_10 ;
  wire \encoded_reg[8]_11 ;
  wire \encoded_reg[8]_12 ;
  wire \encoded_reg[8]_13 ;
  wire \encoded_reg[8]_14 ;
  wire \encoded_reg[8]_15 ;
  wire \encoded_reg[8]_16 ;
  wire \encoded_reg[8]_17 ;
  wire \encoded_reg[8]_18 ;
  wire \encoded_reg[8]_19 ;
  wire [0:0]\encoded_reg[8]_2 ;
  wire \encoded_reg[8]_20 ;
  wire [0:0]\encoded_reg[8]_3 ;
  wire [0:0]\encoded_reg[8]_4 ;
  wire \encoded_reg[8]_5 ;
  wire \encoded_reg[8]_6 ;
  wire \encoded_reg[8]_7 ;
  wire \encoded_reg[8]_8 ;
  wire \encoded_reg[8]_9 ;
  wire \int_trigger_time_s_reg[0] ;
  wire \int_trigger_time_s_reg[0]_0 ;
  wire \int_trigger_time_s_reg[0]_1 ;
  wire \int_trigger_time_s_reg[0]_2 ;
  wire \int_trigger_time_s_reg[0]_3 ;
  wire \int_trigger_time_s_reg[0]_4 ;
  wire \int_trigger_time_s_reg[0]_5 ;
  wire \int_trigger_time_s_reg[0]_6 ;
  wire [3:0]\int_trigger_volt_s_reg[7] ;
  wire \pixel_color3_inferred__0/i__carry_n_1 ;
  wire \pixel_color3_inferred__0/i__carry_n_2 ;
  wire \pixel_color3_inferred__0/i__carry_n_3 ;
  wire \pixel_color3_inferred__1/i__carry_n_1 ;
  wire \pixel_color3_inferred__1/i__carry_n_2 ;
  wire \pixel_color3_inferred__1/i__carry_n_3 ;
  wire pixel_color424_in;
  wire pixel_color4_carry_n_0;
  wire pixel_color4_carry_n_1;
  wire pixel_color4_carry_n_2;
  wire pixel_color4_carry_n_3;
  wire \pixel_color4_inferred__0/i__carry_n_0 ;
  wire \pixel_color4_inferred__0/i__carry_n_1 ;
  wire \pixel_color4_inferred__0/i__carry_n_2 ;
  wire \pixel_color4_inferred__0/i__carry_n_3 ;
  wire pixel_color523_in;
  wire pixel_color532_in;
  wire pixel_color5_carry_n_0;
  wire pixel_color5_carry_n_1;
  wire pixel_color5_carry_n_2;
  wire pixel_color5_carry_n_3;
  wire \pixel_color5_inferred__0/i__carry_n_0 ;
  wire \pixel_color5_inferred__0/i__carry_n_1 ;
  wire \pixel_color5_inferred__0/i__carry_n_2 ;
  wire \pixel_color5_inferred__0/i__carry_n_3 ;
  wire \pixel_color5_inferred__1/i__carry_n_0 ;
  wire \pixel_color5_inferred__1/i__carry_n_1 ;
  wire \pixel_color5_inferred__1/i__carry_n_2 ;
  wire \pixel_color5_inferred__1/i__carry_n_3 ;
  wire \pixel_color5_inferred__2/i__carry_n_0 ;
  wire \pixel_color5_inferred__2/i__carry_n_1 ;
  wire \pixel_color5_inferred__2/i__carry_n_2 ;
  wire \pixel_color5_inferred__2/i__carry_n_3 ;
  wire \pixel_color5_inferred__3/i__carry_n_0 ;
  wire \pixel_color5_inferred__3/i__carry_n_1 ;
  wire \pixel_color5_inferred__3/i__carry_n_2 ;
  wire \pixel_color5_inferred__3/i__carry_n_3 ;
  wire \pixel_color5_inferred__4/i__carry_n_0 ;
  wire \pixel_color5_inferred__4/i__carry_n_1 ;
  wire \pixel_color5_inferred__4/i__carry_n_2 ;
  wire \pixel_color5_inferred__4/i__carry_n_3 ;
  wire pixel_color631_in;
  wire \pixel_color6_inferred__1/i__carry_n_0 ;
  wire \pixel_color6_inferred__1/i__carry_n_1 ;
  wire \pixel_color6_inferred__1/i__carry_n_2 ;
  wire \pixel_color6_inferred__1/i__carry_n_3 ;
  wire \pixel_color6_inferred__2/i__carry_n_0 ;
  wire \pixel_color6_inferred__2/i__carry_n_1 ;
  wire \pixel_color6_inferred__2/i__carry_n_2 ;
  wire \pixel_color6_inferred__2/i__carry_n_3 ;
  wire \pixel_color6_inferred__3/i__carry_n_0 ;
  wire \pixel_color6_inferred__3/i__carry_n_1 ;
  wire \pixel_color6_inferred__3/i__carry_n_2 ;
  wire \pixel_color6_inferred__3/i__carry_n_3 ;
  wire \pixel_color6_inferred__4/i__carry_n_0 ;
  wire \pixel_color6_inferred__4/i__carry_n_1 ;
  wire \pixel_color6_inferred__4/i__carry_n_2 ;
  wire \pixel_color6_inferred__4/i__carry_n_3 ;
  wire [1:0]\processQ_reg[1] ;
  wire [3:0]\processQ_reg[7] ;
  wire [3:0]\processQ_reg[7]_0 ;
  wire [3:0]\processQ_reg[7]_1 ;
  wire [3:0]\processQ_reg[7]_10 ;
  wire [3:0]\processQ_reg[7]_11 ;
  wire [3:0]\processQ_reg[7]_12 ;
  wire [3:0]\processQ_reg[7]_13 ;
  wire [3:0]\processQ_reg[7]_14 ;
  wire [3:0]\processQ_reg[7]_15 ;
  wire [3:0]\processQ_reg[7]_16 ;
  wire [3:0]\processQ_reg[7]_17 ;
  wire [3:0]\processQ_reg[7]_18 ;
  wire [3:0]\processQ_reg[7]_19 ;
  wire [3:0]\processQ_reg[7]_2 ;
  wire [3:0]\processQ_reg[7]_20 ;
  wire [3:0]\processQ_reg[7]_3 ;
  wire [3:0]\processQ_reg[7]_4 ;
  wire [3:0]\processQ_reg[7]_5 ;
  wire [3:0]\processQ_reg[7]_6 ;
  wire [3:0]\processQ_reg[7]_7 ;
  wire [3:0]\processQ_reg[7]_8 ;
  wire [3:0]\processQ_reg[7]_9 ;
  wire [0:0]\processQ_reg[9] ;
  wire [0:0]\processQ_reg[9]_0 ;
  wire [0:0]\processQ_reg[9]_1 ;
  wire [0:0]\processQ_reg[9]_10 ;
  wire [0:0]\processQ_reg[9]_11 ;
  wire [0:0]\processQ_reg[9]_12 ;
  wire [0:0]\processQ_reg[9]_13 ;
  wire [0:0]\processQ_reg[9]_14 ;
  wire [0:0]\processQ_reg[9]_15 ;
  wire [0:0]\processQ_reg[9]_16 ;
  wire [0:0]\processQ_reg[9]_17 ;
  wire [0:0]\processQ_reg[9]_18 ;
  wire [0:0]\processQ_reg[9]_19 ;
  wire [0:0]\processQ_reg[9]_2 ;
  wire [0:0]\processQ_reg[9]_20 ;
  wire [0:0]\processQ_reg[9]_21 ;
  wire [0:0]\processQ_reg[9]_22 ;
  wire [0:0]\processQ_reg[9]_3 ;
  wire [0:0]\processQ_reg[9]_4 ;
  wire [0:0]\processQ_reg[9]_5 ;
  wire [0:0]\processQ_reg[9]_6 ;
  wire [0:0]\processQ_reg[9]_7 ;
  wire [0:0]\processQ_reg[9]_8 ;
  wire [0:0]\processQ_reg[9]_9 ;
  wire \slv_reg10_reg[3] ;
  wire \slv_reg10_reg[5] ;
  wire [3:0]\slv_reg10_reg[9] ;
  wire [9:0]\slv_reg10_reg[9]_0 ;
  wire \slv_reg11_reg[0] ;
  wire \slv_reg11_reg[1] ;
  wire \slv_reg11_reg[2] ;
  wire \slv_reg11_reg[3] ;
  wire \slv_reg11_reg[4] ;
  wire \slv_reg11_reg[5] ;
  wire [3:0]\slv_reg11_reg[7] ;
  wire [3:0]\slv_reg11_reg[9] ;
  wire [0:0]\slv_reg5_reg[0] ;
  wire [3:0]\NLW_pixel_color3_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color3_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:0]NLW_pixel_color4_carry_O_UNCONNECTED;
  wire [3:1]NLW_pixel_color4_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_pixel_color4_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_pixel_color4_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color4_inferred__0/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color4_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:0]NLW_pixel_color5_carry_O_UNCONNECTED;
  wire [3:1]NLW_pixel_color5_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_pixel_color5_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_pixel_color5_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color5_inferred__0/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color5_inferred__1/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__1/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__2/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color5_inferred__2/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__2/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__3/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color5_inferred__3/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__3/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__4/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color5_inferred__4/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color5_inferred__4/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color6_inferred__1/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__1/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__2/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color6_inferred__2/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__2/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__3/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color6_inferred__3/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__3/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__4/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_pixel_color6_inferred__4/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pixel_color6_inferred__4/i__carry__0_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h000088880000F000)) 
    \dc_bias[3]_i_18__1 
       (.I0(pixel_color424_in),
        .I1(pixel_color523_in),
        .I2(pixel_color532_in),
        .I3(pixel_color631_in),
        .I4(\processQ_reg[1] [0]),
        .I5(\processQ_reg[1] [1]),
        .O(\encoded_reg[8]_20 ));
  LUT5 #(
    .INIT(32'h88800000)) 
    i__carry__0_i_3
       (.I0(\int_trigger_time_s_reg[0] ),
        .I1(\slv_reg10_reg[5] ),
        .I2(\encoded_reg[8]_6 ),
        .I3(\int_trigger_time_s_reg[0]_0 ),
        .I4(\int_trigger_time_s_reg[0]_1 ),
        .O(\encoded_reg[8]_5 ));
  LUT6 #(
    .INIT(64'hA888888800000000)) 
    i__carry__0_i_3__0
       (.I0(\int_trigger_time_s_reg[0]_1 ),
        .I1(\int_trigger_time_s_reg[0]_0 ),
        .I2(\encoded_reg[8]_8 ),
        .I3(\int_trigger_time_s_reg[0]_2 ),
        .I4(\slv_reg10_reg[3] ),
        .I5(\slv_reg10_reg[5] ),
        .O(\encoded_reg[8]_7 ));
  LUT6 #(
    .INIT(64'hAAA8888800000000)) 
    i__carry__0_i_3__1
       (.I0(\int_trigger_time_s_reg[0]_1 ),
        .I1(\int_trigger_time_s_reg[0]_0 ),
        .I2(\int_trigger_time_s_reg[0]_2 ),
        .I3(\encoded_reg[8]_8 ),
        .I4(\slv_reg10_reg[3] ),
        .I5(\slv_reg10_reg[5] ),
        .O(\encoded_reg[8]_9 ));
  LUT6 #(
    .INIT(64'hC000A0A0C0000000)) 
    i__carry__0_i_3__2
       (.I0(Q[7]),
        .I1(\slv_reg10_reg[9]_0 [7]),
        .I2(\encoded_reg[8]_11 ),
        .I3(\slv_reg10_reg[9]_0 [6]),
        .I4(\slv_reg5_reg[0] ),
        .I5(Q[6]),
        .O(\encoded_reg[8]_10 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry__0_i_3__4
       (.I0(\slv_reg10_reg[9]_0 [8]),
        .I1(\slv_reg5_reg[0] ),
        .I2(Q[8]),
        .O(\int_trigger_time_s_reg[0]_6 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry__0_i_5
       (.I0(\slv_reg10_reg[9]_0 [9]),
        .I1(\slv_reg5_reg[0] ),
        .I2(Q[9]),
        .O(\int_trigger_time_s_reg[0]_5 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAA8A80)) 
    i__carry_i_10
       (.I0(\slv_reg10_reg[3] ),
        .I1(\slv_reg10_reg[9]_0 [0]),
        .I2(\slv_reg5_reg[0] ),
        .I3(Q[0]),
        .I4(\int_trigger_time_s_reg[0]_4 ),
        .I5(\int_trigger_time_s_reg[0]_2 ),
        .O(\encoded_reg[8]_6 ));
  LUT6 #(
    .INIT(64'hCCC0AAAACCC0A0A0)) 
    i__carry_i_10__0
       (.I0(Q[3]),
        .I1(\slv_reg10_reg[9]_0 [3]),
        .I2(\encoded_reg[8]_8 ),
        .I3(\slv_reg10_reg[9]_0 [2]),
        .I4(\slv_reg5_reg[0] ),
        .I5(Q[2]),
        .O(\encoded_reg[8]_15 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'hCCA000A0)) 
    i__carry_i_10__3
       (.I0(Q[1]),
        .I1(\slv_reg10_reg[9]_0 [1]),
        .I2(Q[0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\slv_reg10_reg[9]_0 [0]),
        .O(\encoded_reg[8]_8 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h00053305)) 
    i__carry_i_10__4
       (.I0(Q[1]),
        .I1(\slv_reg10_reg[9]_0 [1]),
        .I2(Q[0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\slv_reg10_reg[9]_0 [0]),
        .O(\encoded_reg[8]_16 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'hCCA000A0)) 
    i__carry_i_11__0
       (.I0(Q[2]),
        .I1(\slv_reg10_reg[9]_0 [2]),
        .I2(Q[3]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\slv_reg10_reg[9]_0 [3]),
        .O(\encoded_reg[0]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_12
       (.I0(\slv_reg10_reg[9]_0 [0]),
        .I1(\slv_reg5_reg[0] ),
        .I2(Q[0]),
        .O(\int_trigger_time_s_reg[0]_3 ));
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_12__0
       (.I0(\slv_reg10_reg[9]_0 [7]),
        .I1(\slv_reg5_reg[0] ),
        .I2(Q[7]),
        .O(\int_trigger_time_s_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_13
       (.I0(\slv_reg10_reg[9]_0 [4]),
        .I1(\slv_reg5_reg[0] ),
        .I2(Q[4]),
        .O(\int_trigger_time_s_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_15
       (.I0(\slv_reg10_reg[9]_0 [1]),
        .I1(\slv_reg5_reg[0] ),
        .I2(Q[1]),
        .O(\int_trigger_time_s_reg[0]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_16
       (.I0(\slv_reg10_reg[9]_0 [2]),
        .I1(\slv_reg5_reg[0] ),
        .I2(Q[2]),
        .O(\int_trigger_time_s_reg[0]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    i__carry_i_9
       (.I0(\slv_reg10_reg[9]_0 [6]),
        .I1(\slv_reg5_reg[0] ),
        .I2(Q[6]),
        .O(\int_trigger_time_s_reg[0]_1 ));
  LUT6 #(
    .INIT(64'hCCC0AAAACCC0A0A0)) 
    i__carry_i_9__0
       (.I0(Q[5]),
        .I1(\slv_reg10_reg[9]_0 [5]),
        .I2(\encoded_reg[8]_6 ),
        .I3(\slv_reg10_reg[9]_0 [4]),
        .I4(\slv_reg5_reg[0] ),
        .I5(Q[4]),
        .O(\encoded_reg[8]_12 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA80000000)) 
    i__carry_i_9__1
       (.I0(\slv_reg10_reg[5] ),
        .I1(\slv_reg10_reg[3] ),
        .I2(\int_trigger_time_s_reg[0]_2 ),
        .I3(\int_trigger_time_s_reg[0]_3 ),
        .I4(\int_trigger_time_s_reg[0]_4 ),
        .I5(\int_trigger_time_s_reg[0]_0 ),
        .O(\encoded_reg[8]_13 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA88888000)) 
    i__carry_i_9__2
       (.I0(\slv_reg10_reg[5] ),
        .I1(\slv_reg10_reg[3] ),
        .I2(\int_trigger_time_s_reg[0]_4 ),
        .I3(\int_trigger_time_s_reg[0]_3 ),
        .I4(\int_trigger_time_s_reg[0]_2 ),
        .I5(\int_trigger_time_s_reg[0]_0 ),
        .O(\encoded_reg[8]_14 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA80808000)) 
    i__carry_i_9__3
       (.I0(\slv_reg10_reg[5] ),
        .I1(\slv_reg10_reg[3] ),
        .I2(\int_trigger_time_s_reg[0]_2 ),
        .I3(\int_trigger_time_s_reg[0]_3 ),
        .I4(\int_trigger_time_s_reg[0]_4 ),
        .I5(\int_trigger_time_s_reg[0]_0 ),
        .O(\encoded_reg[8]_11 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color3_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\encoded_reg[8]_2 ,\pixel_color3_inferred__0/i__carry_n_1 ,\pixel_color3_inferred__0/i__carry_n_2 ,\pixel_color3_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_pixel_color3_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S(\slv_reg11_reg[9] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color3_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\encoded_reg[0]_1 ,\pixel_color3_inferred__1/i__carry_n_1 ,\pixel_color3_inferred__1/i__carry_n_2 ,\pixel_color3_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_pixel_color3_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S(\slv_reg10_reg[9] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 pixel_color4_carry
       (.CI(1'b0),
        .CO({pixel_color4_carry_n_0,pixel_color4_carry_n_1,pixel_color4_carry_n_2,pixel_color4_carry_n_3}),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_3 ),
        .O(NLW_pixel_color4_carry_O_UNCONNECTED[3:0]),
        .S(\processQ_reg[7]_4 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 pixel_color4_carry__0
       (.CI(pixel_color4_carry_n_0),
        .CO({NLW_pixel_color4_carry__0_CO_UNCONNECTED[3:1],\encoded_reg[8]_1 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_5 }),
        .O(NLW_pixel_color4_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_6 }));
  LUT6 #(
    .INIT(64'hC000A0A0C0000000)) 
    pixel_color4_carry__0_i_3
       (.I0(\int_trigger_volt_s_reg[7] [3]),
        .I1(\slv_reg11_reg[7] [3]),
        .I2(\encoded_reg[8]_18 ),
        .I3(\slv_reg11_reg[7] [2]),
        .I4(\slv_reg5_reg[0] ),
        .I5(\int_trigger_volt_s_reg[7] [2]),
        .O(\encoded_reg[8]_17 ));
  LUT6 #(
    .INIT(64'h0300035533553355)) 
    pixel_color4_carry_i_10
       (.I0(\int_trigger_volt_s_reg[7] [1]),
        .I1(\slv_reg11_reg[7] [1]),
        .I2(\slv_reg11_reg[7] [0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\int_trigger_volt_s_reg[7] [0]),
        .I5(\slv_reg11_reg[1] ),
        .O(\encoded_reg[8]_19 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA88888000)) 
    pixel_color4_carry_i_9
       (.I0(\slv_reg11_reg[5] ),
        .I1(\slv_reg11_reg[3] ),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[0] ),
        .I4(\slv_reg11_reg[2] ),
        .I5(\slv_reg11_reg[4] ),
        .O(\encoded_reg[8]_18 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color4_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color4_inferred__0/i__carry_n_0 ,\pixel_color4_inferred__0/i__carry_n_1 ,\pixel_color4_inferred__0/i__carry_n_2 ,\pixel_color4_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_15 ),
        .O(\NLW_pixel_color4_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S(\processQ_reg[7]_16 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color4_inferred__0/i__carry__0 
       (.CI(\pixel_color4_inferred__0/i__carry_n_0 ),
        .CO({\NLW_pixel_color4_inferred__0/i__carry__0_CO_UNCONNECTED [3:1],pixel_color424_in}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_17 }),
        .O(\NLW_pixel_color4_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_18 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 pixel_color5_carry
       (.CI(1'b0),
        .CO({pixel_color5_carry_n_0,pixel_color5_carry_n_1,pixel_color5_carry_n_2,pixel_color5_carry_n_3}),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7] ),
        .O(NLW_pixel_color5_carry_O_UNCONNECTED[3:0]),
        .S(\processQ_reg[7]_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 pixel_color5_carry__0
       (.CI(pixel_color5_carry_n_0),
        .CO({NLW_pixel_color5_carry__0_CO_UNCONNECTED[3:1],\encoded_reg[8] }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_1 }),
        .O(NLW_pixel_color5_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_2 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color5_inferred__0/i__carry_n_0 ,\pixel_color5_inferred__0/i__carry_n_1 ,\pixel_color5_inferred__0/i__carry_n_2 ,\pixel_color5_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_1 ),
        .O(\NLW_pixel_color5_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S(\processQ_reg[7]_2 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__0/i__carry__0 
       (.CI(\pixel_color5_inferred__0/i__carry_n_0 ),
        .CO({\NLW_pixel_color5_inferred__0/i__carry__0_CO_UNCONNECTED [3:1],\encoded_reg[8]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_3 }),
        .O(\NLW_pixel_color5_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_4 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color5_inferred__1/i__carry_n_0 ,\pixel_color5_inferred__1/i__carry_n_1 ,\pixel_color5_inferred__1/i__carry_n_2 ,\pixel_color5_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_7 ),
        .O(\NLW_pixel_color5_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S(\processQ_reg[7]_8 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__1/i__carry__0 
       (.CI(\pixel_color5_inferred__1/i__carry_n_0 ),
        .CO({\NLW_pixel_color5_inferred__1/i__carry__0_CO_UNCONNECTED [3:1],\encoded_reg[8]_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_9 }),
        .O(\NLW_pixel_color5_inferred__1/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_10 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color5_inferred__2/i__carry_n_0 ,\pixel_color5_inferred__2/i__carry_n_1 ,\pixel_color5_inferred__2/i__carry_n_2 ,\pixel_color5_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_11 ),
        .O(\NLW_pixel_color5_inferred__2/i__carry_O_UNCONNECTED [3:0]),
        .S(\processQ_reg[7]_12 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__2/i__carry__0 
       (.CI(\pixel_color5_inferred__2/i__carry_n_0 ),
        .CO({\NLW_pixel_color5_inferred__2/i__carry__0_CO_UNCONNECTED [3:1],\encoded_reg[0]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_13 }),
        .O(\NLW_pixel_color5_inferred__2/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_14 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__3/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color5_inferred__3/i__carry_n_0 ,\pixel_color5_inferred__3/i__carry_n_1 ,\pixel_color5_inferred__3/i__carry_n_2 ,\pixel_color5_inferred__3/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_13 ),
        .O(\NLW_pixel_color5_inferred__3/i__carry_O_UNCONNECTED [3:0]),
        .S(\processQ_reg[7]_14 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__3/i__carry__0 
       (.CI(\pixel_color5_inferred__3/i__carry_n_0 ),
        .CO({\NLW_pixel_color5_inferred__3/i__carry__0_CO_UNCONNECTED [3:1],pixel_color523_in}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_15 }),
        .O(\NLW_pixel_color5_inferred__3/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_16 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__4/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color5_inferred__4/i__carry_n_0 ,\pixel_color5_inferred__4/i__carry_n_1 ,\pixel_color5_inferred__4/i__carry_n_2 ,\pixel_color5_inferred__4/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_19 ),
        .O(\NLW_pixel_color5_inferred__4/i__carry_O_UNCONNECTED [3:0]),
        .S(\processQ_reg[7]_20 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color5_inferred__4/i__carry__0 
       (.CI(\pixel_color5_inferred__4/i__carry_n_0 ),
        .CO({\NLW_pixel_color5_inferred__4/i__carry__0_CO_UNCONNECTED [3:1],pixel_color532_in}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_21 }),
        .O(\NLW_pixel_color5_inferred__4/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_22 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color6_inferred__1/i__carry_n_0 ,\pixel_color6_inferred__1/i__carry_n_1 ,\pixel_color6_inferred__1/i__carry_n_2 ,\pixel_color6_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(DI),
        .O(\NLW_pixel_color6_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S(S));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__1/i__carry__0 
       (.CI(\pixel_color6_inferred__1/i__carry_n_0 ),
        .CO({\NLW_pixel_color6_inferred__1/i__carry__0_CO_UNCONNECTED [3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9] }),
        .O(\NLW_pixel_color6_inferred__1/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_0 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color6_inferred__2/i__carry_n_0 ,\pixel_color6_inferred__2/i__carry_n_1 ,\pixel_color6_inferred__2/i__carry_n_2 ,\pixel_color6_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_5 ),
        .O(\NLW_pixel_color6_inferred__2/i__carry_O_UNCONNECTED [3:0]),
        .S(\processQ_reg[7]_6 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__2/i__carry__0 
       (.CI(\pixel_color6_inferred__2/i__carry_n_0 ),
        .CO({\NLW_pixel_color6_inferred__2/i__carry__0_CO_UNCONNECTED [3:1],\encoded_reg[8]_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_7 }),
        .O(\NLW_pixel_color6_inferred__2/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_8 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__3/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color6_inferred__3/i__carry_n_0 ,\pixel_color6_inferred__3/i__carry_n_1 ,\pixel_color6_inferred__3/i__carry_n_2 ,\pixel_color6_inferred__3/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_9 ),
        .O(\NLW_pixel_color6_inferred__3/i__carry_O_UNCONNECTED [3:0]),
        .S(\processQ_reg[7]_10 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__3/i__carry__0 
       (.CI(\pixel_color6_inferred__3/i__carry_n_0 ),
        .CO({\NLW_pixel_color6_inferred__3/i__carry__0_CO_UNCONNECTED [3:1],\encoded_reg[0] }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_11 }),
        .O(\NLW_pixel_color6_inferred__3/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_12 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__4/i__carry 
       (.CI(1'b0),
        .CO({\pixel_color6_inferred__4/i__carry_n_0 ,\pixel_color6_inferred__4/i__carry_n_1 ,\pixel_color6_inferred__4/i__carry_n_2 ,\pixel_color6_inferred__4/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(\processQ_reg[7]_17 ),
        .O(\NLW_pixel_color6_inferred__4/i__carry_O_UNCONNECTED [3:0]),
        .S(\processQ_reg[7]_18 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \pixel_color6_inferred__4/i__carry__0 
       (.CI(\pixel_color6_inferred__4/i__carry_n_0 ),
        .CO({\NLW_pixel_color6_inferred__4/i__carry__0_CO_UNCONNECTED [3:1],pixel_color631_in}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\processQ_reg[9]_19 }),
        .O(\NLW_pixel_color6_inferred__4/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,\processQ_reg[9]_20 }));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO
   (DOADO,
    \encoded_reg[8] ,
    CO,
    clk,
    WREN,
    reset_n,
    ADDRARDADDR,
    ADDRBWRADDR,
    \slv_reg1_reg[9] ,
    Q,
    switch,
    \processQ_reg[8] ,
    S);
  output [2:0]DOADO;
  output \encoded_reg[8] ;
  output [0:0]CO;
  input clk;
  input WREN;
  input reset_n;
  input [9:0]ADDRARDADDR;
  input [9:0]ADDRBWRADDR;
  input [9:0]\slv_reg1_reg[9] ;
  input [6:0]Q;
  input [0:0]switch;
  input \processQ_reg[8] ;
  input [0:0]S;

  wire [9:0]ADDRARDADDR;
  wire [9:0]ADDRBWRADDR;
  wire [0:0]CO;
  wire [2:0]DOADO;
  wire [9:3]L_out_bram;
  wire [6:0]Q;
  wire [0:0]S;
  wire WREN;
  wire clk;
  wire \dc_bias[3]_i_15_n_0 ;
  wire \dc_bias[3]_i_16_n_0 ;
  wire \dc_bias[3]_i_17_n_0 ;
  wire \dc_bias[3]_i_18__0_n_0 ;
  wire \dc_bias[3]_i_6__1_n_0 ;
  wire \dc_bias[3]_i_7__0_n_0 ;
  wire \dc_bias[3]_i_8_n_0 ;
  wire \dc_bias_reg[3]_i_3_n_1 ;
  wire \dc_bias_reg[3]_i_3_n_2 ;
  wire \dc_bias_reg[3]_i_3_n_3 ;
  wire \encoded_reg[8] ;
  wire \processQ_reg[8] ;
  wire reset_n;
  wire [9:0]\slv_reg1_reg[9] ;
  wire [0:0]switch;
  wire [3:0]\NLW_dc_bias_reg[3]_i_3_O_UNCONNECTED ;
  wire [15:10]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED ;
  wire [15:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h8888800000000000)) 
    \dc_bias[3]_i_15 
       (.I0(L_out_bram[7]),
        .I1(L_out_bram[5]),
        .I2(DOADO[2]),
        .I3(L_out_bram[3]),
        .I4(L_out_bram[4]),
        .I5(L_out_bram[6]),
        .O(\dc_bias[3]_i_15_n_0 ));
  LUT5 #(
    .INIT(32'hA8880000)) 
    \dc_bias[3]_i_16 
       (.I0(L_out_bram[6]),
        .I1(L_out_bram[4]),
        .I2(L_out_bram[3]),
        .I3(DOADO[2]),
        .I4(L_out_bram[5]),
        .O(\dc_bias[3]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h9999955566666AAA)) 
    \dc_bias[3]_i_17 
       (.I0(Q[3]),
        .I1(L_out_bram[5]),
        .I2(DOADO[2]),
        .I3(L_out_bram[3]),
        .I4(L_out_bram[4]),
        .I5(L_out_bram[6]),
        .O(\dc_bias[3]_i_17_n_0 ));
  LUT5 #(
    .INIT(32'hBDDEE77B)) 
    \dc_bias[3]_i_18__0 
       (.I0(Q[0]),
        .I1(L_out_bram[4]),
        .I2(L_out_bram[3]),
        .I3(DOADO[2]),
        .I4(Q[1]),
        .O(\dc_bias[3]_i_18__0_n_0 ));
  LUT4 #(
    .INIT(16'h6A95)) 
    \dc_bias[3]_i_6__1 
       (.I0(Q[6]),
        .I1(L_out_bram[8]),
        .I2(\dc_bias[3]_i_15_n_0 ),
        .I3(L_out_bram[9]),
        .O(\dc_bias[3]_i_6__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000006909009)) 
    \dc_bias[3]_i_7__0 
       (.I0(L_out_bram[8]),
        .I1(Q[5]),
        .I2(Q[4]),
        .I3(\dc_bias[3]_i_16_n_0 ),
        .I4(L_out_bram[7]),
        .I5(\dc_bias[3]_i_17_n_0 ),
        .O(\dc_bias[3]_i_7__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000556AAA95)) 
    \dc_bias[3]_i_8 
       (.I0(L_out_bram[5]),
        .I1(DOADO[2]),
        .I2(L_out_bram[3]),
        .I3(L_out_bram[4]),
        .I4(Q[2]),
        .I5(\dc_bias[3]_i_18__0_n_0 ),
        .O(\dc_bias[3]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \dc_bias[3]_i_8__0 
       (.I0(switch),
        .I1(CO),
        .I2(\processQ_reg[8] ),
        .O(\encoded_reg[8] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \dc_bias_reg[3]_i_3 
       (.CI(1'b0),
        .CO({CO,\dc_bias_reg[3]_i_3_n_1 ,\dc_bias_reg[3]_i_3_n_2 ,\dc_bias_reg[3]_i_3_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_dc_bias_reg[3]_i_3_O_UNCONNECTED [3:0]),
        .S({\dc_bias[3]_i_6__1_n_0 ,\dc_bias[3]_i_7__0_n_0 ,\dc_bias[3]_i_8_n_0 ,S}));
  (* BOX_TYPE = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("NONE"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(18)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl 
       (.ADDRARDADDR({ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({ADDRBWRADDR,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\slv_reg1_reg[9] }),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED [15:10],L_out_bram,DOADO}),
        .DOBDO(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(WREN),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(reset_n),
        .RSTRAMB(reset_n),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b1,1'b1,1'b1,1'b1}));
endmodule

(* ORIG_REF_NAME = "unimacro_BRAM_SDP_MACRO" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0
   (\encoded_reg[8] ,
    CO,
    clk,
    WREN,
    reset_n,
    ADDRARDADDR,
    ADDRBWRADDR,
    DIBDI,
    Q,
    switch);
  output \encoded_reg[8] ;
  output [0:0]CO;
  input clk;
  input WREN;
  input reset_n;
  input [9:0]ADDRARDADDR;
  input [9:0]ADDRBWRADDR;
  input [9:0]DIBDI;
  input [9:0]Q;
  input [0:0]switch;

  wire [9:0]ADDRARDADDR;
  wire [9:0]ADDRBWRADDR;
  wire [0:0]CO;
  wire [9:0]DIBDI;
  wire [9:0]Q;
  wire [9:0]R_out_bram;
  wire WREN;
  wire clk;
  wire \dc_bias[3]_i_19__1_n_0 ;
  wire \dc_bias[3]_i_20__0_n_0 ;
  wire \dc_bias[3]_i_21_n_0 ;
  wire \dc_bias[3]_i_22_n_0 ;
  wire \dc_bias[3]_i_26_n_0 ;
  wire \dc_bias[3]_i_27__0_n_0 ;
  wire \dc_bias[3]_i_28__0_n_0 ;
  wire \dc_bias[3]_i_29__0_n_0 ;
  wire \dc_bias_reg[3]_i_13_n_1 ;
  wire \dc_bias_reg[3]_i_13_n_2 ;
  wire \dc_bias_reg[3]_i_13_n_3 ;
  wire \encoded_reg[8] ;
  wire reset_n;
  wire [0:0]switch;
  wire [3:0]\NLW_dc_bias_reg[3]_i_13_O_UNCONNECTED ;
  wire [15:10]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED ;
  wire [15:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h6A95)) 
    \dc_bias[3]_i_19__1 
       (.I0(Q[9]),
        .I1(R_out_bram[8]),
        .I2(\dc_bias[3]_i_26_n_0 ),
        .I3(R_out_bram[9]),
        .O(\dc_bias[3]_i_19__1_n_0 ));
  LUT6 #(
    .INIT(64'h0480804040080804)) 
    \dc_bias[3]_i_20__0 
       (.I0(Q[7]),
        .I1(\dc_bias[3]_i_27__0_n_0 ),
        .I2(R_out_bram[8]),
        .I3(\dc_bias[3]_i_28__0_n_0 ),
        .I4(R_out_bram[7]),
        .I5(Q[8]),
        .O(\dc_bias[3]_i_20__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000556AAA95)) 
    \dc_bias[3]_i_21 
       (.I0(R_out_bram[5]),
        .I1(R_out_bram[2]),
        .I2(R_out_bram[3]),
        .I3(R_out_bram[4]),
        .I4(Q[5]),
        .I5(\dc_bias[3]_i_29__0_n_0 ),
        .O(\dc_bias[3]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    \dc_bias[3]_i_22 
       (.I0(R_out_bram[2]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(R_out_bram[1]),
        .I4(Q[0]),
        .I5(R_out_bram[0]),
        .O(\dc_bias[3]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'h8888800000000000)) 
    \dc_bias[3]_i_26 
       (.I0(R_out_bram[7]),
        .I1(R_out_bram[5]),
        .I2(R_out_bram[2]),
        .I3(R_out_bram[3]),
        .I4(R_out_bram[4]),
        .I5(R_out_bram[6]),
        .O(\dc_bias[3]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'h66666AAA99999555)) 
    \dc_bias[3]_i_27__0 
       (.I0(Q[6]),
        .I1(R_out_bram[5]),
        .I2(R_out_bram[2]),
        .I3(R_out_bram[3]),
        .I4(R_out_bram[4]),
        .I5(R_out_bram[6]),
        .O(\dc_bias[3]_i_27__0_n_0 ));
  LUT5 #(
    .INIT(32'hA8880000)) 
    \dc_bias[3]_i_28__0 
       (.I0(R_out_bram[6]),
        .I1(R_out_bram[4]),
        .I2(R_out_bram[3]),
        .I3(R_out_bram[2]),
        .I4(R_out_bram[5]),
        .O(\dc_bias[3]_i_28__0_n_0 ));
  LUT5 #(
    .INIT(32'hBDDEE77B)) 
    \dc_bias[3]_i_29__0 
       (.I0(Q[3]),
        .I1(R_out_bram[4]),
        .I2(R_out_bram[3]),
        .I3(R_out_bram[2]),
        .I4(Q[4]),
        .O(\dc_bias[3]_i_29__0_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \dc_bias[3]_i_7__1 
       (.I0(CO),
        .I1(switch),
        .O(\encoded_reg[8] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \dc_bias_reg[3]_i_13 
       (.CI(1'b0),
        .CO({CO,\dc_bias_reg[3]_i_13_n_1 ,\dc_bias_reg[3]_i_13_n_2 ,\dc_bias_reg[3]_i_13_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_dc_bias_reg[3]_i_13_O_UNCONNECTED [3:0]),
        .S({\dc_bias[3]_i_19__1_n_0 ,\dc_bias[3]_i_20__0_n_0 ,\dc_bias[3]_i_21_n_0 ,\dc_bias[3]_i_22_n_0 }));
  (* BOX_TYPE = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("NONE"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(18)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl 
       (.ADDRARDADDR({ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({ADDRBWRADDR,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,DIBDI}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOADO_UNCONNECTED [15:10],R_out_bram}),
        .DOBDO(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(WREN),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(reset_n),
        .RSTRAMB(reset_n),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b1,1'b1,1'b1,1'b1}));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vcounter
   (\encoded_reg[0] ,
    \encoded_reg[9] ,
    Q,
    \encoded_reg[8] ,
    SR,
    \encoded_reg[8]_0 ,
    \encoded_reg[8]_1 ,
    \encoded_reg[0]_0 ,
    \encoded_reg[8]_2 ,
    \encoded_reg[8]_3 ,
    S,
    \encoded_reg[8]_4 ,
    \encoded_reg[8]_5 ,
    \encoded_reg[8]_6 ,
    \encoded_reg[8]_7 ,
    \encoded_reg[8]_8 ,
    \encoded_reg[8]_9 ,
    \encoded_reg[8]_10 ,
    \encoded_reg[8]_11 ,
    \encoded_reg[8]_12 ,
    \encoded_reg[8]_13 ,
    \encoded_reg[8]_14 ,
    \encoded_reg[8]_15 ,
    \encoded_reg[8]_16 ,
    \encoded_reg[8]_17 ,
    \encoded_reg[8]_18 ,
    \encoded_reg[8]_19 ,
    \encoded_reg[8]_20 ,
    \encoded_reg[8]_21 ,
    \encoded_reg[8]_22 ,
    \encoded_reg[8]_23 ,
    \encoded_reg[8]_24 ,
    \encoded_reg[8]_25 ,
    DI,
    \encoded_reg[8]_26 ,
    \encoded_reg[8]_27 ,
    \encoded_reg[8]_28 ,
    \encoded_reg[8]_29 ,
    \encoded_reg[8]_30 ,
    \encoded_reg[8]_31 ,
    \encoded_reg[9]_0 ,
    \encoded_reg[9]_1 ,
    \dc_bias_reg[3] ,
    \processQ_reg[8]_0 ,
    reset_n,
    \processQ_reg[8]_1 ,
    \processQ_reg[2]_0 ,
    \processQ_reg[4]_0 ,
    \processQ_reg[5]_0 ,
    \processQ_reg[6]_0 ,
    \processQ_reg[9]_0 ,
    \slv_reg11_reg[1] ,
    \int_trigger_volt_s_reg[9] ,
    \slv_reg5_reg[0] ,
    \slv_reg11_reg[9] ,
    DOADO,
    \slv_reg11_reg[9]_0 ,
    \slv_reg11_reg[8] ,
    \slv_reg11_reg[7] ,
    \slv_reg11_reg[6] ,
    \slv_reg11_reg[5] ,
    \slv_reg11_reg[7]_0 ,
    \slv_reg11_reg[6]_0 ,
    \slv_reg11_reg[5]_0 ,
    \slv_reg11_reg[6]_1 ,
    \int_trigger_volt_s_reg[7] ,
    \slv_reg11_reg[5]_1 ,
    \slv_reg11_reg[6]_2 ,
    \slv_reg11_reg[5]_2 ,
    \slv_reg11_reg[6]_3 ,
    \int_trigger_volt_s_reg[2] ,
    \slv_reg11_reg[5]_3 ,
    \slv_reg11_reg[6]_4 ,
    \int_trigger_volt_s_reg[2]_0 ,
    \slv_reg11_reg[3] ,
    \slv_reg11_reg[2] ,
    \slv_reg11_reg[4] ,
    \slv_reg11_reg[0] ,
    \slv_reg11_reg[0]_0 ,
    \int_trigger_volt_s_reg[3] ,
    \int_trigger_volt_s_reg[2]_1 ,
    \slv_reg11_reg[0]_1 ,
    \slv_reg11_reg[1]_0 ,
    \int_trigger_volt_s_reg[2]_2 ,
    \slv_reg11_reg[0]_2 ,
    \processQ_reg[9]_1 ,
    \processQ_reg[9]_2 ,
    \processQ_reg[4]_1 ,
    \processQ_reg[0]_0 ,
    \processQ_reg[1]_0 ,
    \processQ_reg[9]_3 ,
    CO,
    \processQ_reg[1]_1 ,
    \slv_reg11_reg[9]_1 ,
    \processQ_reg[0]_1 ,
    \processQ_reg[0]_2 ,
    \processQ_reg[7]_0 ,
    \slv_reg11_reg[7]_1 ,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[1] ,
    \processQ_reg[8]_2 ,
    \processQ_reg[9]_4 ,
    CLK);
  output \encoded_reg[0] ;
  output \encoded_reg[9] ;
  output [9:0]Q;
  output \encoded_reg[8] ;
  output [0:0]SR;
  output \encoded_reg[8]_0 ;
  output \encoded_reg[8]_1 ;
  output \encoded_reg[0]_0 ;
  output [3:0]\encoded_reg[8]_2 ;
  output \encoded_reg[8]_3 ;
  output [0:0]S;
  output [0:0]\encoded_reg[8]_4 ;
  output [0:0]\encoded_reg[8]_5 ;
  output [3:0]\encoded_reg[8]_6 ;
  output [3:0]\encoded_reg[8]_7 ;
  output [0:0]\encoded_reg[8]_8 ;
  output [0:0]\encoded_reg[8]_9 ;
  output [3:0]\encoded_reg[8]_10 ;
  output [3:0]\encoded_reg[8]_11 ;
  output [3:0]\encoded_reg[8]_12 ;
  output [0:0]\encoded_reg[8]_13 ;
  output [0:0]\encoded_reg[8]_14 ;
  output [3:0]\encoded_reg[8]_15 ;
  output [0:0]\encoded_reg[8]_16 ;
  output [0:0]\encoded_reg[8]_17 ;
  output [3:0]\encoded_reg[8]_18 ;
  output [3:0]\encoded_reg[8]_19 ;
  output [0:0]\encoded_reg[8]_20 ;
  output [0:0]\encoded_reg[8]_21 ;
  output [3:0]\encoded_reg[8]_22 ;
  output [3:0]\encoded_reg[8]_23 ;
  output [0:0]\encoded_reg[8]_24 ;
  output [0:0]\encoded_reg[8]_25 ;
  output [3:0]DI;
  output [3:0]\encoded_reg[8]_26 ;
  output \encoded_reg[8]_27 ;
  output \encoded_reg[8]_28 ;
  output \encoded_reg[8]_29 ;
  output \encoded_reg[8]_30 ;
  output \encoded_reg[8]_31 ;
  output \encoded_reg[9]_0 ;
  output \encoded_reg[9]_1 ;
  input [0:0]\dc_bias_reg[3] ;
  input \processQ_reg[8]_0 ;
  input reset_n;
  input \processQ_reg[8]_1 ;
  input \processQ_reg[2]_0 ;
  input \processQ_reg[4]_0 ;
  input \processQ_reg[5]_0 ;
  input \processQ_reg[6]_0 ;
  input \processQ_reg[9]_0 ;
  input \slv_reg11_reg[1] ;
  input [3:0]\int_trigger_volt_s_reg[9] ;
  input [0:0]\slv_reg5_reg[0] ;
  input [3:0]\slv_reg11_reg[9] ;
  input [2:0]DOADO;
  input \slv_reg11_reg[9]_0 ;
  input \slv_reg11_reg[8] ;
  input \slv_reg11_reg[7] ;
  input \slv_reg11_reg[6] ;
  input \slv_reg11_reg[5] ;
  input \slv_reg11_reg[7]_0 ;
  input \slv_reg11_reg[6]_0 ;
  input \slv_reg11_reg[5]_0 ;
  input \slv_reg11_reg[6]_1 ;
  input \int_trigger_volt_s_reg[7] ;
  input \slv_reg11_reg[5]_1 ;
  input \slv_reg11_reg[6]_2 ;
  input \slv_reg11_reg[5]_2 ;
  input \slv_reg11_reg[6]_3 ;
  input \int_trigger_volt_s_reg[2] ;
  input \slv_reg11_reg[5]_3 ;
  input \slv_reg11_reg[6]_4 ;
  input \int_trigger_volt_s_reg[2]_0 ;
  input \slv_reg11_reg[3] ;
  input \slv_reg11_reg[2] ;
  input \slv_reg11_reg[4] ;
  input \slv_reg11_reg[0] ;
  input \slv_reg11_reg[0]_0 ;
  input \int_trigger_volt_s_reg[3] ;
  input \int_trigger_volt_s_reg[2]_1 ;
  input \slv_reg11_reg[0]_1 ;
  input \slv_reg11_reg[1]_0 ;
  input \int_trigger_volt_s_reg[2]_2 ;
  input \slv_reg11_reg[0]_2 ;
  input [0:0]\processQ_reg[9]_1 ;
  input [0:0]\processQ_reg[9]_2 ;
  input \processQ_reg[4]_1 ;
  input \processQ_reg[0]_0 ;
  input \processQ_reg[1]_0 ;
  input [0:0]\processQ_reg[9]_3 ;
  input [0:0]CO;
  input [1:0]\processQ_reg[1]_1 ;
  input [0:0]\slv_reg11_reg[9]_1 ;
  input \processQ_reg[0]_1 ;
  input \processQ_reg[0]_2 ;
  input \processQ_reg[7]_0 ;
  input \slv_reg11_reg[7]_1 ;
  input [0:0]\dc_bias_reg[3]_0 ;
  input \dc_bias_reg[1] ;
  input \processQ_reg[8]_2 ;
  input \processQ_reg[9]_4 ;
  input CLK;

  wire CLK;
  wire [0:0]CO;
  wire [3:0]DI;
  wire [2:0]DOADO;
  wire [9:0]Q;
  wire [0:0]S;
  wire [0:0]SR;
  wire \dc_bias[3]_i_17__1_n_0 ;
  wire \dc_bias[3]_i_19__0_n_0 ;
  wire \dc_bias[3]_i_20__1_n_0 ;
  wire \dc_bias[3]_i_20_n_0 ;
  wire \dc_bias[3]_i_21__0_n_0 ;
  wire \dc_bias[3]_i_22__0_n_0 ;
  wire \dc_bias[3]_i_28_n_0 ;
  wire \dc_bias[3]_i_29_n_0 ;
  wire \dc_bias[3]_i_31_n_0 ;
  wire \dc_bias[3]_i_32_n_0 ;
  wire \dc_bias[3]_i_43_n_0 ;
  wire \dc_bias[3]_i_44_n_0 ;
  wire \dc_bias[3]_i_47_n_0 ;
  wire \dc_bias[3]_i_48_n_0 ;
  wire \dc_bias[3]_i_49_n_0 ;
  wire \dc_bias[3]_i_52_n_0 ;
  wire \dc_bias[3]_i_53_n_0 ;
  wire \dc_bias[3]_i_54_n_0 ;
  wire \dc_bias[3]_i_59_n_0 ;
  wire \dc_bias[3]_i_60_n_0 ;
  wire \dc_bias_reg[1] ;
  wire [0:0]\dc_bias_reg[3] ;
  wire [0:0]\dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_i_46_n_0 ;
  wire \encoded[9]_i_2_n_0 ;
  wire \encoded[9]_i_3_n_0 ;
  wire \encoded_reg[0] ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire [3:0]\encoded_reg[8]_10 ;
  wire [3:0]\encoded_reg[8]_11 ;
  wire [3:0]\encoded_reg[8]_12 ;
  wire [0:0]\encoded_reg[8]_13 ;
  wire [0:0]\encoded_reg[8]_14 ;
  wire [3:0]\encoded_reg[8]_15 ;
  wire [0:0]\encoded_reg[8]_16 ;
  wire [0:0]\encoded_reg[8]_17 ;
  wire [3:0]\encoded_reg[8]_18 ;
  wire [3:0]\encoded_reg[8]_19 ;
  wire [3:0]\encoded_reg[8]_2 ;
  wire [0:0]\encoded_reg[8]_20 ;
  wire [0:0]\encoded_reg[8]_21 ;
  wire [3:0]\encoded_reg[8]_22 ;
  wire [3:0]\encoded_reg[8]_23 ;
  wire [0:0]\encoded_reg[8]_24 ;
  wire [0:0]\encoded_reg[8]_25 ;
  wire [3:0]\encoded_reg[8]_26 ;
  wire \encoded_reg[8]_27 ;
  wire \encoded_reg[8]_28 ;
  wire \encoded_reg[8]_29 ;
  wire \encoded_reg[8]_3 ;
  wire \encoded_reg[8]_30 ;
  wire \encoded_reg[8]_31 ;
  wire [0:0]\encoded_reg[8]_4 ;
  wire [0:0]\encoded_reg[8]_5 ;
  wire [3:0]\encoded_reg[8]_6 ;
  wire [3:0]\encoded_reg[8]_7 ;
  wire [0:0]\encoded_reg[8]_8 ;
  wire [0:0]\encoded_reg[8]_9 ;
  wire \encoded_reg[9] ;
  wire \encoded_reg[9]_0 ;
  wire \encoded_reg[9]_1 ;
  wire i__carry_i_7__6_n_0;
  wire i__carry_i_9__9_n_0;
  wire \int_trigger_volt_s_reg[2] ;
  wire \int_trigger_volt_s_reg[2]_0 ;
  wire \int_trigger_volt_s_reg[2]_1 ;
  wire \int_trigger_volt_s_reg[2]_2 ;
  wire \int_trigger_volt_s_reg[3] ;
  wire \int_trigger_volt_s_reg[7] ;
  wire [3:0]\int_trigger_volt_s_reg[9] ;
  wire [9:0]plusOp__2;
  wire processQ0;
  wire \processQ[2]_i_1__0_n_0 ;
  wire \processQ[6]_i_1__1_n_0 ;
  wire \processQ[6]_i_2_n_0 ;
  wire \processQ[8]_i_2_n_0 ;
  wire \processQ[9]_i_1__0_n_0 ;
  wire \processQ[9]_i_4__0_n_0 ;
  wire \processQ[9]_i_5_n_0 ;
  wire \processQ[9]_i_6_n_0 ;
  wire \processQ[9]_i_7_n_0 ;
  wire \processQ_reg[0]_0 ;
  wire \processQ_reg[0]_1 ;
  wire \processQ_reg[0]_2 ;
  wire \processQ_reg[1]_0 ;
  wire [1:0]\processQ_reg[1]_1 ;
  wire \processQ_reg[2]_0 ;
  wire \processQ_reg[4]_0 ;
  wire \processQ_reg[4]_1 ;
  wire \processQ_reg[5]_0 ;
  wire \processQ_reg[6]_0 ;
  wire \processQ_reg[7]_0 ;
  wire \processQ_reg[8]_0 ;
  wire \processQ_reg[8]_1 ;
  wire \processQ_reg[8]_2 ;
  wire \processQ_reg[9]_0 ;
  wire [0:0]\processQ_reg[9]_1 ;
  wire [0:0]\processQ_reg[9]_2 ;
  wire [0:0]\processQ_reg[9]_3 ;
  wire \processQ_reg[9]_4 ;
  wire reset_n;
  wire \slv_reg11_reg[0] ;
  wire \slv_reg11_reg[0]_0 ;
  wire \slv_reg11_reg[0]_1 ;
  wire \slv_reg11_reg[0]_2 ;
  wire \slv_reg11_reg[1] ;
  wire \slv_reg11_reg[1]_0 ;
  wire \slv_reg11_reg[2] ;
  wire \slv_reg11_reg[3] ;
  wire \slv_reg11_reg[4] ;
  wire \slv_reg11_reg[5] ;
  wire \slv_reg11_reg[5]_0 ;
  wire \slv_reg11_reg[5]_1 ;
  wire \slv_reg11_reg[5]_2 ;
  wire \slv_reg11_reg[5]_3 ;
  wire \slv_reg11_reg[6] ;
  wire \slv_reg11_reg[6]_0 ;
  wire \slv_reg11_reg[6]_1 ;
  wire \slv_reg11_reg[6]_2 ;
  wire \slv_reg11_reg[6]_3 ;
  wire \slv_reg11_reg[6]_4 ;
  wire \slv_reg11_reg[7] ;
  wire \slv_reg11_reg[7]_0 ;
  wire \slv_reg11_reg[7]_1 ;
  wire \slv_reg11_reg[8] ;
  wire [3:0]\slv_reg11_reg[9] ;
  wire \slv_reg11_reg[9]_0 ;
  wire [0:0]\slv_reg11_reg[9]_1 ;
  wire [0:0]\slv_reg5_reg[0] ;

  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h535303A3)) 
    \TDMS_encoder_blue/encoded[9]_i_1 
       (.I0(\dc_bias_reg[3]_0 ),
        .I1(\encoded[9]_i_2_n_0 ),
        .I2(\encoded_reg[9] ),
        .I3(\dc_bias_reg[1] ),
        .I4(\processQ_reg[8]_2 ),
        .O(\encoded_reg[9]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'hD7)) 
    \TDMS_encoder_green/encoded[9]_i_1 
       (.I0(\encoded_reg[9] ),
        .I1(\dc_bias_reg[3] ),
        .I2(\processQ_reg[9]_4 ),
        .O(\encoded_reg[9]_1 ));
  LUT6 #(
    .INIT(64'hFFD0FFD0FFFFFFD0)) 
    \dc_bias[3]_i_10 
       (.I0(\dc_bias[3]_i_19__0_n_0 ),
        .I1(\dc_bias[3]_i_20_n_0 ),
        .I2(\dc_bias[3]_i_21__0_n_0 ),
        .I3(\processQ_reg[9]_0 ),
        .I4(\dc_bias[3]_i_22__0_n_0 ),
        .I5(\encoded_reg[0]_0 ),
        .O(\encoded_reg[8]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hFDFF)) 
    \dc_bias[3]_i_11__1 
       (.I0(Q[4]),
        .I1(Q[8]),
        .I2(Q[9]),
        .I3(\dc_bias[3]_i_17__1_n_0 ),
        .O(\encoded_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hAAABAAAA)) 
    \dc_bias[3]_i_12__0 
       (.I0(\processQ_reg[8]_1 ),
        .I1(Q[8]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\dc_bias[3]_i_17__1_n_0 ),
        .O(\encoded_reg[8]_29 ));
  LUT6 #(
    .INIT(64'hFFEAFFEAFFFFFFEA)) 
    \dc_bias[3]_i_13 
       (.I0(\dc_bias[3]_i_28_n_0 ),
        .I1(Q[8]),
        .I2(\dc_bias[3]_i_29_n_0 ),
        .I3(\processQ_reg[8]_1 ),
        .I4(\processQ_reg[2]_0 ),
        .I5(\processQ_reg[4]_0 ),
        .O(\encoded_reg[8] ));
  LUT6 #(
    .INIT(64'h777777777F7E7EFE)) 
    \dc_bias[3]_i_13__0 
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(Q[5]),
        .I3(Q[4]),
        .I4(\dc_bias[3]_i_20__1_n_0 ),
        .I5(Q[6]),
        .O(\encoded_reg[8]_31 ));
  LUT6 #(
    .INIT(64'hFEFFFEFFFFFFFEFF)) 
    \dc_bias[3]_i_14 
       (.I0(\dc_bias[3]_i_31_n_0 ),
        .I1(Q[9]),
        .I2(Q[8]),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(\dc_bias[3]_i_32_n_0 ),
        .O(\encoded_reg[8]_3 ));
  LUT5 #(
    .INIT(32'hF0800080)) 
    \dc_bias[3]_i_15__1 
       (.I0(\processQ_reg[9]_3 ),
        .I1(CO),
        .I2(\processQ_reg[1]_1 [0]),
        .I3(\processQ_reg[1]_1 [1]),
        .I4(\slv_reg11_reg[9]_1 ),
        .O(\encoded_reg[8]_28 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'h00000100)) 
    \dc_bias[3]_i_17__1 
       (.I0(Q[7]),
        .I1(Q[5]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[6]),
        .O(\dc_bias[3]_i_17__1_n_0 ));
  LUT6 #(
    .INIT(64'h00B800FF00B80000)) 
    \dc_bias[3]_i_18 
       (.I0(\dc_bias[3]_i_43_n_0 ),
        .I1(Q[2]),
        .I2(\dc_bias[3]_i_44_n_0 ),
        .I3(\processQ_reg[5]_0 ),
        .I4(Q[1]),
        .I5(\dc_bias_reg[3]_i_46_n_0 ),
        .O(\encoded_reg[8]_0 ));
  LUT6 #(
    .INIT(64'h5155151555555555)) 
    \dc_bias[3]_i_19__0 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(Q[6]),
        .I5(\dc_bias[3]_i_47_n_0 ),
        .O(\dc_bias[3]_i_19__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \dc_bias[3]_i_1__1 
       (.I0(\encoded_reg[9] ),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'h00000440)) 
    \dc_bias[3]_i_20 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[8]),
        .I3(Q[5]),
        .I4(\dc_bias[3]_i_48_n_0 ),
        .O(\dc_bias[3]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \dc_bias[3]_i_20__1 
       (.I0(Q[2]),
        .I1(Q[3]),
        .O(\dc_bias[3]_i_20__1_n_0 ));
  LUT6 #(
    .INIT(64'h4000555540555555)) 
    \dc_bias[3]_i_21__0 
       (.I0(Q[0]),
        .I1(Q[7]),
        .I2(Q[8]),
        .I3(Q[5]),
        .I4(Q[2]),
        .I5(\dc_bias[3]_i_49_n_0 ),
        .O(\dc_bias[3]_i_21__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \dc_bias[3]_i_22__0 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(\dc_bias[3]_i_22__0_n_0 ));
  LUT6 #(
    .INIT(64'h2002300020020003)) 
    \dc_bias[3]_i_28 
       (.I0(\dc_bias[3]_i_52_n_0 ),
        .I1(\dc_bias[3]_i_53_n_0 ),
        .I2(Q[8]),
        .I3(Q[7]),
        .I4(Q[5]),
        .I5(Q[6]),
        .O(\dc_bias[3]_i_28_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dc_bias[3]_i_29 
       (.I0(Q[7]),
        .I1(Q[6]),
        .O(\dc_bias[3]_i_29_n_0 ));
  LUT5 #(
    .INIT(32'h00007FFF)) 
    \dc_bias[3]_i_3 
       (.I0(Q[5]),
        .I1(Q[6]),
        .I2(Q[7]),
        .I3(Q[8]),
        .I4(\processQ_reg[8]_1 ),
        .O(\encoded_reg[9] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF1FFF)) 
    \dc_bias[3]_i_31 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[4]),
        .I3(Q[6]),
        .I4(\dc_bias[3]_i_54_n_0 ),
        .I5(\processQ_reg[7]_0 ),
        .O(\dc_bias[3]_i_31_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \dc_bias[3]_i_32 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(\dc_bias[3]_i_32_n_0 ));
  LUT6 #(
    .INIT(64'h0012048008001048)) 
    \dc_bias[3]_i_43 
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[7]),
        .I3(Q[8]),
        .I4(Q[6]),
        .I5(Q[5]),
        .O(\dc_bias[3]_i_43_n_0 ));
  LUT6 #(
    .INIT(64'h0080010402488010)) 
    \dc_bias[3]_i_44 
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[7]),
        .I3(Q[8]),
        .I4(Q[6]),
        .I5(Q[5]),
        .O(\dc_bias[3]_i_44_n_0 ));
  LUT6 #(
    .INIT(64'h0CCC088800000888)) 
    \dc_bias[3]_i_47 
       (.I0(Q[7]),
        .I1(Q[5]),
        .I2(Q[4]),
        .I3(Q[3]),
        .I4(Q[6]),
        .I5(Q[8]),
        .O(\dc_bias[3]_i_47_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT4 #(
    .INIT(16'hBFFE)) 
    \dc_bias[3]_i_48 
       (.I0(Q[1]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[5]),
        .O(\dc_bias[3]_i_48_n_0 ));
  LUT6 #(
    .INIT(64'hCFFFFFFBFFFFFF3B)) 
    \dc_bias[3]_i_49 
       (.I0(Q[8]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(Q[7]),
        .I5(Q[6]),
        .O(\dc_bias[3]_i_49_n_0 ));
  LUT6 #(
    .INIT(64'h000000D000000000)) 
    \dc_bias[3]_i_4__1 
       (.I0(\processQ_reg[0]_1 ),
        .I1(\processQ_reg[0]_2 ),
        .I2(\dc_bias[3]_i_17__1_n_0 ),
        .I3(Q[9]),
        .I4(Q[8]),
        .I5(Q[4]),
        .O(\encoded_reg[8]_30 ));
  LUT6 #(
    .INIT(64'hAAAAAAA8A8A8A8A8)) 
    \dc_bias[3]_i_52 
       (.I0(Q[7]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(Q[2]),
        .O(\dc_bias[3]_i_52_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT4 #(
    .INIT(16'h00E0)) 
    \dc_bias[3]_i_53 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[4]),
        .I3(Q[5]),
        .O(\dc_bias[3]_i_53_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \dc_bias[3]_i_54 
       (.I0(Q[5]),
        .I1(Q[7]),
        .O(\dc_bias[3]_i_54_n_0 ));
  LUT6 #(
    .INIT(64'h0102401000802408)) 
    \dc_bias[3]_i_59 
       (.I0(Q[3]),
        .I1(Q[5]),
        .I2(Q[6]),
        .I3(Q[7]),
        .I4(Q[8]),
        .I5(Q[4]),
        .O(\dc_bias[3]_i_59_n_0 ));
  LUT6 #(
    .INIT(64'hFF08FFFFFF08FF08)) 
    \dc_bias[3]_i_5__0 
       (.I0(\processQ_reg[9]_1 ),
        .I1(\processQ_reg[9]_2 ),
        .I2(\processQ_reg[4]_1 ),
        .I3(\processQ_reg[0]_0 ),
        .I4(\encoded_reg[0]_0 ),
        .I5(\processQ_reg[1]_0 ),
        .O(\encoded_reg[8]_27 ));
  LUT6 #(
    .INIT(64'h0812040001080024)) 
    \dc_bias[3]_i_60 
       (.I0(Q[3]),
        .I1(Q[8]),
        .I2(Q[7]),
        .I3(Q[6]),
        .I4(Q[5]),
        .I5(Q[4]),
        .O(\dc_bias[3]_i_60_n_0 ));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    \dc_bias[3]_i_9 
       (.I0(Q[2]),
        .I1(DOADO[2]),
        .I2(Q[1]),
        .I3(DOADO[1]),
        .I4(Q[0]),
        .I5(DOADO[0]),
        .O(S));
  MUXF7 \dc_bias_reg[3]_i_46 
       (.I0(\dc_bias[3]_i_59_n_0 ),
        .I1(\dc_bias[3]_i_60_n_0 ),
        .O(\dc_bias_reg[3]_i_46_n_0 ),
        .S(Q[2]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \encoded[0]_i_1__1 
       (.I0(\encoded_reg[9] ),
        .I1(\dc_bias_reg[3] ),
        .O(\encoded_reg[0] ));
  LUT6 #(
    .INIT(64'hAAAAA9AAAAAAAAAA)) 
    \encoded[9]_i_2 
       (.I0(\processQ_reg[6]_0 ),
        .I1(Q[9]),
        .I2(Q[4]),
        .I3(Q[8]),
        .I4(Q[2]),
        .I5(\encoded[9]_i_3_n_0 ),
        .O(\encoded[9]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \encoded[9]_i_3 
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[3]),
        .I3(Q[1]),
        .I4(Q[5]),
        .O(\encoded[9]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i__carry__0_i_1__5
       (.I0(Q[9]),
        .I1(\slv_reg11_reg[8] ),
        .I2(\slv_reg11_reg[7] ),
        .I3(\slv_reg11_reg[9]_0 ),
        .I4(Q[8]),
        .O(\encoded_reg[8]_5 ));
  LUT6 #(
    .INIT(64'hC222ABBB80002AAA)) 
    i__carry__0_i_1__6
       (.I0(Q[9]),
        .I1(\slv_reg11_reg[8] ),
        .I2(\slv_reg11_reg[6]_0 ),
        .I3(\slv_reg11_reg[7]_0 ),
        .I4(\slv_reg11_reg[9]_0 ),
        .I5(Q[8]),
        .O(\encoded_reg[8]_9 ));
  LUT6 #(
    .INIT(64'hC222ABBB80002AAA)) 
    i__carry__0_i_1__7
       (.I0(Q[9]),
        .I1(\slv_reg11_reg[8] ),
        .I2(\slv_reg11_reg[6]_2 ),
        .I3(\slv_reg11_reg[7]_0 ),
        .I4(\slv_reg11_reg[9]_0 ),
        .I5(Q[8]),
        .O(\encoded_reg[8]_17 ));
  LUT6 #(
    .INIT(64'hC222ABBB80002AAA)) 
    i__carry__0_i_1__8
       (.I0(Q[9]),
        .I1(\slv_reg11_reg[8] ),
        .I2(\slv_reg11_reg[6]_4 ),
        .I3(\slv_reg11_reg[7]_0 ),
        .I4(\slv_reg11_reg[9]_0 ),
        .I5(Q[8]),
        .O(\encoded_reg[8]_25 ));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry__0_i_2__5
       (.I0(\slv_reg11_reg[9]_0 ),
        .I1(Q[9]),
        .I2(\slv_reg11_reg[8] ),
        .I3(\slv_reg11_reg[7] ),
        .I4(Q[8]),
        .O(\encoded_reg[8]_4 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__6
       (.I0(\slv_reg11_reg[9]_0 ),
        .I1(Q[9]),
        .I2(\slv_reg11_reg[8] ),
        .I3(\slv_reg11_reg[6]_0 ),
        .I4(\slv_reg11_reg[7]_0 ),
        .I5(Q[8]),
        .O(\encoded_reg[8]_8 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__7
       (.I0(\slv_reg11_reg[9]_0 ),
        .I1(Q[9]),
        .I2(\slv_reg11_reg[8] ),
        .I3(\slv_reg11_reg[6]_2 ),
        .I4(\slv_reg11_reg[7]_0 ),
        .I5(Q[8]),
        .O(\encoded_reg[8]_16 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    i__carry__0_i_2__8
       (.I0(\slv_reg11_reg[9]_0 ),
        .I1(Q[9]),
        .I2(\slv_reg11_reg[8] ),
        .I3(\slv_reg11_reg[6]_4 ),
        .I4(\slv_reg11_reg[7]_0 ),
        .I5(Q[8]),
        .O(\encoded_reg[8]_24 ));
  LUT6 #(
    .INIT(64'h7877788887888777)) 
    i__carry_i_1__10
       (.I0(\slv_reg11_reg[8] ),
        .I1(\slv_reg11_reg[7]_1 ),
        .I2(\slv_reg11_reg[9] [3]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\int_trigger_volt_s_reg[9] [3]),
        .I5(Q[9]),
        .O(\encoded_reg[8]_12 [3]));
  LUT5 #(
    .INIT(32'h15403D54)) 
    i__carry_i_1__6
       (.I0(Q[7]),
        .I1(\slv_reg11_reg[6] ),
        .I2(\slv_reg11_reg[5] ),
        .I3(\slv_reg11_reg[7]_0 ),
        .I4(Q[6]),
        .O(\encoded_reg[8]_6 [3]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_1__7
       (.I0(Q[7]),
        .I1(\slv_reg11_reg[6] ),
        .I2(\slv_reg11_reg[5]_0 ),
        .I3(\slv_reg11_reg[7]_0 ),
        .I4(Q[6]),
        .O(\encoded_reg[8]_10 [3]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_1__8
       (.I0(Q[7]),
        .I1(\slv_reg11_reg[6] ),
        .I2(\slv_reg11_reg[5]_2 ),
        .I3(\slv_reg11_reg[7]_0 ),
        .I4(Q[6]),
        .O(\encoded_reg[8]_18 [3]));
  LUT6 #(
    .INIT(64'h2C22BABB0800A2AA)) 
    i__carry_i_1__9
       (.I0(Q[7]),
        .I1(\slv_reg11_reg[6] ),
        .I2(\int_trigger_volt_s_reg[2]_0 ),
        .I3(\slv_reg11_reg[5]_3 ),
        .I4(\slv_reg11_reg[7]_0 ),
        .I5(Q[6]),
        .O(DI[3]));
  LUT6 #(
    .INIT(64'hBCCC2AAAA8880222)) 
    i__carry_i_2__10
       (.I0(Q[5]),
        .I1(\slv_reg11_reg[4] ),
        .I2(\int_trigger_volt_s_reg[2]_2 ),
        .I3(\slv_reg11_reg[3] ),
        .I4(\slv_reg11_reg[5]_3 ),
        .I5(Q[4]),
        .O(DI[2]));
  LUT6 #(
    .INIT(64'h0000000006909009)) 
    i__carry_i_2__6
       (.I0(\slv_reg11_reg[8] ),
        .I1(Q[8]),
        .I2(Q[7]),
        .I3(\slv_reg11_reg[6]_1 ),
        .I4(\slv_reg11_reg[7]_0 ),
        .I5(i__carry_i_7__6_n_0),
        .O(\encoded_reg[8]_12 [2]));
  LUT6 #(
    .INIT(64'h1011454434335D55)) 
    i__carry_i_2__7
       (.I0(Q[5]),
        .I1(\slv_reg11_reg[4] ),
        .I2(\slv_reg11_reg[0] ),
        .I3(\slv_reg11_reg[3] ),
        .I4(\slv_reg11_reg[5]_3 ),
        .I5(Q[4]),
        .O(\encoded_reg[8]_6 [2]));
  LUT5 #(
    .INIT(32'hBC2AA802)) 
    i__carry_i_2__8
       (.I0(Q[5]),
        .I1(\slv_reg11_reg[4] ),
        .I2(\slv_reg11_reg[0]_0 ),
        .I3(\slv_reg11_reg[5]_3 ),
        .I4(Q[4]),
        .O(\encoded_reg[8]_10 [2]));
  LUT5 #(
    .INIT(32'hBC2AA802)) 
    i__carry_i_2__9
       (.I0(Q[5]),
        .I1(\slv_reg11_reg[4] ),
        .I2(\slv_reg11_reg[0]_1 ),
        .I3(\slv_reg11_reg[5]_3 ),
        .I4(Q[4]),
        .O(\encoded_reg[8]_18 [2]));
  LUT5 #(
    .INIT(32'hC2AB802A)) 
    i__carry_i_3__10
       (.I0(Q[3]),
        .I1(\slv_reg11_reg[2] ),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[3] ),
        .I4(Q[2]),
        .O(DI[1]));
  LUT6 #(
    .INIT(64'h0000000060060690)) 
    i__carry_i_3__6
       (.I0(\slv_reg11_reg[5]_3 ),
        .I1(Q[5]),
        .I2(Q[4]),
        .I3(\int_trigger_volt_s_reg[3] ),
        .I4(\slv_reg11_reg[4] ),
        .I5(i__carry_i_9__9_n_0),
        .O(\encoded_reg[8]_12 [1]));
  LUT6 #(
    .INIT(64'h000155545403FD55)) 
    i__carry_i_3__7
       (.I0(Q[3]),
        .I1(\slv_reg11_reg[0]_2 ),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[2] ),
        .I4(\slv_reg11_reg[3] ),
        .I5(Q[2]),
        .O(\encoded_reg[8]_6 [1]));
  LUT6 #(
    .INIT(64'hC02AAABF80002AAA)) 
    i__carry_i_3__8
       (.I0(Q[3]),
        .I1(\slv_reg11_reg[0]_2 ),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[2] ),
        .I4(\slv_reg11_reg[3] ),
        .I5(Q[2]),
        .O(\encoded_reg[8]_10 [1]));
  LUT6 #(
    .INIT(64'hCCC2AAAB8880222A)) 
    i__carry_i_3__9
       (.I0(Q[3]),
        .I1(\slv_reg11_reg[2] ),
        .I2(\slv_reg11_reg[0]_2 ),
        .I3(\slv_reg11_reg[1] ),
        .I4(\slv_reg11_reg[3] ),
        .I5(Q[2]),
        .O(\encoded_reg[8]_18 [1]));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    i__carry_i_4__5
       (.I0(\slv_reg11_reg[2] ),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\slv_reg11_reg[0]_2 ),
        .I4(Q[1]),
        .I5(\slv_reg11_reg[1] ),
        .O(\encoded_reg[8]_12 [0]));
  LUT6 #(
    .INIT(64'h5017505050171717)) 
    i__carry_i_4__6
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[9] [0]),
        .I4(\slv_reg5_reg[0] ),
        .I5(\int_trigger_volt_s_reg[9] [0]),
        .O(\encoded_reg[8]_6 [0]));
  LUT6 #(
    .INIT(64'hFFFF1D001D000000)) 
    i__carry_i_4__7
       (.I0(\int_trigger_volt_s_reg[9] [0]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(Q[0]),
        .I4(Q[1]),
        .I5(\slv_reg11_reg[1] ),
        .O(DI[0]));
  LUT6 #(
    .INIT(64'hFFE200E21DE20000)) 
    i__carry_i_4__8
       (.I0(\int_trigger_volt_s_reg[9] [0]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(\slv_reg11_reg[1] ),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(\encoded_reg[8]_18 [0]));
  LUT6 #(
    .INIT(64'hE0EEE0008A888AAA)) 
    i__carry_i_4__9
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\int_trigger_volt_s_reg[9] [0]),
        .I5(\slv_reg11_reg[1] ),
        .O(\encoded_reg[8]_10 [0]));
  LUT6 #(
    .INIT(64'h9009909009600909)) 
    i__carry_i_5__10
       (.I0(\slv_reg11_reg[7]_0 ),
        .I1(Q[7]),
        .I2(\slv_reg11_reg[6] ),
        .I3(\int_trigger_volt_s_reg[2]_0 ),
        .I4(\slv_reg11_reg[5]_3 ),
        .I5(Q[6]),
        .O(\encoded_reg[8]_26 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__6
       (.I0(\slv_reg11_reg[7]_0 ),
        .I1(Q[7]),
        .I2(\slv_reg11_reg[6] ),
        .I3(\slv_reg11_reg[5] ),
        .I4(Q[6]),
        .O(\encoded_reg[8]_7 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__7
       (.I0(\slv_reg11_reg[7]_0 ),
        .I1(Q[7]),
        .I2(\slv_reg11_reg[6] ),
        .I3(\slv_reg11_reg[5]_0 ),
        .I4(Q[6]),
        .O(\encoded_reg[8]_11 [3]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_5__9
       (.I0(\slv_reg11_reg[7]_0 ),
        .I1(Q[7]),
        .I2(\slv_reg11_reg[6] ),
        .I3(\slv_reg11_reg[5]_2 ),
        .I4(Q[6]),
        .O(\encoded_reg[8]_19 [3]));
  LUT6 #(
    .INIT(64'h6009090906606060)) 
    i__carry_i_6__10
       (.I0(\slv_reg11_reg[5]_3 ),
        .I1(Q[5]),
        .I2(\slv_reg11_reg[4] ),
        .I3(\int_trigger_volt_s_reg[2]_2 ),
        .I4(\slv_reg11_reg[3] ),
        .I5(Q[4]),
        .O(\encoded_reg[8]_26 [2]));
  LUT6 #(
    .INIT(64'h0960090960066060)) 
    i__carry_i_6__7
       (.I0(\slv_reg11_reg[5]_3 ),
        .I1(Q[5]),
        .I2(\slv_reg11_reg[4] ),
        .I3(\slv_reg11_reg[0] ),
        .I4(\slv_reg11_reg[3] ),
        .I5(Q[4]),
        .O(\encoded_reg[8]_7 [2]));
  LUT5 #(
    .INIT(32'h60090660)) 
    i__carry_i_6__8
       (.I0(\slv_reg11_reg[5]_3 ),
        .I1(Q[5]),
        .I2(\slv_reg11_reg[4] ),
        .I3(\slv_reg11_reg[0]_0 ),
        .I4(Q[4]),
        .O(\encoded_reg[8]_11 [2]));
  LUT5 #(
    .INIT(32'h60090660)) 
    i__carry_i_6__9
       (.I0(\slv_reg11_reg[5]_3 ),
        .I1(Q[5]),
        .I2(\slv_reg11_reg[4] ),
        .I3(\slv_reg11_reg[0]_1 ),
        .I4(Q[4]),
        .O(\encoded_reg[8]_19 [2]));
  LUT5 #(
    .INIT(32'h09906009)) 
    i__carry_i_7__10
       (.I0(\slv_reg11_reg[3] ),
        .I1(Q[3]),
        .I2(\slv_reg11_reg[2] ),
        .I3(\slv_reg11_reg[1] ),
        .I4(Q[2]),
        .O(\encoded_reg[8]_26 [1]));
  LUT6 #(
    .INIT(64'h9999955566666AAA)) 
    i__carry_i_7__6
       (.I0(Q[6]),
        .I1(\slv_reg11_reg[5]_3 ),
        .I2(\slv_reg11_reg[3] ),
        .I3(\slv_reg11_reg[2] ),
        .I4(\slv_reg11_reg[4] ),
        .I5(\slv_reg11_reg[6] ),
        .O(i__carry_i_7__6_n_0));
  LUT6 #(
    .INIT(64'h2121211884848442)) 
    i__carry_i_7__7
       (.I0(Q[2]),
        .I1(\slv_reg11_reg[3] ),
        .I2(\slv_reg11_reg[2] ),
        .I3(\slv_reg11_reg[1] ),
        .I4(\slv_reg11_reg[0]_2 ),
        .I5(Q[3]),
        .O(\encoded_reg[8]_7 [1]));
  LUT6 #(
    .INIT(64'h1884848442212121)) 
    i__carry_i_7__8
       (.I0(Q[2]),
        .I1(\slv_reg11_reg[3] ),
        .I2(\slv_reg11_reg[2] ),
        .I3(\slv_reg11_reg[1] ),
        .I4(\slv_reg11_reg[0]_2 ),
        .I5(Q[3]),
        .O(\encoded_reg[8]_11 [1]));
  LUT6 #(
    .INIT(64'h0909099060606009)) 
    i__carry_i_7__9
       (.I0(\slv_reg11_reg[3] ),
        .I1(Q[3]),
        .I2(\slv_reg11_reg[2] ),
        .I3(\slv_reg11_reg[1] ),
        .I4(\slv_reg11_reg[0]_2 ),
        .I5(Q[2]),
        .O(\encoded_reg[8]_19 [1]));
  LUT6 #(
    .INIT(64'h1482141414828282)) 
    i__carry_i_8__4
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[9] [0]),
        .I4(\slv_reg5_reg[0] ),
        .I5(\int_trigger_volt_s_reg[9] [0]),
        .O(\encoded_reg[8]_11 [0]));
  LUT6 #(
    .INIT(64'h4128414141282828)) 
    i__carry_i_8__5
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[9] [0]),
        .I4(\slv_reg5_reg[0] ),
        .I5(\int_trigger_volt_s_reg[9] [0]),
        .O(\encoded_reg[8]_7 [0]));
  LUT6 #(
    .INIT(64'h6066600006000666)) 
    i__carry_i_8__6
       (.I0(\slv_reg11_reg[1] ),
        .I1(Q[1]),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\int_trigger_volt_s_reg[9] [0]),
        .I5(Q[0]),
        .O(\encoded_reg[8]_26 [0]));
  LUT6 #(
    .INIT(64'h4128414141282828)) 
    i__carry_i_8__7
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[9] [0]),
        .I4(\slv_reg5_reg[0] ),
        .I5(\int_trigger_volt_s_reg[9] [0]),
        .O(\encoded_reg[8]_19 [0]));
  LUT6 #(
    .INIT(64'h9A956A65959A656A)) 
    i__carry_i_9__9
       (.I0(Q[3]),
        .I1(\slv_reg11_reg[9] [1]),
        .I2(\slv_reg5_reg[0] ),
        .I3(\int_trigger_volt_s_reg[9] [1]),
        .I4(\slv_reg11_reg[9] [2]),
        .I5(\int_trigger_volt_s_reg[9] [2]),
        .O(i__carry_i_9__9_n_0));
  LUT5 #(
    .INIT(32'h15403D54)) 
    pixel_color4_carry__0_i_1
       (.I0(Q[9]),
        .I1(\slv_reg11_reg[8] ),
        .I2(\int_trigger_volt_s_reg[7] ),
        .I3(\slv_reg11_reg[9]_0 ),
        .I4(Q[8]),
        .O(\encoded_reg[8]_14 ));
  LUT5 #(
    .INIT(32'h09906009)) 
    pixel_color4_carry__0_i_2
       (.I0(\slv_reg11_reg[9]_0 ),
        .I1(Q[9]),
        .I2(\slv_reg11_reg[8] ),
        .I3(\int_trigger_volt_s_reg[7] ),
        .I4(Q[8]),
        .O(\encoded_reg[8]_13 ));
  LUT5 #(
    .INIT(32'h15403D54)) 
    pixel_color4_carry_i_1
       (.I0(Q[7]),
        .I1(\slv_reg11_reg[6] ),
        .I2(\slv_reg11_reg[5]_1 ),
        .I3(\slv_reg11_reg[7]_0 ),
        .I4(Q[6]),
        .O(\encoded_reg[8]_2 [3]));
  LUT6 #(
    .INIT(64'h1011454434335D55)) 
    pixel_color4_carry_i_2
       (.I0(Q[5]),
        .I1(\slv_reg11_reg[4] ),
        .I2(\int_trigger_volt_s_reg[2]_1 ),
        .I3(\slv_reg11_reg[3] ),
        .I4(\slv_reg11_reg[5]_3 ),
        .I5(Q[4]),
        .O(\encoded_reg[8]_2 [2]));
  LUT6 #(
    .INIT(64'h0015403F4000FFD5)) 
    pixel_color4_carry_i_3
       (.I0(Q[2]),
        .I1(\slv_reg11_reg[1] ),
        .I2(\slv_reg11_reg[0]_2 ),
        .I3(\slv_reg11_reg[2] ),
        .I4(Q[3]),
        .I5(\slv_reg11_reg[3] ),
        .O(\encoded_reg[8]_2 [1]));
  LUT6 #(
    .INIT(64'h000056A602A257F7)) 
    pixel_color4_carry_i_4
       (.I0(\slv_reg11_reg[1] ),
        .I1(\int_trigger_volt_s_reg[9] [0]),
        .I2(\slv_reg5_reg[0] ),
        .I3(\slv_reg11_reg[9] [0]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(\encoded_reg[8]_2 [0]));
  LUT5 #(
    .INIT(32'h09906009)) 
    pixel_color4_carry_i_5
       (.I0(\slv_reg11_reg[7]_0 ),
        .I1(Q[7]),
        .I2(\slv_reg11_reg[6] ),
        .I3(\slv_reg11_reg[5]_1 ),
        .I4(Q[6]),
        .O(\encoded_reg[8]_15 [3]));
  LUT6 #(
    .INIT(64'h0960090960066060)) 
    pixel_color4_carry_i_6
       (.I0(\slv_reg11_reg[5]_3 ),
        .I1(Q[5]),
        .I2(\slv_reg11_reg[4] ),
        .I3(\int_trigger_volt_s_reg[2]_1 ),
        .I4(\slv_reg11_reg[3] ),
        .I5(Q[4]),
        .O(\encoded_reg[8]_15 [2]));
  LUT6 #(
    .INIT(64'h6009090906606060)) 
    pixel_color4_carry_i_7
       (.I0(\slv_reg11_reg[3] ),
        .I1(Q[3]),
        .I2(\slv_reg11_reg[2] ),
        .I3(\slv_reg11_reg[0]_2 ),
        .I4(\slv_reg11_reg[1] ),
        .I5(Q[2]),
        .O(\encoded_reg[8]_15 [1]));
  LUT6 #(
    .INIT(64'h1482141414828282)) 
    pixel_color4_carry_i_8
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[9] [0]),
        .I4(\slv_reg5_reg[0] ),
        .I5(\int_trigger_volt_s_reg[9] [0]),
        .O(\encoded_reg[8]_15 [0]));
  LUT6 #(
    .INIT(64'h155540003DDD5444)) 
    pixel_color5_carry__0_i_1
       (.I0(Q[9]),
        .I1(\slv_reg11_reg[8] ),
        .I2(\slv_reg11_reg[6]_3 ),
        .I3(\slv_reg11_reg[7]_0 ),
        .I4(\slv_reg11_reg[9]_0 ),
        .I5(Q[8]),
        .O(\encoded_reg[8]_21 ));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    pixel_color5_carry__0_i_2
       (.I0(\slv_reg11_reg[9]_0 ),
        .I1(Q[9]),
        .I2(\slv_reg11_reg[8] ),
        .I3(\slv_reg11_reg[6]_3 ),
        .I4(\slv_reg11_reg[7]_0 ),
        .I5(Q[8]),
        .O(\encoded_reg[8]_20 ));
  LUT6 #(
    .INIT(64'h51550400D3DD4544)) 
    pixel_color5_carry_i_1
       (.I0(Q[7]),
        .I1(\slv_reg11_reg[6] ),
        .I2(\int_trigger_volt_s_reg[2] ),
        .I3(\slv_reg11_reg[5]_3 ),
        .I4(\slv_reg11_reg[7]_0 ),
        .I5(Q[6]),
        .O(\encoded_reg[8]_22 [3]));
  LUT5 #(
    .INIT(32'h015443D5)) 
    pixel_color5_carry_i_2
       (.I0(Q[5]),
        .I1(\slv_reg11_reg[4] ),
        .I2(\slv_reg11_reg[1]_0 ),
        .I3(\slv_reg11_reg[5]_3 ),
        .I4(Q[4]),
        .O(\encoded_reg[8]_22 [2]));
  LUT5 #(
    .INIT(32'h015443D5)) 
    pixel_color5_carry_i_3
       (.I0(Q[3]),
        .I1(\slv_reg11_reg[2] ),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[3] ),
        .I4(Q[2]),
        .O(\encoded_reg[8]_22 [1]));
  LUT6 #(
    .INIT(64'h000000E200E2FFFF)) 
    pixel_color5_carry_i_4
       (.I0(\int_trigger_volt_s_reg[9] [0]),
        .I1(\slv_reg5_reg[0] ),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(Q[0]),
        .I4(Q[1]),
        .I5(\slv_reg11_reg[1] ),
        .O(\encoded_reg[8]_22 [0]));
  LUT6 #(
    .INIT(64'h9009909009600909)) 
    pixel_color5_carry_i_5
       (.I0(\slv_reg11_reg[7]_0 ),
        .I1(Q[7]),
        .I2(\slv_reg11_reg[6] ),
        .I3(\int_trigger_volt_s_reg[2] ),
        .I4(\slv_reg11_reg[5]_3 ),
        .I5(Q[6]),
        .O(\encoded_reg[8]_23 [3]));
  LUT5 #(
    .INIT(32'h60090660)) 
    pixel_color5_carry_i_6
       (.I0(\slv_reg11_reg[5]_3 ),
        .I1(Q[5]),
        .I2(\slv_reg11_reg[4] ),
        .I3(\slv_reg11_reg[1]_0 ),
        .I4(Q[4]),
        .O(\encoded_reg[8]_23 [2]));
  LUT5 #(
    .INIT(32'h21188442)) 
    pixel_color5_carry_i_7
       (.I0(Q[2]),
        .I1(\slv_reg11_reg[3] ),
        .I2(\slv_reg11_reg[1] ),
        .I3(\slv_reg11_reg[2] ),
        .I4(Q[3]),
        .O(\encoded_reg[8]_23 [1]));
  LUT6 #(
    .INIT(64'h6066600006000666)) 
    pixel_color5_carry_i_8
       (.I0(\slv_reg11_reg[1] ),
        .I1(Q[1]),
        .I2(\slv_reg11_reg[9] [0]),
        .I3(\slv_reg5_reg[0] ),
        .I4(\int_trigger_volt_s_reg[9] [0]),
        .I5(Q[0]),
        .O(\encoded_reg[8]_23 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    \processQ[0]_i_1__0 
       (.I0(Q[0]),
        .O(plusOp__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \processQ[1]_i_1__0 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(plusOp__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \processQ[2]_i_1__0 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(\processQ[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \processQ[3]_i_1__0 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(plusOp__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \processQ[4]_i_1__0 
       (.I0(Q[4]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[3]),
        .O(plusOp__2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processQ[5]_i_1__0 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(plusOp__2[5]));
  LUT6 #(
    .INIT(64'hAAAA6AAAAAAAAAAA)) 
    \processQ[6]_i_1__1 
       (.I0(Q[6]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(\processQ[6]_i_2_n_0 ),
        .I5(Q[5]),
        .O(\processQ[6]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \processQ[6]_i_2 
       (.I0(Q[4]),
        .I1(Q[3]),
        .O(\processQ[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'h9A)) 
    \processQ[7]_i_1__0 
       (.I0(Q[7]),
        .I1(\processQ[8]_i_2_n_0 ),
        .I2(Q[6]),
        .O(plusOp__2[7]));
  LUT4 #(
    .INIT(16'h9AAA)) 
    \processQ[8]_i_1__0 
       (.I0(Q[8]),
        .I1(\processQ[8]_i_2_n_0 ),
        .I2(Q[7]),
        .I3(Q[6]),
        .O(plusOp__2[8]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \processQ[8]_i_2 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(\processQ[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00010000FFFFFFFF)) 
    \processQ[9]_i_1__0 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[8]),
        .I3(\processQ[9]_i_4__0_n_0 ),
        .I4(\processQ_reg[8]_0 ),
        .I5(reset_n),
        .O(\processQ[9]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000888AAAAAAAA)) 
    \processQ[9]_i_2__0 
       (.I0(\processQ_reg[8]_0 ),
        .I1(\processQ[9]_i_5_n_0 ),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[8]),
        .I5(Q[9]),
        .O(processQ0));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \processQ[9]_i_3__0 
       (.I0(Q[9]),
        .I1(Q[8]),
        .I2(Q[7]),
        .I3(Q[6]),
        .I4(Q[5]),
        .I5(\processQ[9]_i_6_n_0 ),
        .O(plusOp__2[9]));
  LUT6 #(
    .INIT(64'hFEFFFFFFFFFFFFFF)) 
    \processQ[9]_i_4__0 
       (.I0(Q[5]),
        .I1(Q[6]),
        .I2(\processQ[9]_i_7_n_0 ),
        .I3(Q[9]),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\processQ[9]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \processQ[9]_i_5 
       (.I0(Q[7]),
        .I1(Q[4]),
        .I2(Q[6]),
        .I3(Q[5]),
        .O(\processQ[9]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \processQ[9]_i_6 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[4]),
        .O(\processQ[9]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \processQ[9]_i_7 
       (.I0(Q[4]),
        .I1(Q[7]),
        .O(\processQ[9]_i_7_n_0 ));
  FDRE \processQ_reg[0] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[0]),
        .Q(Q[0]),
        .R(\processQ[9]_i_1__0_n_0 ));
  FDRE \processQ_reg[1] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[1]),
        .Q(Q[1]),
        .R(\processQ[9]_i_1__0_n_0 ));
  FDRE \processQ_reg[2] 
       (.C(CLK),
        .CE(processQ0),
        .D(\processQ[2]_i_1__0_n_0 ),
        .Q(Q[2]),
        .R(\processQ[9]_i_1__0_n_0 ));
  FDRE \processQ_reg[3] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[3]),
        .Q(Q[3]),
        .R(\processQ[9]_i_1__0_n_0 ));
  FDRE \processQ_reg[4] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[4]),
        .Q(Q[4]),
        .R(\processQ[9]_i_1__0_n_0 ));
  FDRE \processQ_reg[5] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[5]),
        .Q(Q[5]),
        .R(\processQ[9]_i_1__0_n_0 ));
  FDRE \processQ_reg[6] 
       (.C(CLK),
        .CE(processQ0),
        .D(\processQ[6]_i_1__1_n_0 ),
        .Q(Q[6]),
        .R(\processQ[9]_i_1__0_n_0 ));
  FDRE \processQ_reg[7] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[7]),
        .Q(Q[7]),
        .R(\processQ[9]_i_1__0_n_0 ));
  FDRE \processQ_reg[8] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[8]),
        .Q(Q[8]),
        .R(\processQ[9]_i_1__0_n_0 ));
  FDRE \processQ_reg[9] 
       (.C(CLK),
        .CE(processQ0),
        .D(plusOp__2[9]),
        .Q(Q[9]),
        .R(\processQ[9]_i_1__0_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga
   (\int_trigger_time_s_reg[0] ,
    \sdp_bl.ramb18_dp_bl.ram18_bl ,
    \encoded_reg[8] ,
    \encoded_reg[9] ,
    \encoded_reg[8]_0 ,
    \encoded_reg[0] ,
    \processQ_reg[9] ,
    ADDRARDADDR,
    \encoded_reg[8]_1 ,
    \encoded_reg[8]_2 ,
    \int_trigger_time_s_reg[0]_0 ,
    \int_trigger_time_s_reg[0]_1 ,
    \int_trigger_time_s_reg[0]_2 ,
    \int_trigger_time_s_reg[0]_3 ,
    \int_trigger_time_s_reg[0]_4 ,
    \int_trigger_time_s_reg[0]_5 ,
    \int_trigger_time_s_reg[0]_6 ,
    \encoded_reg[8]_3 ,
    \dc_bias_reg[3] ,
    \encoded_reg[8]_4 ,
    \encoded_reg[2] ,
    \encoded_reg[1] ,
    \encoded_reg[4] ,
    \encoded_reg[0]_0 ,
    \encoded_reg[0]_1 ,
    SR,
    S,
    \encoded_reg[9]_0 ,
    \encoded_reg[9]_1 ,
    \encoded_reg[9]_2 ,
    Q,
    \slv_reg5_reg[0] ,
    \slv_reg10_reg[9] ,
    \dc_bias_reg[1] ,
    \dc_bias_reg[3]_0 ,
    reset_n,
    \dc_bias_reg[2] ,
    \slv_reg10_reg[5] ,
    \slv_reg10_reg[3] ,
    \slv_reg10_reg[6] ,
    \int_trigger_time_s_reg[1] ,
    \slv_reg10_reg[6]_0 ,
    \int_trigger_time_s_reg[1]_0 ,
    \slv_reg10_reg[6]_1 ,
    \int_trigger_time_s_reg[1]_1 ,
    \slv_reg10_reg[2] ,
    CO,
    switch,
    \processQ_reg[9]_0 ,
    \processQ_reg[8] ,
    \dc_bias_reg[3]_1 ,
    \slv_reg11_reg[1] ,
    \int_trigger_volt_s_reg[9] ,
    \slv_reg11_reg[9] ,
    DOADO,
    \slv_reg11_reg[9]_0 ,
    \slv_reg11_reg[8] ,
    \slv_reg11_reg[7] ,
    \slv_reg11_reg[6] ,
    \slv_reg11_reg[5] ,
    \slv_reg11_reg[7]_0 ,
    \slv_reg11_reg[6]_0 ,
    \slv_reg11_reg[5]_0 ,
    \slv_reg11_reg[6]_1 ,
    \slv_reg11_reg[6]_2 ,
    \slv_reg11_reg[5]_1 ,
    \slv_reg11_reg[6]_3 ,
    \int_trigger_volt_s_reg[2] ,
    \slv_reg11_reg[5]_2 ,
    \slv_reg11_reg[6]_4 ,
    \int_trigger_volt_s_reg[2]_0 ,
    \slv_reg11_reg[3] ,
    \slv_reg11_reg[2] ,
    \slv_reg11_reg[4] ,
    \slv_reg11_reg[0] ,
    \slv_reg11_reg[0]_0 ,
    \int_trigger_volt_s_reg[3] ,
    \slv_reg11_reg[0]_1 ,
    \slv_reg11_reg[0]_2 ,
    \slv_reg11_reg[1]_0 ,
    \int_trigger_volt_s_reg[2]_1 ,
    \processQ_reg[9]_1 ,
    \slv_reg11_reg[7]_1 ,
    \slv_reg10_reg[7] ,
    \dc_bias_reg[3]_2 ,
    CLK);
  output \int_trigger_time_s_reg[0] ;
  output [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  output \encoded_reg[8] ;
  output \encoded_reg[9] ;
  output \encoded_reg[8]_0 ;
  output \encoded_reg[0] ;
  output [9:0]\processQ_reg[9] ;
  output [7:0]ADDRARDADDR;
  output \encoded_reg[8]_1 ;
  output \encoded_reg[8]_2 ;
  output \int_trigger_time_s_reg[0]_0 ;
  output \int_trigger_time_s_reg[0]_1 ;
  output \int_trigger_time_s_reg[0]_2 ;
  output \int_trigger_time_s_reg[0]_3 ;
  output \int_trigger_time_s_reg[0]_4 ;
  output \int_trigger_time_s_reg[0]_5 ;
  output \int_trigger_time_s_reg[0]_6 ;
  output \encoded_reg[8]_3 ;
  output \dc_bias_reg[3] ;
  output \encoded_reg[8]_4 ;
  output \encoded_reg[2] ;
  output \encoded_reg[1] ;
  output \encoded_reg[4] ;
  output \encoded_reg[0]_0 ;
  output \encoded_reg[0]_1 ;
  output [0:0]SR;
  output [0:0]S;
  output \encoded_reg[9]_0 ;
  output \encoded_reg[9]_1 ;
  output \encoded_reg[9]_2 ;
  input [9:0]Q;
  input [0:0]\slv_reg5_reg[0] ;
  input [9:0]\slv_reg10_reg[9] ;
  input \dc_bias_reg[1] ;
  input [0:0]\dc_bias_reg[3]_0 ;
  input reset_n;
  input \dc_bias_reg[2] ;
  input \slv_reg10_reg[5] ;
  input \slv_reg10_reg[3] ;
  input \slv_reg10_reg[6] ;
  input \int_trigger_time_s_reg[1] ;
  input \slv_reg10_reg[6]_0 ;
  input \int_trigger_time_s_reg[1]_0 ;
  input \slv_reg10_reg[6]_1 ;
  input \int_trigger_time_s_reg[1]_1 ;
  input \slv_reg10_reg[2] ;
  input [0:0]CO;
  input [1:0]switch;
  input \processQ_reg[9]_0 ;
  input \processQ_reg[8] ;
  input [0:0]\dc_bias_reg[3]_1 ;
  input \slv_reg11_reg[1] ;
  input [5:0]\int_trigger_volt_s_reg[9] ;
  input [5:0]\slv_reg11_reg[9] ;
  input [2:0]DOADO;
  input \slv_reg11_reg[9]_0 ;
  input \slv_reg11_reg[8] ;
  input \slv_reg11_reg[7] ;
  input \slv_reg11_reg[6] ;
  input \slv_reg11_reg[5] ;
  input \slv_reg11_reg[7]_0 ;
  input \slv_reg11_reg[6]_0 ;
  input \slv_reg11_reg[5]_0 ;
  input \slv_reg11_reg[6]_1 ;
  input \slv_reg11_reg[6]_2 ;
  input \slv_reg11_reg[5]_1 ;
  input \slv_reg11_reg[6]_3 ;
  input \int_trigger_volt_s_reg[2] ;
  input \slv_reg11_reg[5]_2 ;
  input \slv_reg11_reg[6]_4 ;
  input \int_trigger_volt_s_reg[2]_0 ;
  input \slv_reg11_reg[3] ;
  input \slv_reg11_reg[2] ;
  input \slv_reg11_reg[4] ;
  input \slv_reg11_reg[0] ;
  input \slv_reg11_reg[0]_0 ;
  input \int_trigger_volt_s_reg[3] ;
  input \slv_reg11_reg[0]_1 ;
  input \slv_reg11_reg[0]_2 ;
  input \slv_reg11_reg[1]_0 ;
  input \int_trigger_volt_s_reg[2]_1 ;
  input [0:0]\processQ_reg[9]_1 ;
  input \slv_reg11_reg[7]_1 ;
  input \slv_reg10_reg[7] ;
  input [0:0]\dc_bias_reg[3]_2 ;
  input CLK;

  wire [7:0]ADDRARDADDR;
  wire CLK;
  wire [0:0]CO;
  wire [2:0]DOADO;
  wire [9:0]Q;
  wire [0:0]S;
  wire [0:0]SR;
  wire \dc_bias_reg[1] ;
  wire \dc_bias_reg[2] ;
  wire \dc_bias_reg[3] ;
  wire [0:0]\dc_bias_reg[3]_0 ;
  wire [0:0]\dc_bias_reg[3]_1 ;
  wire [0:0]\dc_bias_reg[3]_2 ;
  wire \encoded_reg[0] ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[0]_1 ;
  wire \encoded_reg[1] ;
  wire \encoded_reg[2] ;
  wire \encoded_reg[4] ;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \encoded_reg[8]_2 ;
  wire \encoded_reg[8]_3 ;
  wire \encoded_reg[8]_4 ;
  wire \encoded_reg[9] ;
  wire \encoded_reg[9]_0 ;
  wire \encoded_reg[9]_1 ;
  wire \encoded_reg[9]_2 ;
  wire horiz_counter_n_0;
  wire horiz_counter_n_1;
  wire horiz_counter_n_2;
  wire horiz_counter_n_20;
  wire horiz_counter_n_21;
  wire horiz_counter_n_22;
  wire horiz_counter_n_23;
  wire horiz_counter_n_24;
  wire horiz_counter_n_25;
  wire horiz_counter_n_26;
  wire horiz_counter_n_27;
  wire horiz_counter_n_28;
  wire horiz_counter_n_29;
  wire horiz_counter_n_3;
  wire horiz_counter_n_30;
  wire horiz_counter_n_31;
  wire horiz_counter_n_32;
  wire horiz_counter_n_33;
  wire horiz_counter_n_34;
  wire horiz_counter_n_35;
  wire horiz_counter_n_36;
  wire horiz_counter_n_37;
  wire horiz_counter_n_38;
  wire horiz_counter_n_39;
  wire horiz_counter_n_40;
  wire horiz_counter_n_41;
  wire horiz_counter_n_42;
  wire horiz_counter_n_43;
  wire horiz_counter_n_44;
  wire horiz_counter_n_45;
  wire horiz_counter_n_46;
  wire horiz_counter_n_47;
  wire horiz_counter_n_48;
  wire horiz_counter_n_49;
  wire horiz_counter_n_50;
  wire horiz_counter_n_51;
  wire horiz_counter_n_52;
  wire horiz_counter_n_53;
  wire horiz_counter_n_54;
  wire horiz_counter_n_55;
  wire horiz_counter_n_56;
  wire horiz_counter_n_57;
  wire horiz_counter_n_58;
  wire horiz_counter_n_59;
  wire horiz_counter_n_60;
  wire horiz_counter_n_61;
  wire horiz_counter_n_62;
  wire horiz_counter_n_63;
  wire horiz_counter_n_64;
  wire horiz_counter_n_65;
  wire horiz_counter_n_66;
  wire horiz_counter_n_67;
  wire horiz_counter_n_68;
  wire horiz_counter_n_69;
  wire horiz_counter_n_7;
  wire horiz_counter_n_70;
  wire horiz_counter_n_71;
  wire horiz_counter_n_72;
  wire horiz_counter_n_73;
  wire horiz_counter_n_74;
  wire horiz_counter_n_75;
  wire horiz_counter_n_76;
  wire horiz_counter_n_77;
  wire horiz_counter_n_78;
  wire horiz_counter_n_79;
  wire horiz_counter_n_88;
  wire horiz_counter_n_89;
  wire horiz_counter_n_9;
  wire horiz_counter_n_90;
  wire horiz_counter_n_91;
  wire horiz_counter_n_92;
  wire horiz_counter_n_93;
  wire horiz_counter_n_94;
  wire horiz_counter_n_95;
  wire horiz_counter_n_96;
  wire horiz_counter_n_97;
  wire \int_trigger_time_s_reg[0] ;
  wire \int_trigger_time_s_reg[0]_0 ;
  wire \int_trigger_time_s_reg[0]_1 ;
  wire \int_trigger_time_s_reg[0]_2 ;
  wire \int_trigger_time_s_reg[0]_3 ;
  wire \int_trigger_time_s_reg[0]_4 ;
  wire \int_trigger_time_s_reg[0]_5 ;
  wire \int_trigger_time_s_reg[0]_6 ;
  wire \int_trigger_time_s_reg[1] ;
  wire \int_trigger_time_s_reg[1]_0 ;
  wire \int_trigger_time_s_reg[1]_1 ;
  wire \int_trigger_volt_s_reg[2] ;
  wire \int_trigger_volt_s_reg[2]_0 ;
  wire \int_trigger_volt_s_reg[2]_1 ;
  wire \int_trigger_volt_s_reg[3] ;
  wire [5:0]\int_trigger_volt_s_reg[9] ;
  wire pixel_color3;
  wire pixel_color31_out;
  wire pixel_color49_in;
  wire pixel_color513_in;
  wire pixel_color517_in;
  wire pixel_color528_in;
  wire pixel_color58_in;
  wire pixel_color612_in;
  wire pixel_color616_in;
  wire pixel_color627_in;
  wire \processQ_reg[8] ;
  wire [9:0]\processQ_reg[9] ;
  wire \processQ_reg[9]_0 ;
  wire [0:0]\processQ_reg[9]_1 ;
  wire reset_n;
  wire sF_n_10;
  wire sF_n_12;
  wire sF_n_15;
  wire sF_n_16;
  wire sF_n_18;
  wire sF_n_19;
  wire sF_n_20;
  wire sF_n_21;
  wire sF_n_22;
  wire sF_n_25;
  wire sF_n_26;
  wire sF_n_27;
  wire sF_n_28;
  wire sF_n_31;
  wire sF_n_32;
  wire sF_n_33;
  wire sF_n_34;
  wire [1:0]\sdp_bl.ramb18_dp_bl.ram18_bl ;
  wire \slv_reg10_reg[2] ;
  wire \slv_reg10_reg[3] ;
  wire \slv_reg10_reg[5] ;
  wire \slv_reg10_reg[6] ;
  wire \slv_reg10_reg[6]_0 ;
  wire \slv_reg10_reg[6]_1 ;
  wire \slv_reg10_reg[7] ;
  wire [9:0]\slv_reg10_reg[9] ;
  wire \slv_reg11_reg[0] ;
  wire \slv_reg11_reg[0]_0 ;
  wire \slv_reg11_reg[0]_1 ;
  wire \slv_reg11_reg[0]_2 ;
  wire \slv_reg11_reg[1] ;
  wire \slv_reg11_reg[1]_0 ;
  wire \slv_reg11_reg[2] ;
  wire \slv_reg11_reg[3] ;
  wire \slv_reg11_reg[4] ;
  wire \slv_reg11_reg[5] ;
  wire \slv_reg11_reg[5]_0 ;
  wire \slv_reg11_reg[5]_1 ;
  wire \slv_reg11_reg[5]_2 ;
  wire \slv_reg11_reg[6] ;
  wire \slv_reg11_reg[6]_0 ;
  wire \slv_reg11_reg[6]_1 ;
  wire \slv_reg11_reg[6]_2 ;
  wire \slv_reg11_reg[6]_3 ;
  wire \slv_reg11_reg[6]_4 ;
  wire \slv_reg11_reg[7] ;
  wire \slv_reg11_reg[7]_0 ;
  wire \slv_reg11_reg[7]_1 ;
  wire \slv_reg11_reg[8] ;
  wire [5:0]\slv_reg11_reg[9] ;
  wire \slv_reg11_reg[9]_0 ;
  wire [0:0]\slv_reg5_reg[0] ;
  wire [1:0]switch;
  wire vert_counter_n_12;
  wire vert_counter_n_14;
  wire vert_counter_n_15;
  wire vert_counter_n_16;
  wire vert_counter_n_17;
  wire vert_counter_n_18;
  wire vert_counter_n_19;
  wire vert_counter_n_20;
  wire vert_counter_n_21;
  wire vert_counter_n_23;
  wire vert_counter_n_24;
  wire vert_counter_n_25;
  wire vert_counter_n_26;
  wire vert_counter_n_27;
  wire vert_counter_n_28;
  wire vert_counter_n_29;
  wire vert_counter_n_30;
  wire vert_counter_n_31;
  wire vert_counter_n_32;
  wire vert_counter_n_33;
  wire vert_counter_n_34;
  wire vert_counter_n_35;
  wire vert_counter_n_36;
  wire vert_counter_n_37;
  wire vert_counter_n_38;
  wire vert_counter_n_39;
  wire vert_counter_n_40;
  wire vert_counter_n_41;
  wire vert_counter_n_42;
  wire vert_counter_n_43;
  wire vert_counter_n_44;
  wire vert_counter_n_45;
  wire vert_counter_n_46;
  wire vert_counter_n_47;
  wire vert_counter_n_48;
  wire vert_counter_n_49;
  wire vert_counter_n_50;
  wire vert_counter_n_51;
  wire vert_counter_n_52;
  wire vert_counter_n_53;
  wire vert_counter_n_54;
  wire vert_counter_n_55;
  wire vert_counter_n_56;
  wire vert_counter_n_57;
  wire vert_counter_n_58;
  wire vert_counter_n_59;
  wire vert_counter_n_60;
  wire vert_counter_n_61;
  wire vert_counter_n_62;
  wire vert_counter_n_63;
  wire vert_counter_n_64;
  wire vert_counter_n_65;
  wire vert_counter_n_66;
  wire vert_counter_n_67;
  wire vert_counter_n_68;
  wire vert_counter_n_69;
  wire vert_counter_n_70;
  wire vert_counter_n_71;
  wire vert_counter_n_72;
  wire vert_counter_n_73;
  wire vert_counter_n_74;
  wire vert_counter_n_75;
  wire vert_counter_n_76;
  wire vert_counter_n_77;
  wire vert_counter_n_78;
  wire vert_counter_n_79;
  wire vert_counter_n_80;
  wire vert_counter_n_81;
  wire vert_counter_n_82;
  wire vert_counter_n_83;
  wire vert_counter_n_84;
  wire vert_counter_n_85;
  wire vert_counter_n_86;
  wire vert_counter_n_87;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_hcounter horiz_counter
       (.ADDRARDADDR(ADDRARDADDR),
        .CLK(CLK),
        .CO(CO),
        .DI({horiz_counter_n_0,horiz_counter_n_1,horiz_counter_n_2,horiz_counter_n_3}),
        .Q({Q[9],Q[3:2],Q[0]}),
        .S({horiz_counter_n_20,horiz_counter_n_21,horiz_counter_n_22,horiz_counter_n_23}),
        .\dc_bias_reg[1] (\dc_bias_reg[1] ),
        .\dc_bias_reg[2] (\dc_bias_reg[2] ),
        .\dc_bias_reg[3] (\dc_bias_reg[3] ),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_1 ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_2 ),
        .\encoded_reg[0] ({horiz_counter_n_40,horiz_counter_n_41,horiz_counter_n_42,horiz_counter_n_43}),
        .\encoded_reg[0]_0 ({horiz_counter_n_60,horiz_counter_n_61,horiz_counter_n_62,horiz_counter_n_63}),
        .\encoded_reg[0]_1 ({horiz_counter_n_64,horiz_counter_n_65,horiz_counter_n_66,horiz_counter_n_67}),
        .\encoded_reg[0]_10 (horiz_counter_n_89),
        .\encoded_reg[0]_11 (horiz_counter_n_90),
        .\encoded_reg[0]_2 (horiz_counter_n_68),
        .\encoded_reg[0]_3 (horiz_counter_n_69),
        .\encoded_reg[0]_4 ({horiz_counter_n_70,horiz_counter_n_71,horiz_counter_n_72,horiz_counter_n_73}),
        .\encoded_reg[0]_5 ({horiz_counter_n_74,horiz_counter_n_75,horiz_counter_n_76,horiz_counter_n_77}),
        .\encoded_reg[0]_6 (horiz_counter_n_78),
        .\encoded_reg[0]_7 (horiz_counter_n_79),
        .\encoded_reg[0]_8 (\encoded_reg[0]_0 ),
        .\encoded_reg[0]_9 (\encoded_reg[0]_1 ),
        .\encoded_reg[1] (\encoded_reg[1] ),
        .\encoded_reg[2] (\encoded_reg[2] ),
        .\encoded_reg[4] (\encoded_reg[4] ),
        .\encoded_reg[8] (\encoded_reg[8] ),
        .\encoded_reg[8]_0 (horiz_counter_n_7),
        .\encoded_reg[8]_1 (\encoded_reg[8]_0 ),
        .\encoded_reg[8]_10 (horiz_counter_n_39),
        .\encoded_reg[8]_11 ({horiz_counter_n_44,horiz_counter_n_45,horiz_counter_n_46,horiz_counter_n_47}),
        .\encoded_reg[8]_12 ({horiz_counter_n_48,horiz_counter_n_49,horiz_counter_n_50,horiz_counter_n_51}),
        .\encoded_reg[8]_13 (horiz_counter_n_52),
        .\encoded_reg[8]_14 (horiz_counter_n_53),
        .\encoded_reg[8]_15 ({horiz_counter_n_54,horiz_counter_n_55,horiz_counter_n_56,horiz_counter_n_57}),
        .\encoded_reg[8]_16 (horiz_counter_n_58),
        .\encoded_reg[8]_17 (horiz_counter_n_59),
        .\encoded_reg[8]_18 (\encoded_reg[8]_3 ),
        .\encoded_reg[8]_19 (\encoded_reg[8]_4 ),
        .\encoded_reg[8]_2 (\encoded_reg[8]_1 ),
        .\encoded_reg[8]_20 (horiz_counter_n_88),
        .\encoded_reg[8]_21 (horiz_counter_n_91),
        .\encoded_reg[8]_22 (horiz_counter_n_92),
        .\encoded_reg[8]_23 (horiz_counter_n_93),
        .\encoded_reg[8]_24 (horiz_counter_n_94),
        .\encoded_reg[8]_25 (horiz_counter_n_96),
        .\encoded_reg[8]_26 (horiz_counter_n_97),
        .\encoded_reg[8]_3 (\encoded_reg[8]_2 ),
        .\encoded_reg[8]_4 ({horiz_counter_n_24,horiz_counter_n_25,horiz_counter_n_26,horiz_counter_n_27}),
        .\encoded_reg[8]_5 (horiz_counter_n_28),
        .\encoded_reg[8]_6 (horiz_counter_n_29),
        .\encoded_reg[8]_7 ({horiz_counter_n_30,horiz_counter_n_31,horiz_counter_n_32,horiz_counter_n_33}),
        .\encoded_reg[8]_8 ({horiz_counter_n_34,horiz_counter_n_35,horiz_counter_n_36,horiz_counter_n_37}),
        .\encoded_reg[8]_9 (horiz_counter_n_38),
        .\encoded_reg[9] (horiz_counter_n_95),
        .\encoded_reg[9]_0 (\encoded_reg[9]_2 ),
        .\int_trigger_time_s_reg[1] (\int_trigger_time_s_reg[1] ),
        .\int_trigger_time_s_reg[1]_0 (\int_trigger_time_s_reg[1]_0 ),
        .\int_trigger_time_s_reg[1]_1 (sF_n_16),
        .\int_trigger_time_s_reg[1]_2 (sF_n_28),
        .\int_trigger_time_s_reg[1]_3 (\int_trigger_time_s_reg[1]_1 ),
        .\int_trigger_time_s_reg[2] (sF_n_27),
        .\int_trigger_time_s_reg[3] (sF_n_26),
        .\int_trigger_time_s_reg[5] (sF_n_21),
        .\int_trigger_time_s_reg[7] (sF_n_19),
        .\processQ_reg[0]_0 (horiz_counter_n_9),
        .\processQ_reg[0]_1 (vert_counter_n_84),
        .\processQ_reg[0]_2 (sF_n_34),
        .\processQ_reg[2]_0 (vert_counter_n_15),
        .\processQ_reg[2]_1 (vert_counter_n_14),
        .\processQ_reg[4]_0 (vert_counter_n_83),
        .\processQ_reg[4]_1 (vert_counter_n_16),
        .\processQ_reg[5]_0 (\encoded_reg[9] ),
        .\processQ_reg[8]_0 (\processQ_reg[8] ),
        .\processQ_reg[8]_1 (vert_counter_n_12),
        .\processQ_reg[8]_2 (vert_counter_n_85),
        .\processQ_reg[8]_3 (vert_counter_n_87),
        .\processQ_reg[9]_0 (\processQ_reg[9]_0 ),
        .\processQ_reg[9]_1 (vert_counter_n_86),
        .\processQ_reg[9]_10 (pixel_color517_in),
        .\processQ_reg[9]_2 (vert_counter_n_21),
        .\processQ_reg[9]_3 (pixel_color49_in),
        .\processQ_reg[9]_4 (pixel_color58_in),
        .\processQ_reg[9]_5 (\processQ_reg[9]_1 ),
        .\processQ_reg[9]_6 ({\processQ_reg[9] [9],\processQ_reg[9] [1:0]}),
        .\processQ_reg[9]_7 (pixel_color528_in),
        .\processQ_reg[9]_8 (pixel_color627_in),
        .\processQ_reg[9]_9 (pixel_color616_in),
        .reset_n(reset_n),
        .\sdp_bl.ramb18_dp_bl.ram18_bl (\sdp_bl.ramb18_dp_bl.ram18_bl ),
        .\slv_reg10_reg[0] (sF_n_12),
        .\slv_reg10_reg[0]_0 (\int_trigger_time_s_reg[0]_6 ),
        .\slv_reg10_reg[1] (\int_trigger_time_s_reg[0] ),
        .\slv_reg10_reg[2] (\int_trigger_time_s_reg[0]_5 ),
        .\slv_reg10_reg[2]_0 (\slv_reg10_reg[2] ),
        .\slv_reg10_reg[3] (\slv_reg10_reg[3] ),
        .\slv_reg10_reg[4] (\int_trigger_time_s_reg[0]_4 ),
        .\slv_reg10_reg[5] (sF_n_22),
        .\slv_reg10_reg[5]_0 (\slv_reg10_reg[5] ),
        .\slv_reg10_reg[5]_1 (sF_n_25),
        .\slv_reg10_reg[5]_2 (sF_n_20),
        .\slv_reg10_reg[6] (\int_trigger_time_s_reg[0]_1 ),
        .\slv_reg10_reg[6]_0 (sF_n_15),
        .\slv_reg10_reg[6]_1 (\slv_reg10_reg[6] ),
        .\slv_reg10_reg[6]_2 (sF_n_18),
        .\slv_reg10_reg[6]_3 (\slv_reg10_reg[6]_0 ),
        .\slv_reg10_reg[6]_4 (\slv_reg10_reg[6]_1 ),
        .\slv_reg10_reg[7] (\int_trigger_time_s_reg[0]_0 ),
        .\slv_reg10_reg[7]_0 (sF_n_10),
        .\slv_reg10_reg[7]_1 (\slv_reg10_reg[7] ),
        .\slv_reg10_reg[8] (\int_trigger_time_s_reg[0]_3 ),
        .\slv_reg10_reg[9] ({\slv_reg10_reg[9] [9],\slv_reg10_reg[9] [3:2],\slv_reg10_reg[9] [0]}),
        .\slv_reg10_reg[9]_0 (\int_trigger_time_s_reg[0]_2 ),
        .\slv_reg10_reg[9]_1 (pixel_color31_out),
        .\slv_reg5_reg[0] (\slv_reg5_reg[0] ),
        .switch(switch));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_scopeFace sF
       (.CO(pixel_color612_in),
        .DI({vert_counter_n_75,vert_counter_n_76,vert_counter_n_77,vert_counter_n_78}),
        .Q(Q),
        .S({vert_counter_n_79,vert_counter_n_80,vert_counter_n_81,vert_counter_n_82}),
        .\encoded_reg[0] (pixel_color627_in),
        .\encoded_reg[0]_0 (pixel_color528_in),
        .\encoded_reg[0]_1 (pixel_color31_out),
        .\encoded_reg[0]_2 (sF_n_27),
        .\encoded_reg[8] (pixel_color513_in),
        .\encoded_reg[8]_0 (pixel_color58_in),
        .\encoded_reg[8]_1 (pixel_color49_in),
        .\encoded_reg[8]_10 (sF_n_19),
        .\encoded_reg[8]_11 (sF_n_20),
        .\encoded_reg[8]_12 (sF_n_21),
        .\encoded_reg[8]_13 (sF_n_22),
        .\encoded_reg[8]_14 (sF_n_25),
        .\encoded_reg[8]_15 (sF_n_26),
        .\encoded_reg[8]_16 (sF_n_28),
        .\encoded_reg[8]_17 (sF_n_31),
        .\encoded_reg[8]_18 (sF_n_32),
        .\encoded_reg[8]_19 (sF_n_33),
        .\encoded_reg[8]_2 (pixel_color3),
        .\encoded_reg[8]_20 (sF_n_34),
        .\encoded_reg[8]_3 (pixel_color616_in),
        .\encoded_reg[8]_4 (pixel_color517_in),
        .\encoded_reg[8]_5 (sF_n_10),
        .\encoded_reg[8]_6 (sF_n_12),
        .\encoded_reg[8]_7 (sF_n_15),
        .\encoded_reg[8]_8 (sF_n_16),
        .\encoded_reg[8]_9 (sF_n_18),
        .\int_trigger_time_s_reg[0] (\int_trigger_time_s_reg[0]_0 ),
        .\int_trigger_time_s_reg[0]_0 (\int_trigger_time_s_reg[0]_4 ),
        .\int_trigger_time_s_reg[0]_1 (\int_trigger_time_s_reg[0]_1 ),
        .\int_trigger_time_s_reg[0]_2 (\int_trigger_time_s_reg[0]_5 ),
        .\int_trigger_time_s_reg[0]_3 (\int_trigger_time_s_reg[0]_6 ),
        .\int_trigger_time_s_reg[0]_4 (\int_trigger_time_s_reg[0] ),
        .\int_trigger_time_s_reg[0]_5 (\int_trigger_time_s_reg[0]_2 ),
        .\int_trigger_time_s_reg[0]_6 (\int_trigger_time_s_reg[0]_3 ),
        .\int_trigger_volt_s_reg[7] ({\int_trigger_volt_s_reg[9] [4:3],\int_trigger_volt_s_reg[9] [1:0]}),
        .\processQ_reg[1] (\processQ_reg[9] [1:0]),
        .\processQ_reg[7] ({vert_counter_n_65,vert_counter_n_66,vert_counter_n_67,vert_counter_n_68}),
        .\processQ_reg[7]_0 ({vert_counter_n_69,vert_counter_n_70,vert_counter_n_71,vert_counter_n_72}),
        .\processQ_reg[7]_1 ({vert_counter_n_55,vert_counter_n_56,vert_counter_n_57,vert_counter_n_58}),
        .\processQ_reg[7]_10 ({horiz_counter_n_70,horiz_counter_n_71,horiz_counter_n_72,horiz_counter_n_73}),
        .\processQ_reg[7]_11 ({horiz_counter_n_64,horiz_counter_n_65,horiz_counter_n_66,horiz_counter_n_67}),
        .\processQ_reg[7]_12 ({horiz_counter_n_60,horiz_counter_n_61,horiz_counter_n_62,horiz_counter_n_63}),
        .\processQ_reg[7]_13 ({horiz_counter_n_0,horiz_counter_n_1,horiz_counter_n_2,horiz_counter_n_3}),
        .\processQ_reg[7]_14 ({horiz_counter_n_54,horiz_counter_n_55,horiz_counter_n_56,horiz_counter_n_57}),
        .\processQ_reg[7]_15 ({horiz_counter_n_48,horiz_counter_n_49,horiz_counter_n_50,horiz_counter_n_51}),
        .\processQ_reg[7]_16 ({horiz_counter_n_44,horiz_counter_n_45,horiz_counter_n_46,horiz_counter_n_47}),
        .\processQ_reg[7]_17 ({horiz_counter_n_34,horiz_counter_n_35,horiz_counter_n_36,horiz_counter_n_37}),
        .\processQ_reg[7]_18 ({horiz_counter_n_30,horiz_counter_n_31,horiz_counter_n_32,horiz_counter_n_33}),
        .\processQ_reg[7]_19 ({horiz_counter_n_24,horiz_counter_n_25,horiz_counter_n_26,horiz_counter_n_27}),
        .\processQ_reg[7]_2 ({vert_counter_n_59,vert_counter_n_60,vert_counter_n_61,vert_counter_n_62}),
        .\processQ_reg[7]_20 ({horiz_counter_n_20,horiz_counter_n_21,horiz_counter_n_22,horiz_counter_n_23}),
        .\processQ_reg[7]_3 ({vert_counter_n_17,vert_counter_n_18,vert_counter_n_19,vert_counter_n_20}),
        .\processQ_reg[7]_4 ({vert_counter_n_49,vert_counter_n_50,vert_counter_n_51,vert_counter_n_52}),
        .\processQ_reg[7]_5 ({vert_counter_n_35,vert_counter_n_36,vert_counter_n_37,vert_counter_n_38}),
        .\processQ_reg[7]_6 ({vert_counter_n_39,vert_counter_n_40,vert_counter_n_41,vert_counter_n_42}),
        .\processQ_reg[7]_7 ({vert_counter_n_25,vert_counter_n_26,vert_counter_n_27,vert_counter_n_28}),
        .\processQ_reg[7]_8 ({vert_counter_n_29,vert_counter_n_30,vert_counter_n_31,vert_counter_n_32}),
        .\processQ_reg[7]_9 ({horiz_counter_n_74,horiz_counter_n_75,horiz_counter_n_76,horiz_counter_n_77}),
        .\processQ_reg[9] (vert_counter_n_74),
        .\processQ_reg[9]_0 (vert_counter_n_73),
        .\processQ_reg[9]_1 (vert_counter_n_64),
        .\processQ_reg[9]_10 (vert_counter_n_23),
        .\processQ_reg[9]_11 (horiz_counter_n_79),
        .\processQ_reg[9]_12 (horiz_counter_n_78),
        .\processQ_reg[9]_13 (horiz_counter_n_69),
        .\processQ_reg[9]_14 (horiz_counter_n_68),
        .\processQ_reg[9]_15 (horiz_counter_n_59),
        .\processQ_reg[9]_16 (horiz_counter_n_58),
        .\processQ_reg[9]_17 (horiz_counter_n_53),
        .\processQ_reg[9]_18 (horiz_counter_n_52),
        .\processQ_reg[9]_19 (horiz_counter_n_39),
        .\processQ_reg[9]_2 (vert_counter_n_63),
        .\processQ_reg[9]_20 (horiz_counter_n_38),
        .\processQ_reg[9]_21 (horiz_counter_n_29),
        .\processQ_reg[9]_22 (horiz_counter_n_28),
        .\processQ_reg[9]_3 (vert_counter_n_54),
        .\processQ_reg[9]_4 (vert_counter_n_53),
        .\processQ_reg[9]_5 (vert_counter_n_48),
        .\processQ_reg[9]_6 (vert_counter_n_47),
        .\processQ_reg[9]_7 (vert_counter_n_34),
        .\processQ_reg[9]_8 (vert_counter_n_33),
        .\processQ_reg[9]_9 (vert_counter_n_24),
        .\slv_reg10_reg[3] (\slv_reg10_reg[3] ),
        .\slv_reg10_reg[5] (\slv_reg10_reg[5] ),
        .\slv_reg10_reg[9] ({horiz_counter_n_40,horiz_counter_n_41,horiz_counter_n_42,horiz_counter_n_43}),
        .\slv_reg10_reg[9]_0 (\slv_reg10_reg[9] ),
        .\slv_reg11_reg[0] (\slv_reg11_reg[0]_1 ),
        .\slv_reg11_reg[1] (\slv_reg11_reg[1] ),
        .\slv_reg11_reg[2] (\slv_reg11_reg[2] ),
        .\slv_reg11_reg[3] (\slv_reg11_reg[3] ),
        .\slv_reg11_reg[4] (\slv_reg11_reg[4] ),
        .\slv_reg11_reg[5] (\slv_reg11_reg[5]_2 ),
        .\slv_reg11_reg[7] ({\slv_reg11_reg[9] [4:3],\slv_reg11_reg[9] [1:0]}),
        .\slv_reg11_reg[9] ({vert_counter_n_43,vert_counter_n_44,vert_counter_n_45,vert_counter_n_46}),
        .\slv_reg5_reg[0] (\slv_reg5_reg[0] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vcounter vert_counter
       (.CLK(CLK),
        .CO(pixel_color612_in),
        .DI({vert_counter_n_75,vert_counter_n_76,vert_counter_n_77,vert_counter_n_78}),
        .DOADO(DOADO),
        .Q(\processQ_reg[9] ),
        .S(S),
        .SR(SR),
        .\dc_bias_reg[1] (\dc_bias_reg[1] ),
        .\dc_bias_reg[3] (\dc_bias_reg[3]_0 ),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_1 ),
        .\encoded_reg[0] (\encoded_reg[0] ),
        .\encoded_reg[0]_0 (vert_counter_n_16),
        .\encoded_reg[8] (vert_counter_n_12),
        .\encoded_reg[8]_0 (vert_counter_n_14),
        .\encoded_reg[8]_1 (vert_counter_n_15),
        .\encoded_reg[8]_10 ({vert_counter_n_35,vert_counter_n_36,vert_counter_n_37,vert_counter_n_38}),
        .\encoded_reg[8]_11 ({vert_counter_n_39,vert_counter_n_40,vert_counter_n_41,vert_counter_n_42}),
        .\encoded_reg[8]_12 ({vert_counter_n_43,vert_counter_n_44,vert_counter_n_45,vert_counter_n_46}),
        .\encoded_reg[8]_13 (vert_counter_n_47),
        .\encoded_reg[8]_14 (vert_counter_n_48),
        .\encoded_reg[8]_15 ({vert_counter_n_49,vert_counter_n_50,vert_counter_n_51,vert_counter_n_52}),
        .\encoded_reg[8]_16 (vert_counter_n_53),
        .\encoded_reg[8]_17 (vert_counter_n_54),
        .\encoded_reg[8]_18 ({vert_counter_n_55,vert_counter_n_56,vert_counter_n_57,vert_counter_n_58}),
        .\encoded_reg[8]_19 ({vert_counter_n_59,vert_counter_n_60,vert_counter_n_61,vert_counter_n_62}),
        .\encoded_reg[8]_2 ({vert_counter_n_17,vert_counter_n_18,vert_counter_n_19,vert_counter_n_20}),
        .\encoded_reg[8]_20 (vert_counter_n_63),
        .\encoded_reg[8]_21 (vert_counter_n_64),
        .\encoded_reg[8]_22 ({vert_counter_n_65,vert_counter_n_66,vert_counter_n_67,vert_counter_n_68}),
        .\encoded_reg[8]_23 ({vert_counter_n_69,vert_counter_n_70,vert_counter_n_71,vert_counter_n_72}),
        .\encoded_reg[8]_24 (vert_counter_n_73),
        .\encoded_reg[8]_25 (vert_counter_n_74),
        .\encoded_reg[8]_26 ({vert_counter_n_79,vert_counter_n_80,vert_counter_n_81,vert_counter_n_82}),
        .\encoded_reg[8]_27 (vert_counter_n_83),
        .\encoded_reg[8]_28 (vert_counter_n_84),
        .\encoded_reg[8]_29 (vert_counter_n_85),
        .\encoded_reg[8]_3 (vert_counter_n_21),
        .\encoded_reg[8]_30 (vert_counter_n_86),
        .\encoded_reg[8]_31 (vert_counter_n_87),
        .\encoded_reg[8]_4 (vert_counter_n_23),
        .\encoded_reg[8]_5 (vert_counter_n_24),
        .\encoded_reg[8]_6 ({vert_counter_n_25,vert_counter_n_26,vert_counter_n_27,vert_counter_n_28}),
        .\encoded_reg[8]_7 ({vert_counter_n_29,vert_counter_n_30,vert_counter_n_31,vert_counter_n_32}),
        .\encoded_reg[8]_8 (vert_counter_n_33),
        .\encoded_reg[8]_9 (vert_counter_n_34),
        .\encoded_reg[9] (\encoded_reg[9] ),
        .\encoded_reg[9]_0 (\encoded_reg[9]_0 ),
        .\encoded_reg[9]_1 (\encoded_reg[9]_1 ),
        .\int_trigger_volt_s_reg[2] (\int_trigger_volt_s_reg[2] ),
        .\int_trigger_volt_s_reg[2]_0 (\int_trigger_volt_s_reg[2]_0 ),
        .\int_trigger_volt_s_reg[2]_1 (sF_n_33),
        .\int_trigger_volt_s_reg[2]_2 (\int_trigger_volt_s_reg[2]_1 ),
        .\int_trigger_volt_s_reg[3] (\int_trigger_volt_s_reg[3] ),
        .\int_trigger_volt_s_reg[7] (sF_n_31),
        .\int_trigger_volt_s_reg[9] ({\int_trigger_volt_s_reg[9] [5],\int_trigger_volt_s_reg[9] [2:0]}),
        .\processQ_reg[0]_0 (horiz_counter_n_89),
        .\processQ_reg[0]_1 (horiz_counter_n_97),
        .\processQ_reg[0]_2 (sF_n_34),
        .\processQ_reg[1]_0 (horiz_counter_n_90),
        .\processQ_reg[1]_1 (\sdp_bl.ramb18_dp_bl.ram18_bl ),
        .\processQ_reg[2]_0 (horiz_counter_n_92),
        .\processQ_reg[4]_0 (horiz_counter_n_94),
        .\processQ_reg[4]_1 (horiz_counter_n_93),
        .\processQ_reg[5]_0 (horiz_counter_n_88),
        .\processQ_reg[6]_0 (horiz_counter_n_7),
        .\processQ_reg[7]_0 (horiz_counter_n_96),
        .\processQ_reg[8]_0 (horiz_counter_n_9),
        .\processQ_reg[8]_1 (horiz_counter_n_95),
        .\processQ_reg[8]_2 (\encoded_reg[8]_0 ),
        .\processQ_reg[9]_0 (horiz_counter_n_91),
        .\processQ_reg[9]_1 (pixel_color616_in),
        .\processQ_reg[9]_2 (pixel_color517_in),
        .\processQ_reg[9]_3 (pixel_color513_in),
        .\processQ_reg[9]_4 (\encoded_reg[8]_2 ),
        .reset_n(reset_n),
        .\slv_reg11_reg[0] (\slv_reg11_reg[0] ),
        .\slv_reg11_reg[0]_0 (\slv_reg11_reg[0]_0 ),
        .\slv_reg11_reg[0]_1 (\slv_reg11_reg[0]_2 ),
        .\slv_reg11_reg[0]_2 (\slv_reg11_reg[0]_1 ),
        .\slv_reg11_reg[1] (\slv_reg11_reg[1] ),
        .\slv_reg11_reg[1]_0 (\slv_reg11_reg[1]_0 ),
        .\slv_reg11_reg[2] (\slv_reg11_reg[2] ),
        .\slv_reg11_reg[3] (\slv_reg11_reg[3] ),
        .\slv_reg11_reg[4] (\slv_reg11_reg[4] ),
        .\slv_reg11_reg[5] (\slv_reg11_reg[5] ),
        .\slv_reg11_reg[5]_0 (\slv_reg11_reg[5]_0 ),
        .\slv_reg11_reg[5]_1 (sF_n_32),
        .\slv_reg11_reg[5]_2 (\slv_reg11_reg[5]_1 ),
        .\slv_reg11_reg[5]_3 (\slv_reg11_reg[5]_2 ),
        .\slv_reg11_reg[6] (\slv_reg11_reg[6] ),
        .\slv_reg11_reg[6]_0 (\slv_reg11_reg[6]_0 ),
        .\slv_reg11_reg[6]_1 (\slv_reg11_reg[6]_1 ),
        .\slv_reg11_reg[6]_2 (\slv_reg11_reg[6]_2 ),
        .\slv_reg11_reg[6]_3 (\slv_reg11_reg[6]_3 ),
        .\slv_reg11_reg[6]_4 (\slv_reg11_reg[6]_4 ),
        .\slv_reg11_reg[7] (\slv_reg11_reg[7] ),
        .\slv_reg11_reg[7]_0 (\slv_reg11_reg[7]_0 ),
        .\slv_reg11_reg[7]_1 (\slv_reg11_reg[7]_1 ),
        .\slv_reg11_reg[8] (\slv_reg11_reg[8] ),
        .\slv_reg11_reg[9] ({\slv_reg11_reg[9] [5],\slv_reg11_reg[9] [2:0]}),
        .\slv_reg11_reg[9]_0 (\slv_reg11_reg[9]_0 ),
        .\slv_reg11_reg[9]_1 (pixel_color3),
        .\slv_reg5_reg[0] (\slv_reg5_reg[0] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video
   (tmds,
    tmdsb,
    \int_trigger_time_s_reg[0] ,
    ADDRARDADDR,
    \processQ_reg[9] ,
    \int_trigger_time_s_reg[0]_0 ,
    \int_trigger_time_s_reg[0]_1 ,
    \int_trigger_time_s_reg[0]_2 ,
    \int_trigger_time_s_reg[0]_3 ,
    \int_trigger_time_s_reg[0]_4 ,
    \int_trigger_time_s_reg[0]_5 ,
    \int_trigger_time_s_reg[0]_6 ,
    \int_trigger_time_s_reg[0]_7 ,
    \int_trigger_time_s_reg[0]_8 ,
    \encoded_reg[8] ,
    \state_reg[0] ,
    S,
    \state_reg[0]_0 ,
    \encoded_reg[8]_0 ,
    \state_reg[0]_1 ,
    \state_reg[0]_2 ,
    \state_reg[0]_3 ,
    \state_reg[0]_4 ,
    reset_n,
    clk,
    Q,
    \slv_reg5_reg[0] ,
    \slv_reg10_reg[9] ,
    CO,
    switch,
    \processQ_reg[9]_0 ,
    \processQ_reg[8] ,
    \int_trigger_volt_s_reg[9] ,
    \slv_reg11_reg[9] ,
    DOADO,
    \processQ_reg[9]_1 ,
    lopt);
  output [3:0]tmds;
  output [3:0]tmdsb;
  output \int_trigger_time_s_reg[0] ;
  output [9:0]ADDRARDADDR;
  output [9:0]\processQ_reg[9] ;
  output \int_trigger_time_s_reg[0]_0 ;
  output \int_trigger_time_s_reg[0]_1 ;
  output \int_trigger_time_s_reg[0]_2 ;
  output \int_trigger_time_s_reg[0]_3 ;
  output \int_trigger_time_s_reg[0]_4 ;
  output \int_trigger_time_s_reg[0]_5 ;
  output \int_trigger_time_s_reg[0]_6 ;
  output \int_trigger_time_s_reg[0]_7 ;
  output \int_trigger_time_s_reg[0]_8 ;
  output \encoded_reg[8] ;
  output \state_reg[0] ;
  output [0:0]S;
  output \state_reg[0]_0 ;
  output \encoded_reg[8]_0 ;
  output \state_reg[0]_1 ;
  output \state_reg[0]_2 ;
  output \state_reg[0]_3 ;
  output \state_reg[0]_4 ;
  input reset_n;
  input clk;
  input [9:0]Q;
  input [0:0]\slv_reg5_reg[0] ;
  input [9:0]\slv_reg10_reg[9] ;
  input [0:0]CO;
  input [1:0]switch;
  input \processQ_reg[9]_0 ;
  input \processQ_reg[8] ;
  input [9:0]\int_trigger_volt_s_reg[9] ;
  input [9:0]\slv_reg11_reg[9] ;
  input [2:0]DOADO;
  input [0:0]\processQ_reg[9]_1 ;
  input lopt;

  wire [9:0]ADDRARDADDR;
  wire [0:0]CO;
  wire [2:0]DOADO;
  wire Inst_vga_n_25;
  wire Inst_vga_n_26;
  wire Inst_vga_n_3;
  wire Inst_vga_n_35;
  wire Inst_vga_n_36;
  wire Inst_vga_n_37;
  wire Inst_vga_n_38;
  wire Inst_vga_n_39;
  wire Inst_vga_n_4;
  wire Inst_vga_n_40;
  wire Inst_vga_n_41;
  wire Inst_vga_n_44;
  wire Inst_vga_n_45;
  wire Inst_vga_n_46;
  wire Inst_vga_n_5;
  wire Inst_vga_n_6;
  wire [9:0]Q;
  wire [0:0]S;
  wire \TDMS_encoder_blue/p_1_in ;
  wire \TDMS_encoder_green/p_1_in ;
  wire \TDMS_encoder_red/p_1_in ;
  wire blank;
  wire blue_s;
  wire clk;
  wire clock_s;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire green_s;
  wire inst_dvid_n_10;
  wire inst_dvid_n_11;
  wire inst_dvid_n_12;
  wire inst_dvid_n_13;
  wire inst_dvid_n_14;
  wire inst_dvid_n_15;
  wire inst_dvid_n_17;
  wire inst_dvid_n_21;
  wire inst_dvid_n_24;
  wire inst_dvid_n_25;
  wire inst_dvid_n_26;
  wire inst_dvid_n_28;
  wire inst_dvid_n_29;
  wire inst_dvid_n_30;
  wire inst_dvid_n_31;
  wire inst_dvid_n_32;
  wire inst_dvid_n_33;
  wire inst_dvid_n_34;
  wire inst_dvid_n_35;
  wire inst_dvid_n_36;
  wire inst_dvid_n_37;
  wire inst_dvid_n_38;
  wire inst_dvid_n_39;
  wire inst_dvid_n_40;
  wire inst_dvid_n_41;
  wire inst_dvid_n_42;
  wire inst_dvid_n_43;
  wire inst_dvid_n_45;
  wire inst_dvid_n_46;
  wire inst_dvid_n_6;
  wire inst_dvid_n_9;
  wire \int_trigger_time_s_reg[0] ;
  wire \int_trigger_time_s_reg[0]_0 ;
  wire \int_trigger_time_s_reg[0]_1 ;
  wire \int_trigger_time_s_reg[0]_2 ;
  wire \int_trigger_time_s_reg[0]_3 ;
  wire \int_trigger_time_s_reg[0]_4 ;
  wire \int_trigger_time_s_reg[0]_5 ;
  wire \int_trigger_time_s_reg[0]_6 ;
  wire \int_trigger_time_s_reg[0]_7 ;
  wire \int_trigger_time_s_reg[0]_8 ;
  wire [9:0]\int_trigger_volt_s_reg[9] ;
  wire lopt;
  wire pixel_clk;
  wire \processQ_reg[8] ;
  wire [9:0]\processQ_reg[9] ;
  wire \processQ_reg[9]_0 ;
  wire [0:0]\processQ_reg[9]_1 ;
  wire red_s;
  wire reset_n;
  wire serialize_clk;
  wire serialize_clk_n;
  wire [9:0]\slv_reg10_reg[9] ;
  wire [9:0]\slv_reg11_reg[9] ;
  wire [0:0]\slv_reg5_reg[0] ;
  wire \state_reg[0] ;
  wire \state_reg[0]_0 ;
  wire \state_reg[0]_1 ;
  wire \state_reg[0]_2 ;
  wire \state_reg[0]_3 ;
  wire \state_reg[0]_4 ;
  wire [1:0]switch;
  wire [3:0]tmds;
  wire [3:0]tmdsb;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga Inst_vga
       (.ADDRARDADDR(ADDRARDADDR[9:2]),
        .CLK(pixel_clk),
        .CO(CO),
        .DOADO(DOADO),
        .Q(Q),
        .S(S),
        .SR(blank),
        .\dc_bias_reg[1] (inst_dvid_n_46),
        .\dc_bias_reg[2] (inst_dvid_n_45),
        .\dc_bias_reg[3] (Inst_vga_n_35),
        .\dc_bias_reg[3]_0 (\TDMS_encoder_green/p_1_in ),
        .\dc_bias_reg[3]_1 (\TDMS_encoder_blue/p_1_in ),
        .\dc_bias_reg[3]_2 (\TDMS_encoder_red/p_1_in ),
        .\encoded_reg[0] (Inst_vga_n_6),
        .\encoded_reg[0]_0 (Inst_vga_n_40),
        .\encoded_reg[0]_1 (Inst_vga_n_41),
        .\encoded_reg[1] (Inst_vga_n_38),
        .\encoded_reg[2] (Inst_vga_n_37),
        .\encoded_reg[4] (Inst_vga_n_39),
        .\encoded_reg[8] (Inst_vga_n_3),
        .\encoded_reg[8]_0 (Inst_vga_n_5),
        .\encoded_reg[8]_1 (Inst_vga_n_25),
        .\encoded_reg[8]_2 (Inst_vga_n_26),
        .\encoded_reg[8]_3 (\encoded_reg[8] ),
        .\encoded_reg[8]_4 (Inst_vga_n_36),
        .\encoded_reg[9] (Inst_vga_n_4),
        .\encoded_reg[9]_0 (Inst_vga_n_44),
        .\encoded_reg[9]_1 (Inst_vga_n_45),
        .\encoded_reg[9]_2 (Inst_vga_n_46),
        .\int_trigger_time_s_reg[0] (\int_trigger_time_s_reg[0] ),
        .\int_trigger_time_s_reg[0]_0 (\int_trigger_time_s_reg[0]_0 ),
        .\int_trigger_time_s_reg[0]_1 (\int_trigger_time_s_reg[0]_1 ),
        .\int_trigger_time_s_reg[0]_2 (\int_trigger_time_s_reg[0]_2 ),
        .\int_trigger_time_s_reg[0]_3 (\int_trigger_time_s_reg[0]_3 ),
        .\int_trigger_time_s_reg[0]_4 (\int_trigger_time_s_reg[0]_5 ),
        .\int_trigger_time_s_reg[0]_5 (\int_trigger_time_s_reg[0]_6 ),
        .\int_trigger_time_s_reg[0]_6 (\int_trigger_time_s_reg[0]_8 ),
        .\int_trigger_time_s_reg[1] (inst_dvid_n_12),
        .\int_trigger_time_s_reg[1]_0 (inst_dvid_n_13),
        .\int_trigger_time_s_reg[1]_1 (inst_dvid_n_15),
        .\int_trigger_volt_s_reg[2] (inst_dvid_n_37),
        .\int_trigger_volt_s_reg[2]_0 (inst_dvid_n_38),
        .\int_trigger_volt_s_reg[2]_1 (inst_dvid_n_42),
        .\int_trigger_volt_s_reg[3] (inst_dvid_n_29),
        .\int_trigger_volt_s_reg[9] ({\int_trigger_volt_s_reg[9] [9],\int_trigger_volt_s_reg[9] [7:6],\int_trigger_volt_s_reg[9] [3:2],\int_trigger_volt_s_reg[9] [0]}),
        .\processQ_reg[8] (\processQ_reg[8] ),
        .\processQ_reg[9] (\processQ_reg[9] ),
        .\processQ_reg[9]_0 (\processQ_reg[9]_0 ),
        .\processQ_reg[9]_1 (\processQ_reg[9]_1 ),
        .reset_n(reset_n),
        .\sdp_bl.ramb18_dp_bl.ram18_bl (ADDRARDADDR[1:0]),
        .\slv_reg10_reg[2] (inst_dvid_n_14),
        .\slv_reg10_reg[3] (\int_trigger_time_s_reg[0]_7 ),
        .\slv_reg10_reg[5] (\int_trigger_time_s_reg[0]_4 ),
        .\slv_reg10_reg[6] (inst_dvid_n_9),
        .\slv_reg10_reg[6]_0 (inst_dvid_n_10),
        .\slv_reg10_reg[6]_1 (inst_dvid_n_11),
        .\slv_reg10_reg[7] (inst_dvid_n_6),
        .\slv_reg10_reg[9] (\slv_reg10_reg[9] ),
        .\slv_reg11_reg[0] (inst_dvid_n_21),
        .\slv_reg11_reg[0]_0 (inst_dvid_n_39),
        .\slv_reg11_reg[0]_1 (inst_dvid_n_28),
        .\slv_reg11_reg[0]_2 (inst_dvid_n_40),
        .\slv_reg11_reg[1] (\state_reg[0] ),
        .\slv_reg11_reg[1]_0 (inst_dvid_n_41),
        .\slv_reg11_reg[2] (inst_dvid_n_25),
        .\slv_reg11_reg[3] (\state_reg[0]_2 ),
        .\slv_reg11_reg[4] (\state_reg[0]_3 ),
        .\slv_reg11_reg[5] (inst_dvid_n_34),
        .\slv_reg11_reg[5]_0 (inst_dvid_n_35),
        .\slv_reg11_reg[5]_1 (inst_dvid_n_36),
        .\slv_reg11_reg[5]_2 (\state_reg[0]_1 ),
        .\slv_reg11_reg[6] (\state_reg[0]_4 ),
        .\slv_reg11_reg[6]_0 (inst_dvid_n_26),
        .\slv_reg11_reg[6]_1 (inst_dvid_n_30),
        .\slv_reg11_reg[6]_2 (inst_dvid_n_31),
        .\slv_reg11_reg[6]_3 (inst_dvid_n_32),
        .\slv_reg11_reg[6]_4 (inst_dvid_n_33),
        .\slv_reg11_reg[7] (inst_dvid_n_17),
        .\slv_reg11_reg[7]_0 (\encoded_reg[8]_0 ),
        .\slv_reg11_reg[7]_1 (inst_dvid_n_24),
        .\slv_reg11_reg[8] (\state_reg[0]_0 ),
        .\slv_reg11_reg[9] ({\slv_reg11_reg[9] [9],\slv_reg11_reg[9] [7:6],\slv_reg11_reg[9] [3:2],\slv_reg11_reg[9] [0]}),
        .\slv_reg11_reg[9]_0 (inst_dvid_n_43),
        .\slv_reg5_reg[0] (\slv_reg5_reg[0] ),
        .switch(switch));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_blue
       (.I(blue_s),
        .O(tmds[0]),
        .OB(tmdsb[0]));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_clock
       (.I(clock_s),
        .O(tmds[3]),
        .OB(tmdsb[3]));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_green
       (.I(red_s),
        .O(tmds[2]),
        .OB(tmdsb[2]));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_red
       (.I(green_s),
        .O(tmds[1]),
        .OB(tmdsb[1]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid inst_dvid
       (.CLK(pixel_clk),
        .Q(\TDMS_encoder_green/p_1_in ),
        .SR(blank),
        .blue_s(blue_s),
        .clk_out2(serialize_clk),
        .clk_out3(serialize_clk_n),
        .clock_s(clock_s),
        .\dc_bias_reg[0] (\TDMS_encoder_blue/p_1_in ),
        .\dc_bias_reg[3] (Inst_vga_n_46),
        .\dc_bias_reg[3]_0 (Inst_vga_n_6),
        .\dc_bias_reg[3]_1 (Inst_vga_n_45),
        .\dc_bias_reg[3]_2 (Inst_vga_n_39),
        .\dc_bias_reg[3]_3 (Inst_vga_n_37),
        .\dc_bias_reg[3]_4 (Inst_vga_n_38),
        .\dc_bias_reg[3]_5 (Inst_vga_n_41),
        .\dc_bias_reg[3]_6 (Inst_vga_n_44),
        .\encoded_reg[0] (inst_dvid_n_6),
        .\encoded_reg[0]_0 (inst_dvid_n_9),
        .\encoded_reg[0]_1 (inst_dvid_n_10),
        .\encoded_reg[0]_2 (inst_dvid_n_11),
        .\encoded_reg[0]_3 (inst_dvid_n_12),
        .\encoded_reg[0]_4 (inst_dvid_n_13),
        .\encoded_reg[0]_5 (inst_dvid_n_14),
        .\encoded_reg[0]_6 (inst_dvid_n_15),
        .\encoded_reg[3] (\TDMS_encoder_red/p_1_in ),
        .\encoded_reg[8] (inst_dvid_n_17),
        .\encoded_reg[8]_0 (\encoded_reg[8]_0 ),
        .\encoded_reg[8]_1 (inst_dvid_n_21),
        .\encoded_reg[8]_10 (inst_dvid_n_33),
        .\encoded_reg[8]_11 (inst_dvid_n_34),
        .\encoded_reg[8]_12 (inst_dvid_n_35),
        .\encoded_reg[8]_13 (inst_dvid_n_36),
        .\encoded_reg[8]_14 (inst_dvid_n_37),
        .\encoded_reg[8]_15 (inst_dvid_n_38),
        .\encoded_reg[8]_16 (inst_dvid_n_39),
        .\encoded_reg[8]_17 (inst_dvid_n_40),
        .\encoded_reg[8]_18 (inst_dvid_n_41),
        .\encoded_reg[8]_19 (inst_dvid_n_42),
        .\encoded_reg[8]_2 (inst_dvid_n_24),
        .\encoded_reg[8]_20 (inst_dvid_n_43),
        .\encoded_reg[8]_21 (inst_dvid_n_45),
        .\encoded_reg[8]_3 (inst_dvid_n_25),
        .\encoded_reg[8]_4 (inst_dvid_n_26),
        .\encoded_reg[8]_5 (inst_dvid_n_28),
        .\encoded_reg[8]_6 (inst_dvid_n_29),
        .\encoded_reg[8]_7 (inst_dvid_n_30),
        .\encoded_reg[8]_8 (inst_dvid_n_31),
        .\encoded_reg[8]_9 (inst_dvid_n_32),
        .\encoded_reg[9] (inst_dvid_n_46),
        .green_s(green_s),
        .\int_trigger_time_s_reg[0] (\int_trigger_time_s_reg[0]_4 ),
        .\int_trigger_time_s_reg[0]_0 (\int_trigger_time_s_reg[0]_7 ),
        .\int_trigger_time_s_reg[5] ({Q[5],Q[3:1]}),
        .\int_trigger_volt_s_reg[9] (\int_trigger_volt_s_reg[9] ),
        .\processQ_reg[5] (Inst_vga_n_25),
        .\processQ_reg[5]_0 (Inst_vga_n_4),
        .\processQ_reg[5]_1 (Inst_vga_n_40),
        .\processQ_reg[6] (Inst_vga_n_3),
        .\processQ_reg[8] (Inst_vga_n_35),
        .\processQ_reg[8]_0 (Inst_vga_n_5),
        .\processQ_reg[9] (Inst_vga_n_26),
        .\processQ_reg[9]_0 (Inst_vga_n_36),
        .red_s(red_s),
        .\slv_reg10_reg[1] (\int_trigger_time_s_reg[0] ),
        .\slv_reg10_reg[2] (\int_trigger_time_s_reg[0]_6 ),
        .\slv_reg10_reg[4] (\int_trigger_time_s_reg[0]_5 ),
        .\slv_reg10_reg[5] ({\slv_reg10_reg[9] [5],\slv_reg10_reg[9] [3:1]}),
        .\slv_reg10_reg[6] (\int_trigger_time_s_reg[0]_1 ),
        .\slv_reg10_reg[7] (\int_trigger_time_s_reg[0]_0 ),
        .\slv_reg11_reg[9] (\slv_reg11_reg[9] ),
        .\slv_reg5_reg[0] (\slv_reg5_reg[0] ),
        .\state_reg[0] (\state_reg[0]_1 ),
        .\state_reg[0]_0 (\state_reg[0]_2 ),
        .\state_reg[0]_1 (\state_reg[0]_3 ),
        .\state_reg[0]_2 (\state_reg[0]_4 ),
        .\state_reg[0]_3 (\state_reg[0] ),
        .\state_reg[0]_4 (\state_reg[0]_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 mmcm_adv_inst_display_clocks
       (.clk_in1(clk),
        .clk_out1(pixel_clk),
        .clk_out2(serialize_clk),
        .clk_out3(serialize_clk_n),
        .lopt(lopt),
        .resetn(reset_n));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
