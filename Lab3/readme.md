# Lab 3 - Microblaze Controlled Oscilloscope

## Author: C2C Jeremy Schwartz

### Last Modified: 15 Mar 2018

**Documentation:** None

#### Gate Check 1

I completed Gate Check 1 at 0800 on 5 Mar 2018.  See the pictures below for proof of functionality.

![1](images/1.PNG)
##### Figure 1: Hello World Printout From SDK

![2](images/2.png)
##### Figure 2: O-Scope Screengrab

#### Gate Check 2

Gate Check 2 was demoed to Dr. York at 1000 on 6 Mar 2018.

#### Required Functionality

Required Functionality was demoed to Dr. York at 1100 on 8 Mar 2018.

#### A Functionality

A Functionality was demoed to Dr. York at 1100 on 8 Mar 2018.

#### Continuous Capture Functionality

Continuous Capture Functionality was demoed to Dr. York at 1100 on 8 Mar 2018.