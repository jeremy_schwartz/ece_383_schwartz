# Homework #10

## Author: C2C Jeremy Schwartz
### Last Modified: 26 Feb 2018

Homework 10 was completed at 1100 on 26 Feb 2018.  See the pictures below for proof of functionality.  See the project folder in the HW10 code folder for lec18 code files.

![No Roll](images/noroll.PNG)
##### Figure 1: Roll = 0 When Q < 0xFF

![Roll](images/roll.PNG)
##### Figure 2: Roll = 1 When Q = 0xFF