--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 12 Feb 2018
-- Course: ECE 383
-- File: lab2_datapath_tb.vhdl
-- Project: Lab2
--
-- Purp: This file contains the tesbench for the Lab 2 datapath.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

use work.lab2Parts.all;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.vcomponents.all;
 
ENTITY Lab2_datapath_tb IS
END Lab2_datapath_tb;
 
ARCHITECTURE behavior OF Lab2_datapath_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    component lab2_datapath is
        Port(
        clk : in  STD_LOGIC;
        reset_n : in  STD_LOGIC;
        ac_mclk : out STD_LOGIC;
        ac_adc_sdata : in STD_LOGIC;
        ac_dac_sdata : out STD_LOGIC;
        ac_bclk : out STD_LOGIC;
        ac_lrclk : out STD_LOGIC;
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC;    
        tmds : out  STD_LOGIC_VECTOR (3 downto 0);
        tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
        sw: out std_logic_vector(2 downto 0);
        switch: in std_logic_vector(3 downto 0);
        cw: in std_logic_vector (2 downto 0);
        btn: in    STD_LOGIC_VECTOR(4 downto 0);
        exWrAddr: in std_logic_vector(9 downto 0);
        exWen, exSel: in std_logic;
        Lbus_out, Rbus_out: out std_logic_vector(15 downto 0);
        exLbus, exRbus: in std_logic_vector(15 downto 0);
        flagQ: out std_logic_vector(7 downto 0);
        flagClear: in std_logic_vector(7 downto 0));
    end component;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset_n : std_logic := '0';
   signal ac_adc_sdata : std_logic := '0';
   signal sda : std_logic := '0';
   signal BIT_CLK : std_logic := '0';
   signal cw : std_logic_vector(2 downto 0) := (others => '0');
   signal btn : std_logic_vector(4 downto 0) := (others => '0');
   signal exWrAddr : std_logic_vector(9 downto 0) := (others => '0');
   signal exWen : std_logic := '0';
   signal exSel : std_logic := '0';
   signal exLbus : std_logic_vector(15 downto 0) := (others => '0');
   signal exRbus : std_logic_vector(15 downto 0) := (others => '0');
   signal flagClear : std_logic_vector(7 downto 0) := (others => '0');
   signal switch : std_logic_vector(3 downto 0) := "1111";

 	--Outputs
   signal ac_mclk : std_logic;
   signal ac_dac_sdata : std_logic;
   signal ac_bclk : std_logic;
   signal ac_lrclk : std_logic;
   signal scl : std_logic;
--   signal AC97_n_RESET : std_logic;
   signal tmds : std_logic_vector(3 downto 0);
   signal tmdsb : std_logic_vector(3 downto 0);
   signal sw : std_logic_vector(2 downto 0);
   signal Lbus_out : std_logic_vector(15 downto 0);
   signal Rbus_out : std_logic_vector(15 downto 0);
   signal flagQ : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 1 ns;  -- Sets clock to ~ 100MHz
--   constant BIT_CLK_period : time := 80 ns;  -- Sets Bit Clock for AC'97 to the necessary 12.288 MHz
   constant BIT_CLK_period : time := 4 ns;  -- Sets Bit Clock for Audio Codec to the necessary 25 MHz

	-- FSM Control signals
	type state_type is (RST, WAIT_TRIGGER, STORE_SAMPLE, WAIT_SAMPLE);
	signal state: state_type;


BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: lab2_datapath PORT MAP (
          clk => clk,
          reset_n => reset_n,
  		  ac_mclk => ac_mclk,
          ac_adc_sdata => ac_adc_sdata,
          ac_dac_sdata => ac_dac_sdata,
          ac_bclk => ac_bclk,
          ac_lrclk => ac_lrclk,
          scl => scl,
          sda => sda,
--          AC97_n_RESET => AC97_n_RESET,
          tmds => tmds,
          tmdsb => tmdsb,
          sw => sw,
          cw => cw,
          btn => btn,
          exWrAddr => exWrAddr,
          exWen => exWen,
          exSel => exSel,
          Lbus_out => Lbus_out,
          Rbus_out => Rbus_out,
          exLbus => exLbus,
          exRbus => exRbus,
          flagQ => flagQ,
          flagClear => flagClear,
          switch => switch
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   BIT_CLK_process :process
   begin
		BIT_CLK <= '0';
		wait for BIT_CLK_period/2;
		BIT_CLK <= '1';
		wait for BIT_CLK_period/2;
   end process;
    
    SDATA_process :process  -- Inputs alternating 1's and 0's on each Bit Clock
    begin
         ac_adc_sdata <= '0';
         wait for BIT_CLK_period;
         ac_adc_sdata <= '1';
         wait for BIT_CLK_period*2;
    end process;

    -- Stimulus process
   stim_proc: process 
   begin		
      -- hold reset state for 100 ns.
		reset_n <= '0', '1' after 10 ns;
		cw <= "011", "011" after 30 ns;
			-- insert stimulus here 
		wait;
   end process;

END;
