# Lab 2

## Author: C2C Jeremy Schwartz

### Last Modified: 20 Jan 2018

**Documentation:** None

#### Gate Check 1

Gate Check 1 was demoed to Dr. York at 1120 on 8 Feb 2018.

#### Gate Check 2

Gate Check 2 was demoed to Dr. York at 0950 on 12 Feb 2018.

#### Functionality Check

A Functionality was demoed to Dr. York at 1030 on 12 Feb 2018.

-------------------------------------------------------------------------------

#### Introduction

In this lab, I continued developing my oscilloscope from Lab 1.  I implemented the ability to display two signals on the screen that were played into the input audio port of the Nexys Video.  This lab implements the Control Unit/Datapath design scheme and uses BRAM to store audio samples.  Additionally, a flag register was implemented that will be used in Lab 3.

#### Implementation
![Schematic](images/schematic.png)
##### Figure 1: Schematic

##### Explanation of Custom Modules

**Lab2_dp**

 - Purpose: Collect samples, determine triggering, and tell the video module when to color in pixels for each signal.
 - Inputs:
     - clk: Master Clock
     - reset_n: Master Reset
     - ac_adc_sdata: Audio serial in.
     - sw: Status Word - sw(2) = Triggered, sw(1) = Done, sw(0) = Ready
     - switch: Switch bus.
     - btn: Button bus.
     - exWrAddr: External write address.
     - exWen: External write enable.
     - exSel: Select line that determines whether to use internal or external signals for the BRAM.
     - exLbus/exRbus: External left and right signals.
     - flagClear: Flag clear line for the Microblaze (currently unconnected)
 - Outputs:
     - ac_mclk: Master clock for audio codec.
     - ac_dac_sdata: Audio serial out.
     - ac_bclk: Serial clock for audio codec.
     - ac_lrclk: Channel clock for audio codec.
     - tmds/tmdsb: HDMI out signals.
     - cw: Control Word - cw(2 downto 1) = Counter Control, cw(0) = Write Enable
     - Lbus_out/Rbus_out: Output of left and right signals to the Microblaze
     - flagQ: Flag output for Microblaze (currently unconnected)
 - Inouts:
     - scl: Serial clock signal.
     - sda: Serial data signal.
 - Behavior: The datapath receives serial audio data and converts it to parallel data.  That data is then converted from signed 18 bit to unsigned 9 bit (0 to 511).  This data is fed into the BRAM.  The datapath also determines when to trigger and when to enable ch1 and ch2.

**Lab2_fsm**

 - Purpose: Implement an FSM to control the BRAM write enable and sample counter of Lab2_dp.
 - Inputs:
     - clk: Master Clock
     - reset_n: Master Reset
     - sw: Status Word - sw(2) = Triggered, sw(1) = Done, sw(0) = Ready
 - Outputs:
     - cw: Control Word - cw(2 downto 1) = Counter Control, cw(0) = Write Enable
 - Behavior: Wait for the trigger signal from the datapath, and then tell the datapath to store samples when they are ready until the datapath gives the done signal.

**Sample Counter**

 - Purpose: Count the current sample from 0 to 1023.  This count is used as the BRAM write address.
 - Inputs:
     - cntr_clk: Master Clock
     - reset: Master Board Reset
     - ctrl: "00" = hold, "01" = count up, "11" = reset to 0
 - Outputs:
     - sample_count: The current sample count (0-1023).
 - Behavior: Count up to 1023 and then rollover to 0.

**Flag Register**

 - Purpose: Store and modify certain flags to be used by the Microblaze.
 - Inputs:
     - clk: Master Clock
     - reset_n: Master Board Reset
     - set: A mask that determines which bits to set.
     - clear: A mask that determines which bits to clear.
 - Outputs:
     - Q: The actual byte of 8 flag bits.
 - Bits are set from the datapath and cleared from the Microblaze according to the mask inputs.

#### Testing/Debug

**Testing:** In order to verify that my finished designed met the specifications of this lab, I drove a series of signals using a function generator to verify that the design worked.  I varied the frequency, amplitude, and shape of the signals.  I also moved the voltage trigger to verify that the displayed signal would also move.

**Debugging:** The main problem I faced while completing this lab was that my signals would not be displayed on the screen.  Eventually I traced this problem back to the fact that no data was being saved in the BRAM.  I eventually determined that this was caused by the BRAM reset, which was active high, unlike every other reset in the project.  After fixing this, my signals still would not display.  I decided to verify the datapath after BRAM by feeding a constant into the BRAM.  This displayed a line on my screen, which lead me to realize that my Signed2Unsigned logic was broken.  After simplifying this logic, the project worked as expected.

#### Conclusion

In this lab I learned how to use memory modules in a VHDL project.  I also gained practical experience using the Datapath/Control Unit design scheme.  Finally, I learned the importance of careful coding and the use of testbenches due to the very long synthesis and implementation time for this project.  In the future I would recommend removing the lab report requirement for this lab.  Writing the lab report was not very educational and just seemed like busy work.  Also, assigning a homework problem in which you need to use BRAM would be useful leading up to this lab.