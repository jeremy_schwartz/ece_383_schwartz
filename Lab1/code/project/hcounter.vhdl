--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 28 Jan 2018
-- Course: ECE 383
-- File: hcounter.vhdl
-- Project: Lab1
--
-- Purp: This file implements the horizontal counting module (mod 800).
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity hcounter is
        port(	cntr_clk, reset: in std_logic; 
                ctrl: in std_logic;
                roll: out std_logic;
                column_count: out unsigned(9 downto 0));
end hcounter;

architecture behavior of hcounter is
    signal processQ: unsigned (9 downto 0);

    begin
        process(cntr_clk)
        begin
        if (rising_edge(cntr_clk)) then
            if (reset = '0') then
                processQ <= (others => '0');
            elsif ((processQ < 799) and (ctrl = '1')) then
                processQ <= processQ + 1;
            elsif ((processQ = 799) and (ctrl = '1')) then
                processQ <= (others => '0');
            end if;
        end if;
    end process;

    roll  <= '1' when (processQ = 799) else '0';
    column_count <= processQ;

end behavior;