--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 28 Jan 2018
-- Course: ECE 383
-- File: vga.vhdl
-- Project: Lab1
--
-- Purp: This file implements the vga module for Lab 1 O-Scope functionality.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity vga is
	Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			h_sync : out  STD_LOGIC;
			v_sync : out  STD_LOGIC; 
			blank : out  STD_LOGIC;
			r: out STD_LOGIC_VECTOR(7 downto 0);
			g: out STD_LOGIC_VECTOR(7 downto 0);
			b: out STD_LOGIC_VECTOR(7 downto 0);
			trigger_time: in unsigned(9 downto 0);
			trigger_volt: in unsigned (9 downto 0);
			row: out unsigned(9 downto 0);
			column: out unsigned(9 downto 0);
			ch1: in std_logic;
			ch1_enb: in std_logic;
			ch2: in std_logic;
			ch2_enb: in std_logic);
end vga;

architecture Behavioral of vga is

component scopeFace is
    Port (  row : in  unsigned(9 downto 0);
            column : in  unsigned(9 downto 0);
		    trigger_volt: in unsigned(9 downto 0);
		    trigger_time: in unsigned(9 downto 0);
            r : out  std_logic_vector(7 downto 0);
            g : out  std_logic_vector(7 downto 0);
            b : out  std_logic_vector(7 downto 0);
			ch1: in std_logic;
			ch1_enb: in std_logic;
			ch2: in std_logic;
			ch2_enb: in std_logic);
end component;

component hcounter is
        port(	cntr_clk, reset: in std_logic; 
                ctrl: in std_logic;
                roll: out std_logic;
                column_count: out unsigned(9 downto 0));
end component;

component vcounter is
        port(	cntr_clk, reset: in std_logic; 
                ctrl: in std_logic;
                roll: out std_logic;
                row_count: out unsigned(9 downto 0));
end component;

signal horiz_roll_to_vert_ctrl, h_blank, v_blank: std_logic;
signal row_s, column_s: unsigned(9 downto 0);

begin   

	horiz_counter:	hcounter
	port map (		
		cntr_clk => clk,
		reset => reset_n,
		ctrl => '1',
		roll => horiz_roll_to_vert_ctrl,
		column_count => column_s);
		
	vert_counter:  vcounter
	port map (		
		cntr_clk => clk,
        reset => reset_n,
        ctrl => horiz_roll_to_vert_ctrl,
        -- roll => ,
        row_count => row_s);
        
    sF: scopeFace
    port map (
        row => row_s,
        column => column_s,
        trigger_volt => trigger_volt,
        trigger_time => trigger_time,
        r => r,
        g => g,
        b => b,
        ch1 => ch1,
        ch1_enb => ch1_enb,
        ch2 => ch2,
        ch2_enb => ch2_enb);
        
row <= row_s;
column <= column_s;
        
h_blank <= '0' when ((column_s >= 0) and (column_s <= 639)) else '1';
v_blank <= '0' when ((row_s >= 0) and (row_s <= 479)) else '1';

h_sync <= '0' when ((column_s >= 656) and (column_s <= 751)) else '1';
v_sync <= '0' when ((row_s >= 490) and (row_s <= 491)) else '1';

blank <= h_blank or v_blank;

end Behavioral;
