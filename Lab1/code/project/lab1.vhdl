--------------------------------------------------------------------
-- Name: C2C Jeremy Schwartz
-- Date: 28 Jan 2018
-- Course: ECE 383
-- File: lab1.vhdl
-- Project: Lab1
--
-- Purp: This file implements the top level for lab 1 o-scope functionality.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity lab1 is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
		   btn: in	STD_LOGIC_VECTOR(4 downto 0);
		   sw: in STD_LOGIC_VECTOR(2 downto 0);
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0));
end lab1;

architecture structure of lab1 is

	signal trigger_time, trigger_volt, row, column: unsigned(9 downto 0);
	signal old_button, button_activity: std_logic_vector(4 downto 0);
	signal ch1_wave, ch2_wave, trigger_clock: std_logic;
	signal clock_divider: unsigned(22 downto 0);
	
	component video is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
           trigger_time: in unsigned(9 downto 0);
           trigger_volt: in unsigned (9 downto 0);
           row: out unsigned(9 downto 0);
           column: out unsigned(9 downto 0);
           ch1: in std_logic;
           ch1_enb: in std_logic;
           ch2: in std_logic;
           ch2_enb: in std_logic);
	end component;

begin

    clock_divider_process: process(clk)
    begin
        if rising_edge(clk) then
            clock_divider <= clock_divider + to_unsigned(1, 23);
        end if;
    end process;

    voltage_trigger_process: process(trigger_clock)
    begin
        if rising_edge(trigger_clock) then
            if reset_n = '0' then
                trigger_volt <= to_unsigned(200, 10);
             elsif btn(0) = '1' and trigger_volt > 0 then
                trigger_volt <= trigger_volt - to_unsigned(1, 10);
              elsif btn(2) = '1' and trigger_volt < 400 then
                trigger_volt <= trigger_volt + to_unsigned(1, 10);
              elsif btn(4) = '1' then
                trigger_volt <= to_unsigned(200, 10);
             end if;
        end if;
    end process;
    
    time_trigger_process: process(trigger_clock)
    begin
        if rising_edge(trigger_clock) then
            if reset_n = '0' then
                trigger_time <= to_unsigned(300, 10);
             elsif btn(1) = '1' and trigger_time > 0 then
                trigger_time <= trigger_time - to_unsigned(1, 10);
              elsif btn(3) = '1'  and trigger_time < 600 then
                trigger_time <= trigger_time + to_unsigned(1, 10);
              elsif btn(4) = '1' then
                trigger_time <= to_unsigned(300, 10);
             end if;
        end if;
    end process;

	video_inst: video port map( 
		clk => clk,
		reset_n => reset_n,
		tmds => tmds,
		tmdsb => tmdsb,
		trigger_time => trigger_time,
		trigger_volt => trigger_volt,
		row => row, 
		column => column,
		ch1 => ch1_wave,
		ch1_enb => sw(0),
		ch2 => ch2_wave,
		ch2_enb => sw(1)); 
		
    trigger_clock <= clock_divider(21) when sw(2) = '1' else clock_divider(22);
	
end structure;
