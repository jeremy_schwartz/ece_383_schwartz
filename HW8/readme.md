# Homework #8

## Author: C2C Jeremy Schwartz
### Last Modified: 4 Feb 2018

#### Datapath Diagram

- See physical turn in.

#### State Diagram

- See physical turn in.

#### Control Word Table

|     State    | Shift | Counter |   CW    |
|:------------:|:-----:|:-------:|:-------:|
|   waitData   |   0   |    00   |   000   |
|   initLoop   |   0   |    11   |   011   |
|    waitLow   |   0   |    00   |   000   |
| performShift |   1   |    01   |   101   |
|   waitHigh   |   0   |    00   |   000   |
|   endState   |   0   |    00   |   000   |

#### VHDL Code

- See code folder.

#### Testbench Results

![Waves](images/waves.png)
##### Figure 1: Testbench Results

- Drag the testbench image into a new tab to zoom in!