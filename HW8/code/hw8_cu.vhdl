----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw8_cu.vhdl
-- HW:		HW8
-- Purp:	This file implements the control unit for HW8.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;		
use IEEE.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

entity hw8_cu is
	Port(	clk: in  STD_LOGIC;
            reset : in  STD_LOGIC;
            kbClk: in std_logic;
            cw: out STD_LOGIC_VECTOR(2 downto 0);
            sw: in STD_LOGIC;
            busy: out std_logic);
end hw8_cu;

architecture behavior of hw8_cu is

	type state_type is (waitData, initLoop, waitLow, performShift, waitHigh, endState);
	signal state: state_type;
	
begin
	
    state_process: process(clk)
    begin
        if rising_edge(clk) then
            if (reset = '0') then 
                state <= waitData;
            else
                case state is
                when waitData =>
                    if (kbClk = '0') then
                        state <= initLoop;
                    end if;                        
                when initLoop =>
                    state <= waitLow;      
                when waitLow =>
                    if (kbClk = '0') then
                        state <= performShift;
                    end if;                  
                when performShift =>
                    state <= waitHigh;          
                when waitHigh =>
                    if (kbClk = '1') then
                        if (sw = '1') then
                            state <= endState;
                        else
                            state <= waitLow;
                        end if;
                    end if;                          
                when endState =>
                    state <= waitData;   
                end case;
            end if;
        end if;
    end process;

cw <=   "000" when state = waitData else
        "011" when state = initLoop else
        "000" when state = waitLow else 
        "101" when state = performShift else
        "000" when state = waitHigh else 
        "000" when state = endState else 
        "000"; -- Catch All
        
busy <= '0' when state = waitData else
        '1'; -- All other states
	
end behavior;