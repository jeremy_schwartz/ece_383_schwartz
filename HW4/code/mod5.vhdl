----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	mod5.vhd
-- HW:		HW4
-- Purp:	This design implements a mod 5 counter.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;						-- These lines are similar to a #include in C
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity mod5 is
        port(	clk, reset: in std_logic; 
                ctrl: in std_logic;
                roll: out std_logic;
                Q: out unsigned(2 downto 0));
end mod5;

architecture behavior of mod5 is
    signal rollSynch, rollCombo: STD_LOGIC;
    signal processQ: unsigned (2 downto 0);

    begin
        process(clk)
        begin
        if (rising_edge(clk)) then
            if (reset = '0') then
                processQ <= (others => '0');
                rollSynch <= '0';
            elsif ((processQ < 4) and (ctrl = '1')) then
                processQ <= processQ + 1;
                rollSynch <= '0';
            elsif ((processQ = 4) and (ctrl = '1')) then
                processQ <= (others => '0');
                rollSynch <= '1';
            end if;
        end if;
    end process;

    rollCombo  <= '1' when (processQ = 4) else '0';
    Q <= processQ;
    roll <= rollCombo;

end behavior;