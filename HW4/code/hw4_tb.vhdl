----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw4.vhd
-- HW:		HW4
-- Purp:	Tests a design that implements a two stage counter.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity hw4_tb is 
end entity hw4_tb;

architecture behavior of hw4_tb is

	----------------------------------------------------------------------
	-- These signal names must match the names of the I/O markers
	----------------------------------------------------------------------
    component hw4 is
            port(   clk_top, reset_top: in std_logic; 
                    ctrl_top: in std_logic;
                    Q1, Q0: out unsigned(2 downto 0));
    end component;
	
	signal clk_s, reset_s, ctrl_s: std_logic := '0';
	signal Q1_s, Q0_s: unsigned(2 downto 0) := "000";
	
begin

	-- Clock Process ------------------------------------
	clk_process : process
	begin
		clk_s <= '0';
		wait for 500ps;
		
		clk_s <= '1';
		wait for 500ps;
	end process clk_process;
	-----------------------------------------------------	

	----------------------------------------------------------------------
	-- Create an instance of hw1
	----------------------------------------------------------------------
	UUT:	hw4 port map ( clk_top => clk_s,
	                       reset_top => reset_s,
	                       ctrl_top => ctrl_s,
	                       Q1 => Q1_s, 
	                       Q0 => Q0_s);

    reset_s <= '0', '1' after 1ns;
    ctrl_s <= '0', '1' after 1ns, '0' after 5ns, '1' after 6ns;

end architecture behavior;
