----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw4.vhd
-- HW:		HW4
-- Purp:	This design implements a two stage counter.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;						-- These lines are similar to a #include in C
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity hw4 is
        port(	clk_top, reset_top: in std_logic; 
		        ctrl_top: in std_logic;
		        Q1, Q0: out unsigned(2 downto 0));
end hw4;

architecture structural of hw4 is

component mod5 is
        port(	clk, reset: in std_logic; 
                ctrl: in std_logic;
                roll: out std_logic;
                Q: out unsigned(2 downto 0));
end component;

signal roll_out, ctrl_in: std_logic;

begin

	mod5_0:	mod5
	port map (		
		clk => clk_top,
		reset => reset_top,
		ctrl => ctrl_top,
		roll => roll_out,
		Q => Q0);
		
	mod5_1:	mod5
	port map (		
		clk => clk_top,
        reset => reset_top,
        ctrl => ctrl_in,
        -- roll => UNCONNECTED,
        Q => Q1);
        
ctrl_in <= '1' when roll_out = '1' and ctrl_top = '1' else '0';
        
end structural;