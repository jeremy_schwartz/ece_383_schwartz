# Homework #4

## Author: C2C Jeremy Schwartz

### Last Modified: 17 Jan 2018

![Test Bench](images/waves.png)
##### Figure 1: Test Bench Wave Forms

![Schematic](images/schem.png)
##### Figure 2: Cascaded Counter Schematic

### Code

Code is in my "code" folder for HW4.