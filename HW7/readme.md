# Homework #7

## Author: C2C Jeremy Schwartz
### Last Modified: 31 Jan 2018

#### Question 1: Timing Diagram

![Timing Diagram](images/timing.png)
##### Figure 1: Timing Diagram

#### Question 2: FSM

![FSM Diagram](images/diagram.png)
##### Figure 2: FSM Diagram

![Waves](images/waves.png)
##### Figure 3: Testbench Results

**Code:** See code folder for VHDL files.