----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw7_top.vhd
-- HW:		HW7
-- Purp:	This file implements the top level for HW7.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity hw7 is
	Port(	clk_top: in  STD_LOGIC;
			reset_top: in STD_LOGIC;
			btn_top: in STD_LOGIC_VECTOR(1 downto 0);
			q_top: out UNSIGNED(2 downto 0));
end hw7;

architecture behavior of hw7 is

component hw7_fsm is
    Port(	clk: in  STD_LOGIC;
            reset: in  STD_LOGIC;
            btn: in STD_LOGIC_VECTOR(1 downto 0);
            output: out STD_LOGIC_VECTOR(1 downto 0));
end component;

component generic_counter is
	generic (N: integer := 4);
	port(	clk: in  STD_LOGIC;
            reset : in  STD_LOGIC;
            crtl: in std_logic_vector(1 downto 0);
            D: in unsigned (N-1 downto 0);
            Q: out unsigned (N-1 downto 0));
end component;
	
signal control_s: STD_LOGIC_VECTOR(1 downto 0);
	
begin

    fsm_inst:	hw7_fsm
    port map (		
        clk => clk_top,
        reset => reset_top,
        btn => btn_top,
        output => control_s);
    
    counter_inst: generic_counter 
    generic map(3)
    port map (
        clk => clk_top,
        reset => reset_top,
        crtl => control_s,
        D => "000",
        Q => q_top);

end behavior;