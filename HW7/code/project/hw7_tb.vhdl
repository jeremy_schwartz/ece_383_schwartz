----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw7_tb.vhd
-- HW:		HW7
-- Purp:	Tests functionality for HW7.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY hw7_tb IS
END hw7_tb;
 
ARCHITECTURE behavior OF hw7_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    component hw7 is
        Port(   clk_top: in  STD_LOGIC;
                reset_top: in STD_LOGIC;
                btn_top: in STD_LOGIC_VECTOR(1 downto 0);
                q_top: out UNSIGNED(2 downto 0));
    end component;
    
   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal btn : std_logic_vector(1 downto 0) := (others => '0');

 	--Outputs
   signal q : unsigned(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 100 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: hw7
	PORT MAP (
          clk_top => clk,
          reset_top => reset,
          btn_top => btn,
          q_top => q
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
	btn <= "00",
	"10" after 2us, "00" after 2.5us,
	"10" after 3us, "00" after 3.5us,
	"01" after 4us, "00" after 4.5us,
	"01" after 5us, "00" after 5.5us,
	"10" after 6us, "00" after 6.5us,
	"01" after 7us, "00" after 7.5us;
	reset <= '0', '1' after 1us;

END;
