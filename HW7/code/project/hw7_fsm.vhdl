----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw7_fsm.vhd
-- HW:		HW7
-- Purp:	This file implements the debouncing FSM for HW7.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity hw7_fsm is
	Port(	clk: in  STD_LOGIC;
			reset: in  STD_LOGIC;
			btn: in STD_LOGIC_VECTOR(1 downto 0);
			output: out STD_LOGIC_VECTOR(1 downto 0));
end hw7_fsm;

architecture behavior of hw7_fsm is

	type state_type is (waitLeft, waitRight, incrementCounter);
	signal state: state_type;
	
    constant left: integer := 1;
    constant right: integer := 0;
	
begin

    state_process: process(clk)
	 begin
		if rising_edge(clk) then
			if (reset = '0') then 
				state <= waitLeft;
			else
				case state is
					when waitLeft =>
                        if (btn(left) = '1') then
                            state <= waitRight;
                        end if;
					when waitRight =>
                        if (btn(right) = '1') then
                            state <= incrementCounter;
                        end if;
					when incrementCounter =>
						state <= waitLeft;							
				end case;
			end if;
		end if;
	end process;

	output <= "00" when state = waitLeft else
			"00" when state = waitRight else
			"01" when state = incrementCounter else 
			"00"; -- Catch All

end behavior;