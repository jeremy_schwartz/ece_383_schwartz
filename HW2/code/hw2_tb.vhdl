----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw2_tb.vhd
-- HW:		HW2
-- Purp:	Tests a PS2 scancode decoder.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity hw2_tb is 
end entity hw2_tb;

architecture behavior of hw2_tb is

	----------------------------------------------------------------------
	-- These signal names must match the names of the I/O markers
	----------------------------------------------------------------------
	component hw2 is
        port(D: in std_logic_vector(7 downto 0);
            H: out std_logic_vector(3 downto 0));
	end component;
	
	signal D_s : std_logic_vector(7 downto 0);
	signal H_s : std_logic_vector(3 downto 0);
	  
	CONSTANT TEST_ELEMENTS:integer:=11;
	SUBTYPE INPUT is std_logic_vector(7 downto 0);
	TYPE TEST_INPUT_VECTOR is array (1 to TEST_ELEMENTS) of INPUT;
	SIGNAL TEST_IN: TEST_INPUT_VECTOR := (x"45", x"16", x"1E", x"26", x"25", x"2E", x"36", x"3D", x"3E", x"46", x"00");

    SUBTYPE OUTPUT is std_logic_vector(3 downto 0);
	TYPE TEST_OUTPUT_VECTOR is array (1 to TEST_ELEMENTS) of OUTPUT;
	SIGNAL TEST_OUT: TEST_OUTPUT_VECTOR := (x"0", x"1", x"2", x"3", x"4", x"5", x"6", x"7", x"8", x"9", x"0");

	SIGNAL i : integer;		

begin

	----------------------------------------------------------------------
	-- Create an instance of hw1
	----------------------------------------------------------------------
	UUT:	hw2 port map (D_s, H_s);

	tb : PROCESS
	BEGIN
	for i in 1 to TEST_ELEMENTS loop
		-----------------------------------------
		-- Parse out the bits of the test_vector
		-----------------------------------------
		D_s <= TEST_IN(i);
		
		wait for 10 ns; 
		assert D_s = test_out(i)
 				report "Error with input " & integer'image(i) & " in D."
				severity warning;
				
	end loop;
	
	---------------------------
	-- Just halt the simulator
	---------------------------
	assert TRUE = FALSE 
		report "---------------Self-checking testbench completed.  Nominal circuit behavior---------------"
		severity failure;
			
	END PROCESS tb;

end architecture behavior;
