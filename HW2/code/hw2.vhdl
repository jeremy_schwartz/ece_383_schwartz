----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw2.vhd
-- HW:		HW2
-- Purp:	Implements a PS2 scancode decoder.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity hw2 is
        port(D: in std_logic_vector(7 downto 0);
                H: out std_logic_vector(3 downto 0));
end hw2;

architecture behavior of hw2 is
-- signal	s1, s2, s3: std_logic;	-- wires which begin and end in the component

begin
	H <=   x"0" when D=x"45" else
		   x"1" when D=x"16" else
		   x"2" when D=x"1E" else
		   x"3" when D=x"26" else
		   x"4" when D=x"25" else
		   x"5" when D=x"2E" else
		   x"6" when D=x"36" else
		   x"7" when D=x"3D" else
		   x"8" when D=x"3E" else
		   x"9" when D=x"46" else
		   x"0";
end behavior;
