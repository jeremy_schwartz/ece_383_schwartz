# Homework #9

## Author: C2C Jeremy Schwartz
### Last Modified: 26 Feb 2018

Homework 9 was completed on 25 Feb 2018.  See the pictures below for proof of functionality.  See the project folder in the HW9 code folder for lec17 code files.

![Hello World](images/proof.PNG)
##### Figure 1: Proof of Functionality (I Used a For Loop Too)