----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw3.vhd
-- HW:		HW3
-- Purp:	This design detects when an 8-bit number is divisible by 17.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;						-- These lines are similar to a #include in C
use IEEE.std_logic_1164.all;

entity hw3 is
        port(input:	in std_logic_vector(7 downto 0); 
			   led: out std_logic);
end hw3;

architecture structure of hw3 is
-- signal	s1, s2, s3: std_logic;	-- wires which begin and end in the component

begin 
	led <= '0' when input = x"00" else
	       '1' when (input(7 downto 4) = input(3 downto 0)) else '0';
end structure;
