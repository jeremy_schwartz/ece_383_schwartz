----------------------------------------------------------------------------------
-- Name:	C2C Jeremy Schwartz
-- Date:	Spring 2018
-- Course: 	ECE 383
-- File: 	hw3.vhd
-- HW:		HW3
-- Purp:	Tests a design that detects when an 8-bit number is divisible by 17.
--
-- Doc:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity hw3_tb is 
end entity hw3_tb;

architecture behavior of hw3_tb is

	----------------------------------------------------------------------
	-- These signal names must match the names of the I/O markers
	----------------------------------------------------------------------
	component hw3 is
        port(input:	in std_logic_vector(7 downto 0); 
           led: out std_logic);
	end component;
	
	signal led_s : std_logic;
	signal input_s : std_logic_vector(7 downto 0);
	  
	CONSTANT TEST_ELEMENTS:integer:=20;
	SUBTYPE INPUT is std_logic_vector(7 downto 0);
	TYPE TEST_INPUT_VECTOR is array (1 to TEST_ELEMENTS) of INPUT;
	SIGNAL TEST_IN: TEST_INPUT_VECTOR := (x"00", x"11", x"22", x"33", x"44", x"55", x"66", x"77", x"88", x"99", x"AA", x"BB", x"CC", x"DD", x"EE", x"FF", x"AB", x"45", x"01", x"FE");

	TYPE TEST_OUTPUT_VECTOR is array (1 to TEST_ELEMENTS) of std_logic;
	SIGNAL TEST_OUT: TEST_OUTPUT_VECTOR := ('0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0');
	
	SIGNAL i : integer;		

begin

	----------------------------------------------------------------------
	-- Create an instance of hw1
	----------------------------------------------------------------------
	UUT:	hw3 port map (input_s, led_s);

	tb : PROCESS
	BEGIN
	for i in 1 to TEST_ELEMENTS loop
		-----------------------------------------
		-- Parse out the bits of the test_vector
		-----------------------------------------
		input_s <= test_in(i);
		
		wait for 10 ns; 
		assert led_s = test_out(i)
 				report "Error with input " & integer'image(i) & " in LED"
				severity warning;
				
	end loop;
	
	---------------------------
	-- Just halt the simulator
	---------------------------
	assert TRUE = FALSE 
		report "---------------Self-checking testbench completed.  Nominal circuit behavior---------------"
		severity failure;
			
	END PROCESS tb;

end architecture behavior;
